<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/4/2015
 * Time: 11:17 AM
 */

namespace App\Utilities;


use Exception;

class ClassMap {

    //return instance of a class that is specified in config -> classmap.php or throw an a exception
    public static function instance($name){
        if(class_exists($name)){
            return new $name();
        }else{
            throw new Exception("Factory error in ClassMap for value:" . $name, 1);
        }
    }

}