<?php namespace App\Utilities;
use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\File;
use App\Models\Project;

class FileStorage{

    public static function name($type, $params, $file)
    {
        //generate name for storing files on s3 - based on last inserted file id
        switch($type) {
            case "proposal":
                $name = 'project_'.$params['proj_id'].'_'.$type.'_'.$params['prop_id'].'_supplier_'.$params['supp_id'].'_'.self::fileID().'.'.$file->guessClientExtension();
                return $name;
                break;
            case "contract":
                $name = 'project_'.$params['proj_id'].'_'.$type.'_'.$params['contr_id'].'_'.self::fileID().'.'.$file->guessClientExtension();
                return $name;
                break;
            case "submittal":
                return '';
                break;
            default:
                return '';
        }
    }
    public static function updateName($type, $params, $file, $file_id)
    {
        //generate name for updating files on s3 - based on file_id
        switch($type) {
            case "proposal":
                $name = 'project_'.$params['proj_id'].'_'.$type.'_'.$params['prop_id'].'_supplier_'.$params['supp_id'].'_'.$file_id.'.'.$file->guessClientExtension();
                return $name;
                break;
            case "contract":
                $name = 'project_'.$params['proj_id'].'_'.$type.'_'.$params['contr_id'].'_'.$file_id.'.'.$file->guessClientExtension();
                return $name;
                break;
            case "submittal":
                return '';
                break;
            default:
                return '';
        }
    }



    public static function path($type, $params)
    {
        //generate path for storing files on s3
        $project= Project::where('id', '=', $params['proj_id'])->firstOrFail();
        $companyId = $project->comp_id;

        switch($type) {
            case "proposal":
                $path = '/company_'.$companyId.'/project_'.$params['proj_id'].'/'.$type.'s/supplier_'.$params['supp_id'];
                return $path;
                break;
            case "contract":
                $path = '/company_'.$companyId.'/project_'.$params['proj_id'].'/'.$type.'s/contract_'.$params['contr_id'];
                return $path;
                break;
            case "submittal":
                return '';
                break;
            default:
                return '';
        }
    }
    public static function pathFromName($type, $name, $projectId)
        {
            //split name
            $segments = explode('_',$name);
            $project= Project::where('id', '=', $projectId)->firstOrFail();
            $companyId = $project->comp_id;

            switch($type) {
                case "contracts":
                    $path = '/company_'.$companyId.'/'.$segments[0].'_'.$segments[1].'/contracts/'.$segments[2].'_'.$segments[3];
                    break;
                case "proposals":
                    $path = '/company_'.$companyId.'/'.$segments[0].'_'.$segments[1].'/proposals/'.$segments[4].'_'.$segments[5];
                    break;
                case "submittals":
                    $path = '/company_'.$companyId.'/'.$segments[0].'_'.$segments[1].'/submittals/';
                    break;
                default:
                    $path = '/company_'.$companyId.'/project_'.$projectId.'/'.$type;
            }
            return $path;

        }

    private static function fileID()
    {
        //get last insert id and return +1 for new record
        $lastID = DB::select( DB::raw("SELECT id FROM files ORDER BY id DESC LIMIT 1;") );
        if (sizeof($lastID) == 0) {
            return 1;
        }
        else {
            return $lastID[0]->id + 1;
        }
    }

}

?>