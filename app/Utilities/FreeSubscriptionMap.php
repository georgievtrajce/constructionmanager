<?php
namespace App\Utilities;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class FreeSubscriptionMap {

    public static function checkReferralPoints($referralPoints)
    {
        switch ($referralPoints) {
            case (Auth::user()->company->subs_id == Config::get('subscription_levels.level1.id') || Auth::user()->company->subs_id == Config::get('subscription_levels.level2.id')) && ($referralPoints >= Config::get('subscription_levels.referral-points.level2.ref_points_needed') && $referralPoints < Config::get('subscription_levels.referral-points.level3.ref_points_needed')):
                return $data['referralPointsLabel'] = 'referral_points_for_level_2';
                break;
            case (Auth::user()->company->subs_id == Config::get('subscription_levels.level1.id') || Auth::user()->company->subs_id == Config::get('subscription_levels.level2.id') || Auth::user()->company->subs_id == Config::get('subscription_levels.level3.id')) && ($referralPoints >= Config::get('subscription_levels.referral-points.level3.ref_points_needed')):
                return $data['referralPointsLabel'] = 'referral_points_for_level_3';
                break;
            default:
                return $data['referralPointsLabel'] = 'referral_points';
        }
    }

}