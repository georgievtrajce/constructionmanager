<?php namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        Validator::extend('recaptcha', 'App\\Validators\\ReCaptcha@validate');
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
        $this->app->bind('App\Modules\Files\Interfaces\CheckUploadedFilesInterface', 'App\Modules\Files\Implementations\CheckUploadedFiles');
        $this->app->bind('App\Modules\Project\Interfaces\ProjectInterface', 'App\Modules\Project\Implementations\Project');

		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}

}
