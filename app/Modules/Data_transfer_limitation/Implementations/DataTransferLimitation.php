<?php
namespace App\Modules\Data_transfer_limitation\Implementations;

use App\Models\Company;
use App\Models\Company_usage_history;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class DataTransferLimitation {

    private $companyId;
    private $companyRepo;
    private $company;
    private $filesRepo;
    /**
     * @var MailRepository
     */
    private $mailRepository;

    /**
     * DataTransferLimitation constructor.
     */
    public function __construct()
    {
        if (Auth::user() != null) {
            $this->companyId = Auth::user()->comp_id;
            $this->company = Auth::user()->company;
        }
        $this->companyRepo = new CompaniesRepository();
        $this->filesRepo = new FilesRepository();
        $this->mailRepository = new MailRepository();
    }

    /**
     * Check if company is allowed to make data transfer (upload and download limit sum)
     * @param $fileSize
     * @return bool
     */
    public function checkDataTransferAllowance($fileSize)
    {
        $company = $this->companyRepo->getCompany($this->companyId);

        if (($company->limit_month == null && $company->limit_year == null) || ($company->limit_month == Carbon::now()->month && $company->limit_year == Carbon::now()->year)) {
            $dataTransferMade = (float)$company->uploaded + (float)$company->downloaded;
            $expectedTransfer = $dataTransferMade + (float)$fileSize;
        } else {
            $expectedTransfer = (float)$fileSize;
        }

        //check allowance
        if ($expectedTransfer > ((float)$company->subscription_type->upload_limit + (float)$company->subscription_type->download_limit)) {
            return false;
        }

        return true;
    }

    /**
     * Check if company is allowed to upload files
     * @param $fileSize
     * @param null $companyId
     * @return bool
     */
    public function checkUploadTransferAllowance($fileSize, $companyId = null)
    {
        if (empty($companyId)) {
            $company = $this->companyRepo->getCompany($this->companyId);
        } else {
            $company = $this->companyRepo->getCompany($companyId);
        }


        if (($company->limit_month == null && $company->limit_year == null) || ($company->limit_month == Carbon::now()->month && $company->limit_year == Carbon::now()->year)) {
            $expectedTransfer = (float)$company->uploaded + (float)$fileSize;
        } else {
            $expectedTransfer = (float)$fileSize;
        }

        //check allowance
        if ($expectedTransfer > (float)$company->subscription_type->upload_limit) {
            return false;
        }

        return true;
    }

    /**
     * Check if company is allowed to store files
     * @param $fileSize
     * @param null $companyId
     * @return bool
     */
    public function checkStorageAllowance($fileSize, $companyId = null)
    {
        if (empty($companyId)) {
            $company = $this->companyRepo->getCompany($this->companyId);
        } else {
            $company = $this->companyRepo->getCompany($companyId);
        }

        //calculate current storage
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        $expectedStorage = $currentStorage + (float)$fileSize;

        //check allowance
        if ($expectedStorage > (float)$company->subscription_type->storage_limit) {
            return false;
        }

        return true;
    }

    /**
     * Get storage used percentage
     * @param $size
     * @return bool
     */
    public function findStorageUsedAllowancePercentage($size)
    {
        $company = $this->companyRepo->getCompany($this->companyId);
        //check allowance

        $used = 0;
        if (!empty($limit = (float)$company->subscription_type->storage_limit) && $size > 0) {
            $used = $size/$limit*100;
        }

        return round($used, 2);
    }

    /**
     * Calculate expected transfer and return allowance as boolean
     * @param $downloaded
     * @param $fileSize
     * @param $downloadLimit
     * @param $company
     * @return bool
     */
    public function calculateExpectedTransfer($downloaded, $fileSize, $downloadLimit, $company)
    {
        if (($company->limit_month == null && $company->limit_year == null) || ($company->limit_month == Carbon::now()->month && $company->limit_year == Carbon::now()->year)) {
            $expectedTransfer = (float)$downloaded + (float)$fileSize;
        } else {
            $expectedTransfer = (float)$fileSize;
        }

        //check allowance
        if ($expectedTransfer > (float)$downloadLimit) {
            return false;
        }
        return true;
    }

    /**
     * Check project creator company download transfer
     * @param $fileSize
     * @param $projectId
     * @return bool
     */
    public function checkParentDownloadTransferAllowance($fileSize, $projectId)
    {
        $projectsRepo = new ProjectRepository();
        $project = $projectsRepo->getSingleProject($projectId);

        if (!is_null($project)) {
            if (!is_null($project->company)) {
                if (!is_null($project->company->subscription_type)) {
                    return $this->calculateExpectedTransfer($project->company->downloaded, $fileSize,
                        $project->company->subscription_type->download_limit, $project->company);
                }
            }
        }
        return false;
    }

    /**
     * Check if company is allowed to download files
     * @param $fileSize
     * @param $projectId
     * @return bool
     */
    public function checkDownloadTransferAllowance($fileSize, $projectId)
    {
        /*
         * check if the download request comes from free Level 0 company
         * if the company is subscribed under Level 0 subscription and
         * Level 0 is free subscription, the data transfer should be increased from
         * the project creator limit
        */
        //if ($this->company->subs_id == Config::get('subscription_levels.level-zero') && $this->company->subscription_type->amount == 0) {
            return $this->checkParentDownloadTransferAllowance($fileSize, $projectId);
        //}

        //return $this->calculateExpectedTransfer($this->company->downloaded, $fileSize, $this->company->subscription_type->download_limit);
    }

    /**
     * Increase company upload transfer when company user or admin uploads new file
     * @param $fileSize
     */
    public function increaseUploadTransfer($fileSize)
    {
        if ($this->company->limit_month == null || ($this->company->limit_month == Carbon::now()->month && $this->company->limit_year == Carbon::now()->year)) {
            if ($this->company->limit_month == null) {
                $this->companyRepo->updateLimitMonthAndYear($this->companyId);
            }
            $increasedTransfer = (float)$this->company->uploaded + (float)$fileSize;
        } else {
            $this->companyRepo->addCompanyUsageHistory($this->company, 'upload', $this->company->uploaded);
            $this->companyRepo->addCompanyUsageHistory($this->company, 'download', $this->company->downloaded);
            $increasedTransfer = (float)$fileSize;
            $this->companyRepo->updateLimitMonthAndYear($this->companyId, true);
            $this->companyRepo->updateCompanyDownloadTransfer($this->companyId, 0);
        }


        $this->companyRepo->updateCompanyUploadTransfer($this->companyId, $increasedTransfer);

        $company = $this->companyRepo->getCompany($this->companyId);

        //upload limit notification
        $limit = $company->subscription_type->upload_limit;
        if ($increasedTransfer > ($limit*0.9) && $this->company->upload_notification == 0) {
            //send warning for upload
            $companyAdmin = $this->companyRepo->getCompanyAdminInfo($this->companyId);

            $data = [
                'email' => $companyAdmin->email,
                'name' => $companyAdmin->name,
                'type' => 'upload'
            ];

            if ($company->subs_id != intval(Config::get('subscription_levels.level0.id'))) {
                $this->mailRepository->sendExceededLimitNotification($data);
            }


            $this->companyRepo->updateCompanyMassAssign($company->id,['upload_notification' => 1]);
        }

        //storage exceeded notification
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        $expectedStorage = $currentStorage + (float)$fileSize;

        //check allowance
        if ($expectedStorage > ((float)$company->subscription_type->storage_limit*0.9) && $this->company->storage_notification == 0) {
            //send warning for storage
            $companyAdmin = $this->companyRepo->getCompanyAdminInfo($this->companyId);

            $data = [
                'email' => $companyAdmin->email,
                'name' => $companyAdmin->name,
                'type' => 'storage'
            ];
            if ($company->subs_id != intval(Config::get('subscription_levels.level0.id'))) {
                $this->mailRepository->sendExceededLimitNotification($data);
            }

            $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 1]);
        }
    }


    /**
     * Increase company download transfer when company user or admin download a file
     * @return mixed
     */
    public function increaseDownloadTransfer($fileSize, $compId=false)
    {
        if ($compId == false) {
            //get files size and file id parameters
            $fileId = Input::get('fileId');
            $fileType = Input::get('fileType');

            $filesRepo = new ProjectFilesRepository();
            $addressBookFileRepo = new FilesRepository();


            if ($fileType == 'address-book') {
                $file = $addressBookFileRepo->getAddressBookFile($fileId);
                $companyId = $file->comp_id;
            } else {
                $file = $filesRepo->getFileById($fileId);
                $companyId = $file->comp_id;
            }
        } else {
            $companyId = $compId;
        }


        $company = $this->companyRepo->getCompany($companyId);

        //project creator increased transfer
        if ($company->limit_month == null || ($company->limit_month == Carbon::now()->month && $company->limit_year == Carbon::now()->year)) {
            if ($company->limit_month == null) {
                $this->companyRepo->updateLimitMonthAndYear($companyId);
            }
            $increasedTransfer = (float)$company->downloaded + (float)$fileSize;
        } else {
            $this->companyRepo->addCompanyUsageHistory($company, 'upload', $company->uploaded);
            $this->companyRepo->addCompanyUsageHistory($company, 'download', $company->downloaded);
            $increasedTransfer = (float)$fileSize;
            $this->companyRepo->updateLimitMonthAndYear($companyId, true);
            $this->companyRepo->updateCompanyUploadTransfer($companyId, 0);
        }

        $this->companyRepo->updateCompanyDownloadTransfer($companyId, $increasedTransfer);

        $limit = $company->subscription_type->download_limit;
        if ($increasedTransfer > ($limit*0.9) && $company->download_notification == 0) {

            $companyAdmin = $this->companyRepo->getCompanyAdminInfo($company->id);

            $data = [
                'email' => $companyAdmin->email,
                'name' => $companyAdmin->name,
                'type' => 'download'
            ];

            if ($company->subs_id != intval(Config::get('subscription_levels.level0.id'))) {
                $this->mailRepository->sendExceededLimitNotification($data);
            }

            $this->companyRepo->updateCompanyMassAssign($company->id,['download_notification' => 1]);
        }
    }

}