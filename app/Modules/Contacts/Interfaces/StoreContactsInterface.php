<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:42 PM
 */

namespace App\Modules\Contacts\Interfaces;


interface StoreContactsInterface {

    public function storeData($companyId, $data);

}