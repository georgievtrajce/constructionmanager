<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:43 PM
 */

namespace App\Modules\Contacts\Interfaces;


interface EditContactsInterface {

    public function editContact($companyId, $contactId, $data);

}