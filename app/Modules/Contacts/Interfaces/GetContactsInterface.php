<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:13 PM
 */
namespace App\Modules\Contacts\Interfaces;

interface GetContactsInterface {

    public function getContacts($companyId);
    public function getEditContact($companyId, $contactId);

}