<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:21 PM
 */

namespace App\Modules\Contacts\Repositories;

use App\Models\Address;
use App\Models\Contact;
use App\Models\Project_company;

class ContactsRepository {

    /**
     * Get all contacts
     * @param $companyId
     * @return mixed
     */
    public function getAllContacts($companyId)
    {
        return Contact::where('ab_id','=',$companyId)
            ->with('office')
            ->orderBy('name','ASC')
            ->get();
    }

    /**
     * Get specified contact
     * @param $companyId
     * @param $contactId
     * @return mixed
     */
    public function getContact($companyId, $contactId)
    {
        return Contact::where('ab_id','=',$companyId)->where('id','=',$contactId)->firstOrFail();
    }

    /**
     * Get contact by email, id and address book owner
     * @param $email
     * @param $contactId
     * @param $parentCompanyId
     * @return mixed
     */
    public function getContactByEmail($email, $contactId, $parentCompanyId)
    {
        return Contact::join('address_book','address_book.id','=','ab_contacts.ab_id')
            ->where('ab_contacts.email','=',$email)
            ->where('ab_contacts.id','=',$contactId)
            ->where('address_book.owner_comp_id','=',$parentCompanyId)
            ->select(
                'ab_contacts.*',
                'address_book.id as ab_id',
                'address_book.owner_comp_id as ab_owner_comp_id'
            )
            ->first();
    }

    /**
     * Return address book contact emails
     * @param $addressBookId
     * @return mixed
     */
    public function getAbContactEmails($addressBookId)
    {
        return Contact::where('ab_id','=',$addressBookId)->lists('email');
    }

    /**
     * Get all addresses for specified address book company
     * @param $companyId
     * @return mixed
     */
    public function getAddressBookAddresses($companyId)
    {
        return Address::where('ab_id','=',$companyId)->get();
    }

    /**
     * Get specified address book company office contacts
     * @param $officeId
     * @return mixed
     */
    public function getOfficeContacts($officeId)
    {
        return Contact::where('address_id','=',$officeId)->get();
    }

    /**
     * Get project Subcontractor contacts (from the companies module) for the specified module entry
     * @param $projectId
     * @param $abSubcontractorId
     * @return mixed
     */
    public function getProjectSubcontractorContacts($projectId, $abSubcontractorId)
    {
        return Project_company::where('proj_id','=',$projectId)
            ->where('project_companies.ab_id','=',$abSubcontractorId)
            ->join('ab_contacts', function($join) use ($abSubcontractorId)
            {
                $join->on('ab_contacts.address_id', '=', 'project_companies.addr_id')
                    ->where('ab_contacts.ab_id','=',$abSubcontractorId);
            })
            ->select(
                'ab_contacts.*'
            )
            ->orderBy('ab_contacts.name')
            ->get();
    }

    /**
     * Store contact
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeContact($companyId, $data)
    {
        return Contact::create([
            'name' => $data['cc_name'],
            'title' => $data['cc_title'],
            'email' => $data['cc_email'],
            'address_id' => $data['cc_address_office_name'],
            'office_phone' => $data['cc_office_phone'],
            'cell_phone' => $data['cc_cell_phone'],
            'fax' => $data['cc_fax'],
            'ab_id' => $companyId
        ]);
    }

    /**
     * Update specified contact
     * @param $companyId
     * @param $contactId
     * @param $data
     * @return mixed
     */
    public function updateContact($companyId, $contactId, $data)
    {
        return Contact::where('ab_id','=',$companyId)->where('id','=',$contactId)->update([
            'name' => $data['cc_name'],
            'title' => $data['cc_title'],
            'email' => $data['cc_email'],
            'address_id' => $data['cc_address_office_name'],
            'office_phone' => $data['cc_office_phone'],
            'cell_phone' => $data['cc_cell_phone'],
            'fax' => $data['cc_fax']
        ]);
    }

    /**
     * Delete specified contact
     * @param $contactId
     * @return int
     */
    public function deleteContact($contactId)
    {
        return Contact::destroy($contactId);
    }

}