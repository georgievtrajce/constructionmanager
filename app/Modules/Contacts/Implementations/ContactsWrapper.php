<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/5/2015
 * Time: 10:12 AM
 */

namespace App\Modules\Contacts\Implementations;


use App\Modules\Contacts\Interfaces\EditContactsInterface;
use App\Modules\Contacts\Interfaces\GetContactsInterface;
use App\Modules\Contacts\Interfaces\StoreContactsInterface;

class ContactsWrapper {

    /**
     * Wrap get all contacts implementation
     * @param GetContactsInterface $getContactsInterface
     * @param $companyId
     * @return mixed
     */
    public function implementGetContacts(GetContactsInterface $getContactsInterface, $companyId)
    {
        return $getContactsInterface->getContacts($companyId);
    }

    /**
     * Wrap get contact for editing implementation
     * @param GetContactsInterface $getContactsInterface
     * @param $companyId
     * @param $contactId
     * @return mixed
     */
    public function implementGetEditContact(GetContactsInterface $getContactsInterface, $companyId, $contactId)
    {
        return $getContactsInterface->getEditContact($companyId, $contactId);
    }

    /**
     * Wrap update contact functionality implementation
     * @param EditContactsInterface $editContactsInterface
     * @param $companyId
     * @param $contactId
     * @param $data
     * @return mixed
     */
    public function implementUpdateContact(EditContactsInterface $editContactsInterface, $companyId, $contactId, $data)
    {
        return $editContactsInterface->editContact($companyId, $contactId, $data);
    }

    /**
     * Wrap store contact functionality implementation
     * @param StoreContactsInterface $storeContactsInterface
     * @param $companyId
     * @param $data
     * @return mixed
     */
    public function implementStoreContacts(StoreContactsInterface $storeContactsInterface, $companyId, $data)
    {
        return $storeContactsInterface->storeData($companyId, $data);
    }

}