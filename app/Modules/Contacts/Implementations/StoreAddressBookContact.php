<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:41 PM
 */

namespace App\Modules\Contacts\Implementations;


use App\Modules\Contacts\Interfaces\StoreContactsInterface;
use App\Modules\Contacts\Repositories\ContactsRepository;

class StoreAddressBookContact implements StoreContactsInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new ContactsRepository();
    }

    /**
     * Store address book contact method
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeData($companyId, $data)
    {
        return $this->repo->storeContact($companyId, $data);
    }

}