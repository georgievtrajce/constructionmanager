<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/23/2015
 * Time: 2:34 PM
 */

namespace App\Modules\Contacts\Implementations;


class ContactsUsersClassMap {

    /**
     * Get page redirect dynamically
     * @param $contactType
     * @param $companyId
     * @return string
     */
    public function getIndexPageRedirect($contactType, $companyId)
    {
        if($contactType == 'address-book')
            return '/address-book/'.$companyId.'/contacts';
        if($contactType == 'company')
            return '/user-profile';
    }

}