<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:10 PM
 */

namespace App\Modules\Contacts\Implementations;


use App\Modules\Contacts\Interfaces\GetContactsInterface;
use App\Modules\Contacts\Repositories\ContactsRepository;

class GetAddressBookContacts implements GetContactsInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new ContactsRepository();
    }

    /**
     * Get all address book contacts for listing
     * @param $companyId
     * @return mixed
     */
    public function getContacts($companyId)
    {
        return $this->repo->getAllContacts($companyId);
    }

    /**
     * Get specified address book contact for editing
     * @param $companyId
     * @param $contactId
     * @return mixed
     */
    public function getEditContact($companyId, $contactId)
    {
        return $this->repo->getContact($companyId, $contactId);
    }

}