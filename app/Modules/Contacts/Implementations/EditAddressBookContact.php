<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/31/2015
 * Time: 5:11 PM
 */

namespace App\Modules\Contacts\Implementations;


use App\Modules\Contacts\Interfaces\EditContactsInterface;
use App\Modules\Contacts\Repositories\ContactsRepository;

class EditAddressBookContact implements EditContactsInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new ContactsRepository();
    }

    /**
     * Update specified address book contact method
     * @param $companyId
     * @param $contactId
     * @param $data
     * @return mixed
     */
    public function editContact($companyId, $contactId, $data)
    {
        return $this->repo->updateContact($companyId, $contactId, $data);
    }

}