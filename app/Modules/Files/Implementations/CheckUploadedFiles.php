<?php namespace App\Modules\Files\Implementations;


use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use Illuminate\Support\Facades\Config;

class CheckUploadedFiles implements CheckUploadedFilesInterface
{

//        1.Subcontractor-in:  $file->version_date_connection == 'rec_sub_file' (Subcontractor-in file)
//        2.Approval-out:  	 $file->version_date_connection == 'sent_appr_file' (Subcontractor-in file or Approval-out file  + Approval-out transmittal)
//        3.Approval-in:  	 $file->version_date_connection == 'rec_appr_file' (Approval-in file)
//        4.Subcontractor-out: $file->version_date_connection == 'subm_sent_sub_file' (Approval-in or Subcontractor-out file + Subcontractor-out transmittal)
    private function configs()
    {
        return [
            'files' => [
                Config::get('constants.submittal_version_files.rec_sub_file') => 1, //Subcontractor-in
                Config::get('constants.submittal_version_files.sent_appr_file') => 2, //Approval-out
                Config::get('constants.submittal_version_files.rec_appr_file') => 3, //Approval-in
                Config::get('constants.submittal_version_files.subm_sent_sub_file') => 4, //Subcontractor-out
            ],
            'dates' => [
                Config::get('constants.submittal_date_type.rec_sub') => 1, //Subcontractor-in
                Config::get('constants.submittal_date_type.sent_appr') => 2, //Approval-out
                Config::get('constants.submittal_date_type.rec_appr') => 3, //Approval-in
                Config::get('constants.submittal_date_type.subm_sent_sub') => 4, //Subcontractor-out
            ]
        ];
    }

//          1.Approval-out:  	 $file->version_date_connection == 'sent_appr_file' (Approval-out file + Approval-out transmittal)
//          2.Approval-in:  	 $file->version_date_connection == 'rec_appr_file'  (Approval-in file)
    private function pcoRecipientConfigs()
    {
        return [
            'files' => [
                Config::get('constants.submittal_version_files.sent_appr_file') => 1, //Approval-out
                Config::get('constants.submittal_version_files.rec_appr_file') => 2, //Approval-in
            ],
            'dates' => [
                Config::get('constants.submittal_date_type.sent_appr') => 1, //Approval-out
                Config::get('constants.submittal_date_type.rec_appr') => 2, //Approval-in
            ]
        ];
    }

//          1.Subcontractor-in:  $file->version_date_connection == 'rec_sub_file' (Subcontractor-in file)
//          2.Subcontractor-out: $file->version_date_connection == 'subm_sent_sub_file' (Subcontractor-out file + Subcontractor-out transmittal)
    private function pcoSubcontractorConfigs()
    {
        return [
            'files' => [
                Config::get('constants.submittal_version_files.rec_sub_file') => 1, //Subcontractor-in
                Config::get('constants.submittal_version_files.subm_sent_sub_file') => 2, //Subcontractor-out
            ],
            'dates' => [
                Config::get('constants.submittal_date_type.rec_sub') => 1, //Subcontractor-in
                Config::get('constants.submittal_date_type.subm_sent_sub') => 2, //Subcontractor-out
            ]
        ];
    }

    public function checkAndOrderUploadedFiles($data, $versionType)
    {
        $versionStatusFilesArray = [];

        switch ($versionType){
            case Config::get('constants.submittals_versions'):
            case Config::get('constants.rfis_versions'):
                $versionStatusFilesArray = $this->configs();
                break;
            case Config::get('constants.recipient_versions'):
                $versionStatusFilesArray = $this->pcoRecipientConfigs();
                break;
            case Config::get('constants.subcontractor_versions'):
                $versionStatusFilesArray = $this->pcoSubcontractorConfigs();
                break;
        }

        $versionDateTypes = array_keys($versionStatusFilesArray['dates']);

        if(isset($data[$versionType]) && count($data[$versionType]) > 0) {
            foreach ($data[$versionType] as $key => $versionData)
            {
                $versionCurrentStatusArray = [];
                $versionDateTypesArray = [];
                foreach ($versionDateTypes as $dateType) {
                    if($versionData[$dateType] != Config::get('constants.empty_date') ) {
                        $versionDateTypesArray[] = $versionStatusFilesArray['dates'][$dateType];
                    }
                }
                $versionData['transmittal_file'] = false;
                $versionData['download_file'] = false;
                $fileFromPreviousRevision = false;
                if (isset($versionData['file']) && count($versionData['file']) > 0) {
                    foreach ($versionData['file'] as $file) {
                        $versionCurrentStatusArray[$file['version_date_connection']] = $versionStatusFilesArray['files'][$file['version_date_connection']];
                    }
                    $fileFromPreviousRevision = false;
                } else {
                    if ($key > 0 && isset($data[$versionType][$key - 1]['file']) && count($data[$versionType][$key - 1]['file']) > 0)
                    {
                        foreach ($data[$versionType][$key - 1]['file'] as $prevCycleFile) {
                            $versionCurrentStatusArray[$prevCycleFile['version_date_connection']] = $versionStatusFilesArray['files'][$prevCycleFile['version_date_connection']];
                        }
                        $fileFromPreviousRevision = true;
                    }
                }

                (!empty($versionDateTypesArray)) ? $maxDate = max($versionDateTypesArray) : $maxDate = 0;


                switch ($versionType)
                {
                    case Config::get('constants.submittals_versions'):
                    case Config::get('constants.rfis_versions'):
                        switch ($maxDate)
                        {
                            //Subcontractor-In & Approval-In
                            case 1:
                            case 3:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = true;
                                if($maxDate == 1) {
                                    //Subcontractor-In file
                                    if( isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_sub_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_sub_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.rec_sub_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_sub_file');
                                    }
                                } else {
                                    //Approval-In file
                                    if( isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_appr_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_appr_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.rec_appr_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_appr_file');
                                    }
                                }
                                if ($fileFromPreviousRevision) {
                                    $versionData['transmittal_file'] = false;
                                    $versionData['download_file'] = true;
                                    $versionData->setRelation('file', $data[$versionType][$key - 1]['file']);
                                }
                                break;
                            //Approval-Out & Subcontractor-Out
                            case 2:
                            case 4:
                                $versionData['transmittal_file'] = true;
                                $versionData['download_file'] = true;

                                //Approval-out
                                if($maxDate == 2) {
                                    //Approval-out file
                                    if( isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.sent_appr_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.sent_appr_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.sent_appr_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.sent_appr_file');
                                    } //or Subcontractor-in file
                                    elseif(isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_sub_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_sub_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.rec_sub_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_sub_file');
                                    }
                                    //Approval-out transmittal
                                    $versionData['transmittal_status'] = Config::get('constants.transmittal_types.transmittalSentFile');
                                }
                                //Subcontractor-Out
                                else {
                                    //Subcontractor-out file
                                    if( isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.subm_sent_sub_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.subm_sent_sub_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.subm_sent_sub_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.subm_sent_sub_file');
                                    } //or Approval-in file
                                    elseif(isset($versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_appr_file')]) &&
                                        $versionCurrentStatusArray[Config::get('constants.submittal_version_files.rec_appr_file')] ==
                                        $versionStatusFilesArray['files'][Config::get('constants.submittal_version_files.rec_appr_file')] ) {
                                            $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_appr_file');
                                    }
                                    //Subcontractor-out transmittal
                                    $versionData['transmittal_status'] = Config::get('constants.transmittal_types.transmittalSubmSentFile');
                                }
                                break;
                            default:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = false;
                                break;
                        }
                        break;
                    case Config::get('constants.recipient_versions'):
                        switch ($maxDate)
                        {
                            //Approval-out
                            case 1:
                                $versionData['transmittal_file'] = true;
                                $versionData['download_file'] = true;
                                $versionData['file_status'] = Config::get('constants.submittal_version_files.sent_appr_file');
                                break;
                            //Approval-in
                            case 2:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = true;
                                $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_appr_file');
                                break;
                            default:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = false;
                                break;
                        }
                        break;
                    case Config::get('constants.subcontractor_versions'):
                        switch ($maxDate)
                        {
                            //Subcontractor-In
                            case 1:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = true;
                                $versionData['file_status'] = Config::get('constants.submittal_version_files.rec_sub_file');
                                break;
                            //Subcontractor-Out
                            case 2:
                                $versionData['transmittal_file'] = true;
                                $versionData['download_file'] = true;
                                $versionData['file_status'] = Config::get('constants.submittal_version_files.subm_sent_sub_file');
                                break;
                            default:
                                $versionData['transmittal_file'] = false;
                                $versionData['download_file'] = false;
                                break;
                        }
                        break;
                }
            }
        }
        return $data;
    }

    public function checkAndOrderUploadedFilesForVersion($files, $versionType)
    {
        $data = [];
        $versionFiles = [];
        if (count($files) > 0)
        {
            switch ($versionType)
            {
                case Config::get('constants.submittals_versions'):
                case Config::get('constants.rfis_versions'):
                    foreach ($files as $file) {
                        $versionFiles[] = $file['version_date_connection'];
                    }
                    break;
                case Config::get('constants.subcontractor_versions'):
                case Config::get('constants.recipient_versions'):
                    foreach ($files as $file) {
                        $versionFiles[] = $file['file']['version_date_connection'];
                    }
            }

            switch ($versionType)
            {
                case Config::get('constants.submittals_versions'):
                case Config::get('constants.rfis_versions'):
                    //if Approval-Out file is empty, check if Subcontractor-In file exists
                    if (in_array(Config::get('constants.submittal_version_files.rec_sub_file'), $versionFiles) && !in_array(Config::get('constants.submittal_version_files.sent_appr_file'), $versionFiles)) {
                        $data['sentAppr3Path'] = Config::get('constants.submittal_version_files.rec_sub_file');
                    } else if (in_array(Config::get('constants.submittal_version_files.sent_appr_file'), $versionFiles)) {
                        $data['sentAppr3Path'] = Config::get('constants.submittal_version_files.sent_appr_file');
                    }
                    //if Subcontractor-Out file is empty, check if Approval-In file exists
                    if (in_array(Config::get('constants.submittal_version_files.rec_appr_file'), $versionFiles) && !in_array(Config::get('constants.submittal_version_files.subm_sent_sub_file'), $versionFiles)) {
                        $data['submSent3Path'] = Config::get('constants.submittal_version_files.rec_appr_file');
                    } else if (in_array(Config::get('constants.submittal_version_files.subm_sent_sub_file'), $versionFiles)) {
                        $data['submSent3Path'] = Config::get('constants.submittal_version_files.subm_sent_sub_file');
                    }
                    break;
                case Config::get('constants.subcontractor_versions'):
                    //if Subcontractor-Out file is empty, check if Subcontractor-In file exists
                    if (in_array(Config::get('constants.submittal_version_files.rec_sub_file'), $versionFiles) && !in_array(Config::get('constants.submittal_version_files.subm_sent_sub_file'), $versionFiles)) {
                        $data['sentAppr3Path'] = Config::get('constants.submittal_version_files.rec_sub_file');
                    } else if (in_array(Config::get('constants.submittal_version_files.subm_sent_sub_file'), $versionFiles)) {
                        $data['sentAppr3Path'] = Config::get('constants.submittal_version_files.subm_sent_sub_file');
                    }
                    break;
                case Config::get('constants.recipient_versions'):
                    //if Approval-In file is empty, check if Approval-Out file exists
                    if (in_array(Config::get('constants.submittal_version_files.sent_appr_file'), $versionFiles) && !in_array(Config::get('constants.submittal_version_files.rec_appr_file'), $versionFiles)) {
                        $data['submSent3Path'] = Config::get('constants.submittal_version_files.sent_appr_file');
                    } else if (in_array(Config::get('constants.submittal_version_files.rec_appr_file'), $versionFiles)) {
                        $data['submSent3Path'] = Config::get('constants.submittal_version_files.rec_appr_file');
                    }
                    break;
            }
        }
        return $data;
    }

}