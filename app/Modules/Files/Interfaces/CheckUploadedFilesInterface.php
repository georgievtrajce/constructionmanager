<?php namespace App\Modules\Files\Interfaces;

interface CheckUploadedFilesInterface {
    public function checkAndOrderUploadedFiles($data, $versionType);
    public function checkAndOrderUploadedFilesForVersion($files, $versionType);
}