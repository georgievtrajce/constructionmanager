<?php namespace App\Modules\Files\Interfaces;

interface FileInterface {
    public function nameFile($type);
    public function uploadFile($type,$file,$storage);
    public function getFiles($type,$storage);

}