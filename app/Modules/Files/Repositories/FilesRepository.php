<?php
namespace App\Modules\Files\Repositories;

use App\Models\Ab_distribution;
use App\Models\Ab_file;
use App\Models\File as FileModel;
use App\Models\File_type;
use App\Models\Project;
use App\Models\Project_file;
use App\Models\Project_permission;
use App\Models\TasksFile;
use App\Models\User_permission;
use App\Models\Download_log;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Utilities\FileStorage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response as Response;

class FilesRepository {

    private $companyId;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        if (Auth::user() != null) {
            $this->companyId = Auth::user()->comp_id;
        }
        $this->filesRepo = $this;
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * upload file to s3
     * @param $path - path for uploading
     * @param $file - file to upload
     * @param $fileName - prepared name for upload
     * @return bool - name of stored file/false if not uploaded
     */
    public function uploadFile($path, $file, $fileName)
    {
        /**
         * upload file locally
         */
        $destinationPath = storage_path().'/app';
        $file->move($destinationPath, $fileName);

        /**
         * s3 upload
         */
//        $disk = Storage::disk('s3');
//
//        $contents = Storage::disk('local')->get($fileName);
//
//        $s3Upload = $disk->put($path.'/'.$fileName, $contents);

        $client = App::make('aws')->get('s3');
        $x = array(
            'Bucket' => Config::get('buckets.bucket'),
            'Key'    => $path.$fileName,
            'SourceFile'   => $destinationPath.'/'.$fileName
        );

        $s3Upload = $client->putObject($x);

        if($s3Upload) {
            //if s3 upload is successful delete local file
            Storage::disk('local')->delete($fileName);
            return $fileName;
        }
        else return false;
    }


    /**
     * get files on path
     * @param $path
     * @return mixed
     */
    public function getFiles($path)
    {
        $disk = Storage::disk('s3');

        $files = $disk->files($path);
        for($i=0;$i<sizeof($files);$i++){
            $files[$i] = basename($files[$i]);
        }
        return $files;

    }

    /**
     * get file by name on path - download
     * @param $path
     * @param $name
     * @return mixed
     */
    public function getFile($path, $name)
    {
        $filePath = $path.'/'.$name;

        $file = Storage::disk('s3')->get($filePath);

        return Response::make($file, 200)->header('Content-type','application/*','Content-Disposition', 'attachment');
    }

    /**
     * store file record in file model in db
     * @param $data
     * @return mixed
     */
    public function storeFileRecord($data)
    {
        $file = FileModel::where('id','=',$data['file_id'])->firstOrFail();
        $file->name = $data['name'];
        $file->number = $data['number'];
        $file->save();

        return $file->id;
    }


    /**
     * delete file if exists on path
     * @param $path
     * @param $name
     * @return bool
     */
    public function deleteFile($path, $name){

        $filePath = $path.'/'.$name;
        $disk = Storage::disk('s3');
        if ($disk->exists($filePath)) {
            return $disk->delete($filePath);
        }
        return true;
    }

    /**
     * update file by deleting old and uploading new
     * returns file name if success, or false if fails
     * @param $file
     * @param $path
     * @param $fileId
     * @param $name
     * @return bool
     */
    public function updateFile($file, $path, $fileId, $name)
    {
        //get current file path
        $fileData = FileModel::where('id', '=', $fileId)->firstOrFail();

        $filePath = $path.'/'.$fileData->file_name;

        //if exists, delete
        $disk = Storage::disk('s3');

        if ($disk->exists($filePath))
        {
            $disk->delete($filePath);
        }

        return $this->uploadFile($path, $file, $name);
    }

    /**
     * Get last uploaded files for a specific project
     * @param $projectID
     * @return mixed
     */
    public function getLastUploadedFiles($projectID)
    {
        $files = Project_file::join('file_types','file_types.id','=','files.ft_id')
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->where('proj_id', '=', $projectID);

        //if user is not admin then get last files only that he is allowed to see
        if (!Auth::user()->hasRole('Company Admin')) {
            $types = User_permission::where('user_id', '=', Auth::user()->id)
                ->where('read', '=', Config::get('constants.hasPermission'))
                ->where('entity_type', '=', Config::get('constants.entity_type.file'))
                ->select('user_permissions.entity_id')
                ->get()
                ->toArray();

            $typesValues = array();

            foreach($types as $type) {
                array_push($typesValues, $type['entity_id']);
            }
            //bind where clauses for specific file types and regular blog posts
            $files = $files->whereIn('ft_id', $types)->orWhereNull('ft_id');
        }

        $files = $files->select('files.*', 'file_types.display_name as display_name')
                        ->orderBy('created_at','DESC')
                        ->take(Config::get('constants.recent_files.files'))->get();
        return $files;
    }

    /**
     * Get last uploaded files for a specific project
     * @param $number
     * @param $projectID
     * @return mixed
     */
    public function getLastUploadedFilesByProject($number, $projectID)
    {
        $files = Project_file::where('proj_id','=',$projectID)
            ->join('file_types', 'file_types.id', '=', 'files.ft_id');

        //if user is not admin then get last files only that he is allowed to see
        if (!Auth::user()->hasRole('Company Admin')) {
            if (Auth::user()->hasRole('Project Admin')) {
                $files->join('projects', function($join) {
                    $join->on('projects.id', '=', 'files.proj_id');
                    $join->where('projects.proj_admin', '=', Auth::user()->id);
                })
                    ->where('files.comp_id', '=', Auth::user()->comp_id)
                    ->where('files.transmittal', '=', Config('constants.transmittal_flags.regular'));
            } else {
                $types = Project_permission::where('user_id', '=', Auth::user()->id)
                    ->where('read', '=', Config::get('constants.hasPermission'))
                    ->lists('entity_id');

                //bind where clauses for specific file types and regular blog posts
                $files = $files->where('files.comp_id', '=', Auth::user()->comp_id)
                    ->where('files.transmittal', '=', Config('constants.transmittal_flags.regular'))
                    ->whereIn('files.ft_id', $types)->orWhereNull('ft_id');
            }
        } else {
            $files =  $files->where('files.comp_id', '=', Auth::user()->comp_id)
                ->where('files.transmittal', '=', Config('constants.transmittal_flags.regular'));

        }

        return $files->select('files.*', 'file_types.display_name', 'file_types.name as ft_name')
            ->with('project')
            ->with('contract_file')
            ->orderBy('files.created_at', 'desc')
            ->take($number)
            ->get();
    }

    /**
     * Returns used storage by project id and
     *
     * @param $projectID
     * @return mixed
     */
    public function getUsedStorageByProject($projectID)
    {
        /*$addressBookFileSize = Project_file::where('proj_id', '=', $projectID)
                                        ->where('comp_id', '=', Auth::user()->comp_id)
                                        ->sum('size');*/

        $projectFileSize = Project_file::where('proj_id', '=', $projectID)
                            ->where('comp_id', '=', Auth::user()->comp_id)
                            ->sum('size');

        return ($projectFileSize);
    }

    /**
     * Returns used storage by project id and
     *
     * @param $projectID
     * @return mixed
     */
    public function getUsedStorageBySmallFilesProjectAndCompany($projectID, $companyId)
    {
        /*$addressBookFileSize = Project_file::where('proj_id', '=', $projectID)
                                        ->where('comp_id', '=', Auth::user()->comp_id)
                                        ->sum('size');*/

        $projectFileSize = Project_file::where('proj_id', '=', $projectID)
            ->whereRaw('CAST(size AS UNSIGNED) < 250000000')
            ->where('comp_id', '=', $companyId)
            ->sum('size');

        return ($projectFileSize);
    }

    /**
     * Returns used storage by company
     *
     * @return mixed
     */
    public function getUsedStorageForProjectFiles()
    {
        return Project_file::where('comp_id', '=', Auth::user()->comp_id)->sum('size');
    }

    /**
     * Returns used storage by company
     *
     * @return mixed
     */
    public function getUsedStorageForAddressBookFiles()
    {
        return Ab_file::where('comp_id', '=', Auth::user()->comp_id)->sum('size');
    }

    /**
     * Returns used storage by company
     *
     * @return mixed
     */
    public function getUsedStorageForTasksFiles()
    {
        return TasksFile::where('comp_id', '=', Auth::user()->comp_id)->sum('size');
    }

    /**
     * Get latest uploaded files for a specific company
     * @param $number
     * @return mixed
     */
    public function getLastUploadedFilesByCompany($number)
    {
        $files = Project_file::where('files.comp_id','=',Auth::user()->comp_id)
            ->where('is_finished', '=', 1)
            ->join('file_types','file_types.id','=','files.ft_id');

        //check if the logged in user has Company User role
        if (Auth::user()->hasRole('Company User')) {
            $files->join('projects as pcu','pcu.id','=','files.proj_id')
                ->join('project_permissions as ppcu', function($join) {
                    $join->on('ppcu.proj_id', '=', 'pcu.id');
                    $join->on('ppcu.file_type_id', '=', 'file_types.id');
                    $join->where('ppcu.user_id', '=', Auth::user()->id);
                    $join->where('ppcu.read', '=', Config::get('constants.hasPermission'));
                });
        }

        //check if the logged in user has Project Admin role
        if (Auth::user()->hasRole('Project Admin')) {
            $files->join('projects as ppa','ppa.id','=','files.proj_id')
                ->join('project_permissions as pppa', function($join) {
                    $join->on('pppa.proj_id', '=', 'ppa.id');
                    $join->on('pppa.file_type_id', '=', 'file_types.id');
                    $join->where('pppa.user_id', '=', Auth::user()->id);
                })
                ->where(function($query) {
                    $query->where('ppa.proj_admin', '=', Auth::user()->id)
                        ->orWhere('pppa.read', '=', Config::get('constants.hasPermission'));
                });
        }

        //return queried results
        return $files->where('files.transmittal', '=', Config('constants.transmittal_flags.regular'))
            ->select(
                'files.*',
                'file_types.display_name',
                'file_types.name as ft_name'
            )
            ->with('project')
            ->with('contract_file')
            ->orderBy('files.created_at', 'desc')
            ->take($number)
            ->get();
    }

    /**
     * Get all uploaded files for a specific company
     * @param $projectId
     * @return mixed
     */
    public function getAllUploadedFiles($projectId)
    {
        $files = Project_file::where('files.comp_id','=',Auth::user()->comp_id)
            ->join('file_types','file_types.id','=','files.ft_id');

        //check if the logged in user has Company User role
        if (Auth::user()->hasRole('Company User')) {
            $files->join('projects','projects.id','=','files.proj_id')
                ->join('project_permissions', function($join) {
                    $join->on('project_permissions.proj_id', '=', 'projects.id');
                    $join->on('project_permissions.file_type_id', '=', 'file_types.id');
                    $join->where('project_permissions.user_id', '=', Auth::user()->id);
                    $join->where('project_permissions.read', '=', Config::get('constants.hasPermission'));
                })
                ->whereNull('projects.deleted_at');
        }

        //check if the logged in user has Project Admin role
        if (Auth::user()->hasRole('Project Admin')) {
            $files->join('projects','projects.id','=','files.proj_id')
                ->join('project_permissions', function($join) {
                    $join->on('project_permissions.proj_id', '=', 'projects.id');
                    $join->on('project_permissions.file_type_id', '=', 'file_types.id');
                    $join->where('project_permissions.user_id', '=', Auth::user()->id);
                })
                ->whereNull('projects.deleted_at')
                ->where(function($query) {
                    $query->where('projects.proj_admin', '=', Auth::user()->id)
                        ->orWhere('project_permissions.read', '=', Config::get('constants.hasPermission'));
                });
        }

        if (!Auth::user()->hasRole('Company User') && !Auth::user()->hasRole('Project Admin')) {
            $files = $files->join('projects','projects.id','=','files.proj_id')
                ->whereNull('projects.deleted_at');
        }

        if (!empty($projectId)) {
            $files = $files->where('files.proj_id', '=', $projectId);
        }

        //return queried results
        return $files->where('files.transmittal', '=', Config('constants.transmittal_flags.regular'))
            ->select(
                'files.*',
                'file_types.display_name',
                'file_types.name as ft_name'
            )
            ->with('project')
            ->orderBy('files.created_at', 'desc')
            ->paginate(Config::get('constants.pagination.all_files'));
    }

    public function deleteDirectory($directory)
    {
        $disk = Storage::disk('s3');
        return ($disk->deleteDirectory($directory));
    }
    public function logDownload($fileId)
    {
        $file = ['file_id' => $fileId, 'user_id' => Auth::user()->id, 'comp_id' => Auth::user()->comp_id];
        return (Download_log::create($file));
    }

    public function logDownloadByName($fileName)
    {
        $file = FileModel::where('file_name', '=', $fileName)->firstOrFail();
        return (Download_log::create(['file_id' => $file->id, 'user_id' => Auth::user()->id, 'comp_id' => Auth::user()->comp_id]));
    }

    public function getAllAddressBookFiles($ab_id, $itemsPerPage = false)
    {
        $addressBook = Ab_file::where('ab_id', '=', $ab_id)->orderBy('created_at', 'desc');

        if ($itemsPerPage) {
            return $addressBook->paginate($itemsPerPage);
        }

        return $addressBook->get();
    }

    public function getAllAddressBookPdfFiles($ab_id, $itemsPerPage = false)
    {
        $addressBook = Ab_file::where('ab_id', '=', $ab_id)->where('file_type', '=', 'pdf')->orderBy('created_at', 'desc');

        if ($itemsPerPage) {
            return $addressBook->paginate($itemsPerPage);
        }

        return $addressBook->get();
    }

    /**
     * Deletes file from address book files
     *
     * @param $params
     * @return bool
     */
    public function deleteSingleAddressBookFile($params)
    {
        //get files from contract_files and files tables
        $file = Ab_file::where('id', '=', $params['fileId'])
            ->where('comp_id','=',Auth::user()->comp_id)
            ->first();

        //delete if exists
        if (!is_null($file)) {
            $fileDelete = $file->delete();
        }

        //if file exists in table and is successfully deleted, return true
        if (isset($fileDelete) && !empty($fileDelete)) {

            //if storage limit bellow 90% reset notification
            /*$company = $this->companyRepo->getCompany(Auth::user()->comp_id);
            $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
            $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
            $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage;

            if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
                $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
            }*/

            return true;
        }
        return false;
    }

    /**
     * Deletes file from address book files
     *
     * @param $params
     * @return bool
     */
    public function deleteSingleDailyReportFile($params)
    {
        //get files from contract_files and files tables
        $file = Ab_file::where('id', '=', $params['fileId'])
            ->where('comp_id','=',Auth::user()->comp_id)
            ->first();

        //delete if exists
        if (!is_null($file)) {
            $fileDelete = $file->delete();
        }

        //if file exists in table and is successfully deleted, return true
        if (isset($fileDelete) && !empty($fileDelete)) {

            //if storage limit bellow 90% reset notification
            /*$company = $this->companyRepo->getCompany(Auth::user()->comp_id);
            $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
            $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
            $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage;

            if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
                $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
            }*/

            return true;
        }
        return false;
    }

    /**
     * Updates address book file by id
     *
     * @param $abId
     * @param $data
     * @return mixed
     */
    public function updateAddressBookFile($abId, $data, $users, $contacts)
    {
        $file = Ab_file::where('id', '=', $abId)
            ->where('comp_id','=',Auth::user()->comp_id)->first();

        Ab_distribution::where('ab_file_id', '=', $file->id)
                       ->where('ab_id', '=', $file->ab_id)
                       ->delete();

        if (!empty($users)) {
            foreach ($users as $userId) {
                Ab_distribution::create([
                    'ab_id' => $file->ab_id,
                    'user_id' => $userId,
                    'ab_file_id' => $file->id
                ]);
            }
        }

        //insert distribution contacts for appr
        if (!empty($contacts)) {
            foreach ($contacts as $contactId) {
                Ab_distribution::create([
                    'ab_id' => $file->ab_id,
                    'ab_cont_id' => $contactId,
                    'ab_file_id' => $file->id
                ]);
            }
        }

        return $file->update($data);
    }

    /**
     * Returns address book file by ID
     *
     * @param $fileId
     * @return mixed
     */
    public function getAddressBookFile($fileId, $compId=null)
    {
        if (empty($compId) && Auth::user() != null) {
            $compId = Auth::user()->comp_id;
        }
        //get files from contract_files and files tables
        $file = Ab_file::where('id', '=', $fileId);

        if (!empty($compId)) {
            $file = $file->where('comp_id', '=', $compId);
        }

        $file = $file->with('company')
            ->with('addressBookFileUsers.users.company')
            ->with('addressBookFileUsers.abUsers.addressBook')
            ->first();

        return $file;
    }

    /**
     * Removes file from s3
     *
     * @param $addressBookId
     * @param $fileId
     * @return bool
     */
    public function deleteAddressBookFileFromS3($addressBookId, $fileId)
    {
        //get file
        $file = $this->getAddressBookFile($fileId);

        $disk = Storage::disk('s3');
        $filePath = 'company_' . Auth::user()->comp_id . '/contact_'. $addressBookId .'/address_book_documents/' . $file->file_name;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Delete all company address book files
     *
     * @param $companyId
     * @return mixed
     */
    public function deleteAllAddressBookFilesByCompany($companyId)
    {
        return Ab_file::where('comp_id', '=', $companyId)->delete();
    }
}