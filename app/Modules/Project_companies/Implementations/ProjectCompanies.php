<?php
namespace App\Modules\Project_companies\Implementations;

use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use Illuminate\Support\Facades\Auth;

class ProjectCompanies {

    public function generateProjectCompany($data)
    {
        $addressBookRepository = new AddressBookRepository();
        $projectCompaniesRepository = new ProjectCompaniesRepository();
        $projectPermissionsRepository = new ProjectPermissionsRepository();

        $companiesForPermissions = [];
        $recipientId = !empty($data['architect_id'])?$data['architect_id']:(!empty($data['prime_subcontractor_id'])?$data['prime_subcontractor_id']:$data['owner_id']);
        $companiesForPermissions[] = $recipientId;
        $generalContractorId = empty($data['make_me_gc']) ? $data['general_contractor_id'] : 0;
        $companiesForPermissions[] = $generalContractorId;

        if (!empty($data['architect_id'])) {
            $companiesForPermissions[] = $data['architect_id'];
        }

        if (!empty($data['prime_subcontractor_id'])) {
            $companiesForPermissions[] = $data['prime_subcontractor_id'];
        }

        if (!empty($data['owner_id'])) {
            $companiesForPermissions[] = $data['owner_id'];
        }

        $addressBooks = $addressBookRepository->getAddressBookEntriesByCompany(Auth::user()->comp_id);
        foreach ($addressBooks as $addressBook) {

            //1.store only Recipient and generalContractor in Companies module
            if(in_array($addressBook->id, $companiesForPermissions)) {

                $companyAddressId = 0;
                if (isset($addressBook->addresses[0]) && count($addressBook->addresses) > 0) {
                    $companyAddressId = $addressBook->addresses[0]->id;
                }
                $company = $projectCompaniesRepository->storeCompany($data['proj_id'], ['ab_id' => $addressBook->id, 'comp_addr' => $companyAddressId]);

                //2.store permissions for this company
                $projectPermissionsRepository->insertInitialProjectCompanyPermissions($data['proj_id'], Auth::user()->comp_id, $company->id);

                //3.store default ab_contact
                $architectTransmittalId = $data['architect_transmittal_id'];
                $primeSubcontractorTransmittalId = $data['prime_subcontractor_transmittal_id'];
                $ownerTransmittalId = $data['owner_transmittal_id'];
                $contractorTransmittalId = $data['contractor_transmittal_id'];
                $transmittalFlag = $data['transmittal_flag'];

                $transmittalId = 0;
                if ($transmittalFlag == '1' && !empty($architectTransmittalId)) {
                    $transmittalId = $architectTransmittalId;
                } elseif ($transmittalFlag == '0' && !empty($contractorTransmittalId)) {
                    $transmittalId = $contractorTransmittalId;
                } elseif ($transmittalFlag == '2' && !empty($primeSubcontractorTransmittalId)) {
                    $transmittalId = $primeSubcontractorTransmittalId;
                } elseif ($transmittalFlag == '3' && !empty($ownerTransmittalId)) {
                    $transmittalId = $ownerTransmittalId;
                }

                if ($transmittalId) {
                    $contactArray['company_contact'] = $transmittalId;
                    $contactArray['officeId'] = $companyAddressId;
                    $projectCompaniesRepository->storeProjectContact($data['proj_id'], $addressBook->id, $company->id, $contactArray);
                }
            }
        }
    }
}