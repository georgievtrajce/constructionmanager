<?php
namespace App\Modules\Project_companies\Repositories;

use App\Models\Ab_contact;
use App\Models\Project;
use App\Models\Company;
use Illuminate\Support\Facades\Auth;
use App\Models\Address_book;
use App\Models\Project_company;
use App\Models\Project_subcontractor;
use App\Models\Project_company_contact;
use Config;
use Illuminate\Support\Facades\DB;

class ProjectCompaniesRepository {

    /**
     * get all info for address book entry
     * @param $addressBookId
     * @return mixed
     */
    public function getCompany($addressBookId){

        $company = Address_book::where('address_book.id', '=', $addressBookId)
            ->join('addresses', 'addresses.ab_id', '=', 'address_book.id')
            ->join('project_companies', 'project_companies.ab_id', '=', 'address_book.id')
            ->select(
                'address_book.*',
                'addresses.id as addr_id',
                'project_companies.id as proj_comp_id',
                'project_companies.addr_id as pc_addr_id'
            )
            ->firstOrFail();
        return $company;
    }

    /**
     * Get single project company
     * @param $projectId
     * @param $projectCompanyId
     * @return mixed
     */
    public function getProjectCompany($projectId, $projectCompanyId)
    {
        return Project_company::leftJoin('address_book','address_book.id','=','project_companies.ab_id')
            ->where('project_companies.id','=',$projectCompanyId)
            ->where('project_companies.proj_id','=',$projectId)
            ->where('address_book.owner_comp_id','=',Auth::user()->comp_id)
            ->select(
                'project_companies.*',
                'address_book.name as ab_name'
            )
            ->with('project')
            ->with('project_subcontractor')
            ->with('project_company_contacts')
            ->with('project_company_contacts.contact')
            ->firstOrFail();
    }

    /**
     * @param $projectId
     * @return mixed
     */
    public function getAllDistinctProjectCompanies($projectId = null)
    {
        $projectCompany = Project_company::leftJoin('projects','projects.id','=','project_companies.proj_id')
            ->with("address_book.contacts")
            ->where('projects.comp_id', '=', Auth::user()->comp_id);

        if (!empty($projectId)) {
            $projectCompany = $projectCompany->where('proj_id', '=', $projectId);
        }

        return $projectCompany->select('project_companies.*')->groupBy('ab_id')->get();
    }

    /**
     * @param $abId
     * @param $projectId
     * @return mixed
     */
    public function getProjectCompaniesByAddressBookAndProjectId($abId, $projectId)
    {
        return Project_company::where('ab_id', '=', $abId)->where('proj_id', '=', $projectId)->first();
    }

    /**
     * get project with all associated companies
     * @param $id
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getProjectCompanies($id, $sort, $order)
    {
        //get project with all associated companies
        $companies = Address_book::
        join('project_companies','project_companies.ab_id', '=', 'address_book.id')
            ->join('addresses', 'addresses.id', '=', 'project_companies.addr_id')
            ->with('default_categories')
            ->with('custom_categories')
            ->with('master_format_item')
            ->where('project_companies.proj_id', '=', $id)
            ->where('address_book.owner_comp_id', '=', Auth::user()->comp_id)
            ->select(
                'project_companies.id as pc_id',
                'project_companies.addr_id as pc_addr_id',
                'address_book.*',
                'addresses.office_title'
            )
            ->orderBy('address_book.'.$sort, $order)
            ->paginate(Config::get('constants.pagination.companies'));
        return $companies;
    }

    /**
     * Get specified project contractor
     * @param $projectID
     * @return mixed
     */
    public function getProjectContractor($projectID)
    {
        //get project with all associated companies
        $project = Project::where('id', '=', $projectID)->firstOrFail();
        $projectContractor = Company::where('id', '=', $project->comp_id)->firstOrFail();
        return $projectContractor;
    }

    /**
     * Get specified project subcontractors
     * @param $id
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getProjectCompaniesSubcontractors($id, $sort, $order)
    {
        //get project with all associated companies
        $companiesQuery = Address_book::join('project_companies', 'project_companies.ab_id', '=', 'address_book.id')
            ->leftJoin('companies', 'companies.id', '=', 'address_book.synced_comp_id')
            ->leftJoin('addresses', 'addresses.id', '=', 'project_companies.addr_id')
            ->leftJoin('project_subcontractors', function ($join) use($id) {
                $join->on('project_subcontractors.comp_child_id', '=', 'companies.id')
                    ->where('project_subcontractors.proj_id', '=', $id)
                    ->where('project_subcontractors.comp_parent_id', '=', Auth::user()->comp_id)
                    ->where('project_subcontractors.status','!=',Config::get('constants.projects.shared_status.unshared'))
                    ->orOn('project_subcontractors.proj_comp_id', '=', 'project_companies.id')
                    ->where('project_subcontractors.proj_id', '=', $id)
                    ->where('project_subcontractors.comp_parent_id', '=', Auth::user()->comp_id)
                    ->where('project_subcontractors.status','!=',Config::get('constants.projects.shared_status.unshared'));
            });

        if ($sort == 'mf.title') {
            $companiesQuery = $companiesQuery->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        $companiesQuery = $companiesQuery->with('default_categories')
            ->with('custom_categories')
            ->with('master_format_items')
            ->where('project_companies.proj_id', '=', $id)
            ->where('address_book.owner_comp_id', '=', Auth::user()->comp_id)
            ->select([
                'project_companies.id as pc_id',
                'project_companies.addr_id as pc_addr_id',
                'address_book.name',
                'address_book.id',
                'address_book.synced_comp_id',
                'companies.subs_id',
                'project_subcontractors.id as ps_id',
                'project_subcontractors.proj_comp_id as proj_comp_id',
                'project_subcontractors.status as ps_status',
                'project_subcontractors.token_active as ps_token_active',
                'addresses.office_title']
            )
            ->groupBy([
                'address_book.name',
                'address_book.id',
                'address_book.synced_comp_id',
               ])
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.companies'));

        return $companiesQuery;
    }

    /**
     * get project contact ids for project from all companies
     * @param $projectId
     * @param $projectCompanyId
     * @return array
     */
    public function getProjectContactsIDs($projectId, $projectCompanyId){

        //get all contacts IDS working for project from all companies
        $result = array();
        $contacts = Project_company_contact::where('proj_id', '=', $projectId)
            ->where('proj_comp_id', '=', $projectCompanyId)
            ->get();
        foreach ($contacts as $contact) {
            array_push($result, $contact->ab_cont_id);
        }
        return $result;

    }

    /**
     * get project contacts for project id
     * @param $projectId
     * @param $projectCompanyId
     * @return mixed
     */
    public function getProjectContacts($projectId, $projectCompanyId)
    {
        //get all contacts working for project from the specified company
        $contacts = Project_company_contact::where('proj_id', '=', $projectId)
            ->where('proj_comp_id', '=', $projectCompanyId)
            ->get();
        return $contacts;

    }

    /**
     * get all contacts IDS working for project from all companies
     * @param $projectId
     * @param $addressBookId
     * @return array
     */
    public function getProjectContactsIDsByCompany($projectId, $addressBookId){

        //get all contacts IDS working for project from all companies
        $result = array();
        $contacts = Project_company_contact::whereHas('contact', function($query) use ($addressBookId) {

            $query->where('ab_id', '=', $addressBookId);

        })->where('proj_id', '=', $projectId)->get();

        foreach ($contacts as $contact) {
            array_push($result, $contact->ab_cont_id);
        }
        return $result;
    }

    /**
     * Get project company contacts
     * @param $projectId
     * @param $projectCompanyId
     * @return
     */
    public function getProjectCompanyContacts($projectId, $projectCompanyId)
    {
        return Project_company_contact::where('proj_comp_id','=',$projectCompanyId)
            ->where('proj_id','=',$projectId)
            ->get();
    }

    /**
     * @param $id
     * @param $addressBookId
     * @return mixed
     */
    public function getProjectContactsByCompany($id, $addressBookId)
    {
        //get all contacts working for project from all companies

        $contacts = Ab_contact::whereHas('project', function($query) use ($id) {

            $query->where('proj_id', '=', $id);

        })->where('ab_id', '=', $addressBookId)->get();

        return $contacts;

    }

    public function getABContacts($addressBookId, $addressID){

        //get all contacts from specific company
        //TO DO replace this function on repo for AB
        $contacts = Ab_contact::where('ab_id', '=', $addressBookId)
            ->where('address_id', '=', $addressID)
            ->get();
        return $contacts;
    }

    /**
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeCompany($projectId, $data) {

        $storedCompany = Project_company::where('ab_id', '=',  $data['ab_id'])->where('proj_id', '=', $projectId)->first();

        //if company was not stored for that project with any address
        if (!$storedCompany) {
            //store company for project
            $companyData = array(
                'proj_id' => $projectId,
                'ab_id' => $data['ab_id'],
                'addr_id' => $data['comp_addr'],
            );
            return (Project_company::firstOrCreate($companyData));
        }

        return $storedCompany;
    }


    /**
     * store project new contacts, remove unticked
     * @param $projectId
     * @param $addressBookId
     * @param $projectCompanyId
     * @param $data
     * @return bool
     */
    public function storeContacts($projectId, $addressBookId, $projectCompanyId, $data)
    {
        $oldContacts = $this->getProjectContactsIDsByCompany($projectId, $addressBookId);

        //array needed for re-creating the contacts
        $contactData = array();
        $contactData['proj_id'] = $projectId;
        $contactData['ab_address_id'] = $data['officeId'];
        $contactData['ab_comp_id'] = $addressBookId;
        $contactData['proj_comp_id'] = $projectCompanyId;

        //flags if newly assigned and removed contacts are successfully updated
        $storeNewContacts = TRUE;
        $deleteContacts = TRUE;

        //add new contacts from input
        for ($i = 0; $i < sizeof($data['company_contacts']); $i++) {
            $contactData['ab_cont_id'] = $data['company_contacts'][$i];
            //update flag
            $storeNewContacts = Project_company_contact::firstOrCreate($contactData);
        }
        //delete contacts if some were unticked
        if (sizeof($oldContacts) > 0) {
            for ($i = 0; $i<sizeof($oldContacts); $i++){

                //if no new ticked or if old were unticked
                if (!$data['company_contacts'] || !in_array($oldContacts[$i],$data['company_contacts'])) {
                    $contact = Project_company_contact::where('ab_cont_id','=',$oldContacts[$i])->where('proj_id','=',$projectId)->firstOrFail();
                    if ($contact){
                        //update flag
                       $deleteContacts =  $contact->delete();
                    }
                }
            }
        }
        if ($storeNewContacts && $deleteContacts) {
            return TRUE;
        }
        return FALSE;

    }


    /**
     * delete company from project with contacts
     * @param $projectId
     * @param $contactID
     * @return bool
     */
    public function deleteCompany($projectId, $contactID)
    {

        //get contacts from company that is to be deleted
        $contacts = $this->getProjectContactsIDsByCompany($projectId, $contactID);
        $deleteContact = TRUE;

        for ($i=0;$i<sizeof($contacts); $i++){
            //delete project contacts
            $contact = Project_company_contact::where('proj_id', '=', $projectId)->where('ab_cont_id', '=', $contacts[$i])->firstOrFail();
            $deleteContact = $contact->delete();
        }

        //delete company
        $company = Project_company::where('proj_id', '=', $projectId)->where('ab_id','=',$contactID)->firstOrFail();
        $deleteCompany = $company->delete();

        if ($deleteContact && $deleteCompany) {
            return TRUE;
        }
        return FALSE;

    }

    /**
     * Delete project company
     * @param $projectId
     * @param $id
     * @return mixed
     */
    public function deleteProjectCompany($projectId, $id)
    {
        return Project_company::where('project_companies.id','=',$id)
            ->join('address_book', function($join)
            {
                $join->on('project_companies.ab_id', '=', 'address_book.id')
                ->where('owner_comp_id','=',Auth::user()->comp_id);
            })
            ->where('proj_id','=',$projectId)
            ->delete();
    }

    /**
     * share project with subcontractor
     * @param $projectId
     * @param $data
     * @return static
     */
    public function shareSubcontractor($projectId, $data)
    {
        //store subcontractor for project
        $uuid1 = Uuid::uuid1()->toString();
        $subcontractorData = array(
            'proj_id' => $projectId,
            'comp_parent_id' => Auth::user()->comp_id,
            'comp_child_id' => $data['comp_id'],
            'address_id' => $data['addresses'],
            'token_active' => 1,
            'token' => $uuid1,
            'status' => 1,
        );

        return Project_subcontractor::firstOrCreate($subcontractorData);
    }

    public function updateAddress($data)
    {
        //get project company
        $company = Project_company::where('id', '=', $data['sub_id'])->firstOrFail();

        //delete project company contacts from previous address (office)
        $contacts = Project_company_contact::where('proj_comp_id','=',$data['sub_id'])
            ->where('proj_id','=',$data['proj_id'])
            ->get();

        //if there are any contacts selected for this company, delete them
        if (count($contacts)) {
            foreach ($contacts as $contact) {
                $contact->delete();
            }
        }

        $company->addr_id = $data['address_id'];
        $company->save();

        return $company;
    }

    /**
     * @param $projectId
     * @param $projectCompanyId
     * @param $abContactId
     * @return mixed
     */
    public function getProjectContact($projectId, $projectCompanyId, $abContactId)
    {
        return Project_company_contact::where('proj_comp_id','=',$projectCompanyId)
            ->where('proj_id','=',$projectId)
            ->where('ab_cont_id','=',$abContactId)
            ->first();
    }

    /**
     * @param $projectId
     * @param $addressBookId
     * @param $projectCompanyId
     * @param $data
     * @return mixed|static
     */
    public function storeProjectContact($projectId, $addressBookId, $projectCompanyId, $data)
    {
        $contact = $this->getProjectContact($projectId, $projectCompanyId, $addressBookId);

        //if company was not stored for that project with any address
        if (!$contact) {
            //store company for project
            $contactData = array(
                'proj_id' => $projectId,
                'ab_address_id' => $data['officeId'],
                'ab_comp_id' => $addressBookId,
                'proj_comp_id' => $projectCompanyId,
                'ab_cont_id' => $data['company_contact'],
            );
            return Project_company_contact::firstOrCreate($contactData);
        }

        return $contact;
    }

}