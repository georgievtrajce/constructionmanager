<?php

namespace App\Modules\Mail\Repositories;
use App\Models\Subscription_type;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;
use App\Models\Project;
use App\Models\Project_subcontractor;
use Illuminate\Support\Facades\Mail;
use App\Models\User;



class MailRepository {

    /**
     * Send email to subcontractor when a project is shared
     * @param $data
     * @return mixed
     */
    public function sendMailSubcontractor($data){

        //get data for receiver of email

        $childCompany = Company::with('users')->where('id','=',$data['comp_id'])->firstOrFail();

        //get project subcontractor record with project
        $projectSubcontractor = Project_subcontractor::with('parent_company')->with('project')
            ->where('comp_child_id','=',$data['comp_id'])
            ->where('comp_parent_id','=',Auth::user()->comp_id)
            ->where('proj_id','=',$data['proj_id'])
            ->firstOrfail();

        $adminEmail = '';
        $adminName = 'Sir or Madam';

        for ($i=0;$i<sizeof($childCompany->users);$i++){
            if ($childCompany->users[$i]->hasRole('Company Admin')) {
                $adminEmail = $childCompany->users[$i]->email;
                $adminName = $childCompany->users[$i]->name;
                break;
            }
        }

        //prepare data for sending email
        $mailData = array(
            'to' => $adminEmail,
            'subject' => 'A new project was shared with you',
            'name' => $adminName,
            'sender' => Auth::user()->name . " (" . $projectSubcontractor->parent_company->name . ") via cloud-pm.com"
        );

        $view = 'emails.share_project_subcontractor';

        $viewData = array(
            'token' => $projectSubcontractor->token,
            'childCompany' => $childCompany->name,
            'parentCompany' => $projectSubcontractor->parent_company->name,
            'project' => $projectSubcontractor->project->name,
            'adminName' => $adminName,
        );

        return $this->sendMail($view, $viewData, $mailData);
    }

    /**
     * Send transmittal notification
     * @param $data
     * @return mixed
     */
    public function sendTransmittalNotification($data)
    {
        $subject = trans('labels.emails.transmittal_notification');
        if (!empty($data['email']->submittal_name)) {
            $subject = $data['email']->project_name." - Submittal: ".$data['email']->submittal_name;
        } else if (!empty($data['email']->rfi_name)) {
            $subject = $data['email']->project_name." - RFI: ".$data['email']->rfi_name;
        } else if (!empty($data['email']->pco_name)) {
            $subject = $data['email']->project_name." - PCO: ".$data['email']->pco_name;
        }

        $email = $data['email']->email;
        return Mail::queue('emails.transmittal_notification', $data, function($message) use ($email, $subject) {
            $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to($email);
            $message->subject($subject);
        });
    }

    /**
     * Send transmittal notification to public users
     * @param $data
     * @return mixed
     */
    public function sendTransmittalNotificationPublic($data)
    {
        $subject = trans('labels.emails.transmittal_notification');
        if (!empty($data['email']->submittal_name)) {
            $subject = $data['email']->project_name." - Submittal: ".$data['email']->submittal_name;
        } else if (!empty($data['email']->rfi_name)) {
            $subject = $data['email']->project_name." - RFI: ".$data['email']->rfi_name;
        } else if (!empty($data['email']->pco_name)) {
            $subject = $data['email']->project_name." - PCO: ".$data['email']->pco_name;
        }

        $email = $data['email']->email;
        return Mail::queue('emails.transmittal_notification_public', $data, function($message) use ($email, $subject) {
            $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to($email);
            $message->subject($subject);
        });
    }

    /**
     * Send project file notification
     * @param $data
     * @return mixed
     */
    public function sendProjectFileNotification($data)
    {
        $email = $data['email']->email;
        $projectName = $data['projectName'];
        $fileName = $data['fileName'];
        return Mail::queue('emails.project_files_notification', $data, function($message) use ($email, $projectName, $fileName) {
            $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to($email);
            $message->subject($projectName.' - '.$fileName);
        });
    }

    /**
     * Send project file notification
     * @param $data
     * @return mixed
     */
    public function sendExceededLimitNotification($data)
    {
        $email = $data['email'];
        $type = $data['type'];
        return Mail::queue('emails.limit_exceeded_notification', $data, function($message) use ($email, $type) {
            $message->to($email);
            $message->subject(($type == 'storage')?trans('labels.emails.limit-storage'):(($type == 'upload')?trans('labels.emails.limit-upload-transfer'):trans('labels.emails.limit-download-transfer')));
        });
    }

    /**
     * send referral email
     * @param $data
     * @return mixed
     */
    public function sendReferral($data)
    {
        //get data for company that invites
        $invitingCompany = Company::with('users')->where('id', '=', $data['company'])->firstOrFail();

        //prepare email data
        $mailData = array(
            'to' => $data['email'],
            'subject' => 'Invitation to join Cloud PM',
            'name' => 'Sir or Madam',
            'sender' => Auth::user()->name . " (" . $invitingCompany->name . ") via cloud-pm.com"
        );

        //prepare view
        $view = 'emails.invite';

        //prepare data in view
        $viewData = array(
            'company' => $invitingCompany->name,
            'token'   => $data['token'],
        );

        //send mail
        return $this->sendMail($view, $viewData, $mailData);
    }

    /**
     * send email
     * @param $view - that appears in email
     * @param $viewData - data contained in emai view
     * @param $mailData - email, name etc
     * @return mixed - return if mail is sent
     */
    public function sendMail($view, $viewData, $mailData)
    {
        $sent = Mail::send($view, $viewData, function($message) use ($mailData) {
            $message->from(env('MAIL_FROM_ADDRESS'), isset($mailData['sender'])?$mailData['sender']:env('MAIL_FROM_ADDRESS'));
            $message->to($mailData['to'], $mailData['name']);
            $message->subject($mailData['subject']);
        });
        return ($sent);
    }

    public function sendChangeSubscriptionReceipt($data) {

        $user = User::where('email', '=', $data['email'])->first();
        $company = Company::where('id', '=', $data['companyId'])->first();
        $subscription = Subscription_type::where('id', '=', $data['subscriptionId'])->first();
        $mailData = array(
            'to' => $data['email'],
            'subject' => $data['subject'],
            'name' => $user->name,
        );

        //prepare data in view
        $viewData = array(
            'name'       => $user->name,
            'companyName' => $company->name,
            'subscription' => $subscription->name,
            'cardToken' => $data['cardToken'],
            'amount' => $data['amount'],
            'transactionId' => $data['transactionId']
        );
        return $this->sendMail($data['view'], $viewData, $mailData);
    }

    /**
     * Send transmittal notification to public users
     * @param $data
     * @return mixed
     */
    public function sendDailyReportNotification($data)
    {
        $email = $data['email']->email;
        $subject = $data['subject'];
        return Mail::queue('emails.daily_report_notification', $data, function($message) use ($email, $subject) {
            $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to($email);
            $message->subject($subject);
        });
    }

    /**
     * Send project file notification
     * @param $data
     * @return mixed
     */
    public function sendTransmittalUpdateNotification($data)
    {
        $email = $data['contact']['email'];
        $currentLoggedUserEmail = Auth::user()->email;
        $projectName = $data['projectName'];
        $moduleName = $data['moduleName'];
        $name = $data['name'];
        return Mail::queue('emails.transmittal_update_notification', $data, function($message) use ($email, $projectName, $moduleName, $name, $currentLoggedUserEmail) {
            $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to([$email, $currentLoggedUserEmail]);
            $message->subject($projectName.' - '.$moduleName. ': '. $name);
        });
    }

    /**
     * Send project file notification
     * @param $data
     * @return mixed
     */
    public function sendPermissionWriteRecipientNotification($data)
    {
        $projectName = $data['projectName'];

        foreach ($data['contacts'] as $contact) {
            $data['contact'] = $contact->toArray();
            $email = $data['contact']['email'];
            Mail::queue('emails.permission_update_notification', $data, function($message) use ($email, $projectName) {
                $message->from(env('MAIL_FROM_ADDRESS'), Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
                $message->to([$email]);
                $message->subject($projectName.' - Write Permissions');
            });
        }

        return true;
    }
}