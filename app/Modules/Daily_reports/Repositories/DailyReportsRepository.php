<?php
namespace App\Modules\Daily_reports\Repositories;

use App\Models\Daily_report_contractor;
use App\Models\Daily_report_contractor_equipment;
use App\Models\Daily_report_contractor_inspection;
use App\Models\Daily_report_contractor_instruction;
use App\Models\Daily_report_contractor_material;
use App\Models\Daily_report_contractor_observation;
use App\Models\Daily_report_contractor_trade;
use App\Models\Daily_report_email;
use App\Models\Daily_report_file;
use App\Models\Daily_report;
use App\Models\Daily_report_subcontractor;
use App\Models\Project_file;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class DailyReportsRepository {

    private $companyId;
    private $userId;

    public function __construct()
    {
        if (Auth::user()) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
    }

    public function getGeneratedNumbers($projectId)
    {
        $dailyReport = Daily_report::select('report_num')
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $this->companyId)
            ->orderBy('id', 'DESC')
            ->first();

        if($dailyReport) {
            $generateNumber =  ltrim($dailyReport->report_num, "0");
            return str_pad(($generateNumber+1), 4, '0', STR_PAD_LEFT);
        } else {
            //0001
            return str_pad(1, 4, '0', STR_PAD_LEFT);
        }
    }

    //step 1 & step 2 & step 10 at once
    public function storeGeneralFullInfo($projectId, $data)
    {
        return Daily_report::create([
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'date' => empty($data['date']) ? '' : Carbon::parse($data['date'])->format('Y-m-d'),
            'report_num' => empty($data['report_num']) ? '' : $data['report_num'],
            'is_uploaded' => empty($data['is_uploaded']) ? 0 : 1,
            'copied_report_id' => empty($data['copied_report_id']) ? 0 : $data['copied_report_id'],

            'is_windy' => empty($data['is_windy']) ? 0 : $data['is_windy'],
            'is_sunny' => empty($data['is_sunny']) ? 0 : $data['is_sunny'],
            'is_overcast' => empty($data['is_overcast']) ? 0 : $data['is_overcast'],
            'is_cloudy' => empty($data['is_cloudy']) ? 0 : $data['is_cloudy'],
            'is_rain' => empty($data['is_rain']) ? 0 : $data['is_rain'],
            'is_snow' => empty($data['is_snow']) ? 0 : $data['is_snow'],
            'is_sleet' => empty($data['is_sleet']) ? 0 : $data['is_sleet'],
            'is_hail' => empty($data['is_hail']) ? 0 : $data['is_hail'],
            'is_lightning' => empty($data['is_lightning']) ? 0 : $data['is_lightning'],
            'temperature' => empty($data['temperature']) ? 0 : $data['temperature'],

            'is_dry' => empty($data['is_dry']) ? 0 : $data['is_dry'],
            'is_frozen' => empty($data['is_frozen']) ? 0 : $data['is_frozen'],
            'ground_is_snow' => empty($data['ground_is_snow']) ? 0 : $data['ground_is_snow'],
            'is_mud' => empty($data['is_mud']) ? 0 : $data['is_mud'],
            'is_water' => empty($data['is_water']) ? 0 : $data['is_water'],
            'condition_notes' => empty($data['condition_notes']) ? 0 : $data['condition_notes'],

            'general_notes' => empty($data['general_notes']) ? 0 : $data['general_notes']
        ]);
    }

    //step 1
    public function storeGeneralInfo($projectId, $data)
    {
        return Daily_report::create([
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'date' => empty($data['date']) ? '' : Carbon::parse($data['date'])->format('Y-m-d'),
            'report_num' => empty($data['report_num']) ? '' : $data['report_num'],
            'is_uploaded' => empty($data['is_uploaded']) ? 0 : 1,
            'copied_report_id' => empty($data['copied_report_id']) ? 0 : $data['copied_report_id'],
        ]);
    }

    public function updateGeneralInfo($projectId, $dailyReportId, $data)
    {
        $dailyReport = Daily_report::where('id','=',$dailyReportId)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $this->companyId)
            ->where('user_id', '=', $this->userId)
            ->firstOrFail();

        $dailyReport->date = empty($data['date']) ? '' : Carbon::parse($data['date'])->format('Y-m-d');
        $dailyReport->report_num = $data['report_num'];
        return $dailyReport->save();
    }

    //step 2
    public function updateWeatherAndGroundConditions($dailyReportId, $data)
    {
        $dailyReport = Daily_report::where('id','=',$dailyReportId)->firstOrFail();

        //weather conditions
        $dailyReport->is_windy = empty($data['is_windy']) ? 0 : 1;
        $dailyReport->is_sunny = empty($data['is_sunny']) ? 0 : 1;
        $dailyReport->is_overcast = empty($data['is_overcast']) ? 0 : 1;
        $dailyReport->is_cloudy = empty($data['is_cloudy']) ? 0 : 1;
        $dailyReport->is_rain = empty($data['is_rain']) ? 0 : 1;
        $dailyReport->is_snow = empty($data['is_snow']) ? 0 : 1;
        $dailyReport->is_sleet = empty($data['is_sleet']) ? 0 : 1;
        $dailyReport->is_hail = empty($data['is_hail']) ? 0 : 1;
        $dailyReport->is_lightning = empty($data['is_lightning']) ? 0 : 1;
        $dailyReport->temperature = empty($data['temperature']) ? 0 : $data['temperature'];

        //ground conditions
        $dailyReport->is_dry = empty($data['is_dry']) ? 0 : 1;
        $dailyReport->is_frozen = empty($data['is_frozen']) ? 0 : 1;
        $dailyReport->ground_is_snow = empty($data['ground_is_snow']) ? 0 : 1;
        $dailyReport->is_mud = empty($data['is_mud']) ? 0 : 1;
        $dailyReport->is_water = empty($data['is_water']) ? 0 : 1;
        $dailyReport->condition_notes = empty($data['condition_notes']) ? 0 : $data['condition_notes'];

        return $dailyReport->save();
    }

    //step 3 tab 1
    public function storeContractorLabor($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            //workers with names tab
            'worker_office' => (isset($data['worker_office']) && !empty($data['worker_office'])) ? $data['worker_office'] : null,
            'worker_name' => (isset($data['worker_name']) && !empty($data['worker_name'])) ? $data['worker_name'] : null,
            'title' => (isset($data['title']) && !empty($data['title'])) ? $data['title'] : null,
            'regular_hours' => (isset($data['regular_hours']) && !empty($data['regular_hours'])) ? $data['regular_hours'] : null,
            'overtime_hours' => (isset($data['overtime_hours']) && !empty($data['overtime_hours'])) ? $data['overtime_hours'] : 0,
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null,
        ]);
    }

    public function getContractorLabor($rowId)
    {
        return Daily_report_contractor::where('id','=',$rowId)->first();
    }

    public function updateContractorLabor($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getContractorLabor($rowId);

            //workers with names tab
            if(isset($data['worker_office'])) $row->worker_office = $data['worker_office'];
            if(isset($data['worker_name'])) $row->worker_name = $data['worker_name'];
            if(isset($data['title'])) $row->title = $data['title'];
            if(isset($data['regular_hours'])) $row->regular_hours = $data['regular_hours'];
            if(isset($data['overtime_hours'])) $row->overtime_hours = $data['overtime_hours'];
            if(isset($data['description'])) $row->description = $data['description'];
            return $row->save();
        }
        return false;
    }

    public function deleteContractorLaborRow($rowId)
    {
        $row = $this->getContractorLabor($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteContractorMultipleRows($rowsArray)
    {
        return Daily_report_contractor::whereIn('id', $rowsArray)->delete();
    }

    //step 3 tab 2
    public function storeContractorTradeLabor($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_trade::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            //workers per trade tab
            'trade' => (isset($data['trade']) && !empty($data['trade'])) ? $data['trade'] : null,
            'workers' => (isset($data['workers']) && !empty($data['workers'])) ? $data['workers'] : null,
            'regular_hours' => (isset($data['regular_hours']) && !empty($data['regular_hours'])) ? $data['regular_hours'] : null,
            'overtime_hours' => (isset($data['overtime_hours']) && !empty($data['overtime_hours'])) ? $data['overtime_hours'] : 0,
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null,
        ]);
    }

    public function getContractorTradeLabor($rowId)
    {
        return Daily_report_contractor_trade::where('id','=',$rowId)->first();
    }

    public function updateContractorTradeLabor($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getContractorTradeLabor($rowId);

            //workers per trade tab
            if(isset($data['trade'])) $row->trade = $data['trade'];
            if(isset($data['workers'])) $row->workers = $data['workers'];
            if(isset($data['regular_hours'])) $row->regular_hours = $data['regular_hours'];
            if(isset($data['overtime_hours'])) $row->overtime_hours = $data['overtime_hours'];
            if(isset($data['description'])) $row->description = $data['description'];
            return $row->save();
        }
        return false;
    }

    public function deleteContractorLaborTradeRow($rowId)
    {
        $row = $this->getContractorTradeLabor($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteContractorTradeMultipleRows($rowsArray)
    {
        return Daily_report_contractor_trade::whereIn('id', $rowsArray)->delete();
    }

    //step 4
    public  function storeSubContractorLabor($projectId, $dailyReportId, $data)
    {
        return Daily_report_subcontractor::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'selected_comp_id' => $data['selected_comp_id'],
            'trade' => (isset($data['trade']) && !empty($data['trade'])) ? $data['trade'] : null,
            'workers' => (isset($data['workers']) && !empty($data['workers'])) ? $data['workers'] : null,
            'regular_hours' => (isset($data['regular_hours']) && !empty($data['regular_hours'])) ? $data['regular_hours'] : null,
            'overtime_hours' => (isset($data['overtime_hours']) && !empty($data['overtime_hours'])) ? $data['overtime_hours'] : 0,
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null
        ]);
    }

    public function getSubContractorLaborRow($rowId)
    {
        return Daily_report_subcontractor::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateSubContractorLabor($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getSubContractorLaborRow($rowId);
            $row->selected_comp_id = $data['selected_comp_id'];
            $row->trade = $data['trade'];
            $row->workers = $data['workers'];
            $row->regular_hours = $data['regular_hours'];
            $row->overtime_hours = $data['overtime_hours'];
            if(isset($data['description'])) $row->description = $data['description'];
            return $row->save();
        }
        return false;
    }

    public function deleteSubContractorLaborRow($rowId)
    {
        $row = $this->getSubContractorLaborRow($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteSubContractorMultipleRows($rowsArray)
    {
        return Daily_report_subcontractor::whereIn('id', $rowsArray)->delete();
    }

    //step 5
    public function storeEquipmentOnSite($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_equipment::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'selected_comp_id' => $data['selected_comp_id'],
            'equipment' => (isset($data['equipment']) && !empty($data['equipment'])) ? $data['equipment'] : null,
            'quality' => (isset($data['quality']) && !empty($data['quality'])) ? $data['quality'] : null,
            'hours' => (isset($data['hours']) && !empty($data['hours'])) ? $data['hours'] : 0,
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null
        ]);
    }

    public function getEquipmentOnSite($rowId)
    {
        return Daily_report_contractor_equipment::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateEquipmentOnSite($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getEquipmentOnSite($rowId);
            $row->equipment = $data['equipment'];
            $row->quality = $data['quality'];
            $row->selected_comp_id = $data['selected_comp_id'];
            $row->hours = $data['hours'];
            if(isset($data['description'])) $row->description = $data['description'];
            return $row->save();
        }
        return false;
    }

    public function deleteEquipmentOnSite($rowId)
    {
        $row = $this->getEquipmentOnSite($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteMultipleEquipmentOnSite($rowsArray)
    {
        return Daily_report_contractor_equipment::whereIn('id', $rowsArray)->delete();
    }

    //step 6
    public function storeConstructionMaterials($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_material::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'material_delivered' => $data['material_delivered'],
            'quantity' => $data['quantity'],
            'selected_comp_id' => $data['selected_comp_id'],
            'notes' => (isset($data['notes']) && !empty($data['notes'])) ? $data['notes'] : null
        ]);
    }

    public function getConstructionMaterials($rowId)
    {
        return Daily_report_contractor_material::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateConstructionMaterials($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getConstructionMaterials($rowId);
            $row->material_delivered = $data['material_delivered'];
            $row->quantity = $data['quantity'];
            $row->selected_comp_id = $data['selected_comp_id'];
            if(isset($data['notes'])) $row->notes = $data['notes'];
            return $row->save();
        }
        return false;
    }

    public function deleteConstructionMaterials($rowId)
    {
        $row = $this->getEquipmentOnSite($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteMultipleConstructionMaterials($rowsArray)
    {
        return Daily_report_contractor_material::whereIn('id', $rowsArray)->delete();
    }

    //step 7
    public function storeInspections($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_inspection::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'testing_performed' => $data['testing_performed'],
            'mf_title' => $data['mf_title'],
            'mf_number' => $data['mf_number'],
            'selected_comp_id' => (isset($data['selected_comp_id']) && !empty($data['selected_comp_id'])) ? $data['selected_comp_id'] : 0,
            'selected_employee_id' => (isset($data['selected_employee_id']) && !empty($data['selected_employee_id'])) ? $data['selected_employee_id'] : 0,
            'other_company' => (isset($data['other_company']) && !empty($data['other_company'])) ? $data['other_company'] : null,
            'other_employee' => (isset($data['other_employee']) && !empty($data['other_employee'])) ? $data['other_employee'] : null,
            'notes' => (isset($data['notes']) && !empty($data['notes'])) ? $data['notes'] : null
        ]);
    }

    public function getInspections($rowId)
    {
        return Daily_report_contractor_inspection::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateInspections($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getInspections($rowId);
            $row->selected_comp_id = $data['selected_comp_id'];
            $row->testing_performed = $data['testing_performed'];
            $row->mf_number = $data['mf_number'];
            $row->mf_title = $data['mf_title'];
            $row->selected_employee_id = $data['selected_employee_id'];
            if(isset($data['notes'])) $row->notes = $data['notes'];
            return $row->save();
        }
        return false;
    }

    public function deleteInspections($rowId)
    {
        $row = $this->getInspections($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteMultipleInspections($rowsArray)
    {
        return Daily_report_contractor_inspection::whereIn('id', $rowsArray)->delete();
    }

    //step 8
    public function storeObservations($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_observation::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'safety_deficiencies_observed' => $data['safety_deficiencies_observed'],
            'selected_comp_id' => $data['selected_comp_id'],
            'notes' => (isset($data['notes']) && !empty($data['notes'])) ? $data['notes'] : null
        ]);
    }

    public function getObservations($rowId)
    {
        return Daily_report_contractor_observation::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateObservations($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getObservations($rowId);
            $row->safety_deficiencies_observed = $data['safety_deficiencies_observed'];
            $row->selected_comp_id = $data['selected_comp_id'];
            if(isset($data['notes'])) $row->notes = $data['notes'];
            return $row->save();
        }
        return false;
    }

    public function deleteObservations($rowId)
    {
        $row = $this->getObservations($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteMultipleObservations($rowsArray)
    {
        return Daily_report_contractor_observation::whereIn('id', $rowsArray)->delete();
    }

    //step 9
    public function storeVerbalInstructions($projectId, $dailyReportId, $data)
    {
        return Daily_report_contractor_instruction::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'selected_comp_id' => $data['selected_comp_id'],
            'selected_employee_id' => $data['selected_employee_id'],
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null
        ]);
    }

    public function getVerbalInstructions($rowId)
    {
        return Daily_report_contractor_instruction::where('id', '=', $rowId)->firstOrFail();
    }

    public function updateVerbalInstructions($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            $row = $this->getVerbalInstructions($rowId);
            $row->selected_comp_id = $data['selected_comp_id'];
            $row->selected_employee_id = $data['selected_employee_id'];
            if(isset($data['description'])) $row->description = $data['description'];
            return $row->save();
        }
        return false;
    }

    public function deleteVerbalInstructions($rowId)
    {
        $row = $this->getVerbalInstructions($rowId);

        if($row) {
            return $row->delete();
        }
        return false;
    }

    public function deleteMultipleVerbalInstructions($rowsArray)
    {
        return Daily_report_contractor_instruction::whereIn('id', $rowsArray)->delete();
    }

    //step 10
    public function updateGeneralNotes($dailyReportId, $data)
    {
        if(isset($data['general_notes']) && !empty($data['general_notes']))
        {
            $dailyReport = Daily_report::where('id','=',$dailyReportId)->firstOrFail();
            $dailyReport->general_notes = $data['general_notes'];
            return $dailyReport->save();
        }
        return false;
    }

    //step11 and step12
    public function storeFiles($projectId, $dailyReportId, $data)
    {
        //store file local - S3 - remove local
        $uploadedFileType = $data['daily_type'] . '_file';
        $originalFileName = $data[$uploadedFileType]->getClientOriginalName();
        $fileSize = $data[$uploadedFileType]->getClientSize();
        $fileMimeType = $data[$uploadedFileType]->getClientMimeType();
//        $fileMimeType2 = $data[$uploadedFileType]->guessClientExtension();
        $path = 'company_' . $this->companyId . '/project_' . $projectId . '/daily_report/' . $dailyReportId . '/' . $data['daily_type'];

        $fileNameS3 = $this->uploadDailyReportFileS3($path, $data[$uploadedFileType], $originalFileName);

        if($fileNameS3)
        {
            //store in files
            $file = Project_file::create([
                'ft_id' => $data['file_type_id'],
                'user_id' => $this->userId,
                'comp_id' => $this->companyId,
                'proj_id' => $projectId,
                'size' => (!empty($fileSize)) ? $fileSize : null,
                'name' => (!empty($originalFileName)) ? $originalFileName : null,
                'file_name' => (!empty($originalFileName)) ? $originalFileName : null,
            ]);

            //store in daily report files
            return Daily_report_file::create([
                'daily_report_id' => $dailyReportId,
                'proj_id' => $projectId,
                'comp_id' => $this->companyId,
                'user_id' => $this->userId,
                'file_id' => $file->id,
                'file_type_id' => $data['file_type_id'],
                'name' => $data['name'],
                'file_name' => (!empty($originalFileName)) ? $originalFileName : null,
                'file_mime_type' => (!empty($fileMimeType)) ? $fileMimeType : null,
                'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null,
                'emailed' => (isset($data['emailed']) && !empty($data['emailed'])) ? 1 : 0
            ]);
        }
        return false;
    }

    public function createDailyReportFile($projectId, $dailyReportId, $data)
    {
        //store in files
        $file = Project_file::create([
            'ft_id' => $data['file_type_id'],
            'user_id' => $this->userId,
            'comp_id' => $this->companyId,
            'proj_id' => $projectId,
            'size' => (!empty($data['size'])) ? $data['size'] : null,
            'name' => (!empty($data['name'])) ? $data['name'] : null,
            'file_name' => (!empty($data['file_name'])) ? $data['file_name'] : null
        ]);

        return Daily_report_file::create([
            'daily_report_id' => $dailyReportId,
            'proj_id' => $projectId,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'file_type_id' => $data['file_type_id'],
            'file_id' => $file->id,
            'name' => $data['name'],
            'file_name' => (!empty($data['file_name'])) ? $data['file_name'] : null,
            'file_mime_type' => (!empty($data['file_mime_type'])) ? $data['file_mime_type'] : null,
            'description' => (isset($data['description']) && !empty($data['description'])) ? $data['description'] : null
        ]);
    }

    public function getDailyReportFile($id)
    {
        return Daily_report_file::where('id','=',$id)->firstOrFail();
    }

    public function getDailyReportFileByDailyReportId($dailyReportId, $fileType = null)
    {
        $dailyReport =  Daily_report_file::where('daily_report_id','=',$dailyReportId);

        if($fileType) {
            $dailyReport->where('file_type_id','=',$fileType);
        }

        return $dailyReport->get();
    }

    public function getGeneratedReportFileByDailyReportId($dailyReportId)
    {
        return Daily_report_file::where('daily_report_id','=',$dailyReportId)
            ->where('file_type_id', '=', Config::get('constants.daily_report_file_type_id'))->first();
    }

    public function deleteGeneratedReportFile($file)
    {
        //delete file from Project files
        Project_file::where('id','=', $file->file_id)->delete();

        //delete file from Daily report files
        Daily_report_file::where('id','=',$file->id)->delete();
    }

    //s3
    public function uploadDailyReportFileS3($path, $file, $fileName)
    {
        //upload file locally
        $destinationPath = storage_path().'/app';
        $file->move($destinationPath, $fileName);

        //s3 upload
        $disk = Storage::disk('s3');
        $contents = Storage::disk('local')->get($fileName);
        $s3Upload = $disk->put($path.'/'.$fileName, $contents);
//        $s3Upload = $disk->put($path.'/'.$fileName,$contents,'public');

        if($s3Upload) {
            //if s3 upload is successful delete local file
            Storage::disk('local')->delete($fileName);
            return $fileName;
        }
        return false;
    }

    private function deleteDailyReportFileFromS3($projectId, $dailyReportId, $file)
    {
        $disk = Storage::disk('s3');
        $dailyFileTypes = Config::get('constants.daily_file_types');
        $filePath = 'company_' . $this->companyId . '/project_'. $projectId .'/daily_report/' . $dailyReportId .'/'.  $dailyFileTypes[$file->file_type_id] . '/' .$file->file_name;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    private function deleteDailyReportFileFromDb($id)
    {
        return Daily_report_file::where('id','=',$id)->delete();
    }

    private function deleteProjectFileFromDb($fileId)
    {
        return Project_file::where('id', '=', $fileId)->delete();
    }

    //delete
    public function deleteDailyReportFile($projectId, $dailyReportId, $id)
    {
        $file = $this->getDailyReportFile($id);
        $this->deleteDailyReportFileFromS3($projectId, $dailyReportId, $file);
        $this->deleteDailyReportFileFromDb($id);
        $this->deleteProjectFileFromDb($file->file_id);
    }

    //update
    public function updateDailyReportFile($projectId, $dailyReportId, $rowId, $data)
    {
        if($rowId)
        {
            //get daily report file from db
            $row = $this->getDailyReportFile($rowId);

            //delete old file from s3
            $this->deleteDailyReportFileFromS3($projectId, $dailyReportId, $row);

            //upload new file to s3
            $uploadedFileType = $data['daily_type'] . '_file';
            $originalFileName = $data[$uploadedFileType]->getClientOriginalName();
            $fileMimeType = $data[$uploadedFileType]->getClientMimeType();
            $fileSize = $data[$uploadedFileType]->getClientSize();
            $path = 'company_' . $this->companyId . '/project_' . $projectId . '/daily_report/' . $dailyReportId . '/' . $data['daily_type'];

            $fileNameS3 = $this->uploadDailyReportFileS3($path, $data[$uploadedFileType], $originalFileName);

            if($fileNameS3)
            {
                //get files from db
                $fileRow = Project_file::where('id', '=', $row->file_id)->first();

                if($fileRow)
                {
                    //update file from db
                    $fileRow->size =  (!empty($fileSize)) ? $fileSize : null;
                    $fileRow->name = (!empty($originalFileName)) ? $originalFileName : null;
                    $fileRow->save();

                    //update daily report file from db
                    $row->name = $data['name'];
                    $row->description = $data['description'];
                    $row->file_name = (!empty($originalFileName)) ? $originalFileName : null;
                    $row->file_mime_type =  (!empty($fileMimeType)) ? $fileMimeType : null;
                    return $row->save();
                }
            }
        }
        return false;
    }

    public function deleteCompleteDailyReport($projectId, $dailyReportId)
    {
        //step 3
        Daily_report_contractor::where('daily_report_id', '=', $dailyReportId)->delete();
        Daily_report_contractor_trade::where('daily_report_id', '=', $dailyReportId)->delete();
        //step 4
        Daily_report_subcontractor::where('daily_report_id', '=', $dailyReportId)->delete();
        //step 5
        Daily_report_contractor_equipment::where('daily_report_id', '=', $dailyReportId)->delete();;
        //step 6
        Daily_report_contractor_material::where('daily_report_id', '=', $dailyReportId)->delete();
        //step 7
        Daily_report_contractor_inspection::where('daily_report_id', '=', $dailyReportId)->delete();
        //step 8
        Daily_report_contractor_observation::where('daily_report_id', '=', $dailyReportId)->delete();
        //step 9
        Daily_report_contractor_instruction::where('daily_report_id', '=', $dailyReportId)->delete();

        //step 11 and 12
        $dailyReportFiles = $this->getDailyReportFileByDailyReportId($dailyReportId);
        foreach ($dailyReportFiles as $dailyReportFile)
        {
            $this->deleteDailyReportFile($projectId, $dailyReportId, $dailyReportFile->id);
        }

        //remove emails
        Daily_report_email::where('daily_report_id', '=', $dailyReportId)->delete();

        //step 1, 2 and 10
        Daily_report::where('id', '=', $dailyReportId)->delete();
    }

    public function getDailyReportFileByFileId($fileId)
    {
        return Daily_report_file::where('file_id', '=', $fileId)->first();
    }

//    public function getCompanyUserEmails($versionId, $userIds)
//    {
//        return User::whereIn('id', $userIds)
//            ->selectRaw('users.email, users.name,
//                    (select submittals.name from submittals, submittals_versions where submittals.id = submittals_versions.submittal_id
//                        and submittals_versions.id = '.$versionId.') as submittal_name,
//                    (select projects.name from submittals, submittals_versions, projects where submittals.id = submittals_versions.submittal_id
//                        and projects.id = submittals.proj_id and submittals_versions.id = '.$versionId.') as project_name,
//                    (select projects.id from submittals, submittals_versions, projects where submittals.id = submittals_versions.submittal_id
//                        and projects.id = submittals.proj_id and submittals_versions.id = '.$versionId.') as project_id')
//            ->where('comp_id', '=', Auth::user()->comp_id)
//            ->get();
//    }

    //edit
    public function getDailyReport($dailyReportId)
    {
        return Daily_report::where('id','=',$dailyReportId)
            ->with('contractors')
            ->with('contractorTrades')
            ->with('subcontractors')
            ->with('equipments')
            ->with('materials')
            ->with('inspections')
            ->with('observations')
            ->with('instructions')
            ->with('files')
            ->with('countImages')
            ->with('countPdfs')
            ->with('emailedUsers')
            ->with('emailedUsers.user')
            ->with('emailedUsers.user.company')
            ->with('emailedContracts')
            ->with('emailedContracts.contract')
            ->with('emailedContracts.contract.addressBook')
            ->with('user')
            ->with('generatedFile')
            ->with('generatedFile.projectFile')
            ->firstOrFail();
    }

    //email
    public function insertEmailedNotifications($dailyReportId, $projectId, $fileId, $userIds, $isAbContract = false)
    {
//        $dailyReportEmails = Daily_report_email::where('daily_report_id','=',$dailyReportId)->firstOrFail();
//
//        //delete previous users
//        foreach ($dailyReportEmails as $dailyReportEmail) {
//            $dailyReportEmail->delete();
//        }

        //insert new users
        if(count($userIds) > 0)
        {
            foreach ($userIds as $userId) {
                Daily_report_email::create([
                    'daily_report_id' => $dailyReportId,
                    'proj_id' => $projectId,
                    'comp_id' => $this->companyId,
                    'user_id' => $userId,
                    'is_ab_contact' => (!empty($isAbContract)) ? 1 : 0,
                    'emailed' => 1
                ]);
            }
        }

        //changes emailed flag to true
        $fileRow = $this->getDailyReportFileByFileId($fileId);

        if($fileRow) {
            $fileRow->emailed = 1;
            $fileRow->save();
        }
    }

    //index - ProjectFilesController
    public function getDailyReportByProjectId($projectId, $sort = 'daily_reports.report_num', $order = 'DESC', $paginate = 10)
    {
        return Daily_report::where('proj_id','=',$projectId)
            ->where('comp_id', '=', $this->companyId)
            ->with('user')
            ->with('generatedFile')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /*********************************************
     * Copy last version
     *********************************************/

    //get last version id
    public function getLastVersionByProjectId($projectId)
    {
        return Daily_report::where('proj_id','=',$projectId)
            ->where('comp_id', '=', $this->companyId)
            ->orderBy('id','DESC')
            ->with('contractors')
            ->with('contractorTrades')
            ->with('subcontractors')
            ->with('equipments')
            ->with('materials')
            ->with('inspections')
            ->with('observations')
            ->with('instructions')
            ->with('files')
            ->with('emailedUsers')
            ->with('emailedUsers.user')
            ->with('emailedUsers.user.company')
            ->with('emailedContracts')
            ->with('emailedContracts.contract')
            ->with('emailedContracts.contract.addressBook')
            ->with('user')
            ->with('generatedFile')
            ->with('generatedFile.projectFile')
            ->first();
    }
}