<?php

namespace App\Modules\Daily_reports\Implementations;


use App\Models\File_type;
use App\Models\Project_file;
use App\Modules\Daily_reports\Repositories\DailyReportsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Services\CmPdfManage;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class DailyReport {

    private $limit;
    private $dailyReportsRepository;
    private $filesRepository;

    public function __construct()
    {
        $this->limit = new DataTransferLimitation();
        $this->dailyReportsRepository = new DailyReportsRepository();
        $this->filesRepository = new ProjectFilesRepository();
        $this->projectRepository = new ProjectRepository();
        $this->manageUsersRepository = new ManageUsersRepository();
    }


    /**
     * Store file in db
     * @param $dailyReportId
     * @param $projectId
     * @param string $addDocuments
     * @return mixed
     */
    public function databaseValues($dailyReportId, $projectId, $addDocuments = "false")
    {
        //get project
        $response['project'] = $this->projectRepository->getProject($projectId);

        //get daily report
        $response['daily_report'] = $this->dailyReportsRepository->getDailyReport($dailyReportId);

        $response['my_company'] = $this->projectRepository->getProjectWithUserPermissions($projectId);
        $projectPermissionUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectId);

        //users with privileges from project permissions
        $projectPermissionUsersArray = [];
        if(count($projectPermissionUsers) > 0)
            foreach ($projectPermissionUsers as $projectPermissionUser){
                $projectPermissionUsersArray[] = $projectPermissionUser->user_id;
            }

        $response['users'] = [];
        $response['project_companies'] = [];
        //my company on first option
        $response['project_companies'][$response['my_company']->company->id] = $response['my_company']->company->name;
        foreach ($response['project']->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $response['users'][$subContractor->id] = $subContractor->project_company_contacts;
                $response['project_companies'][$subContractor->id] = $subContractor->address_book->name;
            }
        }

        $users = $this->manageUsersRepository->getCompanyUsers(Auth::user()->comp_id);
        $response['worker_offices'] = [];
        $response['worker_users'] = [];
        //current logged user
        $response['worker_users'][$response['my_company']->company->currentUser->id] = $response['my_company']->company->currentUser->name;
        foreach ($users as $user) {
            $response['worker_offices'][$user->address_id] = $user->address->office_title;
            if(in_array($user->id, $projectPermissionUsersArray)) {
                $response['worker_users'][$user->id] = $user->name;
            }
        }

        $response['addDocuments'] = $addDocuments;
        $response['name'] = Config::get('constants.generate_daily_report_name');
        $response['type'] = Config::get('constants.generate_daily_report_type');
        $response['view'] = Config::get('constants.generate_daily_report_view');

        $prepareName = str_replace('-', '_', str_replace(' ', '_', $response['name'])); // Replaces all spaces with hyphens.
        $cleanName = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareName); // Removes special chars.

        //file name
        //Office Building_Daily Report_0001_08.06.18_Monday.pdf
        $response['fileName'] = $response['project']->name .'_Daily Report_'. $response['daily_report']->report_num .'_'.
            Carbon::parse($response['daily_report']->date)->format('m_d_Y') .'_'.
            Config::get('constants.carbon_days.'.Carbon::parse($response['daily_report']->date)->dayOfWeek).'.pdf';

        //get previous file
        $previousFile = $this->dailyReportsRepository->getGeneratedReportFileByDailyReportId($dailyReportId);

        if($previousFile) {
            //delete previous generated file
            $this->dailyReportsRepository->deleteGeneratedReportFile($previousFile);
        }

        //create daily report Pdf file
        $fileData = [
            'file_type_id' => Config::get('constants.daily_report_file_type_id'),
            'name' => 'Daily Report',
            'file_name' => $response['fileName'],
            'file_mime_type' => 'application/pdf'
        ];
        $response['file'] = $this->dailyReportsRepository->createDailyReportFile($projectId, $dailyReportId, $fileData);

        //get file type
        $response['fileType'] = File_type::where('display_name', '=', Config::get('constants.daily_report_file_type'))->firstOrFail();

        return $response;
    }

    /**
     * Generate file
     * @param $view
     * @param $data
     * @param $params
     * @return mixed
     */
    public function generate($view, $data, $params)
    {
//        //file name
        $fileName = $data['fileName'];

        //upload file locally
        $savePdf = App::make('dompdf.wrapper');
//        $savePdf->setPaper('A4', 'landscape');
        $pdf = $savePdf->loadView($view, $data);
        $savedFile = $pdf->save(storage_path().'/app/'.$fileName);

        if ($savedFile) {
            //get generated file size
            $size = Storage::size($fileName);

            //check upload limit allowance
//            if ($this->limit->checkUploadTransferAllowance($size)) {
                //upload transmittal on s3
                $contents = Storage::disk('local')->get($fileName);
                $s3 = Storage::disk('s3');
                $fileUrl = 'company_' . Auth::user()->comp_id . '/project_'. $params['projectId'] .'/daily_report/' . $params['dailyReportId'] .'/report/'.$fileName;
                $s3Response = $s3->put($fileUrl, $contents);
                if ($s3Response) {
                    //update uploaded file size
                    Project_file::where('id','=',$params['fileId'])->update([
                        'size' => $size
                    ]);

                    //increase upload data transfer limitation
//                    $this->limit->increaseUploadTransfer($size);

                    //delete file locally if it exists
                    if (Storage::disk('local')->exists($fileName)) {
                        Storage::disk('local')->delete($fileName);
                    }

                    return Response::json(array('generate' => 1, 'message' => 'Successful', 'fileId' => $params['fileId'], 'url' => $fileUrl));
                }
//            } else {
//                return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.upload_error', ['item' => 'Transmittal'])));
//            }

            //delete file locally if it exists
//            if (Storage::disk('local')->exists($fileName)) {
//                Storage::disk('local')->delete($fileName);
//            }
//
//            return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.download_error', ['item' => 'Transmittal'])));
        }

        return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
    }

    /**
     * Generate file
     * @param $view
     * @param $data
     * @param $params
     * @return mixed
     */
    public function mergeAndGenerateReport($view, $data, $params)
    {
        try {
            //file name
            $fileName = $data['fileName'];

            $folderTemp = storage_path() . '/app/' . 'temp';
            if (!File::exists($folderTemp)) {
                File::makeDirectory($folderTemp);
            }

            $folder = $folderTemp . '/user' . Auth::user()->id;
            if (!File::exists($folder)) {
                File::makeDirectory($folder);
            } else {
                File::cleanDirectory($folder);
            }

            $mergePdfStorePath = null;
            $pathArray = [];
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadView($view, $data);
            $pdfStorePath = 'temp/user' . Auth::user()->id . '/' . 'original-' . $fileName;
            $savedFile = $pdf->save(storage_path() . '/app/' . $pdfStorePath);
            $pathArray[] = $pdfStorePath;

            if ($savedFile) {
                $mergedPdf = null;

                if ($data['addDocuments'] == "true") {
                    $pdfDocuments = $this->dailyReportsRepository->getDailyReportFileByDailyReportId($params['dailyReportId'], Config::get('constants.daily_report_pdf_file_type_id'));

                    if(count($pdfDocuments) > 0) {
                        foreach ($pdfDocuments as $document) {
                            $path = 'company_' . $document->comp_id . '/project_' . $document->proj_id . '/daily_report/' . $document->daily_report_id . '/pdf/' . $document->file_name;
                            $file = Storage::disk('s3')->get($path);
                            $storePath = 'temp/user' . Auth::user()->id . '/' . 'pdf-' . $document->file_name;
                            Storage::disk('local')->put($storePath, $file);
                            $pathArray[] = $storePath;
                        }

                        $pdf = new CmPdfManage();
                        foreach ($pathArray as $pathPdf) {
                            $pdf->addPDF(storage_path() . '/app/' . $pathPdf, 'all');
                        }

                        $mergePdfStorePath = 'temp/user' . Auth::user()->id . '/' . 'merged-' . $fileName;
                        $saveMergeFile = storage_path() . '/app/' . $mergePdfStorePath;
                        //TODO - if the pdf files are not compatible, the merge will crash: TCPDF_PARSER ERROR: decodeFilterFlateDecode: invalid code
                        $pdf->merge('file', $saveMergeFile);
                        $pathArray[] = $mergePdfStorePath;
                    }
                }

                if ($mergePdfStorePath) {
                    $pdfStorePath = $mergePdfStorePath;
                }

                $size = Storage::size($pdfStorePath);

                //upload Report on s3
                $contents = Storage::disk('local')->get($pdfStorePath);
                $s3 = Storage::disk('s3');
                $fileUrl = 'company_' . Auth::user()->comp_id . '/project_' . $params['projectId'] . '/daily_report/' . $params['dailyReportId'] . '/report/' . $fileName;
                $s3Response = $s3->put($fileUrl, $contents);
                if ($s3Response) {
                    //update uploaded file size
                    Project_file::where('id', '=', $params['fileId'])->update([
                        'size' => $size
                    ]);

                    //delete files locally if it exists
                    foreach ($pathArray as $pathPdf) {
                        if (Storage::disk('local')->exists($pathPdf)) {
                            Storage::disk('local')->delete($pathPdf);
                        }
                    }
                    return Response::json(array('generate' => 1, 'message' => 'Successful', 'fileId' => $params['fileId'], 'url' => $fileUrl));
                }
            }
            return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
        } catch (\Exception $e) {
            Log::debug($e);
            return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
        }
    }

    public function copyLastReport($projectId)
    {
        $lastDailyReport = $this->dailyReportsRepository->getLastVersionByProjectId($projectId);

        if($lastDailyReport) {
            //prepare for coping last report
            $lastDailyReport->copied_report_id = $lastDailyReport->id;
            $lastDailyReport->report_num = $this->dailyReportsRepository->getGeneratedNumbers($projectId);
            $lastDailyReport->date = Carbon::today();
            $lastDailyReport->is_uploaded = 0;

            //step 1 & step 2 & step 10
            $newDailyReport = $this->dailyReportsRepository->storeGeneralFullInfo($projectId, $lastDailyReport);

            //step 3 tab 1
            if(count($lastDailyReport->contractors) > 0 ) {
//                $array = json_decode(json_encode($newDailyReport->contractors), True);
                foreach ($lastDailyReport->contractors as $contractor) {
                    $this->dailyReportsRepository->storeContractorLabor($projectId, $newDailyReport->id, $contractor);
                }
            }
            //step 3 tab 2
            if(count($lastDailyReport->contractorTrades) > 0 ) {
                foreach ($lastDailyReport->contractorTrades as $contractorTrade) {
                    $this->dailyReportsRepository->storeContractorTradeLabor($projectId, $newDailyReport->id, $contractorTrade);
                }
            }
            //step 4
            if(count($lastDailyReport->subcontractors) > 0 ) {
                foreach ($lastDailyReport->subcontractors as $subcontractor) {
                    $this->dailyReportsRepository->storeSubContractorLabor($projectId, $newDailyReport->id, $subcontractor);
                }
            }
            //step 5
            if(count($lastDailyReport->equipments) > 0 ) {
                foreach ($lastDailyReport->equipments as $equipment) {
                    $this->dailyReportsRepository->storeEquipmentOnSite($projectId, $newDailyReport->id, $equipment);
                }
            }
            //step 6
            if(count($lastDailyReport->materials) > 0 ) {
                foreach ($lastDailyReport->materials as $material) {
                    $this->dailyReportsRepository->storeConstructionMaterials($projectId, $newDailyReport->id, $material);
                }
            }
            //step 7
            if(count($lastDailyReport->inspections) > 0 ) {
                foreach ($lastDailyReport->inspections as $inspection) {
                    $this->dailyReportsRepository->storeInspections($projectId, $newDailyReport->id, $inspection);
                }
            }
            //step 8
            if(count($lastDailyReport->observations) > 0 ) {
                foreach ($lastDailyReport->observations as $observation) {
                    $this->dailyReportsRepository->storeObservations($projectId, $newDailyReport->id, $observation);
                }
            }
            //step 9
            if(count($lastDailyReport->instructions) > 0 ) {
                foreach ($lastDailyReport->instructions as $instruction) {
                    $this->dailyReportsRepository->storeVerbalInstructions($projectId, $newDailyReport->id, $instruction);
                }
            }

            return $newDailyReport;
        }
        return false;
    }

}