<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/7/2015
 * Time: 5:50 PM
 */
namespace App\Modules\Manage_users\Implementations;

use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use Illuminate\Support\Facades\Auth;

class StoreCompanyUserWrapper {

    private $repo;
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    public function __construct(CompaniesRepository $companiesRepository)
    {
        //instance from the manage users repository class
        $this->repo = new ManageUsersRepository();
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * Create new company user
     * @param $companyId
     * @param $data
     * @return bool|string|static
     */
    public function createCompanyUser($companyId, $data)
    {
        $company = $this->companiesRepository->getCompany($companyId);
        //Create some initial random password
        $data['password'] = str_random(10);

        //Store company user
        $createdUser = $this->repo->storeCompanyUser($data);

        //Send email array
        $sendMailArray = [
            'email' => $createdUser->email,
            'name' => $createdUser->name,
            'password' => $data['password'],
            'companyName' => $company->name
        ];

        $this->repo->sendRegisteredCompanyUserEmail($sendMailArray);

        return $createdUser;
    }

}