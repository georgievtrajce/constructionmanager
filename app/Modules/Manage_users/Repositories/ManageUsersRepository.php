<?php
namespace App\Modules\Manage_users\Repositories;

use App\Models\Address;
use App\Models\AssignedRoles;
use App\Models\File_type;
use App\Models\Project;
use App\Models\Module;
use App\Models\Project_permission;
use App\Models\Project_permission_user;
use App\Models\Role;
use App\Models\User;
use App\Models\User_permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/2/2015
 * Time: 4:50 PM
 */


class ManageUsersRepository {

    private $companyId;
    private $userId;

    public function __construct()
    {
        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }

    }

    /**
     * Get all system modules
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllModules()
    {
        return Module::all();
    }


    /**
     * Get specific company user
     * @param $id
     * @return mixed
     */
    public function getUser($id)
    {
        return User::where('id', '=', $id)->firstOrFail();
    }

    /**
     * Get all project modules
     * @return mixed
     */
    public function getProjectModules()
    {
        return Module::where('display_name', '!=', 'address-book')->where('display_name', '!=', 'all-projects-modules')->get();
    }

    /**
     * Get all global application modules
     * @return mixed
     */
    public function getGlobalModules()
    {
        return Module::where('display_name', '=', 'address-book')->where('display_name', '=', 'all-projects-modules')->get();
    }

    /**
     * Get all project file types
     * @return mixed
     */
    public function getProjectFileTypes()
    {
        return File_type::where('name','!=','Submittals')->where('name','!=','Contracts')->where('name','!=','Bids')->where('name','!=','RFIs')->where('name','!=','PCOs')->get();
    }

    /**
     * Store company user permission specified by user id
     * @param $userId
     * @return bool
     */
    public function storeUserPermissions($userId)
    {
        //get modules from modules table
        $modules = Module::where('display_name', '=', 'address-book')->orWhere('display_name', '=', 'all-projects-modules')->get();

        //set all user permissions to 0
        //set modules permissions to 0
        foreach($modules as $module) {
            User_permission::firstOrCreate([
                'user_id' => $userId,
                'read' => 0,
                'write' => 0,
                'delete' => 0,
                'entity_id' => $module->id,
                'entity_type' => Config::get('constants.entity_type.module') //this means that the type of the permission is module
            ]);
        }

        return true;
    }

    /**
     * Get company user permission specified by user id
     * @param $userId
     * @return bool
     */
    public function getUserPermissionsAllProjectsAndModules($userId)
    {
        return User_permission::join('modules', 'user_permissions.entity_id', '=', 'modules.id')
            ->where('display_name', '=', 'all-projects-modules')
            ->where('user_id', '=', $userId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->first();
    }

    public function storeUserProjectPermissions($userId)
    {
        //get modules from modules table
        $modules = $this->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $this->getProjectFileTypes();

        //set all user permissions to 0
        //set modules permissions to 0
        //$projects = Project::where('comp_id', '=', Auth::user()->comp_id)->get();
        $projects = Project::where(function ($query) {
                $query->whereHas('projectSubcontractors', function($query) {

                    $query->where('comp_child_id', '=', Auth::user()->comp_id)
                        ->whereIn('status', [Config::get('constants.projects.shared_status.accepted'), Config::get('constants.projects.shared_status.unshared')]);
                })
                ->orWhere('comp_id', '=', Auth::user()->comp_id);
            })
            ->get();

        //dd($projects);
        foreach ($projects as $project)
        {
            foreach($modules as $module) {
                Project_permission::firstOrCreate([
                    'user_id' => $userId,
                    'read' => 0,
                    'write' => 0,
                    'delete' => 0,
                    'entity_id' => $module->id,
                    'entity_type' => Config::get('constants.entity_type.module'), //this means that the type of the permission is module
                    'proj_id' => $project->id
                ]);
            }

            //set project files permissions to 0
            foreach($projectFiles as $projectFile) {
                Project_permission::firstOrCreate([
                    'user_id' => $userId,
                    'read' => 0,
                    'write' => 0,
                    'delete' => 0,
                    'entity_id' => $projectFile->id,
                    'entity_type' => 2, //this means that the type of the permission is project file
                    'proj_id' => $project->id
                ]);
            }
        }

        return true;
    }

    /**
     * Count registered company users for concrete company
     * @param $companyId
     * @return mixed
     */
    public function countRegisteredCompanyUsers($companyId)
    {
        return User::whereHas('roles', function($q)
        {
            $q->where('name', '=', 'Company User');

        })->where('comp_id','=',$companyId)->where('active','=',1)->orderBy('id','DESC')->count();
    }

    /**
     * Get all company users
     * @param $companyId
     * @param int $paginate
     * @return mixed
     */
    public function getCompanyUsers($companyId, $paginate = 10)
    {
        $users = User::whereHas('roles', function($q)
        {
            $q->whereIn('name',['Company User','Project Admin']);
        })
        ->with('address')
        ->where('comp_id','=',$companyId)
        ->where('active','=',1)
        ->orderBy('id','DESC')
        ->paginate($paginate);

        //dd($users);
        return $users;
    }

    /**
     * Get all company users
     * @param $companyId
     * @return mixed
     */
    public function getAllCompanyUsers($companyId)
    {
        return User::whereHas('roles', function($q)
        {
            $q->where('name', '!=', 'Company Admin');

        })->where('comp_id','=',$companyId)->where('active','=',1)->orderBy('id','DESC')->get();
    }

    /**
     * Get all company users and project admins for the specified company
     * @param $companyId
     * @return mixed
     */
    public function getCompanyUsersAndProjectAdmins($companyId)
    {
        return User::where(function($query) use ($companyId) {
                $query->whereHas('roles', function($q)
                {
                    $q->where('name', '=', 'Company User');

                })->where('comp_id','=',$companyId)->where('active','=',1);
            })
            ->orWhere(function ($query) use ($companyId) {
                $query->whereHas('roles', function($q)
                {
                    $q->where('name', '=', 'Project Admin');

                })->where('comp_id','=',$companyId)->where('active','=',1);
            })
            ->orderBy('id','DESC')
            ->get();
    }

    /**
     * Get concrete company user
     * @param $id
     * @return mixed
     */
    public function getCompanyUser($id)
    {
        return User::where('id','=',$id)
            ->where('comp_id','=',$this->companyId)
            ->where('active','=',1)
            ->with('address')
            ->with('permissions')
            ->with('permissions.module')
            ->with('permissions.project_file')
            ->firstOrFail();
    }

    /**
     * Get company user role
     * @return mixed
     */
    public function getCompanyUserRole()
    {
        return Role::where('name','=','Company User')->first();
    }

    /**
     * Get company admin role
     * @return mixed
     */
    public function getCompanyAdminRole()
    {
        return Role::where('name','=','Company Admin')->first();
    }

    public function getProjectAdminRole()
    {
        return Role::where('name','=','Project Admin')->first();
    }

    /**
     * Get company address
     * @param $companyId
     * @return mixed
     */
    public function getCompanyAddresses($companyId)
    {
        return Address::where('comp_id','=',$companyId)->get();
    }

    /**
     * Update user permission
     * @param $userId
     * @param $entityId
     * @param $readPermission
     * @param $writePermission
     * @param $deletePermission
     * @param $entityType
     * @return mixed
     */
    public function updateUserPermissions($userId, $entityId, $readPermission, $writePermission, $deletePermission, $entityType)
    {
        return User_permission::where('user_id','=',$userId)
            ->where('entity_id','=',$entityId)
            ->where('entity_type','=',$entityType)
            ->update([
                'read' => $readPermission,
                'write' => $writePermission,
                'delete' => $deletePermission
            ]);
    }

    /**
     * Store company user
     * @param $data
     * @return bool|static
     */
    public function storeCompanyUser($data)
    {
        //create user
        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'name' => $data['name'],
            'title' => $data['title'],
            'address_id' => $data['user_address_office'],
            'office_phone' => $data['office_phone'],
            'cell_phone' => $data['cell_phone'],
            'fax' => $data['fax'],
            'comp_id' => $this->companyId,
            'active' => 1,
            'confirmed' => 1,
        ]);

        //set company user an administrator role if it's checked
        $companyUserRole = $this->getCompanyUserRole();
        //role attach alias
        $user->attachRole($companyUserRole);
        $userId = $user->id;

        //store company user permissions
        $this->storeUserPermissions($userId);
        $this->storeUserProjectPermissions($userId);

        return $user;
    }

    /**
     * Send email for company admin registration
     * @param $data
     */
    public function sendRegisteredCompanyAdminEmail($data)
    {
        Mail::send('emails.company_admin_register', $data, function($message) use ($data)
        {
            $message->to($data['email'], $data['name'])->subject('Cloud PM - Company Admin Replacement');
        });
    }

    /**
     * Send email for changing company admin
     * @param $data
     */
    public function sendChangeCompanyAdminEmail($data)
    {
        Mail::send('emails.company_admin_replacement', $data, function($message) use ($data)
        {
            $message->to($data['email'], $data['name'])->subject('Cloud PM - Company Admin Replacement');
        });
    }

    /**
     * Send email for company user registration
     * @param $data
     */
    public function sendRegisteredCompanyUserEmail($data)
    {
        Mail::send('emails.company_user_registration', $data, function($message) use ($data)
        {
            $message->to($data['email'], $data['name'])->subject('Cloud PM - Company User Registration');
        });
    }

    /**
     * Update concrete company user
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateCompanyUser($id, $data)
    {
        //update specified user details
        $user = User::where('id','=',$id)->where('comp_id','=',$this->companyId)->firstOrFail();
        $user->email = $data['email'];
        $user->name = $data['name'];
        $user->title = $data['title'];
        $user->address_id = $data['user_address_office'];
        $user->office_phone = $data['office_phone'];
        $user->cell_phone = $data['cell_phone'];
        $user->fax = $data['fax'];
        $user->save();
        return $user;
    }

    /**
     * Delete company user
     * @param $id
     * @return bool
     */
    public function deleteCompanyUser($id)
    {
        $user = User::where('id', '=', $id)->firstOrFail();
        return ($user->delete());
    }

    public function deleteProjectUserPermissions($userId, $projectId)
    {
        $projectUserPermissions = Project_permission::where('user_id','=',$userId)
            ->where('proj_id','=',$projectId)
            ->get();

        if (count($projectUserPermissions)) {
            foreach ($projectUserPermissions as $permission) {
                $permission->delete();
            }
        }

        return true;
    }

    public function updateCompanyUserAdminPermissions($user, $data)
    {
        if (isset($data['company_admin'])) {
            $companyAdminRole = $this->getCompanyAdminRole();
            $companyUserRole = $this->getCompanyUserRole();
            $loggedInUser = User::find(Auth::user()->id);

            //detach current company admin role
            $loggedInUser->roles()->detach($companyAdminRole);

            //detach current company user role
            $user->roles()->detach($companyUserRole);

            //attach company user role to the logged in user
            $loggedInUser->attachRole($companyUserRole);

            //attach company admin role to the current stored user
            $user->attachRole($companyAdminRole);

            //delete users permissions from user permissions
            //User_permission::where('user_id','=',$user->id)->delete();
            //Project_permission::where('user_id', '=', $user->id)->delete();

            //add permissions for the changed company admin who becomes user
            $this->storeUserPermissions($user->id);
            $this->storeUserProjectPermissions($user->id);

        } else if (isset($data['project_admin'])) {
            $projectAdminRole = $this->getProjectAdminRole();
            AssignedRoles::where('user_id','=',$user->id)
                ->update([
                    'role_id' => $projectAdminRole->id
                ]);
        } else {
            $companyUserRole = $this->getCompanyUserRole();
            AssignedRoles::where('user_id','=',$user->id)
                ->update([
                    'role_id' => $companyUserRole->id
                ]);

            //remove project admin id from projects table
            $projects = Project::where('proj_admin','=',$user->id)
                ->where('comp_id','=',$this->companyId)
                ->get();
            if (count($projects)) {
                foreach ($projects as $project) {
                    $project->proj_admin = null;
                    $project->save();
                }
            }
        }
    }

    public function getProjectPermissionUsers($projectId)
    {
        return Project_permission_user::where('proj_id', '=', $projectId)->get(['user_id']);
    }

    public function storeProjectPermissionUser($projectId, $userIds)
    {
        return Project_permission_user::create([
            'proj_id' => $projectId,
            'user_id' => $userIds
        ]);
    }

    public function deleteProjectPermissionUser($projectId, $userId)
    {
        return Project_permission_user::where('user_id', '=', $userId)
                                      ->where('proj_id', '=', $projectId)
                                      ->delete();
    }

}