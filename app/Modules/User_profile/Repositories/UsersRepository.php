<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/23/2015
 * Time: 3:29 PM
 */
namespace App\Modules\User_profile\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersRepository {

    /**
     * Update user profile data
     * @param $data
     * @return mixed
     */
    public function updateUserProfile($data)
    {
        $content = [
            'address_id' => isset($data['user_address_office']) ? $data['user_address_office'] : NULL,
            'office_phone' => $data['office_phone'],
            'cell_phone' => $data['cell_phone'],
            'fax' => $data['fax'],
        ];

        if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Super Admin')) {
            $content['name'] = $data['name'];
            $content['email'] = $data['email'];
            $content['title'] = $data['title'];
        }

        return User::where('id', '=', Auth::user()->id)->update($content);
    }

    /**
     * Change user profile password
     * @param $data
     * @return mixed
     */
    public function changePassword($data)
    {
        return User::where('id', '=', Auth::user()->id)->update([
            'password' => Hash::make($data['password'])
        ]);
    }

    /**
     * Get last users by company
     * @param $number
     * @return mixed
     */
    public function getLastUsersByCompany($number)
    {
        return User::where('comp_id', '=', Auth::user()->comp_id)
            ->orderBy('created_at', 'desc')
            ->take($number)
            ->get();
    }

    /**
     * Get last users by company
     * @param $number
     * @return mixed
     */
    public function getAllUsersByCompany($companyId)
    {
        return User::where('comp_id', '=', $companyId)->get();
    }

    /**
     * Get user by id
     * @param $id
     * @return mixed
     */
    public function getUserByID($id)
    {
        return User::where('id', '=', $id)->with('company')->firstOrFail();
    }

    /**
     * Get user by email
     * @param $email
     * @return mixed
     */
    public function getUserByEmail($email)
    {
        return User::where('email', '=', $email)->firstOrFail();
    }

    /**
     * Get already registered user by email (needed for checking in invitations)
     * @param $email
     * @return mixed
     */
    public function getRegisteredUserByEmail($email)
    {
        return User::where('email', '=', $email)
            ->with('company')
            ->first();
    }
}