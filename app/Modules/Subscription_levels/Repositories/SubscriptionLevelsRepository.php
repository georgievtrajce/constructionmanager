<?php
namespace App\Modules\Subscription_levels\Repositories;

use App\Models\Subscription_type;
use App\Services\CustomHelper;

class SubscriptionLevelsRepository {

    public function getSubscriptionLevels()
    {
        return Subscription_type::all();
    }

    public function getSubscriptionLevel($subscriptionLevelId)
    {
        return Subscription_type::where('id','=',$subscriptionLevelId)->firstOrFail();
    }

    public function updateSubscriptionLevel($subscriptionLevelId, $data)
    {
        $uploadLimit = CustomHelper::convertInBytes($data['upload_limit'], $data['upload_unit']);
        $downloadLimit = CustomHelper::convertInBytes($data['download_limit'], $data['download_unit']);
        $storageLimit = CustomHelper::convertInBytes($data['storage_limit'], $data['storage_unit']);
        return Subscription_type::where('id','=',$subscriptionLevelId)
            ->update([
                'ref_points_needed' => $data['ref_points_needed'],
                'ref_points' => $data['ref_points'],
                'upload_limit' => $uploadLimit,
                'download_limit' => $downloadLimit,
                'storage_limit' => $storageLimit,
                'amount' => CustomHelper::amountToDouble($data['amount']),
            ]);
    }

}