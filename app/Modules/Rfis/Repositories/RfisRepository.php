<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/28/2015
 * Time: 4:43 PM
 */
namespace App\Modules\Rfis\Repositories;

use App\Models\File_type;
use App\Models\Module;
use App\Models\Project;
use App\Models\Project_company_permission;
use App\Models\Project_file;
use App\Models\Project_subcontractor;
use App\Models\Proposal_supplier_permission;
use App\Models\Rfi;
use App\Models\Rfi_version;
use App\Models\Rfi_status;
use App\Models\Rfi_permission;
use App\Models\Rfi_version_distribution;
use App\Models\Rfi_version_file;
use App\Models\RfisSubcontractorProposalDistribution;
use App\Models\Transmittal;
use App\Models\User;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Services\CustomHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class RfisRepository {

    private $companyId;
    private $userId;
    private $projectSubcontractorsRepo;
    private $projectPermissionsRepository;
    private $rfiModule;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        if (Auth::user()) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $this->rfiModule = File_type::where('display_name', '=', 'rfis')->first();
        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * Get all rfis by project id
     * @param $projectId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllRFIsForProject($projectId, $compId)
    {
        return Rfi::where('proj_id', '=', $projectId)
            ->where('comp_id','=', $compId)
            ->join('rfis_versions','rfis_versions.id','=','last_version')
            ->leftJoin('rfi_statuses', 'rfis_versions.status_id', '=', 'rfi_statuses.id')
            ->with('rfis_versions.file')
            ->with('rfis_versions.smallFile')
            ->with('rfis_versions.transmittalSubmSentFile')
            ->with('rfis_versions.transmittalSentFile')
            ->select(['rfis.*', 'rfi_statuses.short_name as status_short_name'])
            ->get();
    }

    /**
     * Get all project rfis
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getProjectRfis($projectId, $sort, $order, $paginate = 50)
    {
        return Rfi::where('rfis.proj_id', '=', $projectId)
            ->where('rfis.comp_id', '=', $this->companyId)
            ->join('rfis_versions','rfis_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('rfi_statuses', 'status_id', '=', 'rfi_statuses.id')
            ->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status_id',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('rfis_versions')
            ->with('rfis_versions.file')
            ->with('rfis_versions.transmittalSentFile')
            ->with('rfis_versions.transmittalSubmSentFile')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Get shared project rfis
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return bool
     */
    public function getSharedProjectRfis($projectId, $sort, $order, $paginate = 50)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($projectId, $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($projectId, $this->companyId);
        $project = Project::where('id', '=', $projectId)->first();

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $rfiType = Module::where('display_name','=','rfis')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //check companies permissions
        $rfisPermissions = Project_company_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        if (!is_null($rfisPermissions) || !is_null($bidsPermissions)) {
            $sharedRfis = Rfi::where('rfis.proj_id','=',$projectId)
                ->where('rfis.comp_id','=',$project->comp_id);

            if ((!is_null($rfisPermissions) && $rfisPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
                $sharedRfis->join('rfis_versions','rfis_versions.id','=','last_version')
                    ->leftJoin('files', function($leftJoin)
                    {
                        $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                        WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                        ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
                    ->leftJoin('rfi_statuses', 'status_id', '=', 'rfi_statuses.id');
            } else {
                $sharedRfis->rightJoin('rfi_permissions','rfis.id','=','rfi_permissions.rfi_id')
                    ->join('rfis_versions','rfis_versions.id','=','last_version')
                    ->leftJoin('files', function($leftJoin)
                    {
                        $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                        WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                        ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
                    ->leftJoin('rfi_statuses', 'status_id', '=', 'rfi_statuses.id')
                    ->where('rfi_permissions.comp_child_id', '=', $this->companyId);
            }

            return $sharedRfis->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status_id',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
                ->with('rfis_versions')
                ->with('rfis_versions.file')
                ->with('rfis_versions.transmittalSentFile')
                ->with('rfis_versions.transmittalSubmSentFile')
                ->orderBy($sort, $order)
                ->paginate($paginate);
        }
        return null;
    }

    /**
     * Get all created rfis shares with different companies
     * @param $projectId
     * @param $companyType
     * @param $projectCreatorId
     * @param int $paginate
     * @return mixed
     */
    public function getSharedRfis($projectId, $companyType, $projectCreatorId, $paginate = 50)
    {
        //if the logged in company is a subcontractor on the specified project
        if ($companyType == Config::get('constants.company_type.subcontractor')) {
            //get the parent company that has shared this project to the logged in company
            $parentCompany = CustomHelper::getCompanyParentForProject($projectId);

            //check if the parent company has shared any submittals with the logged in company
            if (is_null($parentCompany)) {
                $parentId = 0;
            } else {
                $parentId = $parentCompany->comp_parent_id;
            }

            $creatorCompanyId = $projectCreatorId;
        } else {
            $parentId = $this->companyId;
            $creatorCompanyId = $this->companyId;
        }

        return Rfi::where('rfis.proj_id','=',$projectId)
            ->where('rfis.comp_id','=',$creatorCompanyId)
            ->rightJoin('rfi_permissions','rfi_permissions.rfi_id','=','rfis.id')
            ->where('rfi_permissions.comp_parent_id','=',$parentId)
            ->join('companies','rfi_permissions.comp_child_id','=','companies.id')
            ->join('rfis_versions','rfis_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('rfi_statuses', 'status_id', '=', 'rfi_statuses.id')
            ->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status_id',
                'rfi_permissions.comp_parent_id as comp_parent_id',
                'rfi_permissions.comp_child_id as comp_child_id',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection',
                'companies.name as company_name')
            ->with('rfis_versions')
            ->with('rfis_versions.file')
            ->orderBy('rfis.id','DESC')
            ->paginate($paginate);
    }

    public function getReceivedRfis($projectId, $creatorCompanyId, $sort, $order, $paginate = 50)
    {
        return Rfi::where('rfis.proj_id','=',$projectId)
            ->where('rfis.comp_id','!=',$creatorCompanyId)
            ->where('rfis.comp_id','!=',$this->companyId)
            ->rightJoin('rfi_permissions','rfi_permissions.rfi_id','=','rfis.id')
            //->where('rfi_permissions.comp_parent_id','=',$parentId)
            ->join('companies','rfi_permissions.comp_parent_id','=','companies.id')
            ->join('rfis_versions','rfis_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('rfi_statuses', 'status_id', '=', 'rfi_statuses.id')
            ->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status_id',
                'rfi_permissions.comp_parent_id as comp_parent_id',
                'rfi_permissions.comp_child_id as comp_child_id',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.size as size',
                'files.version_date_connection as version_date_connection',
                'companies.name as company_name')
            ->with('rfis_versions')
            ->with('rfis_versions.file')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Unshare all rfis shares from second level of sharing and further possible shares
     * @param $rfiId
     * @param $shareSubcontractorId
     * @return bool
     * @internal param $submittalId
     * @internal param $fileId
     */
    public function unshareNextLevels($rfiId, $shareSubcontractorId)
    {
        $nextLevelUnshare = Rfi_permission::where('rfi_id','=',$rfiId)
            ->where('comp_parent_id','=',$shareSubcontractorId)
            ->get();
        if(count($nextLevelUnshare) > 0) {
            foreach($nextLevelUnshare as $unshareFile) {
                $nextShareCompanyId = $unshareFile->comp_child_id;
                $unshareFile->delete();

                //recursion until there are still shares of the specified rfi
                $this->unshareNextLevels($rfiId, $nextShareCompanyId);
            }
        }
        return true;
    }

    /**
     * Unshare specified project rfi with specified company (first level of sharing)
     * @param $rfiId
     * @param $shareCompanyId
     * @param $receiveCompanyId
     * @return bool
     * @internal param $submittalId
     * @internal param $fileId
     */
    public function unshareRfi($rfiId, $shareCompanyId, $receiveCompanyId)
    {
        $sharingStep = Rfi_permission::where('rfi_id','=',$rfiId)
            ->where('comp_parent_id','=',$shareCompanyId)
            ->where('comp_child_id','=',$receiveCompanyId)
            ->firstOrFail();

        //do the next levels of unsharing
        $nextLevelUnshare = $this->unshareNextLevels($rfiId, $sharingStep->comp_child_id);

        if($nextLevelUnshare) {
            $sharingStep->delete();
            return true;
        }
        return false;
    }

    /**
     * Get transmittals
     * @param $projectId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getTransmittals($projectId, $sort, $order)
    {
        $fileType = File_type::where('display_name','=','rfis')->firstOrFail();

        return Transmittal::where('file_type_id','=',$fileType->id)
            ->where('transmittals_logs.proj_id','=',$projectId)
            ->join('rfis_versions','rfis_versions.id','=','transmittals_logs.version_id')
            ->leftJoin('rfi_statuses','rfi_statuses.id','=','rfis_versions.status_id')
            ->join('rfis','rfis.id','=','rfis_versions.rfi_id')
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", rfis.sub_id, rfis.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as sent_appr',
                'rfis_versions.subm_sent_sub as subm_sent_sub',
                'rfis.number as number',
                'rfis.subject as subject',
                'rfis.mf_number as mf_number',
                'rfis.mf_title as mf_title',
                'address_book.name as sent_to',
                'rfi_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN rfis_versions.subm_sent_sub ELSE rfis_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Get User emails from my company
     * @param $versionId
     * @return mixed
     */
    public function getCompanyUserEmails($versionId, $userIds)
    {
        return User::whereIn('id', $userIds)
            ->selectRaw('users.email, users.name, 
                    (select rfis.name from rfis, rfis_versions where rfis.id = rfis_versions.rfi_id 
                        and rfis_versions.id = '.$versionId.') as rfi_name, 
                    (select projects.name from rfis, rfis_versions, projects where rfis.id = rfis_versions.rfi_id 
                        and projects.id = rfis.proj_id and rfis_versions.id = '.$versionId.') as project_name, 
                    (select projects.id from rfis, rfis_versions, projects where rfis.id = rfis_versions.rfi_id 
                        and projects.id = rfis.proj_id and rfis_versions.id = '.$versionId.') as project_id')
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->get();
    }

    /**
     * Get Recipient emails selected in the companies module
     * needed for transmittal email notifications
     * @param $id
     * @param $projectId
     * @return mixed
     */
    public function getRfiCompanyEmails($versionId, $contactIds)
    {
        return Rfi_version::join('rfis', 'rfis.id', '=', 'rfis_versions.rfi_id')
            ->join('projects', 'projects.id', '=', 'rfis.proj_id')
            ->join('project_company_contacts', 'project_company_contacts.proj_id', '=', 'projects.id')
            ->leftJoin('project_subcontractors', function($join)
            {
                $join->on('project_company_contacts.proj_comp_id', '=', 'project_subcontractors.proj_comp_id')
                    ->where('project_subcontractors.once_accepted','=',Config::get('constants.projects.once_accepted.yes'))
                    ->where('project_subcontractors.status','=',Config::get('constants.projects.shared_status.accepted'));
            })
            ->join('ab_contacts', 'project_company_contacts.ab_cont_id', '=', 'ab_contacts.id')
            ->leftJoin('address_book', 'ab_contacts.ab_id', '=', 'address_book.id')
            ->where('rfis_versions.id', '=', $versionId)
            ->whereIn('ab_contacts.id', $contactIds)
            ->select(
                'ab_contacts.*',
                'rfis.name as rfi_name',
                'project_subcontractors.comp_child_id as comp_child_id',
                'projects.name as project_name',
                'projects.id as project_id',
                'address_book.name as company_name'
            )
            ->groupBy(['ab_contacts.id'])
            ->get();
    }

    /**
     * Filter transmittal records
     * @param $params
     * @return mixed
     */
    public function filterRfiTransmittals($params)
    {
        $fileType = File_type::where('display_name','=','rfis')->firstOrFail();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'file_type_id' => $fileType->id,
            'transmittals_logs.proj_id' => $params['projectId'],
            'transmittals_logs.comp_id' => $this->companyId
        ];

        //set sorting
        if (isset($params['sort'])) {
            if ($params['sort'] != '') {
                $sort = $params['sort'];
            } else {
                $sort = 'created_at';
            }
        } else {
            $sort = 'created_at';
        }

        //set sorting order
        if (isset($params['order'])) {
            if ($params['order'] != '') {
                $order = $params['order'];
            } else {
                $order = 'asc';
            }
        } else {
            $order = 'asc';
        }

        if($params['sentTo'] != '') {
            $query['address_book.id'] = $params['sentTo'];
        }

        $fullQuery = Transmittal::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
        }

        //Get rfi transmittals by status
        if($status != 'All') {
            $fullQuery->where('rfi_statuses.id','=',$status);
        }

        return $fullQuery->join('rfis_versions','rfis_versions.id','=','transmittals_logs.version_id')
            ->leftJoin('rfi_statuses','rfi_statuses.id','=','rfis_versions.status_id')
            ->join('rfis','rfis.id','=','rfis_versions.rfi_id')
            ->leftJoin('address_book', function($leftJoin) {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", rfis.sub_id, rfis.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as sent_appr',
                'rfis_versions.subm_sent_sub as subm_sent_sub',
                'rfis.number as number',
                'rfis.subject as subject',
                'rfis.mf_number as mf_number',
                'rfis.mf_title as mf_title',
                'address_book.name as sent_to',
                'address_book.id as sent_to_id',
                'rfi_statuses.short_name as version_status',
                'rfi_statuses.id as version_status_id',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN rfis_versions.subm_sent_sub ELSE rfis_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transmittals'));
    }

    public function deleteTransmittalFromS3($projectId, $fileId)
    {
        //get file
        $file = Project_file::where('id','=',$fileId)->firstOrFail();

        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/rfis/transmittals/'.$file->file_name;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    public function getTransmittal($transmittalId)
    {
        return Transmittal::where('id','=',$transmittalId)
            ->with('file')
            ->firstOrFail();
    }

    public function deleteTransmittal($transmittalId)
    {
        $transmittal = Transmittal::where('id','=',$transmittalId)->firstOrFail();

        Project_file::where('id','=',$transmittal->file_id)->delete();

        return $transmittal->delete();
    }

    /**
     * Get all rfis versions statuses from database table
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllRfiStatuses()
    {
        return Rfi_status::all();
    }

    public function RfiTransmittalsReport($params)
    {
        $fileType = File_type::where('display_name','=','rfis')->firstOrFail();

        return Transmittal::where('file_type_id','=',$fileType->id)
            ->where('transmittals_logs.proj_id','=',$params['projectId'])
            ->join('rfis_versions','rfis_versions.id','=','transmittals_logs.version_id')
            ->leftJoin('rfi_statuses','rfi_statuses.id','=','rfis_versions.status_id')
            ->join('rfis','rfis.id','=','rfis_versions.rfi_id')
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", rfis.sub_id, rfis.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as sent_appr',
                'rfis_versions.subm_sent_sub as subm_sent_sub',
                'rfis.number as number',
                'rfis.subject as subject',
                'rfis.mf_number as mf_number',
                'rfis.mf_title as mf_title',
                'address_book.name as sent_to',
                'rfi_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN rfis_versions.subm_sent_sub ELSE rfis_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Filter rfis
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterRfis($params, $paginate = 50)
    {
        $status = $params['status'];
        //Create query conditions
        $query = [
            'rfis.proj_id' => $params['projectId']
        ];

        if($params['subcontractor'] != '') {
            $query['rfis.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Rfi::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('rfis.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('rfis.mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['name'])) {
            $fullQuery->where('rfis.name', 'LIKE', $params['name'].'%');
        }

        //Get rfis by status\
        if($status == 'All') {
            $fullQuery->join('rfis_versions','rfis_versions.id','=','last_version');
        }
        else {
            $fullQuery->rightJoin('rfis_versions', function($join) use ($status)
            {
                $join->on(DB::raw("rfis_versions.id = last_version and rfis_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
            });
        }

        return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'rfis.sub_id', '=', 'address_book.id')
            ->leftJoin('rfi_statuses', 'rfis_versions.status_id', '=', 'rfi_statuses.id')
            ->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('rfis_versions')
            ->with('rfis_versions.file')
            ->orderBy($params['sort'],$params['order'])
            ->paginate($paginate);
    }

    /**
     * Report rfis
     * @param $params
     * @return array
     * @internal param int $perPage
     */
    public function reportRfis($params)
    {
        $status = $params['status'];
        //Create query conditions
        $query = [
            'rfis.proj_id' => $params['projectId']
        ];

        if($params['subcontractor'] != '') {
            $query['rfis.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Rfi::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('rfis.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('rfis.mf_title','LIKE', $params['masterFormat'].'%');
        }

        //Get rfis by status\
        if($status == 'All') {
            $fullQuery->join('rfis_versions','rfis_versions.id','=','last_version');
        }
        else {
            $fullQuery->rightJoin('rfis_versions', function($join) use ($status)
            {
                $join->on(DB::raw("rfis_versions.id = last_version and rfis_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
            });
        }

        return $fullQuery->leftJoin('files', function($leftJoin)
        {
            $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
        })
            ->leftJoin('address_book', 'rfis.sub_id', '=', 'address_book.id')
            ->leftJoin('rfi_statuses', 'rfis_versions.status_id', '=', 'rfi_statuses.id')
            ->select('rfis.*',
                'rfis_versions.id as version_id',
                'rfis_versions.cycle_no as version_cycle_no',
                'rfis_versions.sent_appr as version_sent_appr',
                'rfis_versions.rec_appr as version_rec_appr',
                'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                'rfis_versions.rec_sub as version_rec_sub',
                'rfis_versions.status_id as version_status',
                'rfi_statuses.short_name as version_status_short_name',
                'rfi_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('rfis_versions')
            ->with('last_version_report')
            ->with('rfis_versions.file')
            ->orderBy($params['sort'],$params['order'])
            ->get();
    }

    /**
     * Filter shared rfis
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterSharedRfis($params, $paginate = 50)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'rfis.proj_id' => $params['projectId'],
            'rfis.comp_id' => $project->comp_id
        ];

        if($params['subcontractor'] != '') {
            $query['rfis.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Rfi::where($query);

        //join only shared rfis
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $rfiType = Module::where('display_name','=','rfis')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $rfisPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        if (!is_null($rfisPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($rfisPermissions) && $rfisPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('rfi_permissions', 'rfis.id', '=', 'rfi_permissions.rfi_id')
                    ->where('rfi_permissions.comp_child_id', '=', $this->companyId);
            }

            //master format search field transformation in a proper search segment
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('rfis.mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('rfis.mf_title','LIKE', $params['masterFormat'].'%');
            }

            if (!empty($params['name'])) {
                $fullQuery->where('rfis.name', 'LIKE', $params['name'].'%');
            }

            //Get rfis by status
            if($status == 'All') {
                $fullQuery->join('rfis_versions','rfis_versions.id','=','last_version');
            }
            else {
                $fullQuery->rightJoin('rfis_versions', function($join) use ($status)
                {
                    $join->on(DB::raw("rfis_versions.id = last_version and rfis_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
                });
            }

            return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
                ->leftJoin('address_book', 'rfis.sub_id', '=', 'address_book.id')
                ->leftJoin('rfi_statuses', 'rfis_versions.status_id', '=', 'rfi_statuses.id')
                ->select('rfis.*',
                    'rfis_versions.id as version_id',
                    'rfis_versions.cycle_no as version_cycle_no',
                    'rfis_versions.sent_appr as version_sent_appr',
                    'rfis_versions.rec_appr as version_rec_appr',
                    'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                    'rfis_versions.rec_sub as version_rec_sub',
                    'rfis_versions.status_id as version_status',
                    'rfi_statuses.short_name as version_status_short_name',
                    'rfi_statuses.name as version_status_name',
                    'address_book.name as subcontractor_name',
                    'files.file_name as file_name',
                    'files.id as file_id',
                    'files.version_date_connection as version_date_connection')
                ->with('rfis_versions')
                ->with('rfis_versions.file')
                ->paginate($paginate);
        }

        return null;
    }

    /**
     * Create pdf from shared rfis
     * @param $params
     * @return array
     * @internal param int $perPage
     */
    public function reportSharedRfis($params)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'rfis.proj_id' => $params['projectId'],
            'rfis.comp_id' => $project->comp_id
        ];

        if($params['subcontractor'] != '') {
            $query['rfis.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Rfi::where($query);

        //join only shared rfis
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $rfiType = Module::where('display_name','=','rfis')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $rfisPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$rfiType->id)
            ->first();

        if (!is_null($rfisPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($rfisPermissions) && $rfisPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('rfi_permissions', 'rfis.id', '=', 'rfi_permissions.rfi_id')
                    ->where('rfi_permissions.comp_child_id', '=', $this->companyId);
            }

            //master format search field transformation in a proper search segment
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('rfis.mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('rfis.mf_title','LIKE', $params['masterFormat'].'%');
            }

            //Get rfis by status
            if($status == 'All') {
                $fullQuery->join('rfis_versions','rfis_versions.id','=','last_version');
            }
            else {
                $fullQuery->rightJoin('rfis_versions', function($join) use ($status)
                {
                    $join->on(DB::raw("rfis_versions.id = last_version and rfis_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
                });
            }

            return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select rfi_version_files.file_id as file_id from rfi_version_files
                WHERE rfi_version_files.rfi_version_id = rfis_versions.id
                ORDER BY rfi_version_files.download_priority DESC LIMIT 1)'));
            })
                ->leftJoin('address_book', 'rfis.sub_id', '=', 'address_book.id')
                ->leftJoin('rfi_statuses', 'rfis_versions.status_id', '=', 'rfi_statuses.id')
                ->select('rfis.*',
                    'rfis_versions.id as version_id',
                    'rfis_versions.cycle_no as version_cycle_no',
                    'rfis_versions.sent_appr as version_sent_appr',
                    'rfis_versions.rec_appr as version_rec_appr',
                    'rfis_versions.subm_sent_sub as version_subm_sent_sub',
                    'rfis_versions.rec_sub as version_rec_sub',
                    'rfis_versions.status_id as version_status',
                    'rfi_statuses.short_name as version_status_short_name',
                    'rfi_statuses.name as version_status_name',
                    'address_book.name as subcontractor_name',
                    'files.file_name as file_name',
                    'files.id as file_id',
                    'files.version_date_connection as version_date_connection')
                ->with('rfis_versions')
                ->with('rfis_versions.file')
                ->orderBy($params['sort'],$params['order'])
                ->get();
        }

        return null;
    }

    /**
     * Get rfi version for generation of rfi transmittal
     * @param $versionId
     * @return mixed
     */
    public function getRfiVersionForTransmittal($versionId)
    {
        return Rfi_version::where('id','=',$versionId)
            ->with('rfi')
            ->with('rfi.general_contractor')
            ->with('rfi.subcontractor')
            ->with('rfi.subcontractor_contact')
            ->with('rfi.subcontractor_contact.office')
            ->with('rfi.recipient')
            ->with('rfi.recipient_contact')
            ->with('rfi.recipient_contact.office')
            ->with('rfi.project')
            ->with('rfi.project.company')
            ->with('file')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get rfi status specified by id
     * @param $statusId
     * @return mixed
     */
    public function getStatusById($statusId)
    {
        return Rfi_status::where('id','=',$statusId)->firstOrFail();
    }

    /**
     * Store rfi and initial first version
     * @param $projectId
     * @param $data
     * @return Rfi
     */
    public function storeProjectRfi($projectId, $data)
    {
        $project = Project::where('id', '=', $projectId)->first();

        if (str_pad(($project->rfi_counter+1), 3, '0', STR_PAD_LEFT) == $data['number']) {
            $project->rfi_counter++;
            $project->save();
        }

        //Store rfi
        $rfi = Rfi::create([
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'name' => $data['name'],
            'number' => $data['number'],
            'subject' => $data['subject'],
            'sent_via' => (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'],
            'is_change' => $data['is_change'],
            'mf_number' => $data['master_format_number'],
            'mf_title' => $data['master_format_title'],
            'self_performed' => empty($data['self_performed']) ? 0 : 1,
            'sub_id' => (empty($data['self_performed']) && !empty($data['sub_id'])) ? $data['sub_id'] : 0,
            //'sub_office_id' => !empty($data['sub_office']) ? $data['sub_office'] : 0,
            'sub_contact_id' => !empty($data['sub_contact']) ? $data['sub_contact'] : 0,
            'recipient_id' => !empty($data['recipient_id']) ? $data['recipient_id'] : 0,
            //'recipient_office_id' => !empty($data['recipient_office']) ? $data['recipient_office'] : 0,
            'recipient_contact_id' => !empty($data['recipient_contact']) ? $data['recipient_contact'] : 0,
            'gc_id' => !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0,
            'make_me_gc' => isset($data['make_me_gc']) ? 1 : 0,
            'proj_id' => $projectId,
            'note' => $data['note'],
            'send_notif_subcontractor' => !empty($data['send_notif_subcontractor']) ? 1 : 0,
            'send_notif_recipient' => !empty($data['send_notif_recipient']) ? 1 : 0
        ]);

        //Store rfi version
        $rfiVersion = Rfi_version::create([
            'rfi_id' => $rfi->id,
            'cycle_no' => $data['cycle_no'],
            'sent_appr' => empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d'),
            'rec_appr' => empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d'),
            'subm_sent_sub' => empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d'),
            'rec_sub' => empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d'),
            'status_id' => 8, //default is draft //$data['status'],
            'notes' => ''
        ]);

        //update the last version field with the version id
        $rfi->last_version = $rfiVersion->id;
        if ($data['useCustom'] != 'true') {
            $rfi->cycle_counter++;
        }
        $rfi->save();

        return $rfi;
    }

    /**
     * Store rfi version
     * @param $rfiId
     * @param $data
     * @return static
     */
    public function storeRfiVersion($rfiId, $data)
    {
        $newVersion = Rfi_version::create([
            'rfi_id' => $rfiId,
            'cycle_no' => $data['cycle_no'],
            'sent_appr' => empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d'),
            'rec_appr' => empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d'),
            'subm_sent_sub' => empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d'),
            'rec_sub' => empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d'),
            'status_id' => $data['status'],
        ]);

        $rfiVersionFile = new Rfi_version_file();

        //connect rfi version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $rfiVersionFile::create([
                'rfi_version_id' => $newVersion->id,
                'file_id' => $data['rec_sub_file_id'],
                'download_priority' => 1,
            ]);
        }

        if ($data['sent_appr_file_id'] != '') {
            $rfiVersionFile::create([
                'rfi_version_id' => $newVersion->id,
                'file_id' => $data['sent_appr_file_id'],
                'download_priority' => 2,
            ]);
        }

        if ($data['rec_appr_file_id'] != '') {
            $rfiVersionFile::create([
                'rfi_version_id' => $newVersion->id,
                'file_id' => $data['rec_appr_file_id'],
                'download_priority' => 3,
            ]);
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $rfiVersionFile::create([
                'rfi_version_id' => $newVersion->id,
                'file_id' => $data['subm_sent_sub_file_id'],
                'download_priority' => 4,
            ]);
        }

        $rfi = Rfi::where('id','=',$rfiId)->first();
        $rfi->last_version = $newVersion->id;

        if ($data['useCustom'] != 'true') {
            $rfi->cycle_counter++;
        }

        $rfi->save();

        return $newVersion;
    }

    /**
     * Returns contacts from submittal version
     *
     * @param $versionId
     * @return mixed
     */
    public function getContactsForRfiVersion($versionId, $type)
    {
        return Rfi_version_distribution::where('rfi_version_id', '=', $versionId)
            ->where('type', '=', $type)
            ->get();
    }

    /**
     * Get concrete rfi
     * @param $rfiId
     * @return mixed
     */
    public function getRfi($rfiId)
    {
        return Rfi::where('id','=',$rfiId)
            ->with('subcontractor')
            ->with('subcontractor.contacts')
            ->with('subcontractor_contact')
            ->with('recipient')
            ->with('recipient.contacts')
            ->with('recipient_contact')
            ->with('general_contractor')
            ->with([
                'rfis_versions',
                'rfis_versions.status',
                'rfis_versions.file',
                'rfis_versions.transmittalSentFile',
                'rfis_versions.transmittalSubmSentFile',
                ])
            ->firstOrFail();
    }

    /**
     * Get concrete rfi version
     * @param $versionId
     * @return mixed
     */
    public function getRfiVersion($versionId)
    {
        return Rfi_version::where('id','=',$versionId)
            ->with('rfi.subcontractor')
            ->with('rfi.recipient')
            ->with('rfiVersionApprUsers.users.company')
            ->with('rfiVersionApprUsers.abUsers.addressBook')
            ->with('rfiVersionSubcUsers.users.company')
            ->with('rfiVersionSubcUsers.abUsers.addressBook')
            ->with('transmittalSubmSentFile')
            ->with('transmittalSentFile')
            ->with('file')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get all rfi versions
     * @param $rfiId
     * @return mixed
     */
    public function getRfiVersions($rfiId)
    {
        return Rfi_version::where('rfi_id','=',$rfiId)->with('file')->get();
    }

    /**
     * Update specific rfi
     * @param $rfiId
     * @param $data
     * @return mixed
     */
    public function updateRfi($rfiId, $data)
    {
        $rfi = Rfi::where('id','=',$rfiId)->firstOrFail();
        $rfi->name = $data['name'];
        $rfi->number = $data['number'];
        $rfi->subject = $data['subject'];
        $rfi->sent_via = (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'];
        $rfi->is_change = $data['is_change'];
        $rfi->mf_number = $data['master_format_number'];
        $rfi->mf_title = $data['master_format_title'];
        $rfi->self_performed = empty($data['self_performed']) ? 0 : 1;
        $rfi->sub_id = (empty($data['self_performed']) && !empty($data['sub_id'])) ? $data['sub_id'] : 0;
        $rfi->sub_contact_id = !empty($data['sub_contact']) ? $data['sub_contact'] : 0;
        $rfi->recipient_id = !empty($data['recipient_id']) ? $data['recipient_id'] : 0;
        $rfi->recipient_contact_id = !empty($data['recipient_contact']) ? $data['recipient_contact'] : 0;
        $rfi->gc_id = !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0;
        $rfi->make_me_gc = isset($data['make_me_gc']) ? 1 : 0;
        $rfi->note = $data['note'];
        $rfi->send_notif_subcontractor = !empty($data['send_notif_subcontractor']) ? 1 : 0;
        $rfi->send_notif_recipient = !empty($data['send_notif_recipient']) ? 1 : 0;
        //$rfi->answer = $data['answer'];
        $rfi->save();
        return $rfi;
    }

    public function checkIfFileExist($versionId, $fileDateConnectionString)
    {
        $rfiVersion = Rfi_version::where('id','=',$versionId)
            ->with(array('file' => function($query) use ($fileDateConnectionString)
            {
                $query->where('files.version_date_connection', $fileDateConnectionString);
            }))
            ->first();

        if (!is_null($rfiVersion)) {
            if (count($rfiVersion->file)) {
                return Project_file::where('id','=',$rfiVersion->file[0]->id)->first();
            }
        }

        return null;
    }

    /**
     * Update concrete rfi version
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateRfiVersion($versionId, $data)
    {
        $rfiVersion = Rfi_version::where('id','=',$versionId)->firstOrFail();
        $rfiVersion->cycle_no = $data['cycle_no'];
        $rfiVersion->sent_appr = empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d');
        $rfiVersion->rec_appr = empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d');
        $rfiVersion->subm_sent_sub = empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d');
        $rfiVersion->rec_sub = empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d');
        $rfiVersion->status_id = $data['status'];
        $rfiVersion->save();

        $rfiVersionFile = new Rfi_version_file();

        //connect rfi version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $currentVersionFile = $rfiVersionFile->where('rfi_version_id','=',$rfiVersion->id)
                ->where('file_id','=',$data['rec_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 1
                ]);
            } else {
                $rfiVersionFile::create([
                    'rfi_version_id' => $rfiVersion->id,
                    'file_id' => $data['rec_sub_file_id'],
                    'download_priority' => 1,
                ]);
            }
        }

        if ($data['sent_appr_file_id'] != '') {
            $currentVersionFile = $rfiVersionFile->where('rfi_version_id','=',$rfiVersion->id)
                ->where('file_id','=',$data['sent_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 2
                ]);
            } else {
                $rfiVersionFile::create([
                    'rfi_version_id' => $rfiVersion->id,
                    'file_id' => $data['sent_appr_file_id'],
                    'download_priority' => 2,
                ]);
            }
        }

        if ($data['rec_appr_file_id'] != '') {
            $currentVersionFile = $rfiVersionFile->where('rfi_version_id','=',$rfiVersion->id)
                ->where('file_id','=',$data['rec_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 3
                ]);
            } else {
                $rfiVersionFile::create([
                    'rfi_version_id' => $rfiVersion->id,
                    'file_id' => $data['rec_appr_file_id'],
                    'download_priority' => 3,
                ]);
            }
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $currentVersionFile = $rfiVersionFile->where('rfi_version_id','=',$rfiVersion->id)
                ->where('file_id','=',$data['subm_sent_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 4
                ]);
            } else {
                $rfiVersionFile::create([
                    'rfi_version_id' => $rfiVersion->id,
                    'file_id' => $data['subm_sent_sub_file_id'],
                    'download_priority' => 4,
                ]);
            }
        }

        return $rfiVersion;
    }

    /**
     * Update concrete rfi version with transmittal notes
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateRfiVersionVersionTransmittalNotes($versionId, $data)
    {
        $rfiVersion = Rfi_version::where('id','=',$versionId)->firstOrFail();
        $rfiVersion->question = $data['version_question'];
        $rfiVersion->answer = $data['version_answer'];
        $rfiVersion->save();

        return $rfiVersion;
    }

    /**
     * Delete rfi file from S3
     * @param $projectId
     * @param $filename
     * @return bool
     */
    public function deleteRfiFile($projectId, $filename)
    {
        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/rfis/'.$filename;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Check the specified rfi version by its id if it's the last created version of the specified rfi
     * @param $rfiId
     * @param $versionId
     * @return bool
     */
    public function lastVersionCheck($rfiId, $versionId)
    {
        $currentVersion = Rfi_version::where('rfi_id','=',$rfiId)->orderBy('id','DESC')->firstOrFail();
        if($currentVersion->id == $versionId) {
            return true;
        }
        return false;
    }

    /**
     * Change the last rfi version with another rfi version specified by id as last version of that rfi
     * @param $rfiId
     * @param $versionId
     * @return bool
     */
    public function changeLastVersion($rfiId, $versionId)
    {
        $version = Rfi_version::where('rfi_id','=',$rfiId)->orderBy('id','DESC')->take(1)->skip(1)->firstOrFail();
        $rfi = Rfi::where('id','=',$rfiId)->first();
        $rfi->last_version = $version->id;
        $rfi->save();
        return true;
    }

    /**
     * Delete concrete rfi version
     * @param $versionId
     * @return int
     */
    public function deleteRfiVersion($versionId)
    {
        //get rfi version
        $version = Rfi_version::where('id','=',$versionId)
            ->with('file')
            ->firstOrFail();

        //check if the rfi version has files
        if (count($version->file)) {
            foreach ($version->file as $file) {
                //delete rfi version and file connection
                Rfi_version_file::where('file_id','=',$file->id)
                    ->where('rfi_version_id','=',$versionId)
                    ->delete();

                //delete rfi version file
                Project_file::where('id','=',$file->id)
                    ->delete();
            }
        }

        //delete rfi version
        return $version->delete();
    }

    /**
     * Delete specified rfi with all its versions
     * @param $rfiId
     * @return bool|null
     */
    public function deleteRfi($rfiId)
    {
        $result = Rfi::find($rfiId);
        $versions = Rfi_version::where('rfi_id','=',$result->id)
            ->with('file')
            ->get();
        if (count($versions)) {
            foreach ($versions as $version) {
                if (count($version->file)) {
                    $versionId = $version->id;
                    foreach ($version->file as $file) {
                        Rfi_version_file::where('file_id','=',$file->id)->where('rfi_version_id','=',$versionId)->delete();
                        Project_file::where('id','=',$file->id)->delete();
                    }
                }
            }
            return $result->delete();
        }
        return false;
    }

    /**
     * return all rfi statuses
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSubmittalStatuses()
    {
        return Rfi_status::all();
    }

    public function shareWithAllSubcontractors($projectId, $selectedRfis)
    {
        //get all subcontractors for the project
        $allProjectSubcontractors = Project_subcontractor::where('proj_id','=',$projectId)
            ->where('comp_parent_id','=',$this->companyId)
            ->get();

        if(count($allProjectSubcontractors)) {
            foreach($selectedRfis as $rfi) {
                foreach($allProjectSubcontractors as $subcontractor) {
                    Rfi_permission::firstOrCreate([
                        'rfi_id' => $rfi,
                        'comp_child_id' => $subcontractor->comp_child_id,
                        'comp_parent_id' => $this->companyId
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Share selected files with selected subcontractors or with
     * the general contractor
     * @param $selectedRfis
     * @param $selectedSubcontractors
     * @return bool
     * @internal param $selectedFiles
     */
    public function shareWithSelectedSubcontractors($selectedRfis, $selectedSubcontractors)
    {
        foreach($selectedRfis as $rfi) {
            foreach($selectedSubcontractors as $subcontractor) {
                Rfi_permission::firstOrCreate([
                    'rfi_id' => $rfi,
                    'comp_child_id' => $subcontractor,
                    'comp_parent_id' => $this->companyId
                ]);
            }
        }

        return true;
    }

    /**
     * Delete specific RFI file
     * @param $params
     * @return mixed
     */
    public function deleteSingleRfiFile($params)
    {
        Rfi_version_file::where('file_id','=',$params['fileId'])
            ->delete();

        $result = Project_file::where('id','=',$params['fileId'])
            ->delete();

        //if storage limit bellow 90% reset notification
        $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
            $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
        }

        return $result;
    }

    public function markTransmittalApprSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $rfi = Rfi_version_distribution::firstOrCreate([
                    'rfi_version_id' => $versionId,
                    'user_id' => $userId,
                    'type' => 'appr'
                ]);

                $rfi->created_at = Carbon::now()->toDateString();
                $rfi->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $rfi = Rfi_version_distribution::firstOrCreate([
                    'rfi_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'appr'
                ]);

                $rfi->created_at = Carbon::now()->toDateString();
                $rfi->save();
            }
        }
    }

    public function markTransmittalSubrSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $rfi = Rfi_version_distribution::firstOrCreate([
                    'rfi_version_id' => $versionId,
                    'user_id' => $userId,
                    'type' => 'subc'
                ]);

                $rfi->created_at = Carbon::now()->toDateString();
                $rfi->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $rfi = Rfi_version_distribution::firstOrCreate([
                    'rfi_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'subc'
                ]);

                $rfi->created_at = Carbon::now()->toDateString();
                $rfi->save();
            }
        }
    }

    public function getRfiSubcontractorRecipientUserDistribution($versionId, $subcontractor=false)
    {
        return RfisSubcontractorProposalDistribution::where('version_id', '=', $versionId)
            ->where('subcontractor', '=', $subcontractor)
            ->with('user.company')
            ->get();
    }

    /**
     * Creates an entry for each user where the email is sent
     * @param $versionId
     * @param $userId
     * @param $subcontractor
     * @return RfisSubcontractorProposalDistribution
     */
    public function addProposalUserDistribution($versionId, $userId, $subcontractor)
    {
        $rfiSubcontractorProposal = RfisSubcontractorProposalDistribution::firstOrCreate(
            [
                'version_id' => $versionId,
                'user_id' => $userId,
                'subcontractor' => $subcontractor
            ]);

        $rfiSubcontractorProposal->created_at = Carbon::now()->toDateString();
        $rfiSubcontractorProposal->save();

        return $rfiSubcontractorProposal;
    }


    /** Get number column from rfis table for specific project_id
     * @param $projectId
     */
    public function getGeneratedNumbers($projectId)
    {
        return Rfi::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $this->companyId)
            ->where('user_id', '=', $this->userId)
            ->orderBy('number', 'ASC')
            ->get(['number']);
    }
}