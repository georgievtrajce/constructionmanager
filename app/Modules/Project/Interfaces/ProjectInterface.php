<?php
namespace App\Modules\Project\Interfaces;

interface ProjectInterface
{
    public function generateNumber($numbers, $counterValue);
}