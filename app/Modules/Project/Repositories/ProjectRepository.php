<?php
namespace App\Modules\Project\Repositories;

use App\Models\AssignedRoles;
use App\Models\File_type;
use App\Models\Module;
use App\Models\Project_company_rating;
use App\Models\Project_permission;
use App\Models\Project_permission_user;
use App\Models\Project_subcontractor;
use App\Models\ProjectBatchFile;
use App\Models\Role;
use App\Models\Subcontractor_project_info;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Services\CustomHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Project;
use App\Models\Address;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class ProjectRepository {
    private $projectParams = array('name','number', 'start_date', 'end_date', 'architect_id', 'owner_id', 'comp_id', 'active', 'upc_code', 'price');
    private $addressParams = array('addr_number', 'street', 'zip', 'city', 'state', 'proj_id');

    /**
     * Get created or completed projects
     * @param $active
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllProjects($active, $sort, $order)
    {
        //get all active projects unless $active = 0 to get inactive
        //sort and order provided from input - if not provided default by id, asc
        $projects = Project::where(function ($query) {
            $query->where(function ($subQuery) {
                    $subQuery->where(function ($sq) {
                        $sq->whereHas('projectSubcontractors', function($q) {
                            $q->where('comp_child_id', '=', Auth::user()->comp_id)
                                ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
                        });
                    })
                    ->orWhere(function ($sq) {
                        $sq->whereHas('projectBidders', function($q) {
                            $q->where('invited_comp_id', '=', Auth::user()->comp_id)
                                ->where('once_accepted', '=', 1)
                                ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
                        });
                    });
                })
                ->orWhere('comp_id', '=', Auth::user()->comp_id);
            })
            ->where('active', '=', $active)
            ->with('projectSubcontractorsSelf')
            ->with('projectBiddersSelf')
            ->with('subcontractorAddress')
            ->with('subcontractorProjectInfo')
            ->with('company')
            ->with('subcontractor')
            ->with('proposals.suppliers.project_bidder')
            ->with('address')
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.projects'));
        return $projects;
    }

    /**
     * Get created or completed projects
     * @param $active
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllProjectsForCompany($active, $sort, $order, $writeArray)
    {
        //get all active projects unless $active = 0 to get inactive
        //sort and order provided from input - if not provided default by id, asc
        $projects = Project::with('projectCompanies.project_company_contacts.contact')
                            ->with('projectCompanies.address_book')
                            ->where('comp_id', '=', Auth::user()->comp_id)
                            ->where('active', '=', $active);

        if (((!Auth::user()->hasRole('Company Admin')) && (!Auth::user()->hasRole('Project Admin')))) {
            $projects = $projects->whereIn('id', $writeArray);
        } else if (((!Auth::user()->hasRole('Company Admin')) && (Auth::user()->hasRole('Project Admin')))) {
            $projects = $projects->where(function ($query) use ($writeArray) {
                                $query->where('proj_admin', '=', Auth::user()->id)
                                      ->orWhereIn('id', $writeArray);
                        });
        }

        return $projects->orderBy($sort, $order)
            ->get();
    }

    /**
     * Get last created projects by company (permission dependent)
     * number parameter specifies the number of projects should be get
     * @param $number
     * @return mixed
     */
    public function getLastProjectsByCompany($number)
    {
        $projects = Project::where(function ($query) {
            $query->whereHas('projectSubcontractors', function($query) {

                $query->where('comp_child_id', '=', Auth::user()->comp_id)
                    ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
            })
            ->orWhereHas('projectBidders', function($query) {

                $query->where('invited_comp_id', '=', Auth::user()->comp_id)
                    ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
            })
            ->orWhere('comp_id', '=', Auth::user()->comp_id);
        });

        //if the logged in user is a simple company user
        if (Auth::user()->hasRole('Company User')) {
            //get the projects module id
            $moduleId = Module::where('display_name','=','projects')->pluck('id');

            $projects->whereHas('projectPermissions', function($query) use ($moduleId) {
                $query->where('project_permissions.user_id', '=', Auth::user()->id)
                    ->where('project_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                    ->where('project_permissions.entity_id', '=', $moduleId)
                    ->where('project_permissions.read', '=', Config::get('constants.hasPermission'));
            });
        }

        //if the logged in user is a project admin
        if (Auth::user()->hasRole('Project Admin')) {
            //get the projects module id
            $moduleId = Module::where('display_name','=','projects')->pluck('id');

            $projects->whereHas('projectPermissions', function($query) use ($moduleId) {
                $query->where('project_permissions.user_id', '=', Auth::user()->id)
                    ->where('project_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                    ->where('project_permissions.entity_id', '=', $moduleId)
                    ->where('project_permissions.read', '=', Config::get('constants.hasPermission'));
            })
            ->orWhere('projects.proj_admin', '=', Auth::user()->id);
        }

        return $projects->whereNull('deleted_at')
            ->select('projects.*')
            ->orderBy('created_at', 'desc')
            ->take($number)
            ->get();
    }

    public function getSingleProject($id)
    {
        return Project::where('id','=',$id)
            ->with('company')
            ->first();
    }

    /**
     * Get own or somebody else's project
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function getProject($id)
    {
        $project = Project::where('id', '=', $id)->where('comp_id', '=', Auth::user()->comp_id)
                ->with('address')
                ->with('architect')
                ->with('primeSubcontractor')
                ->with('owner')
                ->with('company')
                ->with('contractorTransmittal')
                ->with('architectTransmittal')
                ->with('primeSubcontractorTransmittal')
                ->with('ownerTransmittal')
                ->with('generalContractor')
                ->with('projectCompanies.address_book')
                ->with('projectCompanies.project_company_contacts.contact')
                ->first();

        if (!$project) {
            $project = Project::where('id', '=', $id)
                ->where(function($query) {
                    $query->where(function($query) {
                        $query->whereHas('projectSubcontractors', function($subQuery) {
                            $subQuery->where('comp_child_id', '=', Auth::user()->comp_id);
                        });
                        })
                        ->orWhere(function ($sq) {
                            $sq->whereHas('projectBidders', function($q) {
                                $q->where('invited_comp_id', '=', Auth::user()->comp_id);
                                $q->where('once_accepted', '=', 1);
                            });
                        });
                })
                ->with('address')
                ->with('subcontractorProjectInfo')
                ->with('subcontractorProjectInfo.architect')
                ->with('subcontractorProjectInfo.owner')
                ->with('subcontractorAddress')
                ->with('architect')
                ->with('owner')
                ->with('company')
                ->with('contractorTransmittal')
                ->with('architectTransmittal')
                ->with('generalContractor')
                ->with('projectCompanies.address_book')
                ->with('projectCompanies.project_company_contacts.contact')
                ->firstOrFail();
        }
        return $project;
    }

    public function getProjectRecord($id)
    {
        return (Project::where('id', '=', $id)->firstOrFail());
    }

    /**
     * Get shared project
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|static
     */

    public function isShared($id)
    {
        $project = Project::where('id', '=', $id)
            ->whereHas('projectSubcontractors', function($query) {
                $query->where('comp_child_id', '=', Auth::user()->comp_id);
            })->first();
        return $project;
    }

    /**
     * Search created and completed projects
     * @param $active
     * @param $searchBy
     * @param $search
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function searchProjects($active, $searchBy, $search, $sort, $order)
    {
        //get all active projects unless $active = 0 to get inactive
        //sort and order provided from input - if not provided default by id, asc

        if ($searchBy == 'location') {
            $projects = Project::whereHas('address', function($query) use($search) {
                $query->where(function($q) use ($search) {
                    $q->where('city', 'LIKE', "$search%")
                        ->orWhere('state', 'LIKE', "$search%")
                        ->orWhere('street', 'LIKE', "$search%")
                        ->orWhere('number', 'LIKE', "$search%")
                        ->orWhere('zip', 'LIKE', "$search%");
                    });
            });
        } else {
            $projects = Project::with('address')->where($searchBy, 'LIKE', "$search%");
        }

        $projects = $projects->where(function ($query) {
                $query->where(function ($subQuery) {
                    $subQuery->where(function ($sq) {
                        $sq->whereHas('projectSubcontractors', function($q) {
                            $q->where('comp_child_id', '=', Auth::user()->comp_id)
                                ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
                        });
                    })
                        ->orWhere(function ($sq) {
                            $sq->whereHas('projectBidders', function($q) {
                                $q->where('invited_comp_id', '=', Auth::user()->comp_id)
                                    ->where('once_accepted', '=', 1)
                                    ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
                            });
                        });
                })
                    ->orWhere('comp_id', '=', Auth::user()->comp_id);
            })
            ->where('active', '=', $active)
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.projects'));

        return $projects;
    }

    /**
     * Search shared projects
     * @param $searchBy
     * @param $search
     * @return mixed
     */
    public function searchSharedProjects($searchBy, $search, $sort, $order)
    {
        //get projects shared with logged in company
        $shared = Project::with('address')->whereHas('projectSubcontractors', function($query) {
            $query->where('comp_child_id', '=', Auth::user()->comp_id);
        })->where('active', '=', Config::get('constants.projects.status.active'));

        if ($searchBy == 'location') {
            $shared = $shared->whereHas('address', function($query) use($search) {
                $query->where('city', 'LIKE', "%$search%")->whereNotNull('city');
                $query->orWhere('state', 'LIKE', "%$search%")->whereNotNull('state');
                $query->orWhere('street', 'LIKE', "%$search%")->whereNotNull('street');
                $query->orWhere('number', 'LIKE', "%$search%")->whereNotNull('number');
                $query->orWhere('zip', 'LIKE', "%$search%")->whereNotNull('zip');
            });
        } else {
           $shared = $shared->where($searchBy, 'LIKE', "%$search%");
         }


        $shared = $shared->orderBy('id', 'asc')
            ->paginate(Config::get('constants.pagination.projects'));
        return $shared;
    }


    /**
     * Get shared projects
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getSharedProjects($sort, $order)
    {
        //get projects shared with logged in company
        $shared = Project::whereHas('projectSubcontractors', function($query) {

            $query->where('comp_child_id', '=', Auth::user()->comp_id)
                    ->where('status', '=', Config::get('constants.projects.shared_status.accepted'));
        })->where('active', '=', Config::get('constants.projects.status.active'))->orderBy($sort, $order)
            ->with('address')
            ->paginate(Config::get('constants.pagination.projects'));
        return $shared;
    }

    /**
     * Check if the logged in company has allowance to create more projects
     * @return bool
     */
    public function checkStoreAllowance()
    {
        //count all companies projects
        $projects = Project::where('comp_id', '=', Auth::user()->comp_id)->count();

        //if the company is under subscription level one it has only one free project
        if(Auth::user()->company->subs_id == Config::get('subscription_levels.level-one')) {
            if($projects < Config::get('subscription_levels.level-one-projects')) {
                return true;
            }
        }

        //if the company is under subscription level two it has only three free projects
        if(Auth::user()->company->subs_id == Config::get('subscription_levels.level-two')) {
            if($projects < Config::get('subscription_levels.level-two-projects')) {
                return true;
            }
        }

        //if the company is under subscription level three there are no limitations
        if(Auth::user()->company->subs_id == Config::get('subscription_levels.level-three')) {
            return true;
        }

        return false;
    }

    /**
     * store project data
     * @param $data
     * @return Project
     */
    public function storeProject($data)
    {
        $projectData = array('name' => $data['name'],
            'number'=> $data['number'],
            'start_date'=>empty($data['start_date']) ? '' : Carbon::parse($data['start_date'])->format('Y-m-d'),
            'end_date'=>empty($data['end_date']) ? '' : Carbon::parse($data['end_date'])->format('Y-m-d'),
            'comp_id'=>Auth::user()->comp_id,
            'creator_id'=>Auth::user()->id,
            'proj_admin'=>Auth::user()->id,
            'active'=> Config::get('constants.projects.status.active'),
            'price'=> CustomHelper::amountToDouble($data['price']),
            'architect_id' => empty($data['architect'])?null:$data['architect_id'],
            'prime_subcontractor_id' => $data['prime_subcontractor_id'],
            'owner_id' => $data['owner_id'],
            'make_me_gc' => empty($data['make_me_gc']) ? 0 : 1,
            'general_contractor_id' => empty($data['general_contractor_id']) ? 0 : $data['general_contractor_id'],
            'architect_transmittal_id' => ($data['transmittal_flag'] == '1') ? (!empty($data['architect_transmittal_id'])) ? $data['architect_transmittal_id']: 0 : 0,
            'prime_subcontractor_transmittal_id' => ($data['transmittal_flag'] == '2') ? (!empty($data['prime_subcontractor_transmittal_id'])) ? $data['prime_subcontractor_transmittal_id']: 0 : 0,
            'owner_transmittal_id' => ($data['transmittal_flag'] == '3') ? (!empty($data['owner_transmittal_id'])) ? $data['owner_transmittal_id']: 0 : 0,
            'contractor_transmittal_id' => ($data['transmittal_flag'] == '0') ? (!empty($data['contractor_transmittal_id'])) ? $data['contractor_transmittal_id']: 0 : 0,
            'master_format_type_id' => $data['master_format_type']
        );

        $project = Project::firstOrCreate($projectData);

        //set correct upc code for project
        $project->upc_code = 'UPC-'.str_pad($project->id, 6, "0", STR_PAD_LEFT);
        $project->update();

        //store address
        $this->storeAddress($project->id, $data);

        /*
         *  ADD INITIAL PERMISSIONS FOR ALL USERS FOR THIS PROJECT
         */
        $manageUsersRepository = new ManageUsersRepository();

        //get modules from modules table
        $modules = $manageUsersRepository->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $manageUsersRepository->getProjectFileTypes();

        //get all company users
        $allCompanyUsers = $manageUsersRepository->getAllCompanyUsers(Auth::user()->comp_id);

        foreach ($allCompanyUsers as $user) {

            $manageUserPermissionsRepository = app(ManageUsersRepository::class);
            $manageUserPermissions = $manageUserPermissionsRepository->getUserPermissionsAllProjectsAndModules($user->id);
            foreach ($modules as $module) {
                $properFileType = File_type::where('display_name','=',$module->display_name)->first();

                Project_permission::firstOrCreate([
                    'user_id' => $user->id,
                    'read' => ($manageUserPermissions->read == 1)?$manageUserPermissions->read:0,
                    'write' => ($manageUserPermissions->write == 1)?$manageUserPermissions->write:0,
                    'delete' => ($manageUserPermissions->delete == 1)?$manageUserPermissions->delete:0,
                    'entity_id' => $module->id,
                    'entity_type' => Config::get('constants.entity_type.module'), //this means that the type of the permission is module
                    'file_type_id' => is_null($properFileType) ? 0 : $properFileType->id, //write concrete file type for this module od project file section (needed for listing the last uploaded files)
                    'proj_id' => $project->id
                ]);

                if ($manageUserPermissions->read == 1 || $manageUserPermissions->write == 1 || $manageUserPermissions->delete == 1) {
                    Project_permission_user::firstOrCreate(['user_id' => $user->id, 'proj_id' => $project->id]);
                }

            }

            //set project files permissions to 0
            foreach ($projectFiles as $projectFile) {
                Project_permission::firstOrCreate([
                    'user_id' => $user->id,
                    'read' => ($manageUserPermissions->read == 1)?$manageUserPermissions->read:0,
                    'write' => ($manageUserPermissions->write == 1)?$manageUserPermissions->write:0,
                    'delete' => ($manageUserPermissions->delete == 1)?$manageUserPermissions->delete:0,
                    'entity_id' => $projectFile->id,
                    'entity_type' => 2, //this means that the type of the permission is project file
                    'file_type_id' => $projectFile->id, //write concrete file type for this module od project file section (needed for listing the last uploaded files)
                    'proj_id' => $project->id
                ]);
            }
        }

        return $project;
    }

    /**
     * Store subcontractor info for a specific project
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeSubcontractorProjectInfo($projectId, $data)
    {
        $project = Project::where('id','=',$projectId)
            ->where(function ($query) {
                $query->whereHas('projectSubcontractors', function($query) {

                    $query->where('comp_child_id', '=', Auth::user()->comp_id)
                        ->where('once_accepted', '=', 1);
                });
            })
            ->orWhere(function ($query) {
                $query->whereHas('projectBidders', function($query) {

                    $query->where('invited_comp_id', '=', Auth::user()->comp_id)
                        ->where('once_accepted', '=', 1);
                });
            })
            ->firstOrFail();

        $projectData = [
            'name' => $data['name'],
            'number'=> $data['number'],
            'start_date'=>empty($data['start_date']) ? '' : Carbon::parse($data['start_date'])->format('Y-m-d'),
            'end_date'=>empty($data['end_date']) ? '' : Carbon::parse($data['end_date'])->format('Y-m-d'),
            'active'=> Config::get('constants.projects.status.active'),
            'price'=> CustomHelper::amountToDouble($data['price']),
            'architect_id' => $data['architect_id'],
            'owner_id' => $data['owner_id'],
            'proj_id' => $projectId,
            'subcontractor_id' => Auth::user()->comp_id,
            'creator_id' => $project->comp_id
        ];

        //store subcontractor project info
        $subcontractorInfo = Subcontractor_project_info::firstOrCreate($projectData);

        //store subcontractor project address
        $this->storeSubcontractorProjectAddress($project->id, $data);

        return $subcontractorInfo;
    }

    /**
     * store address for project
     * @param $id
     * @param $data
     * @return static
     */
    public function storeAddress($id, $data)
    {
        $addressData = array(
            'street' => $data['street'],
            'zip' => $data['zip'],
            'city' => $data['city'],
            'state' => $data['state'],
            'proj_id' => $id,
        );
        $address = Address::firstOrCreate($addressData);
        return $address;
    }

    /**
     * Store subcontractor address for a specific project
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeSubcontractorProjectAddress($projectId, $data)
    {
        $addressData = array(
            'street' => $data['street'],
            'zip' => $data['zip'],
            'city' => $data['city'],
            'state' => $data['state'],
            'proj_id' => $projectId,
            'subcontractor_id' => Auth::user()->comp_id
        );
        $address = Address::firstOrCreate($addressData);
        return $address;
    }

    /**
     * Update project data
     * @param $id
     * @param $data
     * @return bool
     */
    public function updateProject($id, $data)
    {
        $project = Project::where('id', '=', $id)->firstOrFail();

        $project->name = $data['name'];
        $project->price = CustomHelper::amountToDouble($data['price']);
        $project->active = $data['active'];
        $project->number = $data['number'];
        $project->owner_id = $data['owner_id'];
        $project->architect_id = empty($data['architect'])?null:$data['architect_id'];
        $project->prime_subcontractor_id = (empty($data['prime_subcontractor']))?null:$data['prime_subcontractor_id'];
        $project->owner_id = $data['owner_id'];
        $project->start_date = empty($data['start_date']) ? '' : Carbon::parse($data['start_date'])->format('Y-m-d');
        $project->end_date = empty($data['end_date']) ? '' : Carbon::parse($data['end_date'])->format('Y-m-d');
        $project->make_me_gc = empty($data['make_me_gc']) ? 0 : 1;
        $project->general_contractor_id = empty($data['general_contractor_id']) ? 0 : $data['general_contractor_id'];
        $project->architect_transmittal_id = ($data['transmittal_flag'] == '1') ? (!empty($data['architect_transmittal_id'])) ? $data['architect_transmittal_id']: 0 : 0;
        $project->prime_subcontractor_transmittal_id = ($data['transmittal_flag'] == '2') ? (!empty($data['prime_subcontractor_transmittal_id'])) ? $data['prime_subcontractor_transmittal_id']: 0 : 0;
        $project->owner_transmittal_id = ($data['transmittal_flag'] == '3') ? (!empty($data['owner_transmittal_id'])) ? $data['owner_transmittal_id']: 0 : 0;
        $project->contractor_transmittal_id = ($data['transmittal_flag'] == '0') ? (!empty($data['contractor_transmittal_id'])) ? $data['contractor_transmittal_id']: 0 : 0;

        $updateProject = $project->update();

        //update address
        $updateAddress = $this->updateAddress($id, $data);

        if ($updateProject && $updateAddress) {
            return true;
        }
        return false;
    }

    /**
     * Updare subcontractor info for a specific project
     * @param $projectId
     * @param $data
     * @return bool
     */
    public function updateSubcontractorProjectInfo($projectId,$data)
    {
        $project = Project::where('id','=',$projectId)
            ->where(function ($query) {
                $query->whereHas('projectSubcontractors', function($query) {

                    $query->where('comp_child_id', '=', Auth::user()->comp_id)
                        ->where('once_accepted', '=', 1);
                });
            })
            ->orWhere(function ($query) {
                $query->whereHas('projectBidders', function($query) {

                    $query->where('invited_comp_id', '=', Auth::user()->comp_id)
                        ->where('once_accepted', '=', 1);
                });
            })
            ->firstOrFail();

        $projectSubcontractorInfo = Subcontractor_project_info::where('proj_id','=',$projectId)
            ->where('subcontractor_id','=',Auth::user()->comp_id)
            ->where('creator_id','=',$project->comp_id)
            ->firstOrFail();;

        $projectSubcontractorInfo->name = $data['name'];
        $projectSubcontractorInfo->price = CustomHelper::amountToDouble($data['price']);
        $projectSubcontractorInfo->active = $data['active'];
        $projectSubcontractorInfo->number = $data['number'];
        $projectSubcontractorInfo->owner_id = $data['owner_id'];
        $projectSubcontractorInfo->architect_id = $data['architect_id'];
        $projectSubcontractorInfo->start_date = empty($data['start_date']) ? '' : Carbon::parse($data['start_date'])->format('Y-m-d');
        $projectSubcontractorInfo->end_date = empty($data['end_date']) ? '' : Carbon::parse($data['end_date'])->format('Y-m-d');

        $updateSubcontractorProjectInfo = $projectSubcontractorInfo->update();

        //update subcontractor project address
        $updateAddress = $this->updateSubcontractorProjectAddress($projectId, $data);

        if ($updateSubcontractorProjectInfo && $updateAddress) {
            return true;
        }
        return false;
    }

    /**
     * update project address
     * @param $id
     * @param $data
     * @return ProjectRepository
     */
    public function updateAddress($id, $data)
    {
        $address = Address::where('proj_id', '=', $id)
            ->where('subcontractor_id', '=', 0)
            ->first();

        if ($address) {
            foreach ($data as $key => $value){
                foreach ($this->addressParams as $addressParam){
                    if($key == $addressParam){
                        $address->$key = $value;
                    }
                }
            }
            return ($address->update());
        } else {
            return ($this->storeAddress($id, $data));
        }
    }

    /**
     * Update or store subcontractor address for a specific project
     * @param $projectId
     * @param $data
     * @return ProjectRepository
     */
    public function updateSubcontractorProjectAddress($projectId, $data)
    {
        $address = Address::where('proj_id', '=', $projectId)
            ->where('subcontractor_id', '=', Auth::user()->comp_id)
            ->first();

        if ($address) {
            foreach ($data as $key => $value){
                foreach ($this->addressParams as $addressParam){
                    if($key == $addressParam){
                        $address->$key = $value;
                    }
                }
            }
            return ($address->update());
        } else {
            return ($this->storeSubcontractorProjectAddress($projectId, $data));
        }
    }

    /**
     * Change subcontractor's project number
     * @param $projectNumberId
     * @param $projectNumber
     * @return mixed
     */
    public function updateSubcontractorProjectNumber($projectNumberId, $projectNumber)
    {
        $projectSubcontractor = Project_subcontractor::where('id','=',$projectNumberId)->firstOrFail();
        $projectSubcontractor->sub_proj_number = $projectNumber;
        $projectSubcontractor->save();
        return $projectSubcontractor;
    }

    public function getSubcontractorProjectNumber($projectNumberId)
    {
        $projectSubcontractor = Project_subcontractor::where('id','=',$projectNumberId)->firstOrFail();
        return $projectSubcontractor;
    }

    /**
     * delete project
     * @param $id
     * @return mixed
     */
    public function deleteProject($id)
    {
        $project = Project::where('id', '=', $id)->firstOrFail();
        $project->deleted_by = (Auth::user() != null)?Auth::user()->id:null;
        $project->save();
        return ($project->delete());
    }

    /**
     * @param $id
     * @param $active
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllProjectsForSubcontractor($id, $active, $sort, $order)
    {
        //sort and order provided from input - if not provided default by id, asc
        $projects = Project::join('project_companies', 'project_companies.proj_id', '=', 'projects.id')
            ->where('project_companies.ab_id', '=', $id)
            ->where('projects.active', '=', $active)
            ->where('projects.comp_id', '=', Auth::user()->comp_id)
            ->select('projects.*',
                'project_companies.ab_id as ab_id')
            ->with('address')
            ->orderBy('projects.'.$sort, $order)
            ->paginate(Config::get('constants.pagination.projects'));

        return $projects;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAllProjectsForContact($id)
    {
        $projects = Project::join('project_companies', 'project_companies.proj_id', '=', 'projects.id')
            ->where('project_companies.ab_id', '=', $id)
            ->where('projects.comp_id', '=', Auth::user()->comp_id)
            ->select(DB::raw('projects.*, 
            (select project_rating from project_company_ratings 
                WHERE project_company_ratings.proj_id = projects.id 
                AND project_company_ratings.ab_cont_id = '.$id.') as project_rating'))
            ->orderBy('projects.id', 'desc')
            ->get();

        return $projects;
    }

    /**
     * @param $projectId
     * @return mixed
     */
    public function getAllRatingsPerProject($projectId)
    {
        $projectRatings = Project_company_rating::join('address_book', 'address_book.id', '=', 'project_company_ratings.ab_cont_id')
            ->where('project_company_ratings.proj_id', '=', $projectId)
            ->select(['address_book.*','project_company_ratings.project_rating'])
            ->get();

        return $projectRatings;
    }

    /**
     * get all active projects unless $active = 0 to get inactive
     * sort and order provided from input - if not provided default by id, asc
     * @param $id
     * @param $active
     * @param $searchBy
     * @param $search
     * @return mixed
     */
    public function searchProjectsBySubcontractor($id,$active,$searchBy, $search)
    {
        $projects = Project::with('address')
            ->join('project_companies','project_companies.proj_id', '=', 'projects.id')
            ->where('project_companies.ab_id', '=', $id)
            ->where('projects.active', '=', $active)
            ->where('projects.comp_id', '=', Auth::user()->comp_id)
            ->where('projects.'.$searchBy, 'LIKE', "%$search%")
            ->orderBy('projects.id', 'asc')
            ->paginate(Config::get('constants.pagination.projects'));

        return $projects;
    }

    /**
     * Change (update) project admin
     * @param $userId
     * @param $projectId
     * @param bool $removePermission
     * @return mixed
     */
    public function changeProjectAdmin($userId, $projectId, $removePermission = false)
    {
        $projectAdmin = Project::where('comp_id','=',Auth::user()->comp_id)
            ->where('id','=',$projectId)
            ->update([
                'proj_admin' => ($removePermission)?null:$userId
            ]);

        if ($projectAdmin) {
            //get project admin role
            $role = Role::where('name', '=', 'Project Admin')->first();
            if(!is_null($role)) {
                $userRole = AssignedRoles::where('user_id','=',(int)$userId)->update([
                    'role_id' => (int)$role->id
                ]);

                if(!$userRole) {
                    return null;
                }
            }
        }

        return $projectAdmin;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getAllDeletedProjects()
    {
        $projects = Project::onlyTrashed()->where('files_deleted', '=', '0')->get();
        return $projects;
    }

    /**
     * Returns all projects for a company
     *
     * @param $companyId
     * @return mixed
     */
    public function getAllProjectsForCompanyId($companyId)
    {
        return Project::where('comp_id', '=', $companyId)->get();
    }

    /**
     * Returns all batch download requests
     *
     * @return mixed
     */
    public function getProjectBatchRequests()
    {
        return ProjectBatchFile::with('project')->join('projects','project_batch_files.proj_id', '=', 'projects.id')
            ->where('generated', '=', 0)
            ->whereNull('projects.deleted_at')
            ->select(['project_batch_files.*'])
            ->get();
    }

    /**
     * Returns all batch download expired files
     *
     * @return mixed
     */
    public function getProjectBatchExpiredFiles($numberOfDays)
    {
        return ProjectBatchFile::with('project')
                ->where('generated', '=', 1)
                ->where('created_at', '<', Carbon::now()->subDays($numberOfDays)->toDateString())
                ->get();
    }

    /**
     * Returns all batch download expired files
     *
     * @return mixed
     */
    public function deleteBatchFile($id)
    {
        return ProjectBatchFile::where('id', '=', $id)->delete();
    }

    /**
     * Used for adding batch download request by project id
     *
     * @param $projectId
     */
    public function addBatchDownloadRequest($projectId)
    {
        ProjectBatchFile::where('proj_id', '=', $projectId)->where('comp_id', '=', Auth::user()->comp_id)->delete();

        $data['proj_id'] = $projectId;
        $data['comp_id'] = Auth::user()->comp_id;
        $data['user_id'] = Auth::user()->id;
        $data['generated'] = 0;
        $data['sent'] = 0;

        ProjectBatchFile::create($data);
    }

    /**
     * Get specific batch download request by project id
     *
     * @param $projectId
     * @return mixed
     */
    public function getBatchDownloadRequestByProject($projectId)
    {
        return ProjectBatchFile::where('proj_id', '=', $projectId)->where('comp_id', '=', Auth::user()->comp_id)->first();
    }

    public function getProjectWithUserPermissions($id)
    {
        return Project::where('id', '=', $id)->where('comp_id', '=', Auth::user()->comp_id)
            ->with('company')
            ->with('company.currentUser')
            ->with('projectUserPermissions')
            ->with('projectUserPermissions.user')
            ->first();
    }
}



