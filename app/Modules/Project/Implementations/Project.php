<?php
namespace App\Modules\Project\Implementations;


use App\Modules\Project\Interfaces\ProjectInterface;

class Project implements ProjectInterface {

    public function generateNumber($numbers, $counterValue)
    {
        $generatedNumber = str_pad(($counterValue+1), 3, '0', STR_PAD_LEFT);

        $existedNumbers = [];
        if(isset($numbers) && count($numbers) > 0)
        {
            foreach ($numbers as $row)
            {
                $existedNumbers[] = $row['number'];
            }

            if(count($existedNumbers) > 0)
            {
                //if the auto generated number is in existed elements array, than generate new number
                if(!in_array($generatedNumber, $existedNumbers))
                {
                    $lastGeneratedNumber = $existedNumbers[count($existedNumbers) - 1];
                    if(!preg_match('/^[0-9]{3}$/', $lastGeneratedNumber)) {
                        //CUSTOM PATTERN: if the generated number not used the default pattern, get last element + 1
                        $lastGeneratedNumber = $existedNumbers[count($existedNumbers) - 1];
                        $generateNumber =  ltrim($lastGeneratedNumber, "0");
                        $generatedNumber = str_pad(($generateNumber+1), 3, '0', STR_PAD_LEFT);

                    }
                    else {
                        //DEFAULT PATTERN: if the generated number use the pattern: 001, 010 or 101
                        $generateNumber =  ltrim(max($existedNumbers), "0");
                        $generatedNumber = str_pad(($generateNumber+1), 3, '0', STR_PAD_LEFT);
                    }
                }
            }
        }

        return $generatedNumber;
    }
}