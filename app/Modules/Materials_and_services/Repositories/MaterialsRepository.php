<?php
namespace App\Modules\Materials_and_services\Repositories;

use App\Models\Expediting_report;
use App\Models\Expediting_report_status;
use App\Models\Module;
use App\Models\Project_company_permission;
use App\Models\Proposal_supplier;
use App\Models\Proposal_file;
use App\Models\File;
use App\Models\File_type;
use App\Models\Project;
use App\Models\Proposal_supplier_permission;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Services\CompanyPermissions;
use Carbon\Carbon;
use Config;

use Request;
use Input;
use Flash;
use Session;
use Auth;

class MaterialsRepository {

    // parameters of material/service tables, used when editing

    private $materialParams = array('name', 'mf_title', 'mf_number', 'position', 'ab_id', 'status_id', 'subm_status','need_by');

    public function __construct()
    {
        $this->projectSubcontractorsRepo = new projectSubcontractorsRepository();
    }

    /**
     * Get all materials/services for project
     * @param $projectId
     * @param $sort
     * @param $order
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getMaterialsByProject($projectId, $sort, $order)
    {
        if ($sort == 'ab_name') {
            $sort = 'address_book.name';
        } else if ($sort == 'subm_status') {
            $sort = 'submittal_statuses.name';
        } else {
            $sort = 'expediting_reports.'.$sort;
        }
        $materials = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
            ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->with('status')
            ->select('expediting_reports.*', 'address_book.name as ab_name', 'submittal_statuses.name as status_name')
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.materials_and_services'));
        return $materials;
    }

    /**
     * Get all materials/services for project
     * @param $projectId
     * @param $sort
     * @param $order
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllMaterialsByProject($projectId, $sort, $order)
    {
        $materials = Expediting_report::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->orderBy($sort, $order)
            ->get();

        return $materials;
    }

    /**
     * Get shared expediting report records
     * @param $projectId
     * @param $sort
     * @param $order
     * @return bool|null
     */
    public function getSharedMaterialsByProject($projectId, $sort, $order)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($projectId, Auth::user()->comp_id);
        $bidder = $this->projectSubcontractorsRepo->isBidder($projectId, Auth::user()->comp_id);
        $project = Project::where('id', '=', $projectId)->firstOrFail();
        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }
        if ($sort == 'ab_name') {
            $sort = 'address_book.name';
        } else if ($sort == 'subm_status') {
            $sort = 'submittal_statuses.name';
        } else {
            $sort = 'expediting_reports.'.$sort;
        }

        $expeditingReportType = Module::where('display_name','=',Config::get('constants.modules_display_names.expediting_report'))->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $expeditingReportsPermissions = Project_company_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        if (!is_null($expeditingReportsPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($expeditingReportsPermissions) && $expeditingReportsPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
                $materials = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
                    ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
                    ->where('proj_id', '=', $projectId)
                    ->where('comp_id', '!=', Auth::user()->comp_id)
                    ->select('expediting_reports.*', 'address_book.name as ab_name', 'submittal_statuses.name as status_name')
                    ->orderBy($sort, $order)
                    ->paginate(Config::get('constants.pagination.materials_and_services'));
                return $materials;
            }
        }

        return null;
    }

    /**
     * Get material/service with subcontractor by id
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getMaterial($id)
    {
        $material = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
            ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
            ->where('expediting_reports.id', '=', $id)
            ->select(
                'expediting_reports.*',
                'address_book.name as ab_name',
                'address_book.id as ab_id',
                'submittal_statuses.name as status_name'
            )
            ->firstOrFail();
        return $material;
    }

    /**
     * Get all expediting report statuses
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getStatuses()
    {
        return Expediting_report_status::all();
    }

    /**
     * Save material/service to database
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeMaterial($projectId, $data)
    {
        $materialData = array(
            'proj_id' => $projectId,
            'mf_title' => $data['mf_title'],
            'mf_number' => $data['mf_number'],
            'name' => $data['name'],
            'position' => $data['position'],
            'ab_id' => $data['mas_sub_id'],
            'er_status_id' => empty($data['er_status_id']) ? 0 : $data['er_status_id'],
            'subm_needed' => $data['subm_needed'],
            'status_id' => $data['status_id'],
            'comp_id' => Auth::user()->comp_id,
            'need_by' => Carbon::parse($data['need_by'])->format('Y-m-d')
        );
        $material = Expediting_report::firstOrCreate($materialData);
        return $material;
    }

    /**
     * Update material/service by id with data
     * @param $id
     * @param $data
     */
    public function updateMaterial($id,$data)
    {
        $data['need_by'] = Carbon::parse($data['need_by'])->format('Y-m-d');
        $data['ab_id'] = $data['mas_sub_id'];

        $material = Expediting_report::where('id', '=', $id)->firstOrFail();

        $material->mf_number = $data['mf_number'];
        $material->mf_title = $data['mf_title'];
        $material->name = $data['name'];
        $material->position = $data['position'];
        $material->ab_id = $data['mas_sub_id'];
        $material->er_status_id = empty($data['er_status_id']) ? 0 : $data['er_status_id'];
        $material->subm_needed = $data['subm_needed'];
        $material->status_id = $data['status_id'];
        $material->need_by = $data['need_by'];
        return $material->save();
    }


    /**
     * delete material/service
     * @param $id
     * @return mixed
     */
    public function deleteMaterial($id)
    {
        $material = Expediting_report::where('id','=',$id)->firstOrFail();
        return ($material->delete());
    }

    /**
     * Filter expediting report
     * @param $params
     * @return mixed
     */
    public function filterMaterials($params)
    {
        $query = [
            'expediting_reports.proj_id' => $params['projectId']
        ];

        if($params['status'] != '' && $params['status'] != 'All') {
            $query['expediting_reports.status_id'] = $params['status'];
        }
        if($params['erStatus'] != '' && $params['erStatus'] != 'All') {
            $query['expediting_reports.er_status_id'] = $params['erStatus'];
        }
        if($params['subcontractor'] != '') {
            $query['expediting_reports.ab_id'] = $params['subcontractor'];
        }

        $fullQuery = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
            ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
            ->leftJoin('expediting_report_statuses','expediting_report_statuses.id','=','expediting_reports.er_status_id')
            ->where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('expediting_reports.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('expediting_reports.mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['name'])) {
            $fullQuery->where('expediting_reports.name', 'LIKE', $params['name'].'%');
        }

        if (!empty($params['location'])) {
            $fullQuery->where('expediting_reports.position', 'LIKE', $params['location'].'%');
        }

        return $fullQuery->select('expediting_reports.*', 'address_book.name as ab_name','submittal_statuses.name as status_name','expediting_report_statuses.name as er_status_name')
            ->orderBy($params['sort'], $params['order'])
            ->paginate(Config::get('constants.pagination.materials_and_services'));;
    }

    /**
     * Filter shared expediting report
     * @param $params
     * @return bool|null
     */
    public function filterSharedMaterials($params)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], Auth::user()->comp_id);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], Auth::user()->comp_id);

        $project = Project::where('id', '=', $params['projectId'])->firstOrFail();
        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $expeditingReportType = Module::where('display_name','=',Config::get('constants.modules_display_names.expediting_report'))->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $expeditingReportsPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        if (!is_null($expeditingReportsPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($expeditingReportsPermissions) && $expeditingReportsPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
                $query = [
                    'expediting_reports.proj_id' => $params['projectId']
                ];

                if($params['status'] != '' && $params['status'] != 'All') {
                    $query['expediting_reports.status_id'] = $params['status'];
                }
                if($params['erStatus'] != '' && $params['erStatus'] != 'All') {
                    $query['expediting_reports.er_status_id'] = $params['erStatus'];
                }
                if($params['subcontractor'] != '') {
                    $query['expediting_reports.ab_id'] = $params['subcontractor'];
                }

                $fullQuery = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
                    ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
                    ->leftJoin('expediting_report_statuses','expediting_report_statuses.id','=','expediting_reports.er_status_id')
                    ->where($query);

                //master format search field transformation in a proper search segment
                if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                    $masterFormatPieces = explode(" - ", $params['masterFormat']);
                    $fullQuery->where('expediting_reports.mf_number','LIKE',$masterFormatPieces[0].'%');
                } else {
                    $fullQuery->where('expediting_reports.mf_title','LIKE',$params['masterFormat'].'%');
                }

                if (!empty($params['name'])) {
                    $fullQuery->where('expediting_reports.name', 'LIKE', $params['name'].'%');
                }

                if (!empty($params['location'])) {
                    $fullQuery->where('expediting_reports.position', 'LIKE', $params['location'].'%');
                }

                return $fullQuery->select('expediting_reports.*', 'address_book.name as ab_name','submittal_statuses.name as status_name','expediting_report_statuses.name as er_status_name')
                    ->paginate(Config::get('constants.pagination.materials_and_services'));
            }
        }

        return null;
    }

    /**
     * Create PDF list from the shared expediting report
     * @param $params
     * @return bool|null
     */
    public function reportSharedMaterials($params)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], Auth::user()->comp_id);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], Auth::user()->comp_id);
        $project = Project::where('id', '=', $params['projectId'])->firstOrFail();
        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $expeditingReportType = Module::where('display_name','=',Config::get('constants.modules_display_names.expediting_report'))->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $expeditingReportsPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            ->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$expeditingReportType->id)
            ->first();

        if (!is_null($expeditingReportsPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($expeditingReportsPermissions) && $expeditingReportsPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
                $query = [
                    'expediting_reports.proj_id' => $params['projectId']
                ];

                if($params['status'] != '' && $params['status'] != 'All') {
                    $query['expediting_reports.status_id'] = $params['status'];
                }
                if($params['erStatus'] != '' && $params['erStatus'] != 'All') {
                    $query['expediting_reports.er_status_id'] = $params['erStatus'];
                }
                if($params['subcontractor'] != '') {
                    $query['expediting_reports.ab_id'] = $params['subcontractor'];
                }

                $fullQuery = Expediting_report::leftJoin('address_book','address_book.id','=','expediting_reports.ab_id')
                    ->leftJoin('submittal_statuses','submittal_statuses.id','=','expediting_reports.status_id')
                    ->leftJoin('expediting_report_statuses','expediting_report_statuses.id','=','expediting_reports.er_status_id')
                    ->where($query);

                //master format search field transformation in a proper search segment
                if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                    $masterFormatPieces = explode(" - ", $params['masterFormat']);
                    $fullQuery->where('expediting_reports.mf_number','LIKE',$masterFormatPieces[0].'%');
                } else {
                    $fullQuery->where('expediting_reports.mf_title','LIKE', $params['masterFormat'].'%');
                }

                return $fullQuery->select('expediting_reports.*', 'address_book.name as ab_name','submittal_statuses.name as status_name','expediting_report_statuses.name as er_status_name')
                    ->get();
            }
        }

        return null;
    }
}