<?php

namespace App\Modules\Tasks\Repositories;

use App\Models\Master_Format;
use App\Models\Task;
use App\Models\Tasks_file;
use App\Models\TasksFile;
use App\Models\TaskUsersDistribution;
use App\Models\TasksUser;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class TasksRepository
{
    protected $companyRepo;
    protected $filesRepo;
    /**
     * TasksRepository constructor.
     */
    public function __construct()
    {
        $this->companyRepo = App::make(CompaniesRepository::class);
        $this->filesRepo = App::make(FilesRepository::class);
    }

    /**
     * Returns task by id
     *
     * @param $id
     * @return mixed
     */
    public function getTaskById($id)
    {
        return Task::where('id', '=', $id)
                    ->with('taskUsers')
                    ->with('project')
                    ->with('addedByUser')
                    ->with('taskFiles')
                    ->with('taskUsers.users.company')
                    ->with('taskUsers.abUsers.addressBook')
                    ->with('taskUsers.distribution')
                    ->with('addedByUser.company')
                    ->first();
    }

    /**
     * Get all tasks
     *
     * @param $filterBy
     * @param $filterText
     * @param string $orderBy
     * @param string $orderDir
     * @param bool $paginate
     * @return mixed
     */
    public function getAllTasks($filterBy, $filterText, $orderBy, $orderDir, $writeArray, $paginate=true)
    {
        $tasks = Task::join('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');


        if (!empty($filterBy) && !empty($filterText)) {
            if ($filterBy == "assignee") {
                $tasks = $tasks->where(function ($q) use ($filterText) {
                    $q->whereRaw("(Select count(*) from tasks_users, ab_contacts where ab_contacts.name like '" . $filterText . "%' AND
                                tasks_users.ab_cont_id = ab_contacts.id AND tasks_users.task_id = tasks.id) > 0")
                        ->orWhereRaw("(Select count(*) from tasks_users, users where users.name like '" . $filterText . "%' AND
                                tasks_users.user_id = users.id AND tasks_users.task_id = tasks.id) > 0");
                });
            } else if ($filterBy == "type") {
                $keys = [];
                foreach (config('constants.tasks.type') as $key => $taskName) {
                    if (strpos(mb_strtolower($taskName), mb_strtolower($filterText)) !== false) {
                        $keys[] = $key;
                    }
                }

                if (count($keys) > 0) {
                    $tasks = $tasks->where(function ($query) use ($keys, $filterText) {
                        $query->whereIn('type_id', $keys)
                            ->orWhere('custom_type', 'like', $filterText.'%');
                    });
                } else {
                    $tasks = $tasks->where('custom_type', 'like', $filterText.'%');
                }

            } else {
                $tasks = $tasks->where($filterBy, 'like', $filterText.'%');
            }
        }

        if (((!Auth::user()->hasRole('Company Admin')) && (!Auth::user()->hasRole('Project Admin')))) {
            $tasks = $tasks->whereIn('tasks.project_id', (!empty($writeArray)?$writeArray:[0]));
        } else if (((!Auth::user()->hasRole('Company Admin')) && (Auth::user()->hasRole('Project Admin')))) {
            $tasks = $tasks->where(function ($query) use ($writeArray) {
                $query->where('projects.proj_admin', '=', Auth::user()->id)
                    ->orWhereIn('tasks.project_id', (!empty($writeArray)?$writeArray:[0]));
            });
        }

        $tasks = $tasks->whereNull('parent_id')
                      ->where('tasks.comp_id', '=', Auth::user()->comp_id)
                      ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
                      ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(50);
        } else {
            return $tasks->get();
        }
    }

    /**
     * Get all my tasks
     *
     * @param $filterBy
     * @param $filterText
     * @param string $orderBy
     * @param string $orderDir
     * @param bool $paginate
     * @return mixed
     */
    public function getAllMyTasks($filterBy, $filterText, $orderBy, $orderDir, $paginate = true)
    {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');


        if (!empty($filterBy) && !empty($filterText)) {
            if ($filterBy == "assignee") {
                $tasks = $tasks->where(function($q) use ($filterText) {
                    $q->whereRaw("(Select count(*) from tasks_users, ab_contacts where ab_contacts.name like '%".$filterText."%' AND
                                tasks_users.ab_cont_id = ab_contacts.id AND tasks_users.task_id = tasks.id) > 0")
                        ->orWhereRaw("(Select count(*) from tasks_users, users where users.name like '%".$filterText."%' AND
                                tasks_users.user_id = users.id AND tasks_users.task_id = tasks.id) > 0");
                });
            } else if ($filterBy == "type") {
                $keys = [];
                foreach (config('constants.tasks.type') as $key => $taskName) {
                    if (strpos(mb_strtolower($taskName), mb_strtolower($filterText)) !== false) {
                        $keys[] = $key;
                    }
                }

                if (count($keys) > 0) {
                    $tasks = $tasks->where(function ($query) use ($keys, $filterText) {
                        $query->whereIn('type_id', $keys)
                            ->orWhere('custom_type', 'like', '%'.$filterText.'%');
                    });
                } else {
                    $tasks = $tasks->where('custom_type', 'like', '%'.$filterText.'%');
                }

            } else {
                $tasks = $tasks->where($filterBy, 'like', '%'.$filterText.'%');
            }
        }

        $tasks = $tasks->where(function($q) {
                $q->where(function($r) {
                    $r->where('tasks.comp_id', '=', Auth::user()->comp_id)
                        ->whereRaw("(Select count(*) from tasks_users, users where users.id = ".Auth::user()->id." AND
                                tasks_users.user_id = users.id AND tasks_users.task_id = tasks.id) > 0")
                        ->whereRaw("(Select count(*) from tasks_users, users where users.id = ".Auth::user()->id." AND
                                tasks_users.user_id = users.id AND tasks_users.task_id = tasks.parent_id) = 0");
                    })
                ->orWhere(function($r) {
                    $r->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.id) > 0")
                      ->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.parent_id) = 0");
                });
            })
            ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
            ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(50);
        } else {
            return $tasks->get();
        }

    }

    /**
     * Get all project tasks
     *
     * @param $filterBy
     * @param $filterText
     * @param string $orderBy
     * @param string $orderDir
     * @return mixed
     */
    public function getAllProjectTasks($projectId, $filterBy, $filterText, $orderBy, $orderDir, $paginate=true)
    {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');


        if (!empty($filterBy) && !empty($filterText)) {
            if ($filterBy == "assignee") {
                $tasks = $tasks->where(function($q) use ($filterText) {
                    $q->whereRaw("(Select count(*) from tasks_users, ab_contacts where ab_contacts.name like '%".$filterText."%' AND
                                tasks_users.ab_cont_id = ab_contacts.id AND tasks_users.task_id = tasks.id) > 0")
                        ->orWhereRaw("(Select count(*) from tasks_users, users where users.name like '%".$filterText."%' AND
                                tasks_users.user_id = users.id AND tasks_users.task_id = tasks.id) > 0");
                });
            } else if ($filterBy == "type") {
                $keys = [];
                foreach (config('constants.tasks.type') as $key => $taskName) {
                    if (strpos(mb_strtolower($taskName), mb_strtolower($filterText)) !== false) {
                        $keys[] = $key;
                    }
                }

                if (count($keys) > 0) {
                    $tasks = $tasks->where(function ($query) use ($keys, $filterText) {
                        $query->whereIn('type_id', $keys)
                            ->orWhere('custom_type', 'like', '%'.$filterText.'%');
                    });
                } else {
                    $tasks = $tasks->where('custom_type', 'like', '%'.$filterText.'%');
                }

            } else {
                $tasks = $tasks->where($filterBy, 'like', '%'.$filterText.'%');
            }
        }

        $tasks = $tasks->whereNull('parent_id')
            ->where(function($q) {
                $q->where('tasks.comp_id', '=', Auth::user()->comp_id)
                  ->orWhere(function($r) {
                        $r->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.id) > 0")
                            ->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.parent_id) = 0");
                  });
            })
            ->where('tasks.project_id', '=', $projectId)
            ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
            ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(50);
        } else {
            return $tasks->get();
        }
    }

    public function getAllTasksSubcontructor()
    {
        $tasks = TasksUser::join('ab_contacts', 'ab_contacts.id', '=', 'tasks_users.ab_cont_id')
                           ->join('address_book', 'address_book.id', '=', 'ab_contacts.ab_id')
                           ->join('users', 'address_book.synced_comp_id', '=', 'users.comp_id')
                           ->whereRaw('ab_contacts.email = users.email')
                           ->where('users.id', '=', Auth::user()->id)
                           ->select(['tasks_users.task_id'])
                           ->get()
                           ->toArray();

        return $tasks;
    }

    public function deleteEntry($id)
    {
        $task = Task::where('id', '=', $id)->first();
        $subtasks = Task::where('parent_id', '=', $id)->get();
        foreach ($subtasks as $subtask) {
            $this->deleteEntry($subtask->id);
        }

        if ($task->status != 1) {
            return $task->delete();
        }
        return false;
    }

    public function storeEditTask($data, $id=null)
    {
        if (!empty($id)) {
            $task = Task::where('id', '=', $id)->first();
            if (!$task) {
                $task = new Task();
            }
        } else {
            $task = new Task();
        }

        $task->title = (!empty($data['title']))?$data['title']:null;
        $task->note = (!empty($data['note']))?$data['note']:null;
        $task->status = (!empty($data['status']))?$data['status']:null;
        $task->project_id = (!empty($data['project']))?$data['project']:null;
        $task->type_id = (!empty($data['module_type']))?$data['module_type']:null;
        $task->parent_id = (!empty($data['parent_id']))?$data['parent_id']:null;
        $dueDate = Carbon::createFromFormat('m/d/Y', $data['expiration-date']);
        $task->due_date = $dueDate->toDateString();
        $task->added_by = Auth::user()->id;
        $task->comp_id = Auth::user()->comp_id;
        $task->parent_id = (!empty($data['parent_id']))?$data['parent_id']:null;
        $task->mf_number = (!empty($data['mf_number']))?$data['mf_number']:null;
        $task->mf_title = (!empty($data['mf_title']))?$data['mf_title']:null;
        $task->custom_type = (!empty($data['custom_type']))?$data['custom_type']:null;
        $task->notification_days = (!empty($data['daysNotify']))?intval($data['daysNotify']):null;
        $task->notification = (!empty($data['daysNotify']))?1:null;


        //module item
        if (!empty($data['module_type'])) {
            switch ($data['module_type']) {
                case 4: //bids
                    $task->bid_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
                case 5: //contracts
                    $task->contract_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
                case 3: //submittals
                    $task->submittal_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
                case 9: //rfis
                    $task->rfi_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
                case 10: //pcos
                    $task->pco_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
                case 6: //materials
                    $task->material_id = (!empty($data['module_item']))?$data['module_item']:null;
                    break;
            }
        }

        $task->save();

        //link file to a task
        if (!empty($data['file_id'])) {
            $file = TasksFile::where('id', '=', $data['file_id'])->first();
            $file->task_id = $task->id;
            $file->save();
        }


        //assign users to task
        TasksUser::where('task_id', '=', $task->id)->delete();

        if (!empty($data['employees_users'])) {
            foreach ($data['employees_users'] as $userId) {
                $taskUser = TasksUser::firstOrCreate(['task_id' => $task->id, 'user_id' => $userId]);
            }
        }

        if (!empty($data['employees_contacts'])) {
            foreach ($data['employees_contacts'] as $contactId) {
                $dataContact = explode('-', $contactId);
                if (count($dataContact) == 2) {
                    if ($dataContact[0] == $data['project']) {
                        $taskUser = TasksUser::firstOrCreate(['task_id' => $task->id, 'ab_cont_id' => $dataContact[1]]);
                    }
                }

            }
        }

        return $task;
    }

    public function deleteTasksFile($data)
    {
        //link file to a task
        if (!empty($data['fileId'])) {
            $fileDelete = TasksFile::where('id', '=', $data['fileId'])->delete();
        }

        if (isset($fileDelete) && !empty($fileDelete)) {
            //if storage limit bellow 90% reset notification
            $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
            $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
            $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
            $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage;

            if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
                $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
            }

            return true;
        }
    }

    /**
     * Returns contacts for tasks notifications
     *
     * @return mixed
     */
    public function getTasksContactsForNotifications()
    {
        return Task::join('tasks_users', 'tasks_users.task_id', '=', 'tasks.id')
            ->join('companies', 'companies.id', '=', 'tasks.comp_id')
            ->join('projects', 'projects.id', '=', 'tasks.project_id')
            ->join('users as addedByUser', 'addedByUser.id', '=', 'tasks.added_by')
            ->leftJoin('users', 'users.id', '=', 'tasks_users.user_id')
            ->leftJoin('companies as userCompany', 'userCompany.id', '=', 'users.comp_id')
            ->leftJoin('ab_contacts', 'ab_contacts.id', '=', 'tasks_users.ab_cont_id')
            ->leftJoin('address_book', 'address_book.id', '=', 'ab_contacts.ab_id')
            ->where('tasks.notification', '=', 1)
            ->whereRaw('tasks.due_date = date(NOW() + INTERVAL tasks.notification_days DAY)')
            ->select(['users.email as userEmail', 'users.name as userName', 'tasks.title as taskName',
                'ab_contacts.name as contactName', 'ab_contacts.email as contactEmail', 'addedByUser.name as addedByUserName',
                'addedByUser.email as addedByUserEmail', 'addedByUser.title as addedByUserTitle',
                'addedByUser.office_phone as addedByUserOfficePhone', 'tasks.note as taskNote',
                'address_book.name as contactCompanyName', 'userCompany.name as userCompanyName',
                'companies.name as senderCompanyName', 'tasks.due_date as dueDate', 'projects.name as projectName'])
            ->get();
    }

    /**
     * Creates an entry for each task user when email is sent
     *
     * @param $taskUserId
     * @return TaskUsersDistribution
     */
    public function addTaskUserDistribution($taskUserId)
    {
        $task = TaskUsersDistribution::firstOrCreate(['task_user_id' => $taskUserId]);

        $task->created_at = Carbon::now()->toDateString();
        $task->save();

        return $task;
    }

    /**
     * Get all project tasks by submittal ID
     *
     * @param $submittalId
     * @param string $orderBy
     * @param string $orderDir
     * @param bool $paginate
     * @return mixed
     */
    public function getAllProjectTasksBySubmittal($submittalId, $orderBy, $orderDir, $paginate=true)
    {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');

        $tasks = $tasks->whereNull('parent_id')
            ->where(function($q) {
                $q->where('tasks.comp_id', '=', Auth::user()->comp_id)
                    ->orWhere(function($r) {
                        $r->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.id) > 0")
                            ->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.parent_id) = 0");
                    });
            })
            ->where('tasks.submittal_id', '=', $submittalId)
            ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
            ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(10);
        } else {
            return $tasks->get();
        }
    }

    /**
     * Get all project tasks by rfi ID
     *
     * @param $rfiId
     * @param string $orderBy
     * @param string $orderDir
     * @param bool $paginate
     * @return mixed
     */
    public function getAllProjectTasksByRfi($rfiId, $orderBy, $orderDir, $paginate=true)
    {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');

        $tasks = $tasks->whereNull('parent_id')
            ->where(function($q) {
                $q->where('tasks.comp_id', '=', Auth::user()->comp_id)
                    ->orWhere(function($r) {
                        $r->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.id) > 0")
                            ->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.parent_id) = 0");
                    });
            })
            ->where('tasks.rfi_id', '=', $rfiId)
            ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
            ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(10);
        } else {
            return $tasks->get();
        }
    }

    /**
     * Get all project tasks by pco ID
     *
     * @param $pcoId
     * @param string $orderBy
     * @param string $orderDir
     * @param bool $paginate
     * @return mixed
     */
    public function getAllProjectTasksByPco($pcoId, $orderBy, $orderDir, $paginate=true)
    {
        $tasks = Task::leftJoin('projects', 'projects.id', '=', 'tasks.project_id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.type_id')
            ->with('subtasks')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook')
            ->with('addedByUser.company');

        $tasks = $tasks->whereNull('parent_id')
            ->where(function($q) {
                $q->where('tasks.comp_id', '=', Auth::user()->comp_id)
                    ->orWhere(function($r) {
                        $r->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.id) > 0")
                            ->whereRaw("(Select count(*) from tasks_users, ab_contacts, address_book, users
                                    where tasks_users.ab_cont_id = ab_contacts.id AND ab_contacts.ab_id = address_book.id
                                    AND address_book.synced_comp_id = users.comp_id AND ab_contacts.email = users.email 
                                    AND users.id = ".Auth::user()->id." AND tasks_users.task_id = tasks.parent_id) = 0");
                    });
            })
            ->where('tasks.pco_id', '=', $pcoId)
            ->selectRaw('tasks.*, projects.name as projectName, IFNULL(tasks.custom_type, modules.name) AS combinedType')
            ->orderBy($orderBy, $orderDir);

        if ($paginate) {
            return $tasks->paginate(10);
        } else {
            return $tasks->get();
        }
    }
}