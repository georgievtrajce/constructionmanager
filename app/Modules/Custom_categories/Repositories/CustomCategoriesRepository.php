<?php namespace App\Modules\Custom_categories\Repositories;
use App\Models\Address_book;
use App\Models\Custom_category;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/25/2015
 * Time: 2:48 PM
 */


class CustomCategoriesRepository {

    private $companyId;

    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
    }

    /**
     * Get all custom categories for specified logged in company
     * @return mixed
     */
    public function getAllCategories()
    {
        return Custom_category::where('comp_id','=',$this->companyId)->orderBy('id','DESC')->paginate(10);
    }

    /**
     * Get concrete custom category specified by id
     * @param $id
     * @return mixed
     */
    public function getCategory($id)
    {
        return Custom_category::where('id','=',$id)->where('comp_id','=',$this->companyId)->firstOrFail();

    }

    /**
     * Store custom category for specified logged in company
     * @param $data
     * @return static
     */
    public function storeCategory($data)
    {
        return Custom_category::create([
            'name' => $data['name'],
            'comp_id' => $this->companyId
        ]);
    }

    /**
     * Update specified custom category
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCategory($id, $data)
    {
        return Custom_category::where('id','=',$id)->where('comp_id','=',$this->companyId)->update([
            'name' => $data['name']
        ]);
    }

    /**
     * Check if specified custom category is connected to some address book entry
     * This is used when someone tries to delete concrete custom category
     * @param $id
     * @return array
     */
    public function checkCategoryDependency($id)
    {
        return Custom_category::where('comp_id','=',$this->companyId)->with('address_book_entries')->find($id)->toArray();
    }

    /**
     * Delete specified custom category
     * @param $id
     * @return mixed
     */
    public function deleteCategory($id)
    {
        return Custom_category::where('id', '=', $id)->where('comp_id','=',$this->companyId)->delete();
    }

}