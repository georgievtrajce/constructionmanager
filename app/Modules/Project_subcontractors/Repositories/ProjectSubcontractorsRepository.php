<?php
namespace App\Modules\Project_subcontractors\Repositories;

use App\Models\Project;
use App\Models\Project_bidder;
use App\Models\Project_company;
use App\Models\Project_permission;
use App\Models\Subcontractor_project_info;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Rhumsaa\Uuid\Uuid;
use Illuminate\Support\Facades\Auth;
use App\Models\Project_subcontractor;


class ProjectSubcontractorsRepository {
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * ProjectSubcontractorsRepository constructor.
     */
    public function __construct()
    {
        $this->projectRepository = new ProjectRepository();
    }


    /**
     * Get subcontractors as arrays for project id
     * @param $projectId
     * @return array
     */
    public function getProjectSubcontractors($projectId)
    {

        $subObj = Project_subcontractor::join('companies','companies.id', '=', 'project_subcontractors.comp_child_id')
            ->where('proj_id', '=', $projectId)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->select('project_subcontractors.comp_child_id', 'project_subcontractors.id', 'companies.subs_id')
            ->get()->toArray();

        $subcontractors = array();

        for ($i=0;$i<sizeof($subObj);$i++) {
            //push in array subcontractor company id
            $subcontractors[$i]['comp_child_id'] = $subObj[$i]['comp_child_id'];
            //push in array project subcontractor record id
            $subcontractors[$i]['ps_id'] = $subObj[$i]['id'];
            $subcontractors[$i]['subs_id'] = $subObj[$i]['subs_id'];
        }
        return $subcontractors;

    }

    /**
     * Get project subcontractor by token
     * @param $token
     * @return mixed
     */
    public function getProjectSubcontractorByToken($token)
    {
        return Project_subcontractor::join('project_companies','project_companies.id', '=', 'project_subcontractors.proj_comp_id')
            ->where('project_subcontractors.token', '=', $token)
            ->select(
                'project_subcontractors.*',
                'project_companies.ab_id as ab_id'
            )
            ->first();
    }

    /**
     * Store Project Subcontractor permissions for the specified project
     * @param $subcontractorId
     * @param $projectId
     * @return bool
     */
    public function storeSubcontractorProjectPermissions($subcontractorId, $projectId)
    {
        /*
            *  ADD INITIAL PERMISSIONS FOR ALL USERS FOR THIS PROJECT
            */
        $manageUsersRepository = new ManageUsersRepository();

        //get modules from modules table
        $modules = $manageUsersRepository->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $manageUsersRepository->getProjectFileTypes();

        //get all company users
        $allCompanyUsers = $manageUsersRepository->getCompanyUsersAndProjectAdmins($subcontractorId);

        if (count($allCompanyUsers)) {
            foreach ($allCompanyUsers as $user) {
                if (count($modules)) {
                    foreach ($modules as $module) {
                        Project_permission::firstOrCreate([
                            'user_id' => $user->id,
                            'read' => 0,
                            'write' => 0,
                            'delete' => 0,
                            'entity_id' => $module->id,
                            'entity_type' => 1, //this means that the type of the permission is module
                            'proj_id' => $projectId
                        ]);
                    }
                }

                //set project files permissions to 0
                if (count($projectFiles)) {
                    foreach ($projectFiles as $projectFile) {
                        Project_permission::firstOrCreate([
                            'user_id' => $user->id,
                            'read' => 0,
                            'write' => 0,
                            'delete' => 0,
                            'entity_id' => $projectFile->id,
                            'entity_type' => 2, //this means that the type of the permission is project file
                            'proj_id' => $projectId
                        ]);
                    }
                }
            }
        }
        return true;
    }

    /**
     * store subcontractor in database for project
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeSubcontractor($projectId, $data)
    {
        //check if this project was shared previously with this subcontractor
        $existingSubcontractor = Project_subcontractor::where([
                'proj_id' => $projectId,
                'comp_parent_id' => Auth::user()->comp_id,
                'comp_child_id' => $data['comp_id'],
            ])
            ->where('token_active','=',Config::get('constants.projects.status.inactive'))
            ->where('status','=',Config::get('constants.projects.shared_status.unshared'))
            ->first();

        //create new project subcontractor with pending status
        $uuid1 = Uuid::uuid1()->toString();
        if (is_null($existingSubcontractor)) {
            $subcontractorData = array(
                'proj_id' => $projectId,
                'comp_parent_id' => Auth::user()->comp_id,
                'proj_comp_id' => $data['project_company_id'],
                'comp_child_id' => $data['comp_id'],
                'address_id' => $data['addr_id'],
                'token_active' => Config::get('constants.projects.status.active'),
                'token' => $uuid1,
                'status' => Config::get('constants.projects.shared_status.pending')
            );

            $newSubcontractor =  Project_subcontractor::firstOrCreate($subcontractorData);

            //store project permissions for the new added subcontractor
            $this->storeSubcontractorProjectPermissions($newSubcontractor->comp_child_id, $projectId);

            return $newSubcontractor;
        }

        //update the project subcontractor status to pending
        $existingSubcontractor->token = $uuid1;
        $existingSubcontractor->status = Config::get('constants.projects.shared_status.pending');
        $existingSubcontractor->token_active = Config::get('constants.projects.status.active');
        $existingSubcontractor->save();

        //store project permissions for the subcontractor if their aren't any
        $this->storeSubcontractorProjectPermissions($existingSubcontractor->comp_child_id, $projectId);

        return $existingSubcontractor;
    }

    /**
     * Store invited subcontractor
     * @param $projectId
     * @param $projectCompanyId
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function storeInvitedSubcontractor($projectId, $projectCompanyId)
    {
        return Project_subcontractor::firstOrCreate([
            'comp_parent_id' => Auth::user()->comp_id,
            'proj_comp_id' => $projectCompanyId,
            'proj_id' => $projectId,
            'token' => Uuid::uuid1()->toString(),
            'token_active' => Config::get('constants.projects.status.active'),
            'status' => Config::get('constants.projects.shared_status.pending')
        ]);
    }

    /**
     * Get child subcontractors array for a project
     * @param $projectId
     * @return array
     */
    public function getChildSubcontractors($projectId)
    {

        //get direct subcontractors
        $subcontractors = Project_subcontractor::where('proj_id', '=', $projectId)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->get();

        $subArray = array();

        //get children companies of direct subcontractors as objects

        for ($i=0;$i<sizeof($subcontractors);$i++){
            $childSubcontractors = Project_subcontractor::with('child_company_single')
                ->where('proj_id', '=', $projectId)
                ->where('comp_parent_id', '=', $subcontractors[$i]->comp_child_id)->get();
            $subArray[$subcontractors[$i]->comp_child_id] = $childSubcontractors;
        }
        return $subArray;
    }

    /**
     * Update subcontractor's address
     * @param $data
     */
    public function updateAddress($data)
    {
        $subcontractor = Project_subcontractor::where('id', '=', $data['sub_id'])->firstOrFail();
        $subcontractor->address_id = $data['address_id'];
        return ($subcontractor->save());
    }


    /**
     * Unshare project with subcontractor
     * @param $projectId
     * @param $projectSubcontractorId
     * @return mixed
     */
    public function unshareProject($projectId, $projectSubcontractorId)
    {
        //get direct subcontractor
        $subcontractor = Project_subcontractor::where('id', '=', $projectSubcontractorId)->firstOrFail();

        //update project company permissions with child_comp_id = 0
        //$subcontractorPermissions = new ProjectPermissionsRepository();
        //$subcontractorPermissions->deleteProjectCompanyPermissions($projectId, $subcontractor->comp_child_id);
        //$subcontractorPermissions->updatePermissionsSubcontractor(Config::get('constants.project_subcontractors.no_project_company'), $projectId, $parentCompanyId, $childCompanyId);

        //get his subcontractors
        $children = Project_subcontractor::where('comp_parent_id', '=', $subcontractor['comp_child_id'])->where('proj_id', '=', $projectId)->get();

        //recursive unshare to subcontractors
        if (sizeof($children) > 0) {
            for ($i=0; $i<sizeof($children); $i++) {
                //delete project modules and project files company permissions
                $childrenSubcontractorsPermissions = new ProjectPermissionsRepository();
                $childrenSubcontractorsPermissions->deleteProjectCompanyPermissions($projectId, $children[$i]->comp_child_id);

                $this->unshareProject($projectId, $children[$i]->id);
            }
        }

        //if the project is not accepted yet and have status pending (1), delete the project from project_subcontractors
        if ($subcontractor->once_accepted == Config::get('constants.projects.once_accepted.no')) {
            //delete subcontractor users permissions
            $manageUsersRepository = new ManageUsersRepository();

            //get all company users
            $allCompanyUsers = $manageUsersRepository->getAllCompanyUsers($subcontractor->comp_child_id);

            if (count($allCompanyUsers)) {
                foreach ($allCompanyUsers as $user) {
                    $manageUsersRepository->deleteProjectUserPermissions($user->id, $projectId);
                }
            }

            return $subcontractor->delete();
        }

        //delete subcontractor (unshare)
        return $subcontractor->update([
            'token_active' => Config::get('constants.projects.status.inactive'),
            'status' => Config::get('constants.projects.shared_status.unshared')
        ]);
        //return ($subcontractor->delete());
    }

    /**
     * Change status of subcontractor and deactivate token
     * @param $token
     * @param $status
     * @return bool
     */
    public function updateSubcontractorStatus($token, $status)
    {
        $subcontractor = Project_subcontractor::where('token', '=', $token)->first();

        if (!is_null($subcontractor)) {
            //if the subcontractor reject the shared project - delete from subcontractor's table
            if ($status == Config::get('constants.projects.shared_status.rejected') && $subcontractor->once_accepted == 0) {
                //delete subcontractor users permissions
                $manageUsersRepository = new ManageUsersRepository();

                //get all company users
                $allCompanyUsers = $manageUsersRepository->getAllCompanyUsers($subcontractor->comp_child_id);

                if (count($allCompanyUsers)) {
                    foreach ($allCompanyUsers as $user) {
                        $manageUsersRepository->deleteProjectUserPermissions($user->id, $subcontractor->proj_id);
                    }
                }

                return ($subcontractor->delete());
            }

            if ($subcontractor->token_active == Config::get('constants.projects.status.active')) {
                //deactivate token
                $subcontractor->token_active = Config::get('constants.projects.status.inactive');
                //change status
                $subcontractor->status = $status;
                $subcontractor->once_accepted = 1;
                return ($subcontractor->save());
            }
        }

        return TRUE;
    }

    /**
     * Update subcontractor after accepting the invitation
     * @param $token
     * @param $childCompanyId
     * @param $status
     * @return bool
     */
    public function updateSubcontractor($token, $childCompanyId, $status)
    {
        $subcontractor = Project_subcontractor::where('token', '=', $token)->first();

        if (!is_null($subcontractor)) {
            //if the subcontractor reject the shared project - delete from subcontractor's table
            if ($status == Config::get('constants.projects.shared_status.rejected') && $subcontractor->once_accepted == 0) {
                //delete subcontractor users permissions
                $manageUsersRepository = new ManageUsersRepository();

                //get all company users
                $allCompanyUsers = $manageUsersRepository->getAllCompanyUsers($subcontractor->comp_child_id);

                if (count($allCompanyUsers)) {
                    foreach ($allCompanyUsers as $user) {
                        $manageUsersRepository->deleteProjectUserPermissions($user->id, $subcontractor->proj_id);
                    }
                }

                return ($subcontractor->delete());
            }

            if ($subcontractor->token_active == Config::get('constants.projects.status.active')) {
                //deactivate token
                $subcontractor->token_active = Config::get('constants.projects.status.inactive');

                //update child (subcontractor) company id
                $subcontractor->comp_child_id = $childCompanyId;

                //change status
                $subcontractor->status = $status;
                $subcontractor->once_accepted = 1;

                $subcontractor->save();
                return $subcontractor;
            }
        }

        return TRUE;
    }


    public function addProjectInfoToSubcontractor($data)
    {
        $projId = null;
        //add project info to the subcontractor
        if (!empty($data['projectCompanyId'])) {
            $projectCompany = Project_company::where('id', '=', $data['projectCompanyId'])->first();
            $projId = $projectCompany->proj_id;
        } else if (!empty($data['bidder'])) {
            $projectBidder = Project_bidder::where('token', '=', $data['token'])
                                ->first();
            $projId = $projectBidder->proj_id;
        }

        if ($projId) {
            $project = Project::where('id', '=', $projId)->with('address')->first();
            if ($project) {
                $projectData = [
                    'name' => $project->name,
                    'number'=> $project->number,
                    'start_date'=>empty($project->start_date) ? '' : Carbon::parse($project->start_date)->format('Y-m-d'),
                    'end_date'=>empty($project->end_date) ? '' : Carbon::parse($project->end_date)->format('Y-m-d'),
                    'active'=> $project->active,
                    'price'=> null,
                    'architect_id' => $project->architect_id,
                    'owner_id' => $project->owner_id,
                    'proj_id' => $projId,
                    'subcontractor_id' => Auth::user()->comp_id,
                    'creator_id' => $project->comp_id
                ];

                //store subcontractor project info
                $subcontractorInfo = Subcontractor_project_info::firstOrCreate($projectData);

                $addressData = array(
                    'street' => $project->address->street,
                    'zip' => $project->address->zip,
                    'city' => $project->address->city,
                    'state' => $project->address->state,
                );

                //store subcontractor project address
                $this->projectRepository->storeSubcontractorProjectAddress($project->id, $addressData);
            }
        }
    }

    /**
     * Get project subcontractor by token
     * @param $token
     * @return mixed
     */
    public function getSubcontractorByToken($token)
    {
        $subcontractor = Project_subcontractor::where('token', '=', $token)
            ->with('project_company.address_book')
            ->first();
        return $subcontractor;
    }

    /**
     * Check if specified company is already a subcontractor for the concrete project
     * @param $projectID
     * @param $compID
     * @return bool
     */
    public function isSubcontractor($projectID, $compID)
    {
        $subcontractor = Project_subcontractor::where('proj_id', '=', $projectID)
            ->where('comp_child_id', '=', $compID)
            ->where('status', '=', Config::get('constants.projects.shared_status.accepted'))
            ->first();
        if ($subcontractor) {
            return $subcontractor;
        }
        return false;
    }

    /**
     * Check if specified company is already a bidder for the concrete project
     * @param $projectID
     * @param $compID
     * @return bool
     */
    public function isBidder($projectID, $compID)
    {
        $bidder = Project_bidder::where('proj_id', '=', $projectID)
            ->where('invited_comp_id', '=', $compID)
            ->where('status', '=', Config::get('constants.projects.shared_status.accepted'))
            ->first();
        if ($bidder) {
            return $bidder;
        }
        return false;
    }

    /**
     * Delete project subcontractor specified by token
     * @param $token
     * @return bool
     */
    public function deleteProjectSubcontractor($token)
    {
        $subcontractor = Project_subcontractor::where('token','=',$token)
            ->first();
        if (!is_null($subcontractor)) {
            return $subcontractor->delete();
        }
        return false;
    }

}



