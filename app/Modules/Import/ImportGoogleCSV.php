<?php namespace App\Modules\Import;

use App\Models\Ab_import_history;
use App\Models\Address;
use App\Modules\Import\ImportInterface;
use Excel;
use App\Models\Address_book;
use App\Models\Ab_contact;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/6/2015
 * Time: 11:49 AM
 */
class ImportGoogleCSV implements ImportInterface
{

    private $file;

    public function load($f)
    {
        $this->file = $f;
        $map = $this->getMap();
        $companies = array();
        $i = 0;
        Excel::load($this->file, function ($reader) use ($map, &$companies, &$i) {
            $reader->each(function ($row) use ($map, &$companies, &$i) {
                //if there is a company name insert in address book

                if ($row->organization_1_name) {

                    if (!in_array($row->organization_1_name, $companies)) {
                        $company = Address_book::where('name', '=', $row->organization_1_name)->where('owner_comp_id', '=', Auth::user()->comp_id)->first();
                        if ($company) {
                            $companyName = $row->organization_1_name. "- imported duplicate";
                        } else {
                            $companyName = $row->organization_1_name;
                        }
                        $address_book = Address_book::create(['name' => $companyName, 'owner_comp_id' => Auth::user()->comp_id]);
                        $companies[$address_book->id] = $row->organization_1_name;
                        $addressBookId = $address_book->id;
                    } else {
                        $addressBookId = array_search($row->organization_1_name, $companies);
                    }

                    $officeId = null;
                    $j = 1;
                    while (isset($row['address_'.$j.'_type']) && isset($row['address_'.$j.'_street'])) {

                        if ((isset($row['address_'.$j.'_type']) && $row['address_'.$j.'_type'] == 'Work') && ($row['address_'.$j.'_street'] || $row['address_'.$j.'_postal_code'])) {
                            $office = Address::where('street', '=', $row['address_'.$j.'_street'])
                                ->where('zip', '=', $row['address_'.$j.'_postal_code'])
                                ->where('ab_id', '=', $addressBookId)
                                ->first();

                            if ($office) {
                                $officeId = $office->id;
                            } else {
                                $i++;
                                $insertedAddress = Address::create([
                                    'office_title' => ((!empty($row['address_'.$j.'_city']))?$row['address_'.$j.'_city']:''). " Office ".$i,
                                    'street' => (!empty($row['address_'.$j.'_street']))?$row['address_'.$j.'_street']:'',
                                    'zip' => (!empty($row['address_'.$j.'_postal_code']))?$row['address_'.$j.'_postal_code']:'',
                                    'city' => (!empty($row['address_'.$j.'_city']))?$row['address_'.$j.'_city']:'',
                                    'state' => (!empty($row['address_'.$j.'_region']))?$row['address_'.$j.'_region']:'',
                                    'ab_id' => $addressBookId
                                ]);

                                $officeId = $insertedAddress->id;
                            }
                        }

                        $j++;
                    }


                    //prepare contact

                    //trim
                    $row->phone_1_type = str_replace("\0", "", $row->phone_1_type);
                    $row->phone_2_type = str_replace("\0", "", $row->phone_2_type);
                    $row->e_mail_1_type = str_replace("\0", "", $row->e_mail_1_type);
                    $row->e_mail_2_type = str_replace("\0", "", $row->e_mail_2_type);

                    //assign correct email
                    if (strpos($row->e_mail_1_type, 'Work') || (!empty($row->e_mail_1_type[0]) && ($row->e_mail_1_type[0] == trim('*')))) {
                        $email = $row->e_mail_1_value;
                    } else if (strpos($row->e_mail_2_type, 'Work') || (!empty($row->e_mail_2_type[0]) && ($row->e_mail_2_type[0] == trim('*')))) {
                        $email = $row->e_mail_2_value;
                    } else $email = NULL;

                    //first or create or instance new object
                    if (isset($email)) {
                        $contact = Ab_contact::firstOrCreate(['ab_id'=>$addressBookId,'email'=>$email, 'address_id' => $officeId]);
                        //map attributes
                        $contact->name = $row->given_name . ' ' . $row->additional_name . ' ' . $row->family_name;
                        $contact->title = $row->occupation;
                        $contact->fax = null;
                        $contact->email = $email;

                        if ($row->phone_1_type == 'Mobile') {
                            $contact->cell_phone = $row->phone_1_value;
                        } else if ($row->phone_2_type == 'Mobile') {
                            $contact->cell_phone = $row->phone_2_value;
                        } else $contact->cell_phone = NULL;

                        if ($row->phone_1_type == 'Work') {
                            $contact->office_phone = $row->phone_1_value;
                        } else if ($row->phone_2_type == 'Work') {
                            $contact->office_phone = $row->phone_2_value;
                        } else $contact->office_phone = NULL;

                        $contact->save();

                    }

                }
            });
        });

        if (!empty($companies)) {
            //delete all old entries
            Ab_import_history::where('user_id', '=', Auth::user()->id)->delete();
            //insert new history entry
            Ab_import_history::create(['user_id' => Auth::user()->id, 'ab_ids' => implode(',', array_keys($companies))]);
        }

    }

    public function getMap()
    {
        $map = array(
            'name' => array('given_name', 'additional_name', 'family_name'),
            'title' => 'occupation',
            'email' => array('e_mail_1_value', 'e_mail_2_value'),
            'office_phone' => array('phone_1_value', 'phone_2_value'),
            'cell_phone' => array('phone_1_value', 'phone_2_value'),
            'fax' => '',
        );
        return $map;
    }
}
