<?php
namespace App\Modules\Import;
/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/6/2015
 * Time: 11:28 AM
 */
class ImportFile {
    private $file;

    function __construct($file) {
        $this->file = $file;
    }

    function load(ImportInterface $interface){
        $interface->load($this->file);
    }
}