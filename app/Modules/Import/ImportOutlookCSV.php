<?php
namespace App\Modules\Import;
use App\Models\Ab_custom_category;
use App\Models\Ab_default_category;
use App\Models\Ab_import_history;
use App\Models\Address;
use App\Models\Custom_category;
use App\Models\Default_category;
use App\Models\Master_Format;
use App\Models\Mf_address_book;
use App\Modules\Import\ImportInterface;
use Excel;
use App\Models\Address_book;
use App\Models\Ab_contact;
use Auth;
/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/6/2015
 * Time: 11:49 AM
 */
class ImportOutlookCSV implements ImportInterface {

    private $file;
    public function load($f) {
        $this->file = $f;
        $map = $this->getMap();
        $import = false;
        $companies = array();
        $i = 0;
        Excel::load($this->file, function ($reader) use ($map, $import, &$companies, &$i) {
            $reader->each(function ($row) use ($map, $import, &$companies, &$i) {

                //if there is a company name insert in address book
                if ($row->company) {
                    if (!in_array($row->company, $companies)) {
                        $company = Address_book::where('name', '=', $row->company)->where('owner_comp_id', '=', Auth::user()->comp_id)->first();
                        if ($company) {
                            $companyName = $row->company. "- imported duplicate";
                        } else {
                            $companyName = $row->company;
                        }
                        $address_book = Address_book::create(['name' => $companyName, 'owner_comp_id' => Auth::user()->comp_id]);
                        $companies[$address_book->id] = $row->company;
                        $addressBookId = $address_book->id;
                    } else {
                        $addressBookId = array_search($row->company, $companies);
                    }

                    if (!empty($row->user_1)) {
                        $masterFormatNumbers = explode(";", $row->user_1);

                        foreach ($masterFormatNumbers as $masterFormatNumber) {
                            $mf = Master_Format::where('number', '=', $masterFormatNumber)->first();
                            if ($mf) {
                                Mf_address_book::firstOrCreate(['ab_id' => $addressBookId, 'mf_id' => $mf->id]);
                            }
                        }
                    }

                    if (!empty($row->user_2)) {
                        $addressBookCategories = explode(";", $row->user_2);

                        foreach ($addressBookCategories as $addressBookCategory) {
                            $defaultCategory = Default_category::where('name', '=', $addressBookCategory)->first();
                            if ($defaultCategory) {
                                Ab_default_category::firstOrCreate(['ab_id' => $addressBookId, 'default_category_id' => $defaultCategory->id]);
                            } else {
                                $customCategory = Custom_category::where('name', '=', $addressBookCategory)
                                                                 ->where('comp_id', '=', Auth::user()->comp_id)
                                                                 ->first();

                                if ($customCategory) {
                                    Ab_custom_category::firstOrCreate(['ab_id' => $addressBookId, 'custom_category_id' => $customCategory->id]);
                                } else {
                                    $customCategory = Custom_category::firstOrCreate(['name' => $addressBookCategory, 'comp_id' => Auth::user()->comp_id]);
                                    Ab_custom_category::firstOrCreate(['ab_id' => $addressBookId, 'custom_category_id' => $customCategory->id]);
                                }
                            }
                        }
                    }


                    $officeId = null;
                    if ($row->business_street || $row->business_postal_code) {
                        $office = Address::where('street', '=', $row->business_street)
                            ->where('zip', '=', $row->business_postal_code)
                            ->where('ab_id', '=', $addressBookId)
                            ->first();

                        if ($office) {
                            $officeId = $office->id;
                        } else {
                            $i++;
                            $insertedAddress = Address::create([
                                'office_title' => ((!empty($row->business_city))?$row->business_city:''). " - office ".$i,
                                'street' => (!empty($row->business_street))?$row->business_street:'',
                                'zip' => (!empty($row->business_postal_code))?$row->business_postal_code:'',
                                'city' => (!empty($row->business_city))?$row->business_city:'',
                                'state' => (!empty($row->business_state))?$row->business_state:'',
                                'ab_id' => $addressBookId
                            ]);

                            $officeId = $insertedAddress->id;
                        }
                    }

                    //create contact by email and address book id
                    $contact = Ab_contact::create(['ab_id' => $addressBookId, 'email' => $row->e_mail_address, 'address_id' => $officeId]);

                    $contact->name = $row->first_name . ' ' . $row->last_name;
                    $contact->title = '';
                    if (!empty($row->title)) {
                        $contact->title = $row->title;
                    } else if (!empty($row->job_title)) {
                        $contact->title = $row->job_title;
                    }

                    $contact->email = (!empty($row->e_mail_address))?$row->e_mail_address:'';
                    $contact->office_phone = (!empty($row->business_phone))?$row->business_phone:'';
                    $contact->cell_phone = (!empty($row->mobile_phone))?$row->mobile_phone:'';
                    $contact->fax = '';
                    if (!empty($row->business_fax)) {
                        $contact->fax = $row->business_fax;
                    } else if (!empty($row->home_fax)) {
                        $contact->fax = $row->home_fax;
                    } else if (!empty($row->other_fax)) {
                        $contact->fax = $row->other_fax;
                    }

                    $contact->save();
                }
            });
        });

        if (!empty($companies)) {
            //delete all old entries
            Ab_import_history::where('user_id', '=', Auth::user()->id)->delete();
            //insert new history entry
            Ab_import_history::create(['user_id' => Auth::user()->id, 'ab_ids' => implode(',', array_keys($companies))]);
        }
    }

    public function getMap() {
        $map = array(
            'name' => array('first_name','last_name'),
            'title' => 'title',
            'email' => 'e_mail_address',
            'office_phone' => 'business_phone',
            'cell_phone' =>  'primary_phone',
            'fax' => 'business_fax',
        );
        return $map;
    }
}