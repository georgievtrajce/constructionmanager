<?php namespace App\Modules\Import;
/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/6/2015
 * Time: 11:28 AM
 */
interface ImportInterface
{
    public function load($file);
    public function getMap();
}