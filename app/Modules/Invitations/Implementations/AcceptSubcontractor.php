<?php
namespace App\Modules\Invitations\Implementations;

use App\Models\Project;
use App\Models\Project_company;
use App\Models\Subcontractor_project_info;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Invitations\Interfaces\AcceptInvitationInterface;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class AcceptSubcontractor implements AcceptInvitationInterface
{
    private $projectSubcontractorsRepo;
    private $usersRepo;
    private $abContactsRepo;
    private $addressBookRepo;
    private $projectPermissionsRepo;
    private $referralsRepo;

    public function __construct()
    {
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->usersRepo = new UsersRepository();
        $this->abContactsRepo = new ContactsRepository();
        $this->addressBookRepo = new AddressBookRepository();
        $this->projectPermissionsRepo = new ProjectPermissionsRepository();
        $this->referralsRepo = new ReferralsRepository();
    }

    public function redirectToRegister($data)
    {
        return Redirect::to('auth/register?referral_token='.$data['referralToken'].'&sub_token='.$data['token'].'&projectCompanyId='.$data['projectCompanyId'].'&bidderId='.$data['bidderId']);
    }

    /**
     * Accept invitation for a shared project for already registered company
     * @param $data
     * @return bool|null
     */
    public function accept($data)
    {
        //get address book contact with address book company id
        $addressBookContact = $this->abContactsRepo->getContactByEmail($data['email'], $data['contactId'], $data['parentCompanyId']);

        if ($data['registeredCompanyId'] == Auth::user()->comp_id) {
            //sync address book company with registered company
            if (!is_null($addressBookContact)) {
                $this->addressBookRepo->syncCompanies(Auth::user()->comp_id, $addressBookContact->ab_id, $data['parentCompanyId']);

                //delete sent referrals because this company is already registered
                $abContactEmails = $this->abContactsRepo->getAbContactEmails($addressBookContact->ab_id);

                foreach ($abContactEmails as $email) {
                    $this->referralsRepo->deleteReferralsToContacts($email, $data['parentCompanyId']);
                }
            }

            //update subcontractor status to accepted
            $projectSubcontractor = $this->projectSubcontractorsRepo->updateSubcontractor($data['token'], Auth::user()->comp_id, Config::get('constants.projects.shared_status.accepted'));

            $this->projectSubcontractorsRepo->addProjectInfoToSubcontractor($data);

            if (is_null($projectSubcontractor)) {
                return null;
            }

            //update company permissions with the child company id (subcontractor)
            $permissions = $this->projectPermissionsRepo->updatePermissionsSubcontractor($data['projectCompanyId'], $projectSubcontractor->proj_id, $projectSubcontractor->comp_parent_id, Auth::user()->comp_id);

            if ($permissions) {
                return true;
            }
        }

        return null;
    }

    /**
     * Get invitation acceptance
     * @param $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|null|void
     */
    public function getInvitation($data)
    {
        //get stored subcontractor
        $subcontractor = $this->projectSubcontractorsRepo->getSubcontractorByToken($data['token']);

        //check if the company is stored as a subcontractor during invitation
        if (!is_null($subcontractor)) {
            //check if the user is already registered in the system
            $registeredCompany = $this->usersRepo->getRegisteredUserByEmail($data['email']);

            if (!is_null($registeredCompany)) {
                $data['registeredCompanyId'] = $registeredCompany->comp_id;
                return $this->accept($data);
            }

            //this user is not registered
            return $this->redirectToRegister($data);
        }

        return null;
    }

}