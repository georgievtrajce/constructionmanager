<?php
namespace App\Modules\Invitations\Implementations;

use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Invitations\Interfaces\AcceptInvitationInterface;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class AcceptBidder implements AcceptInvitationInterface
{

    private $proposalsRepo;
    private $usersRepo;
    private $abContactsRepo;
    private $addressBookRepo;
    private $projectPermissionsRepo;
    private $referralsRepo;
    private $projectSubcontractorsRepo;

    public function __construct()
    {
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->proposalsRepo = new ProposalsRepository();
        $this->usersRepo = new UsersRepository();
        $this->abContactsRepo = new ContactsRepository();
        $this->addressBookRepo = new AddressBookRepository();
        $this->projectPermissionsRepo = new ProjectPermissionsRepository();
        $this->referralsRepo = new ReferralsRepository();
    }

    /**
     * Redirect to register page if invited company is not registered
     * @param $data
     * @return mixed
     */
    public function redirectToRegister($data)
    {
        return Redirect::to('auth/register?referral_token='.$data['referralToken'].'&sub_token='.$data['token'].'&projectCompanyId='.$data['projectCompanyId'].'&bidderId='.$data['bidderId']);
    }

    /**
     * Accept invitation to bid for already registered company
     * @param $data
     * @return bool|null
     */
    public function accept($data)
    {
        //get address book contact with address book company id
        $addressBookContact = $this->abContactsRepo->getContactByEmail($data['email'], $data['contactId'], $data['parentCompanyId']);

        if ($data['registeredCompanyId'] == Auth::user()->comp_id) {
            //sync address book company with registered company
            if (!is_null($addressBookContact)) {
                $this->addressBookRepo->syncCompanies(Auth::user()->comp_id, $addressBookContact->ab_id, $data['parentCompanyId']);

                //delete sent referrals because this company is already registered
                $abContactEmails = $this->abContactsRepo->getAbContactEmails($addressBookContact->ab_id);

                foreach ($abContactEmails as $email) {
                    $this->referralsRepo->deleteReferralsToContacts($email, $data['parentCompanyId']);
                }
            }

            //update bidder status to accepted
            $projectBidder = $this->proposalsRepo->acceptBidder($data['token'], $data['bidderId'], Auth::user()->comp_id, Config::get('constants.bids.status.accepted'));

            $data['bidder'] = true;

            $this->projectSubcontractorsRepo->addProjectInfoToSubcontractor($data);

            if (is_null($projectBidder)) {
                return null;
            }

            return true;
        }

        return null;
    }

    /**
     * Get invitation acceptance
     * @param $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|null|void
     */
    public function getInvitation($data)
    {
        //get stored subcontractor
        $bidder = $this->proposalsRepo->getProjectBidderByToken($data['token'], $data['bidderId']);

        //check if the company is stored as a subcontractor during invitation
        if (!is_null($bidder)) {
            //check if the user is already registered in the system
            $registeredCompany = $this->usersRepo->getRegisteredUserByEmail($data['email']);

            if (!is_null($registeredCompany)) {
                $data['registeredCompanyId'] = $registeredCompany->comp_id;
                return $this->accept($data);
            }

            //this user is not registered
            return $this->redirectToRegister($data);
        }

        return null;
    }

}