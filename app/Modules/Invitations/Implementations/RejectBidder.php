<?php
namespace App\Modules\Invitations\Implementations;


use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Invitations\Interfaces\RejectInvitationInterface;
use App\Modules\Proposals\Repositories\ProposalsRepository;

class RejectBidder implements RejectInvitationInterface
{

    private $referralsRepo;
    private $proposalsRepo;

    public function __construct()
    {
        $this->referralsRepo = new ReferralsRepository();
        $this->proposalsRepo = new ProposalsRepository();
    }

    /**
     * Reject invitation to bid
     * @param $data
     * @return bool
     */
    public function reject($data)
    {
        //delete referral
        $deleteReferral = $this->referralsRepo->deleteSingleReferral($data['refToken']);

        //delete bidder
        $updateBidder = $this->proposalsRepo->rejectAllBidderRecords($data['subToken']);

        if ($deleteReferral !== false && $updateBidder !== false) {
            return true;
        }
        return false;
    }

}