<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/13/2016
 * Time: 9:51 AM
 */

namespace App\Modules\Invitations\Implementations;


use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Invitations\Interfaces\RejectInvitationInterface;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;

class RejectSubcontractor implements RejectInvitationInterface
{

    private $referralsRepo;
    private $projectSubcontractorsRepo;

    public function __construct()
    {
        $this->referralsRepo = new ReferralsRepository();
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
    }

    /**
     * Reject the project invitation in companies module
     * @param $data
     * @return bool
     */
    public function reject($data)
    {
        //delete referral
        $deleteReferral = $this->referralsRepo->deleteSingleReferral($data['refToken']);

        //delete project subcontractor
        $deleteProjectSubcontractor = $this->projectSubcontractorsRepo->deleteProjectSubcontractor($data['subToken']);

        if ($deleteReferral !== false && $deleteProjectSubcontractor !== false) {
            return true;
        }
        return false;
    }

}