<?php
namespace App\Modules\Invitations\Interfaces;


interface RejectInvitationInterface
{

    public function reject($data);

}