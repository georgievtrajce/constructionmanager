<?php
namespace App\Modules\Invitations\Interfaces;

interface AcceptInvitationInterface
{

    public function redirectToRegister($data);
    public function accept($data);
    public function getInvitation($data);

}