<?php namespace App\Modules\Auto_complete\Interfaces;
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/18/2015
 * Time: 4:20 PM
 */

interface AutoCompleteInterface {
    public function getData($query, $projectId);
    public function writeData($data);
}