<?php

namespace App\Modules\Auto_complete\Repositories;
use App\Models\Address_book;
use App\Models\Master_Format;
use App\Models\Company;
use App\Models\Project_company;
use App\Models\Project_subcontractor;
use App\Modules\Project\Repositories\ProjectRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/19/2015
 * Time: 10:33 AM
 */

class AutoCompleteRepository {

    private $companyId;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * AutoCompleteRepository constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
        $this->projectRepository = new ProjectRepository();
    }

    /**
     * Get master format entries by number like
     * @param $query
     * @return mixed
     */
    public function getMasterFormatNumbers($query, $masterFormatTypeId)
    {
        $type = $query;
        return Master_Format::numbers($type, $masterFormatTypeId);
    }

    /**
     * Get master format entries by title like
     * @param $query
     * @return mixed
     */
    public function getMasterFormatTitles($query, $masterFormatTypeId)
    {
        $type = $query;
        return Master_Format::titles($type, $masterFormatTypeId);
    }

    /**
     * Search master format entries by number
     * @param $data
     * @return mixed
     */
    public function searchByMasterFormatNumber($data)
    {
        $response =  Master_Format::getEntryByNumber($data);

        return $response;
    }

    /**
     * Search master format entries by title
     * @param $data
     * @return mixed
     */
    public function searchByMasterFormatTitle($data)
    {
        $response = Master_Format::getEntryByTitle($data);

        return $response;

    }

    /**
     * Get address book entry with relations specified by name
     * @param $data
     * @param $projectId
     * @return mixed
     */
    public function getAddressBookEntry($data, $projectId)
    {
        $addressBookCompanies = Address_book::where('owner_comp_id','=',Auth::user()->comp_id)
            ->where('name','LIKE', $data.'%');

        //check if the request is for project companies module
        //and if it is exclude the already added companies
        if ($projectId != 0) {
            $addressBookCompanies->leftJoin('project_companies', function($leftJoin) use ($projectId)
                {
                    $leftJoin->on('project_companies.ab_id', '=', 'address_book.id')
                        ->where('project_companies.proj_id','=',$projectId);
                })
                ->whereNull('project_companies.ab_id')
                ->select('address_book.*');
        }

        return $addressBookCompanies->with('addresses')
            ->with('addresses.ab_contacts')
            ->with('contacts')
            ->with('default_categories')
            ->with('custom_categories')
            ->orderBy('name')
            ->take(10)
            ->get();
    }

    /**
     * Get address book entry with relations specified by name
     * @param $data
     * @param $projectId
     * @return mixed
     */
    public function getCompaniesAddressBookEntry($data, $projectId)
    {
        $projectCompanies = Project_company::where('proj_id','=',$projectId)
            ->join('address_book', function($join) use ($data)
            {
                $join->on('address_book.id', '=', 'project_companies.ab_id')
                    ->where('address_book.owner_comp_id','=',Auth::user()->comp_id)
                    ->where('address_book.name','LIKE', $data.'%');
            })
//            ->whereHas('address_book', function($query) use ($data) {
//                $query->where('owner_comp_id','=',Auth::user()->comp_id)
//                    ->where('name','LIKE', $data.'%');
//            })
            ->select(
                'project_companies.*',
                'address_book.name as ab_name'
            )
            ->with('address_book')
            ->with('project_company_contacts')
            ->with('project_company_contacts.contact')
            ->orderBy('ab_name')
            ->take(10)
            ->get();

        return $projectCompanies;


//        $addressBookCompanies = Address_book::where('owner_comp_id','=',Auth::user()->comp_id)
//            ->where('name','LIKE', $data.'%')
//            ->join('project_companies', function($leftJoin) use ($projectId)
//            {
//                $leftJoin->on('project_companies.ab_id', '=', 'address_book.id')
//                    ->where('project_companies.proj_id','=',$projectId);
//            })
//            ->select('address_book.*');
//
//        //check if the request is for project companies module
//        //and if it is exclude the already added companies
//        if ($projectId != 0) {
//            $addressBookCompanies->leftJoin('project_companies', function($leftJoin) use ($projectId)
//            {
//                $leftJoin->on('project_companies.ab_id', '=', 'address_book.id')
//                    ->where('project_companies.proj_id','=',$projectId);
//            })
//                ->whereNull('project_companies.ab_id')
//                ->select('address_book.*');
//        }
//
//        return $addressBookCompanies->with('addresses')
//            ->with('addresses.ab_contacts')
//            ->with('contacts')
//            ->with('default_categories')
//            ->with('custom_categories')
//            ->orderBy('name')
//            ->take(10)
//            ->get();
    }

    public function getCompaniesSharedAddressBookEntry($data, $projectId)
    {
        $project = $this->projectRepository->getProjectRecord($projectId);

        $projectCompanies = Project_company::where('proj_id', '=', $projectId)
            ->join('address_book', function ($join) use ($data, $project) {
                $join->on('address_book.id', '=', 'project_companies.ab_id')
                    ->where('address_book.owner_comp_id', '=', $project->comp_id)
                    ->where('address_book.name', 'LIKE', $data . '%');
            })
//            ->whereHas('address_book', function($query) use ($data) {
//                $query->where('owner_comp_id','=',Auth::user()->comp_id)
//                    ->where('name','LIKE', $data.'%');
//            })
            ->select(
                'project_companies.*',
                'address_book.name as ab_name'
            )
            ->with('address_book')
            ->with('project_company_contacts')
            ->with('project_company_contacts.contact')
            ->orderBy('ab_name')
            ->take(10)
            ->get();

        return $projectCompanies;
    }



    /**
     * Get specified address book entry specified by name
     * @param $data
     * @return mixed
     */
    public function getAddressBookEntryByTitle($data){
        return Address_book::where('owner_comp_id','=',Auth::user()->comp_id)
            ->where('name','=', $data)
            ->get()
            ->first();
    }

    /**
     * Get address book entry categories
     * @param $data
     * @return mixed
     */
    public function getAddressBookEntryCategories($data){
        return Address_book::where('owner_comp_id','=',Auth::user()->comp_id)
            ->with('default_categories')
            ->with('custom_categories')
            ->where('name','=', $data)
            ->get()
            ->first();
    }

    /**
     * Get registered company with relations
     * @param $data
     * @return mixed
     */
    public function getCompanyEntry($data){
        return Company::where('name','LIKE', $data.'%')
            ->with('addresses')
            ->orderBy('name')
            ->take(10)
            ->get();
    }

    /**
     * Get project subcontractors
     * @param $data
     * @return mixed
     */
    public function getProjectSubcontractors($data)
    {
        $parentContractor = DB::table('project_subcontractors')->where('proj_id','=',Request::segment(2))
                                    ->where('comp_child_id','=',$this->companyId)
                                    ->join('companies', function($join) use($data){
                                        $join->on('companies.id','=','comp_parent_id');
                                    })
                                    ->select('project_subcontractors.*',
                                        'companies.id as company_id',
                                        'companies.name as company_name')
                                    ->where('companies.name','LIKE',$data.'%');

        return DB::table('project_subcontractors')->where('proj_id','=',Request::segment(2))
                                    ->where('comp_parent_id','=',$this->companyId)
                                    ->join('companies', function($join) use($data){
                                        $join->on('companies.id','=','comp_child_id');
                                    })
                                    ->select('project_subcontractors.*',
                                        'companies.id as company_id',
                                        'companies.name as company_name')
                                    ->where('companies.name','LIKE',$data.'%')
                                    ->union($parentContractor)
                                    ->orderBy('company_name','desc')
                                    ->take(10)
                                    ->get();
    }
}