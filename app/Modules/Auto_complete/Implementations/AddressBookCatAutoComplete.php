<?php
namespace App\Modules\Auto_complete\Implementations;

use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class AddressBookCatAutoComplete implements AutoCompleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * Get data for categories auto complete
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        $response = [];
        $results = $this->repo->getAddressBookEntry($query, $projectId);
        foreach($results as $result){
            $response[] = (string)$result->name;
        }
        return $response;
    }

    /**
     * Write auto completed address book entry categories in input (deprecated method)
     * @param $data
     * @return array
     */
    public function writeData($data)
    {
        $results =  $this->repo->getAddressBookEntryCategories($data);
        $categories = array();

        //if there are any default categories
        if (isset($results->default_categories)){
            foreach($results->default_categories as $default){
                array_push($categories,$default->name);
            }
        }

        //if there are any custom categories
        if (isset($results->custom_categories)) {
            foreach ($results->custom_categories as $custom) {
                array_push($categories,$custom->name);
            }
        }

        //implode categories and split them with comma
        $cat = implode(", ",$categories);

        //prepare response array
        $result = [ 'id'=>$results->id, 'categories'=>$cat];
        return $result;
    }

}