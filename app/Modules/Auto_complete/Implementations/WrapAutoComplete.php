<?php
namespace App\Modules\Auto_complete\Implementations;

use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;

class WrapAutoComplete {

    /**
     * Get autocomplete data by search segment
     * @param AutoCompleteInterface $i
     * @param $query
     * @param $projectId
     * @return
     */
    public function search(AutoCompleteInterface $i, $query, $projectId)
    {
        return $i->getData($query, $projectId);
    }

}