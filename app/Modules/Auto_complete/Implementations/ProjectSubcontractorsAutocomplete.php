<?php
namespace App\Modules\Auto_complete\Implementations;


use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class ProjectSubcontractorsAutocomplete implements AutoCompleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * Get project subcontractors specified by some query string
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        $response = [];
        $results = $this->repo->getProjectSubcontractors($query);

        foreach($results as $result) {
            $response[] = [
                'id' => (string)$result->company_id,
                'name' => (string)$result->company_name
            ];
        }

        return $response;
    }

    public function writeData($data)
    {
        //
    }

}