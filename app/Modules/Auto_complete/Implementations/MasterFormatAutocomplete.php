<?php
namespace App\Modules\Auto_complete\Implementations;

use App\Models\Master_format_type;
use App\Models\Project;
use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class MasterFormatAutocomplete implements AutoCompleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * Get master format entries specified by some query string
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        if (!empty($projectId)) {
            $project = Project::find($projectId);
            $masterFormatTypeId = $project->master_format_type_id;
        } else {
            $masterFormatType = Master_format_type::orderBy('year', 'desc')->first();
            $masterFormatTypeId = $masterFormatType->id;
        }

        $response = [];
        if(is_numeric($query))
        {
            $numbersResults = $this->repo->getMasterFormatNumbers($query, $masterFormatTypeId);

            foreach($numbersResults as $numbersResult){
                $response[] = [
                    'id' => (string)$numbersResult->id,
                    'number' => (string)$numbersResult->number,
                    'title' => (string)$numbersResult->title
                ];
            }

            return $response;
        }

        $titlesResults = $this->repo->getMasterFormatTitles($query, $masterFormatTypeId);
        foreach($titlesResults as $titlesResult){
            $response[] = [
                'id' => (string)$titlesResult->id,
                'number' => (string)$titlesResult->number,
                'title' => (string)$titlesResult->title
            ];
        }
        return $response;
    }

    /**
     * Write selected master format entry in input
     * @param $data
     * @return mixed
     */
    public function writeData($data)
    {
        if(is_numeric($data))
        {
            return $this->repo->searchByMasterFormatNumber($data);
        }

        return $this->repo->searchByMasterFormatTitle($data);
    }

}