<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/27/2016
 * Time: 1:05 PM
 */

namespace App\Modules\Auto_complete\Implementations;


use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class CompaniesAddressBookAutocomplete implements AutoCompleteInterface
{

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * Get address book entries specified by some query string
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        $response = [];
        $results = $this->repo->getCompaniesAddressBookEntry($query, $projectId);

        for($i=0; $i<count($results); $i++){
            //create the response array for the auto complete controller
            $response[$i] = [
                'id' => (string)$results[$i]->ab_id,
                'proj_comp_id' => (string)$results[$i]->id,
                'ab_addr_id' => (string)$results[$i]->addr_id,
            ];

            if (!is_null($results[$i]->address_book)) {
                $response[$i]['name'] = $results[$i]->address_book->name;
            } else {
                $response[$i]['name'] = 'Unknown';
            }

            if (count($results[$i]->project_company_contacts)) {
                for ($j=0; $j<count($results[$i]->project_company_contacts); $j++) {
                    if (!is_null($results[$i]->project_company_contacts[$j]->contact)) {
                        $response[$i]['contacts'][$j]['id'] = $results[$i]->project_company_contacts[$j]->contact->id;
                        $response[$i]['contacts'][$j]['name'] = $results[$i]->project_company_contacts[$j]->contact->name;
                        $response[$i]['contacts'][$j]['title'] = $results[$i]->project_company_contacts[$j]->contact->title;
                        $response[$i]['contacts'][$j]['email'] = $results[$i]->project_company_contacts[$j]->contact->email;
                    } else {
                        $response[$i]['contacts'][$j] = null;
                    }
                }

            } else {
                $response[$i]['contacts'] = [];
            }

        }

        return $response;
    }

    /**
     * Write selected entry from the auto complete to an input (deprecated method)
     * @param $data
     * @return mixed
     */
    public function writeData($data)
    {
        return $this->repo->getAddressBookEntryByTitle($data);
    }

}