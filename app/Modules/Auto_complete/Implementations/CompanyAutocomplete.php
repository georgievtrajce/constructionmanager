<?php
namespace App\Modules\Auto_complete\Implementations;

use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class CompanyAutocomplete implements AutoCompleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        $response = [];
        $results = $this->repo->getCompanyEntry($query);
        foreach($results as $result){
            $response[] = [
                'id' => $result->id,
                'name' => (string)$result->name,
                'addresses' => $result->addresses,
            ];
        }
        return $response;
    }

    public function writeData($data)
    {

        $company = $this->repo->getCompanyEntryByTitle($data);

        return $company;
    }

}