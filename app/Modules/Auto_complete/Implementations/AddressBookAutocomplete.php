<?php
namespace App\Modules\Auto_complete\Implementations;

use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;
use App\Modules\Auto_complete\Repositories\AutoCompleteRepository;

class AddressBookAutocomplete implements AutoCompleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the contacts repository class
        $this->repo = new AutoCompleteRepository();
    }

    /**
     * Get address book entries specified by some query string
     * @param $query
     * @param $projectId
     * @return array
     */
    public function getData($query, $projectId)
    {
        $response = [];
        $results = $this->repo->getAddressBookEntry($query, $projectId);
        foreach($results as $result){
            $categories = array();
            //if there are any default categories
            if (isset($result->default_categories)){
                foreach($result->default_categories as $default){
                    array_push($categories,$default->name);
                }
            }

            //if there are any custom categories
            if (isset($result->custom_categories)) {
                foreach ($result->custom_categories as $custom) {
                    array_push($categories,$custom->name);
                }
            }

            //implode all categories, split them with comma
            $cat = implode(", ",$categories);

            //create the response array for the auto complete controller
            $response[] = [
                'id' => (string)$result->id,
                'name' => (string)$result->name,
                'addresses' => $result->addresses,
                'contacts' => $result->contacts,
                'categories' => $cat,
            ];
        }
        return $response;
    }

    /**
     * Write selected entry from the auto complete to an input (deprecated method)
     * @param $data
     * @return mixed
     */
    public function writeData($data)
    {
        return $this->repo->getAddressBookEntryByTitle($data);
    }

}