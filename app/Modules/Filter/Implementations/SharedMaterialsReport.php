<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/30/2015
 * Time: 1:16 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Materials_and_services\Repositories\MaterialsRepository;

class SharedMaterialsReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new MaterialsRepository();
    }

    /**
     * Generate shared pcos report
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportSharedMaterials($params);
    }

}