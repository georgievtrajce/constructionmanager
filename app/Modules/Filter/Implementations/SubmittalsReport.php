<?php
namespace App\Modules\Filter\Implementations;

use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Submittals\Repositories\SubmittalsRepository;

class SubmittalsReport implements FilterInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Report submittals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportSubmittals($params);
    }

}