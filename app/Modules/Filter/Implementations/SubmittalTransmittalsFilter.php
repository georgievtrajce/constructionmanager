<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/16/2015
 * Time: 10:19 AM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Submittals\Repositories\SubmittalsRepository;

class SubmittalTransmittalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Filter rfis
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSubmittalTransmittals($params);
    }

}