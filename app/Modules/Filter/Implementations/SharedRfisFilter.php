<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/28/2015
 * Time: 3:37 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Rfis\Repositories\RfisRepository;

class SharedRfisFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Filter submittals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSharedRfis($params);
    }

}