<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 9/25/2015
 * Time: 1:39 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Filter\Interfaces\FilterInterface;

class CompanyOfficeReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new AddressBookRepository();
    }

    /**
     * Generate office report with all contacts, address and contact details
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->getOfficeData($params);
    }

}