<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 9/25/2015
 * Time: 2:48 PM
 */
namespace App\Modules\Filter\Implementations;

use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Filter\Interfaces\FilterInterface;

class AddressBookContactReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new AddressBookRepository();
    }

    /**
     * Generate single contact report
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->getContactData($params);
    }

}