<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/15/2015
 * Time: 12:42 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class PcoTransmittalsDataFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Filter pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterPcoTransmittals($params);
    }

}