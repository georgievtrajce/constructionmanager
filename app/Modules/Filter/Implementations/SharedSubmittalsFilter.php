<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/28/2015
 * Time: 2:26 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Submittals\Repositories\SubmittalsRepository;

class SharedSubmittalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Filter submittals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSharedSubmittals($params);
    }

}