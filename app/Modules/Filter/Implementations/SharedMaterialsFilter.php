<?php

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Materials_and_services\Repositories\MaterialsRepository;

class SharedMaterialsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new MaterialsRepository();
    }

    /**
     * Filter shared materials
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSharedMaterials($params);
    }

}