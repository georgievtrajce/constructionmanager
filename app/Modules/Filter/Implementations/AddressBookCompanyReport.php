<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 9/22/2015
 * Time: 11:27 AM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Filter\Interfaces\FilterInterface;

class AddressBookCompanyReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new AddressBookRepository();
    }

    /**
     * Generate address book company report
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->getAddressBookCompanyData($params);
    }

}