<?php
namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Proposals\Repositories\ProposalsRepository;

class ProposalsReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new ProposalsRepository();
    }

    /**
     * Filter proposals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        if ($params['sort'] == 'proposal_final') {
            return $this->repo->reportProposalsSortedByFinalProposal($params);
        }

        return $this->repo->reportProposals($params);
    }

}