<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/28/2015
 * Time: 4:00 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Rfis\Repositories\RfisRepository;

class SharedRfisReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Filter rfis
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportSharedRfis($params);
    }

}