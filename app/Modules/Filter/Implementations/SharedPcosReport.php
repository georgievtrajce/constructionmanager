<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/29/2015
 * Time: 1:03 PM
 */

namespace App\Modules\Filter\Implementations;

use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class SharedPcosReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Generate shared pcos report
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportSharedPcos($params);
    }

}