<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/29/2015
 * Time: 1:02 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class SharedPcosFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Filter shared pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSharedPcos($params);
    }

}