<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/5/2015
 * Time: 1:43 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class PcosFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Filter pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterPcos($params);
    }

}