<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/14/2015
 * Time: 4:43 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Rfis\Repositories\RfisRepository;

class RfiTransmittalsDataFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Filter pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterRfiTransmittals($params);
    }

}