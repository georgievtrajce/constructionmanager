<?php
namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Rfis\Repositories\RfisRepository;

class RfisReport implements FilterInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Filter rfis
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportRfis($params);
    }

}