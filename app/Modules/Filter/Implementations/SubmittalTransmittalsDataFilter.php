<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 12/14/2015
 * Time: 2:32 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Submittals\Repositories\SubmittalsRepository;

class SubmittalTransmittalsDataFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Filter pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSubmittalTransmittals($params);
    }

}