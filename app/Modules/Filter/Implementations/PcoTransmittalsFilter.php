<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/16/2015
 * Time: 3:05 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class PcoTransmittalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Filter rfis
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterPcoTransmittals($params);
    }

}