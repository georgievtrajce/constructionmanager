<?php
namespace App\Modules\Filter\Implementations;


use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;

class ContractsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new ContractsRepository(new ProjectSubcontractorsRepository(), new ProjectPermissionsRepository());
    }

    /**
     * Filter contracts
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterContracts($params);
    }

}