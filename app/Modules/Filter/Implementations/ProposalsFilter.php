<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 1/26/2016
 * Time: 1:04 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;

class ProposalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new ProposalsRepository();
    }

    /**
     * Filter proposals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        if ($params['sort'] == 'proposal_final') {
            return $this->repo->filterProposalsSortedByFinalProposal($params);
        }

        return $this->repo->filterProposals($params);
    }

}