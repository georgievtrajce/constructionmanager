<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/16/2015
 * Time: 9:25 AM
 */

namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Rfis\Repositories\RfisRepository;

class RfiTransmittalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Filter rfis
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterRfiTransmittals($params);
    }

}