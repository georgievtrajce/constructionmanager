<?php
namespace App\Modules\Filter\Implementations;


use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class PcosReport implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Filter pcos
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->reportPcos($params);
    }

}