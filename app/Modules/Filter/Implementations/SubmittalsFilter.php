<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 5/14/2015
 * Time: 2:51 PM
 */

namespace App\Modules\Filter\Implementations;


use App\Models\Project_subcontractor;
use App\Models\Submittal;
use App\Modules\Filter\Interfaces\FilterInterface;
use App\Modules\Submittals\Repositories\SubmittalsRepository;

class SubmittalsFilter implements FilterInterface {

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Filter submittals
     * @param $params
     * @return array
     */
    public function getData($params)
    {
        return $this->repo->filterSubmittals($params);
    }

}