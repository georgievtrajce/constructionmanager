<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 5/14/2015
 * Time: 2:29 PM
 */
namespace App\Modules\Filter\Implementations;

use App\Modules\Filter\Interfaces\FilterInterface;

class FilterDataWrapper {

    /**
     * Implement the filter functionality
     * @param FilterInterface $filterInterface
     * @param $params
     * @return mixed
     */
    public function implementFilter(FilterInterface $filterInterface, $params)
    {
        return $filterInterface->getData($params);
    }

}