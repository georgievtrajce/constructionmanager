<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 5/14/2015
 * Time: 2:49 PM
 */
namespace App\Modules\Filter\Interfaces;

interface FilterInterface {

    public function getData($params);

}