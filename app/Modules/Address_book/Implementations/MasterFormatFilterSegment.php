<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/27/2015
 * Time: 11:16 AM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\MasterFormatFilterInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;

class MasterFormatFilterSegment implements MasterFormatFilterInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Filter address book entries by searched segment (number segment or title segment)
     * @param $searchData
     * @return mixed
     */
    public function filterData($searchData)
    {
        if(is_numeric($searchData['search']))
            return $this->repo->getEntriesByNumericSegment($searchData['search']);
        else
            return $this->repo->getEntriesByAlphabeticSegment($searchData['search']);
    }

}