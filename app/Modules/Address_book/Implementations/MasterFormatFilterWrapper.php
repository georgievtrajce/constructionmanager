<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/27/2015
 * Time: 10:20 AM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\MasterFormatFilterInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;

class MasterFormatFilterWrapper {

    private $repo;
    private  $type;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Implement the needed master format search class
     * @param MasterFormatFilterInterface $masterFormatFilterInterface
     * @param null $searchData
     * @return mixed
     */
    public function prepareFilter(MasterFormatFilterInterface $masterFormatFilterInterface, $searchData = NULL)
    {
        return $masterFormatFilterInterface->filterData($searchData);
    }


    /**
     * Switch the master format number and title filter. Iterate through searching by segment (number or title),
     * specific master format number or title or get all entries if any segment is searched
     * @param $searchData
     * @return mixed
     * @throws \Exception
     */
    public function getData($searchData)
    {
        if (!empty($searchData['companyName'])) {
            $this->type = 'company-name';
        } else {
            if(empty($searchData['search']))
                $this->type = 'mf-filter-all';
            if(empty($searchData['number']) && empty($searchData['title']))
                $this->type = 'mf-filter-segment';
            if(!empty($searchData['search']) && !empty($searchData['number']) && !empty($searchData['title']))
                $this->type = 'mf-filter-concrete';
        }

        return $this->prepareFilter(ClassMap::instance(Config::get('classmap.address-book.'.$this->type)), $searchData);
    }

}