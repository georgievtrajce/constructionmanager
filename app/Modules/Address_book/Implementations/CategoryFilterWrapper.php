<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/26/2015
 * Time: 3:01 PM
 */

namespace App\Modules\Address_book\Implementations;

use App\Modules\Address_book\Interfaces\CategoryFilterInterface;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;

class CategoryFilterWrapper {

    /**
     * Implement the needed category filter class
     * @param CategoryFilterInterface $categoryFilter
     * @param $id
     * @return mixed
     */
    public function prepareFilter(CategoryFilterInterface $categoryFilter, $id)
    {
        return $categoryFilter->getData($id);
    }

    /**
     * Implement the needed category filter class
     * @param CategoryFilterInterface $categoryFilter
     * @param $id
     * @return mixed
     */
    public function prepareFilterNotPaginated(CategoryFilterInterface $categoryFilter, $id)
    {
        return $categoryFilter->getDataNotPaginated($id);
    }
}