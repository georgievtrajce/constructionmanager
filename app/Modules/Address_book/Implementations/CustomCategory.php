<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/26/2015
 * Time: 4:55 PM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\CategoryFilterInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;

class CustomCategory implements CategoryFilterInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Filter address book entries by given custom category
     * @param $id
     * @return mixed
     */
    public function getData($id)
    {
        return $this->repo->getEntriesByCustomCategory($id);
    }

    /**
     * Filter address book entries by given custom category with no pagination
     * @param $id
     * @return mixed
     */
    public function getDataNotPaginated($id)
    {
        return $this->repo->getEntriesByCustomCategoryNotPaginated($id);
    }

}