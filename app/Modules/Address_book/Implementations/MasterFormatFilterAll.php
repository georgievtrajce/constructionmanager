<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/27/2015
 * Time: 11:15 AM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\MasterFormatFilterInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;

class MasterFormatFilterAll implements MasterFormatFilterInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Get all entries
     * @param $number
     * @return mixed
     */
    public function filterData($number)
    {
        return $this->repo->getAllEntries();
    }

}