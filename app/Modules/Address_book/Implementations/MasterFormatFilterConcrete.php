<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/27/2015
 * Time: 11:16 AM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\MasterFormatFilterInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;

class MasterFormatFilterConcrete implements MasterFormatFilterInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Filter address book entries by concrete master format number or title
     * @param $searchData
     * @return mixed
     */
    public function filterData($searchData)
    {
        if(is_numeric($searchData['search']))
            return $this->repo->getEntriesByNumber($searchData['number']);
        else
            return $this->repo->getEntriesByTitle($searchData['title']);
    }

}