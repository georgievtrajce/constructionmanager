<?php namespace App\Modules\Address_book\Implementations;

use App\Modules\Address_book\Interfaces\StoreAddressBookEntryInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Auto_complete\Interfaces\AutoCompleteInterface;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/23/2015
 * Time: 11:03 AM
 */

class StoreBasicInfo implements StoreAddressBookEntryInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * get all data from the store basic address book info form
     * @param array $data
     * @param null $company_id
     * @return static
     * @internal param null $company
     */
    public function postData(array $data, $company_id = NULL)
    {
        $syncedCompany = empty($data['company_id']) ? NULL : $data['company_id'];
        $company = $this->repo->storeCompany($data['name'], $syncedCompany, $data['note']);

        //store default categories if there are any
        if(array_key_exists('default_categories', $data)) {
            foreach($data['default_categories'] as $category)
            {
                if(!empty($category)) {
                    $this->repo->storeDefaultCategory($category, $company);
                }
            }
        }

        //store custom categories if there are any
        if(array_key_exists('custom_categories', $data)) {
            foreach ($data['custom_categories'] as $customCategory) {
                if (!empty($customCategory)) {
                    $this->repo->storeCustomCategory($customCategory, $company);
                }
            }
        }

        //multiple master format items store
        foreach($data as $key=>$value){
            if("mf_id_#" == substr($key,0,7)){
                $this->repo->storeMasterFormatItem($data[$key], $company);
            }
        }

        return $company;
    }

}