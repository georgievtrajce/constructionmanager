<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/30/2015
 * Time: 11:26 AM
 */

namespace App\Modules\Address_book\Implementations;


use App\Modules\Address_book\Interfaces\EditAddressBookEntryInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;

class UpdateBasicInfo implements EditAddressBookEntryInterface {

    private $repo;

    public function __construct()
    {
        //instance from the address book repository class
        $this->repo = new AddressBookRepository();
    }

    /**
     * Edit concrete address book company basic info implementation
     * @param $id
     * @param $data
     * @return mixed
     */
    public function editData($id, $data)
    {
        $company = $this->repo->updateCompany($id, $data);

        //delete current default categories
        if(count($company->default_categories) > 0) {
            $defaultCategoriesIdsArray = [];
            foreach($company->default_categories as $defaultCategoriesId) {
                $defaultCategoriesIdsArray[] = $defaultCategoriesId->id;
            }
            $this->repo->deleteDefaultCategories($company, $defaultCategoriesIdsArray);
        }

        //store new default categories
        if(array_key_exists('default_categories', $data)) {
            foreach ($data['default_categories'] as $defaultCategory) {
                if(!empty($defaultCategory)) {
                    $this->repo->storeDefaultCategory($defaultCategory, $company);
                }
            }
        }

        //delete current custom categories
        if(count($company->custom_categories) > 0) {
            $customCategoriesIdsArray = [];
            foreach($company->custom_categories as $customCategoriesId) {
                $customCategoriesIdsArray[] = $customCategoriesId->id;
            }
            $this->repo->deleteCustomCategories($company, $customCategoriesIdsArray);
        }

        //store new custom categories
        if(array_key_exists('custom_categories', $data)) {
            foreach ($data['custom_categories'] as $customCategory) {
                if (!empty($customCategory)) {
                    $this->repo->storeCustomCategory($customCategory, $company);
                }
            }
        }

        //delete current master format items
        if(count($company->master_format_items) > 0) {
            $masterFormatItemsIdsArray = [];
            foreach($company->master_format_items as $masterFormatItemId) {
                $masterFormatItemsIdsArray[] = $masterFormatItemId->id;
            }
            $this->repo->deleteMasterFormatItems($company, $masterFormatItemsIdsArray);
        }

        //store new master format items
        foreach($data as $key=>$value){
            if("mf_id_#" == substr($key,0,7)){
                $this->repo->storeMasterFormatItem($data[$key], $company);
            }
        }

        return $company;
    }

}