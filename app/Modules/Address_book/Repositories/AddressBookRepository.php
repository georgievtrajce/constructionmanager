<?php

namespace App\Modules\Address_book\Repositories;

use App\Models\Ab_contact;
use App\Models\Ab_custom_category;
use App\Models\Ab_default_category;
use App\Models\Ab_distribution;
use App\Models\Ab_file;
use App\Models\Ab_import_history;
use App\Models\Address;
use App\Models\Address_book;
use App\Models\Company;
use App\Models\Contact;
use App\Models\Custom_category;
use App\Models\Default_category;
use App\Models\Master_Format;
use App\Models\Project_company_rating;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class AddressBookRepository {

    private $companyId;

    public function __construct()
    {
        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
        }

    }

    /**
     * Get all address book entries
     * @param null $name
     * @return mixed
     */
    public function getAllEntries($name = null)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        $addressBookItems =  Address_book::allEntriesWithMasterFormat($this->companyId);

            if (!empty($name)) {
                $addressBookItems = $addressBookItems->where('name', 'like', $name['companyName'].'%');
            }

        return $addressBookItems->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->paginate(30);
    }

    /**
     * Get all address book entries
     * @return mixed
     */
    public function getAllEntriesNoPagination()
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        return Address_book::allEntriesWithMasterFormat($this->companyId)
            ->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->get();
    }

    /**
     * Get basic info for specific address book company
     * @param $id
     * @return mixed
     */
    public function getEntryBasicInfo($id)
    {
        return Address_book::getBasicInfo($id, $this->companyId);
    }

    /**
     * Get address book entry with addresses and contacts
     * @param $id
     * @return mixed
     */
    public function getEntryContactsAddresses($id)
    {
        return Address_book::with('addresses')->with('contacts')->where('id','=',$id)->first();
    }
    /**
     * Filter address book entries by some default category
     * @param $id
     * @return mixed
     */

    public function getEntriesByDefaultCategory($id)
    {
        return Address_book::whereDefaultCategoryExist($id)->paginate(30);
    }

    /**
     * Filter address book entries by some default category
     * @param $id
     * @return mixed
     */

    public function getEntriesByDefaultCategoryNotPaginated($id)
    {
        return Address_book::whereDefaultCategoryExist($id)->get();
    }

    /**
     * Filter address book entry by some custom category
     * @param $id
     * @return mixed
     */
    public function getEntriesByCustomCategory($id)
    {
        return Address_book::whereCustomCategoryExist($id)->paginate(30);
    }

    /**
     * Filter address book entry by some custom category
     * @param $id
     * @return mixed
     */
    public function getEntriesByCustomCategoryNotPaginated($id)
    {
        return Address_book::whereCustomCategoryExist($id)->get();
    }

    /**
     * Search address book entries by segment from the master format number
     * @param $segment
     * @return mixed
     */
    public function getEntriesByNumericSegment($segment)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        return Address_book::masterFormatNumberSegmentFilter($segment, $this->companyId)
            ->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->paginate(30);
    }

    /**
     * Search address book entries by segment from the master format title
     * @param $segment
     * @return mixed
     */
    public function getEntriesByAlphabeticSegment($segment)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        return Address_book::masterFormatTitleSegmentFilter($segment, $this->companyId)
            ->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->paginate(30);
    }

    /**
     * Search address book entries by master format number
     * @param $number
     * @return mixed
     */
    public function getEntriesByNumber($number)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        return Address_book::masterFormatNumberFilter($number, $this->companyId)
            ->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->paginate(30);
    }

    /**
     * Search address book entries by master format title
     * @param $title
     * @return mixed
     */
    public function getEntriesByTitle($title)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);
        return Address_book::masterFormatTitleFilter($title, $this->companyId)
            ->orderBy($sortArr[0], $sortArr[1])
            ->select(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->groupBy(['address_book.id', 'address_book.name', 'address_book.total_rating'])
            ->paginate(30);
    }

    /**
     * Get default categories
     * @return array
     */
    public function getDefaultCategories()
    {
        return Default_category::all()->toArray();
    }

    /**
     * Get custom categories
     * @return mixed
     */
    public function getCustomCategories()
    {
        return Custom_category::where('comp_id', '=', $this->companyId)->get()->toArray();
    }

    /**
     * Get specific number of last entered address book companies
     * @param $number
     * @return mixed
     */
    public function getLastAddressBookEntries($number)
    {
        return Address_book::where('owner_comp_id', '=', Auth::user()->comp_id)
            ->orderBy('created_at', 'desc')
            ->take($number)
            ->get();
    }

    /**
     * Get address book company with all basic info, offices and contacts
     * @param $id
     * @return mixed
     */
    public function getAddressBookCompanyData($id)
    {
        return Address_book::where('id','=',$id)
            ->where('owner_comp_id', '=', Auth::user()->comp_id)
            ->with('default_categories')
            ->with('custom_categories')
            ->with('master_format_items')
            ->with('addresses')
            ->with('addresses.ab_contacts')
            ->with('company')
            ->firstOrFail();
    }

    /**
     * Get address book company entries
     * @param $companyId
     * @return mixed
     */
    public function getAddressBookEntriesByCompany($companyId)
    {
        return Address_book::where('owner_comp_id', '=', $companyId)
            ->with('addresses')
            ->get();
    }

    /**
     * Get company office address and its contacts and details
     * @param $id
     */
    public function getOfficeData($id)
    {
        return Address::where('id','=',$id)
            ->with('address_book')
            ->with('ab_contacts')
            ->firstOrFail();
    }

    public function getContactData($id)
    {
        return Contact::where('id','=',$id)
            ->with('office')
            ->with('office.address_book')
            ->firstOrFail();
    }

    /**
     * Get current category (default or custom)
     * @param $categoryType
     * @param $categoryId
     * @return mixed
     */
    public function getCurrentCategory($categoryType, $categoryId)
    {
        if ($categoryType == Config::get('constants.address_book_categories.default')) {
            $category = Default_category::where('id','=',$categoryId)->firstOrFail();
        } else {
            $category = Custom_category::where('id','=',$categoryId)->firstOrFail();
        }
        return $category;
    }

    /**
     * Sync address book company with already registered company in the system
     * @param $syncCompanyId
     * @param $addressBookId
     * @param $parentCompanyId
     * @return mixed
     */
    public function syncCompanies($syncCompanyId, $addressBookId, $parentCompanyId)
    {
        return Address_book::where('id','=',$addressBookId)
            ->where('owner_comp_id','=',$parentCompanyId)
            ->update([
                'synced_comp_id' => $syncCompanyId
            ]);
    }


    /**
     * Store address book company
     * @param $name
     * @param $syncedCompany
     * @param $note
     * @return static
     */
    public function storeCompany($name, $syncedCompany, $note)
    {
        return Address_book::create([
            'name' => $name,
            'owner_comp_id' => $this->companyId,
            'synced_comp_id' => $syncedCompany,
            'note' => $note,
        ]);
    }

    /**
     * Store default category
     * @param $category
     * @param $company
     * @return mixed
     */
    public function storeDefaultCategory($category, $company)
    {
        $getCategory = Default_category::where('id','=',(int)$category)->first();
        $attachCategory = $company->default_categories()->attach($getCategory->id);
        return $attachCategory;
    }

    /**
     * Store custom category
     * @param $category
     * @param $company
     * @return mixed
     */
    public function storeCustomCategory($category, $company)
    {
        $getCategory = Custom_category::getCategories($category, $this->companyId);
        $attachCategory = $company->custom_categories()->attach($getCategory->id);
        return $attachCategory;
    }

    /**
     * @param $masterFormatItemId
     * @param $addressBookCompany
     * @return mixed
     */
    public function storeMasterFormatItem($masterFormatItemId, $addressBookCompany)
    {
        $getMasterFormatItem = Master_Format::where('id','=',$masterFormatItemId)->first();
        $attachedMasterFormats = $addressBookCompany->master_format_items()->attach($getMasterFormatItem->id);
        return $attachedMasterFormats;
    }

    /**
     * Update address book company's name and master format number and title
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateCompany($id, $data)
    {
        $company = Address_book::where('id','=',$id)->first();
        $company->name = $data['name'];
        $company->synced_comp_id = (empty($data['company_id'])) ? NULL : $data['company_id'];
        $company->note = $data['note'];
        $company->save();
        return $company;
    }

    /**
     * Delete address book company
     * @param $id
     * @return bool|null
     */
    public function deleteEntry($id)
    {
        $result = Address_book::where('id','=',$id)
            ->where('owner_comp_id','=',$this->companyId)
            ->with('companies')
            ->with('proposal_suppliers')
            ->with('submittals')
            ->with('materials_and_services')
            ->with('contracts')
            ->with('project_owner')
            ->with('project_architect')
            ->first();

        if ($result) {
            //check if this address book company is connected with some project modules
            if(count($result->companies) || count($result->proposal_suppliers) || count($result->submittals) || count($result->materials_and_services) || count($result->contracts) || count($result->project_owner) || count($result->project_architect)) {
                return null;
            }

            return $result->delete();
        }
    }

    /**
     * Delete assigned address book company default categories
     * @param $company
     * @param $defaultCategoriesIdsArray
     * @return mixed
     */
    public function deleteDefaultCategories($company, $defaultCategoriesIdsArray)
    {
        return $company->default_categories()->detach($defaultCategoriesIdsArray);
    }

    /**
     * Delete assigned address book company custom categories
     * @param $company
     * @param $customCategoriesIdsArray
     * @return mixed
     */
    public function deleteCustomCategories($company, $customCategoriesIdsArray)
    {
        return $company->custom_categories()->detach($customCategoriesIdsArray);
    }

    public function deleteMasterFormatItems($company, $masterFormatItemsIdsArray)
    {
        return $company->master_format_items()->detach($masterFormatItemsIdsArray);
    }

    /**
     * Adds or updates rating in database for specific project and company
     *
     * @param $companyId
     * @param $projectId
     * @param $data
     * @return static
     */
    public function addOrUpdateCompanyRatingForProject($companyId, $projectId, $data)
    {
        $companyProjectRating = Project_company_rating::where('ab_cont_id', '=', $companyId)
                                                      ->where('proj_id', '=', $projectId)
                                                      ->first();

        if (!empty($companyProjectRating)) {
            return $companyProjectRating->update($data);
        } else {
            $data['ab_cont_id'] = $companyId;
            $data['proj_id'] = $projectId;
            return Project_company_rating::create($data);
        }
    }

    /**
     * Returns rating for project and company
     *
     * @param $companyId
     * @param $projectId
     * @return mixed
     */
    public function getCompanyRatingForProject($companyId, $projectId)
    {
        return Project_company_rating::where('ab_cont_id', '=', $companyId)
                                                        ->where('proj_id', '=', $projectId)
                                                        ->first();
    }

    /**
     * Updates address book contact rating
     *
     * @param $companyId
     * @param $data
     * @return mixed
     */
    public function updateAddressBook($companyId, $data)
    {
        return Address_book::where('id', '=', $companyId)->update($data);
    }

    public function undoLastImportAction()
    {
        $lastAction = Ab_import_history::where('user_id', '=', Auth::user()->id)->orderBy('id', 'desc')->first();

        if ($lastAction) {
            $addressBookIds = explode(',', $lastAction->ab_ids);

            foreach ($addressBookIds as $addressBookId) {
                $this->removeCompanyById($addressBookId);
            }

            $lastAction->delete();
        }
    }

    public function removeCompanyById($id)
    {
        $addressBookEntry = Address_book::where('id', '=', $id)->first();
        if ($addressBookEntry) {

            //remove contacts by address book id
            Ab_contact::where('ab_id', '=', $addressBookEntry->id)->delete();

            //delete addresses
            Address::where('ab_id', '=', $addressBookEntry->id)->delete();

            //delete relation to default categories
            Ab_default_category::where('ab_id', '=', $addressBookEntry->id)->delete();

            //delete relation to custom categories
            $customCategories = Ab_custom_category::where('ab_id', '=', $addressBookEntry->id)->get();
            foreach ($customCategories as $customCategory) {
                Custom_category::where('id', '=', $customCategory->custom_category_id)->delete();
            }

            Ab_custom_category::where('ab_id', '=', $addressBookEntry->id)->delete();

            //delete entry
            Address_book::where('id', '=', $addressBookEntry->id)->delete();
        }

    }

    public function checkIfEmptyImportHistory()
    {
        $abHistory = Ab_import_history::where('user_id', '=', Auth::user()->id)->first();

        if ($abHistory) {
            return false;
        }

        return true;
    }

    /**
     * Returns contacts from address book file
     *
     * @param $abId
     * @return mixed
     */
    public function getContactsForAddressBookFile($abId)
    {
        return Ab_distribution::where('ab_id', '=', $abId)
            ->get();
    }

    /**
     * Returns contacts for address book notifications
     *
     * @return mixed
     */
    public function getAddressBookContactsForNotifications()
    {
        return Ab_file::join('address_book_distribution', 'address_book_distribution.ab_file_id', '=', 'ab_files.id')
                ->join('address_book', 'address_book.id', '=', 'ab_files.ab_id')
                ->join('companies', 'companies.id', '=', 'ab_files.comp_id')
                ->leftJoin('users', 'users.id', '=', 'address_book_distribution.user_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id', '=', 'address_book_distribution.ab_cont_id')
                ->where('ab_files.notification', '=', 1)
                ->whereRaw('ab_files.expiration_date = date(NOW() + INTERVAL ab_files.notification_days DAY)')
                ->select(['users.email as userEmail', 'users.name as userName', 'ab_files.name as fileName',
                    'ab_contacts.name as contactName', 'ab_contacts.email as contactEmail', 'address_book.name as companyName',
                    'companies.name as senderCompanyName', 'ab_files.expiration_date as dueDate', 'ab_files.id as fileId',
                    'ab_files.file_name as fileDiskName', 'ab_files.ab_id as addressBookId', 'companies.id as senderCompanyId'])
                ->get();
    }

    /**
     * Delete all address book entry by company
     *
     * @param $companyId
     * @return bool|null
     */
    public function deleteAllByCompanyId($companyId)
    {
        return Address_book::where('owner_comp_id','=',$companyId)->delete();
    }

    /**
     * Returns address book which is synced with your company
     *
     * @param $companyId
     * @param $ownerId
     * @return mixed
     */
    public function getAddressBookEntryBySyncedCompany($companyId, $ownerId)
    {
        return Address_book::where('synced_comp_id', '=', $companyId)
                           ->where('owner_comp_id', '=', $ownerId)
                           ->first();
    }

    public function getProjectTransmittalsByProjectId($projectId = 0, $addressBookId = 0, $isGeneralContractor = 0)
    {
        $select = Ab_contact::select(['ab_contacts.id as id','ab_contacts.name as name', 'ab_contacts.email as email', 'address_book.id as ab_id'])
        ->join('address_book', 'address_book.id', '=', 'ab_contacts.ab_id')
        ->where('address_book.owner_comp_id', '=', $this->companyId);

        if(!$isGeneralContractor) {
            $select = $select->where('address_book.id', '=', $addressBookId);
        }

        return $select->get();
    }


}