<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/24/2015
 * Time: 6:25 PM
 */

namespace App\Modules\Address_book\Interfaces;

interface StoreAddressBookEntryInterface {
    public function postData(array $data, $company);
}