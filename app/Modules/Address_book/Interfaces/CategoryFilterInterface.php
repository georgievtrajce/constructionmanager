<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/26/2015
 * Time: 2:21 PM
 */

namespace App\Modules\Address_book\Interfaces;


interface CategoryFilterInterface {

    public function getData($id);

    public function getDataNotPaginated($id);

}