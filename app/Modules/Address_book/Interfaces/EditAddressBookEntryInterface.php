<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/30/2015
 * Time: 9:11 AM
 */

namespace App\Modules\Address_book\Interfaces;


interface EditAddressBookEntryInterface {

    public function editData($id, $data);

}