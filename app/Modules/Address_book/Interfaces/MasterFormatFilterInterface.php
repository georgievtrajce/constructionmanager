<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/27/2015
 * Time: 11:08 AM
 */

namespace App\Modules\Address_book\Interfaces;


interface MasterFormatFilterInterface {

    public function filterData($searchData);

}