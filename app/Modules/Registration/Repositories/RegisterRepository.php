<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/11/2015
 * Time: 10:05 AM
 */

namespace App\Modules\Registration\Repositories;


use App\Models\Address;
use App\Models\Company;
use App\Models\Role;
use App\Models\Subscription_type;
use App\Models\Transaction;
use App\Models\User;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class RegisterRepository {

    /**
     * when clicked on the link in email, update referral points and status
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function accept($token, $user)
    {
        $referralsRepository = new ReferralsRepository();

        //check if the company is level 0
        //make queue referral which will be counted if in future this company change its subscription level to
        //some of the payed levels
        if ($user->company->subs_id == Config::get('subscription_levels.level-zero')) {
            //update referral in db with comp_to id and make queued token
            $referralsRepository->makeReferralInQueue($token);
        } else {
            //update referral in db with comp_to id and make inactive token
            $referralsRepository->updateReferral($token);
        }

        //update points on appropriate comp_from account
        $referralsRepository->updateReferralPoints($token);

        return true;
    }

    /**
     * Accept shared project
     * @param $subToken
     * @param $compId
     * @param $projectCompanyId
     * @return bool
     */
    public function acceptProject($subToken, $compId, $projectCompanyId)
    {
        //instances from project subcontractors repo and address book repo
        $projectSubcontractorRepo = new ProjectSubcontractorsRepository();
        $addressBookRepo = new AddressBookRepository();
        $projectCompanyPermissionsRepo = new ProjectPermissionsRepository();

        //get project subcontractor
        $projectSubcontractor = $projectSubcontractorRepo->getProjectSubcontractorByToken($subToken);

        //store initial module permissions for the subcontractor
        //$projectCompanyPermissionsRepo->insertProjectCompanyPermissions($projectSubcontractor->proj_id, $projectSubcontractor->comp_parent_id, Auth::user()->comp_id);
        $projectCompanyPermissionsRepo->updatePermissionsSubcontractor($projectCompanyId, $projectSubcontractor->proj_id, $projectSubcontractor->comp_parent_id, Auth::user()->comp_id);

        //sync companies in address book
        $addressBookRepo->syncCompanies(Auth::user()->comp_id, $projectSubcontractor->ab_id, $projectSubcontractor->comp_parent_id);

        //store project permissions for the new added subcontractor users
        $projectSubcontractorRepo->storeSubcontractorProjectPermissions(Auth::user()->comp_id, $projectSubcontractor->proj_id);

        //update subcontractor (accepted project)
        return $projectSubcontractorRepo->updateSubcontractor($subToken, $compId, Config::get('constants.projects.shared_status.accepted'));
    }

    /**
     * Accept invitation to bid on a project
     * @param $subToken
     * @param $compId
     * @param $bidderId
     * @return mixed
     */
    public function acceptBid($subToken, $compId, $bidderId)
    {
        //instantiate proposals (bids) repo
        $proposalsRepo = new ProposalsRepository();
        $addressBookRepo = new AddressBookRepository();

        //get project bidder
        $projectBidder = $proposalsRepo->getProjectBidderByToken($subToken, $bidderId);

        //sync companies in address book
        $addressBookRepo->syncCompanies(Auth::user()->comp_id, $projectBidder->ab_id, $projectBidder->proposal->comp_id);

        //update bidder permissions with child company id
        $bidderPermissions = $proposalsRepo->getBidderPermissions($projectBidder->ab_id, $projectBidder->proposal->proj_id);
        if (count($bidderPermissions)) {
            foreach ($bidderPermissions as $permission) {
                $permission->comp_child_id = $compId;
                $permission->save();
            }
        }

        //update bidder
        $projectBidder->project_bidder->invited_comp_id = $compId;
        $projectBidder->project_bidder->token_active = 0;
        $projectBidder->project_bidder->status = Config::get('constants.bids.status.accepted');
        $projectBidder->project_bidder->once_accepted = 1;
        $projectBidder->project_bidder->save();

        //update all other bidder and bid items where this company is added for this concrete project
        //$proposalsRepo->updateOtherBidderRecordsToAccept($projectBidder->ab_id, $projectBidder->proposal->proj_id);

        return $projectBidder;
    }

    /**
     * Activate registered account after confirmation
     * @param $code
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @return bool
     */
    public function accountIsActive($code, $token, $subToken, $projectCompanyId, $bidderId)
    {
        $user = User::where('confirmation_code', '=', $code)
            ->with('company')
            ->firstOrFail();

        $user->confirmation_code = '';
        $user->confirmed = 1;

        //check if the user is logged in
        if (Auth::check()) {
            if ($user->email == Auth::user()->email) {
                $user->save();
                //if there is a token in the form, redirect to function for updating tokens
                if (!empty($token)) {
                    $this->accept($token, $user);
                }

                //check if there is subcontractor token for shared project
                if (!empty($subToken) && !empty($projectCompanyId)) {
                    $this->acceptProject($subToken, $user->comp_id, $projectCompanyId);
                }

                //check if there is bidder token for companies invited to bid
                if (!empty($subToken) && !empty($bidderId)) {
                    $this->acceptBid($subToken, $user->comp_id, $bidderId);
                }

                return true;
            }
        } else {
            if ($user->save()) {
                Auth::login($user);
                //if there is a token in the form, redirect to function for updating tokens
                if (!empty($token)) {
                    $this->accept($token, $user);
                }

                //check if there is subcontractor token for shared project
                if (!empty($subToken) && !empty($projectCompanyId)) {
                    $this->acceptProject($subToken, $user->comp_id, $projectCompanyId);
                }

                //check if there is bidder token for companies invited to bid
                if (!empty($subToken) && !empty($bidderId)) {
                    $this->acceptBid($subToken, $user->comp_id, $bidderId);
                }

                return true;
            }
        }
        return false;
    }

    /**
     * Send confirmation email for registration
     * @param $data
     * @return bool
     */
    public function sendConfirmationEmail($data)
    {
        $email = $data['email'];
        //send confirmation email
        Mail::queue('emails.activate_account', $data, function($message) use ($email) {
            $message->to($email);
            $message->subject(trans('labels.emails.confirmation_subject'));
        });

        return true;
    }

    /**
     * Send confirmation email for registration of a free account
     * @param $data
     * @return bool
     */
    public function sendFreeAccountConfirmationEmail($data)
    {
        $email = $data['email'];
        //send confirmation email
        Mail::queue('emails.activate_free_account', $data, function($message) use ($email) {
            $message->to($email);
            $message->subject(trans('labels.emails.confirmation_subject'));
        });

        return true;
    }

    /**
     * @param $companyId
     * @param $roleId
     * @return mixed
     */
    public function getCompanyAdmin($companyId, $roleId)
    {
        return User::where('comp_id','=',$companyId)->where('type','=',$roleId)->firstOrFail();
    }

    /**
     * Store the payment transaction in database
     * @param $data
     * @return static
     */
    public function storeTransaction($data)
    {
        return Transaction::create([
            'comp_id' => $data['companyId'],
            'subs_id' => $data['subscriptionId'],
            'transaction_id' => $data['transactionId'],
            'email' => $data['email'],
            'card_token' => $data['cardToken'],
            'amount' => $data['amount'],
        ]);
    }

    /**
     * Upload company logo
     * @param $company_logo
     * @param $id
     * @return static
     */
    public function uploadCompanyLogo($company_logo, $id)
    {
        /**
         * upload logo locally
         */
        $destinationPath = storage_path().'/app';
        $filename = 'company_logo_'.str_random(15).'.'.$company_logo->guessClientExtension();
        $company_logo->move($destinationPath, $filename);
        File::extension($destinationPath.DIRECTORY_SEPARATOR.$filename);

        /**
         * s3 upload
         */
        $disk = Storage::disk('s3');

        $contents = Storage::disk('local')->get($filename);
        $s3Upload = $disk->put('company_'.$id.'/'.$filename,$contents,'public');

        if($s3Upload) {
            Storage::disk('local')->delete($filename);
        }

        return 'company_'.$id.'/'.$filename;
    }

    /**
     * Insert Company into companies table
     * @param $data
     * @return static
     */
    public function insertCompany($data)
    {
        $company = Company::create([
                    'name' => $data['company_name'],
                    'subs_id' => $data['subscription_type'],
                    'uploaded' => '0',
                    'downloaded' => '0',
                ]);

        //set correct uac code for registered company
        $company->uac = 'UAC-'.str_pad($company->id, 6, "0", STR_PAD_LEFT);
        $company->update();

        if(isset($data['company_logo'])) {
            $returnFileName = $this->uploadCompanyLogo($data['company_logo'], $company->id);
            $company->logo = $returnFileName;
            $company->save();
        }

        return $company;
    }

    /**
     * Create company for free account
     * @param $data
     * @return static
     */
    public function insertCompanyForFreeAccount($data)
    {
        $company = Company::create([
            'name' => $data['company_name'],
            'subs_id' => $data['subscription_type'],
            'ref_points' => 0,
            'referred_by' => null,
            'subscription_payment_date' => date('Y-m-d'),
            'subscription_expire_date' => empty($data['expiration_date']) ? '' : Carbon::parse($data['expiration_date'])->format('Y-m-d'),
            'uploaded' => '0',
            'downloaded' => '0',
            'free_account' => 1,
        ]);

        //set correct uac code for registered company
        $company->uac = 'UAC-'.str_pad($company->id, 6, "0", STR_PAD_LEFT);
        $company->update();

        if(isset($data['company_logo'])) {
            $returnFileName = $this->uploadCompanyLogo($data['company_logo'], $company->id);
            $company->logo = $returnFileName;
            $company->save();
        }

        //update referral child company id if company is registered as level 0 free subscription
        //if this company change subscription level in future to some of the payed subscriptions
        //the referral points should be awarded to the company which invite it
        if ($data['subscription_type'] === Config::get('subscription_levels.level-zero')) {

        }

        return $company;
    }

    /**
     * Insert Address into addresses table
     * @param $data
     * @param $id
     * @return static
     */
    public function insertAddress($data, $id)
    {
        return Address::create([
                    'office_title' => $data['company_addr_office_title_req'],
                    'street' => $data['company_addr_street_req'],
                    'zip' => $data['company_addr_zip_code_req'],
                    'city' => $data['company_addr_town_req'],
                    'state' => $data['company_addr_state_req'],
                    'comp_id' => $id,
                ]);
    }

    /**
     * Insert User into users table
     * @param $data
     * @param $id
     * @return static
     */
    public function insertUser($data, $id)
    {
        //create activation code
        $confirmation_code = str_random(60).$data['email'];

        $company_admin_role = Role::cadmin();
        $user = User::create([
                    'email' => $data['email'],
                    'password' => Hash::make($data['password']),
                    'name' => $data['admin_name'],
                    'title' => $data['admin_title'],
                    'office_phone' => $data['admin_office_phone'],
                    'cell_phone' => $data['admin_cell_phone'],
                    'fax' => $data['admin_fax'],
                    'comp_id' => $id,
                    'type' => $company_admin_role->id,
                    'active' => (isset($data['account_type']) && $data['account_type'] == Config::get('subscription_levels.account_type.free')) ? 1 : 0,
                    'confirmation_code' => $confirmation_code,
                    'confirmed' => 0,
                ]);

        $user->roles()->attach($company_admin_role->id);

        return $user;
    }

    /**
     * Create the full registration
     * @param $data
     * @return static
     */
    public function postReg($data)
    {
        $company = $this->insertCompany($data);

        //required company address insert
        $this->insertAddress($data, $company->id);

        //additional company addresses insert
        if (isset($data['company_add_addr']))  {
            foreach ($data['company_add_addr'] as $company_add_addr) {
                $additional_addresses = [
                                            'company_addr_office_title_req' => $company_add_addr['company_addr_office_title'],
                                            'company_addr_street_req' => $company_add_addr['company_addr_street'],
                                            'company_addr_zip_code_req' => $company_add_addr['company_addr_zip_code'],
                                            'company_addr_town_req' => $company_add_addr['company_addr_town'],
                                            'company_addr_state_req' => $company_add_addr['company_addr_state'],
                                        ];

                $this->insertAddress($additional_addresses, $company->id);
            }
        }

        $user = $this->insertUser($data, $company->id);

        return $user;
    }

    /**
     * Create free account (through super admin section)
     * @param $data
     * @return RegisterRepository
     */
    public function createFreeAccount($data)
    {
        $company = $this->insertCompanyForFreeAccount($data);

        //required company address insert
        $this->insertAddress($data, $company->id);

        //Create some initial random password
        $data['password'] = str_random(10);

        //create user (company admin)
        $user = $this->insertUser($data, $company->id);

        if (!is_null($user)) {
            $dataArr = array(
                'name' => $user->name,
                'email' => $user->email,
                'password' => $data['password'],
                'code' => $user->confirmation_code,
                'token' => $user->referral_token
            );

            //send confirmation email
            $this->sendFreeAccountConfirmationEmail($dataArr);
        }

        return $user;
    }

    /**
     * On successful payment, update subscription level with dates, subscription id
     * @param $companyId
     * @param $subscriptionId
     * @return bool
     */
    public function updateSubscriptionLevel($companyId, $subscriptionId)
    {
        return Company::where('id','=',$companyId)->update([
            'subs_id' => $subscriptionId,
            'subscription_payment_date' => date('Y-m-d'),
            'subscription_expire_date'  => date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day")),
            'free_account' => 0
        ]);
    }

    /**
     * Update the subscription level to level one
     * Initially used when the subscription time expire (used on login)
     * @param $companyId
     * @return mixed
     */
    public function becomeSubscriptionLevelOne($companyId)
    {
        return Company::where('id','=',$companyId)->update([
            'subs_id' => (int)Config::get('subscription_levels.level-one'),
            'subscription_payment_date' => NULL,
            'subscription_expire_date'  => NULL,
        ]);
    }

    /**
     * Get subscription data
     *
     * @param $level
     * @return mixed
     */
    public function getSubscription($level)
    {
        return Subscription_type::where('id','=', Config::get($level))->firstOrFail();
    }
}