<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/9/2015
 * Time: 2:16 PM
 */

namespace App\Modules\Registration\Interfaces;


use App\Modules\Registration\Repositories\RegisterRepository;

interface SubscriptionInterface {
    //public function paySubscription($data);
    public function createAccount(RegisterRepository $repo, $data);
}