<?php
namespace App\Modules\Registration\Interfaces;

interface PaymentInterface {

    public function setPaymentPage($registeredUser, $token, $subToken, $projectCompanyId, $bidderId);

}