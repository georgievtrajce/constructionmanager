<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/13/2016
 * Time: 2:48 PM
 */

namespace App\Modules\Registration\Implementations;


use App\Models\Subscription_type;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Registration\Interfaces\PaymentInterface;
use App\Modules\Registration\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class ZeroSubscriptionPayment implements PaymentInterface
{

    /**
     * Redirect to the Helcim payment page
     * or send confirmation email if the level 0 is free subscription
     * @param $registeredUser
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @return \Illuminate\View\View
     */
    public function setPaymentPage($registeredUser, $token, $subToken, $projectCompanyId, $bidderId)
    {
        $subscriptionLevelZero = Subscription_type::where('name','=',Config::get('subscription_levels.level-zero-title'))->firstOrFail();

        //check if the subscription level is free
        if ($subscriptionLevelZero->amount > 0) {
            return Redirect::to('payment/hosted/'.$registeredUser->company->id.'/'.$subscriptionLevelZero->id.'/'.$registeredUser->email.'/'.$registeredUser->company->name.'?refToken='.$token.'&subToken='.$subToken.'&projectCompanyId='.$projectCompanyId.'&bidderId='.$bidderId);
        }

        if (!is_null($registeredUser)) {
            $dataArr = array(
                'name' => $registeredUser->name,
                'email' => $registeredUser->email,
                'code' => $registeredUser->confirmation_code,
                'token' => $token,
                'subToken' => (empty($subToken)) ? '' : $subToken,
                'projectCompanyId' => $projectCompanyId,
                'bidderId' => $bidderId
            );

            //activate account if subscription level 0 is free
            $registeredUser->active = 1;
            $registeredUser->save();

            //enter subscription payment date and subscription expire date
            $companiesRepo = new CompaniesRepository();
            $companiesRepo->updateCompanyDates($registeredUser->comp_id);

            //send confirmation email
            $registerRepo = new RegisterRepository();
            $registerRepo->sendConfirmationEmail($dataArr);

            Flash::info(trans('labels.confirmation.create.email'));
            return view('auth.activate_account');
        }

        Flash::info(trans('labels.confirmation.create.error'));
        return view('auth.activate_account');
    }

}