<?php
namespace App\Modules\Registration\Implementations;

use App\Models\Subscription_type;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Registration\Interfaces\PaymentInterface;
use App\Modules\Registration\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class FirstSubscriptionPayment implements PaymentInterface {

    /**
     * Redirect to the Helcim payment page
     * or send confirmation email if the level 1 is free subscription
     * @param $registeredUser
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @return \Illuminate\View\View
     */
    public function setPaymentPage($registeredUser, $token, $subToken, $projectCompanyId, $bidderId)
    {
        $subscriptionLevelOne = Subscription_type::where('name','=',Config::get('subscription_levels.level-one-title'))->firstOrFail();

        //check if the subscription level is free
        if ($subscriptionLevelOne->amount > 0) {
            return Redirect::to('payment/hosted/'.$registeredUser->company->id.'/'.$subscriptionLevelOne->id.'/'.$registeredUser->email.'/'.$registeredUser->company->name.'?refToken='.$token.'&subToken='.$subToken.'&projectCompanyId='.$projectCompanyId.'&bidderId='.$bidderId);
        }

        if (!is_null($registeredUser)) {
            $dataArr = array(
                'name' => $registeredUser->name,
                'email' => $registeredUser->email,
                'code' => $registeredUser->confirmation_code,
                'token' => $token,
                'subToken' => (empty($subToken)) ? '' : $subToken,
                'projectCompanyId' => $projectCompanyId,
                'bidderId' => $bidderId
            );

            //activate account if subscription level 1 is free
            $registeredUser->active = 1;
            $registeredUser->save();

            //enter subscription payment date and subscription expire date
            $companiesRepo = new CompaniesRepository();
            $companiesRepo->updateCompanyDates($registeredUser->comp_id);

            $registerRepo = new RegisterRepository();

            //send confirmation email
            $registerRepo->sendConfirmationEmail($dataArr);

            Flash::info(trans('labels.confirmation.create.email'));
            return view('auth.activate_account');
        }

        Flash::info(trans('labels.confirmation.create.error'));
        return view('auth.activate_account');
    }

}