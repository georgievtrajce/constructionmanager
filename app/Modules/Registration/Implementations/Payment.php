<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 7/30/2015
 * Time: 10:48 AM
 */

namespace App\Modules\Registration\Implementations;


use App\Modules\Registration\Interfaces\PaymentInterface;
use App\Modules\Registration\Repositories\RegisterRepository;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class Payment {

    /**
     * Start the payment process
     * @param $registeredUser
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @param $subscriptionType
     * @return mixed
     * @throws \Exception
     */
    public function startPaymentImplementation($registeredUser, $token, $subToken, $projectCompanyId, $bidderId, $subscriptionType)
    {
        $wrapPayment = new WrapPayment();
        $result = $wrapPayment->doPayment(ClassMap::instance(Config::get('classmap.payments.'.$subscriptionType)), $registeredUser, $token, $subToken, $projectCompanyId, $bidderId);
        return $result;
    }

}