<?php
namespace App\Modules\Registration\Implementations;

use App\Modules\Registration\Interfaces\SubscriptionInterface;
use App\Modules\Registration\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Config;

class TestAccountSubscription implements SubscriptionInterface
{

    private $subscription;

    /**
     * Get subscription level from subscription_levels config
     */
    public function __construct()
    {
        $this->subscription = Config::get('subscription_levels.test-account');
    }

    /**
     * Create company account and admin account
     * @param RegisterRepository $repo
     * @param $data
     * @return bool|static
     */
    public function createAccount(RegisterRepository $repo, $data)
    {
        return $repo->createFreeAccount($data);
    }

}