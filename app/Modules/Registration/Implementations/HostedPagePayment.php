<?php
namespace App\Modules\Registration\Implementations;

class HostedPagePayment {

    public function pay()
    {
        // SET URL
        $url = 'https://gatewaytest.helcim.com/hosted/';

        // SET TRANSACTION VALUES
        $merchantId		= '9999105997';
        $token			= '179g2f22a2fi2i1882';
        //$type			= 'purchase';
        $amount			= '270.00';
//		$cardNumber		= '5454545454545454';
//		$expiryDate		= '1020';
//		$cvvIndicator	= '1';
//		$cvv			= '200';
//		$orderId		= 'INV091293123';
        $test			= '1';

        // CREATE POST STRING
        $postString =	'merchantId='.$merchantId.'&'.
            'token='.$token.'&'.
            //'type='.$type.'&'.
            'amount='.$amount.'&'.
//						'cardNumber='.$cardNumber.'&'.
//						'expiryDate='.$expiryDate.'&'.
//						'cvvIndicator='.$cvvIndicator.'&'.
//						'cvv='.$cvv.'&'.
//						'orderId='.$orderId.'&'.
            'test='.$test;

        // SET CURL OPTIONS
        $curlOptions = array(	CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_AUTOREFERER => TRUE,
            CURLOPT_FRESH_CONNECT => TRUE,
            CURLOPT_HEADER => FALSE,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $postString,
            CURLOPT_TIMEOUT => 30 );

        // CREATE NEW CURL RESOURCE
        $curl = curl_init($url);

        // SET CURL OPTIONS
        curl_setopt_array($curl,$curlOptions);

        // PROCESS TRANSACTION - GET RESPONSE
        $response = curl_exec($curl);

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        // CLOSE CURL REOURCE
        curl_close($curl);

        dd($response);

        //header("Location: $url");

//        // CONVERT CARRIAGE RETURN LINE FEED TO AMPERSAND
//        $response = preg_replace("/\x0D/",'&',$response);
//        $response = preg_replace("/\x0A/",'&',$response);
//
//        // PARSE RESPONSE INTO AN ARRAY
//        parse_str($response,$responseArray);
//
//        return $responseArray;

//        // CHECK FOR APPROVAL
//        if($responseArray['response'] == 1){
//
//            // APPROVED
//            echo 'TRANSACTION APPROVED!';
//
//            // DISPLAY RESPONSE ARRAY
//            print_r($responseArray);
//
//            // KILL PROCESS
//            exit;
//
//        }else{
//
//            // DECLINED
//            echo 'TRANSACTION DECLINED!';
//
//            // DISPLAY RESPONSE ARRAY
//            print_r($responseArray);
//
//            // KILL PROCESS
//            exit;
//
//        }
    }

}