<?php
namespace App\Modules\Registration\Implementations;


use App\Modules\Registration\Interfaces\SubscriptionInterface;
use App\Modules\Registration\Repositories\RegisterRepository;
use Illuminate\Support\Facades\Config;

class ZeroSubscription implements SubscriptionInterface
{

    private $subscription;

    /**
     * Get subscription level from subscription_levels config
     */
    public function __construct()
    {
        $this->subscription = Config::get('subscription_levels.level-zero');
    }

    /**
     * Create company account and admin account
     * @param RegisterRepository $repo
     * @param $data
     * @return bool|static
     */
    public function createAccount(RegisterRepository $repo, $data)
    {
        //check if the registered account is a free account created through the super admin section
        if ($data['account_type'] == Config::get('subscription_levels.account_type.free')) {
            return $repo->createFreeAccount($data);
        }

        if($this->subscription = $repo->postReg($data)) {
            return $this->subscription;
        }
        return false;
    }

}