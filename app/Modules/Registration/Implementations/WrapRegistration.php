<?php
namespace App\Modules\Registration\Implementations;
use App\Modules\Registration\Interfaces\SubscriptionInterface;
use App\Modules\Registration\Repositories\RegisterRepository;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 3/9/2015
 * Time: 2:55 PM
 */

class WrapRegistration {

    /**
     * Different subscription types registration implementation
     * (level 1, level 2 and level 3 are available at this moment)
     * @param SubscriptionInterface $i
     * @param $data
     * @return
     */
    public function doRegister(SubscriptionInterface $i, $data)
    {
        return $i->createAccount(new RegisterRepository(), $data);
    }

}