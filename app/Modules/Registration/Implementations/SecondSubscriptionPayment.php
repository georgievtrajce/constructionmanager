<?php
namespace App\Modules\Registration\Implementations;

use App\Models\Subscription_type;
use App\Modules\Registration\Interfaces\PaymentInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class SecondSubscriptionPayment implements PaymentInterface {

    /**
     * Redirect to the Helcim payment page
     * @param $registeredUser
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @return \Illuminate\View\View
     */
    public function setPaymentPage($registeredUser, $token, $subToken, $projectCompanyId, $bidderId)
    {
        $subscriptionLevelTwo = Subscription_type::where('name','=',Config::get('subscription_levels.level-two-title'))->firstOrFail();

        if ($subscriptionLevelTwo->amount > 0) {
            return Redirect::to('payment/hosted/'.$registeredUser->company->id.'/'.$subscriptionLevelTwo->id.'/'.$registeredUser->email.'/'.$registeredUser->company->name.'?refToken='.$token.'&subToken='.$subToken.'&projectCompanyId='.$projectCompanyId.'&bidderId='.$bidderId);
        }

        Flash::info(trans('labels.confirmation.create.email'));
        return view('auth.activate_account');
    }

}