<?php
namespace App\Modules\Registration\Implementations;

use App\Modules\Registration\Interfaces\PaymentInterface;

class WrapPayment {

    /**
     * Wrap the payment process implementation for the different subscription levels
     * @param PaymentInterface $i
     * @param $registeredUser
     * @param $token
     * @param $subToken
     * @param $projectCompanyId
     * @param $bidderId
     * @return mixed
     */
    public function doPayment(PaymentInterface $i, $registeredUser, $token, $subToken, $projectCompanyId, $bidderId)
    {
        return $i->setPaymentPage($registeredUser, $token, $subToken, $projectCompanyId, $bidderId);
    }

}