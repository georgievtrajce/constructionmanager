<?php
namespace App\Modules\Project_permissions\Repositories;
use App\Models\Project_permission;
use App\Models\Project_company_permission;
use App\Models\Project_permission_user;
use App\Models\Proposal_supplier_permission;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use Auth;
use Illuminate\Support\Facades\Config;

class ProjectPermissionsRepository {

    /**
     * @param $projectID
     * @param $userId
     * @return mixed
     */

    public function __construct()
    {
        $this->manageUsersRepository = new ManageUsersRepository();
    }

    /**
     * get permissions for user for project
     * @param $projectID
     * @param $userId
     * @return mixed
     */
    public function getUserPermissionsByProject($projectID, $userId)
   {
       return Project_permission::where('proj_id', '=', $projectID)
           ->where('user_id', '=', $userId)
           ->with('module')
           ->with('fileType')
           ->get();
   }

    /**
     * Get batch download file permissions
     *
     * @param $projectId
     * @param $userId
     * @return mixed
     */
    public function getUserPermissionBatchDownloadByProject($projectId, $userId)
   {
       return Project_permission::where('proj_id', '=', $projectId)
           ->where('user_id', '=', $userId)
           ->where('entity_type', '=', Config::get('constants.entity_type.other'))
           ->first();
   }

    /**
     * Set batch download file permissions
     *
     * @param $projectId
     * @param $userId
     * @return mixed
     */
    public function updateUserPermissionBatchDownloadByProject($projectId, $userId, $readPermission=0, $writePermission=0, $deletePermission=0)
    {
        $userBatchPermissions = $this->getUserPermissionBatchDownloadByProject($projectId, $userId);

        if ($userBatchPermissions) {
            $userBatchPermissions->update([
                'read' => $readPermission,
                'write' => $writePermission,
                'delete' => $deletePermission
            ]);
        } else {
            Project_permission::firstOrCreate([
                'user_id' => $userId,
                'read' => $readPermission,
                'write' => $writePermission,
                'delete' => $deletePermission,
                'entity_id' => 1,
                'entity_type' => Config::get('constants.entity_type.other'), //this means that the type of the permission is module
                'proj_id' => $projectId
            ]);
        }
    }


    /**
     * update user project permissions
     * @param $userID
     * @param $module
     * @param $readPermission
     * @param $writePermission
     * @param $deletePermission
     * @param $showCostPermission
     * @param $entityType
     * @param $projectID
     * @return mixed
     */

    public function updateUserProjectPermissions($userID, $module, $readPermission, $writePermission, $deletePermission, $showCostPermission, $entityType, $projectID)
    {
        return Project_permission::where('user_id', '=', $userID)
            ->where('entity_id', '=', $module)
            ->where('entity_type', '=', $entityType)
            ->where('proj_id', '=', $projectID)
            ->update([
                'read' => $readPermission,
                'write' => $writePermission,
                'delete' => $deletePermission,
                'show_cost' => (!empty($showCostPermission)) ? $showCostPermission : 0
            ]);
    }

    /**
     * get company permissions for projects
     * @param $projectID
     * @param $companyID
     * @return mixed
     */
    public function getCompanyPermissionsByProject($projectID, $companyID)
    {
        return Project_company_permission::where('proj_id', '=', $projectID)
            ->where('proj_comp_id', '=', $companyID)
            ->with('project_company.address_book')
            ->with('module')
            ->with('fileType')
            ->get();
    }

    /**
     * get company permissions for projects
     * @param $projectID
     * @param $companyID
     * @param $entityId
     * @return mixed
     */
    public function getCompanyPermissionsByProjectAndAddressBookCompany($projectID, $companyID, $entityId)
    {
        return Project_company_permission::join('project_companies', 'project_companies.id', '=', 'project_company_permissions.proj_comp_id')
            ->where('project_company_permissions.proj_id', '=', $projectID)
            ->where('project_company_permissions.entity_id', '=', $entityId)
            ->where('project_companies.ab_id', '=', $companyID)
            ->first();
    }

    /**
     * get all write permissions for project and company
     * @param $projectID
     * @param $companyID
     * @param $entityId
     * @return mixed
     */
    public function getCompanyPermissionsWriteModuleNames($projectID, $companyID)
    {
        return Project_company_permission::join('project_companies', 'project_companies.id', '=', 'project_company_permissions.proj_comp_id')
            ->join('modules', 'modules.id', '=', 'project_company_permissions.entity_id')
            ->where('project_company_permissions.proj_id', '=', $projectID)
            ->where('project_company_permissions.write', '=', 1)
            ->where('project_company_permissions.entity_type', '=', 1)
            ->where('project_companies.ab_id', '=', $companyID)
            ->get();
    }

    /**
     * initial insert company permissions for project
     * @param $projectID
     * @param $parentID
     * @param $childID
     * @return bool
     */
    public function insertProjectCompanyPermissions($projectID, $parentID, $childID)
    {
        //get modules from modules table
        $modules = $this->manageUsersRepository->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $this->manageUsersRepository->getProjectFileTypes();

        //set all user permissions to 0
        //set modules permissions to 0
        foreach ($modules as $module) {
            if ($module->name != Config::get('constants.modules.projects')) {

                $existingProjectPermission = Project_company_permission::where('comp_parent_id', '=', $parentID)
                                                                ->where('comp_child_id', '=', $childID)
                                                                ->where('entity_id', '=', $module->id)
                                                                ->where('entity_type', '=', Config::get('constants.entity_type.module'))
                                                                ->where('proj_id', '=', $projectID)
                                                                ->first();

                if (!$existingProjectPermission) {
                    Project_company_permission::firstOrCreate([
                        'comp_parent_id' => $parentID,
                        'comp_child_id' => $childID,
                        'read' => 0,
                        'write' => 0,
                        'delete' => 0,
                        'entity_id' => $module->id,
                        'entity_type' => Config::get('constants.entity_type.module'), //this means that the type of the permission is module
                        'proj_id' => $projectID
                    ]);
                }
            }
        }

        //set project files permissions to 0
        foreach ($projectFiles as $projectFile) {

            $existingProjectPermission = Project_company_permission::where('comp_parent_id', '=', $parentID)
                                                            ->where('comp_child_id', '=', $childID)
                                                            ->where('entity_id', '=', $projectFile->id)
                                                            ->where('entity_type', '=', Config::get('constants.entity_type.file'))
                                                            ->where('proj_id', '=', $projectID)
                                                            ->first();
            if (!$existingProjectPermission) {
                Project_company_permission::firstOrCreate([
                    'comp_parent_id' => $parentID,
                    'comp_child_id' => $childID,
                    'read' => 0,
                    'write' => 0,
                    'delete' => 0,
                    'entity_id' => $projectFile->id,
                    'entity_type' => Config::get('constants.entity_type.file'), //this means that the type of the permission is project file
                    'proj_id' => $projectID
                ]);
            }
        }

        return true;
    }

    /**
     * Update permissions child company
     * @param $projectCompanyId
     * @param $projectId
     * @param $parentCompanyId
     * @param $childCompanyId
     * @return mixed
     */
    public function updatePermissionsSubcontractor($projectCompanyId, $projectId, $parentCompanyId, $childCompanyId)
    {
        return Project_company_permission::where('proj_comp_id','=',$projectCompanyId)
            ->where('proj_id','=',$projectId)
            ->where('comp_parent_id','=',$parentCompanyId)
            ->update([
                'comp_child_id' => $childCompanyId
            ]);
    }

    /**
     * initial insert company permissions for project
     * (new method created when the project companies permission work flow is changed)
     * Now company permissions are created initially when company is added in the companies module
     * @param $projectID
     * @param $parentID
     * @param $projectCompanyID
     * @return bool
     * @internal param $childID
     */
    public function insertInitialProjectCompanyPermissions($projectID, $parentID, $projectCompanyID)
    {
        //get modules from modules table
        $modules = $this->manageUsersRepository->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $this->manageUsersRepository->getProjectFileTypes();

        //set all user permissions to 0
        //set modules permissions to 0
        foreach ($modules as $module) {
            if ($module->name != Config::get('constants.modules.projects')) {

                $existingProjectPermission = Project_company_permission::where('comp_parent_id', '=', $parentID)
                                                            ->where('proj_comp_id', '=', $projectCompanyID)
                                                            ->where('entity_id', '=', $module->id)
                                                            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
                                                            ->where('proj_id', '=', $projectID)
                                                            ->first();

                if (!$existingProjectPermission) {
                    Project_company_permission::firstOrCreate([
                        'proj_comp_id' => $projectCompanyID,
                        'comp_parent_id' => $parentID,
                        'read' => 0,
                        'write' => 0,
                        'delete' => 0,
                        'entity_id' => $module->id,
                        'entity_type' => Config::get('constants.entity_type.module'), //this means that the type of the permission is module
                        'proj_id' => $projectID
                    ]);
                }
            }
        }

        //set project files permissions to 0
        foreach ($projectFiles as $projectFile) {
            $existingProjectPermission = Project_company_permission::where('comp_parent_id', '=', $parentID)
                ->where('proj_comp_id', '=', $projectCompanyID)
                ->where('entity_id', '=', $projectFile->id)
                ->where('entity_type', '=', Config::get('constants.entity_type.file'))
                ->where('proj_id', '=', $projectID)
                ->first();

            if (!$existingProjectPermission) {
                Project_company_permission::firstOrCreate([
                    'proj_comp_id' => $projectCompanyID,
                    'comp_parent_id' => $parentID,
                    'read' => 0,
                    'write' => 0,
                    'delete' => 0,
                    'entity_id' => $projectFile->id,
                    'entity_type' => Config::get('constants.entity_type.file'), //this means that the type of the permission is project file
                    'proj_id' => $projectID
                ]);
            }
        }

        return true;
    }

    /**
     * Get all company permissions by different entity type (modules or file types)
     * @param $compID
     * @param $entityType
     * @param $projectID
     * @return mixed
     */
    public function getAllCompanyPermissionsByEntity($compID, $entityType, $projectID)
    {
        return Project_company_permission::where('proj_comp_id', '=', $compID)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->where('entity_type', '=', $entityType)
            ->where('proj_id', '=', $projectID)
            ->get();
    }

    /**
     * Get all bidder permissions by different entity type (modules or file types)
     * @param $abId
     * @param $entityType
     * @param $projectID
     * @return mixed
     * @internal param $compID
     * @internal param $entityType
     * @internal param $projectID
     */
    public function getAllBidderPermissionsByEntity($abId, $entityType, $projectID)
    {
        return Proposal_supplier_permission::where('ab_id', '=', $abId)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->where('entity_type', '=', $entityType)
            ->where('proj_id', '=', $projectID)
            ->get();
    }

    /**
     * update company permissions for project
     * @param $id
     * @param $read
     * @param $write
     * @param $pcoPermissionType
     * @return mixed
     */
    public function updateProjectCompanyPermission($syncedCompId, $id, $read, $pcoPermissionType, $write=0)
    {
        $permission = Project_company_permission::where('id', '=', $id)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->with('module')
            ->firstOrFail();

        $permission->read = $read;
        $permission->write = $write;
        $permission->delete = 0;
        $permission->comp_child_id = $syncedCompId;

        if (!is_null($permission->module) && $permission->module->display_name == Config::get('constants.pcos')) {
            if (is_null($pcoPermissionType)) {
                $permission->pco_permission_type = 0;
            } else {
                $permission->pco_permission_type = (int)$pcoPermissionType;
            }
        }

        $permission->save();
        return $permission;
    }

    /**
     * update bidder permissions for project
     * @param $id
     * @param $read
     * @param $pcoPermissionType
     * @param int $write
     * @param int $delete
     * @return mixed
     */
    public function updateBidderPermission($id, $read, $pcoPermissionType, $write=0, $delete=0)
    {
        $permission = Proposal_supplier_permission::where('id', '=', $id)
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->with('module')
            ->firstOrFail();

        $permission->read = $read;
        $permission->write = $write;
        $permission->delete = $delete;

        if (!is_null($permission->module) && $permission->module->display_name == Config::get('constants.pcos')) {
            if (is_null($pcoPermissionType)) {
                $permission->pco_permission_type = 0;
            } else {
                $permission->pco_permission_type = (int)$pcoPermissionType;
            }
        }

        $permission->save();
        return $permission;

//        return Proposal_supplier_permission::where('id', '=', $id)
//            ->update([
//                'read' => $param,
//                'write' => 0,
//                'delete' => 0,
//            ]);
    }

    // get all subcontractor IDs for which the specified module is allowed for the logged in company
    public function getAllowedParentCompanyIDsByModule($projectID, $module, $companyTo)
    {
        $companyIDs = Project_company_permission::where('comp_child_id', '=', $companyTo)
            ->where('proj_id', '=', $projectID)
            ->where('entity_id', '=', $module)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->select('comp_parent_id', 'comp_child_id')
            ->get();

        $companyIDsArray = array();
        foreach ($companyIDs as $companyID)
        {
            array_push($companyIDsArray, $companyID['comp_child_id']);
        }

        return $companyIDsArray;
    }

    /**
     * Check if the logged in company has permissions for this module
     * @param $projectId
     * @param $moduleId
     * @param $companyId
     * @return mixed
     */
    public function checkModulePermissions($projectId, $moduleId, $companyId)
    {
        $companiesPermissions = Project_company_permission::where('comp_child_id', '=', $companyId)
            ->where('proj_id', '=', $projectId)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->first();

        $bidsPermissions = Proposal_supplier_permission::where('comp_child_id', '=', $companyId)
            ->where('proj_id', '=', $projectId)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->first();

        if ($companiesPermissions || $bidsPermissions) {
            return true;
        }

        return false;
    }

    /**
     * Check if the logged in company has permissions for this module
     * @param $projectId
     * @param $moduleId
     * @param $companyId
     * @return mixed
     */
    public function checkWriteModulePermissions($projectId, $moduleId, $companyId)
    {
        $companiesPermissions = Project_company_permission::where('comp_child_id', '=', $companyId)
            ->where('proj_id', '=', $projectId)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('write', '=', 1)
            ->first();

        $bidsPermissions = Proposal_supplier_permission::where('comp_child_id', '=', $companyId)
            ->where('proj_id', '=', $projectId)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('write', '=', 1)
            ->first();

        if ($companiesPermissions || $bidsPermissions) {
            return true;
        }

        return false;
    }

    /**
     * Delete project company permissions
     * @param $projectId
     * @param $companyId
     * @return mixed
     */
    public function deleteProjectCompanyPermissions($projectId, $companyId)
    {
        return Project_company_permission::where('comp_child_id','=',$companyId)
            ->where('proj_id','=',$projectId)
            ->delete();
    }

    /**
     * Check if the logged in user has read permissions for at least one project and this module
     * @param $moduleId
     * @return mixed
     */
    public function userHasAtLeastOneReadPermission($moduleId)
    {
        $userReadPermissions = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->first();

        if (!is_null($userReadPermissions)) {
            return true;
        }

        return false;
    }

    /**
     * Check if the logged in user has write permissions for at least one project and this module
     * @param $moduleId
     * @return mixed
     */
    public function userHasAtLeastOneWritePermission($moduleId)
    {
        $userWritePermissions = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('write', '=', 1)
            ->first();

        if (!is_null($userWritePermissions)) {
            return true;
        }

        return false;
    }

    /**
     * Check if the logged in user has delete permissions for at least one project and this module
     * @param $moduleId
     * @return mixed
     */
    public function userHasAtLeastOneDeletePermission($moduleId)
    {
        $userDeletePermissions = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('delete', '=', 1)
            ->first();

        if (!is_null($userDeletePermissions)) {
            return true;
        }

        return false;
    }

    /**
     * Get projects by user for module
     * @param $moduleId
     * @return mixed
     */
    public function getUserProjectsWritePermissionsForModule($moduleId)
    {
        $projects = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('write', '=', 1)
            ->select(['project_permissions.proj_id'])
            ->get();

        return $projects;
    }

    /**
     * Get projects by user for module read
     * @param $moduleId
     * @return mixed
     */
    public function getUserProjectsReadPermissionsForModule($moduleId)
    {
        $projects = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->select(['project_permissions.proj_id'])
            ->get();

        return $projects;
    }

    /**
     * Get projects by user for module read
     * @param $moduleId
     * @return mixed
     */
    public function getUserProjectsReadPermissionsForModuleAndProject($moduleId, $projectId)
    {
        $projectPermission = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('read', '=', 1)
            ->where('proj_id', '=', $projectId)
            ->select(['project_permissions.proj_id'])
            ->first();

        if (!is_null($projectPermission)) {
            return true;
        }

        return false;
    }

    /**
     * Get projects by user for module write
     * @param $moduleId
     * @return mixed
     */
    public function getUserProjectsWritePermissionsForModuleAndProject($moduleId, $projectId)
    {
        $projectPermission = Project_permission::where('user_id', '=', Auth::user()->id)
            ->where('entity_id', '=', $moduleId)
            ->where('entity_type', '=', Config::get('constants.entity_type.module'))
            ->where('write', '=', 1)
            ->where('proj_id', '=', $projectId)
            ->select(['project_permissions.proj_id'])
            ->first();

        if (!is_null($projectPermission)) {
            return true;
        }

        return false;
    }

    /**
     * Bulk update all user permissions
     *
     * @param $userId
     * @param $read
     * @param $write
     * @param $delete
     * @param $entityTypes
     */
    public function updateUserPermissionsForAllProjects($userId, $read, $write, $delete, $entityTypes)
    {
        Project_permission::where('user_id', '=', $userId)
                           ->whereIn('entity_type', $entityTypes)
                           ->where(function($q) use ($read, $write, $delete) {
                                $q->where('read', '!=', $read)
                                  ->orWhere('write', '!=', $write)
                                  ->orWhere('delete', '!=', $delete);
                           })
                           ->update([
                               'read' => $read,
                               'write' => $write,
                               'delete' => $delete
                           ]);



        //add the user to specific projects
        $allProjectPermissions = Project_permission::where('user_id', '=', $userId)
                                                    ->whereIn('entity_type', $entityTypes)
                                                    ->get();

        foreach ($allProjectPermissions as $projectPermission) {
            Project_permission_user::firstOrCreate(['user_id' => $userId, 'proj_id' => $projectPermission->proj_id]);
        }
    }
}