<?php
namespace App\Modules\Company_profile\Repositories;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Rhumsaa\Uuid\Uuid;
use App\Models\Company;
use App\Models\User;
use App\Models\Subscription_type;
use App\Models\Referral;

class ReferralsRepository {

    /**
     * Get queued referral
     * @param $compTo
     */
    public function getQueueReferral($compTo)
    {
        return Referral::where('comp_to','=',$compTo)
            ->where('token_active','=',Config::get('constants.status.queue'))
            ->with('companyFrom')
            ->with('companyTo')
            ->orderBy('created_at','asc')
            ->first();
    }

    /**
     * @param $input
     * @return mixed
     */
    public function insertReferral($input)
    {
        //create new referral with email, comp_id, token, token active.
        $referralData = array(
            'comp_from' => Auth::user()->comp_id,
            'email' =>  $input['email'],
            'token' => Uuid::uuid1()->toString(),
            'token_active' => Config::get('constants.status.active'),
        );
        $referral = Referral::firstOrCreate($referralData);
        return $referral->token;

    }

    /**
     * update referral points for registered users in a higher subscription level
     * @param $token
     * @return mixed
     * @internal param $companyID
     * @internal param $subscriptionType
     */
    public function updateReferralPoints($token)
    {
        //get referral with From and To companies
        $referral = Referral::with('companyFrom')->with('companyTo')->where('token', '=', $token)->firstOrFail();

        //get points for subscribed company
        $subscription = Subscription_type::where('id', '=', $referral->companyTo->subs_id)->firstOrFail();

        if ($referral->companyTo->subs_id != Config::get('subscription_levels.level-zero')) {
            $companyFrom = Company::where('id', '=', $referral->comp_from)->firstOrFail();
            $companyFrom->ref_points += $subscription->ref_points;
            return ($companyFrom->save());
        }
        return true;
    }

    public function updateReferralPointsByCompanyId($queuedReferral, $subscriptionId)
    {
        //get points for subscribed company
        $subscription = Subscription_type::where('id', '=', $subscriptionId)->first();

        if (!is_null($subscription) && !is_null($queuedReferral->companyTo) && $queuedReferral->companyTo->subs_id != Config::get('subscription_levels.level-zero')) {
            $companyFrom = Company::where('id', '=', $queuedReferral->comp_from)->first();
            $companyFrom->ref_points += $subscription->ref_points;
            return ($companyFrom->save());
        }
        return true;
    }

    /**
     * @param $token
     * @return bool
     */
    public function makeReferralInQueue($token)
    {
        $referral = Referral::where('token', '=', $token)->firstOrFail();

        $referral->token_active = Config::get('constants.status.queue');
        $referral->comp_to = Auth::user()->comp_id;
        $referral->save();

        return true;
    }

    /**
     * Update referral
     * @param $token
     * @return bool
     */
    public function updateReferral($token)
    {
        $referral = Referral::where('token', '=', $token)->firstOrFail();

        $referral->token_active = Config::get('constants.status.inactive');
        $referral->comp_to = Auth::user()->comp_id;
        $referral->save();

        return true;
    }

    /**
     * Check if email exists
     * @param $email
     * @return mixed
     */
    public function emailExists($email)
    {
        return (User::where('email', '=', $email)->first());
    }

    /**
     * Delete referral specified with an email and parent company id
     * @param $email
     * @param $parentCompanyId
     * @return mixed
     */
    public function deleteReferralsToContacts($email, $parentCompanyId)
    {
        $referrals = Referral::where('comp_from','=',$parentCompanyId)
            ->where('email','=',$email)
            ->get();

        if (count($referrals)) {
            foreach ($referrals as $referral) {
                $referral->delete();
            }
        }

        return true;
    }

    /**
     * Delete single referral specified by token
     * @param $token
     * @return bool
     */
    public function deleteSingleReferral($token)
    {
        $referral = Referral::where('token','=',$token)->first();
        if (!is_null($referral)) {
            return $referral->delete();
        }
        return false;
    }


}