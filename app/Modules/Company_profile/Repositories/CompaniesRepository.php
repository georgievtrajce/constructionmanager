<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/21/2015
 * Time: 6:10 PM
 */
namespace App\Modules\Company_profile\Repositories;

use App\Models\Company;
use App\Models\Company_usage_history;
use App\Models\Subscription_type;
use App\Models\User;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CompaniesRepository {
    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var AddressBookRepository
     */
    private $addressBookRepository;
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * CompaniesRepository constructor.
     */
    public function __construct()
    {
        $this->projectRepository = App::make('App\Modules\Project\Repositories\ProjectRepository');
        $this->addressBookRepository = App::make('App\Modules\Address_book\Repositories\AddressBookRepository');
        $this->usersRepository = App::make('App\Modules\User_profile\Repositories\UsersRepository');
    }

    /**
     * Get company
     * @param $companyId
     * @return mixed
     */
    public function getCompany($companyId)
    {
        return Company::where('id','=',$companyId)->with('addresses')->with('subscription_type')->first();
    }

    /**
     * Get concrete company UAC
     * @param $uac
     * @return mixed
     */
    public function getCompanyByUac($uac)
    {
         return Company::where('uac','=',$uac)->with('addresses')->first();
    }

    /**
     * Get company with users id
     * @param $id
     * @return mixed
     */
    public function getCompanyWithUsersByID($id)
    {
        return Company::where('id','=',$id)->with('users')->first();
    }

    /**
     * Update company
     * @param $companyId
     * @param $data
     * @return mixed
     */
    public function updateCompany($companyId, $data)
    {
        return Company::where('id','=',$companyId)->update([
            'name' => $data['name']
        ]);
    }

    /**
     * Update company upload transfer with a given increased value
     * @param $companyId
     * @param $transferSize
     * @return mixed
     */
    public function updateCompanyUploadTransfer($companyId, $transferSize)
    {
        return Company::where('id','=',$companyId)->update([
            'uploaded' => $transferSize
        ]);
    }

    /**
     * Update company upload transfer with a given increased value
     * @param $companyId
     * @return mixed
     */
    public function updateCompanyMassAssign($companyId, $data)
    {
        return Company::where('id','=',$companyId)->update($data);
    }

    /**
     * @param $companyId
     * @param bool $resetNotifications
     * @return mixed
     */
    public function updateLimitMonthAndYear($companyId, $resetNotifications=false)
    {
        $data = [
            'limit_month' => Carbon::now()->month,
            'limit_year' => Carbon::now()->year,
        ];

        if ($resetNotifications) {
            $data['download_notification'] = 0;
            $data['upload_notification'] = 0;
        }

        return Company::where('id', '=', $companyId)->update($data);
    }

    public function addCompanyUsageHistory($company, $type, $data)
    {
        Company_usage_history::insert([
            'company_id' => $company->id,
            'data' => $data,
            'type' => $type,
            'month' => $company->limit_month,
            'year' => $company->limit_year
        ]);
    }

    /**
     * Update company download transfer with a given increased value
     * @param $companyId
     * @param $transferSize
     * @return mixed
     */
    public function updateCompanyDownloadTransfer($companyId, $transferSize)
    {
        return Company::where('id','=',$companyId)->update([
            'downloaded' => $transferSize
        ]);
    }


    /**
     * Update company logo
     * @param $company_logo
     * @param $id
     * @return mixed
     */
    public function updateCompanyLogo($company_logo, $id)
    {
        /**
         * upload logo locally
         */
        $destinationPath = storage_path().'/app';
        $filename = 'company_logo_'.str_random(15).'.'.$company_logo->guessClientExtension();
        $company_logo->move($destinationPath, $filename);
        File::extension($destinationPath.DIRECTORY_SEPARATOR.$filename);

        /**
         * s3 upload
         */
        $disk = Storage::disk('s3');

        $contents = Storage::disk('local')->get($filename);
        $s3Upload = $disk->put('company_'.$id.'/'.$filename,$contents,'public');

        if ($s3Upload) {
            Storage::disk('local')->delete($filename);
        }

        $logo = 'company_'.$id.'/'.$filename;

        //delete current logo
        if (!is_null(Auth::user()->company->logo)) {
            if ($disk->exists(Auth::user()->company->logo)) {
                $disk->delete(Auth::user()->company->logo);
            }
        }

        //Update company logo field
        return Company::where('id','=',$id)->update([
            'logo' => $logo
        ]);
    }

    /**
     * Update registered company subscription payment and expire date
     * @param $id
     * @return mixed
     */
    public function updateCompanyDates($id)
    {
        return Company::where('id','=',$id)
            ->update([
                'subscription_payment_date' => date('Y-m-d'),
                'subscription_expire_date'  => date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"))
            ]);
    }

    /**
     * return admin info for company
     * @param $companyID
     * @return bool
     */
    public function getCompanyAdminInfo($companyID)
    {

        $users = User::where('comp_id','=',$companyID)->get();

        foreach ($users as $user)
        {
            if ($user->hasRole('Company Admin')) {
                return $user;
            }
        }
        return false;

    }

    /**
     * Change subscription levels by points
     * @param $companyID
     * @param $subscriptionID
     * @return bool
     */
    public function changeSubscriptionLevelByPoints($companyID, $subscriptionID)
    {
        $subscription = Subscription_type::where('id', '=', $subscriptionID)->first();
        $company = Company::where('id', '=', $companyID)->first();
        if ($company->ref_points >= $subscription->ref_points_needed) {
            $company->ref_points -= $subscription->ref_points_needed;
            $company->subs_id = $subscriptionID;
            $company->subscription_expire_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
            return ($company->update());
        }
        else return false;
    }

    /**
     * Get specified subscription level by id
     * @param $id
     * @return mixed
     */
    public function getSubscriptionTypeByID($id)
    {
        return (Subscription_type::where('id', '=', $id)->first());
    }

    /**
     * Returns all companies with expired subscription which are not level 0
     *
     * @return mixed
     */
    public function changeAllExpiredCompaniesToLevel0()
    {
        $companies = Company::whereIn('subs_id',[1,2,3])
                      ->where('subscription_expire_date','<', Carbon::now()->toDateString())
                      ->get();

        foreach ($companies as $company) {
            $company->subs_id = 5;
            $company->save();
        }
    }

    public function AddLimitsForAllCompaniesToHistory()
    {
        $companies = Company::where('limit_month','!=', Carbon::now()->month)
            ->orWhere('limit_year','!=', Carbon::now()->year)
            ->get();

        foreach ($companies as $company) {
            $this->addCompanyUsageHistory($company, 'upload', $company->uploaded);
            $this->addCompanyUsageHistory($company, 'download', $company->downloaded);
            $this->updateLimitMonthAndYear($company->id, true);
            $this->updateCompanyDownloadTransfer($company->id, 0);
            $this->updateCompanyUploadTransfer($company->id, 0);
        }

        $companies = Company::whereNull('limit_month')
            ->whereNull('limit_year')
            ->get();

        foreach ($companies as $company) {
            $company->limit_month = Carbon::now()->month;
            $company->limit_year = Carbon::now()->year;
            $company->save();
        }

    }

    public function getAllCompaniesWithExpiredSubscriptionByNumberOfDay($days, $toDays)
    {
        return Company::where('subs_id', '=', 0)
                       ->where('subscription_expire_date', '<', Carbon::now()->subDays($days)->toDateString())
                       ->where('subscription_expire_date', '>=', Carbon::now()->subDays($toDays)->toDateString())
                       ->get();

    }

    public function deleteCompany($companyId)
    {
        $company = $this->getCompany($companyId);
        if ($company && $company->subs_id === Config('subscription_levels.level-zero')) {
            $projects = $this->projectRepository->getAllProjectsForCompanyId($companyId);
            foreach ($projects as $project) {
                $this->projectRepository->deleteProject($project->id);
            }

            $addressBookEntries = $this->addressBookRepository->getAddressBookEntriesByCompany($companyId);
            foreach ($addressBookEntries as $addressBookEntry) {
                $this->addressBookRepository->deleteEntry($addressBookEntry->id);
            }

            $usersEntries = $this->usersRepository->getAllUsersByCompany($companyId);
            foreach ($usersEntries as $usersEntry) {
                $usersEntry->delete();
            }

            return $company->delete();
        }

        return false;
    }
}