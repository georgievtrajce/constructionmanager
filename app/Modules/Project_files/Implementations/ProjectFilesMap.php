<?php
namespace App\Modules\Project_files\Implementations;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProjectFilesMap {

    private $companyId;

    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
    }

    /**
     * Returns the specified project file download link
     * @param $projectId
     * @param $fileType
     * @param $projectFile
     * @param $generalCompanyId
     * @return mixed
     */
    public function downloadLinkMap($projectId, $fileType, $projectFile, $generalCompanyId)
    {
        switch ($fileType) {
            case "proposals":
                $returnLink = 'company_'.$generalCompanyId.'/project_'.$projectId.'/'.$fileType.'/supplier_'.$projectFile->proposal_file->proposal_supplier->id.'/'.$projectFile->file_name;
                break;
            case "contracts":
                $returnLink = 'company_'.$generalCompanyId.'/project_'.$projectId.'/'.$fileType.'/contract_'.$projectFile->contract_file->contr_id.'/'.$projectFile->file_name;
                break;
            case "submittals":
                $returnLink = 'company_'.$generalCompanyId.'/project_'.$projectId.'/'.$fileType.'/'.$projectFile->file_name;
                break;
            default:
                $returnLink = 'company_'.$generalCompanyId.'/project_'.$projectId.'/'.$fileType.'/'.$projectFile->file_name;
        }

        return $returnLink;
    }

    /**
     * Returns all project files for specified project with an edit link
     * @param $projectId
     * @param $fileType
     * @param $projectFiles
     * @return mixed
     */
    public function editLinkMap($projectId, $fileType, $projectFiles)
    {
        switch ($fileType) {
            case "proposals":
                foreach($projectFiles as $file) {
                    $file->edit_link = 'projects/'.$projectId.'/proposals/'.$file->proposal_file->proposal_supplier->prop_id.'/suppliers/'.$file->proposal_file->proposal_supplier->id.'/files/'.$file->proposal_file->file_id.'/edit';
                }
                $return = $projectFiles;
                break;
            case "contracts":
                foreach($projectFiles as $file) {
                    $file->edit_link = 'projects/'.$projectId.'/contracts/'.$file->contract_file->contr_id.'/files/'.$file->id.'/edit';
                }
                $return = $projectFiles;
                break;
            case "submittals":
                foreach($projectFiles as $file) {
                    $file->edit_link = 'projects/'.$projectId.'/submittals/'.$file->submittal_version[0]->submittal->id.'/version/'.$file->submittal_version[0]->id.'/edit';
                }
                $return = $projectFiles;
                break;
            default:
                foreach($projectFiles as $file) {
                    $file->edit_link = 'projects/'.$projectId.'/project-files/'.$fileType.'/file/'.$file->id.'/edit';
                }
                $return = $projectFiles;
                break;
        }

        return $return;
    }

    /**
     * Returns the specified project file create new link
     * @param $projectId
     * @param $fileType
     * @return string
     */
    public function createLinkMap($projectId, $fileType)
    {
        switch ($fileType) {
            case "submittals":
                $returnLink = 'projects/'.$projectId.'/submittals/create';
                break;
            case "proposals":
                $returnLink = 'projects/'.$projectId.'/proposals/create';
                break;
            case "contracts":
                $returnLink = 'projects/'.$projectId.'/contracts/create';
                break;
            default:
                $returnLink = 'projects/'.$projectId.'/project-files/'.$fileType.'/file/create';
        }

        return $returnLink;
    }

}