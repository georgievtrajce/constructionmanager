<?php
namespace App\Modules\Project_files\Implementations;


use App\Modules\Project_files\Interfaces\EmailNotificationsInterface;

class NotificationsWrapper
{

    /**
     * Mapper for company emails for subcontractors and recipients imported from address book
     * into every Project Files section
     * @param EmailNotificationsInterface $notificationsInterface
     * @param $data
     * @return mixed
     */
    public function wrapCompanyEmails(EmailNotificationsInterface $notificationsInterface, $data)
    {
        return $notificationsInterface->getCompanyEmails($data);
    }

    public function wrapUserEmails(EmailNotificationsInterface $notificationsInterface, $data)
    {
        return $notificationsInterface->getProjectFilesUserEmails($data);
    }


}