<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 7/25/2016
 * Time: 10:02 AM
 */

namespace App\Modules\Project_files\Implementations;


use App\Modules\Project_files\Interfaces\EmailNotificationsInterface;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;

class ProjectFileNotifications implements EmailNotificationsInterface
{

    private $repo;
    private $response;

    public function __construct()
    {
        $this->repo = new ProjectFilesRepository();
    }

    public function getCompanyEmails($data)
    {
            return $this->response = $this->repo->getProjectFilesCompanyEmails($data['projectId'], $data['contactIds']);
    }

    /**
     * Get user emails
     * @param $data
     * @return mixed|null
     */
    public function getProjectFilesUserEmails($data)
    {
           return $this->response = $this->repo->getProjectFilesUserEmails($data['userIds']);
    }

}