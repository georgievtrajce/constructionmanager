<?php
namespace App\Modules\Project_files\Interfaces;

interface EmailNotificationsInterface
{

    public function getCompanyEmails($data);

    public function getProjectFilesUserEmails($data);

}