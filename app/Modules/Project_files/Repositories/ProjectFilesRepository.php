<?php
namespace App\Modules\Project_files\Repositories;

use App\Models\Ab_contact;
use App\Models\Ab_file;
use App\Models\File_permission;
use App\Models\File_type;
use App\Models\Pco_version;
use App\Models\Pco_version_file;
use App\Models\Project;
use App\Models\Project_company_contact;
use App\Models\Project_file;
use App\Models\Project_files_distribution;
use App\Models\Project_subcontractor;
use App\Models\Rfi_version;
use App\Models\Rfi_version_file;
use App\Models\Submittal_version;
use App\Models\Submittal_version_file;
use App\Models\TasksFile;
use App\Models\Transmittal;
use App\Models\User;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Repositories\FilesRepository;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;


/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/9/2015
 * Time: 4:31 PM
 * @property FilesRepository filesRepo
 */

class ProjectFilesRepository {

    private $companyId;
    private $userId;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        if (Auth::user() != null) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * Get all uploaded project files by type
     * @param $projectId
     * @param $type
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllProjectFilesByType($projectId, $type, $sort, $order)
    {
        $filesType = File_type::where('display_name','=',$type)->first();
        $fullQuery = Project_file::where('ft_id','=',$filesType->id)
            ->where('files.proj_id','=',$projectId)
            ->where('files.comp_id','=',$this->companyId)
            ->where('files.is_finished','=', 1);
        return ($this->processProjectFiles($type, $fullQuery, $sort, $order, config('constants.pagination.project_files')));
    }

    /**
     * Get all uploaded project files by type
     * @param $projectId
     * @param $type
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllSmallProjectFilesByTypeAndCompany($projectId, $companyId, $type, $sort, $order)
    {
        $filesType = File_type::where('display_name','=',$type)->first();
        $fullQuery = Project_file::where('ft_id','=',$filesType->id)
            ->whereRaw('CAST(size AS UNSIGNED) < 250000000')
            ->where('files.proj_id','=',$projectId)
            ->where('files.comp_id','=',$companyId);
        return ($this->processProjectFiles($type, $fullQuery, $sort, $order));
    }

    /**
     * Get all project files by project
     *
     * @param $projectId
     * @return mixed
     */
    public function getAllProjectFilesByProject($projectId)
    {
        return Project_file::where('files.proj_id','=',$projectId)->get();
    }

    /**
     * Get all received project files by type
     * @param $projectId
     * @param $type
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getAllReceivedProjectFilesByType($projectId, $type, $sort, $order, $paginate = 10)
    {
        $filesType = File_type::where('display_name','=',$type)->first();
        return Project_file::where('ft_id','=',$filesType->id)
            ->where('files.proj_id','=',$projectId)
            ->rightJoin('file_permissions','file_permissions.file_id','=','files.id')
            ->select('files.*','file_permissions.comp_id as shared_company_id')
            ->where('file_permissions.comp_id','=',$this->companyId)
            ->with('type')
            ->with('submittal_version')
            ->with('submittal_version.submittal')
            ->with('contract_file')
            ->with('proposal_file')
            ->with('proposal_file.proposal_supplier')
            ->with('proposal_file.proposal_supplier.proposal')
            ->with('companies')
            ->with('share_company')
            ->with('shares')
            ->orderBy($sort,$order)
            ->paginate($paginate);
    }

    /**
     * Get all uploaded project files shares with different companies
     * @param $projectId
     * @param $type
     * @param int $paginate
     * @return mixed
     */
    public function getSharedProjectFilesByType($projectId, $type, $paginate = 10)
    {
        $filesType = File_type::where('display_name','=',$type)->first();
        return Project_file::where('ft_id','=',$filesType->id)
            ->where('files.proj_id','=',$projectId)
            ->where('files.comp_id','=',$this->companyId)
            ->rightJoin('file_permissions','file_permissions.file_id','=','files.id')
            ->where('file_permissions.share_comp_id','=',$this->companyId)
            ->join('companies','file_permissions.comp_id','=','companies.id')
            ->select('files.*',
                'file_permissions.share_comp_id as share_comp_id',
                'file_permissions.comp_id as receive_comp_id',
                'file_permissions.file_id as shared_file_id',
                'companies.name as company_name')
            ->with('type')
            ->with('submittal_version')
            ->with('submittal_version.submittal')
            ->with('contract_file')
            ->with('proposal_file')
            ->with('proposal_file.proposal_supplier')
            ->with('proposal_file.proposal_supplier.proposal')
            ->orderBy('files.id','DESC')
            ->paginate($paginate);
    }

    /**
     * Get all received project files shares with different companies
     * @param $projectId
     * @param $type
     * @param int $paginate
     * @return mixed
     */
    public function getSharedReceivedProjectFilesByType($projectId, $type, $paginate = 10)
    {
        $filesType = File_type::where('display_name', '=', $type)->first();
        $files = Project_file::where('ft_id', '=', $filesType->id)
            ->where('files.proj_id', '=', $projectId)
            //->where('files.comp_id','=',$this->companyId)
            ->whereIn('files.id', File_permission::all(['file_id'])->toArray())
            ->rightJoin('file_permissions','file_permissions.file_id','=','files.id')
            ->rightJoin('companies','file_permissions.comp_id', '=', 'companies.id')
            ->where('file_permissions.share_comp_id', '=', $this->companyId)
            ->where('files.comp_id', '!=', $this->companyId)
            ->select('files.*',
                'file_permissions.comp_id as receive_comp_id',
                'file_permissions.share_comp_id as share_comp_id',
                'companies.name as company_name',
                'companies.id as company_id')
            ->with('type')
            ->with('submittal_version')
            ->with('submittal_version.submittal')
            ->with('contract_file')
            ->with('proposal_file')
            ->with('proposal_file.proposal_supplier')
            ->with('proposal_file.proposal_supplier.proposal')
            //->with('companies')
            //->with('share_company')
            ->orderBy('files.id','DESC')
            ->paginate($paginate);
        return $files;
    }

    public function getAllProjectFilesModuleTypes()
    {
        return File_type::whereNotIn('id', [1,2,3,7,8])->get();
    }

    /**
     * Get project file specified by name
     * @param $projectId
     * @param $fileName
     * @return mixed
     */
    public function getProjectFileByName($projectId, $fileName)
    {
        return Project_file::where('proj_id','=',$projectId)
            ->where('file_name','=',$fileName)
            ->with('type')
            ->with('submittal_version')
            ->with('submittal_version.submittal')
            ->with('proposal_file')
            ->with('proposal_file.proposal_supplier')
            ->with('proposal_file.proposal_supplier.proposal')
            ->with('contract_file')
            ->with('company')
            ->firstOrFail();
    }

    /**
     * Get project file by project id and file id
     * @param $projectId
     * @param $fileId
     * @return mixed
     */
    public function getProjectFile($projectId, $fileId)
    {
        return Project_file::where('proj_id','=',$projectId)
            ->with('project')
            ->with('projectFileUsers.users.company')
            ->with('projectFileUsers.abUsers.addressBook')
            ->where('id','=',$fileId)
            ->firstOrFail();
    }

    /**
     * Returns contacts from project file distribution table
     *
     * @param $fileId
     * @return mixed
     */
    public function getContactsForProjectFiles($fileId)
    {
        return Project_files_distribution::where('project_file_id', '=', $fileId)
            ->get();
    }

    /**
     * Get project file by file id
     * @param $fileId
     * @return mixed
     */
    public function getProjectFileById($fileId)
    {
        return Project_file::where('id','=',$fileId)
            ->firstOrFail();
    }

    /**
     * Get address book file by file id
     * @param $fileId
     * @return mixed
     */
    public function getAddressBookFileById($fileId)
    {
        return Ab_file::where('id','=',$fileId)
            ->firstOrFail();
    }

    /**
     * Get task file by file id
     * @param $fileId
     * @return mixed
     */
    public function getTasksFileById($fileId)
    {
        return TasksFile::where('id','=',$fileId)
            ->firstOrFail();
    }

    /**
     * Get file by its id
     * @param $id
     * @return mixed
     */
    public function getFileById($id)
    {
        return Project_file::where('id','=',$id)
            ->with('company')
            ->first();
    }

    /**
     * Get all project files share records needed for checking
     * the file delete allowance
     * @param $fileId
     * @return mixed
     */
    public function getProjectFileShareRecords($fileId)
    {
        return File_permission::where('file_id','=',$fileId)->get();
    }

    public function getProjectFilesCompanyEmails($projectId, $contactIds)
    {
        return Ab_contact::leftJoin('project_company_contacts', 'project_company_contacts.ab_cont_id', '=', 'ab_contacts.id')
            ->leftJoin('project_subcontractors', function($join)
            {
                $join->on('project_company_contacts.proj_comp_id', '=', 'project_subcontractors.proj_comp_id')
                    ->where('project_subcontractors.once_accepted','=',Config::get('constants.projects.once_accepted.yes'))
                    ->where('project_subcontractors.status','=',Config::get('constants.projects.shared_status.accepted'));
            })
            ->join('projects', 'projects.id', '=', 'project_company_contacts.proj_id')
            ->join('address_book', 'address_book.id', '=', 'ab_contacts.ab_id')
            ->where('projects.id', '=', $projectId)
            ->whereIn('ab_contacts.id', $contactIds)
            ->select(
                'project_subcontractors.comp_child_id as comp_child_id',
                'ab_contacts.*',
                'projects.name as project_name',
                'projects.id as project_id',
                'address_book.name as companyName'
            )
            ->groupBy(['ab_contacts.id'])
            ->get();
    }

    /**
     * Get User emails from my company
     * @param $fileId
     * @return mixed
     */
    public function getProjectFilesUserEmails($userIds)
    {
        return User::join('companies', 'companies.id', '=', 'users.comp_id')
                    ->whereIn('users.id', $userIds)
                    ->selectRaw('users.email, users.name, companies.name as companyName')
                    ->where('comp_id', '=', Auth::user()->comp_id)
                    ->get();
    }

    /**
     * Get Contracts by id
     *
     * @param $contractIds
     * @return mixed
     */
    public function getContractByIdArray($contractIds)
    {
        return Ab_contact::whereIn('id', $contractIds)->get();
    }

    /**
     * Store project file in database
     * @param $fileType
     * @param $projectId
     * @param $data
     * @param null $fileId
     * @return static
     */
    public function storeOrUpdateProjectFile($fileType, $projectId, $data, $oldFileId = null)
    {
        $type = File_type::where('display_name','=',$fileType)->firstOrFail();
        if (empty($data['file_id'])) {
            $file = Project_file::create([
                'ft_id' => $type->id,
                'user_id' => $this->userId,
                'comp_id' => $this->companyId,
                'proj_id' => $projectId,
                'name' => $data['file_name'],
                'number' => $data['file_number'],
                'mf_number' => $data['master_format_number'],
                'mf_title' => $data['master_format_title'],
                'is_finished' => 1,
            ]);

            return $file;
        }

        $fileIds = explode('-', $data['file_id']);

        $i = 0;
        $returnFile = null;
        foreach ($fileIds as $fileId) {
                $fileData = array();

                $fileData['name'] = $data['file_name'].strval(($i == 0)?'':'-'.$i);
                $fileData['number'] = $data['file_number'];
                $fileData['mf_number'] = $data['master_format_number'];
                $fileData['mf_title'] = $data['master_format_title'];
                $fileData['is_finished'] = 1;
                Project_file::where('id','=',(int)$fileId)->update($fileData);
                $file = Project_file::where('id','=',(int)$fileId)->firstOrFail();
                if ($i == 0) {
                    $returnFile = $file;
                }
                $i++;

                if ($oldFileId != $fileId) {
                    Project_file::where('id','=',(int)$oldFileId)->delete();
                }
        }

        return $returnFile;
    }

    public function markFileSentTo($fileId, $data)
    {
        if (!empty($data['employees_users_in'])) {
            foreach ($data['employees_users_in'] as $userId) {
                $projectFile = Project_files_distribution::firstOrCreate([
                    'project_file_id' => $fileId,
                    'user_id' => $userId,
                ]);

                $projectFile->created_at = Carbon::now()->toDateString();
                $projectFile->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['employees_contacts_in'])) {
            foreach ($data['employees_contacts_in'] as $contactId) {
                $projectFile = Project_files_distribution::firstOrCreate([
                    'project_file_id' => $fileId,
                    'ab_cont_id' => $contactId,
                ]);

                $projectFile->created_at = Carbon::now()->toDateString();
                $projectFile->save();
            }
        }
    }

    /**
     * Delete project file from s3
     * @param $projectId
     * @param $fileType
     * @param $fileName
     * @return bool
     */
    public function deleteProjectFileAws($projectId, $fileType, $fileName)
    {
        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/'.$fileType.'/'.$fileName;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Delete specified project file
     * @param $projectId
     * @param $fileId
     * @return mixed
     */
    public function deleteProjectFile($projectId, $fileId)
    {
        return Project_file::where('id','=',$fileId)
            ->where('proj_id','=',$projectId)
            ->where('comp_id','=',$this->companyId)
            ->delete();
    }

    public function deleteProjectFilesByProjectId($projectId)
    {
        return Project_file::where('proj_id','=',$projectId)->delete();
    }

    /**
     * Delete single project file
     * delete from files and contract_files tables
     * @param $params
     * @return bool
     */
    public function deleteSingleProjectFile($params)
    {
        //get files from contract_files and files tables
        $file = Project_file::where('id','=',$params['fileId'])
            ->where('comp_id','=',Auth::user()->comp_id)
            ->first();

        //delete if exists
        if (!is_null($file)) {
            $fileDelete = $file->delete();
        }

        //if file exists in table and is successfully deleted, return true
        if (isset($fileDelete) && !empty($fileDelete)) {

            //if storage limit bellow 90% reset notification
            $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
            $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
            $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
            $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
            $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

            if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
                $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
            }

            return true;
        }
        return false;
    }

    /**
     * Share selected files with all subcontractors
     * @param $projectId
     * @param $selectedFiles
     * @return bool
     */
    public function shareWithAllSubcontractors($projectId, $selectedFiles)
    {
        //get all subcontractors for the project
        $allProjectSubcontractors = Project_subcontractor::where('proj_id','=',$projectId)
            ->where('comp_parent_id','=',$this->companyId)
            ->get();

        if(count($allProjectSubcontractors)) {
            foreach($selectedFiles as $file) {
                foreach($allProjectSubcontractors as $subcontractor) {
                    File_permission::firstOrCreate([
                        'file_id' => $file,
                        'comp_id' => $subcontractor->comp_child_id,
                        'share_comp_id' => $this->companyId
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Share selected files with selected subcontractors or with
     * the general contractor
     * @param $selectedFiles
     * @param $selectedSubcontractors
     * @return bool
     */
    public function shareWithSelectedSubcontractors($selectedFiles, $selectedSubcontractors)
    {
        foreach($selectedFiles as $file) {
            foreach($selectedSubcontractors as $subcontractor) {
                File_permission::firstOrCreate([
                    'file_id' => $file,
                    'comp_id' => $subcontractor,
                    'share_comp_id' => $this->companyId
                ]);
            }
        }

        return true;
    }

    /**
     * Unshare all file shares from second level of sharing and further posible shares
     * @param $fileId
     * @param $shareSubcontractorId
     * @return bool
     */
    public function unshareNextLevels($fileId, $shareSubcontractorId)
    {
        $nextLevelUnshare = File_permission::where('file_id','=',$fileId)
            ->where('share_comp_id','=',$shareSubcontractorId)
            ->get();
        if(count($nextLevelUnshare) > 0) {
            foreach($nextLevelUnshare as $unshareFile) {
                $nextShareCompanyId = $unshareFile->comp_id;
                $unshareFile->delete();

                //recursion until there are still shares of the specified file
                $this->unshareNextLevels($fileId, $nextShareCompanyId);
            }
        }
        return true;
    }

    /**
     * Unshare specified project file with specified company (first level of sharing)
     * @param $fileId
     * @param $shareCompanyId
     * @param $receiveCompanyId
     * @return bool
     */
    public function unshareFile($fileId, $shareCompanyId, $receiveCompanyId)
    {
        $sharingStep = File_permission::where('file_id','=',$fileId)
            ->where('share_comp_id','=',$shareCompanyId)
            ->where('comp_id','=',$receiveCompanyId)
            ->firstOrFail();

        //do the next levels of unsharing
        $nextLevelUnshare = $this->unshareNextLevels($fileId, $sharingStep->comp_id);

        if($nextLevelUnshare) {
            $sharingStep->delete();
            return true;
        }
        return false;
    }

    /**
     * Get only shared project files by specified file type
     * @param $projectID
     * @param $projectCreatorId
     * @param $type
     * @param $sort
     * @param $order
     * @param $sharedFilesType
     * @return mixed
     */
    public function getOnlySharedProjectFilesByType($projectID, $projectCreatorId, $type, $sort, $order, $sharedFilesType)
    {
        $filesType = File_type::where('display_name', '=', $type)->first();

        if ($sharedFilesType == Config::get('constants.shared_files.master')) {
            $fullQuery = Project_file::join('file_permissions', 'file_permissions.file_id', '=', 'files.id')
                ->where('file_permissions.comp_id', '=', Auth::user()->comp_id)
                ->where('files.ft_id', '=', $filesType->id)
                ->where('files.proj_id', '=', $projectID)
                ->where('files.comp_id', '!=', Auth::user()->comp_id)
                ->where('files.comp_id', '=', $projectCreatorId)
                ->with('company')
                ->select('files.*',
                    'files.id as f_id');
        } else {
            $fullQuery = Project_file::join('file_permissions', 'file_permissions.file_id', '=', 'files.id')
                ->where('file_permissions.comp_id', '=', Auth::user()->comp_id)
                ->where('files.ft_id', '=', $filesType->id)
                ->where('files.proj_id', '=', $projectID)
                ->where('files.comp_id', '!=', Auth::user()->comp_id)
                ->where('files.comp_id', '!=', $projectCreatorId)
                ->with('company')
                ->select('files.*',
                    'files.id as f_id');
        }

        return ($this->processProjectFiles($type, $fullQuery, $sort, $order));
    }

    /**
     * Get all allowed project files by type
     * @param $projectID
     * @param $projectCreatorId
     * @param $type
     * @param $sort
     * @param $order
     * @param $sharedFilesType
     * @param $permissions
     * @return mixed
     */
    public function getAllAllowedProjectFilesByType($projectID, $projectCreatorId, $type, $sort, $order, $sharedFilesType, $permissions)
    {
        $filesType = File_type::where('display_name', '=', $type)->first();

        if ($sharedFilesType == Config::get('constants.shared_files.master')) {
            if ($permissions == Config::get('constants.module_permission_type.companies')) {
                $fullQuery = Project_file::join('project_company_permissions', 'project_company_permissions.entity_id', '=', 'files.ft_id')
                    ->where('project_company_permissions.proj_id','=', $projectID)
                    ->where('project_company_permissions.comp_child_id','=', Auth::user()->comp_id)
                    ->where('project_company_permissions.read','=', 1)
                    ->where('project_company_permissions.entity_id', '=', $filesType->id)
                    ->where('project_company_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                    ->where('files.proj_id', '=', $projectID)
                    ->where('files.comp_id', '!=', Auth::user()->comp_id)
                    ->where('files.comp_id', '=', $projectCreatorId)
                    ->select('files.*',
                        'files.id as f_id');
            } else {
                $fullQuery = Project_file::join('proposal_supplier_permissions', 'proposal_supplier_permissions.entity_id', '=', 'files.ft_id')
                    ->where('proposal_supplier_permissions.proj_id','=', $projectID)
                    ->where('proposal_supplier_permissions.comp_child_id','=', Auth::user()->comp_id)
                    ->where('proposal_supplier_permissions.read','=', 1)
                    ->where('proposal_supplier_permissions.entity_id', '=', $filesType->id)
                    ->where('proposal_supplier_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                    ->where('files.proj_id', '=', $projectID)
                    ->where('files.comp_id', '!=', Auth::user()->comp_id)
                    ->where('files.comp_id', '=', $projectCreatorId)
                    ->select('files.*',
                        'files.id as f_id');
            }
        } else {
            if ($permissions == Config::get('module_permission_type.companies')) {
                $fullQuery = Project_file::join('project_company_permissions', 'project_company_permissions.entity_id', '=', 'files.ft_id')
                    ->where('project_company_permissions.proj_id','=', $projectID)
                    ->where('project_company_permissions.comp_child_id','=', Auth::user()->comp_id)
                    ->where('project_company_permissions.read','=', 1)
                    ->where('project_company_permissions.entity_id', '=', $filesType->id)
                    ->where('project_company_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                    ->where('files.proj_id', '=', $projectID)
                    ->where('files.comp_id', '!=', Auth::user()->comp_id)
                    ->where('files.comp_id', '!=', $projectCreatorId)
                    ->select('files.*',
                        'files.id as f_id');
            } else {
                $fullQuery = Project_file::join('proposal_supplier_permissions', 'proposal_supplier_permissions.entity_id', '=', 'files.ft_id')
                    ->where('proposal_supplier_permissions.proj_id','=', $projectID)
                    ->where('proposal_supplier_permissions.comp_child_id','=', Auth::user()->comp_id)
                    ->where('proposal_supplier_permissions.read','=', 1)
                    ->where('proposal_supplier_permissions.entity_id', '=', $filesType->id)
                    ->where('proposal_supplier_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                    ->where('files.proj_id', '=', $projectID)
                    ->where('files.comp_id', '!=', Auth::user()->comp_id)
                    ->where('files.comp_id', '!=', $projectCreatorId)
                    ->select('files.*',
                        'files.id as f_id');
            }
        }


        return ($this->processProjectFiles($type, $fullQuery, $sort, $order));

    }

    /**
     * @param $type
     * @param $query
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function processProjectFiles($type, $query, $sort, $order, $paginate = 10) {
        if ($type == Config::get('constants.contracts')) {
            $query->rightJoin('contract_files','files.id','=','contract_files.file_id')
                ->rightJoin('contracts','contract_files.contr_id','=','contracts.id')
                ->select('files.*','contracts.id as contract_id');
        }
        if ($type == Config::get('constants.proposals')) {
            $query->rightJoin('proposal_files','files.id','=','proposal_files.file_id')
                ->rightJoin('proposal_suppliers','proposal_files.prop_supp_id','=','proposal_suppliers.id')
                ->rightJoin('proposals','proposal_suppliers.prop_id','=','proposals.id')
                ->select('files.*','proposals.id as proposal_id');
        }
        if ($type == Config::get('constants.submittals')) {
            $query->rightJoin('submittal_version_files','submittal_version_files.file_id','=','files.id')
                ->rightJoin('submittals_versions','submittal_version_files.submittal_version_id','=','submittals_versions.id')
                ->select('files.*','submittals_versions.submittal_id as submittal_id');
        }

        return $query->with('type')
            ->with('submittal_version')
            ->with('submittal_version.submittal')
            ->with('contract_file')
            ->with('proposal_file')
            ->with('proposal_file.proposal_supplier')
            ->with('proposal_file.proposal_supplier.proposal')
            ->with('companies')
            ->with('shares')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Get specific file type by its display name
     * @param $displayName
     * @return mixed
     */
    public function getFileTypeByDisplayName($displayName)
    {
        return File_type::where('display_name', '=', $displayName)->firstOrFail();
    }

    /**
     * Store module file (ajax route)
     * stores submittal, rfi and pco files
     * @param $filename
     * @param $filesize
     * @param $type
     * @param $projectId
     * @param $exsistingFileId
     * @param $versionDate
     * @param $module
     * @return mixed
     */
    public function storeFile($filename, $filesize, $type, $projectId, $exsistingFileId, $versionDate, $module)
    {
        $transferLimitation = new DataTransferLimitation();
        //check upload file allowance
        if ($transferLimitation->checkUploadTransferAllowance($filesize)) {
            $fileType = File_type::where('display_name', '=', $module)->firstOrFail();

            if (empty($exsistingFileId)) {
                $file = Project_file::create([
                    'ft_id' => $fileType->id,
                    'user_id' => $this->userId,
                    'comp_id' => $this->companyId,
                    'proj_id' => $projectId,
                    'version_date_connection' => $versionDate,
                    'size' => $filesize
                ]);
            } else {
                $file = Project_file::where('id', '=', $exsistingFileId)->first();
                $disk = Storage::disk('s3');
                //dd($file->proj_id);
                $filePath = 'company_' . $this->companyId . '/project_' . $file->proj_id . '/' . $module . '/' . $file->file_name;
                if ($disk->exists($filePath)) {
                    $disk->delete($filePath);
                }
            }

            //get submittal file original name
            $originalName = $filename;

            //parse the original name to get only the name without extension
            $parseOriginalName = substr($originalName, 0, strrpos($originalName, "."));

            //get current project upc
            $project = Project::where('id', '=', $projectId)
                ->select(array('upc_code'))
                ->firstOrFail();

            //create new submittal file name
            $filename = $parseOriginalName . '_' . $project->upc_code . '_' . $file->id . '.' . $type;

            $file->file_name = $filename;
            $file->save();

            $fullFilePath = 'company_' . $this->companyId . '/project_' . $projectId . '/' . $module . '/' . $filename;

            //return $fullFilePath;
            return Response::json(array('not_allowed' => 0, 'full_file_path' => $fullFilePath, 'file_name' => $filename, 'file_id' => $file->id));
        }

        return Response::json(array('not_allowed' => 1));
    }

    /**
     * Store module shared file (ajax route)
     * stores submittal, rfi and pco files
     * @param $filename
     * @param $filesize
     * @param $type
     * @param $projectId
     * @param $exsistingFileId
     * @param $versionDate
     * @param $module
     * @return mixed
     */
    public function storeFileShared($filename, $filesize, $type, $projectId, $exsistingFileId, $versionDate, $module, $version)
    {
        $transferLimitation = new DataTransferLimitation();
        //check upload file allowance

        $projectRepo = App::make('App\Modules\Project\Repositories\ProjectRepository');
        $project = $projectRepo->getProjectRecord($projectId);

        //if level zero check parent for limit
        if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-zero')) {
            $companyId = $project->comp_id;
        } else {
            $companyId = $this->companyId;
        }

        if ($transferLimitation->checkUploadTransferAllowance($filesize,$companyId)) {
            $fileType = File_type::where('display_name', '=', $module)->firstOrFail();

            if (empty($exsistingFileId)) {
                $file = Project_file::create([
                    'ft_id' => $fileType->id,
                    'user_id' => $this->userId,
                    'comp_id' => $companyId,
                    'proj_id' => $projectId,
                    'version_date_connection' => $versionDate,
                    'size' => $filesize
                ]);
            } else {
                $file = Project_file::where('id', '=', $exsistingFileId)->first();
                $disk = Storage::disk('s3');
                //dd($file->proj_id);
                $filePath = 'company_' . $project->comp_id . '/project_' . $file->proj_id . '/' . $module . '/' . $file->file_name;
                if ($disk->exists($filePath)) {
                    $disk->delete($filePath);
                }
            }

            //get submittal file original name
            $originalName = $filename;

            //parse the original name to get only the name without extension
            $parseOriginalName = substr($originalName, 0, strrpos($originalName, "."));

            //create new submittal file name
            $filename = $parseOriginalName . '_' . $project->upc_code . '_' . $file->id . '.' . $type;

            $file->file_name = $filename;
            $file->save();

            $fullFilePath = 'company_' . $project->comp_id . '/project_' . $projectId . '/' . $module . '/' . $filename;

            if ($module == 'submittals') {
                $submittalVersion = Submittal_version::where('id', '=', $version)->first();
                if ($versionDate == 'rec_sub_file') {
                    Submittal_version_file::firstOrCreate([
                        'submittal_version_id' => $version,
                        'file_id' => $file->id ,
                        'download_priority' => 1
                    ]);

                    if ($submittalVersion) {
                        $submittalVersion->rec_sub = Carbon::now()->toDateString();
                    }
                }

                if ($versionDate == 'rec_appr_file') {
                    Submittal_version_file::firstOrCreate([
                        'submittal_version_id' => $version,
                        'file_id' => $file->id,
                        'download_priority' => 3
                    ]);

                    if ($submittalVersion) {
                        $submittalVersion->rec_appr = Carbon::now()->toDateString();
                    }
                }

                if ($submittalVersion) {
                    $submittalVersion->save();
                }

            } else if ($module == 'rfis') {
                $rfiVersion = Rfi_version::where('id', '=', $version)->first();

                if ($versionDate == 'rec_sub_file') {
                    Rfi_version_file::firstOrCreate([
                        'rfi_version_id' => $version,
                        'file_id' => $file->id ,
                        'download_priority' => 1
                    ]);

                    if ($rfiVersion) {
                        $rfiVersion->rec_sub = Carbon::now()->toDateString();
                    }
                }

                if ($versionDate == 'rec_appr_file') {
                    Rfi_version_file::firstOrCreate([
                        'rfi_version_id' => $version,
                        'file_id' => $file->id,
                        'download_priority' => 3
                    ]);

                    if ($rfiVersion) {
                        $rfiVersion->rec_appr = Carbon::now()->toDateString();
                    }
                }

                if ($rfiVersion) {
                    $rfiVersion->save();
                }
            } else if ($module == 'pcos') {
                $pcoVersion = Pco_version::where('id', '=', $version)->first();

                if ($versionDate == 'rec_sub_file') {
                    Pco_version_file::firstOrCreate([
                        'pco_version_id' => $version,
                        'file_id' => $file->id ,
                        'download_priority' => 1
                    ]);

                    if ($pcoVersion) {
                        $pcoVersion->rec_sub = Carbon::now()->toDateString();
                    }
                }

                if ($versionDate == 'rec_appr_file') {
                    Pco_version_file::firstOrCreate([
                        'pco_version_id' => $version,
                        'file_id' => $file->id,
                        'download_priority' => 3
                    ]);

                    if ($pcoVersion) {
                        $pcoVersion->rec_sub = Carbon::now()->toDateString();
                    }
                }

                if ($pcoVersion) {
                    $pcoVersion->save();
                }
            }


            //return $fullFilePath;
            return Response::json(array('not_allowed' => 0, 'full_file_path' => $fullFilePath, 'file_name' => $filename, 'file_id' => $file->id));
        }

        return Response::json(array('not_allowed' => 1));
    }

    /**
     * Store transmittal file
     * @param $projectId
     * @param $fileName
     * @param $fileTypeId
     * @return static
     */
    public function storeTransmittalFile($projectId, $fileName, $fileTypeId)
    {
        return Project_file::create([
            'ft_id' => $fileTypeId,
            'transmittal' => 1,
            'user_id' => $this->userId,
            'comp_id' => $this->companyId,
            'proj_id' => $projectId,
            'file_name' => $fileName,
        ]);
    }

    /**
     * Delete old transmittal
     * @param $versionId
     * @param $type
     * @param $fileTypeId
     * @return bool
     */
    public function deleteOldTransmittals($versionId, $type, $fileTypeId)
    {
        $transmittals = Transmittal::where('version_id','=',$versionId)
            ->where('date_type','=',$type)
            ->where('file_type_id','=',$fileTypeId)
            ->get();

        foreach ($transmittals as $transmittal) {
            Project_file::where('id','=',$transmittal->file_id)->delete();
            $transmittal->delete();
        }

        return true;
    }

    /**
     * Update emailed field in files
     * @param $fileId
     * @param $emailed
     * @return mixed
     */
    public function updatedEmailedFile($fileId, $emailed)
    {
        return Project_file::where('id','=',$fileId)
            ->where('comp_id','=',$this->companyId)
            ->update([
                'emailed' => $emailed
            ]);
    }
}