<?php
namespace App\Modules\Proposals\Repositories;

use App\Models\Address_book;
use App\Models\Module;
use App\Models\Project_bidder;
use App\Models\Project_file;
use App\Models\Proposal;
use App\Models\Proposal_supplier;
use App\Models\Proposal_file;
use App\Models\File;
use App\Models\File_type;

use App\Models\Proposal_supplier_permission;
use App\Models\ProposalSupplierProposalsDistribution;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Services\CustomHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Request;
use Input;
use Flash;
use Session;

class ProposalsRepository {

    // parameters of proposal tables, used when editing

    private $proposalParams = array('proj_id', 'name', 'mf_title', 'mf_number', 'value', 'unit_price', 'quantity');
    private $proposalSupplierParams = array('proposal_1','proposal_2','proposal_final','addr_id','ab_cont_id');
    private $proposalFileParams = array('prop_supp_id', 'file_id');
    private $fileParams = array('name','number','size','file_name');
    private $projectPermissionsRepository;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        //$this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $this->proposalsModule = Module::where('display_name', '=', 'bids')->first();

        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
 * Get all proposals for project with addresses and contacts
 * @param $projectId
 * @param $sort
 * @param $order
 * @return \Illuminate\Database\Eloquent\Collection|static[]
 */
    public function getProposalsByProject($projectId, $sort, $order)
    {
        // Get all proposals for project with suppliers
        $proposals = Proposal::with(array('suppliers' => function($query) use ($sort, $order)
        {
            $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                //->where('proposal_suppliers.status','!=',Config::get('constants.bids.status.unshared'))
                ->select(
                    'proposal_suppliers.*',
                    'address_book.name as ab_name',
                    'address_book.id as ab_id',
                    'addresses.office_title as office_title',
                    'ab_contacts.name as ab_cont_name',
                    'addresses.office_title as office_title'
                )
                ->orderBy('ab_name', 'asc');
        },
            'suppliers.project_bidder',
            'suppliers.project_bidder.invited_company',
        ))
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.proposals'));
        return $proposals;

    }

    /**
     * Returns module id
     *
     * @return bool
     */
    public function getModuleId()
    {
        $proposalModule = Module::where('display_name', '=', 'bids')->first();
        if ($proposalModule) {
            return $proposalModule->id;
        } else {
            return false;
        }
    }

    /**
     * Get all proposals for project with addresses and contacts
     * @param $projectId
     * @param $sort
     * @param $order
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllProposalsByProject($projectId, $sort, $order)
    {
        // Get all proposals for project with suppliers
        $proposals = Proposal::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->orderBy($sort, $order)
            ->get();

        return $proposals;
    }

    public function getAllProposalsByProjectAndCompany($projectId, $companyId, $sort, $order)
    {
        // Get all proposals for project with suppliers
        $proposals = Proposal::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $companyId)
            ->with('suppliers.files.file')
            ->with('suppliers.files.smallFile')
            ->with('suppliers.address_book')
            ->orderBy($sort, $order)
            ->get();

        return $proposals;
    }

    /**
     * Get all proposals for project with addresses and contacts
     * @param $projectId
     * @param $sort
     * @param $order
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getProposalsByProjectSortedByFinalProposal($projectId, $sort, $order)
    {
        // Get all proposals for project with suppliers
        $proposals = Proposal::with(array('suppliers' => function($query) use ($sort, $order)
        {
            $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                //->where('proposal_suppliers.status','!=',Config::get('constants.bids.status.unshared'))
                ->select(
                    'proposal_suppliers.*',
                    'address_book.name as ab_name',
                    'address_book.id as ab_id',
                    'addresses.office_title as office_title',
                    'ab_contacts.name as ab_cont_name',
                    'addresses.office_title as office_title'
                )
                ->orderBy($sort, $order);
        },
            'suppliers.project_bidder',
            'suppliers.project_bidder.invited_company',
        ))
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->orderBy('proposals.id', 'asc')
            ->paginate(Config::get('constants.pagination.proposals'));
        return $proposals;

    }

    /**
     * Filter Proposals
     * @param $params
     * @return mixed
     */
    public function filterProposals($params)
    {
        $fullQuery = Proposal::with(array('suppliers' => function($query)
            {
                $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                    ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                    ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                    //->where('synced_comp_id', '=', Auth::user()->comp_id)
                    ->select(
                        'proposal_suppliers.*',
                        'address_book.name as ab_name',
                        'address_book.id as ab_id',
                        'addresses.office_title as office_title',
                        'ab_contacts.name as ab_cont_name',
                        'addresses.office_title as office_title'
                    )
                    ->orderBy('ab_name', 'asc');
            },
                'suppliers.project_bidder',
                'suppliers.project_bidder.invited_company',
            ))
            ->where('proj_id', '=', $params['projectId']);

        $status = $params['status'];

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['bidName'])) {
            $fullQuery->where('proposals.name', 'LIKE', $params['bidName'].'%');
        }

        if (!empty($params['bidCompany'])) {
            $fullQuery->whereRaw("(Select count(*) from proposal_suppliers, address_book where address_book.name like '" . $params['bidCompany'] . "%' AND
                                proposal_suppliers.ab_id = address_book.id AND proposal_suppliers.prop_id = proposals.id) > 0");
        }

        //Get proposals by status
        if($status != 'All') {
            $fullQuery->where('status','=',$status);
        }

        return $fullQuery->select(
                'proposals.*'
            )
            ->orderBy($params['sort'], $params['order'])
            ->paginate(Config::get('constants.pagination.proposals'));
    }

    /**
     * Filter proposals which are sorted by bidder final proposal
     * @param $params
     * @return mixed
     */
    public function filterProposalsSortedByFinalProposal($params)
    {
        $sort = $params['sort'];
        $order = $params['order'];
        $fullQuery = Proposal::with(array('suppliers' => function($query) use ($sort, $order)
            {
                $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                    ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                    ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                    //->where('proposal_suppliers.status','!=',Config::get('constants.bids.status.unshared'))
                    ->select(
                        'proposal_suppliers.*',
                        'address_book.name as ab_name',
                        'address_book.id as ab_id',
                        'addresses.office_title as office_title',
                        'ab_contacts.name as ab_cont_name',
                        'addresses.office_title as office_title'
                    )
                    ->orderBy($sort, $order);
            },
                'suppliers.project_bidder',
                'suppliers.project_bidder.invited_company',
            ))
            ->where('proj_id', '=', $params['projectId'])
            ->where('comp_id', '=', Auth::user()->comp_id);

        $status = $params['status'];

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['bidName'])) {
            $fullQuery->where('proposals.name', 'LIKE', $params['bidName'].'%');
        }

        if (!empty($params['bidCompany'])) {
            $fullQuery->whereRaw("(Select count(*) from proposal_suppliers, address_book where address_book.name like '" . $params['bidCompany'] . "%' AND
                                proposal_suppliers.ab_id = address_book.id AND proposal_suppliers.prop_id = proposals.id) > 0");
        }

        //Get proposals by status
        if($status != 'All') {
            $fullQuery->where('status','=',$status);
        }

        return $fullQuery->select(
            'proposals.*'
        )
            ->orderBy('proposals.id', 'asc')
            ->paginate(Config::get('constants.pagination.proposals'));
    }

    /**
     * Proposals Report
     * @param $params
     * @return mixed
     */
    public function reportProposals($params)
    {
        $fullQuery = Proposal::with(array('suppliers' => function($query)
            {
                $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                    ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                    ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                    ->select(
                        'proposal_suppliers.*',
                        'address_book.name as ab_name',
                        'address_book.id as ab_id',
                        'addresses.office_title as office_title',
                        'ab_contacts.name as ab_cont_name',
                        'addresses.office_title as office_title'
                    )
                    ->orderBy('ab_name', 'asc');
            }))
            ->where('proj_id', '=', $params['projectId'])
            ->where('comp_id', '=', Auth::user()->comp_id);

        $status = $params['status'];

        //master format search field transformation in a proper search segment
        if (!empty($params['masterFormat'])) {
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
            }
        }

        //Get proposals by status
        if (!empty($status)) {
            if($status != 'All') {
                $fullQuery->where('status','=',$status);
            }
        }

        return $fullQuery->select(
            'proposals.*'
        )
            ->orderBy($params['sort'], $params['order'])
            ->get();
    }

    /**
     * Proposals report which are sorted by bidder final proposal
     * @param $params
     * @return mixed
     */
    public function reportProposalsSortedByFinalProposal($params)
    {
        $sort = $params['sort'];
        $order = $params['order'];
        $fullQuery = Proposal::with(array('suppliers' => function($query) use ($sort, $order)
        {
            $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                ->select(
                    'proposal_suppliers.*',
                    'address_book.name as ab_name',
                    'address_book.id as ab_id',
                    'addresses.office_title as office_title',
                    'ab_contacts.name as ab_cont_name',
                    'addresses.office_title as office_title'
                )
                ->orderBy($sort, $order);
        }))
            ->where('proj_id', '=', $params['projectId'])
            ->where('comp_id', '=', Auth::user()->comp_id);

        $status = $params['status'];

        //master format search field transformation in a proper search segment
        if (!empty($params['masterFormat'])) {
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
            }
        }

        //Get proposals by status
        if (!empty($status)) {
            if($status != 'All') {
                $fullQuery->where('status','=',$status);
            }
        }

        return $fullQuery->select(
            'proposals.*'
        )
            ->orderBy('proposals.id', 'asc')
            ->get();
    }

    /**
     * Get shared proposals for a specific project
     * @param $projectId
     * @param $companyCreatorId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getSharedProposalsByProject($projectId, $companyCreatorId, $sort, $order)
    {
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $companyPermissions = $this->projectPermissionsRepository->checkModulePermissions($projectId, $this->proposalsModule->id, Auth::user()->comp_id);

        if (is_null($companyPermissions)) {
            $companyCreatorId = 0;
        }

        // Get all proposals for project with suppliers
        $proposals = Proposal::with(array('suppliers' => function($query) use ($sort, $order)
        {
            $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                ->where('synced_comp_id', '=', Auth::user()->comp_id)
                ->select(
                    'proposal_suppliers.*',
                    'address_book.name as ab_name',
                    'address_book.id as ab_id',
                    'addresses.office_title as office_title',
                    'ab_contacts.name as ab_cont_name',
                    'addresses.office_title as office_title'
                )
                ->orderBy('ab_name', 'asc');
        }))
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $companyCreatorId)
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.proposals'));
        return $proposals;
    }

    /**
     * Get proposal with suppliers by id
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    public function getProposal($id)
    {
        $proposal = Proposal::with(array('suppliers' => function($query)
        {
            $query->leftJoin('addresses','addresses.id', '=','proposal_suppliers.addr_id')
                ->join('address_book','address_book.id','=','proposal_suppliers.ab_id')
                ->leftJoin('ab_contacts', 'ab_contacts.id','=','proposal_suppliers.ab_cont_id')
                //->where('proposal_suppliers.status','!=',Config::get('constants.bids.status.unshared'))
                ->select(
                    'proposal_suppliers.*',
                    'address_book.name as ab_name',
                    'address_book.id as ab_id',
                    'addresses.office_title as office_title',
                    'ab_contacts.name as ab_cont_name',
                    'addresses.office_title as office_title'
                )
                ->orderBy('ab_name', 'asc');
        },
            'suppliers.project_bidder',
            'suppliers.project_bidder.invited_company',
        ))
            ->where('id', '=', $id)
            ->firstOrFail();

        return $proposal;

    }

    /**
     * Save proposal to database
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeProposal($projectId, $data)
    {
        $proposalData = array(
            'proj_id' => $projectId,
            'mf_title' => $data['master_format_title'],
            'mf_number' => $data['master_format_number'],
            'name' => $data['name'],
            'unit_price' => CustomHelper::amountToDouble($data['unit_price']),
            'quantity' => $data['quantity'],
            'value' => CustomHelper::amountToDouble($data['value']),
            'status' => $data['status'],
            'scope_of_work' => $data['scopeOfWorkContent'],
            'proposals_needed_by' => empty($data['proposals_needed_by']) ? '' : Carbon::parse($data['proposals_needed_by'])->format('Y-m-d'),
            'comp_id' => Auth::user()->comp_id
        );
        $proposal = Proposal::firstOrCreate($proposalData);
        return $proposal;
    }


    /**
     * Update proposal by id with data
     * @param $id
     * @param $projectId
     * @param $data
     * @return mixed
     */
    public function updateProposal($id, $projectId, $data)
    {
        $proposal = Proposal::where('id','=',$id)
            ->where('comp_id','=',Auth::user()->comp_id)
            ->with('suppliers')
            ->with('suppliers.project_bidder')
            ->firstOrFail();

        $proposal->mf_title = $data['master_format_title'];
        $proposal->mf_number = $data['master_format_number'];
        $proposal->name = $data['name'];
        $proposal->unit_price = CustomHelper::amountToDouble($data['unit_price']);
        $proposal->quantity = $data['quantity'];
        $proposal->value = CustomHelper::amountToDouble($data['value']);
        $proposal->status = $data['status'];
        $proposal->scope_of_work = $data['scopeOfWorkContent'];
        $proposal->proposals_needed_by = empty($data['proposals_needed_by']) ? '' : Carbon::parse($data['proposals_needed_by'])->format('Y-m-d');

        //un-share project with all bidders for this specific bid item
        if ($data['status'] == Config::get('constants.bids.bid_item_status.closed')) {
            if (count($proposal->suppliers)) {
                foreach ($proposal->suppliers as $bidder) {
                    //count other bidders that corresponds with the same company and the same project which are not in the same bid item
                    $otherBiddersInBid = Proposal_supplier::where('ab_id','=',$bidder->ab_id)
                        ->whereHas('proposal', function($query) use ($projectId) {
                            $query->where('comp_id', '=', Auth::user()->comp_id);
                            $query->where('proj_id', '=', $projectId);
                        })
                        ->where('prop_id','!=',$proposal->id)
                        ->count();

                    //check if there aren't other bid items with the same bidder company
                    if (empty($otherBiddersInBid)) {
                        //get project bidder
                        $projectBidder = Project_bidder::where('id','=',$bidder->proj_bidder_id)
                            ->where('comp_parent_id','=',Auth::user()->comp_id)
                            ->first();

                        //check if the bidder already accepted the bid invitation or not
                        if (!is_null($projectBidder)) {
                            if ($projectBidder->once_accepted == 0) {
                                //if the bidder haven't accept the invitation, delete the record from project bidders
                                $projectBidder->delete();
                            } else {
                                $projectBidder->token = '';
                                $projectBidder->token_active = 0;
                                $projectBidder->status = Config::get('constants.bids.status.unshared');
                                $projectBidder->save();
                            }
                        }

                        $bidder->proj_bidder_id = 0;
                    }

                    $bidder->save();
                }
            }
        }

        return ($proposal->save());
    }

    /**
     * Get bidder permissions
     * @param $abId
     * @param $projectId
     * @param int $companyChildId
     * @return mixed
     */
    public function getBidderPermissions($abId, $projectId, $companyChildId = 0)
    {
        $permissions = Proposal_supplier_permission::where('ab_id','=',$abId)
            ->where('proj_id','=',$projectId);

        if (!empty($companyChildId)) {
            $permissions->where('comp_child_id','=',$companyChildId);
        }
        return $permissions->get();
    }


    /**
     * delete proposal with suppliers
     * @param $id
     * @param $projectId
     * @return mixed
     */
    public function deleteProposal($id, $projectId)
    {
        //get bid item
        $proposal = Proposal::where('id','=',$id)
            ->with('suppliers')
            ->with('suppliers.project_bidder')
            ->firstOrFail();

        //check if the bid item has any bidders
        if (count($proposal->suppliers)) {
            foreach ($proposal->suppliers as $bidder) {
                //count other bidders that corresponds with the same company and the same project which are not in the same bid item
                $otherBiddersInBid = Proposal_supplier::where('ab_id','=',$bidder->ab_id)
                    ->whereHas('proposal', function($query) use ($projectId) {
                        $query->where('comp_id', '=', Auth::user()->comp_id);
                        $query->where('proj_id', '=', $projectId);
                    })
                    ->where('prop_id','!=',$proposal->id)
                    ->count();

                //check if there aren't other bid items with the same bidder company
                if (empty($otherBiddersInBid)) {
                    //check if the bidder already accepted the bid invitation or not
                    if (!is_null($bidder->project_bidder) && $bidder->project_bidder->once_accepted == 0) {
                        //if the bidder haven't accept the invitation, delete the record from project bidders
                        $bidder->project_bidder->delete();
                    } else {
                        $bidder->project_bidder->token = '';
                        $bidder->project_bidder->token_active = 0;
                        $bidder->project_bidder->status = Config::get('constants.bids.status.unshared');
                        $bidder->project_bidder->save();
                    }

                    //delete bidder permissions for the project modules
                    $bidderPermissions = $this->getBidderPermissions($bidder->ab_id, $projectId, $bidder->project_bidder->invited_comp_id);
                    if (count($bidderPermissions)) {
                        foreach ($bidderPermissions as $permission) {
                            $permission->delete();
                        }
                    }
                }

                //delete bidder
                $bidder->delete();
            }
        }

        //delete proposal
        return ($proposal->delete());
    }

    /**
     * get proposal with proposal suppliers
     * @param $id
     * @param $supplierId
     * @return mixed
     */
    public function getProposalSupplier($id, $supplierId)
    {

        $proposal = Proposal::whereHas('suppliers', function($query) use ($supplierId) {
            $query->where('id', '=', $supplierId);
        })->where('id', '=',$id)->firstOrFail();


        return $proposal;

    }

    /**
     * get proposal by id
     *
     * @param $id
     * @return mixed
     */
    public function getProposalById($id)
    {
        $proposal = Proposal::where('id', '=',$id)->firstOrFail();
        return $proposal;
    }

    /**
     * get supplier by id with address book contact etc
     * @param $supplierId
     * @return mixed
     */
    public function getSupplier($supplierId)
    {
        $supplier = Proposal_supplier::where('id', '=', $supplierId)
            ->with('files')
            ->with('files.file')
            ->with('address_book_contact')
            ->with('address')
            ->with('address.ab_contacts')
            ->with('address_book')
            ->with('proposal')
            ->with('project_bidder')
            ->firstOrFail();
        return $supplier;
    }

    /**
     * Get bidder with all permissions
     * @param $bidderId
     * @return mixed
     */
    public function getBidderWithPermissions($bidderId)
    {
        return Proposal_supplier::where('proposal_suppliers.id', '=', $bidderId)
            ->join('proposals', function($join) {
                $join->on('proposals.id','=','proposal_suppliers.prop_id')
                    ->where('proposals.comp_id','=',Auth::user()->comp_id);
            })
            ->join('proposal_supplier_permissions', function($join) {
                $join->on('proposal_supplier_permissions.ab_id','=','proposal_suppliers.ab_id')
                    ->on('proposal_supplier_permissions.proj_id','=','proposals.proj_id');
            })
            ->leftJoin('modules', function($join) {
                $join->on('proposal_supplier_permissions.entity_id','=','modules.id')
                    ->where('proposal_supplier_permissions.entity_type','=',Config::get('constants.entity_type.module'));
            })
            ->leftJoin('file_types', function($join) {
                $join->on('proposal_supplier_permissions.entity_id','=','file_types.id')
                    ->where('proposal_supplier_permissions.entity_type','=',Config::get('constants.entity_type.file'));
            })
            ->select(
                'proposal_suppliers.*',
                'proposal_supplier_permissions.comp_parent_id as comp_parent_id',
                'proposal_supplier_permissions.comp_child_id as comp_parent_id',
                'proposal_supplier_permissions.read as read',
                'proposal_supplier_permissions.pco_permission_type as pco_permission_type',
                'proposal_supplier_permissions.write as write',
                'proposal_supplier_permissions.delete as delete',
                'proposal_supplier_permissions.entity_id as entity_id',
                'proposal_supplier_permissions.entity_type as entity_type',
                'proposals.name as bid_item_name',
                'modules.id as module_id',
                'modules.name as module_name',
                'modules.display_name as module_display_name',
                'file_types.id as file_type_id',
                'file_types.name as file_type_name',
                'file_types.display_name as file_type_display_name'
            )
            ->with('address_book')
            ->get();
    }

    /**
     * Get bidder
     * @param $projectId
     * @param $bidId
     * @param $bidderId
     * @param $abId
     * @return mixed
     */
    public function getBidder($projectId, $bidId, $bidderId, $abId)
    {
        return Proposal_supplier::where('id','=',$bidderId)
            ->whereHas('proposal', function($query) use ($bidId, $projectId) {
                $query->where('id', '=', $bidId);
                $query->where('proj_id', '=', $projectId);
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->with('proposal.project')
            ->whereHas('address_book', function($query) use ($abId) {
                $query->where('id', '=', $abId);
                $query->where('owner_comp_id', '=', Auth::user()->comp_id);
            })
            ->with('address_book_contact')
            ->firstOrFail();
    }

    public function getExistingProjectBidder($projectId, $abId)
    {
        $addressBookCompany = Address_book::where('id','=',$abId)->first();

        if (!is_null($addressBookCompany)) {
            $projectBidder = Project_bidder::where('invited_comp_id','=',$addressBookCompany->synced_comp_id)
                ->where('comp_parent_id','=',Auth::user()->comp_id)
                ->where('proj_id','=',$projectId)
                ->first();

            return $projectBidder;
        }

        return null;
    }

    /**
     * Get specific project bidder company by its invitation token
     * @param $subToken
     * @param $bidderId
     * @return mixed
     */
    public function getProjectBidderByToken($subToken, $bidderId)
    {
        return Proposal_supplier::where('id','=',$bidderId)
            ->whereHas('project_bidder', function($query) use ($subToken) {
                $query->where('token', '=', $subToken)
                    ->where('token_active','=',1);
            })
            ->with('proposal')
            ->firstOrFail();
    }

    /**
     * Check if the specified address book company is already invited to bid on the concrete project
     * @param $projectId
     * @param $abId
     * @return bool
     */
    public function checkAlreadyInvited($projectId, $abId)
    {
        $bidders = Proposal_supplier::where('proposal_suppliers.ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proposals.proj_id', '=', $projectId);
            })
            ->join('project_bidders', function($join) {
                $join->on('project_bidders.id','=','proposal_suppliers.proj_bidder_id');
            })
            ->whereIn('project_bidders.status', [Config::get('constants.bids.status.pending'), Config::get('constants.bids.status.accepted')])
            ->select(
                'proposal_suppliers.*',
                'project_bidders.status as status',
                'project_bidders.invited_comp_id as invited_comp_id',
                'project_bidders.token as token',
                'project_bidders.token_active as token_active',
                'project_bidders.once_accepted as once_accepted'
            )
            ->get();

        if (count($bidders)) {
            foreach ($bidders as $bidder) {
                if (!empty($bidder->token) && ($bidder->token_active == 1 || $bidder->once_accepted)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Get all other project bidder records
     * @param $invitedBidderRecordId
     * @param $abId
     * @param $projectId
     * @return mixed
     */
    public function getAllOtherProjectBidderRecords($invitedBidderRecordId, $abId, $projectId)
    {
        return Proposal_supplier::where('ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proj_id', '=', $projectId);
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->where('id','!=',$invitedBidderRecordId)
            ->get();
    }

    /**
     * Get accepted invite to bid for a concrete address book contact
     * @param $projectId
     * @param $abId
     * @return mixed
     */
    public function getAcceptedBid($projectId, $abId)
    {
        return Proposal_supplier::where('ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proj_id', '=', $projectId);
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->whereHas('project_bidder', function($query) {
                $query->where('status','=',Config::get('constants.bids.status.accepted'));
            })
            ->first();
    }

    /**
     * Get supplier by id with address book contact etc only if it belong to shared proposal
     * @param $projectId
     * @param $supplierId
     * @return mixed
     */
    public function getSharedSupplier($projectId, $supplierId)
    {
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $companyPermissions = $this->projectPermissionsRepository->checkModulePermissions($projectId, $this->proposalsModule->id, Auth::user()->comp_id);

        if (is_null($companyPermissions)) {
            return null;
        }

        $supplier = Proposal_supplier::where('id', '=', $supplierId)
            ->with('proposal')
            ->with('files')
            ->with('files.file')
            ->with('address_book_contact')
            ->with('address')
            ->with('address.ab_contacts')
            ->with('address_book')
            ->firstOrFail();
        return $supplier;
    }

    /**
     * Get supplier by id with address book contact etc only if it belong to shared proposal
     * @param $projectId
     * @param $supplierId
     * @return mixed
     */
    public function getSharedSupplierByAbId($projectId, $propositionId, $addressBookId)
    {
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $companyPermissions = $this->projectPermissionsRepository->checkModulePermissions($projectId, $this->proposalsModule->id, Auth::user()->comp_id);

        if (is_null($companyPermissions)) {
            return null;
        }

        $supplier = Proposal_supplier::where('ab_id', '=', $addressBookId)
            ->where('prop_id', '=', $propositionId)
            ->with('proposal')
            ->with('files')
            ->with('files.file')
            ->with('address_book_contact')
            ->with('address')
            ->with('address.ab_contacts')
            ->with('address_book')
            ->firstOrFail();
        return $supplier;
    }

    /**
     * Check if Bidder company is already added for the concrete project
     * @param $projectId
     * @param $abId
     * @return bool
     */
    public function checkMultipleTimesAddedBidder($projectId, $abId)
    {
        $bidders = Proposal_supplier::where('ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proj_id', '=', $projectId);
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->get();

        if (count($bidders) > 1) {
            return true;
        }
        return false;
    }

    /**
     * Initial bidder permissions insert
     * @param $projectId
     * @param $parentId
     * @param $bidderId
     * @param $abId
     * @return bool
     */
    public function insertInitialBidderPermissions($projectId, $parentId, $bidderId, $abId)
    {
        //get modules from modules table
        $manageUsersRepo = new ManageUsersRepository();
        $modules = $manageUsersRepo->getProjectModules();

        //get project file types from file_types table
        $projectFiles = $manageUsersRepo->getProjectFileTypes();

        //set all user permissions to 0
        //set modules permissions to 0
        foreach ($modules as $module) {
            if ($module->name != Config::get('constants.modules.projects')) {
                Proposal_supplier_permission::firstOrCreate([
                    'bidder_id' => $bidderId,
                    'comp_parent_id' => $parentId,
                    'read' => 0,
                    'write' => 0,
                    'delete' => 0,
                    'entity_id' => $module->id,
                    'entity_type' => Config::get('constants.entity_type.module'), //this means that the type of the permission is module
                    'proj_id' => $projectId,
                    'ab_id' => $abId
                ]);
            }
        }

        //set project files permissions to 0
        foreach ($projectFiles as $projectFile) {
            Proposal_supplier_permission::firstOrCreate([
                'bidder_id' => $bidderId,
                'comp_parent_id' => $parentId,
                'read' => 0,
                'write' => 0,
                'delete' => 0,
                'entity_id' => $projectFile->id,
                'entity_type' => Config::get('constants.entity_type.file'), //this means that the type of the permission is project file
                'proj_id' => $projectId,
                'ab_id' => $abId
            ]);
        }

        return true;
    }

    /**
     * store proposal supplier by proposal id with data
     * @param $id
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeSupplier($id, $projectId, $data)
    {
        //get address book company
        $addressBookCompany = Address_book::where('id','=',$data['prop_ab_id'])
            ->where('owner_comp_id','=',Auth::user()->comp_id)
            ->firstOrFail();

        $supplierData = array(
            'ab_id' => $addressBookCompany->id,
            'prop_id' => $id,
            'ab_cont_id' => $data['cont_id'],
            'addr_id' => $data['prop_addr']
        );

        if (!empty($data['proposal_1'])) {
            $supplierData['proposal_1'] = CustomHelper::amountToDouble($data['proposal_1']);
        }

        if (!empty($data['proposal_2'])) {
            $supplierData['proposal_2'] = CustomHelper::amountToDouble($data['proposal_2']);
        }

        if (!empty($data['proposal_final'])) {
            $supplierData['proposal_final'] = CustomHelper::amountToDouble($data['proposal_final']);
        }

        $supplier = Proposal_supplier::create($supplierData);

        //declare proposal files ids array
        $proposalFilesIdArr = [];

        //first proposal file
        if (!empty($data['proposal_one_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.first_proposal')] = $data['proposal_one_file_id'];
        }

        //second proposal file
        if (!empty($data['proposal_two_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.second_proposal')] = $data['proposal_two_file_id'];
        }

        //final proposal file
        if (!empty($data['proposal_final_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.final_proposal')] = $data['proposal_final_file_id'];
        }

        //create proposal files
        foreach ($proposalFilesIdArr as $key => $value) {
            Proposal_file::create([
                'prop_supp_id' => $supplier->id,
                'file_id' => $value,
                'item_no' => $key
            ]);
        }

        //create bidder initial permissions
        if (!is_null($supplier)) {
            if (!$this->checkMultipleTimesAddedBidder($projectId, $supplier->ab_id)) {
                $this->insertInitialBidderPermissions($projectId, Auth::user()->comp_id, $supplier->id, $supplier->ab_id);
            }
        }

        return $supplier;
    }

    /**
     * Store new project bidder invitation
     * @param $bidder
     * @param $projectId
     * @param $token
     * @return static
     */
    public function storeProjectBidder($bidder, $projectId, $token)
    {
        return Project_bidder::create([
            'comp_parent_id' => Auth::user()->comp_id,
            'invited_comp_id' => (!is_null($bidder->address_book) && !empty($bidder->address_book->synced_comp_id)) ? $bidder->address_book->synced_comp_id : 0,
            'ab_id' => $bidder->ab_id,
            'proj_id' => $projectId,
            'token' => $token,
            'token_active' => 1,
            'status' => Config::get('constants.bids.status.pending'),
            'once_accepted' => 0,
        ]);
    }

    /**
     * update proposal supplier with data by id
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateSupplier($id, $data)
    {
        $supplier = Proposal_supplier::where('id', '=', $id)->firstOrFail();
        $supplier->ab_id = $data['prop_ab_id'];
        $supplier->ab_cont_id = $data['ab_cont_id'];
        $supplier->addr_id = $data['addr_id'];

        if (!empty($data['proposal_1'])) {
            $supplier->proposal_1 = CustomHelper::amountToDouble($data['proposal_1']);
        }

        if (!empty($data['proposal_2'])) {
            $supplier->proposal_2 = CustomHelper::amountToDouble($data['proposal_2']);
        }

        if (!empty($data['proposal_final'])) {
            $supplier->proposal_final = CustomHelper::amountToDouble($data['proposal_final']);
        }

        $supplier->save();

        //declare proposal files ids array
        $proposalFilesIdArr = [];

        //first proposal file
        if (!empty($data['proposal_one_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.first_proposal')] = $data['proposal_one_file_id'];
        }

        //second proposal file
        if (!empty($data['proposal_two_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.second_proposal')] = $data['proposal_two_file_id'];
        }

        //final proposal file
        if (!empty($data['proposal_final_file_id'])) {
            $proposalFilesIdArr[Config::get('constants.proposal_files.final_proposal')] = $data['proposal_final_file_id'];
        }

        //create proposal files
        foreach ($proposalFilesIdArr as $key => $value) {
            $supplierFile = Proposal_file::where('prop_supp_id','=',$id)
                ->where('item_no','=',$key)
                ->first();
            if (!is_null($supplierFile)) {
                $supplierFile->file_id = $value;
                $supplierFile->save();
            } else {
                Proposal_file::create([
                    'prop_supp_id' => $id,
                    'file_id' => $value,
                    'item_no' => $key
                ]);
            }
        }

        return $supplier;
    }

    /**
     * Accept invitation to bid for bidder company
     * @param $token
     * @param $bidderId
     * @param $invitedCompanyId
     * @param $status
     * @return mixed
     */
    public function acceptBidder($token, $bidderId, $invitedCompanyId, $status)
    {
        $bidder = Proposal_supplier::where('id', '=', $bidderId)
            ->whereHas('project_bidder', function($query) use ($token, $bidderId) {
                $query->where('token', '=', $token);
                $query->where('token_active', '=', 1);
            })
            ->with('proposal')
            ->first();

        if (!is_null($bidder)) {
            $bidder->project_bidder->invited_comp_id = $invitedCompanyId;
            $bidder->project_bidder->token_active = 0;
            $bidder->project_bidder->status = $status;
            $bidder->project_bidder->once_accepted = 1;
            $bidder->project_bidder->save();

            //update company child id in bidder permissions
            $bidderPermissions = $this->getBidderPermissions($bidder->ab_id, $bidder->proposal->proj_id);
            foreach ($bidderPermissions as $permission) {
                $permission->comp_child_id = $invitedCompanyId;
                $permission->save();
            }
        }

        return $bidder;
    }

    /**
     * Update all other bidder records to accept the invitation
     * @param $abId
     * @param $projectId
     * @return mixed
     */
    public function updateOtherBidderRecordsToAccept($abId, $projectId)
    {
        return Proposal_supplier::where('ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proj_id', '=', $projectId);
            })
            ->update([
                'status' => Config::get('constants.bids.status.accepted'),
                'invited_comp_id' => Auth::user()->comp_id,
                'once_accepted' => 1,
            ]);
    }

    /**
     * delete proposal supplier
     * @param $id
     * @return mixed
     */
    public function deleteSupplier($id)
    {
        return Proposal_supplier::where('id','=',$id)
            ->whereHas('proposal', function($query) {
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->delete();
    }

    /**
     * Unshare or delete project
     * depending on whether the project is once accepted or not
     * @param $bidderId
     * @return mixed
     */
    public function unshareProjectWithBidder($bidderId)
    {
        $bidder = Proposal_supplier::where('id','=',$bidderId)
            ->whereHas('proposal', function($query) {
                $query->where('comp_id', '=', Auth::user()->comp_id);
            })
            ->with('project_bidder')
            ->firstOrFail();

        $bidderPermissions = Proposal_supplier_permission::where('ab_id','=',$bidder->ab_id)
            ->where('proj_id','=',$bidder->proposal->proj_id)
            ->get();

        //delete bidder permissions
        if (count($bidderPermissions)) {
            foreach ($bidderPermissions as $permission) {
                $permission->delete();
            }
        }

        //if the project bid is once accepted, just unshare the project
        if (!is_null($bidder->project_bidder) && $bidder->project_bidder->once_accepted == 1) {
            $bidder->project_bidder->token = '';
            $bidder->project_bidder->token_active = 0;
            $bidder->project_bidder->status = Config::get('constants.bids.status.unshared');
            $bidder->project_bidder->save();
        } elseif(!is_null($bidder->project_bidder)) {
            //delete project bidder
            $bidder->project_bidder->delete();
        }

        return $bidder->delete();
    }

    /**
     * Unshare or delete project
     * depending on whether the project is once accepted or not
     * @param $bidderId
     * @return mixed
     */
    public function unshareProject($projectId, $projectBidderId)
    {
        $bidder = Project_bidder::where('id', '=', $projectBidderId)->firstOrFail();

        //get his subcontractors
        $children = Project_bidder::where('comp_parent_id', '=', $bidder->invited_comp_id)->where('proj_id', '=', $projectId)->get();

        //recursive unshare to subcontractors
        if (sizeof($children) > 0) {
            for ($i=0; $i<sizeof($children); $i++) {
                $this->unshareProject($projectId, $children[$i]->id);
            }
        }

        //if the project is not accepted yet and have status pending (1), delete the project from project_subcontractors
        if ($bidder->once_accepted == Config::get('constants.projects.once_accepted.no')) {
            //delete subcontractor users permissions
            $manageUsersRepository = new ManageUsersRepository();

            //get all company users
            $allCompanyUsers = $manageUsersRepository->getAllCompanyUsers($bidder->invited_comp_id);

            if (count($allCompanyUsers)) {
                foreach ($allCompanyUsers as $user) {
                    $manageUsersRepository->deleteProjectUserPermissions($user->id, $projectId);
                }
            }

            return $bidder->delete();
        }

        //delete subcontractor (unshare)
        return $bidder->update([
            'token_active' => Config::get('constants.projects.status.inactive'),
            'status' => Config::get('constants.projects.shared_status.unshared')
        ]);


    }

    /**
     * Reject all other bidder records for the same company and project
     * @param $projectId
     * @param $abId
     * @return bool
     */
    public function rejectAllOtherBidderRecords($projectId, $abId)
    {
        $allOtherRecords = Proposal_supplier::where('ab_id','=',$abId)
            ->whereHas('proposal', function($query) use ($projectId) {
                $query->where('proj_id', '=', $projectId);
            })
            ->get();

        if (count($allOtherRecords)) {
            foreach ($allOtherRecords as $bidder) {
                $bidder->token = '';
                $bidder->token_active = 0;
                $bidder->status = Config::get('constants.bids.status.not_invited');
                $bidder->once_accepted = 0;
                $bidder->save();
            }
        }
        return true;
    }

    /**
     * Update proposal_suppliers for rejecting the project by the specified bidder
     * @param $token
     * @return mixed
     */
    public function rejectAllBidderRecords($token)
    {
        $bidder = Project_bidder::where('token','=',$token)
            ->where('once_accepted','=',0)
            ->with('bidders')
            ->first();

        if (!is_null($bidder)) {
            //if (count($bidder->bidders)) {
            //    foreach ($bidder->bidders as $bidderRecord) {
            //        $bidderRecord->proj_bidder_id = 0;
            //        $bidderRecord->save();
            //    }
            //}
            $bidder->token_active = 0;
            $bidder->once_accepted = 0;
            $bidder->status = Config::get('constants.bids.status.rejected');
            $bidder->save();
        }

        return $bidder;
    }

    /**
     * update proposal record in db
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateFile($id, $data){
        $file = Proposal_file::where('id','=',$id)->firstOrFail();

        foreach ($data as $key => $value){
            foreach ($this->proposalFileParams as $param){
                if($key == $param){
                    $file->$key = $value;
                }
            }
        }
        return ($file->save());
    }

    /**
     * get proposal file records for supplier id
     * @param $projectId
     * @param $proposalId
     * @param $supplierId
     * @return mixed
     */
    public function getProposalFiles($projectId, $proposalId, $supplierId)
    {
        $fileType = File_type::where('name','=','Bids')->firstOrFail();

        $files = File::join('proposal_files','proposal_files.file_id', '=','files.id')
            ->where('ft_id', '=', $fileType->id)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->where('prop_supp_id', '=', $supplierId)
            ->select('proposal_files.*', 'files.name', 'files.number','files.file_name','files.id as f_id')
            ->get();
        return $files;
    }

    /**
     * get proposal file record for supplier id
     * @param $projectId
     * @param $fileId
     * @param $supplierId
     * @return mixed
     */
    public function getProposalFile($projectId, $fileId, $supplierId)
    {
        $fileType = File_type::where('name','=','Bids')->firstOrFail();
        $file = File::join('proposal_files','proposal_files.file_id', '=','files.id')
            ->where('file_id', '=', $fileId)
            ->where('ft_id', '=', $fileType->id)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->where('prop_supp_id', '=', $supplierId)
            ->select('proposal_files.*', 'files.name', 'files.number','files.file_name', 'files.id as f_id')
            ->firstOrFail();
        return $file;
    }

    /**
     * delete file record by id
     * @param $id
     * @return mixed
     */
    public function deleteFile($id)
    {
        $file = Proposal_file::where('id','=',$id)->firstOrFail();
        return $file->delete();
    }

    /**
     * update file record
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateProposalFile($id, $data)
    {
        $file = File::where('id','=',$id)->firstOrFail();

        foreach ($data as $key => $value){
            foreach ($this->fileParams as $fileParam){
                if($key == $fileParam){
                    $file->$key = $value;
                }
            }
        }
       return $file->save();
    }

    public function getSuppliersIDsByProposal($id)
    {
        $proposal = Proposal::with('suppliers')->where('id', '=', $id)->firstOrFail();
        $supplierIDs = array();
        for ($i=0; $i<sizeof($proposal->suppliers); $i++) {
            array_push($supplierIDs, $proposal->suppliers[$i]->id);
        }
        return $supplierIDs;
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteProposalFile($params)
    {
        //delete from proposal_files table
        Proposal_file::where('file_id','=',$params['fileId'])
            ->delete();

        //delete from files table
        $result = Project_file::where('id','=',$params['fileId'])
            ->delete();

        //if storage limit bellow 90% reset notification
        $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
            $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
        }

        return $result;
    }

    /**
     * Creates an entry for each bidder adn proposal number when email is sent
     * @param $bidderId
     * @param $userId
     * @param $proposalNumber
     * @return ProposalSupplierProposalsDistribution
     */
    public function addProposalUserDistribution($bidderId, $userId, $proposalNumber)
    {
        $proposal = ProposalSupplierProposalsDistribution::firstOrCreate(
            [
                'bidder_id' => $bidderId,
                'user_id' => $userId,
                'proposal_no' => $proposalNumber
            ]);

        $proposal->created_at = Carbon::now()->toDateString();
        $proposal->save();

        return $proposal;
    }

    public function getProposalUserDistributionByBidder($bidderId, $proposalNumber)
    {
        return ProposalSupplierProposalsDistribution::where('bidder_id', '=', $bidderId)
                                                     ->where('proposal_no', '=', $proposalNumber)
                                                     ->with('user.company')
                                                     ->get();
    }
}