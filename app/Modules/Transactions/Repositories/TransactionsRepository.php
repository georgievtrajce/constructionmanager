<?php
namespace App\Modules\Transactions\Repositories;

use App\Models\Transaction;
use Illuminate\Support\Facades\Config;

class TransactionsRepository {

    /**
     * Get all transactions
     * @param $sort
     * @param $order
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getAllTransactions($sort, $order)
    {
        return Transaction::leftJoin('companies','companies.id','=','transactions.comp_id')
            ->leftJoin('subscription_types','subscription_types.id','=','transactions.subs_id')
            ->select('transactions.*',
                'companies.name as company_name',
                'subscription_types.name as subscription_type_name')
            ->with('company')
            ->with('company.addresses')
            ->with('subscription_type')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transactions'));
    }

}