<?php
namespace App\Modules\Contracts\Repositories;

use App\Models\Contract;
use App\Models\Contract_item;
use App\Models\Contract_file;
use App\Models\File;
use App\Models\Module;
use App\Models\Project;
use App\Models\Project_company_permission;
use App\Models\Project_file;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Services\CompanyPermissions;
use App\Services\CustomHelper;
use Config;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;

use App\Models\File_type;
use Illuminate\Support\Facades\Auth;
use Request;
use Input;
use Flash;
use Session;
use Paginate;

class ContractsRepository {

    private $contractParams = array('proj_id', 'ab_id', 'number', 'value');
    private $contractItemParams = array('mf_title','mf_number','name');
    private $contractFileParams = array('contr_id', 'file_id');
    private $fileParams = array('name','number','size','file_name');
    private $filesRepo;
    private $companyRepo;

    public function __construct(ProjectSubcontractorsRepository $projectSubcontractorsRepo, ProjectPermissionsRepository $projectPermissionsRepository)
    {
        $this->projectSubcontractorsRepo = $projectSubcontractorsRepo;
        $this->projectPermissionsRepository = $projectPermissionsRepository;
        $this->contractModule = Module::where('display_name', '=', 'contracts')->first();
        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * get contracts by project
     * @param $projectId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getContractsByProject($projectId, $sort, $order)
    {
        $contracts = Contract::with('items')
            ->where('contracts.comp_id', '=', Auth::user()->comp_id)
            ->join('address_book','address_book.id','=','contracts.ab_id')
            ->select('contracts.*',
                'address_book.name as company_name')
            ->where('proj_id','=',$projectId)
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.contracts'));
        return $contracts;
    }

    /**
     * get contracts by project
     * @param $projectId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllContractsByProject($projectId, $sort, $order)
    {
        $contracts = Contract::where('contracts.comp_id', '=', Auth::user()->comp_id)
                ->join('address_book','address_book.id','=','contracts.ab_id')
                ->select('contracts.*',
                    'address_book.name as company_name')
                ->where('proj_id','=',$projectId)
                ->orderBy($sort, $order)
                ->get();

        return $contracts;
    }

    /**
     * get contracts by project
     * @param $projectId
     * @param $companyId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllContractsByProjectAndCompany($projectId, $companyId, $sort, $order)
    {
        $contracts = Contract::where('contracts.comp_id', '=', $companyId)
                             ->where('proj_id','=',$projectId)
                             ->with('contractFiles.files')
                             ->with('contractFiles.smallFiles')
                             ->orderBy($sort, $order)
                             ->get();

        return $contracts;
    }

    public function filterContracts($params, $paginate = 50)
    {
        $query = [
            'contracts.proj_id' => $params['projectId'],
            'contracts.comp_id' => Auth::user()->comp_id
        ];

        if($params['company'] != '') {
            $query['contracts.ab_id'] = $params['company'];
        }

        $fullQuery = Contract::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('contracts.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('contracts.mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['name'])) {
            $fullQuery->where('contracts.name', 'LIKE', $params['name'].'%');
        }

        $fullQuery->join('address_book','address_book.id','=','contracts.ab_id')
            ->select('contracts.*',
                'address_book.name as company_name');

        if (!empty($params['sort']) && !empty($params['order'])) {
            $fullQuery->orderBy($params['sort'], $params['order']);
        }

        return $fullQuery->paginate(Config::get('constants.pagination.contracts'));
    }


    /**
     * get not own contracts if logged in is subcontractor
     * @param $projectId
     * @param $companyCreatorId
     * @param $sort
     * @param $order
     * @return bool
     */
    public function getSharedContractsByProject($projectId, $companyCreatorId, $sort, $order)
    {
        $companyPermissions = $this->projectPermissionsRepository->checkModulePermissions($projectId, $this->contractModule->id, Auth::user()->comp_id);

        if (empty($companyPermissions)) {
            $companyCreatorId = 0;
        }

        return Contract::with('items')
            ->where('contracts.comp_id','=',$companyCreatorId)
            ->join('address_book','address_book.id','=','contracts.ab_id')
            ->select('contracts.*',
                'address_book.name as company_name')
            ->where('proj_id','=',$projectId)
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.contracts'));
    }

    /**
     * get contract by id with company
     * @param $id
     * @return mixed
     */
    public function getContract($id)
    {
        $contract = Contract::join('address_book','address_book.id','=','contracts.ab_id')
            ->select('contracts.*',
                'address_book.name as company_name')
            ->where('contracts.id','=',$id)
            ->with('items')
            ->with('contractFiles')
            ->with('contractFiles.files')
            ->first();
        return $contract;

    }

    /**
     * store contract in db from input
     * @param $projectId
     * @param $data
     * @return mixed
     */
    public function storeContract($projectId, $data)
    {
        $contractData = array(
            'proj_id' => $projectId,
            'number' => $data['number'],
            'ab_id' => $data['contr_ab_id'],
            'value' => CustomHelper::amountToDouble($data['contr_price']),
            'mf_title' =>$data['mf_title'],
            'mf_number' =>$data['mf_number'],
            'name' => $data['name'],
            'comp_id' => Auth::user()->comp_id
        );
        $contract = Contract::firstOrCreate($contractData);
        return $contract->id;
    }

    /**
     * store contract item with input data
     * @param $id
     * @param $data
     * @return mixed
     */
    public function storeContractItem($id, $data)
    {
        $itemData = array(
            'contr_id' => $id,
            'mf_title' =>$data['mf_title'],
            'mf_number' =>$data['mf_number'],
            'name' => $data['name'],
        );

        $item = Contract_item::firstOrCreate($itemData);
        return $item->id;
    }

    /**
     * update contract
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateContract($id, $data)
    {
        $contract = Contract::where('id', '=', $id)->firstOrFail();
        $contract->ab_id = $data['contr_ab_id'];
        $contract->number = $data['number'];
        $contract->value = CustomHelper::amountToDouble($data['value']);
        $contract->mf_number = $data['mf_number'];
        $contract->mf_title = $data['mf_title'];
        $contract->name = $data['name'];

        return $contract->save();
    }

    /**
     * update contract item data
     * @param $contractItemId
     * @param $data
     * @return mixed
     */
    public function updateContractItem($contractItemId, $data)
    {
        $item = Contract_item::where('id', '=', $contractItemId)->firstOrFail();
        foreach ($data as $key => $value){
            foreach ($this->contractItemParams as $contractItemParam){
                if($key == $contractItemParam){
                    $item->$key = $value;
                }
            }
        }
        return $item->save();
    }

    /**
     * get contract item by id
     * @param $contractItemId
     * @return mixed
     */
    public function getContractItem($contractItemId)
    {
        $item = Contract_item::where('id', '=', $contractItemId)->firstOrFail();
        return $item;
    }

    /**
     * delete contract item by id
     * @param $contractItemId
     * @return mixed
     */
    public function deleteContractItem($contractItemId)
    {
        $item = Contract_item::where('id', '=', $contractItemId)->firstOrFail();
        return $item->delete();
    }

    /**
     * delete contract data
     * @param $id
     * @return mixed
     */
    public function deleteContract($id)
    {
        $item = Contract::where('id', '=', $id)->firstOrFail();
        return $item->delete();
    }

    /**
     * store file record in contract file
     * @param $id
     * @param $fileId
     * @param $fileCost
     * @return static
     */
    public function storeFile($id, $fileId, $fileCost)
    {

        $proposalFile = Contract_file::firstOrCreate([
            'contr_id' => $id,
            'file_id' => $fileId,
            'file_cost' => !empty($fileCost) ? CustomHelper::amountToDouble($fileCost) : 0
        ]);
        return $proposalFile;
    }

    /**
     * update contract file
     * @param $id
     * @param $data
     */
    public function updateFile($id,$data)
    {
        $file = Contract_file::where('id','=',$id)->firstOrFail();

        foreach ($data as $key => $value){
            foreach ($this->contractFileParams as $param){
                if($key == $param){
                    $file->$key = $value;
                }
            }
        }
        $file->save();
    }

    /**
     * get contract files by project
     * @param $projectId
     * @param $id
     * @return mixed
     */
    public function getContractFiles($projectId, $id)
    {
        $fileType = File_type::where('name', '=', 'Contracts')->firstOrFail();
        $files = File::join('contract_files', 'contract_files.file_id', '=', 'files.id')
            ->where('contr_id','=', $id)
            ->where('ft_id', '=', $fileType->id)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->select('contract_files.*', 'files.name', 'files.number', 'files.file_name', 'files.id as f_id')
            ->get();
        return $files;
    }

    public function getSharedContract($projectID, $contractID)
    {
        $companyIDs = $this->projectPermissionsRepository->getAllowedParentCompanyIDsByModule($projectID, $this->contractModule->id, Auth::user()->comp_id);

        $contracts = Contract::with('items')
            ->whereIn('contracts.comp_id', $companyIDs)
            ->join('address_book','address_book.id','=','contracts.ab_id')
            ->select('contracts.*',
                'address_book.name as company_name')
            ->where('contracts.id','=',$contractID)
            ->where('proj_id','=',$projectID)
            ->first();
        return $contracts;
    }

    /**
     * get contract files by project
     * @param $projectId
     * @param $contractId
     * @return mixed
     * @internal param $id
     */
    public function getSharedContractFiles($projectId, $contractId)
    {
        $fileType = File_type::where('name', '=', 'Contracts')->firstOrFail();
        $files = File::join('contract_files', 'contract_files.file_id', '=', 'files.id')
            ->where('contr_id','=', $contractId)
            ->where('ft_id', '=', $fileType->id)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '!=', Auth::user()->comp_id)
            ->select('contract_files.*', 'files.name', 'files.number', 'files.file_name', 'files.id as f_id', 'files.comp_id')
            ->get();
        return $files;
    }

    /**
     * get contract file by id
     * @param $projectId
     * @param $fileId
     * @return mixed
     */
    public function getContractFile($projectId, $fileId)
    {
        $fileType = File_type::where('name', '=', 'Contracts')->firstOrFail();

        $file = File::join('contract_files','contract_files.file_id', '=', 'files.id')
            ->where('files.id', '=', $fileId)
            ->where('ft_id', '=', $fileType->id)
            ->where('proj_id', '=', $projectId)
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->select('contract_files.*', 'files.name', 'files.number', 'files.file_name', 'files.id as f_id')
            ->firstOrFail();
        return $file;
    }

    /**
     * delete contract file record
     * @param $id
     * @return mixed
     */
    public function deleteFile($id)
    {
        $file = Contract_file::where('id', '=', $id)->firstOrFail();
        return $file->delete();
    }

    /**
     * delete contract file record
     * @param $id
     * @return mixed
     */
    public function getFile($id)
    {
        $file = Contract_file::where('id', '=', $id)->firstOrFail();
        return $file;
    }

    /**
     * Delete single contract file
     * delete from files and contract_files tables
     * @param $params
     * @return bool
     */
    public function deleteSingleContractFile($params)
    {
        //get files from contract_files and files tables
        $file = Project_file::where('id','=',$params['fileId'])
            ->where('comp_id','=',Auth::user()->comp_id)
            ->first();

        $contractFile = Contract_file::where('file_id','=',$params['fileId'])
            ->first();

        //delete if exists
        if (!is_null($contractFile)) {
            $contractFileDelete = $contractFile->delete();
        }

        if (!is_null($file)) {
            $fileDelete = $file->delete();
        }

        //if file exists in tables and is successfully deleted, return true
        if ((isset($contractFileDelete) && !empty($contractFileDelete)) && (isset($fileDelete) && !empty($fileDelete))) {

            //if storage limit bellow 90% reset notification
            $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
            $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
            $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
            $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
            $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

            if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
                $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
            }

            return true;
        }
        return false;
    }

    /**
     * update contract file record
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateContractFile($id,$data)
    {
        $file = File::where('id', '=', $id)->firstOrFail();

        foreach ($data as $key => $value){
            foreach ($this->fileParams as $fileParam){
                if($key == $fileParam){
                    $file->$key = $value;
                }
            }
        }
        return $file->save();
    }

    public function updateContractFileCost($contractId, $fileId, $fileCost)
    {
        $contractFile = Contract_file::where('contr_id','=',$contractId)
            ->where('file_id','=',$fileId)
            ->firstOrFail();

        $contractFile->file_cost = !empty($fileCost) ? CustomHelper::amountToDouble($fileCost) : 0;
        $contractFile->save();
        return $contractFile;
    }

}