<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/13/2015
 * Time: 2:54 PM
 */

namespace App\Modules\Transmittals\Interfaces;


interface TransmittalsDatabaseInterface {

    public function databaseValues($fileType, $versionId, $projectId, $transmittalDate);

}