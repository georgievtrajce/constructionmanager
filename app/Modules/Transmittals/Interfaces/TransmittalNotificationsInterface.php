<?php
namespace App\Modules\Transmittals\Interfaces;


interface TransmittalNotificationsInterface
{

    public function getCompanyEmails($data);

    public function getUserEmails($data);

    public function markSent($data);

}