<?php
namespace App\Modules\Transmittals\Interfaces;

interface TransmittalsInterface {

    public function generate($view, $data, $params);

}