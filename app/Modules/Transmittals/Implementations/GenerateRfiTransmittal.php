<?php
namespace App\Modules\Transmittals\Implementations;


use App\Models\Project_file;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Transmittals\Interfaces\TransmittalsInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class GenerateRfiTransmittal implements TransmittalsInterface {

    private $limit;

    public function __construct()
    {
        $this->limit = new DataTransferLimitation();
    }

    /**
     * Generate rfis version transmittal
     * @param $view
     * @param $data
     * @param $params
     * @return mixed
     */
    public function generate($view, $data, $params)
    {
        $data['dateType'] = $params['transmittalDate'];

        $prepareName = str_replace('-', '_', str_replace(' ', '_', $params['name'])); // Replaces all spaces with hyphens.
        $cleanName = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareName); // Removes special chars.

        //file name
        $fileName = $cleanName.'_'.$params['versionId'].'_transmittal_'.$data['dateType'].'.pdf';

        //upload file locally
        $savePdf = App::make('dompdf.wrapper');
        $savedFile = $savePdf->loadView($view, $data)->save(storage_path().'/app/'.$fileName);

        if ($savedFile) {
            //get generated file size
            $size = Storage::size($fileName);

            //check upload limit allowance
            if ($this->limit->checkUploadTransferAllowance($size)) {
                //upload transmittal on s3
                $contents = Storage::disk('local')->get($fileName);
                $s3 = Storage::disk('s3');
                $fileUrl = 'company_'.Auth::user()->company->id.'/project_'.$params['projectId'].'/rfis/transmittals/'.$fileName;
                $s3Response = $s3->put($fileUrl, $contents);

                if ($s3Response) {
                    //update uploaded file size
                    Project_file::where('id','=',$params['fileId'])->update([
                        'size' => $size
                    ]);

                    //increase upload data transfer limitation
                    $this->limit->increaseUploadTransfer($size);

                    //delete file locally if it exists
                    if (Storage::disk('local')->exists($fileName)) {
                        Storage::disk('local')->delete($fileName);
                    }

                    return Response::json(array('generate' => 1, 'message' => 'Successful', 'fileId' => $params['fileId'], 'url' => $fileUrl));
                }
            } else {
                return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.upload_error', ['item' => 'Transmittal'])));
            }

            //delete file locally if it exists
            if (Storage::disk('local')->exists($fileName)) {
                Storage::disk('local')->delete($fileName);
            }

            return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.download_error', ['item' => 'Transmittal'])));
        }

        return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
    }

}