<?php
namespace App\Modules\Transmittals\Implementations;


use App\Models\File_type;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Transmittals\Interfaces\TransmittalsDatabaseInterface;
use Illuminate\Support\Facades\Config;

class DatabaseRfiTransmittal implements TransmittalsDatabaseInterface {

    private $rfiRepository;
    private $filesRepository;

    public function __construct()
    {
        $this->rfiRepository = new RfisRepository();
        $this->filesRepository = new ProjectFilesRepository();
    }

    public function databaseValues($fileType, $versionId, $projectId, $transmittalDate)
    {
        //get rfi version with the proper rfi
        $response['version'] = $this->rfiRepository->getRfiVersionForTransmittal($versionId);
        $response['name'] = $response['version']->rfi->name;

        //Set report type
        $response['type'] = Config::get('constants.transmittal_type.rfi');

        //Set report view
        $response['view'] = Config::get('constants.transmittal_view.rfi');

        $prepareName = str_replace('-', '_', str_replace(' ', '_', $response['name'])); // Replaces all spaces with hyphens.
        $cleanName = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareName); // Removes special chars.

        //file name
        $response['fileName'] = $cleanName.'_'.$versionId.'_transmittal_'.$transmittalDate.'.pdf';

        //get file type
        $response['fileType'] = File_type::where('display_name', '=', $fileType)->firstOrFail();

        //delete old transmittals
        $this->filesRepository->deleteOldTransmittals($versionId, $transmittalDate, $response['fileType']->id);

        //save file in database
        $response['file'] = $this->filesRepository->storeTransmittalFile($projectId, $response['fileName'], $response['fileType']->id);

        return $response;
    }

}