<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/13/2015
 * Time: 3:00 PM
 */

namespace App\Modules\Transmittals\Implementations;


use App\Models\File_type;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Transmittals\Interfaces\TransmittalsDatabaseInterface;
use Illuminate\Support\Facades\Config;

class DatabaseSubmittalTransmittal implements TransmittalsDatabaseInterface {

    private $submittalsRepository;
    private $filesRepository;

    public function __construct()
    {
        $this->submittalsRepository = new SubmittalsRepository();
        $this->filesRepository = new ProjectFilesRepository();
    }


    /**
     * Generate transmittal - database manipulation
     * @param $fileType
     * @param $versionId
     * @param $projectId
     * @param $transmittalDate
     * @return mixed
     */
    public function databaseValues($fileType, $versionId, $projectId, $transmittalDate)
    {
        //get submittal version with the proper submittal
        $response['version'] = $this->submittalsRepository->getSubmittalVersionForTransmittal($versionId);
        $response['name'] = $response['version']->submittal->name;

        //Set report type
        $response['type'] = Config::get('constants.transmittal_type.submittal');

        //Set report view
        $response['view'] = Config::get('constants.transmittal_view.submittal');

        $prepareName = str_replace('-', '_', str_replace(' ', '_', $response['name'])); // Replaces all spaces with hyphens.
        $cleanName = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareName); // Removes special chars.

        //file name
        $response['fileName'] = $cleanName.'_'.$versionId.'_transmittal_'.$transmittalDate.'.pdf';

        //get file type
        $response['fileType'] = File_type::where('display_name', '=', $fileType)->firstOrFail();

        //delete old transmittals
        $this->filesRepository->deleteOldTransmittals($versionId, $transmittalDate, $response['fileType']->id);

        //save file in database
        $response['file'] = $this->filesRepository->storeTransmittalFile($projectId, $response['fileName'], $response['fileType']->id);

        return $response;
    }

}