<?php
namespace App\Modules\Transmittals\Implementations;


use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Transmittals\Interfaces\TransmittalNotificationsInterface;
use Illuminate\Support\Facades\Config;

class RfiNotificationsTransmittal implements TransmittalNotificationsInterface
{

    private $repo;
    private $response;

    public function __construct()
    {
        $this->repo = new RfisRepository();
        $this->response = null;
    }

    /**
     * Get company emails
     * @param $data
     * @return mixed|null
     */
    public function getCompanyEmails($data)
    {
       return $this->repo->getRfiCompanyEmails($data['versionId'], $data['contactIds']);
    }

    /**
     * Get user emails
     * @param $data
     * @return mixed|null
     */
    public function getUserEmails($data)
    {
        return $this->repo->getCompanyUserEmails($data['versionId'], $data['userIds']);
    }

    /**
     * Get user emails
     * @param $data
     * @return mixed|null
     */
    public function markSent($data)
    {
        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.subcontractor')) {
            $this->repo->markTransmittalSubrSentTo($data['versionId'],$data);
        }

        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.recipient')) {
            $this->repo->markTransmittalApprSentTo($data['versionId'],$data);
        }

        return $this->response;
    }

}