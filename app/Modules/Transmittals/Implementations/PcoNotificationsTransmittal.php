<?php
namespace App\Modules\Transmittals\Implementations;


use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Transmittals\Interfaces\TransmittalNotificationsInterface;
use Illuminate\Support\Facades\Config;

class PcoNotificationsTransmittal implements TransmittalNotificationsInterface
{

    private $repo;
    private $response;

    public function __construct()
    {
        $this->repo = new PcosRepository();
        $this->response = null;
    }

    /**
     * Get company emails
     * @param $data
     * @return null
     */
    public function getCompanyEmails($data)
    {
        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.subcontractor')) {
            $this->response = $this->repo->getPcoSubcontractorCompanyEmails($data['versionId'], $data['contactIds']);
        }

        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.recipient')) {
            $this->response = $this->repo->getPcoRecipientCompanyEmails($data['versionId'], $data['contactIds']);
        }

        return $this->response;
    }

    /**
     * Get user emails
     * @param $data
     * @return mixed|null
     */
    public function getUserEmails($data)
    {
        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.subcontractor')) {
            $this->response = $this->repo->getCompanyUserSubcontractorEmails($data['versionId'], $data['userIds']);
        }

        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.recipient')) {
            $this->response = $this->repo->getCompanyUserRecipientEmails($data['versionId'], $data['userIds']);
        }

        return $this->response;
    }

    /**
     * Get user emails
     * @param $data
     * @return mixed|null
     */
    public function markSent($data)
    {
        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.subcontractor')) {
            $this->repo->markTransmittalSubrSentTo($data['versionId'],$data);
        }

        if (isset($data['subRec']) && $data['subRec'] == Config::get('constants.ab_company_type.recipient')) {
            $this->repo->markTransmittalApprSentTo($data['versionId'],$data);
        }

        return $this->response;
    }

}