<?php
namespace App\Modules\Transmittals\Implementations;

use App\Modules\Transmittals\Interfaces\TransmittalNotificationsInterface;
use App\Modules\Transmittals\Interfaces\TransmittalsDatabaseInterface;
use App\Modules\Transmittals\Interfaces\TransmittalsInterface;

class TransmittalsWrapper {

    /**
     * Create database store, delete old transmittals and return needed data
     * @param TransmittalsDatabaseInterface $transmittalsDatabaseInterface
     * @param $fileType
     * @param $versionId
     * @param $projectId
     * @param $transmittalDate
     * @return mixed
     */
    public function wrapDatabaseData(TransmittalsDatabaseInterface $transmittalsDatabaseInterface, $fileType, $versionId, $projectId, $transmittalDate)
    {
        return $transmittalsDatabaseInterface->databaseValues($fileType, $versionId, $projectId, $transmittalDate);
    }

    /**
     * Wrap the different transmittal type generator
     * The transmittals could of type submittal, rfi or pco
     * @param $view
     * @param $data
     * @param $params
     * @param TransmittalsInterface $transmittalsInterface
     * @return mixed
     */
    public function wrap(TransmittalsInterface $transmittalsInterface, $view, $data, $params)
    {
        return $transmittalsInterface->generate($view, $data, $params);
    }

    /**
     * Mapper for company emails for subcontractors and recipients imported from address book
     * into Submittals, RFI's and PCO's modules
     * @param TransmittalNotificationsInterface $notificationsInterface
     * @param $data
     * @return mixed
     */
    public function wrapCompanyEmails(TransmittalNotificationsInterface $notificationsInterface, $data)
    {
        return $notificationsInterface->getCompanyEmails($data);
    }

    /**
     * Mapper for user emails for subcontractors and recipients imported from address book
     * into Submittals, RFI's and PCO's modules
     * @param TransmittalNotificationsInterface $notificationsInterface
     * @param $data
     * @return mixed
     */
    public function wrapUserEmails(TransmittalNotificationsInterface $notificationsInterface, $data)
    {
        return $notificationsInterface->getUserEmails($data);
    }

    public function wrapMarkSent(TransmittalNotificationsInterface $notificationsInterface, $data)
    {
        return $notificationsInterface->markSent($data);
    }
}