<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/16/2015
 * Time: 11:23 AM
 */

namespace App\Modules\Transmittals\Implementations;


use App\Models\File_type;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Transmittals\Interfaces\TransmittalsDatabaseInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DatabasePcoTransmittal implements TransmittalsDatabaseInterface {

    private $pcoRepository;
    private $filesRepository;

    public function __construct()
    {
        $this->pcoRepository = new PcosRepository();
        $this->filesRepository = new ProjectFilesRepository();
    }

    public function databaseValues($fileType, $versionId, $projectId, $transmittalDate)
    {
        //get pco version with the proper pco
        $response['version'] = $this->pcoRepository->getPcoVersionForTransmittal($versionId);

        //pco name and subcontractor or recipient name and id
        if ($transmittalDate == Config::get('constants.transmittal_date_type.sent_appr')) {
            $response['name'] = $response['version']->recipient->pco->name;
            $response['subocntractorRecipientName'] = $response['version']->recipient->ab_recipient->name;
            $response['subocntractorRecipientId'] = $response['version']->recipient->ab_recipient->id;

            //Set report view
            $response['view'] = Config::get('constants.transmittal_view.pco_recipient');
        } else {
            $response['name'] = $response['version']->subcontractor->pco->name;
            $response['subocntractorRecipientName'] = $response['version']->subcontractor->self_performed ? Auth::user()->company->name : $response['version']->subcontractor->ab_subcontractor->name;
            $response['subocntractorRecipientId'] = $response['version']->subcontractor->self_performed ? 'self_performed' : $response['version']->subcontractor->ab_subcontractor->id;

            //Set report view
            $response['view'] = Config::get('constants.transmittal_view.pco_subcontractor');
        }

        //Set report type
        $response['type'] = Config::get('constants.transmittal_type.pco');

        $prepareName = str_replace('-', '_', str_replace(' ', '_', $response['name'])); // Replaces all spaces with hyphens.
        $cleanName = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareName); // Removes special chars.

        $prepareSubRec = str_replace('-', '_', str_replace(' ', '_', $response['subocntractorRecipientName'])); // Replaces all spaces with hyphens.
        $cleanSubRec = preg_replace('/[^A-Za-z0-9\-_]/', '', $prepareSubRec); // Removes special chars.

        //file name
        $response['fileName'] = $cleanName.'_'.$versionId.'_'.$cleanSubRec.'_'.$response['subocntractorRecipientId'].'_transmittal_'.$transmittalDate.'.pdf';

        //get file type
        $response['fileType'] = File_type::where('display_name', '=', $fileType)->firstOrFail();

        //delete old transmittals
        $this->filesRepository->deleteOldTransmittals($versionId, $transmittalDate, $response['fileType']->id);

        //save file in database
        $response['file'] = $this->filesRepository->storeTransmittalFile($projectId, $response['fileName'], $response['fileType']->id);

        return $response;
    }

}