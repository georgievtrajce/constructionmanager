<?php
namespace App\Modules\Transmittals\Repositories;

use App\Models\Transmittal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class TransmittalsRepository {

    private $companyId;
    private $userId;

    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
        $this->userId = Auth::user()->id;
    }

    /**
     * Store Transmittal log entry
     * @param $projectId
     * @param $fileId
     * @param $fileTypeId
     * @param $versionId
     * @param $date
     * @return static
     */
    public function storeTransmittalLog($projectId, $fileId, $fileTypeId, $versionId, $date)
    {
        return Transmittal::create([
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'file_id' => $fileId,
            'file_type_id' => $fileTypeId,
            'proj_id' => $projectId,
            'version_id' => $versionId,
            'date_type' => $date
        ]);
    }

    /**
     * Get module's "Sent for Approval" transmittal file
     * @param $fileType
     * @param $projectId
     * @param $versionId
     * @param $dateType
     * @return mixed
     */
    public function getTransmittal($fileType, $projectId, $versionId, $dateType)
    {
        return Transmittal::where('comp_id','=',$this->companyId)
            ->join('file_types', function($join) use ($fileType) {
                $join->on('file_types.id','=','transmittals_logs.file_type_id')
                    ->where('file_types.display_name','=',$fileType);
            })
            ->select([
                'transmittals_logs.*'
            ])
            //->where('file_type_id','=',$fileType)
            ->where('proj_id','=',$projectId)
            ->where('version_id','=',$versionId)
            ->where('date_type','=',$dateType)
            ->with('file')
            ->first();
    }

    /**
     * Check if transmittal already exist
     * @param $fileType
     * @param $versionId
     * @param $projectId
     * @param $transmittalDate
     * @return bool
     */
    public function checkExisting($fileType, $versionId, $projectId, $transmittalDate)
    {
        $transmittal = Transmittal::where('comp_id','=',$this->companyId)
            ->join('file_types', function($join) use ($fileType) {
                $join->on('file_types.id','=','transmittals_logs.file_type_id')
                    ->where('file_types.display_name','=',$fileType);
            })
            ->where('proj_id','=',$projectId)
            ->where('version_id','=',$versionId)
            ->where('date_type','=',$transmittalDate)
            ->first();

        return is_null($transmittal) ? false : true;
    }

    /**
     * Update emailed flag field in transmittal logs
     * @param $id
     * @param $emailed
     * @return mixed
     */
    public function updatedEmailedTransmittal($id, $emailed)
    {
        return Transmittal::where('id','=',$id)
            ->where('comp_id','=',$this->companyId)
            ->update([
                'emailed' => $emailed
            ]);
    }

}