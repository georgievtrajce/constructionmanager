<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/9/2015
 * Time: 3:15 PM
 */
namespace App\Modules\List_companies_users\Repositories;

use App\Models\Company;
use App\Models\Download_log;
use App\Models\User;
use Carbon\Carbon;
use Config;

class ListCompaniesUsersRepository {

    /**
     * Get all registered companies in the system
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllRegisteredCompanies($sort, $order)
    {
        return Company::orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.super_admin_companies'));
    }

    /**
     * Filter companies by specified params
     * @param $status
     * @param $accountType
     * @param $subscriptionLevel
     * @param $subscriptionLevels
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function filterCompanies($status, $accountType, $subscriptionLevel, $subscriptionLevels, $sort, $order)
    {
        $companies = Company::orderBy($sort,$order);

        //filter by status
        if (!empty($status)) {
            if ($status == 'active') {
                $companies->where('subscription_expire_date','>',Carbon::now()->toDateString());
            } else {
                $companies->where('subscription_expire_date','<=',Carbon::now()->toDateString());
            }
        }

        //filter by account type
        if (!empty($accountType)) {
            if ($accountType == 'free') {
                $companies->whereNull('subscription_payment_date');
            } else {
                $companies->whereNotNull('subscription_payment_date');
            }
        }

        //filter by subscription level
        if (!empty($subscriptionLevel)) {
            foreach ($subscriptionLevels as $subscription) {
                if ($subscription->id == $subscriptionLevel) {
                    $companies->where('subs_id','=',$subscriptionLevel);
                }
            }
        }

        //return filtered data
        return $companies->paginate(Config::get('constants.pagination.super_admin_companies'));
    }

    /**
     * Get registered company specified by id
     * @param $companyId
     * @return mixed
     */
    public function getCompany($companyId)
    {
        return Company::where('id','=',$companyId)
            ->with('users.roles')
            ->with('addresses')
            ->with('subscription_type')
            ->firstOrFail();
    }

    /**
     * Get registered company user specified by id
     * @param $userId
     * @return mixed
     */
    public function getCompanyUser($userId)
    {
        return User::where('id','=',$userId)
            ->with('address')
            ->with('company')
            ->firstOrFail();
    }

    /**
     * Get All Logs
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getAllLogs($sort, $order)
    {
        $logs = Download_log::join('users','users.id','=','download_logs.user_id')
            ->join('companies','companies.id','=','download_logs.comp_id')
            ->join('files','files.id','=','download_logs.file_id')
            ->join('file_types', 'file_types.id', '=', 'files.ft_id')
            ->select('users.name as user_name', 'companies.name as company_name', 'file_types.name as file_type', 'files.name as file_name', 'download_logs.*')
            ->orderBy($sort, $order)
            ->paginate(Config::get('constants.pagination.logs'));
        return $logs;
    }

    /**
     * Reset specific company refferal points (for sales agent)
     * @param $companyId
     * @return mixed
     */
    public function resetCompanyReferralPoints($companyId)
    {
        return Company::where('id','=',$companyId)->update([
            'ref_points' => 0
        ]);
    }

    /**
     * Update specific company expire date
     * @param $companyId
     * @param $expireDate
     * @return null
     */
    public function updateCompanyExpireDate($companyId, $expireDate)
    {
        //if date is empty return null
        if (empty($expireDate)) {
           return null;
        }

        //update expire date
        $company = Company::where('id','=',$companyId)->firstOrFail();
        $company->subscription_expire_date =  Carbon::parse($expireDate)->format('Y-m-d');
        $company->save();
        return $company;
    }

    /**
     * Update specific company subscription type
     * @param $companyId
     * @param $subscriptionType
     * @return null
     */
    public function updateCompanySubscriptionType($companyId, $subscriptionType)
    {
        //if date is empty return null
        if (empty($subscriptionType)) {
            return null;
        }

        //update expire date
        $company = Company::where('id','=',$companyId)->firstOrFail();
        $company->subs_id =  $subscriptionType;

        if ($subscriptionType == '5') {
            $company->free_account = 1;
        } else {
            $company->free_account = 0;
        }

        $company->save();
        return $company;
    }
}