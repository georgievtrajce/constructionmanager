<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/28/2015
 * Time: 4:43 PM
 */
namespace App\Modules\Submittals\Repositories;

use App\Models\Ab_contact;
use App\Models\File_type;
use App\Models\Module;
use App\Models\Project;
use App\Models\Project_company_permission;
use App\Models\Project_file;
use App\Models\Project_subcontractor;
use App\Models\Proposal_supplier_permission;
use App\Models\Submittal;
use App\Models\Submittal_version;
use App\Models\Submittal_status;
use App\Models\Submittal_permission;
use App\Models\Submittal_version_distribution;
use App\Models\Submittal_version_file;
use App\Models\SubmittalsSubcontractorProposalDistribution;
use App\Models\Transmittal;
use App\Models\User;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Services\CustomHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class SubmittalsRepository {

    private $companyId;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }

        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $this->submittalModule = File_type::where('display_name', '=', 'submittals')->first();

        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * Get all project submittals
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getProjectSubmittals($projectId, $sort, $order, $paginate = 50)
    {
        return Submittal::where('submittals.proj_id', '=', $projectId)
            ->where('submittals.comp_id', '=', $this->companyId)
            ->join('submittals_versions','submittals_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=',
                    DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                    WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                    ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('submittal_statuses', 'status_id', '=', 'submittal_statuses.id')
            ->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status_id',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('submittals_versions')
            ->with('submittals_versions.file')
            ->with('submittals_versions.transmittalSentFile')
            ->with('submittals_versions.transmittalSubmSentFile')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }


    /**
     * Returns submittals for project
     *
     * @param $projectId
     * @return mixed
     */
    public function getAllSubmittalsWithFilesForProject($projectId)
    {
        return Submittal::where('submittals.proj_id', '=', $projectId)
            ->select('submittals.*')
            ->with('submittals_versions.file')
            ->get();
    }

    /**
     * Get all submittals by project id
     * @param $projectId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllSubmittalsForProject($projectId, $compId)
    {
        return Submittal::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $compId)
            ->join('submittals_versions','submittals_versions.id','=','last_version')
            ->join('submittal_statuses', 'submittals_versions.status_id', '=', 'submittal_statuses.id')
            ->with('submittals_versions.file')
            ->with('submittals_versions.smallFile')
            ->with('submittals_versions.transmittalSubmSentFile')
            ->with('submittals_versions.transmittalSentFile')
            ->select(['submittals.*', 'submittal_statuses.short_name as status_short_name'])
            ->get();
    }

    /**
     * Get all shared project submittals
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return bool
     */
    public function getSharedProjectSubmittals($projectId, $sort, $order, $paginate = 50)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($projectId, $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($projectId, $this->companyId);
        $project = Project::where('id', '=', $projectId)->first();

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $submittalType = Module::where('display_name','=','submittals')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //check companies permissions
        $submittalsPermissions = Project_company_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        if (!is_null($submittalsPermissions) || !is_null($bidsPermissions)) {
            $sharedSubmittals = Submittal::where('submittals.proj_id','=',$projectId)
                ->where('submittals.comp_id','=',$project->comp_id);

            if ((!is_null($submittalsPermissions) && $submittalsPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
                $sharedSubmittals->join('submittals_versions','submittals_versions.id','=','last_version')
                    ->leftJoin('files', function($leftJoin)
                    {
                        $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                        WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                        ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
                    ->leftJoin('submittal_statuses', 'status_id', '=', 'submittal_statuses.id');
            } else {
                $sharedSubmittals->rightJoin('submittal_permissions','submittals.id','=','submittal_permissions.subm_id')
                    ->join('submittals_versions','submittals_versions.id','=','last_version')
                    ->leftJoin('files', function($leftJoin)
                    {
                        $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                        WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                        ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
                    ->leftJoin('submittal_statuses', 'status_id', '=', 'submittal_statuses.id')
                    ->where('submittal_permissions.comp_child_id', '=', $this->companyId);
            }

            return $sharedSubmittals->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status_id',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
                ->with('submittals_versions')
                ->with('submittals_versions.file')
                ->with('submittals_versions.transmittalSentFile')
                ->with('submittals_versions.transmittalSubmSentFile')
                ->orderBy($sort, $order)
                ->paginate($paginate);
        }

        return null;
    }

    /**
     * Get all created submittals shares with different companies
     * @param $projectId
     * @param int $paginate
     * @param int $companyType
     * @param int $projectCreatorId
     * @return mixed
     */
    public function getSharedSubmittals($projectId, $companyType, $projectCreatorId, $paginate = 50)
    {
        //if the logged in company is a subcontractor on the specified project
        if ($companyType == Config::get('constants.company_type.subcontractor')) {
            //get the parent company that has shared this project to the logged in company
            $parentCompany = CustomHelper::getCompanyParentForProject($projectId);

            //check if the parent company has shared any submittals with the logged in company
            if (is_null($parentCompany)) {
                $parentId = 0;
            } else {
                $parentId = $parentCompany->comp_parent_id;
            }

            $creatorCompanyId = $projectCreatorId;
        } else {
            $parentId = $this->companyId;
            $creatorCompanyId = $this->companyId;
        }

        return Submittal::where('submittals.proj_id','=',$projectId)
            ->where('submittals.comp_id','=',$creatorCompanyId)
            ->rightJoin('submittal_permissions','submittal_permissions.subm_id','=','submittals.id')
            ->where('submittal_permissions.comp_parent_id','=',$parentId)
            ->join('companies','submittal_permissions.comp_child_id','=','companies.id')
            ->join('submittals_versions','submittals_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('submittal_statuses', 'status_id', '=', 'submittal_statuses.id')
            ->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status_id',
                'submittal_permissions.comp_parent_id as comp_parent_id',
                'submittal_permissions.comp_child_id as comp_child_id',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection',
                'companies.name as company_name')
            ->with('submittals_versions')
            ->with('submittals_versions.file')
            ->orderBy('submittals.id','DESC')
            ->paginate($paginate);
    }

    /**
     * Get received submittals
     * @param $projectId
     * @param $creatorCompanyId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getReceivedSubmittals($projectId, $creatorCompanyId, $sort, $order, $paginate = 50)
    {
        return Submittal::where('submittals.proj_id','=',$projectId)
            ->where('submittals.comp_id','!=',$creatorCompanyId)
            ->where('submittals.comp_id','!=',$this->companyId)
            ->rightJoin('submittal_permissions','submittal_permissions.subm_id','=','submittals.id')
            //->where('submittal_permissions.comp_parent_id','=',$parentId)
            ->join('companies','submittal_permissions.comp_parent_id','=','companies.id')
            ->join('submittals_versions','submittals_versions.id','=','last_version')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'sub_id', '=', 'address_book.id')
            ->leftJoin('submittal_statuses', 'status_id', '=', 'submittal_statuses.id')
            ->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status_id',
                'submittal_permissions.comp_parent_id as comp_parent_id',
                'submittal_permissions.comp_child_id as comp_child_id',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.size as size',
                'files.version_date_connection as version_date_connection',
                'submittal_permissions.comp_parent_id as comp_parent_id',
                'companies.name as company_name')
            ->with('submittals_versions')
            ->with('submittals_versions.file')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Get all transmittals
     * @param $projectId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getTransmittals($projectId, $sort, $order)
    {
        $fileType = File_type::where('display_name','=','submittals')->firstOrFail();

        return Transmittal::where('file_type_id','=',$fileType->id)
            ->where('transmittals_logs.proj_id','=',$projectId)
            ->join('submittals_versions','submittals_versions.id','=','transmittals_logs.version_id')
            ->leftJoin('submittal_statuses','submittal_statuses.id','=','submittals_versions.status_id')
            ->join('submittals','submittals.id','=','submittals_versions.submittal_id')
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", submittals.sub_id, submittals.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as sent_appr',
                'submittals_versions.subm_sent_sub as subm_sent_sub',
                'submittals.subject as subject',
                'submittals.number as number',
                'submittals.mf_number as mf_number',
                'submittals.mf_title as mf_title',
                'address_book.name as sent_to',
                'submittal_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN submittals_versions.subm_sent_sub ELSE submittals_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Get Subcontractor emails selected in the companies module
     * needed for transmittal email notifications
     * @param $id
     * @param $projectId
     * @return mixed
     */
    public function getSubmittalCompanyEmails($versionId, $contactIds)
    {
        return Submittal_version::join('submittals', 'submittals.id', '=', 'submittals_versions.submittal_id')
                                ->join('projects', 'projects.id', '=', 'submittals.proj_id')
                                ->join('project_company_contacts', 'project_company_contacts.proj_id', '=', 'projects.id')
                                ->leftJoin('project_subcontractors', function($join)
                                {
                                    $join->on('project_company_contacts.proj_comp_id', '=', 'project_subcontractors.proj_comp_id')
                                        ->where('project_subcontractors.once_accepted','=',Config::get('constants.projects.once_accepted.yes'))
                                        ->where('project_subcontractors.status','=',Config::get('constants.projects.shared_status.accepted'));
                                })
                                ->join('ab_contacts', 'project_company_contacts.ab_cont_id', '=', 'ab_contacts.id')
                                ->leftJoin('address_book', 'ab_contacts.ab_id', '=', 'address_book.id')
                                ->where('submittals_versions.id', '=', $versionId)
                                ->whereIn('ab_contacts.id', $contactIds)
                                ->select(
                                    'ab_contacts.*',
                                    'submittals.name as submittal_name',
                                    'project_subcontractors.comp_child_id as comp_child_id',
                                    'projects.name as project_name',
                                    'projects.id as project_id',
                                    'address_book.name as company_name'
                                )
                                ->groupBy(['ab_contacts.id'])
                                ->get();
    }

    /**
     * Get User emails from my company
     * @param $versionId
     * @return mixed
     */
    public function getCompanyUserEmails($versionId, $userIds)
    {
        return User::whereIn('id', $userIds)
                    ->selectRaw('users.email, users.name, 
                    (select submittals.name from submittals, submittals_versions where submittals.id = submittals_versions.submittal_id 
                        and submittals_versions.id = '.$versionId.') as submittal_name, 
                    (select projects.name from submittals, submittals_versions, projects where submittals.id = submittals_versions.submittal_id 
                        and projects.id = submittals.proj_id and submittals_versions.id = '.$versionId.') as project_name, 
                    (select projects.id from submittals, submittals_versions, projects where submittals.id = submittals_versions.submittal_id 
                        and projects.id = submittals.proj_id and submittals_versions.id = '.$versionId.') as project_id')
                    ->where('comp_id', '=', Auth::user()->comp_id)
                    ->get();
    }

    /**
     * Filter Submittal Transmittals by given parameters
     * @param $params
     * @return mixed
     */
    public function filterSubmittalTransmittals($params)
    {
        $fileType = File_type::where('display_name','=','submittals')->firstOrFail();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'file_type_id' => $fileType->id,
            'transmittals_logs.proj_id' => $params['projectId'],
            'transmittals_logs.comp_id' => $this->companyId
        ];

        //set sorting
        if (isset($params['sort'])) {
            if ($params['sort'] != '') {
                $sort = $params['sort'];
            } else {
                $sort = 'created_at';
            }
        } else {
            $sort = 'created_at';
        }

        //set sorting order
        if (isset($params['order'])) {
            if ($params['order'] != '') {
                $order = $params['order'];
            } else {
                $order = 'asc';
            }
        } else {
            $order = 'asc';
        }

        if($params['sentTo'] != '') {
            $query['address_book.id'] = $params['sentTo'];
        }

        $fullQuery = Transmittal::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
        }

        //Get submittal transmittals by status
        if ($status == 'All') {
            $fullQuery->join('submittals_versions','submittals_versions.id','=','transmittals_logs.version_id');
        } else if ($status == 'all_open') {
            $allOpenStatuses = Submittal_status::whereIn('short_name', Config::get('constants.all_open'))
                ->lists('id');

            if (count($allOpenStatuses)) {
                $fullQuery->rightJoin('submittals_versions', function($join) use ($allOpenStatuses)
                {
                    $join->on(DB::raw("submittals_versions.id = transmittals_logs.version_id and submittals_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                });
            }
        } else {
            $fullQuery->where('submittal_statuses.id','=',$status)
                ->join('submittals_versions','submittals_versions.id','=','transmittals_logs.version_id');
        }

        return $fullQuery->leftJoin('submittal_statuses','submittal_statuses.id','=','submittals_versions.status_id')
            ->join('submittals','submittals.id','=','submittals_versions.submittal_id')
            ->leftJoin('address_book', function($leftJoin) {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", submittals.sub_id, submittals.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as sent_appr',
                'submittals_versions.subm_sent_sub as subm_sent_sub',
                'submittals.number as number',
                'submittals.subject as subject',
                'submittals.mf_number as mf_number',
                'submittals.mf_title as mf_title',
                'address_book.name as sent_to',
                'address_book.id as sent_to_id',
                'submittal_statuses.short_name as version_status',
                'submittal_statuses.id as version_status_id',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN submittals_versions.subm_sent_sub ELSE submittals_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Get all submittal versions statuses from database table
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllSubmittalStatuses()
    {
        return Submittal_status::all();
    }

    /**
     * Get Submittal Transmittals for pdf report
     * @param $params
     * @return mixed
     */
    public function SubmittalTransmittalsReport($params)
    {
        $fileType = File_type::where('display_name','=','submittals')->firstOrFail();

        return Transmittal::where('file_type_id','=',$fileType->id)
            ->where('transmittals_logs.proj_id','=',$params['projectId'])
            ->join('submittals_versions','submittals_versions.id','=','transmittals_logs.version_id')
            ->leftJoin('submittal_statuses','submittal_statuses.id','=','submittals_versions.status_id')
            ->join('submittals','submittals.id','=','submittals_versions.submittal_id')
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'", submittals.sub_id, submittals.recipient_id)'));
            })
            ->select('transmittals_logs.*',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as sent_appr',
                'submittals_versions.subm_sent_sub as subm_sent_sub',
                'submittals.number as number',
                'submittals.subject as subject',
                'submittals.mf_number as mf_number',
                'submittals.mf_title as mf_title',
                'address_book.name as sent_to',
                'submittal_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN submittals_versions.subm_sent_sub ELSE submittals_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Filter submittals
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterSubmittals($params, $paginate = 50)
    {
        $status = $params['status'];
        //Create query conditions
        $query = [
            'submittals.proj_id' => $params['projectId'],
            'submittals.comp_id' => $this->companyId
        ];

        if($params['subcontractor'] != '') {
            $query['submittals.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Submittal::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('submittals.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('submittals.mf_title','LIKE', $params['masterFormat'].'%');
        }

        if (!empty($params['name'])) {
            $fullQuery->where('submittals.name', 'LIKE', $params['name'].'%');
        }

        //Get submittals by status\
        if ($status == 'All') {
            $fullQuery->join('submittals_versions','submittals_versions.id','=','last_version');
        }
        else if ($status == 'all_open') {
            $allOpenStatuses = Submittal_status::whereIn('short_name', Config::get('constants.all_open'))
                ->lists('id');

            if (count($allOpenStatuses)) {
                $fullQuery->rightJoin('submittals_versions', function($join) use ($allOpenStatuses)
                {
                    $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                });
            }
        }
        else {
            $fullQuery->rightJoin('submittals_versions', function($join) use ($status)
            {
                $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
            });
        }

        return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'submittals.sub_id', '=', 'address_book.id')
            ->leftJoin('submittal_statuses', 'submittals_versions.status_id', '=', 'submittal_statuses.id')
            ->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('submittals_versions')
            ->with('submittals_versions.file')
            ->with('submittals_versions.transmittalSentFile')
            ->with('submittals_versions.transmittalSubmSentFile')
            ->orderBy($params['sort'],$params['order'])
            ->paginate($paginate);
    }

    /**
     * Report submittals
     * @param $params
     * @return array
     * @internal param int $perPage
     */
    public function reportSubmittals($params)
    {
        $status = $params['status'];
        //Create query conditions
        $query = [
            'submittals.proj_id' => $params['projectId'],
            'submittals.comp_id' => $this->companyId
        ];

        if($params['subcontractor'] != '') {
            $query['submittals.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Submittal::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('submittals.mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('submittals.mf_title','LIKE', $params['masterFormat'].'%');
        }

        //Get submittals by status\
        if ($status == 'All') {
            $fullQuery->join('submittals_versions','submittals_versions.id','=','last_version');
        }
        else if ($status == 'all_open') {
            $allOpenStatuses = Submittal_status::whereIn('short_name', Config::get('constants.all_open'))
                ->lists('id');

            if (count($allOpenStatuses)) {
                $fullQuery->rightJoin('submittals_versions', function($join) use ($allOpenStatuses)
                {
                    $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                });
            }
        }
        else {
            $fullQuery->rightJoin('submittals_versions', function($join) use ($status)
            {
                $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
            });
        }

        return $fullQuery->leftJoin('files', function($leftJoin)
        {
            $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
        })
            ->leftJoin('address_book', 'submittals.sub_id', '=', 'address_book.id')
            ->leftJoin('submittal_statuses', 'submittals_versions.status_id', '=', 'submittal_statuses.id')
            ->select('submittals.*',
                'submittals_versions.id as version_id',
                'submittals_versions.cycle_no as version_cycle_no',
                'submittals_versions.sent_appr as version_sent_appr',
                'submittals_versions.rec_appr as version_rec_appr',
                'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                'submittals_versions.rec_sub as version_rec_sub',
                'submittals_versions.status_id as version_status',
                'submittal_statuses.short_name as version_status_short_name',
                'submittal_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('submittals_versions')
            ->with('last_version_report')
            ->with('submittals_versions.file')
            ->orderBy($params['sort'],$params['order'])
            ->get();
    }

    /**
     * Filter shared submittals
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterSharedSubmittals($params, $paginate = 50)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'submittals.proj_id' => $params['projectId'],
            'submittals.comp_id' => $project->comp_id
        ];

        if($params['subcontractor'] != '') {
            $query['submittals.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Submittal::where($query);

        //join only shared submittals
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $submittalType = Module::where('display_name','=','submittals')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $submittalsPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        if (!is_null($submittalsPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($submittalsPermissions) && $submittalsPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('submittal_permissions', 'submittals.id', '=', 'submittal_permissions.subm_id')
                    ->where('submittal_permissions.comp_child_id', '=', $this->companyId);
            }

            //master format search field transformation in a proper search segment
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('submittals.mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('submittals.mf_title','LIKE', $params['masterFormat'].'%');
            }

            if (!empty($params['name'])) {
                $fullQuery->where('submittals.name', 'LIKE', $params['name'].'%');
            }

            //Get submittals by status\
            if($status == 'All') {
                $fullQuery->join('submittals_versions','submittals_versions.id','=','last_version');
            } else if ($status == 'all_open') {
                $allOpenStatuses = Submittal_status::whereIn('short_name', Config::get('constants.all_open'))
                    ->lists('id');

                if (count($allOpenStatuses)) {
                    $fullQuery->rightJoin('submittals_versions', function($join) use ($allOpenStatuses)
                    {
                        $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                    });
                }
            } else {
                $fullQuery->rightJoin('submittals_versions', function($join) use ($status)
                {
                    $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
                });
            }

            return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
            })
                ->leftJoin('address_book', 'submittals.sub_id', '=', 'address_book.id')
                ->leftJoin('submittal_statuses', 'submittals_versions.status_id', '=', 'submittal_statuses.id')
                ->select('submittals.*',
                    'submittals_versions.id as version_id',
                    'submittals_versions.cycle_no as version_cycle_no',
                    'submittals_versions.sent_appr as version_sent_appr',
                    'submittals_versions.rec_appr as version_rec_appr',
                    'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                    'submittals_versions.rec_sub as version_rec_sub',
                    'submittals_versions.status_id as version_status',
                    'submittal_statuses.short_name as version_status_short_name',
                    'submittal_statuses.name as version_status_name',
                    'address_book.name as subcontractor_name',
                    'files.file_name as file_name',
                    'files.id as file_id',
                    'files.version_date_connection as version_date_connection')
                ->with('submittals_versions')
                ->with('submittals_versions.file')
                ->paginate($paginate);
        }

        return null;
    }

    /**
     * Create pdf from shared submittals
     * @param $params
     * @return array
     * @internal param int $perPage
     */
    public function reportSharedSubmittals($params)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'submittals.proj_id' => $params['projectId'],
            'submittals.comp_id' => $project->comp_id
        ];

        if($params['subcontractor'] != '') {
            $query['submittals.sub_id'] = $params['subcontractor'];
        }

        $fullQuery = Submittal::where($query);

        //join only shared submittals
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $submittalType = Module::where('display_name','=','submittals')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $submittalsPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$submittalType->id)
            ->first();

        if (!is_null($submittalsPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($submittalsPermissions) && $submittalsPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('submittal_permissions', 'submittals.id', '=', 'submittal_permissions.subm_id')
                    ->where('submittal_permissions.comp_child_id', '=', $this->companyId);
            }

            //master format search field transformation in a proper search segment
            if (is_numeric(substr($params['masterFormat'], 0, 1))) {
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $fullQuery->where('submittals.mf_number','LIKE',$masterFormatPieces[0].'%');
            } else {
                $fullQuery->where('submittals.mf_title','LIKE', $params['masterFormat'].'%');
            }

            //Get submittals by status\
            if($status == 'All') {
                $fullQuery->join('submittals_versions','submittals_versions.id','=','last_version');
            } else if ($status == 'all_open') {
                $allOpenStatuses = Submittal_status::whereIn('short_name', Config::get('constants.all_open'))
                    ->lists('id');

                if (count($allOpenStatuses)) {
                    $fullQuery->rightJoin('submittals_versions', function($join) use ($allOpenStatuses)
                    {
                        $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                    });
                }
            } else {
                $fullQuery->rightJoin('submittals_versions', function($join) use ($status)
                {
                    $join->on(DB::raw("submittals_versions.id = last_version and submittals_versions.status_id = '".$status."'"), DB::raw(''), DB::raw(''));
                });
            }

            return $fullQuery->leftJoin('files', function($leftJoin)
                {
                    $leftJoin->on('files.id', '=', DB::raw('(select submittal_version_files.file_id as file_id from submittal_version_files
                    WHERE submittal_version_files.submittal_version_id = submittals_versions.id
                    ORDER BY submittal_version_files.download_priority DESC LIMIT 1)'));
                })
                ->leftJoin('address_book', 'submittals.sub_id', '=', 'address_book.id')
                ->leftJoin('submittal_statuses', 'submittals_versions.status_id', '=', 'submittal_statuses.id')
                ->select('submittals.*',
                    'submittals_versions.id as version_id',
                    'submittals_versions.cycle_no as version_cycle_no',
                    'submittals_versions.sent_appr as version_sent_appr',
                    'submittals_versions.rec_appr as version_rec_appr',
                    'submittals_versions.subm_sent_sub as version_subm_sent_sub',
                    'submittals_versions.rec_sub as version_rec_sub',
                    'submittals_versions.status_id as version_status',
                    'submittal_statuses.short_name as version_status_short_name',
                    'submittal_statuses.name as version_status_name',
                    'address_book.name as subcontractor_name',
                    'files.file_name as file_name',
                    'files.id as file_id',
                    'files.version_date_connection as version_date_connection')
                ->with('submittals_versions')
                ->with('submittals_versions.file')
                ->orderBy($params['sort'],$params['order'])
                ->get();
        }

        return null;
    }

    /**
     * Get a specific status by its id
     * @param $statusId
     * @return mixed
     */
    public function getStatusById($statusId)
    {
        return Submittal_status::where('id','=',$statusId)->firstOrFail();
    }

    /**
     * Unshare all submittals shares from second level of sharing and further possible shares
     * @param $submittalId
     * @param $shareSubcontractorId
     * @return bool
     * @internal param $fileId
     */
    public function unshareNextLevels($submittalId, $shareSubcontractorId)
    {
        $nextLevelUnshare = Submittal_permission::where('subm_id','=',$submittalId)
            ->where('comp_parent_id','=',$shareSubcontractorId)
            ->get();
        if(count($nextLevelUnshare) > 0) {
            foreach($nextLevelUnshare as $unshareFile) {
                $nextShareCompanyId = $unshareFile->comp_child_id;
                $unshareFile->delete();

                //recursion until there are still shares of the specified submittal
                $this->unshareNextLevels($submittalId, $nextShareCompanyId);
            }
        }
        return true;
    }

    /**
     * Unshare specified project submittal with specified company (first level of sharing)
     * @param $submittalId
     * @param $shareCompanyId
     * @param $receiveCompanyId
     * @return bool
     * @internal param $fileId
     */
    public function unshareSubmittal($submittalId, $shareCompanyId, $receiveCompanyId)
    {
        $sharingStep = Submittal_permission::where('subm_id','=',$submittalId)
            ->where('comp_parent_id','=',$shareCompanyId)
            ->where('comp_child_id','=',$receiveCompanyId)
            ->firstOrFail();

        //do the next levels of unsharing
        $nextLevelUnshare = $this->unshareNextLevels($submittalId, $sharingStep->comp_child_id);

        if($nextLevelUnshare) {
            $sharingStep->delete();
            return true;
        }
        return false;
    }

    /**
     * Store submittal and initial first version
     * @param $projectId
     * @param $data
     * @return static
     */
    public function storeProjectSubmittal($projectId, $data)
    {
        $project = Project::where('id', '=', $projectId)->first();

        if (str_pad(($project->submittals_counter+1), 3, '0', STR_PAD_LEFT) == $data['number']) {
            $project->submittals_counter++;
            $project->save();
        }

        //Store submittal
        $submittal = Submittal::create([
            'name' => $data['name'],
            'number' => $data['number'],
            'subject' => $data['subject'],
            'sent_via' => (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'],
            'submittal_type' => (!empty($data['submittal_type_dropdown']))?$data['submittal_type_dropdown']:$data['submittal_type'],
            'quantity' => ($data['quantity'] != '') ? $data['quantity'] : 0,
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'mf_number' => $data['master_format_number'],
            'mf_title' => $data['master_format_title'],
            'sub_id' => (empty($data['self_performed']) && !empty($data['sub_id'])) ? $data['sub_id'] : 0,
            'self_performed' => empty($data['self_performed']) ? 0 : 1,
            //'sub_office_id' => !empty($data['sub_office']) ? $data['sub_office'] : 0,
            'sub_contact_id' => !empty($data['sub_contact']) ? $data['sub_contact'] : 0,
            'recipient_id' => !empty($data['recipient_id']) ? $data['recipient_id'] : 0,
            //'recipient_office_id' => isset($data['recipient_office']) ? $data['recipient_office'] : 0,
            'recipient_contact_id' => isset($data['recipient_contact']) ? $data['recipient_contact'] : 0,
            'gc_id' => !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0,
            'make_me_gc' => isset($data['make_me_gc']) ? 1 : 0,
            'proj_id' => $projectId,
            'note' => $data['note'],
            'send_notif_subcontractor' => !empty($data['send_notif_subcontractor']) ? 1 : 0,
            'send_notif_recipient' => !empty($data['send_notif_recipient']) ? 1 : 0
        ]);

        //Store submittal version
        $submittalVersion = Submittal_version::create([
            'submittal_id' => $submittal->id,
            'cycle_no' => $data['cycle_no'],
            'sent_appr' => empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d'),
            'rec_appr' => empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d'),
            'subm_sent_sub' => empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d'),
            'rec_sub' => empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d'),
            'status_id' => 8, //draft as default //$data['status'],
            'notes' => ''
        ]);

        //update the last version field with the version id
        $submittal->last_version = $submittalVersion->id;

        if ($data['useCustom'] != 'true') {
            $submittal->cycle_counter++;
        }

        $submittal->save();

        return $submittal;
    }

    /**
     * Store submittal version
     * @param $submittalId
     * @param $data
     * @return static
     */
    public function storeSubmittalVersion($submittalId, $data)
    {
        $newVersion = Submittal_version::create([
            'submittal_id' => $submittalId,
            'cycle_no' => $data['cycle_no'],
            'sent_appr' => empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d'),
            'rec_appr' => empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d'),
            'subm_sent_sub' => empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d'),
            'rec_sub' => empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d'),
            'status_id' => $data['status'],
            'submitted_for' => (!empty($data['submitted_for_dropdown']))?$data['submitted_for_dropdown']:$data['submitted_for']
        ]);

        $submfittalVersionFile = new Submittal_version_file();

        //connect submittal version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $submfittalVersionFile::create([
                'submittal_version_id' => $newVersion->id,
                'file_id' => $data['rec_sub_file_id'],
                'download_priority' => 1,
            ]);
        }

        if ($data['sent_appr_file_id'] != '') {
            $submfittalVersionFile::create([
                'submittal_version_id' => $newVersion->id,
                'file_id' => $data['sent_appr_file_id'],
                'download_priority' => 2,
            ]);
        }

        if ($data['rec_appr_file_id'] != '') {
            $submfittalVersionFile::create([
                'submittal_version_id' => $newVersion->id,
                'file_id' => $data['rec_appr_file_id'],
                'download_priority' => 3,
            ]);
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $submfittalVersionFile::create([
                'submittal_version_id' => $newVersion->id,
                'file_id' => $data['subm_sent_sub_file_id'],
                'download_priority' => 4,
            ]);
        }

        $submittal = Submittal::where('id','=',$submittalId)->first();
        $submittal->last_version = $newVersion->id;

        if ($data['useCustom'] != 'true') {
            $submittal->cycle_counter++;
        }

        $submittal->save();

        return $newVersion;
    }

    /**
     * Returns contacts from submittal version
     *
     * @param $versionId
     * @return mixed
     */
    public function getContactsForSubmittalVersion($versionId, $type)
    {
        return Submittal_version_distribution::where('submittal_version_id', '=', $versionId)
                                              ->where('type', '=', $type)
                                              ->get();
    }

    /**
     * Get concrete submittal
     * @param $submittalId
     * @return mixed
     */
    public function getSubmittal($submittalId)
    {
        return Submittal::where('id','=',$submittalId)
            ->with('subcontractor')
            ->with('subcontractor.addresses')
            ->with('subcontractor.addresses.ab_contacts')
            ->with('subcontractor.contacts')
            ->with('subcontractor_contact')
            ->with('recipient')
            ->with('recipient.addresses')
            ->with('recipient.addresses.ab_contacts')
            ->with('recipient.contacts')
            ->with('recipient_contact')
            ->with('general_contractor')
            ->with([
                'submittals_versions',
                'submittals_versions.status',
                'submittals_versions.file',
                'submittals_versions.transmittalSentFile',
                'submittals_versions.transmittalSubmSentFile'
            ])
            ->firstOrFail();
    }

    /**
     * Get concrete submittal version
     * @param $versionId
     * @return mixed
     */
    public function getSubmittalVersion($versionId)
    {
        return Submittal_version::where('id','=',$versionId)
            ->with('submittal')
            ->with('submittal.recipient')
            ->with('submittal.subcontractor')
            ->with('submittalVersionApprUsers.users.company')
            ->with('submittalVersionApprUsers.abUsers.addressBook')
            ->with('submittalVersionSubcUsers.users.company')
            ->with('submittalVersionSubcUsers.abUsers.addressBook')
            ->with('transmittalSubmSentFile')
            ->with('transmittalSentFile')
            ->with('file')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get submittal version for generation of submittal transmittal
     * @param $versionId
     * @return mixed
     */
    public function getSubmittalVersionForTransmittal($versionId)
    {
        return Submittal_version::where('id','=',$versionId)
            ->with('submittal')
            ->with('submittal.general_contractor')
            ->with('submittal.subcontractor')
            ->with('submittal.subcontractor_contact')
            ->with('submittal.subcontractor_contact.office')
            ->with('submittal.recipient')
            ->with('submittal.recipient_contact')
            ->with('submittal.recipient_contact.office')
            ->with('submittal.project')
            ->with('submittal.project.company')
            ->with('file')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get all submittal versions
     * @param $submittalId
     * @return mixed
     */
    public function getSubmittalVersions($submittalId)
    {
        return Submittal_version::where('submittal_id','=',$submittalId)->with('file')->get();
    }

    /**
     * Update concrete submittal
     * @param $submittalId
     * @param $data
     * @return mixed
     */
    public function updateSubmittal($submittalId, $data)
    {
        $submittal = Submittal::where('id','=',$submittalId)->firstOrFail();
        $submittal->name = $data['name'];
        $submittal->number = $data['number'];
        $submittal->subject = $data['subject'];
        $submittal->sent_via = (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'];
        $submittal->submittal_type = (!empty($data['submittal_type_dropdown']))?$data['submittal_type_dropdown']:$data['submittal_type'];
        $submittal->quantity = $data['quantity'];
        $submittal->mf_number = $data['master_format_number'];
        $submittal->mf_title = $data['master_format_title'];
        $submittal->self_performed = empty($data['self_performed']) ? 0 : 1;
        $submittal->sub_id = (empty($data['self_performed']) && !empty($data['sub_id'])) ? $data['sub_id'] : null;
        //$submittal->sub_office_id = !empty($data['sub_office']) ? $data['sub_office'] : 0;
        $submittal->sub_contact_id = !empty($data['sub_contact']) ? $data['sub_contact'] : 0;
        $submittal->recipient_id = !empty($data['recipient_id']) ? $data['recipient_id'] : 0;
        //$submittal->recipient_office_id = isset($data['recipient_office']) ? $data['recipient_office'] : 0;
        $submittal->recipient_contact_id = isset($data['recipient_contact']) ? $data['recipient_contact'] : 0;
        $submittal->gc_id = !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0;
        $submittal->make_me_gc = isset($data['make_me_gc']) ? 1 : 0;
        $submittal->note = $data['note'];
        $submittal->send_notif_subcontractor = !empty($data['send_notif_subcontractor']) ? 1 : 0;
        $submittal->send_notif_recipient = !empty($data['send_notif_recipient']) ? 1 : 0;
        $submittal->save();
        return $submittal;
    }

    /**
     * @param $versionId
     * @param $fileDateConnectionString
     * @return null
     */
    public function checkIfFileExist($versionId, $fileDateConnectionString)
    {
        $submittalVersion = Submittal_version::where('id','=',$versionId)
            ->with(array('file' => function($query) use ($fileDateConnectionString)
            {
                $query->where('files.version_date_connection', $fileDateConnectionString);
            }))
            ->first();

        if (!is_null($submittalVersion)) {
            if (count($submittalVersion->file)) {
                return Project_file::where('id','=',$submittalVersion->file[0]->id)->first();
            }
        }

        return null;
    }

    /**
     * Update concrete submittal version
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateSubmittalVersion($versionId, $data)
    {
        $submittalVersion = Submittal_version::where('id','=',$versionId)->firstOrFail();
        $submittalVersion->cycle_no = $data['cycle_no'];
        $submittalVersion->sent_appr = empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d');
        $submittalVersion->rec_appr = empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d');
        $submittalVersion->subm_sent_sub = empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d');
        $submittalVersion->rec_sub = empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d');
        $submittalVersion->status_id = $data['status'];
        $submittalVersion->submitted_for = (!empty($data['submitted_for_dropdown']))?$data['submitted_for_dropdown']:$data['submitted_for'];
        $submittalVersion->save();

        $submfittalVersionFile = new Submittal_version_file();

        //connect submittal version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $currentVersionFile = $submfittalVersionFile->where('submittal_version_id','=',$submittalVersion->id)
                ->where('file_id','=',$data['rec_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 1
                ]);
            } else {
                $submfittalVersionFile::create([
                    'submittal_version_id' => $submittalVersion->id,
                    'file_id' => $data['rec_sub_file_id'],
                    'download_priority' => 1,
                ]);
            }
        }

        if ($data['sent_appr_file_id'] != '') {
            $currentVersionFile = $submfittalVersionFile->where('submittal_version_id','=',$submittalVersion->id)
                ->where('file_id','=',$data['sent_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 2
                ]);
            } else {
                $submfittalVersionFile::create([
                    'submittal_version_id' => $submittalVersion->id,
                    'file_id' => $data['sent_appr_file_id'],
                    'download_priority' => 2,
                ]);
            }
        }

        if ($data['rec_appr_file_id'] != '') {
            $currentVersionFile = $submfittalVersionFile->where('submittal_version_id','=',$submittalVersion->id)
                ->where('file_id','=',$data['rec_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 3
                ]);
            } else {
                $submfittalVersionFile::create([
                    'submittal_version_id' => $submittalVersion->id,
                    'file_id' => $data['rec_appr_file_id'],
                    'download_priority' => 3,
                ]);
            }
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $currentVersionFile = $submfittalVersionFile->where('submittal_version_id','=',$submittalVersion->id)
                ->where('file_id','=',$data['subm_sent_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 4
                ]);
            } else {
                $submfittalVersionFile::create([
                    'submittal_version_id' => $submittalVersion->id,
                    'file_id' => $data['subm_sent_sub_file_id'],
                    'download_priority' => 4,
                ]);
            }
        }

        return $submittalVersion;
    }

    /**
     * Update concrete submittal version for transmittal notes
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateSubmittalVersionTransmittalNotes($versionId, $data)
    {
        $submittalVersion = Submittal_version::where('id','=',$versionId)->firstOrFail();
        $submittalVersion->appr_notes = $data['version_appr_note'];
        $submittalVersion->subc_notes = $data['version_subc_note'];
        $submittalVersion->save();

        return $submittalVersion;
    }

    /**
     * Delete submittal file from S3
     * @param $projectId
     * @param $filename
     * @return bool
     */
    public function deleteSubmittalFile($projectId, $filename)
    {
        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/submittals/'.$filename;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Check the specified submittal version by its id if it's the last created version of the specified submittal
     * @param $submittalId
     * @param $versionId
     * @return bool
     */
    public function lastVersionCheck($submittalId, $versionId)
    {
        $currentVersion = Submittal_version::where('submittal_id','=',$submittalId)->orderBy('id','DESC')->firstOrFail();
        if($currentVersion->id == $versionId) {
            return true;
        }
        return false;
    }

    /**
     * Change the last submittal version with another submittal version specified by id as last version of that submittal
     * @param $submittalId
     * @param $versionId
     * @return bool
     */
    public function changeLastVersion($submittalId, $versionId)
    {
        $version = Submittal_version::where('submittal_id','=',$submittalId)->orderBy('id','DESC')->take(1)->skip(1)->first();
        if ($version) {
            $submittal = Submittal::where('id','=',$submittalId)->first();
            $submittal->last_version = $version->id;
            $submittal->save();

            return true;
        }

        return false;
    }

    /**
     * Delete concrete submittal version
     * @param $versionId
     * @return int
     */
    public function deleteSubmittalVersion($versionId)
    {
        //get submittal version
        $version = Submittal_version::where('id','=',$versionId)
            ->with('file')
            ->firstOrFail();

        //check if the submittal version has files
        if (count($version->file)) {
            foreach ($version->file as $file) {
                //delete submittal version and file connection
                Submittal_version_file::where('file_id','=',$file->id)
                    ->where('submittal_version_id','=',$versionId)
                    ->delete();

                //delete submittal version file
                Project_file::where('id','=',$file->id)
                    ->delete();
            }
        }

        //delete submittal version
        return $version->delete();
    }

    /**
     * Delete concrete submittal with all its versions
     * @param $submittalId
     * @return bool|null
     */
    public function deleteSubmittal($submittalId)
    {
        $result = Submittal::find($submittalId);
        $versions = Submittal_version::where('submittal_id','=',$result->id)
            ->with('file')
            ->get();
        if (count($versions)) {
            foreach ($versions as $version) {
                if (count($version->file)) {
                    $versionId = $version->id;
                    foreach ($version->file as $file) {
                        Submittal_version_file::where('file_id','=',$file->id)->where('submittal_version_id','=',$versionId)->delete();
                        Project_file::where('id','=',$file->id)->delete();
                    }
                }
            }
            return $result->delete();
        }
        return false;
    }

    public function deleteTransmittalFromS3($projectId, $fileId)
    {
        //get file
        $file = Project_file::where('id','=',$fileId)->firstOrFail();

        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/submittals/transmittals/'.$file->file_name;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    public function getTransmittal($transmittalId)
    {
        return Transmittal::where('id','=',$transmittalId)
            ->with('file')
            ->firstOrFail();
    }

    public function deleteTransmittal($transmittalId)
    {
        $transmittal = Transmittal::where('id','=',$transmittalId)->firstOrFail();

        Project_file::where('id','=',$transmittal->file_id)->delete();

        return $transmittal->delete();
    }

    /**
     * return all submittal statuses
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getSubmittalStatuses()
    {
        return Submittal_status::all();
    }

    public function shareWithAllSubcontractors($projectId, $selectedSubmittals)
    {
        //get all subcontractors for the project
        $allProjectSubcontractors = Project_subcontractor::where('proj_id','=',$projectId)
            ->where('comp_parent_id','=',$this->companyId)
            ->get();

        if(count($allProjectSubcontractors)) {
            foreach($selectedSubmittals as $submittal) {
                foreach($allProjectSubcontractors as $subcontractor) {
                    Submittal_permission::firstOrCreate([
                        'subm_id' => $submittal,
                        'comp_child_id' => $subcontractor->comp_child_id,
                        'comp_parent_id' => $this->companyId
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Share selected files with selected subcontractors or with
     * the general contractor
     * @param $selectedSubmittals
     * @param $selectedSubcontractors
     * @return bool
     */
    public function shareWithSelectedSubcontractors($selectedSubmittals, $selectedSubcontractors)
    {
        foreach($selectedSubmittals as $submittal) {
            foreach($selectedSubcontractors as $subcontractor) {
                Submittal_permission::firstOrCreate([
                    'subm_id' => $submittal,
                    'comp_child_id' => $subcontractor,
                    'comp_parent_id' => $this->companyId
                ]);
            }
        }

        return true;
    }

    /**
     * Delete specific submittal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleSubmittalFile($params)
    {
        Submittal_version_file::where('file_id','=',$params['fileId'])
            ->delete();

        $result = Project_file::where('id','=',$params['fileId'])
            ->delete();

        //if storage limit bellow 90% reset notification
        $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
            $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
        }

        return $result;
    }

    public function markTransmittalApprSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $submittal = Submittal_version_distribution::firstOrCreate([
                        'submittal_version_id' => $versionId,
                        'user_id' => $userId,
                        'type' => 'appr'
                ]);

                $submittal->created_at = Carbon::now()->toDateString();
                $submittal->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $submittal = Submittal_version_distribution::firstOrCreate([
                    'submittal_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'appr'
                ]);

                $submittal->created_at = Carbon::now()->toDateString();
                $submittal->save();
            }
        }
    }

    public function markTransmittalSubrSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $submittal = Submittal_version_distribution::firstOrCreate([
                    'submittal_version_id' => $versionId,
                    'user_id' => $userId,
                    'type' => 'subc'
                ]);

                $submittal->created_at = Carbon::now()->toDateString();
                $submittal->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $submittal = Submittal_version_distribution::firstOrCreate([
                    'submittal_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'subc'
                ]);

                $submittal->created_at = Carbon::now()->toDateString();
                $submittal->save();
            }
        }
    }

    public function getSubmittalSubcontractorRecipientUserDistribution($versionId, $subcontractor=false)
    {
        return SubmittalsSubcontractorProposalDistribution::where('version_id', '=', $versionId)
            ->where('subcontractor', '=', $subcontractor)
            ->with('user.company')
            ->get();
    }

    /**
     * Creates an entry for each user where the email is sent
     * @param $versionId
     * @param $userId
     * @param $subcontractor
     * @return SubmittalsSubcontractorProposalDistribution
     */
    public function addProposalUserDistribution($versionId, $userId, $subcontractor)
    {
        $submittalSubcontractorProposal = SubmittalsSubcontractorProposalDistribution::firstOrCreate(
            [
                'version_id' => $versionId,
                'user_id' => $userId,
                'subcontractor' => $subcontractor
            ]);

        $submittalSubcontractorProposal->created_at = Carbon::now()->toDateString();
        $submittalSubcontractorProposal->save();

        return $submittalSubcontractorProposal;
    }

    /** Get number column from submittals table for specific project_id
     * @param $projectId
     */
    public function getGeneratedNumbers($projectId)
    {
        return Submittal::where('proj_id', '=', $projectId)
        ->where('comp_id', '=', $this->companyId)
        ->where('user_id', '=', $this->userId)
        ->orderBy('number', 'ASC')
            ->get(['number']);
    }
}