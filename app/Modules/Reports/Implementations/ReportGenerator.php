<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 5/19/2015
 * Time: 1:05 PM
 */
namespace App\Modules\Reports\Implementations;

use Barryvdh\DomPDF\PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class ReportGenerator {

    /**
     * Generate pdf report
     * @param $type
     * @param $data
     * @param $view
     * @param $name
     * @return mixed
     */
    public function generateReport($type, $data, $view, $name)
    {
//        $pdf = PDF::loadView($view, $data);
//        return $pdf->download($type.'_invoice.pdf');

        //prepare array with different types for landscape oriented pdfs
        $landscapeTypeArr = [
            Config::get('constants.submittal_transmittals'),
            Config::get('constants.rfi_transmittals'),
            Config::get('constants.pco_transmittals'),
            Config::get('constants.proposals'),
            Config::get('constants.proposals-report'),
            Config::get('constants.submittals'),
            Config::get('constants.submittals-report'),
            Config::get('constants.submittals-shared'),
            Config::get('constants.submittals-shared-report'),
            Config::get('constants.rfis'),
            Config::get('constants.rfis-report'),
            Config::get('constants.rfis-shared'),
            Config::get('constants.rfis-shared-report'),
            Config::get('constants.pcos'),
            Config::get('constants.pcos-report'),
            Config::get('constants.pcos-shared'),
            Config::get('constants.pcos-shared-report'),
            Config::get('constants.materials'),
            Config::get('constants.materials-shared'),
            Config::get('constants.materials-shared-report'),
            Config::get('constants.address-book-report'),
            Config::get('constants.tasks-report'),
        ];

        //Generate the file
        $pdf = App::make('dompdf.wrapper');

        //check if the pdf should be generated as a landscape document
        if (in_array($type, $landscapeTypeArr)) {
            return $pdf->loadView($view, $data)->setOrientation('landscape')->stream($name.'.pdf');
        }

        return $pdf->loadView($view, $data)->stream($name.'.pdf');

        //Download the file
        //return $pdf->download($type.'_invoice.pdf');

        //return $pdf->loadFile('http://www.github.com')->stream('github.pdf');
    }

    public function generateReportRaw($type, $data, $view, $name)
    {
//        $pdf = PDF::loadView($view, $data);
//        return $pdf->download($type.'_invoice.pdf');

        //prepare array with different types for landscape oriented pdfs
        $landscapeTypeArr = [
            Config::get('constants.submittal_transmittals'),
            Config::get('constants.rfi_transmittals'),
            Config::get('constants.pco_transmittals'),
            Config::get('constants.proposals'),
            Config::get('constants.proposals-report'),
            Config::get('constants.submittals'),
            Config::get('constants.submittals-report'),
            Config::get('constants.submittals-shared'),
            Config::get('constants.submittals-shared-report'),
            Config::get('constants.rfis'),
            Config::get('constants.rfis-report'),
            Config::get('constants.rfis-shared'),
            Config::get('constants.rfis-shared-report'),
            Config::get('constants.pcos'),
            Config::get('constants.pcos-report'),
            Config::get('constants.pcos-shared'),
            Config::get('constants.pcos-shared-report'),
            Config::get('constants.materials'),
            Config::get('constants.materials-shared'),
            Config::get('constants.materials-shared-report'),
            Config::get('constants.address-book-report'),
        ];

        //Generate the file
        $pdf = App::make('dompdf.wrapper');

        //check if the pdf should be generated as a landscape document
        if (in_array($type, $landscapeTypeArr)) {
            return $pdf->loadView($view, $data)->setOrientation('landscape');
        }

        return $pdf->loadView($view, $data);
        //Download the file
        //return $pdf->download($type.'_invoice.pdf');

        //return $pdf->loadFile('http://www.github.com')->stream('github.pdf');
    }

}