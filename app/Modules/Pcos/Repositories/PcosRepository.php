<?php
namespace App\Modules\Pcos\Repositories;

use App\Models\File_type;
use App\Models\Module;
use App\Models\Pco;
use App\Models\Pco_permission;
use App\Models\Pco_status;
use App\Models\Pco_subcontractor_recipient;
use App\Models\Pco_version;
use App\Models\Pco_version_distribution;
use App\Models\Pco_version_file;
use App\Models\PcosSubcontractorProposalDistribution;
use App\Models\Project;
use App\Models\Project_company_permission;
use App\Models\Project_file;
use App\Models\Project_subcontractor;
use App\Models\Proposal_supplier_permission;
use App\Models\Transmittal;
use App\Models\User;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Services\CustomHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PcosRepository {

    private $companyId;
    private $userId;
    private $projectSubcontractorsRepo;
    private $projectPermissionsRepository;
    private $pcoModule;
    private $filesRepo;
    private $companyRepo;

    public function __construct()
    {
        if (Auth::user()) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->projectPermissionsRepository = new ProjectPermissionsRepository();
        $this->pcoModule = File_type::where('display_name', '=', 'pcos')->first();
        $this->filesRepo = new FilesRepository();
        $this->companyRepo = new CompaniesRepository();
    }

    /**
     * Get all project pcos
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getProjectPcos($projectId, $sort, $order, $paginate = 50)
    {
        return Pco::where('pcos.proj_id', '=', $projectId)
            ->where('pcos.comp_id', '=', $this->companyId)
            ->leftJoin('pcos_subcontractors_recipients', function($leftJoin)
            {
                $leftJoin->on('pcos_subcontractors_recipients.pco_id', '=', 'pcos.id')->where('pcos_subcontractors_recipients.ab_recipient_id','!=',0);
            })
            ->leftJoin('pcos_versions','pcos_subcontractors_recipients.last_recipient_version','=','pcos_versions.id')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                WHERE pco_version_files.pco_version_id = pcos_versions.id
                ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'pcos_subcontractors_recipients.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
            ->select('pcos.*',
                'pcos_subcontractors_recipients.mf_number as mf_number',
                'pcos_subcontractors_recipients.mf_title as mf_title',
                'pcos_versions.id as version_id',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as version_sent_appr',
                'pcos_versions.rec_appr as version_rec_appr',
                'pcos_versions.status_id as version_status_id',
                'pcos_versions.cost as cost',
                'pco_statuses.short_name as version_status_short_name',
                'pco_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('subcontractors')
            ->with('subcontractors.subcontractor_versions')
            ->with('subcontractors.subcontractor_versions.file')
            ->with('subcontractors.subcontractor_versions.transmittalSubmSentFile')
            ->with('subcontractors.ab_subcontractor')
            ->with('recipient')
            ->with('recipient.recipient_versions')
            ->with('recipient.recipient_versions.file')
            ->with('recipient.recipient_versions.transmittalSentFile')
            ->with('recipient.ab_recipient')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Filter pcos by master format number or title and recipient sub-query
     * @param $fullQuery
     * @param $masterFormatSegment
     * @param $recipient
     * @param bool $hisShared
     * @return mixed
     */
    public function filterPcosMFRecipient($fullQuery, $masterFormatSegment, $recipient, $hisShared = false)
    {
        //if entered master format number or title input is numeric, filter data by mf_number
        if(is_numeric(substr($masterFormatSegment, 0, 1))) {
            $fullQuery->join('pcos_subcontractors_recipients as subcontractors', function($join) use ($masterFormatSegment) {
                    $join->on('subcontractors.pco_id', '=', 'pcos.id');
                    $join->where('subcontractors.mf_number','LIKE',$masterFormatSegment.'%')
                        ->where('subcontractors.type','=',0);
                });

            //join address book table if the subcontractor have permission to see only his PCO's
            if ($hisShared) {
                $fullQuery->leftJoin('address_book as ab_subcontractor', function($join) {
                    $join->on('subcontractors.ab_subcontractor_id','=','ab_subcontractor.id');
                })
                    ->where('ab_subcontractor.synced_comp_id','=',$this->companyId);
            }

            if (empty($recipient)) {
                $fullQuery->leftJoin('pcos_subcontractors_recipients as recipient', function($join) use ($recipient)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id');
                    $join->where('recipient.type','=',1);
                });
            } else {
                $fullQuery->join('pcos_subcontractors_recipients as recipient', function($join) use ($recipient)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id');
                    $join->where('recipient.ab_recipient_id','=',$recipient);
                    $join->where('recipient.type','=',1);
                });
            }

            $fullQuery->groupBy('subcontractors.pco_id');
        }
        //if entered master format number or title input is not numeric, filter data by mf_title
        elseif(ctype_alpha(substr($masterFormatSegment, 0, 1))) {
            $fullQuery->join('pcos_subcontractors_recipients as subcontractors', function($join) use ($masterFormatSegment)
            {
                $join->on('subcontractors.pco_id', '=', 'pcos.id');
                $join->where('subcontractors.mf_title','LIKE',$masterFormatSegment.'%')
                    ->where('subcontractors.type','=',0);
            });

            //join address book table if the subcontractor have permission to see only his PCO's
            if ($hisShared) {
                $fullQuery->leftJoin('address_book as ab_subcontractor', function($join) {
                    $join->on('subcontractors.ab_subcontractor_id','=','ab_subcontractor.id');
                })
                    ->where('ab_subcontractor.synced_comp_id','=',$this->companyId);
            }

            if (empty($recipient)) {
                $fullQuery->leftJoin('pcos_subcontractors_recipients as recipient', function($join) use ($recipient)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id');
                    $join->where('recipient.type','=',1);
                });
            } else {
                $fullQuery->join('pcos_subcontractors_recipients as recipient', function($join) use ($recipient)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id');
                    $join->where('recipient.ab_recipient_id','=',$recipient);
                    $join->where('recipient.type','=',1);
                });
            }

            $fullQuery->groupBy('subcontractors.pco_id');
        }
        //if master format number or title input is empty than filter data only by recipient
        else {
            //join address book table if the subcontractor have permission to see only his PCO's
            if ($hisShared) {
                $fullQuery->join('pcos_subcontractors_recipients as subcontractors', function($join) {
                        $join->on('subcontractors.pco_id', '=', 'pcos.id')
                            ->where('subcontractors.type','=',Config::get('constants.pco_subcontractor_or_recipient.subcontractor'));
                    })
                    ->leftJoin('address_book as ab_subcontractor', function($join) {
                        $join->on('subcontractors.ab_subcontractor_id','=','ab_subcontractor.id');
                    })
                    ->where('ab_subcontractor.synced_comp_id','=',$this->companyId);
            }

            if (empty($recipient)) {
                $fullQuery->leftJoin('pcos_subcontractors_recipients as recipient', function($join)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id')
                        ->where('recipient.type','=',1);
                });
            } else {
                $fullQuery->join('pcos_subcontractors_recipients as recipient', function($join) use ($recipient)
                {
                    $join->on('recipient.pco_id', '=', 'pcos.id')
                        ->where('recipient.ab_recipient_id','=',$recipient)
                        ->where('recipient.type','=',1);
                });
            }
        }

        return $fullQuery;
    }

    public function filterPcosStatus($fullQuery, $status)
    {
        //if status input is All
        if($status == 'All') {
            $fullQuery->leftJoin('pcos_versions','recipient.last_recipient_version','=','pcos_versions.id');
        }
        //if status input is all open
        else if ($status == 'all_open') {
            //get all open status options
            $allOpenStatuses = Pco_status::whereIn('short_name', Config::get('constants.all_open'))
                ->lists('id');

            if (count($allOpenStatuses)) {
                $fullQuery->rightJoin('pcos_versions', function($join) use ($allOpenStatuses)
                {
                    $join->on(DB::raw("pcos_versions.id = recipient.last_recipient_version and pcos_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                });
            }
        }
        //if status input is some of the regular pco statuses fetched from the database
        else {
            $fullQuery->join('pcos_versions', function($join) use ($status)
            {
                $join->on('recipient.last_recipient_version', '=', 'pcos_versions.id')
                    ->where('pcos_versions.status_id','=',$status);
            });
        }

        return $fullQuery;
    }

    /**
     * Get all pcos by project id
     * @param $projectId
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPcosForProject($projectId, $compId)
    {
        return Pco::where('proj_id', '=', $projectId)
            ->where('comp_id','=',$compId)
            ->with('recipient.latest_recipient_version.status')
            ->with('subcontractors.subcontractor_versions.file')
            ->with('subcontractors.subcontractor_versions.smallFile')
            ->with('subcontractors.subcontractor_versions.transmittalSubmSentFile')
            ->with('subcontractors.subcontractor_versions.transmittalSentFile')
            ->with('recipient.recipient_versions.file')
            ->with('recipient.recipient_versions.smallFile')
            ->with('recipient.recipient_versions.transmittalSubmSentFile')
            ->with('recipient.recipient_versions.transmittalSentFile')
            ->get();
    }

    /**
     * Filter pcos
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterPcos($params, $paginate = 50)
    {
        //get status filter input
        $status = $params['status'];

        //Create query conditions
        $query = [
            'pcos.proj_id' => $params['projectId']
        ];

        $startQuery = Pco::where($query);

        //parse master format filter input
        $masterFormatPieces = explode(" - ", $params['masterFormat']);
        $masterFormatSegment = $masterFormatPieces[0];

        //filter data by entered master format number or title and recipient
        $fullQuery = $this->filterPcosMFRecipient($startQuery, $masterFormatSegment, $params['recipient']);

        //filter data by selected status
        $fullQuery = $this->filterPcosStatus($fullQuery, $status);

        if (!empty($params['name'])) {
            $fullQuery->where('pcos.name', 'LIKE', $params['name'].'%');
        }

        return $fullQuery->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                WHERE pco_version_files.pco_version_id = pcos_versions.id
                ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
            ->select('pcos.*',
                'recipient.mf_number as mf_number',
                'recipient.mf_title as mf_title',
                'recipient.ab_recipient_id as ab_recipient_id',
                'recipient.id as psr_id',
                'pcos_versions.id as version_id',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as version_sent_appr',
                'pcos_versions.rec_appr as version_rec_appr',
                'pcos_versions.status_id as version_status_id',
                'pco_statuses.short_name as version_status_short_name',
                'pco_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('subcontractors')
            ->with('subcontractors.ab_subcontractor')
            ->orderBy($params['sort'],$params['order'])
            ->paginate($paginate);
    }

    /**
     * Report pcos
     * @param $params
     * @return array
     * @internal param int $perPage
     */
    public function reportPcos($params)
    {
        //get status filter input
        $status = $params['status'];

        //Create query conditions
        $query = [
            'pcos.proj_id' => $params['projectId']
        ];

        $startQuery = Pco::where($query);

        //parse master format filter input
        $masterFormatPieces = explode(" - ", $params['masterFormat']);
        $masterFormatSegment = $masterFormatPieces[0];

        //filter data by entered master format number or title and recipient
        $fullQuery = $this->filterPcosMFRecipient($startQuery, $masterFormatSegment, $params['recipient']);

        //filter data by selected status
        $fullQuery = $this->filterPcosStatus($fullQuery, $status);

        return $fullQuery->leftJoin('files', function($leftJoin)
        {
            $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                WHERE pco_version_files.pco_version_id = pcos_versions.id
                ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
        })
            ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
            ->select('pcos.*',
                'recipient.mf_number as mf_number',
                'recipient.mf_title as mf_title',
                'recipient.ab_recipient_id as ab_recipient_id',
                'recipient.id as psr_id',
                'pcos_versions.id as version_id',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as version_sent_appr',
                'pcos_versions.rec_appr as version_rec_appr',
                'pcos_versions.status_id as version_status_id',
                'pco_statuses.short_name as version_status_short_name',
                'pco_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection')
            ->with('subcontractors')
            ->with('subcontractors.ab_subcontractor')
            ->with('recipient.latest_recipient_version')
            ->orderBy($params['sort'],$params['order'])
            ->get();
    }

    /**
     * Get all shared pcos
     * @param $sharedPcos
     * @return mixed
     */
    public function getAllSharedPcos($sharedPcos)
    {
        $sharedPcos->leftJoin('pcos_subcontractors_recipients as recipient', function($leftJoin)
            {
                $leftJoin->on('recipient.pco_id', '=', 'pcos.id')->where('recipient.type','=',Config::get('constants.pco_subcontractor_or_recipient.recipient'));
            })
            ->leftJoin('pcos_versions','recipient.last_recipient_version','=','pcos_versions.id')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                        WHERE pco_version_files.pco_version_id = pcos_versions.id
                        ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id');
        return $sharedPcos;
    }

    /**
     * Get only the pco records where the logged in company figures
     * @param $sharedPcos
     * @return mixed
     */
    public function getHisSharedPcos($sharedPcos)
    {
        $sharedPcos->join('pcos_subcontractors_recipients as subcontractors', function($join) {
                $join->on('subcontractors.pco_id', '=', 'pcos.id')
                    ->where('subcontractors.type','=',Config::get('constants.pco_subcontractor_or_recipient.subcontractor'));
            })
            ->join('address_book as ab_subcontractor', function($join) {
                $join->on('subcontractors.ab_subcontractor_id','=','ab_subcontractor.id')
                    ->where('ab_subcontractor.synced_comp_id','=',$this->companyId);
            });

        $sharedPcos->leftJoin('pcos_subcontractors_recipients as recipient', function($join)
            {
                $join->on('recipient.pco_id', '=', 'pcos.id');
                $join->where('recipient.type','=',Config::get('constants.pco_subcontractor_or_recipient.recipient'));
            })
            ->leftJoin('pcos_versions','recipient.last_recipient_version','=','pcos_versions.id')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                        WHERE pco_version_files.pco_version_id = pcos_versions.id
                        ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book as address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id');

        $sharedPcos->groupBy('subcontractors.pco_id');

        return $sharedPcos;
    }

    /**
     * Filter shared pcos
     * @param $params
     * @param int $paginate
     * @return array
     * @internal param int $perPage
     */
    public function filterSharedPcos($params, $paginate = 50)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'pcos.proj_id' => $params['projectId'],
            'pcos.comp_id' => $project->comp_id
        ];

        $fullQuery = Pco::where($query);

        //join only shared pcos
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $pcoType = Module::where('display_name','=','pcos')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $pcosPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        if (!is_null($pcosPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($pcosPermissions) && $pcosPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('pco_permissions', 'pcos.id', '=', 'pco_permissions.pco_id')
                    ->where('pco_permissions.comp_child_id', '=', $this->companyId);
            }

            $pcoPermissionsType = Config::get('constants.pco_permission_type.none');
            if ((!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) ||
                (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos'))) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
            }

            if ((!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) ||
                (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos'))) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
            }

            if ($pcoPermissionsType != Config::get('constants.pco_permission_type.none')) {
                //parse master format filter input
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $masterFormatSegment = $masterFormatPieces[0];

                if ($pcoPermissionsType == Config::get('constants.pco_permission_type.his_pcos')) {
                    //filter data by entered master format number or title and recipient
                    $fullQuery = $this->filterPcosMFRecipient($fullQuery, $masterFormatSegment, $params['recipient'], true);
                } else {
                    //filter data by entered master format number or title and recipient
                    $fullQuery = $this->filterPcosMFRecipient($fullQuery, $masterFormatSegment, $params['recipient']);
                }

                if (!empty($params['name'])) {
                    $fullQuery->where('pcos.name', 'LIKE', $params['name'].'%');
                }

                //filter data by selected status
                $fullQuery = $this->filterPcosStatus($fullQuery, $status);

                $responseData['pcoPermissionsType'] = $pcoPermissionsType;

                $responseData['sharedPcos'] = $fullQuery->leftJoin('files', function ($leftJoin) {
                    $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                    WHERE pco_version_files.pco_version_id = pcos_versions.id
                    ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
                })
                    ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
                    ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
                    ->select('pcos.*',
                        'recipient.mf_number as mf_number',
                        'recipient.mf_title as mf_title',
                        'pcos_versions.id as version_id',
                        'pcos_versions.cycle_no as version_cycle_no',
                        'pcos_versions.sent_appr as version_sent_appr',
                        'pcos_versions.rec_appr as version_rec_appr',
                        'pcos_versions.status_id as version_status_id',
                        'pco_statuses.short_name as version_status_short_name',
                        'pco_statuses.name as version_status_name',
                        'address_book.name as subcontractor_name',
                        'files.file_name as file_name',
                        'files.id as file_id',
                        'files.version_date_connection as version_date_connection')
                    ->with('subcontractors')
                    ->with('subcontractors.ab_subcontractor')
                    ->orderBy($params['sort'],$params['order'])
                    ->paginate($paginate);

                return $responseData;
            }
        }

        return null;
    }

    /**
     * Generate report from shared pcos
     * @param $params
     * @return bool|null
     */
    public function reportSharedPcos($params)
    {
        //get the project
        $project = Project::where('id', '=', $params['projectId'])->first();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'pcos.proj_id' => $params['projectId'],
            'pcos.comp_id' => $project->comp_id
        ];

        $fullQuery = Pco::where($query);

        //join only shared pcos
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($params['projectId'], $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($params['projectId'], $this->companyId);

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        //get pco module data
        $pcoType = Module::where('display_name','=','pcos')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //get companies permissions
        $pcosPermissions = Project_company_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$params['projectId'])
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        if (!is_null($pcosPermissions) || !is_null($bidsPermissions)) {
            if ((!is_null($pcosPermissions) && $pcosPermissions->read != 1) || (!is_null($bidsPermissions) && $bidsPermissions->read != 1)) {
                $fullQuery->rightJoin('pco_permissions', 'pcos.id', '=', 'pco_permissions.pco_id')
                    ->where('pco_permissions.comp_child_id', '=', $this->companyId);
            }

            $pcoPermissionsType = Config::get('constants.pco_permission_type.none');
            if ((!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) ||
                (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos'))) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
            }

            if ((!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) ||
                (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos'))) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
            }

            if ($pcoPermissionsType != Config::get('constants.pco_permission_type.none')) {
                //parse master format filter input
                $masterFormatPieces = explode(" - ", $params['masterFormat']);
                $masterFormatSegment = $masterFormatPieces[0];

                if ($pcoPermissionsType == Config::get('constants.pco_permission_type.his_pcos')) {
                    //filter data by entered master format number or title and recipient
                    $fullQuery = $this->filterPcosMFRecipient($fullQuery, $masterFormatSegment, $params['recipient'], true);
                } else {
                    //filter data by entered master format number or title and recipient
                    $fullQuery = $this->filterPcosMFRecipient($fullQuery, $masterFormatSegment, $params['recipient']);
                }

                //filter data by selected status
                $fullQuery = $this->filterPcosStatus($fullQuery, $status);

                $responseData['pcoPermissionsType'] = $pcoPermissionsType;

                $responseData['sharedPcos'] = $fullQuery->leftJoin('files', function ($leftJoin) {
                    $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                    WHERE pco_version_files.pco_version_id = pcos_versions.id
                    ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
                    ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
                    ->select('pcos.*',
                        'recipient.mf_number as mf_number',
                        'recipient.mf_title as mf_title',
                        'pcos_versions.id as version_id',
                        'pcos_versions.cycle_no as version_cycle_no',
                        'pcos_versions.sent_appr as version_sent_appr',
                        'pcos_versions.rec_appr as version_rec_appr',
                        'pcos_versions.status_id as version_status_id',
                        'pco_statuses.short_name as version_status_short_name',
                        'pco_statuses.name as version_status_name',
                        'address_book.name as subcontractor_name',
                        'files.file_name as file_name',
                        'files.id as file_id',
                        'files.version_date_connection as version_date_connection')
                    ->with('subcontractors')
                    ->with('subcontractors.ab_subcontractor')
                    ->orderBy($params['sort'], $params['order'])
                    ->get();

                return $responseData;
            }
        }

        return null;
    }

    /**
     * Get shared project pcos
     * @param $projectId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return bool
     */
    public function getSharedProjectPcos($projectId, $sort, $order, $paginate = 50)
    {
        $subcontractor = $this->projectSubcontractorsRepo->isSubcontractor($projectId, $this->companyId);
        $bidder = $this->projectSubcontractorsRepo->isBidder($projectId, $this->companyId);
        $project = Project::where('id', '=', $projectId)->first();

        if ((Auth::user()->comp_id != $project->comp_id) && !$subcontractor && !$bidder) {
            return false;
        }

        $pcoType = Module::where('display_name','=','pcos')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //check companies permissions
        $pcosPermissions = Project_company_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        if (!is_null($pcosPermissions) || !is_null($bidsPermissions)) {
            $sharedPcos = Pco::where('pcos.proj_id', '=', $projectId)
                ->where('pcos.comp_id', '=', $project->comp_id);

            $pcoPermissionsType = null;

            if ((!is_null($pcosPermissions) && $pcosPermissions->read == 1 && !empty($pcosPermissions->pco_permission_type)) ||
                (!is_null($bidsPermissions) && $bidsPermissions->read == 1 && !empty($bidsPermissions->pco_permission_type))) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.none');
                if (!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) {
                    $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
                    $sharedPcos = $this->getAllSharedPcos($sharedPcos);
                } elseif (!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) {
                    $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
                    $sharedPcos = $this->getHisSharedPcos($sharedPcos);
                } else {
                    if (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) {
                        $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
                        $sharedPcos = $this->getAllSharedPcos($sharedPcos);
                    } elseif (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) {
                        $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
                        $sharedPcos = $this->getHisSharedPcos($sharedPcos);
                    }
                }
            } else {
                $sharedPcos->rightJoin('pco_permissions','pco_permissions.pco_id','=','pcos.id')
                    ->leftJoin('pcos_subcontractors_recipients as recipient', function($leftJoin)
                    {
                        $leftJoin->on('recipient.pco_id', '=', 'pcos.id')->where('recipient.ab_recipient_id','!=',0);
                    })
                    ->leftJoin('pcos_versions','recipient.last_recipient_version','=','pcos_versions.id')
                    ->leftJoin('files', function($leftJoin)
                    {
                        $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                        WHERE pco_version_files.pco_version_id = pcos_versions.id
                        ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
                    })
                    ->leftJoin('address_book', 'recipient.ab_recipient_id', '=', 'address_book.id')
                    ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
                    ->where('pco_permissions.comp_child_id', '=', $this->companyId);
            }

            $responseData['pcoPermissionsType'] = $pcoPermissionsType;

            $responseData['sharedPcos'] = $sharedPcos->select('pcos.*',
                    'recipient.mf_number as mf_number',
                    'recipient.mf_title as mf_title',
                    'pcos_versions.id as version_id',
                    'pcos_versions.cycle_no as version_cycle_no',
                    'pcos_versions.sent_appr as version_sent_appr',
                    'pcos_versions.rec_appr as version_rec_appr',
                    'pcos_versions.status_id as version_status_id',
                    'pco_statuses.short_name as version_status_short_name',
                    'pco_statuses.name as version_status_name',
                    'address_book.name as subcontractor_name',
                    'files.file_name as file_name',
                    'files.id as file_id',
                    'files.version_date_connection as version_date_connection')
                ->with('company')
                ->with('subcontractors')
                ->with('subcontractors.ab_subcontractor')
                ->with('subcontractors.subcontractor_versions')
                ->with('subcontractors.subcontractor_versions.file')
                ->with('subcontractors.subcontractor_versions.transmittalSubmSentFile')
                ->with('recipient')
                ->with('recipient.recipient_versions')
                ->with('recipient.recipient_versions.file')
                ->with('recipient.recipient_versions.transmittalSentFile')
                ->with('recipient.ab_recipient')
                ->orderBy($sort, $order)
                ->paginate($paginate);
            return $responseData;
        }
        return null;

    }

    /**
     * Get all created pcos shares with different companies
     * @param $projectId
     * @param $companyType
     * @param $projectCreatorId
     * @param int $paginate
     * @return mixed
     */
    public function getSharedPcos($projectId, $companyType, $projectCreatorId, $paginate = 50)
    {
        //if the logged in company is a subcontractor on the specified project
        if ($companyType == Config::get('constants.company_type.subcontractor')) {
            //get the parent company that has shared this project to the logged in company
            $parentCompany = CustomHelper::getCompanyParentForProject($projectId);

            //check if the parent company has shared any submittals with the logged in company
            if (is_null($parentCompany)) {
                $parentId = 0;
            } else {
                $parentId = $parentCompany->comp_parent_id;
            }

            $creatorCompanyId = $projectCreatorId;
        } else {
            $parentId = $this->companyId;
            $creatorCompanyId = $this->companyId;
        }

        return Pco::where('pcos.proj_id', '=', $projectId)
            ->where('pcos.comp_id', '=', $creatorCompanyId)
            ->rightJoin('pco_permissions','pco_permissions.pco_id','=','pcos.id')
            ->where('pco_permissions.comp_parent_id','=',$parentId)
            ->join('companies','pco_permissions.comp_child_id','=','companies.id')
            ->leftJoin('pcos_subcontractors_recipients', function($leftJoin)
            {
                $leftJoin->on('pcos_subcontractors_recipients.pco_id', '=', 'pcos.id')->where('pcos_subcontractors_recipients.ab_recipient_id','!=',0);
            })
            ->leftJoin('pcos_versions','pcos_subcontractors_recipients.last_recipient_version','=','pcos_versions.id')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                WHERE pco_version_files.pco_version_id = pcos_versions.id
                ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'pcos_subcontractors_recipients.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
            ->select('pcos.*',
                'pcos_subcontractors_recipients.mf_number as mf_number',
                'pcos_subcontractors_recipients.mf_title as mf_title',
                'pcos_versions.id as version_id',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as version_sent_appr',
                'pcos_versions.rec_appr as version_rec_appr',
                'pcos_versions.status_id as version_status_id',
                'pco_permissions.comp_parent_id as comp_parent_id',
                'pco_permissions.comp_child_id as comp_child_id',
                'pco_statuses.short_name as version_status_short_name',
                'pco_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.version_date_connection as version_date_connection',
                'companies.name as company_name')
            ->with('company')
            ->with('subcontractors')
            ->with('subcontractors.ab_subcontractor')
            ->with('recipient')
            ->with('recipient.ab_recipient')
            ->paginate($paginate);
    }

    /**
     * Get received pcos
     * @param $projectId
     * @param $creatorCompanyId
     * @param $sort
     * @param $order
     * @param int $paginate
     * @return mixed
     */
    public function getReceivedPcos($projectId, $creatorCompanyId, $sort, $order, $paginate = 50)
    {
        return Pco::where('pcos.proj_id', '=', $projectId)
            ->where('pcos.comp_id', '!=', $creatorCompanyId)
            ->where('pcos.comp_id','!=',$this->companyId)
            ->rightJoin('pco_permissions','pco_permissions.pco_id','=','pcos.id')
            //->where('pco_permissions.comp_parent_id','=',$parentId)
            ->join('companies','pco_permissions.comp_parent_id','=','companies.id')
            ->leftJoin('pcos_subcontractors_recipients', function($leftJoin)
            {
                $leftJoin->on('pcos_subcontractors_recipients.pco_id', '=', 'pcos.id')->where('pcos_subcontractors_recipients.ab_recipient_id','!=',0);
            })
            ->leftJoin('pcos_versions','pcos_subcontractors_recipients.last_recipient_version','=','pcos_versions.id')
            ->leftJoin('files', function($leftJoin)
            {
                $leftJoin->on('files.id', '=', DB::raw('(select pco_version_files.file_id as file_id from pco_version_files
                WHERE pco_version_files.pco_version_id = pcos_versions.id
                ORDER BY pco_version_files.download_priority DESC LIMIT 1)'));
            })
            ->leftJoin('address_book', 'pcos_subcontractors_recipients.ab_recipient_id', '=', 'address_book.id')
            ->leftJoin('pco_statuses', 'status_id', '=', 'pco_statuses.id')
            ->select('pcos.*',
                'pcos_subcontractors_recipients.mf_number as mf_number',
                'pcos_subcontractors_recipients.mf_title as mf_title',
                'pcos_versions.id as version_id',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as version_sent_appr',
                'pcos_versions.rec_appr as version_rec_appr',
                'pcos_versions.status_id as version_status_id',
                'pco_permissions.comp_parent_id as comp_parent_id',
                'pco_permissions.comp_child_id as comp_child_id',
                'pco_statuses.short_name as version_status_short_name',
                'pco_statuses.name as version_status_name',
                'address_book.name as subcontractor_name',
                'files.file_name as file_name',
                'files.id as file_id',
                'files.size as size',
                'files.version_date_connection as version_date_connection',
                'companies.name as company_name')
            ->with('subcontractors')
            ->with('subcontractors.ab_subcontractor')
            ->with('recipient')
            ->with('recipient.ab_recipient')
            ->orderBy($sort, $order)
            ->paginate($paginate);
    }

    /**
     * Get pco status specified by id
     * @param $statusId
     * @return mixed
     */
    public function getStatusById($statusId)
    {
        return Pco_status::where('id','=',$statusId)->firstOrFail();
    }

    /**
     * Get concrete pco
     * @param $pcoId
     * @return mixed
     * @internal param $pcoId
     */
    public function getPco($pcoId)
    {
        return Pco::where('id','=',$pcoId)
            ->with([
                'subcontractors',
                'subcontractors.latest_subcontractor_version',
                'subcontractors.latest_subcontractor_version.file' => function($q){
                    $q->orderBy('download_priority', 'desc');
                },
                'subcontractors.subcontractor_versions',
                'subcontractors.subcontractor_versions.file',
                'subcontractors.subcontractor_versions.transmittalSubmSentFile'
            ])
            ->with('subcontractors.ab_subcontractor')
            ->with([
                'recipient',
                'recipient.latest_recipient_version',
                'recipient.latest_recipient_version.file' => function($q){
                    $q->orderBy('download_priority', 'desc');
                },
                'recipient.recipient_versions',
                'recipient.recipient_versions.file',
                'recipient.recipient_versions.transmittalSentFile',
            ])
            ->with('recipient.ab_recipient')
            ->with('general_contractor')
            ->with('company')
            ->firstOrFail();
    }

    /**
     * Get single shared pco
     * @param $projectId
     * @param $pcoId
     * @return mixed
     */
    public function getSharedPco($projectId, $pcoId)
    {
        $pcoType = Module::where('display_name','=','pcos')->first();

        //entity type 1 = modules
        $entityType = Config::get('constants.entity_type.module');

        //check companies permissions
        $pcosPermissions = Project_company_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        //check bids permissions
        $bidsPermissions = Proposal_supplier_permission::where('proj_id','=',$projectId)
            //->where('comp_parent_id','=',$project->comp_id)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->where('entity_type','=',$entityType)
            ->where('entity_id','=',$pcoType->id)
            ->first();

        $pcoPermissionsType = null;

        if ((!is_null($pcosPermissions) && $pcosPermissions->read == 1) || (!is_null($bidsPermissions) && $bidsPermissions->read == 1)) {
            $pcoPermissionsType = Config::get('constants.pco_permission_type.none');
            if (!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
            } elseif (!is_null($pcosPermissions) && $pcosPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) {
                $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
            } else {
                if (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) {
                    $pcoPermissionsType = Config::get('constants.pco_permission_type.all_pcos');
                } elseif (!is_null($bidsPermissions) && $bidsPermissions->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) {
                    $pcoPermissionsType = Config::get('constants.pco_permission_type.his_pcos');
                }
            }
        }

        $responseData['pcoPermissionsType'] = $pcoPermissionsType;

        $responseData['pco'] = Pco::where('id','=',$pcoId)
            ->with([
                'subcontractors',
                'subcontractors.latest_subcontractor_version',
                'subcontractors.latest_subcontractor_version.file' => function($q){
                    $q->orderBy('download_priority', 'desc');
                }
            ])
            ->with('subcontractors.ab_subcontractor')
            ->with([
                'subcontractors.subcontractor_versions',
                'subcontractors.subcontractor_versions.file',
            ])
            ->with([
                'recipient',
                'recipient.latest_recipient_version',
                'recipient.latest_recipient_version.file' => function($q){
                    $q->orderBy('download_priority', 'desc');
                }
            ])
            ->with('recipient.ab_recipient')
            ->with('general_contractor')
            ->with('company')
            ->firstOrFail();

        return $responseData;
    }

    /**
     * Get pco subcontractor
     * @param $pcoId
     * @param $pcoSubcontractorId
     * @return mixed
     */
    public function getPcoSubcontractorRecipient($pcoId, $pcoSubcontractorId)
    {
        return Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorId)
            ->where('pco_id','=',$pcoId)
            ->firstOrFail();
    }

    /**
     * Get pco subcontractor
     * @param $pcoId
     * @param $pcoSubcontractorId
     * @return mixed
     */
    public function getPcoSubcontractor($pcoId, $pcoSubcontractorId)
    {
        return Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorId)
            ->with('subcontractor_contact')
            ->where('pco_id','=',$pcoId)
            ->with('pco')
            ->with('pco.company')
            ->with('ab_subcontractor')
            ->with('ab_subcontractor.addresses')
            ->with('ab_subcontractor.addresses.ab_contacts')
            ->with('ab_subcontractor.contacts')
            ->with('subcontractor_versions')
            ->with('subcontractor_versions.status')
            //->with('subcontractor_versions.latest_file')
            ->with([
                'subcontractor_versions.file',
                'subcontractor_versions.transmittalSubmSentFile'
            ])
            ->firstOrFail();
    }

    /**
     * Get pco recipient
     * @param $pcoId
     * @param $pcoRecipientId
     * @return mixed
     */
    public function getPcoRecipient($pcoId, $pcoRecipientId)
    {
        return Pco_subcontractor_recipient::where('id','=',$pcoRecipientId)
            ->with('recipient_contact')
            ->where('pco_id','=',$pcoId)
            ->with('pco')
            ->with('ab_recipient')
            ->with('ab_recipient.addresses')
            ->with('ab_recipient.addresses.ab_contacts')
            ->with('ab_recipient.contacts')
            ->with('recipient_versions')
            //->with('recipient_versions.latest_file')
            ->with([
                'recipient_versions.file',
                'recipient_versions.transmittalSentFile'
            ])
            ->firstOrFail();
    }

    /**
     * Get specific pco version with all relations
     * @param $versionId
     * @return mixed
     */
    public function getPcoVersion($versionId)
    {
        return Pco_version::where('id','=',$versionId)
            ->with('subcontractor.ab_subcontractor')
            ->with('recipient.ab_recipient')
            ->with('pcoVersionApprUsers.users.company')
            ->with('pcoVersionApprUsers.abUsers.addressBook')
            ->with('transmittalSubmSentFile')
            ->with('transmittalSentFile')
            ->with('status')
            ->with('files')
            ->with('files.file')
            ->firstOrFail();
    }

    /**
     * Get subcontractor version for generation of pco transmittal
     * @param $versionId
     * @return mixed
     */
    public function getSubcontractorVersionForTransmittal($versionId)
    {
        return Pco_version::where('id','=',$versionId)
            ->with('subcontractor')
            ->with('subcontractor.pco')
            ->with('subcontractor.pco.recipient')
            ->with('subcontractor.pco.recipient.ab_recipient')
            ->with('subcontractor.pco.recipient.ab_recipient.contacts')
            ->with('subcontractor.pco.recipient.ab_recipient.contacts.office')
            ->with('subcontractor.pco.project')
            ->with('subcontractor.pco.project.company')
            ->with('subcontractor.ab_subcontractor')
            ->with('subcontractor.subcontractor_contact')
            ->with('subcontractor.subcontractor_contact.office')
            ->with('subcontractor.latest_subcontractor_version')
            ->with('files')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get recipient version for generation of pco transmittal
     * @param $versionId
     * @return mixed
     */
    public function getRecipientVersionForTransmittal($versionId)
    {
        return Pco_version::where('id','=',$versionId)
            ->with('recipient')
            ->with('recipient.pco')
            ->with('recipient.pco.subcontractors')
            ->with('recipient.pco.subcontractors.ab_subcontractor')
            ->with('recipient.pco.subcontractors.ab_subcontractor.contacts')
            ->with('recipient.pco.subcontractors.ab_subcontractor.contacts.office')
            ->with('recipient.pco.project')
            ->with('recipient.pco.project.company')
            ->with('recipient.ab_recipient')
            ->with('recipient.recipient_contact')
            ->with('recipient.recipient_contact.office')
            ->with('recipient.latest_recipient_version')
            ->with('files')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get pco version for generation of pco transmittal
     * @param $versionId
     * @return mixed
     */
    public function getPcoVersionForTransmittal($versionId)
    {
        return Pco_version::where('id','=',$versionId)
            ->with('subcontractor')
            ->with('subcontractor.pco')
            ->with('subcontractor.pco.project')
            ->with('subcontractor.ab_subcontractor')
            ->with('subcontractor.ab_subcontractor.contacts')
            ->with('subcontractor.ab_subcontractor.contacts.office')
            ->with('recipient')
            ->with('recipient.pco')
            ->with('recipient.pco.project')
            ->with('recipient.pco.subcontractors')
            ->with('recipient.ab_recipient')
            ->with('recipient.ab_recipient.contacts')
            ->with('recipient.ab_recipient.contacts.office')
            ->with('files')
            ->with('status')
            ->firstOrFail();
    }

    /**
     * Get Subcontractor emails selected in the companies module
     * needed for transmittal email notifications
     * @param $versionId
     * @return mixed
     */
    public function getPcoSubcontractorCompanyEmails($versionId, $contactIds)
    {
        return Pco_version::join('pcos_subcontractors_recipients', 'pcos_subcontractors_recipients.id', '=', 'pcos_versions.subcontractor_id')
            ->join('pcos', 'pcos.id', '=', 'pcos_subcontractors_recipients.pco_id')
            ->join('projects', 'projects.id', '=', 'pcos.proj_id')
            ->join('project_company_contacts', 'project_company_contacts.proj_id', '=', 'projects.id')
            ->leftJoin('project_subcontractors', function($join)
            {
                $join->on('project_company_contacts.proj_comp_id', '=', 'project_subcontractors.proj_comp_id')
                    ->where('project_subcontractors.once_accepted','=',Config::get('constants.projects.once_accepted.yes'))
                    ->where('project_subcontractors.status','=',Config::get('constants.projects.shared_status.accepted'));
            })
            ->join('ab_contacts', 'project_company_contacts.ab_cont_id', '=', 'ab_contacts.id')
            ->leftJoin('address_book', 'ab_contacts.ab_id', '=', 'address_book.id')
            ->where('pcos_versions.id', '=', $versionId)
            ->whereIn('ab_contacts.id', $contactIds)
            ->select(
                'ab_contacts.*',
                'pcos.name as pco_name',
                'project_subcontractors.comp_child_id as comp_child_id',
                'projects.name as project_name',
                'projects.id as project_id',
                'address_book.name as company_name'
            )
            ->groupBy(['ab_contacts.id'])
            ->get();
    }

    /**
     * Get User emails from my company
     * @param $versionId
     * @return mixed
     */
    public function getCompanyUserSubcontractorEmails($versionId, $userIds)
    {
        return User::whereIn('id', $userIds)
            ->selectRaw('users.email, users.name, 
                    (select pcos.name from pcos, pcos_versions, pcos_subcontractors_recipients 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and pcos_subcontractors_recipients.id = pcos_versions.subcontractor_id
                        and pcos_versions.id = '.$versionId.') as pco_name, 
                    (select projects.name from pcos, pcos_versions, pcos_subcontractors_recipients, projects 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and projects.id = pcos.proj_id
                        and pcos_subcontractors_recipients.id = pcos_versions.subcontractor_id
                        and pcos_versions.id = '.$versionId.') as project_name, 
                    (select projects.id from pcos, pcos_versions, pcos_subcontractors_recipients, projects 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and projects.id = pcos.proj_id
                        and pcos_subcontractors_recipients.id = pcos_versions.subcontractor_id
                        and pcos_versions.id = '.$versionId.') as project_id')
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->get();
    }

    /**
     * Get Recipient emails selected in the companies module
     * needed for transmittal email notifications
     * @param $id
     * @param $projectId
     * @param $recSubId
     * @return mixed
     */
    public function getPcoRecipientCompanyEmails($versionId, $contactIds)
    {
        return Pco_version::join('pcos_subcontractors_recipients', 'pcos_subcontractors_recipients.id', '=', 'pcos_versions.recipient_id')
            ->join('pcos', 'pcos.id', '=', 'pcos_subcontractors_recipients.pco_id')
            ->join('projects', 'projects.id', '=', 'pcos.proj_id')
            ->join('project_company_contacts', 'project_company_contacts.proj_id', '=', 'projects.id')
            ->leftJoin('project_subcontractors', function($join)
            {
                $join->on('project_company_contacts.proj_comp_id', '=', 'project_subcontractors.proj_comp_id')
                    ->where('project_subcontractors.once_accepted','=',Config::get('constants.projects.once_accepted.yes'))
                    ->where('project_subcontractors.status','=',Config::get('constants.projects.shared_status.accepted'));
            })
            ->join('ab_contacts', 'project_company_contacts.ab_cont_id', '=', 'ab_contacts.id')
            ->leftJoin('address_book', 'ab_contacts.ab_id', '=', 'address_book.id')
            ->where('pcos_versions.id', '=', $versionId)
            ->whereIn('ab_contacts.id', $contactIds)
            ->select(
                'ab_contacts.*',
                'pcos.name as pco_name',
                'project_subcontractors.comp_child_id as comp_child_id',
                'projects.name as project_name',
                'projects.id as project_id',
                'address_book.name as company_name'
            )
            ->groupBy(['ab_contacts.id'])
            ->get();
    }

    /**
     * Get User emails from my company
     * @param $versionId
     * @return mixed
     */
    public function getCompanyUserRecipientEmails($versionId, $userIds)
    {
        return User::whereIn('id', $userIds)
            ->selectRaw('users.email, users.name, 
                    (select pcos.name from pcos, pcos_versions, pcos_subcontractors_recipients 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and pcos_subcontractors_recipients.id = pcos_versions.recipient_id
                        and pcos_versions.id = '.$versionId.') as pco_name, 
                    (select projects.name from pcos, pcos_versions, pcos_subcontractors_recipients, projects 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and projects.id = pcos.proj_id
                        and pcos_subcontractors_recipients.id = pcos_versions.recipient_id
                        and pcos_versions.id = '.$versionId.') as project_name, 
                    (select projects.id from pcos, pcos_versions, pcos_subcontractors_recipients, projects 
                    where pcos.id = pcos_subcontractors_recipients.pco_id 
                        and projects.id = pcos.proj_id
                        and pcos_subcontractors_recipients.id = pcos_versions.recipient_id
                        and pcos_versions.id = '.$versionId.') as project_id')
            ->where('comp_id', '=', Auth::user()->comp_id)
            ->get();
    }

    /**
     * Get pco transmittals
     * @param $projectId
     * @param $sort
     * @param $order
     * @return mixed
     */
    public function getTransmittals($projectId, $sort, $order)
    {
        $fileType = File_type::where('display_name','=','pcos')->firstOrFail();

        return Transmittal::where('file_type_id','=',$fileType->id)
            ->where('transmittals_logs.proj_id','=',$projectId)
            ->join('pcos_versions','pcos_versions.id','=','transmittals_logs.version_id')
            ->join('pco_statuses','pco_statuses.id','=','pcos_versions.status_id')
            ->join('pcos_subcontractors_recipients', function($join)
            {
                $join->on('pcos_subcontractors_recipients.id', '=', DB::raw('IF(pcos_versions.subcontractor_id = 0, pcos_versions.recipient_id, pcos_versions.subcontractor_id)'));
            })
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(pcos_subcontractors_recipients.ab_subcontractor_id = 0, pcos_subcontractors_recipients.ab_recipient_id, pcos_subcontractors_recipients.ab_subcontractor_id)'));
            })
            ->join('pcos','pcos.id','=','pcos_subcontractors_recipients.pco_id')
            ->select('transmittals_logs.*',
                'pcos.number as pco_no',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as sent_appr',
                'pcos_versions.subm_sent_sub as subm_sent_sub',
                'pcos.subject as subject',
                'pcos_subcontractors_recipients.mf_number as mf_number',
                'pcos_subcontractors_recipients.mf_title as mf_title',
                'pcos_subcontractors_recipients.self_performed as self_performed',
                'address_book.name as sent_to',
                'pco_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN pcos_versions.subm_sent_sub ELSE pcos_versions.sent_appr END) AS date_sent')
            )
            ->with('file')
            ->orderBy($sort, $order)
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Filter pco transmittals
     * @param $params
     * @return mixed
     */
    public function filterPcoTransmittals($params)
    {
        $fileType = File_type::where('display_name','=','pcos')->firstOrFail();

        $status = $params['status'];
        //Create query conditions
        $query = [
            'file_type_id' => $fileType->id,
            'transmittals_logs.proj_id' => $params['projectId'],
            'transmittals_logs.comp_id' => $this->companyId
        ];

        //set sorting
        if (isset($params['sort'])) {
            if ($params['sort'] != '') {
                $sort = $params['sort'];
            } else {
                $sort = 'created_at';
            }
        } else {
            $sort = 'created_at';
        }

        //set sorting order
        if (isset($params['order'])) {
            if ($params['order'] != '') {
                $order = $params['order'];
            } else {
                $order = 'asc';
            }
        } else {
            $order = 'asc';
        }

        if($params['sentTo'] != '') {
            $query['address_book.id'] = $params['sentTo'];
        }

        $fullQuery = Transmittal::where($query);

        //master format search field transformation in a proper search segment
        if (is_numeric(substr($params['masterFormat'], 0, 1))) {
            $masterFormatPieces = explode(" - ", $params['masterFormat']);
            $fullQuery->where('mf_number','LIKE',$masterFormatPieces[0].'%');
        } else {
            $fullQuery->where('mf_title','LIKE', $params['masterFormat'].'%');
        }

        //Get pco transmittals by status
        if ($status == 'All') {
            $fullQuery->join('pcos_versions','pcos_versions.id','=','transmittals_logs.version_id');
        } else if ($status == 'all_open') {
            $allOpenStatuses = Pco_status::whereIn('short_name', Config::get('constants.all_open'))
                ->lists('id');

            if (count($allOpenStatuses)) {
                $fullQuery->rightJoin('pcos_versions', function($join) use ($allOpenStatuses)
                {
                    $join->on(DB::raw("pcos_versions.id = transmittals_logs.version_id and pcos_versions.status_id IN (".implode(',', array_map('intval', $allOpenStatuses)).")"), DB::raw(''), DB::raw(''));
                });
            }
        } else {
            $fullQuery->where('pco_statuses.id','=',$status)
                ->join('pcos_versions','pcos_versions.id','=','transmittals_logs.version_id');
        }

        return $fullQuery->join('pco_statuses','pco_statuses.id','=','pcos_versions.status_id')
            ->join('pcos_subcontractors_recipients', function($join)
            {
                $join->on('pcos_subcontractors_recipients.id', '=', DB::raw('IF(pcos_versions.subcontractor_id = 0, pcos_versions.recipient_id, pcos_versions.subcontractor_id)'));
            })
            ->leftJoin('address_book', function($leftJoin)
            {
                $leftJoin->on('address_book.id', '=', DB::raw('IF(pcos_subcontractors_recipients.ab_subcontractor_id = 0, pcos_subcontractors_recipients.ab_recipient_id, pcos_subcontractors_recipients.ab_subcontractor_id)'));
            })
            ->join('pcos','pcos.id','=','pcos_subcontractors_recipients.pco_id')
            ->select('transmittals_logs.*',
                'pcos.number as pco_no',
                'pcos_versions.cycle_no as version_cycle_no',
                'pcos_versions.sent_appr as sent_appr',
                'pcos_versions.subm_sent_sub as subm_sent_sub',
                'pcos.subject as subject',
                'pcos_subcontractors_recipients.mf_number as mf_number',
                'pcos_subcontractors_recipients.mf_title as mf_title',
                'pcos_subcontractors_recipients.self_performed as self_performed',
                'address_book.name as sent_to',
                'pco_statuses.short_name as version_status',
                DB::raw('(CASE WHEN transmittals_logs.date_type = "'.Config::get('constants.transmittal_date_type.subm_sent_sub').'" THEN pcos_versions.subm_sent_sub ELSE pcos_versions.sent_appr END) AS date_sent')
            )
            ->orderBy($sort, $order)
            ->with('file')
            ->paginate(Config::get('paginations.transmittals'));
    }

    /**
     * Delete transmittal file from AWS S3
     * @param $projectId
     * @param $fileId
     * @return bool
     */
    public function deleteTransmittalFromS3($projectId, $fileId)
    {
        //get file
        $file = Project_file::where('id','=',$fileId)->firstOrFail();

        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/pcos/transmittals/'.$file->file_name;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Get transmittal by id
     * @param $transmittalId
     * @return mixed
     */
    public function getTransmittal($transmittalId)
    {
        return Transmittal::where('id','=',$transmittalId)
            ->with('file')
            ->firstOrFail();
    }

    /**
     * Delete transmittal by id
     * @param $transmittalId
     * @return mixed
     */
    public function deleteTransmittal($transmittalId)
    {
        $transmittal = Transmittal::where('id','=',$transmittalId)->firstOrFail();

        Project_file::where('id','=',$transmittal->file_id)->delete();

        return $transmittal->delete();
    }

    /**
     * Get all pcos versions statuses from database table
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllPcoStatuses()
    {
        return Pco_status::all();
    }

    /**
     * Get pco transmittals report
     * @param $params
     * @return mixed
     */
    public function PcoTransmittalsReport($params)
    {
        $fileType = File_type::where('display_name','=','pcos')->firstOrFail();

        return Transmittal::where('proj_id','=',$params['projectId'])
            ->where('file_type_id','=',$fileType->id)
            ->with('file')
            ->with('pco_version')
            ->with('pco_version.status')
            ->with('pco_version.recipient')
            ->with('pco_version.recipient.pco')
            ->with('pco_version.recipient.ab_recipient')
            ->with('pco_version.subcontractor')
            ->with('pco_version.subcontractor.pco')
            ->with('pco_version.subcontractor.ab_subcontractor')
            ->orderBy('id','DESC')
            ->get();
    }

    /**
     * Store new pco
     * @param $projectId
     * @param $data
     * @return mixed
     */
    public function storeProjectPco($projectId, $data)
    {
        $project = Project::where('id', '=', $projectId)->first();

        if (str_pad(($project->pco_counter+1), 3, '0', STR_PAD_LEFT) == $data['number']) {
            $project->pco_counter++;
            $project->save();
        }

        $pco = Pco::create([
            'comp_id' => $this->companyId,
            'user_id' => $this->userId,
            'name' => $data['name'],
            'number' => $data['number'],
            'subject' => $data['subject'],
            'sent_via' => (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'],
            'gc_id' => !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0,
            'make_me_gc' => isset($data['make_me_gc']) ? 1 : 0,
            'proj_id' => $projectId,
            'reason_for_change' => (!empty($data['reason_for_change_dropdown']))?$data['reason_for_change_dropdown']:$data['reason_for_change'],
            'description_of_change' => $data['description_of_change'],
            'recipient_id' => !empty($data['recipient_id']) ? $data['recipient_id'] : 0,
            'recipient_contact_id' => isset($data['recipient_contact']) ? $data['recipient_contact'] : 0,
        ]);

        if ($pco && $pco->recipient_id) {
            $data['recipient_id'] = $pco->recipient_id;
            $data['recipient_contact'] = $pco->recipient_contact_id;
            $data['note'] = '';

            $this->storePcoRecipient($pco->id, $data);
        }


        return $pco;
    }

    /**
     * Store new pco Subcontractor
     * @param $pcoId
     * @param $data
     * @return static
     */
    public function storePcoSubcontractor($pcoId, $data)
    {
        return Pco_subcontractor_recipient::create([
            'pco_id' => $pcoId,
            'pco_name' => $data['name'],
            'type' => Config::get('constants.pco_subcontractor_or_recipient.subcontractor'),
            'self_performed' => empty($data['self_performed']) ? 0 : 1,
            'ab_subcontractor_id' => empty($data['self_performed']) ? $data['sub_id'] : 0,
            //'sub_office_id' => empty($data['self_performed']) ? (isset($data['sub_office']) ? $data['sub_office'] : 0) : 0,
            'sub_contact_id' => empty($data['self_performed']) ? (isset($data['sub_contact']) ? $data['sub_contact'] : 0) : 0,
            'ab_recipient_id' => 0,
            'mf_number' => $data['master_format_number'],
            'mf_title' => $data['master_format_title'],
            'note' => $data['note'],
            'send_notif' => !empty($data['send_notif_subcontractor']) ? 1 : 0
        ]);
    }

    /**
     * Store new pco Recipient
     * @param $pcoId
     * @param $data
     * @return static
     */
    public function storePcoRecipient($pcoId, $data)
    {
        return Pco_subcontractor_recipient::create([
            'pco_id' => $pcoId,
            'type' => Config::get('constants.pco_subcontractor_or_recipient.recipient'),
            'ab_subcontractor_id' => 0,
            'ab_recipient_id' => $data['recipient_id'],
            //'recipient_office_id' => isset($data['recipient_office']) ? $data['recipient_office'] : 0,
            'recipient_contact_id' => isset($data['recipient_contact']) ? $data['recipient_contact'] : 0,
            'mf_number' => '',
            'mf_title' => '',
            'note' => $data['note'],
            'send_notif' => !empty($data['send_notif_recipient']) ? 1 : 0
        ]);
    }

    /**
     * Store new pco subcontractor version
     * @param $pcoSubcontractorRecipientId
     * @param $data
     * @return static
     */
    public function storePcoSubcontractorVersion($pcoSubcontractorRecipientId, $data)
    {
        $newVersion = Pco_version::create([
            'subcontractor_id' => $pcoSubcontractorRecipientId,
            'recipient_id' => 0,
            'cycle_no' => $data['cycle_no'],
            'cost' => !empty($data['cost']) ? CustomHelper::amountToDouble($data['cost']) : 0,
            'rec_sub' => empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d'),
            'subm_sent_sub' => empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d'),
            'status_id' => $data['status']
        ]);

        $pcoVersionFile = new Pco_version_file();

        //connect pco version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $pcoVersionFile::create([
                'pco_version_id' => $newVersion->id,
                'file_id' => $data['rec_sub_file_id'],
                'download_priority' => 1,
            ]);
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $pcoVersionFile::create([
                'pco_version_id' => $newVersion->id,
                'file_id' => $data['subm_sent_sub_file_id'],
                'download_priority' => 4,
            ]);
        }

        $lastRecipientVersion = Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorRecipientId)->firstOrFail();
        $lastRecipientVersion->last_recipient_version = $newVersion->id;

        if ($data['useCustom'] != 'true') {
            $lastRecipientVersion->cycle_counter++;
        }

        $lastRecipientVersion->save();

        return $newVersion;
    }

    /**
     * Store new pco recipient version
     * @param $pcoSubcontractorRecipientId
     * @param $data
     * @return static
     */
    public function storePcoRecipientVersion($pcoSubcontractorRecipientId, $data)
    {
        $newVersion = Pco_version::create([
            'subcontractor_id' => 0,
            'recipient_id' => $pcoSubcontractorRecipientId,
            'cycle_no' => $data['cycle_no'],
            'cost' => !empty($data['cost']) ? CustomHelper::amountToDouble($data['cost']) : 0,
            'sent_appr' => empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d'),
            'rec_appr' => empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d'),
            'status_id' => $data['status']
        ]);

        $pcoVersionFile = new Pco_version_file();

        //connect pco version with the uploaded files if there are any
        if ($data['sent_appr_file_id'] != '') {
            $pcoVersionFile::create([
                'pco_version_id' => $newVersion->id,
                'file_id' => $data['sent_appr_file_id'],
                'download_priority' => 2,
            ]);
        }

        if ($data['rec_appr_file_id'] != '') {
            $pcoVersionFile::create([
                'pco_version_id' => $newVersion->id,
                'file_id' => $data['rec_appr_file_id'],
                'download_priority' => 3,
            ]);
        }

        $lastRecipientVersion = Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorRecipientId)->firstOrFail();
        $lastRecipientVersion->last_recipient_version = $newVersion->id;

        if ($data['useCustom'] != 'true') {
            $lastRecipientVersion->cycle_counter++;
        }

        $lastRecipientVersion->save();

        return $newVersion;
    }

    /**
     * Update specific pco
     * @param $pcoId
     * @param $data
     * @return mixed
     */
    public function updatePco($pcoId, $data)
    {
        $pco = Pco::where('id','=',$pcoId)->firstOrFail();
        $pco->name = $data['name'];
        $pco->number = $data['number'];
        $pco->subject = $data['subject'];
        $pco->sent_via = (!empty($data['sent_via_dropdown']))?$data['sent_via_dropdown']:$data['sent_via'];
        $pco->gc_id = !empty($data['general_contractor_id']) ? $data['general_contractor_id'] : 0;
        $pco->make_me_gc = isset($data['make_me_gc']) ? 1 : 0;
        $pco->reason_for_change = (!empty($data['reason_for_change_dropdown']))?$data['reason_for_change_dropdown']:$data['reason_for_change'];
        $pco->description_of_change = $data['description_of_change'];
        $pco->save();
        return $pco;
    }

    /**
     * Update pco subcontractor
     * @param $pcoId
     * @param $pcoSubcontractorId
     * @param $data
     * @return mixed
     */
    public function updatePcoSubcontractor($pcoId, $pcoSubcontractorId, $data)
    {
        $pcoSubcontractor = Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorId)
            ->where('pco_id','=',$pcoId)
            ->firstOrFail();
        $pcoSubcontractor->pco_name = $data['name'];
        $pcoSubcontractor->self_performed = empty($data['self_performed']) ? 0 : 1;
        $pcoSubcontractor->ab_subcontractor_id = empty($data['self_performed']) ? $data['sub_id'] : 0;
        //$pcoSubcontractor->sub_office_id = empty($data['self_performed']) ? (isset($data['sub_office']) ? $data['sub_office'] : 0) : 0;
        $pcoSubcontractor->sub_contact_id = empty($data['self_performed']) ? (isset($data['sub_contact']) ? $data['sub_contact'] : 0) : 0;
        $pcoSubcontractor->mf_number = $data['master_format_number'];
        $pcoSubcontractor->mf_title = $data['master_format_title'];
        $pcoSubcontractor->note = $data['note'];
        $pcoSubcontractor->send_notif = !empty($data['send_notif_subcontractor']) ? 1 : 0;
        $pcoSubcontractor->save();
        return $pcoSubcontractor;
    }

    /**
     * Update pco recipient
     * @param $pcoId
     * @param $pcoRecipientId
     * @param $data
     * @return mixed
     */
    public function updatePcoRecipient($pcoId, $pcoRecipientId, $data)
    {
        $pcoRecipient = Pco_subcontractor_recipient::where('id','=',$pcoRecipientId)
            ->where('pco_id','=',$pcoId)
            ->firstOrFail();
        $pcoRecipient->ab_recipient_id = $data['recipient_id'];
        $pcoRecipient->recipient_contact_id = isset($data['recipient_contact']) ? $data['recipient_contact'] : 0;
        //$pcoRecipient->recipient_office_id = isset($data['recipient_office']) ? $data['recipient_office'] : 0;
        $pcoRecipient->mf_number = '';
        $pcoRecipient->mf_title = '';
        $pcoRecipient->note = $data['note'];
        $pcoRecipient->send_notif = !empty($data['send_notif_recipient']) ? 1 : 0;
        $pcoRecipient->save();
        return $pcoRecipient;
    }

    /**
     * Update specific pco subcontractor version
     * @param $pcoSubcontractorId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updatePcoSubcontractorVersion($pcoSubcontractorId, $versionId, $data)
    {
        $pcoVersion = Pco_version::where('id','=',$versionId)->firstOrFail();
        $pcoVersion->cycle_no = $data['cycle_no'];
        $pcoVersion->subm_sent_sub = empty($data['subm_sent_sub']) ? '' : Carbon::parse($data['subm_sent_sub'])->format('Y-m-d');
        $pcoVersion->rec_sub = empty($data['rec_sub']) ? '' : Carbon::parse($data['rec_sub'])->format('Y-m-d');
        $pcoVersion->status_id = $data['status'];
        $pcoVersion->notes = null;
        $pcoVersion->cost = !empty($data['cost']) ? CustomHelper::amountToDouble($data['cost']) : 0;
        $pcoVersion->save();

        $pcoVersionFile = new Pco_version_file();

        //connect pco version with the uploaded files if there are any
        if ($data['rec_sub_file_id'] != '') {
            $currentVersionFile = $pcoVersionFile->where('pco_version_id','=',$pcoVersion->id)
                ->where('file_id','=',$data['rec_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 1
                ]);
            } else {
                $pcoVersionFile::create([
                    'pco_version_id' => $pcoVersion->id,
                    'file_id' => $data['rec_sub_file_id'],
                    'download_priority' => 1,
                ]);
            }
        }

        if ($data['subm_sent_sub_file_id'] != '') {
            $currentVersionFile = $pcoVersionFile->where('pco_version_id','=',$pcoVersion->id)
                ->where('file_id','=',$data['subm_sent_sub_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 4
                ]);
            } else {
                $pcoVersionFile::create([
                    'pco_version_id' => $pcoVersion->id,
                    'file_id' => $data['subm_sent_sub_file_id'],
                    'download_priority' => 4,
                ]);
            }
        }

        return $pcoVersion;
    }

    /**
     * Update specific pco subcontractor version
     * @param $pcoSubcontractorId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updatePcoSubcontractorVersionTransmittalNotes($versionId, $data)
    {
        $pcoVersion = Pco_version::where('id','=',$versionId)->firstOrFail();
        $pcoVersion->notes = $data['version_note'];
        $pcoVersion->save();

        return $pcoVersion;
    }

    /**
     * Update specific pco recipient version
     * @param $pcoRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updatePcoRecipientVersion($pcoRecipientId, $versionId, $data)
    {
        $pcoVersion = Pco_version::where('id','=',$versionId)->firstOrFail();
        $pcoVersion->cycle_no = $data['cycle_no'];
        $pcoVersion->sent_appr = empty($data['sent_appr']) ? '' : Carbon::parse($data['sent_appr'])->format('Y-m-d');
        $pcoVersion->rec_appr = empty($data['rec_appr']) ? '' : Carbon::parse($data['rec_appr'])->format('Y-m-d');
        $pcoVersion->status_id = $data['status'];
        $pcoVersion->cost = !empty($data['cost']) ? CustomHelper::amountToDouble($data['cost']) : 0;
        $pcoVersion->save();

        $pcoVersionFile = new Pco_version_file();

        //connect pco version with the uploaded files if there are any
        if ($data['sent_appr_file_id'] != '') {
            $currentVersionFile = $pcoVersionFile->where('pco_version_id','=',$pcoVersion->id)
                ->where('file_id','=',$data['sent_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 2
                ]);
            } else {
                $pcoVersionFile::create([
                    'pco_version_id' => $pcoVersion->id,
                    'file_id' => $data['sent_appr_file_id'],
                    'download_priority' => 2,
                ]);
            }
        }

        if ($data['rec_appr_file_id'] != '') {
            $currentVersionFile = $pcoVersionFile->where('pco_version_id','=',$pcoVersion->id)
                ->where('file_id','=',$data['rec_appr_file_id'])
                ->first();
            if (!is_null($currentVersionFile)) {
                $currentVersionFile->update([
                    'download_priority' => 3
                ]);
            } else {
                $pcoVersionFile::create([
                    'pco_version_id' => $pcoVersion->id,
                    'file_id' => $data['rec_appr_file_id'],
                    'download_priority' => 3,
                ]);
            }
        }

        return $pcoVersion;
    }

    /**
     * Update specific pco recipient version
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updatePcoRecipientVersionTransmittalNotes($versionId, $data)
    {
        $pcoVersion = Pco_version::where('id','=',$versionId)->firstOrFail();
        $pcoVersion->notes = $data['version_note'];
        $pcoVersion->save();

        return $pcoVersion;
    }

    /**
     * Returns contacts from pco version
     *
     * @param $versionId
     * @return mixed
     */
    public function getContactsForPcoVersion($versionId, $type)
    {
        return Pco_version_distribution::where('pco_version_id', '=', $versionId)
            ->where('type', '=', $type)
            ->get();
    }

    /**
     * Delete pco file from S3
     * @param $projectId
     * @param $filename
     * @return bool
     */
    public function deletePcoFile($projectId, $filename)
    {
        $disk = Storage::disk('s3');
        $filePath = 'company_'.$this->companyId.'/project_'.$projectId.'/pcos/'.$filename;
        if($disk->exists($filePath))
        {
            return $disk->delete($filePath);
        }

        return false;
    }

    /**
     * Delete specific pco version with all its files
     * Delete pco version with all its files
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @return mixed
     */
    public function deletePcoVersion($versionType, $pcoSubcontractorRecipientId, $versionId)
    {
        //get specified version
        $pcoVersion =  Pco_version::where('id','=',$versionId)->firstOrFail();

        //change last version before delete
        if ($versionType == Config::get('constants.pco_version_type.recipient')) {
            $getLastVersion = Pco_version::where('recipient_id','=',$pcoSubcontractorRecipientId)->orderBy('id','DESC')->take(1)->skip(1)->firstOrFail();
            Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorRecipientId)->update([
                'last_recipient_version' => $getLastVersion->id
            ]);
        }

        //get all connected files
        $pcoVersionFiles = Pco_version_file::where('pco_version_id','=',$pcoVersion->id)->get();

        if (count($pcoVersionFiles)) {
            foreach ($pcoVersionFiles as $pcoVersionFile) {
                //delete files from table pco_version_files and from table files (cascade delete)
                $pcoVersionFile->delete();
            }
        }
        //delete the version
        return $pcoVersion->delete();
    }

    /**
     * Delete specific pco subcontractor with all its versions and files
     * @param $pcoSubcontractorId
     * @return mixed
     */
    public function deletePcoSubcontractor($pcoSubcontractorId)
    {
        //get pco subcontractor
        $pcoSubcontractor = Pco_subcontractor_recipient::where('id','=',$pcoSubcontractorId)->firstOrFail();

        //get pco versions for the selected subcontractor
        $pcoVersions =  Pco_version::where('subcontractor_id','=',$pcoSubcontractor->id)->get();

        if (count($pcoVersions)) {
            foreach ($pcoVersions as $version) {
                //get all files for a specific version
                $pcoVersionFiles = Pco_version_file::where('pco_version_id','=',$version->id)->get();

                if (count($pcoVersionFiles)) {
                    foreach ($pcoVersionFiles as $pcoVersionFile) {
                        //delete files from table pco_version_files and from table files (cascade delete)
                        $pcoVersionFile->delete();
                    }
                }
                //delete version
                $version->delete();
            }
        }
        //delete pco subcontractor
        return $pcoSubcontractor->delete();
    }

    /**
     * Delete full pco with pco subcontractors, recipient and all their versions and files
     * @param $pcoId
     * @return mixed
     */
    public function deletePco($pcoId)
    {
        //get the specific pco
        $pco = Pco::where('id','=',$pcoId)->firstOrFail();

        //get all pco subcontractors and recipient if there are any
        $pcoSubcontractorsRecipient = Pco_subcontractor_recipient::where('pco_id','=',$pco->id)->get();
        if (count($pcoSubcontractorsRecipient)) {
            foreach ($pcoSubcontractorsRecipient as $subcontractorRecipient) {
                //get all versions for the subcontractors and the recipient
                $versions = Pco_version::where('subcontractor_id','=',$subcontractorRecipient->id)->get();

                if (count($versions)) {
                    foreach ($versions as $version) {
                        //get all version files
                        $versionFiles = Pco_version_file::where('pco_version_id','=',$version->id)->get();

                        if (count($versionFiles)) {
                            foreach ($versionFiles as $file) {
                                //delete files from table pco_version_files and from table files (cascade delete)
                                $file->delete();
                            }
                        }
                        //delete version
                        $version->delete();
                    }
                }
                //delete subcontractor or recipient
                $subcontractorRecipient->delete();
            }
        }
        //delete pco
        return $pco->delete();
    }

    /**
     * Share pcos with all subcontractors for a specific project
     * @param $projectId
     * @param $selectedPcos
     * @return bool
     */
    public function shareWithAllSubcontractors($projectId, $selectedPcos)
    {
        //get all subcontractors for the project
        $allProjectSubcontractors = Project_subcontractor::where('proj_id','=',$projectId)
            ->where('comp_parent_id','=',$this->companyId)
            ->get();

        if(count($allProjectSubcontractors)) {
            foreach($selectedPcos as $pco) {
                foreach($allProjectSubcontractors as $subcontractor) {
                    Pco_permission::firstOrCreate([
                        'pco_id' => $pco,
                        'comp_child_id' => $subcontractor->comp_child_id,
                        'comp_parent_id' => $this->companyId
                    ]);
                }
            }

            return true;
        }
        return false;
    }

    /**
     * Share selected files with selected subcontractors or with
     * the general contractor
     * @param $selectedPcos
     * @param $selectedSubcontractors
     * @return bool
     * @internal param $selectedFiles
     */
    public function shareWithSelectedSubcontractors($selectedPcos, $selectedSubcontractors)
    {
        foreach($selectedPcos as $pco) {
            foreach($selectedSubcontractors as $subcontractor) {
                Pco_permission::firstOrCreate([
                    'pco_id' => $pco,
                    'comp_child_id' => $subcontractor,
                    'comp_parent_id' => $this->companyId
                ]);
            }
        }

        return true;
    }

    /**
     * Unshare all pcos shares from second level of sharing and further possible shares
     * @param $pcoId
     * @param $shareSubcontractorId
     * @return bool
     * @internal param $submittalId
     * @internal param $fileId
     */
    public function unshareNextLevels($pcoId, $shareSubcontractorId)
    {
        $nextLevelUnshare = Pco_permission::where('pco_id','=',$pcoId)
            ->where('comp_parent_id','=',$shareSubcontractorId)
            ->get();
        if(count($nextLevelUnshare) > 0) {
            foreach($nextLevelUnshare as $unshareFile) {
                $nextShareCompanyId = $unshareFile->comp_child_id;
                $unshareFile->delete();

                //recursion until there are still shares of the specified pco
                $this->unshareNextLevels($pcoId, $nextShareCompanyId);
            }
        }
        return true;
    }

    /**
     * Unshare specified project pco with specified company (first level of sharing)
     * @param $pcoId
     * @param $shareCompanyId
     * @param $receiveCompanyId
     * @return bool
     * @internal param $submittalId
     * @internal param $fileId
     */
    public function unsharePco($pcoId, $shareCompanyId, $receiveCompanyId)
    {
        $sharingStep = Pco_permission::where('pco_id','=',$pcoId)
            ->where('comp_parent_id','=',$shareCompanyId)
            ->where('comp_child_id','=',$receiveCompanyId)
            ->firstOrFail();

        //do the next levels of unsharing
        $nextLevelUnshare = $this->unshareNextLevels($pcoId, $sharingStep->comp_child_id);

        if($nextLevelUnshare) {
            $sharingStep->delete();
            return true;
        }
        return false;
    }

    /**
     * Delete specific PCO file
     * @param $params
     * @return mixed
     */
    public function deleteSinglePcoFile($params)
    {
        Pco_version_file::where('file_id','=',$params['fileId'])
            ->delete();

        $result = Project_file::where('id','=',$params['fileId'])
            ->delete();

        //if storage limit bellow 90% reset notification
        $company = $this->companyRepo->getCompany(Auth::user()->comp_id);
        $filesUsedStorage = $this->filesRepo->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $this->filesRepo->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $this->filesRepo->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;

        if ($company->storage_notification == 1 && $currentStorage < ((float)$company->subscription_type->storage_limit*0.9)) {
            $this->companyRepo->updateCompanyMassAssign($company->id,['storage_notification' => 0]);
        }

        return $result;
    }

    public function markTransmittalApprSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $pco = Pco_version_distribution::firstOrCreate([
                    'pco_version_id' => $versionId,
                    'user_id' => $userId,
                    'type' => 'appr'
                ]);

                $pco->created_at = Carbon::now()->toDateString();
                $pco->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $pco = Pco_version_distribution::firstOrCreate([
                    'pco_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'appr'
                ]);

                $pco->created_at = Carbon::now()->toDateString();
                $pco->save();
            }
        }
    }

    public function markTransmittalSubrSentTo($versionId, $data)
    {
        if (!empty($data['userIds'])) {
            foreach ($data['userIds'] as $userId) {
                $pco = Pco_version_distribution::firstOrCreate([
                    'pco_version_id' => $versionId,
                    'user_id' => $userId,
                    'type' => 'appr'
                ]);

                $pco->created_at = Carbon::now()->toDateString();
                $pco->save();
            }
        }

        //insert distribution contacts for appr
        if (!empty($data['contactIds'])) {
            foreach ($data['contactIds'] as $contactId) {
                $pco = Pco_version_distribution::firstOrCreate([
                    'pco_version_id' => $versionId,
                    'ab_cont_id' => $contactId,
                    'type' => 'appr'
                ]);

                $pco->created_at = Carbon::now()->toDateString();
                $pco->save();
            }
        }
    }

    public function getPcoSubcontractorRecipientUserDistribution($versionId, $subcontractor=false)
    {
        return PcosSubcontractorProposalDistribution::where('version_id', '=', $versionId)
            ->where('subcontractor', '=', $subcontractor)
            ->with('user.company')
            ->get();
    }

    /**
     * Creates an entry for each user where the email is sent
     * @param $versionId
     * @param $userId
     * @param $subcontractor
     * @return PcosSubcontractorProposalDistribution
     */
    public function addProposalUserDistribution($versionId, $userId, $subcontractor)
    {
        $pcosSubcontractorProposal = PcosSubcontractorProposalDistribution::firstOrCreate(
            [
                'version_id' => $versionId,
                'user_id' => $userId,
                'subcontractor' => $subcontractor
            ]);

        $pcosSubcontractorProposal->created_at = Carbon::now()->toDateString();
        $pcosSubcontractorProposal->save();

        return $pcosSubcontractorProposal;
    }


    /** Get number column from pcos table for specific project_id
     * @param $projectId
     */
    public function getGeneratedNumbers($projectId)
    {
        return Pco::where('proj_id', '=', $projectId)
            ->where('comp_id', '=', $this->companyId)
            ->where('user_id', '=', $this->userId)
            ->orderBy('number', 'ASC')
            ->get(['number']);
    }

}