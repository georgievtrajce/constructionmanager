<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/3/2015
 * Time: 4:51 PM
 */

namespace App\Modules\Pcos\Implementations;


use App\Models\Pco;
use App\Models\Pco_version;
use App\Models\Pco_version_file;
use App\Modules\Pcos\Interfaces\PcoVersionsInterface;
use App\Modules\Pcos\Repositories\PcosRepository;
use Carbon\Carbon;

class SubcontractorVersions implements PcoVersionsInterface {

    private $pcosRepo;

    public function __construct()
    {
        $this->pcosRepo = new PcosRepository();
    }

    /**
     * Store pco subcontractor version
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $data
     * @return static
     */
    public function storeVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $data)
    {
        return $this->pcosRepo->storePcoSubcontractorVersion($pcoSubcontractorRecipientId, $data);
    }

    /**
     * Update pco subcontractor version
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data)
    {
        return $this->pcosRepo->updatePcoSubcontractorVersion($pcoSubcontractorRecipientId, $versionId, $data);
    }

    /**
     * Update pco subcontractor version
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateVersionTransmittalNotes($versionId, $data)
    {
        return $this->pcosRepo->updatePcoSubcontractorVersionTransmittalNotes($versionId, $data);
    }

}