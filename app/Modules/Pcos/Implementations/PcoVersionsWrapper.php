<?php
namespace App\Modules\Pcos\Implementations;

use App\Modules\Pcos\Interfaces\PcoVersionsInterface;

class PcoVersionsWrapper {

    /**
     * Wrapper for the pco version store
     * Get an instance from a class depends on the version type (subcontractor version or recipient version)
     * @param PcoVersionsInterface $pcoVersionsInterface
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $data
     * @return mixed
     */
    public function wrapVersionStore(PcoVersionsInterface $pcoVersionsInterface, $pcoId, $versionType, $pcoSubcontractorRecipientId, $data)
    {
        return $pcoVersionsInterface->storeVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $data);
    }

    /**
     * Wrapper for the pco version update
     * Get an instance from a class depends on the version type (subcontractor version or recipient version)
     * @param PcoVersionsInterface $pcoVersionsInterface
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function wrapVersionUpdate(PcoVersionsInterface $pcoVersionsInterface, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data)
    {
        return $pcoVersionsInterface->updateVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data);
    }

    /**
     * Wrapper for the pco version update on transmittal notes
     * Get an instance from a class depends on the version type (subcontractor version or recipient version)
     * @param PcoVersionsInterface $pcoVersionsInterface
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function wrapVersionTransmittalNotesUpdate(PcoVersionsInterface $pcoVersionsInterface, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data)
    {
        return $pcoVersionsInterface->updateVersionTransmittalNotes($versionId, $data);
    }
}