<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 11/5/2015
 * Time: 9:13 AM
 */

namespace App\Modules\Pcos\Implementations;


use App\Modules\Pcos\Interfaces\PcoVersionsInterface;
use App\Modules\Pcos\Repositories\PcosRepository;

class RecipientVersions implements PcoVersionsInterface {

    private $pcosRepo;

    public function __construct()
    {
        $this->pcosRepo = new PcosRepository();
    }

    /**
     * Store pco recipient version
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $data
     * @return static
     */
    public function storeVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $data)
    {
        return $this->pcosRepo->storePcoRecipientVersion($pcoSubcontractorRecipientId, $data);
    }

    /**
     * Update pco recipient version
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data)
    {
        return $this->pcosRepo->updatePcoRecipientVersion($pcoSubcontractorRecipientId, $versionId, $data);
    }

    /**
     * Update pco recipient version
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param $data
     * @return mixed
     */
    public function updateVersionTransmittalNotes($versionId, $data)
    {
        return $this->pcosRepo->updatePcoRecipientVersionTransmittalNotes($versionId, $data);
    }
}