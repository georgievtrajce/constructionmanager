<?php
namespace App\Modules\Pcos\Interfaces;

interface PcoVersionsInterface {

    public function storeVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $data);
    public function updateVersion($pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $data);
    public function updateVersionTransmittalNotes($versionId, $data);

}