<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 3:19 PM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\GetAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class GetAddressBookAddresses implements GetAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Get all address book entry addresses
     * @param $companyId
     * @return mixed
     */
    public function getAddresses($companyId)
    {
        return $this->repo->getAllAddressBookCompanyAddresses($companyId);
    }

    /**
     * Get address book address for editing
     * @param $companyId
     * @param $addressId
     * @return mixed
     */
    public function getEditAddress($companyId, $addressId)
    {
        return $this->repo->getAddressBookCompanyAddress($companyId, $addressId);
    }

}