<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 3:59 PM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\StoreAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class StoreAddressBookAddress implements StoreAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Store address book entry address
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeData($companyId, $data)
    {
        return $this->repo->storeAddressBookCompanyAddress($companyId, $data);
    }

}