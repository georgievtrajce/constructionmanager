<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/5/2015
 * Time: 1:08 PM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\EditAddressesInterface;
use App\Modules\Addresses\Interfaces\GetAddressesInterface;
use App\Modules\Addresses\Interfaces\StoreAddressesInterface;

class AddressesWrapper {

    /**
     * Get all addresses
     * @param GetAddressesInterface $addressesInterface
     * @param $companyId
     * @return mixed
     */
    public function implementGetAddresses(GetAddressesInterface $addressesInterface, $companyId)
    {
        return $addressesInterface->getAddresses($companyId);
    }

    /**
     * Get address
     * @param GetAddressesInterface $addressesInterface
     * @param $companyId
     * @param $contactId
     * @return mixed
     */
    public function implementGetEditAddress(GetAddressesInterface $addressesInterface, $companyId, $contactId)
    {
        return $addressesInterface->getEditAddress($companyId, $contactId);
    }

    /**
     * Store address implementation
     * @param StoreAddressesInterface $storeAddressesInterface
     * @param $companyId
     * @param $data
     * @return mixed
     */
    public function implementStoreAddress(StoreAddressesInterface $storeAddressesInterface, $companyId, $data)
    {
        return $storeAddressesInterface->storeData($companyId, $data);
    }

    /**
     * Update address
     * @param EditAddressesInterface $editAddressesInterface
     * @param $companyId
     * @param $addressId
     * @param $data
     * @return mixed
     */
    public function implementUpdateAddress(EditAddressesInterface $editAddressesInterface, $companyId, $addressId, $data)
    {
        return $editAddressesInterface->editAddress($companyId, $addressId, $data);
    }

}