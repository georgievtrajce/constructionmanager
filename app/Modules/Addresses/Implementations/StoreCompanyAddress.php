<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/22/2015
 * Time: 3:46 PM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\StoreAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class StoreCompanyAddress implements StoreAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Store registered company address
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeData($companyId, $data)
    {
        return $this->repo->storeRegisteredCompanyAddress($companyId, $data);
    }

}