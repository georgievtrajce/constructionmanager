<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/23/2015
 * Time: 10:16 AM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\CheckAddressForDeleteInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class CheckCompanyAddressDelete implements CheckAddressForDeleteInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Check if the address can be deleted
     * @param $addressId
     * @return mixed
     */
    public function checkForDelete($addressId)
    {
        return $this->repo->getCompanyAddressDependency($addressId);
    }

}