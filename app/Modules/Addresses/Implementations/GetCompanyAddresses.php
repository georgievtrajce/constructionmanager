<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/22/2015
 * Time: 4:33 PM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\GetAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class GetCompanyAddresses implements GetAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Get all registered company addresses
     * @param $companyId
     * @return mixed
     */
    public function getAddresses($companyId)
    {
        return $this->repo->getAllRegisteredCompanyAddresses($companyId);
    }

    /**
     * Get registered company address for editing
     * @param $companyId
     * @param $addressId
     * @return mixed
     */
    public function getEditAddress($companyId, $addressId)
    {
        return $this->repo->getRegisteredCompanyAddress($addressId);
    }

}