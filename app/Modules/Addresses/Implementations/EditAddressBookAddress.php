<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/7/2015
 * Time: 10:41 AM
 */

namespace App\Modules\Addresses\Implementations;

use App\Modules\Addresses\Interfaces\EditAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class EditAddressBookAddress implements EditAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Update address book entry address
     * @param $companyId
     * @param $addressId
     * @param $data
     * @return mixed
     */
    public function editAddress($companyId, $addressId, $data)
    {
        return $this->repo->updateAddressBookCompanyAddress($addressId, $data);
    }

}