<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/23/2015
 * Time: 9:51 AM
 */

namespace App\Modules\Addresses\Implementations;


use App\Modules\Addresses\Interfaces\EditAddressesInterface;
use App\Modules\Addresses\Repositories\AddressesRepository;

class EditCompanyAddress implements EditAddressesInterface {

    private $repo;

    public function __construct()
    {
        //instance from the addresses repository class
        $this->repo = new AddressesRepository();
    }

    /**
     * Update registered company address
     * @param $companyId
     * @param $addressId
     * @param $data
     * @return mixed
     */
    public function editAddress($companyId, $addressId, $data)
    {
        return $this->repo->updateRegisteredCompanyAddress($addressId, $data);
    }

}