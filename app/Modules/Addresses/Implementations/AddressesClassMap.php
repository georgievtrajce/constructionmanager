<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 3:07 PM
 */

namespace App\Modules\Addresses\Implementations;

use Illuminate\Support\Facades\Config;

class AddressesClassMap {

    /**
     * Get index page for redirection
     * @param $addressType
     * @param $companyId
     * @return string
     */
    public function getIndexPageRedirect($addressType, $companyId)
    {
        if($addressType == 'address-book')
            return '/address-book/'.$companyId.'/addresses';
        if($addressType == 'company-profile')
            return '/company-profile/'.$companyId;
    }

}