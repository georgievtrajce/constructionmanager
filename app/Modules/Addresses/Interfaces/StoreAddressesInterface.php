<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 4:06 PM
 */

namespace App\Modules\Addresses\Interfaces;


interface StoreAddressesInterface {

    public function storeData($companyId, $data);

}