<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 3:12 PM
 */
namespace App\Modules\Addresses\Interfaces;

interface GetAddressesInterface {

    public function getAddresses($companyId);
    public function getEditAddress($companyId, $addressId);

}