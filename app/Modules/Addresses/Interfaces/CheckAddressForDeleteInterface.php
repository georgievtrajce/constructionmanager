<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/7/2015
 * Time: 12:00 PM
 */

namespace App\Modules\Addresses\Interfaces;


interface CheckAddressForDeleteInterface {

    public function checkForDelete($addressId);

}