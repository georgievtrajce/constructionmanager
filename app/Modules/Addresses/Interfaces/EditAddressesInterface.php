<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/7/2015
 * Time: 10:45 AM
 */

namespace App\Modules\Addresses\Interfaces;


interface EditAddressesInterface {

    public function editAddress($companyId, $addressId, $data);

}