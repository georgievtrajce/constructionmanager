<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 4/6/2015
 * Time: 3:19 PM
 */
namespace App\Modules\Addresses\Repositories;

use App\Models\Ab_contact;
use App\Models\Ab_contact_address;
use App\Models\Address;
use App\Models\User;

class AddressesRepository {

    /**
     * Get all companies addresses that belongs to specified company address book
     * @param $companyId
     * @return mixed
     */
    public function getAllAddressBookCompanyAddresses($companyId)
    {
        return Address::where('ab_id','=',$companyId)->orderBy('office_title','ASC')->get();
    }

    /**
     * Get all registered company addresses specified by id
     * @param $companyId
     * @return mixed
     */
    public function getAllRegisteredCompanyAddresses($companyId)
    {
        return Address::where('comp_id','=',$companyId)->orderBy('id','DESC')->get();
    }

    /**
     * Get address book company address
     * @param $addressBookId
     * @param $id
     * @return mixed
     */
    public function getAddressBookCompanyAddress($addressBookId, $id)
    {
        return Address::where('ab_id','=',$addressBookId)
            ->where('id','=',$id)
            ->firstOrFail();
    }

    /**
     * Get registered company address
     * @param $id
     * @return mixed
     */
    public function getRegisteredCompanyAddress($id)
    {
        return Address::where('id','=',$id)->firstOrFail();
    }

    /**
     * Store address book company address in database table
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeAddressBookCompanyAddress($companyId, $data)
    {
        return Address::create([
            'office_title' => $data['office_title'],
            'street' => $data['ca_street'],
            'zip' => $data['ca_zip'],
            'city' => $data['ca_town'],
            'state' => $data['ca_state'],
            'ab_id' => $companyId
        ]);
    }

    /**
     * Store registered company address in database
     * @param $companyId
     * @param $data
     * @return static
     */
    public function storeRegisteredCompanyAddress($companyId, $data)
    {
        return Address::create([
            'office_title' => $data['office_title'],
            'street' => $data['ca_street'],
            'zip' => $data['ca_zip'],
            'city' => $data['ca_town'],
            'state' => $data['ca_state'],
            'comp_id' => $companyId
        ]);
    }

    /**
     * Update address book company address
     * @param $id
     * @param $data
     * @return mixed
     */
    public function updateAddressBookCompanyAddress($id, $data)
    {
        return Address::where('id','=',$id)->update([
            'office_title' => $data['office_title'],
            'street' => $data['ca_street'],
            'zip' => $data['ca_zip'],
            'city' => $data['ca_town'],
            'state' => $data['ca_state']
        ]);
    }

    /**
     * Update registered company address
     * @param $addressId
     * @param $data
     * @return mixed
     */
    public  function updateRegisteredCompanyAddress($addressId, $data)
    {
        return Address::where('id','=',$addressId)->update([
            'office_title' => $data['office_title'],
            'street' => $data['ca_street'],
            'zip' => $data['ca_zip'],
            'city' => $data['ca_town'],
            'state' => $data['ca_state']
        ]);
    }

    /**
     * Check if address book company address is connected to address book contact
     * @param $addressId
     * @return mixed
     */
    public function getAddressDependency($addressId)
    {
        $response = Address::where('id','=',$addressId)
            ->with('ab_contacts')
            ->with('project_companies')
            ->with('proposal_suppliers')
            ->firstOrFail();

        if(count($response->ab_contacts) || count($response->project_companies) || count($response->proposal_suppliers)) {
            return null;
        }

        return $response;
    }

    /**
     * Check if registered company address is connected to registered company user
     * @param $addressId
     * @return mixed
     */
    public function getCompanyAddressDependency($addressId)
    {
        $response = Address::where('id','=',$addressId)
            ->with('users')
            ->firstOrFail();

        if(count($response->users)) {
            return null;
        }

        return $response;
    }

    /**
     * Delete address
     * @param $addressId
     * @return int
     */
    public function deleteAddress($addressId)
    {
        return Address::destroy($addressId);
    }

}