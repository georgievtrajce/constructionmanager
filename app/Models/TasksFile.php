<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TasksFile extends Model {

    protected $table = 'tasks_files';

    protected $fillable = array('task_id', 'comp_id', 'file_name', 'expiration_date', 'size');

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }
}
