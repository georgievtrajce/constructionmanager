<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_distribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address_book_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ab_id','ab_cont_id','ab_file_id', 'user_id'];

    public function users()
    {
        return $this->hasMany('App\Models\User','id','user_id');
    }

    public function abUsers()
    {
        return $this->hasMany('App\Models\Ab_contact','id','ab_cont_id');
    }
}
