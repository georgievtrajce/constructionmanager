<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Custom_category extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'comp_id'];

    public function address_book_entries()
    {
        return $this->belongsToMany('App\Models\Address_book','ab_custom_categories','custom_category_id','ab_id');
    }

    public function scopeGetCategories($query, $categoryId, $companyId)
    {
        return $query->where('id','=',(int)$categoryId)->where('comp_id','=',$companyId)->first();
    }

}
