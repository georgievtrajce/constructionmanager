<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File_permission extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'file_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_id','comp_id','share_comp_id'];

    public function file()
    {
        return $this->belongsTo('App\Models\Project_file','id','file_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

}
