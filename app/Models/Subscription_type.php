<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription_type extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscription_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'ref_points', 'ref_points_needed', 'upload_limit', 'download_limit',
        'storage_limit', 'amount'];

}
