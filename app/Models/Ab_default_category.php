<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_default_category extends Model {

    //
    protected $table = 'ab_default_categories';

    protected $fillable = array('ab_id','default_category_id');

}
