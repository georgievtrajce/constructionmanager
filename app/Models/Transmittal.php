<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transmittal extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transmittals_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comp_id','user_id','file_id','file_type_id','proj_id','version_id','date_type','emailed'];

    //relations
    public function company()
    {
        return $this->belongsTo('App\Models\Company','id','comp_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User','id','user_id');
    }

    public function file()
    {
        return $this->hasOne('App\Models\Project_file','id','file_id');
    }

    //submittal versions
    public function version()
    {
        return $this->belongsTo('App\Models\Submittal_version','version_id','id');
    }

    //rfi versions
    public function rfi_version()
    {
        return $this->belongsTo('App\Models\Rfi_version','version_id','id');
    }

    //pco versions
    public function pco_version()
    {
        return $this->belongsTo('App\Models\Pco_version','version_id','id');
    }

    public function fileType()
    {
        return $this->belongsTo('App\Models\File_type','id','file_type_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project','proj_id','id');
    }

}
