<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_permission extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','add','edit','delete','entity_id','entity_type'];

    //relationships
    public function module()
    {
        return $this->belongsTo('App\Models\Module','entity_id','id');
    }

    public function project_file()
    {
        return $this->belongsTo('App\Models\File_type','entity_id','id');
    }

}
