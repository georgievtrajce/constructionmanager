<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_permission extends Model
{
    protected $table = 'project_permissions';
    protected $fillable = array('proj_id', 'user_id', 'read', 'write', 'delete', 'show_cost', 'entity_type', 'entity_id', 'file_type_id');

    public function module()
    {
        return $this->hasOne('App\Models\Module', 'id', 'entity_id');
    }
    public function fileType()
    {
        return $this->hasOne('App\Models\File_type', 'id', 'entity_id');
    }

    public function scopeModule()
    {
        return $this->with('module')->where('entity_type', '=', 1)->first();
    }

    public function scopeFileType() {
        return $this->with('module')->where('entity_type', '=', 2)->first();
    }

}