<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission_role extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permission_role';
    public $timestamps = false;

}
