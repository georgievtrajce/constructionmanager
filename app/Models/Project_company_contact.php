<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_company_contact extends Model {
    protected $table = 'project_company_contacts';
    protected $fillable = array(
        'proj_id',
        'ab_cont_id',
        'ab_address_id',
        'ab_comp_id',
        'proj_comp_id'
    );

    function project(){
        return $this->belongsTo('App\Models\Project','id','comp_id');
    }
    function contact(){
        return $this->belongsTo('App\Models\Ab_contact','ab_cont_id','id');
    }
}