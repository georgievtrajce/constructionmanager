<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Pco_version extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pcos_versions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pco_id',
        'subcontractor_id',
        'recipient_id',
        'pco_number',
        'cycle_no',
        'sent_appr',
        'rec_appr',
        'subm_sent_sub',
        'rec_sub',
        'status_id',
        'notes',
        'created_at',
        'updated_at',
        'cost'
    ];

    //relations
    public function subcontractor()
    {
        return $this->belongsTo('App\Models\Pco_subcontractor_recipient','subcontractor_id','id');
    }

    public function recipient()
    {
        return $this->belongsTo('App\Models\Pco_subcontractor_recipient','recipient_id','id');
    }

    public function status()
    {
        return $this->hasOne('App\Models\Pco_status','id','status_id');
    }

    public function latest_file()
    {
        return $this->hasOne('App\Models\Pco_version_file','pco_version_id','id')->latest();
    }

    public function file()
    {
        return $this->belongsToMany('App\Models\Project_file','pco_version_files', 'pco_version_id', 'file_id');
    }

    public function smallFile()
    {
        return $this->belongsToMany('App\Models\Project_file','pco_version_files', 'pco_version_id', 'file_id')
            ->whereRaw('CAST(size AS UNSIGNED) < 250000000');
    }

    public function files()
    {
        return $this->hasMany('App\Models\Pco_version_file','pco_version_id','id');
    }

    // override existing delete method.
    // invoke when we call pco version delete().
    public function delete()
    {
        // delete all associated files
        $this->files()->delete();

        // delete the address book entry
        return parent::delete();
    }

    public function pcoVersionApprUsers()
    {
        return $this->hasMany('App\Models\Pco_version_distribution','pco_version_id','id')
            ->where('type', '=', 'appr');
    }

    public function transmittalSubmSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 8)
            ->where('date_type', '=', 'subm_sent_sub');
    }

    public function transmittalSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 8)
            ->where('date_type', '=', 'sent_appr');
    }

}
