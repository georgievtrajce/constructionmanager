<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subcontractor_project_info extends Model {

    protected $table = 'subcontractor_project_info';
    protected $fillable = array(
        'name',
        'number',
        'start_date',
        'end_date',
        'owner_id',
        'active',
        'upc_code',
        'price',
        'architect_id',
        'proj_id',
        'subcontractor_id',
        'creator_id'
    );

    function project(){
        return $this->belongsTo('App\Models\Project', 'proj_id', 'id');
    }

    function architect(){
        return $this->hasOne('App\Models\Address_book', 'id', 'architect_id');
    }
    function owner(){
        return $this->hasOne('App\Models\Address_book', 'id', 'owner_id');
    }

}
