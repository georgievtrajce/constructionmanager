<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_import_history extends Model {

    //
    protected $table = 'ab_import_history';

    protected $fillable = array('user_id','ab_ids');

}
