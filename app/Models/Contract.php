<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contracts';
    protected $fillable = [
        'ab_id',
        'proj_id',
        'value',
        'number',
        'comp_id',
        'mf_title',
        'mf_number',
        'name'
    ];


    function items(){
        return $this->hasMany('App\Models\Contract_item', 'contr_id', 'id');
    }
    function contractFiles(){
        return $this->hasMany('App\Models\Contract_file', 'contr_id', 'id');
    }

    function delete(){

        // delete all associated suppliers
        $this->items()->delete();

        // delete all contract files
        $files = $this->contractFiles()->get();
        foreach ($files as $file) {
            //delete all permissions for files
            $file->delete();
        }
        $this->contractFiles()->delete();

        return parent::delete();
    }


}