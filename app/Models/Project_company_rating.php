<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_company_rating extends Model {
    protected $table = 'project_company_ratings';
    protected $fillable = array(
        'price',
        'quality_of_work',
        'quality_of_materials',
        'expertise',
        'problems_resolution',
        'safety',
        'cleanness',
        'organized_and_professional',
        'communication_skills',
        'completed_on_time',
        'project_rating',
        'ab_cont_id',
        'proj_id'
    );

    function project(){
        return $this->belongsTo('App\Models\Project','id','comp_id');
    }
    function contact(){
        return $this->belongsTo('App\Models\Ab_contact','ab_cont_id','id');
    }
}