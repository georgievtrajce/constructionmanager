<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_contractor_instruction extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_contractor_verbal_instructions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        'selected_comp_id',
        'selected_employee_id',
        'description',
        'created_at',
        'updated_at'
    ];

}
