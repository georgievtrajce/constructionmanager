<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_contractor_inspection extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_contractor_inspections';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        'testing_performed',
        'mf_number',
        'mf_title',
        'selected_comp_id',
        'selected_employee_id',
        'other_company',
        'other_employee',
        'notes',
        'created_at',
        'updated_at'
    ];

}
