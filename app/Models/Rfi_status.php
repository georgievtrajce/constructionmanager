<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rfi_status extends Model {

    protected $table = 'rfi_statuses';

    protected $fillable = array('name','short_name');

}
