<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignedRoles extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_user';
    protected $fillable = array('user_id','role_id');
    public $timestamps = false;

}
