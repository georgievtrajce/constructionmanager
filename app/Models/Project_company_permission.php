<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_company_permission extends Model
{
    protected $table = 'project_company_permissions';
    protected $fillable = array('proj_comp_id', 'proj_id', 'comp_parent_id', 'comp_child_id', 'read', 'pco_permission_type', 'write', 'delete', 'entity_type', 'entity_id');

    public function module()
    {
        return $this->hasOne('App\Models\Module', 'id', 'entity_id');
    }
    public function fileType()
    {
        return $this->hasOne('App\Models\File_type', 'id', 'entity_id');
    }

    function project_company(){
        return $this->belongsTo('App\Models\Project_company', 'proj_comp_id', 'id');
    }

}