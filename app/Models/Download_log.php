<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Download_log extends Model
{
    protected $table = 'download_logs';
    protected $fillable = array('file_id', 'user_id', 'comp_id');

}