<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Project extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

	//
    protected $table = 'projects';
    protected $fillable = array('name','number', 'start_date', 'end_date', 'owner_id', 'comp_id','creator_id',
        'proj_admin','active','upc_code','price', 'architect_id', 'prime_subcontractor_id', 'deleted_by', 'files_deleted',
        'contractor_transmittal_id', 'architect_transmittal_id', 'prime_subcontractor_transmittal_id', 'make_me_gc',
        'general_contractor_id', 'owner_transmittal_id', 'master_format_type_id');

    function company(){
        return $this->belongsTo('App\Models\Company', 'comp_id', 'id');
    }

    function projectSubcontractors(){
        return $this->hasMany('App\Models\Project_subcontractor', 'proj_id', 'id');
    }

    function projectSubcontractorsSelf(){
        return $this->hasMany('App\Models\Project_subcontractor', 'proj_id', 'id')
                    ->where('comp_child_id', '=', Auth::user()->comp_id)
                    ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
    }

    function projectBiddersSelf(){
        return $this->hasMany('App\Models\Project_bidder', 'proj_id', 'id')
            ->where('invited_comp_id', '=', Auth::user()->comp_id)
            ->whereIn('status', [Config::get('constants.projects.shared_status.accepted')]);
    }

    function projectBidders(){
        return $this->hasMany('App\Models\Project_bidder', 'proj_id', 'id');
    }

    function subcontractor(){
        return $this->hasOne('App\Models\Project_subcontractor', 'proj_id', 'id')->where('comp_child_id','=',Auth::user()->comp_id);
    }

    function address(){
        return $this->hasOne('App\Models\Address', 'proj_id', 'id')->where('subcontractor_id','=',0);
    }

    function subcontractorAddress(){
        return $this->hasOne('App\Models\Address', 'proj_id', 'id')->where('subcontractor_id','=',Auth::user()->comp_id);
    }

    function subcontractorProjectInfo(){
        return $this->hasOne('App\Models\Subcontractor_project_info', 'proj_id', 'id')->where('subcontractor_id','=',Auth::user()->comp_id);
    }

    function architect(){
        return $this->hasOne('App\Models\Address_book', 'id', 'architect_id');
    }

    function primeSubcontractor(){
        return $this->hasOne('App\Models\Address_book', 'id', 'prime_subcontractor_id');
    }

    function owner(){
        return $this->hasOne('App\Models\Address_book', 'id', 'owner_id');
    }

    function projectCompanies(){
        return $this->hasMany('App\Models\Project_company', 'proj_id', 'id');
    }

    function projectContacts(){
        return $this->hasMany('App\Models\Project_company_contact', 'proj_id', 'id');
    }

    function submittal() {
        //should be checked if used, and deleted. use the one under
        return $this->belongsToMany('App\Models\Submittal', 'proj_id', 'id');
    }

    function submittals() {
        return $this->hasMany('App\Models\Submittal', 'proj_id', 'id');
    }

    function proposals() {
        return $this->hasMany('App\Models\Proposal', 'proj_id', 'id');
    }

    function contracts() {
        return $this->hasMany('App\Models\Contract','proj_id','id');
    }

    function materialsAndServices() {
        return $this->hasMany('App\Models\Expediting_report', 'proj_id', 'id');
    }

    function blogPosts() {
        return $this->hasMany('App\Models\BlogPost', 'proj_id', 'id');
    }

    function projectFiles() {
        return $this->hasMany('App\Models\Project_file', 'proj_id', 'id');
    }

    function proposalFiles() {
        return $this->hasMany('App\Models\Project_file', 'proj_id', 'id')->where('files.ft_id', '=',2);
    }

    function projectPermissions() {
        return $this->hasMany('App\Models\Project_permission', 'proj_id', 'id');
    }

    function projectUserPermissions() {
        return $this->hasMany('App\Models\Project_permission_user', 'proj_id', 'id');
    }

    function contractorTransmittal() {
        return $this->hasOne('App\Models\Ab_contact', 'id', 'contractor_transmittal_id');
    }

    function architectTransmittal() {
        return $this->hasOne('App\Models\Ab_contact', 'id', 'architect_transmittal_id');
    }

    function generalContractor() {
        return $this->hasOne('App\Models\Address_book', 'id', 'general_contractor_id');
    }

    function primeSubcontractorTransmittal() {
        return $this->hasOne('App\Models\Ab_contact', 'id', 'prime_subcontractor_transmittal_id');
    }

    function ownerTransmittal() {
        return $this->hasOne('App\Models\Ab_contact', 'id', 'owner_transmittal_id');
    }
}
