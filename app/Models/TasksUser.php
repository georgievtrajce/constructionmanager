<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class TasksUser extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tasks_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'task_id', 'user_id', 'ab_cont_id', 'ab_comp_id'];

    public function users()
    {
        return $this->hasMany('App\Models\User','id','user_id');
    }

    public function abUsers()
    {
        return $this->hasMany('App\Models\Ab_contact','id','ab_cont_id');
    }

    public function distribution()
    {
        return $this->hasOne('App\Models\TaskUsersDistribution','task_user_id','id');
    }
}
