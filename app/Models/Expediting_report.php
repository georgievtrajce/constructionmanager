<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expediting_report extends Model
{
    protected $table = 'expediting_reports';
    protected $fillable = array('proj_id', 'name', 'mf_title', 'mf_number', 'value', 'position', 'ab_id', 'er_status_id', 'subm_needed', 'status_id', 'need_by', 'comp_id');

    public function status()
    {
        return $this->hasOne('App\Models\Expediting_report_status','id','er_status_id');
    }

}