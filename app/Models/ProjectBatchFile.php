<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectBatchFile extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['proj_id', 'comp_id', 'user_id', 'generated', 'sent'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_batch_files';

    //select project for blog post
    public function project()
    {
        return $this->hasOne('App\Models\Project','id', 'proj_id');
    }

    //blog post author
    public function author()
    {
        return $this->hasOne('App\Models\User','id', 'user_id');
    }
}
