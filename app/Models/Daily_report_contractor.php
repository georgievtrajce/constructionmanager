<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_contractor extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_contractors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        //workers with names tab
        'worker_office',
        'worker_name',
        'title',
        'regular_hours',
        'overtime_hours',
        'description',
        'created_at',
        'updated_at'
    ];

}
