<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Daily_report extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'date',
        'report_num',
        'is_emailed',
        'is_windy',
        'is_sunny',
        'is_overcast',
        'is_cloudy',
        'is_rain',
        'is_snow',
        'is_sleet',
        'is_hail',
        'is_lightning',
        'temperature',
        'is_dry',
        'is_frozen',
        'ground_is_snow',
        'is_mud',
        'is_water',
        'condition_notes',
        'general_notes',
        'is_uploaded',
        'copied_report_id',
        'created_at',
        'updated_at'
    ];

    public function contractors()
    {
        return $this->hasMany('App\Models\Daily_report_contractor', 'daily_report_id', 'id');
    }

    public function contractorTrades()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_trade', 'daily_report_id', 'id');
    }

    public function subcontractors()
    {
        return $this->hasMany('App\Models\Daily_report_subcontractor', 'daily_report_id', 'id');
    }

    public function equipments()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_equipment', 'daily_report_id', 'id');
    }

    public function materials()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_material', 'daily_report_id', 'id');
    }

    public function inspections()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_inspection', 'daily_report_id', 'id');
    }

    public function observations()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_observation', 'daily_report_id', 'id');
    }

    public function instructions()
    {
        return $this->hasMany('App\Models\Daily_report_contractor_instruction', 'daily_report_id', 'id');
    }

    public function files()
    {
        return $this->hasMany('App\Models\Daily_report_file', 'daily_report_id', 'id');
    }

    public function countImages()
    {
        return $this->hasMany('App\Models\Daily_report_file', 'daily_report_id', 'id')->where('file_type_id', '=', Config::get('constants.daily_report_image_file_type_id'))->selectRaw('daily_report_id, count(*) as count_images');
    }

    public function countPdfs()
    {
        return $this->hasMany('App\Models\Daily_report_file', 'daily_report_id', 'id')->where('file_type_id', '=', Config::get('constants.daily_report_pdf_file_type_id'))->selectRaw('daily_report_id, count(*) as count_pdfs');
    }

    public function emailedUsers()
    {
        return $this->hasMany('App\Models\Daily_report_email', 'daily_report_id', 'id')->where('emailed', '=', 1)->where('is_ab_contact', '=', 0);
    }

    public function emailedContracts()
    {
        return $this->hasMany('App\Models\Daily_report_email', 'daily_report_id', 'id')->where('emailed', '=', 1)->where('is_ab_contact', '=', 1);
    }

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function generatedFile()
    {
        return $this->hasOne('App\Models\Daily_report_file', 'daily_report_id', 'id')->where('file_type_id', '=', Config::get('constants.daily_report_file_type_id'));
    }

}
