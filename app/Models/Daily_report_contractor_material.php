<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_contractor_material extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_contractor_materials';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        'material_delivered',
        'quantity',
        'selected_comp_id',
        'notes',
        'created_at',
        'updated_at'
    ];

}
