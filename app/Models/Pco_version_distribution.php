<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco_version_distribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pcos_versions_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pco_version_id','ab_cont_id','type', 'user_id', 'created_at'];

    public function pco()
    {
        return $this->belongsTo('App\Models\Pco_version','pco_version_id','id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User','id','user_id');
    }

    public function abUsers()
    {
        return $this->hasMany('App\Models\Ab_contact','id','ab_cont_id');
    }
}
