<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ab_contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'title', 'email', 'address_id', 'office_phone', 'cell_phone', 'fax',
        'ab_id'];

    //relations
    public function office()
    {
        return $this->hasOne('App\Models\Address','id','address_id');
    }

}
