<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_email extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_email_notifications';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'is_ab_contact',
        'daily_report_id',
        'emailed',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function contract()
    {
        return $this->hasOne('App\Models\Ab_contact', 'id', 'user_id');
    }

}
