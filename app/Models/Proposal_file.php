<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Proposal_file extends Model
{
    protected $table = 'proposal_files';
    protected $fillable = array('prop_supp_id', 'file_id','item_no');

    function file(){
        return $this->hasOne('App\Models\File','id','file_id');
    }

    function smallFile(){
        return $this->hasOne('App\Models\File','id','file_id')
            ->whereRaw('CAST(size AS UNSIGNED) < 250000000');
    }

    function proposal_supplier(){
        return $this->hasOne('App\Models\Proposal_supplier','id','prop_supp_id');
    }

    function delete(){
        // delete all associated files
        $this->file()->delete();

        return parent::delete();
    }

}