<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Proposal extends Model
{
    protected $table = 'proposals';
    protected $fillable = array('proj_id', 'name', 'mf_title', 'mf_number', 'value', 'status', 'unit_price', 'quantity',
        'comp_id', 'proposals_needed_by', 'scope_of_work');

    function project(){
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

    function suppliers(){
        return $this->hasMany('App\Models\Proposal_supplier', 'prop_id', 'id');
    }

//    function bidder(){
//        return $this->hasMany('App\Models\Proposal_supplier', 'prop_id', 'id')->where('invited_comp_id','=',Auth::user()->comp_id);
//    }

//    function delete(){
//
//        // delete all associated suppliers
//        $suppliers = $this->suppliers()->get();
//        foreach ($suppliers as $supplier) {
//            //delete all permissions for files
//            $supplier->delete();
//        }
//        $this->suppliers()->delete();
//
//        return parent::delete();
//    }

}