<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_company extends Model {
    protected $table = 'project_companies';
    protected $fillable = array('proj_id', 'ab_id', 'custom_field','addr_id');

    function project(){
        return $this->belongsTo('App\Models\Project','proj_id','id');
    }
    function address_book(){
        return $this->belongsTo('App\Models\Address_book','ab_id','id');
    }

    //project company contacts
    public function project_company_contacts()
    {
        return $this->hasMany('App\Models\Project_company_contact','proj_comp_id','id');
    }

    //project subcontractor
    public function project_subcontractor()
    {
        return $this->belongsTo('App\Models\Project_subcontractor','proj_comp_id','id');
    }
}