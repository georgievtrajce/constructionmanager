<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Default_category extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'default_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function address_book_entries()
    {
        return $this->belongsToMany('App\Models\Address_book','ab_default_categories','default_category_id','ab_id');
    }

}
