<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco_version_file extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pco_version_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pco_version_id',
        'file_id',
        'download_priority',
        'created_at',
        'updated_at'
    ];

    //files
    public function version()
    {
        return $this->hasOne('App\Models\Pco_version','id','pco_version_id');
    }

    public function file()
    {
        return $this->hasOne('App\Models\Project_file','id','file_id');
    }

    // override existing delete method.
    // invoke when we call pco version delete().
    public function delete()
    {
        // delete all associated files
        $this->file()->delete();

        // delete the address book entry
        return parent::delete();
    }

}
