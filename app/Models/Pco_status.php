<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco_status extends Model {

    protected $table = 'pco_statuses';

    protected $fillable = array('name','short_name');

    //relations
    public function pco_version()
    {
        return $this->belongsTo('App\Models\Pco_version','status_id','id');
    }

}
