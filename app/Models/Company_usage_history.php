<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company_usage_history extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies_usage_history';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['company_id','data','type', 'month', 'year'];
}
