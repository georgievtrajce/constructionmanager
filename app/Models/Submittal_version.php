<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Submittal_version extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'submittals_versions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['submittal_id','submittal_no','file_id','cycle_no','sent_appr', 'rec_appr',
        'subm_sent_sub','rec_sub','status_id', 'appr_notes', 'subc_notes',' created_at', 'updated_at', 'submitted_for'];

    public function submittal()
    {
        return $this->belongsTo('App\Models\Submittal','submittal_id','id');
    }

//    public function file()
//    {
//        return $this->hasOne('App\Models\Project_file','id','file_id');
//    }

    public function file()
    {
        return $this->belongsToMany('App\Models\Project_file','submittal_version_files', 'submittal_version_id', 'file_id');
    }

    public function smallFile()
    {
        return $this->belongsToMany('App\Models\Project_file','submittal_version_files', 'submittal_version_id', 'file_id')
                 ->whereRaw('CAST(size AS UNSIGNED) < 250000000');
    }

    public function status()
    {
        return $this->hasOne('App\Models\Submittal_status','id','status_id');
    }

    public function submittalVersionApprUsers()
    {
        return $this->hasMany('App\Models\Submittal_version_distribution','submittal_version_id','id')
                        ->where('type', '=', 'appr');
    }

    public function submittalVersionSubcUsers()
    {
        return $this->hasMany('App\Models\Submittal_version_distribution','submittal_version_id','id')
                        ->where('type', '=', 'subc');
    }

    public function transmittalSubmSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 1)
            ->where('date_type', '=', 'subm_sent_sub');
    }

    public function transmittalSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 1)
            ->where('date_type', '=', 'sent_appr');
    }
}
