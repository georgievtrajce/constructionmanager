<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_permission_user extends Model
{
    protected $table = 'project_permission_selected_users';
    protected $fillable = ['proj_id', 'user_id'];
    public $timestamps  = false;

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}