<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco_permission extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pco_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comp_child_id', 'comp_parent_id', 'pco_id'];

}
