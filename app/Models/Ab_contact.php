<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_contact extends Model {

	//
    protected $table = 'ab_contacts';

    protected $fillable = array('name','title','email','ab_id', 'address_id');

    public function project()
    {
        return $this->hasMany('App\Models\Project_company_contact','ab_cont_id','id');
    }

    public function office()
    {
        return $this->hasOne('App\Models\Address','id','address_id');
    }

    public function addressBook()
    {
        return $this->hasOne('App\Models\Address_book','id','ab_id');
    }
}
