<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submittal_status extends Model {

	//
    protected $table = 'submittal_statuses';

    protected $fillable = array('name','short_name');

}
