<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_custom_category extends Model {

    protected $table = 'ab_custom_categories';

    protected $fillable = array('ab_id','custom_category_id');

}
