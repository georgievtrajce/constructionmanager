<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_contact_address extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ab_contact_address';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['address_id', 'contact_id'];

    //relationships


}
