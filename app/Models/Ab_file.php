<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ab_file extends Model {

    protected $table = 'ab_files';

    protected $fillable = array('ab_id', 'comp_id', 'name', 'number', 'file_name', 'expiration_date', 'size',
        'file_orientation', 'notification', 'notification_days');

    public function addressBookFileUsers()
    {
        return $this->hasMany('App\Models\Ab_distribution','ab_file_id','id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

}
