<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comp_id','subs_id','transaction_id','email','card_token','amount'];

    //relations
    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

    public function subscription_type()
    {
        return $this->hasOne('App\Models\Subscription_type','id','subs_id');
    }

}
