<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProposalSupplierProposalsDistribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'proposal_supplier_proposals_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bidder_id', 'user_id', 'proposal_no', 'created_at'];

    public function bidder()
    {
        return $this->belongsTo('App\Models\Proposal_supplier','bidder_id','id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }
}
