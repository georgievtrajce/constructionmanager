<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submittal_version_file extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'submittal_version_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['submittal_version_id','file_id','download_priority','created_at','updated_at'];

}
