<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Company extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'companies';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uac',
        'name',
        'logo',
        'subs_id',
        'ref_points',
        'referred_by',
        'subscription_payment_date',
        'subscription_expire_date',
        'uploaded',
        'downloaded',
        'free_account',
        'limit_month',
        'limit_year',
        'download_notification',
        'upload_notification',
        'storage_notification'
    ];

    function projects(){
        return $this->hasMany('App\Models\Project','comp_id','id');
    }
    function project_subcontractor(){
        return $this->hasMany('App\Models\Project_subcontractor','comp_child_id','id');
    }

    public function users()
    {
        return $this->hasMany('App\Models\User','comp_id','id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address','comp_id','id');
    }

    public function subscription_type()
    {
        return $this->hasOne('App\Models\Subscription_type','id','subs_id');
    }

    public function currentUser()
    {
        return $this->hasOne('App\Models\User','comp_id','id')->where('id', '=', Auth::user()->id);
    }

}
