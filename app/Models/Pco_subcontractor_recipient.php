<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco_subcontractor_recipient extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pcos_subcontractors_recipients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pco_id',
        'pco_name',
        'type',
        'self_performed',
        'ab_subcontractor_id',
        'sub_office_id',
        'sub_contact_id',
        'ab_recipient_id',
        'recipient_office_id',
        'recipient_contact_id',
        'mf_number',
        'mf_title',
        'note',
        'send_notif',
        'created_at',
        'updated_at'
    ];

    //relations
    public function pco()
    {
        return $this->belongsTo('App\Models\Pco','pco_id','id');
    }

    public function ab_subcontractor()
    {
        return $this->hasOne('App\Models\Address_book','id','ab_subcontractor_id');
    }

    public function subcontractor_contact()
    {
        return $this->hasOne('App\Models\Ab_contact','id','sub_contact_id');
    }

    public function ab_recipient()
    {
        return $this->hasOne('App\Models\Address_book','id','ab_recipient_id');
    }

    public function recipient_contact()
    {
        return $this->hasOne('App\Models\Ab_contact','id','recipient_contact_id');
    }

    public function subcontractor_versions()
    {
        return $this->hasMany('App\Models\Pco_version','subcontractor_id','id');
    }

    public function latest_subcontractor_version()
    {
        return $this->hasOne('App\Models\Pco_version','subcontractor_id','id')->latest();
    }

    public function recipient_versions()
    {
        return $this->hasMany('App\Models\Pco_version','recipient_id','id');
    }

    public function latest_recipient_version()
    {
        return $this->hasOne('App\Models\Pco_version','recipient_id','id')->latest();
    }

}
