<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PcosSubcontractorProposalDistribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pcos_subcontractor_proposal_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['version_id', 'user_id', 'subcontractor', 'created_at'];

    public function version()
    {
        return $this->belongsTo('App\Models\Pco_version','version_id','id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }
}
