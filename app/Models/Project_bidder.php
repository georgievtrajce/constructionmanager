<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project_bidder extends Model {

    protected $table = 'project_bidders';
    protected $fillable = array(
        'comp_parent_id',
        'invited_comp_id',
        'ab_id',
        'proj_id',
        'token',
        'token_active',
        'status',
        'once_accepted'
    );

    public function bidders()
    {
        return $this->hasMany('App\Models\Proposal_supplier', 'proj_bidder_id', 'id');
    }

    function invited_company(){
        return $this->hasOne('App\Models\Company','id','invited_comp_id');
    }

    function address_book_company(){
        return $this->hasOne('App\Models\Address_book','id','ab_id');
    }

    function project(){
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

}
