<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rfi_version_file extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rfi_version_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rfi_version_id','file_id','download_priority','created_at','updated_at'];

}
