<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Project_subcontractor extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_subcontractors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comp_parent_id', 'comp_child_id', 'proj_comp_id', 'proj_id','token', 'token_active','status','address_id'];

    function project(){
        return $this->belongsTo('App\Models\Project','proj_id','id');
    }

    function child_company(){
        return $this->belongsTo('App\Models\Company', 'comp_child_id', 'id');
    }

    function parent_company(){
        return $this->belongsTo('App\Models\Company', 'comp_parent_id', 'id');
    }

    function address(){
        return $this->hasOne('App\Models\Address', 'id', 'address_id');
    }

    function child_company_single(){
        return $this->hasOne('App\Models\Company', 'id', 'comp_child_id');
    }

    function project_company() {
        return $this->hasOne('App\Models\Project_company', 'id', 'proj_comp_id');
    }
}
