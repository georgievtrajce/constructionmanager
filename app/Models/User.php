<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password', 'name', 'title', 'address_id', 'office_phone', 'cell_phone', 'fax',
		'comp_id',
		'type', 'active', 'confirmation_code', 'confirmed'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	//Relations
	public function company()
	{
		return $this->hasOne('App\Models\Company', 'id', 'comp_id');
	}

	public function address()
	{
		return $this->hasOne('App\Models\Address', 'id', 'address_id');
	}

	//relationships
	public function permissions()
	{
		return $this->hasMany('App\Models\User_permission', 'user_id', 'id');
	}

	public function projectPermissions()
	{
		return $this->hasMany('App\Models\Project_permission', 'user_id', 'id');
	}

	public function delete()
	{
		$this->permissions()->delete();
		$this->projectPermissions()->delete();

		return parent::delete();
	}

}
