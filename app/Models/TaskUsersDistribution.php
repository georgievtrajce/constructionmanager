<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskUsersDistribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tasks_users_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['task_user_id'];
}
