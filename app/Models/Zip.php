<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zip extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'zips';

}
