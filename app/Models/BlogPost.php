<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'blog_posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['proj_id', 'comp_id', 'title', 'content', 'author_id', 'ft_id'];


    //select project for blog post
    public function project()
    {
        return $this->hasOne('App\Models\Project','id', 'proj_id');
    }

    //blog post author
    public function author()
    {
        return $this->hasOne('App\Models\User','id', 'author_id');
    }
}
