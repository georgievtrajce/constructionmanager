<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Submittal extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'submittals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_version',
        'name',
        'number',
        'subject',
        'sent_via',
//        'submitted_for',
        'submittal_type',
        'quantity',
        'mf_number',
        'mf_title',
        'sub_id',
        //'sub_office_id',
        'sub_contact_id',
        'recipient_id',
        //'recipient_office_id',
        'recipient_contact_id',
        'gc_id',
        'make_me_gc',
        'proj_id',
        'note',
        'comp_id',
        'user_id',
        'self_performed',
        'send_notif_subcontractor',
        'send_notif_recipient',
        'created_at',
        'updated_at'
    ];

    //relations
    public function subcontractor()
    {
        return $this->hasOne('App\Models\Address_book','id','sub_id');
    }

    public function subcontractor_contact()
    {
        return $this->hasOne('App\Models\Ab_contact','id','sub_contact_id');
    }

    public function recipient()
    {
        return $this->hasOne('App\Models\Address_book','id','recipient_id');
    }

    public function recipient_contact()
    {
        return $this->hasOne('App\Models\Ab_contact','id','recipient_contact_id');
    }

    public function general_contractor()
    {
        return $this->hasOne('App\Models\Address_book','id','gc_id');
    }

    public function project()
    {
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

    public function submittals_versions()
    {
        return $this->hasMany('App\Models\Submittal_version','submittal_id','id');
    }

    public function last_version()
    {
        return $this->hasOne('App\Models\Submittal_version','submittal_id','id')->latest();
    }

    public function last_version_report()
    {
        return $this->last_version();
    }

    // override existing delete method.
    public function delete()
    {
        // delete all associated submittal versions
        $this->submittals_versions()->delete();

        // delete the submittal
        return parent::delete();
    }

}
