<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expediting_report_status extends Model {

    protected $table = 'expediting_report_statuses';

    protected $fillable = array('name','short_name');

}
