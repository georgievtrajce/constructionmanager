<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Rfi_version extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rfis_versions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['rfi_id','rfi_number','cycle_no','sent_appr','rec_appr','subm_sent_sub','rec_sub','status_id','question', 'answer','created_at','updated_at'];

    public function rfi()
    {
        return $this->belongsTo('App\Models\Rfi','rfi_id','id');
    }

    public function file()
    {
        return $this->belongsToMany('App\Models\Project_file','rfi_version_files', 'rfi_version_id', 'file_id');
    }

    public function smallFile()
    {
        return $this->belongsToMany('App\Models\Project_file','rfi_version_files', 'rfi_version_id', 'file_id')
                ->whereRaw('CAST(size AS UNSIGNED) < 250000000');
    }

    public function status()
    {
        return $this->hasOne('App\Models\Rfi_status','id','status_id');
    }

    public function rfiVersionApprUsers()
    {
        return $this->hasMany('App\Models\Rfi_version_distribution','rfi_version_id','id')
            ->where('type', '=', 'appr');
    }

    public function rfiVersionSubcUsers()
    {
        return $this->hasMany('App\Models\Rfi_version_distribution','rfi_version_id','id')
            ->where('type', '=', 'subc');
    }

    public function transmittalSubmSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 7)
            ->where('date_type', '=', 'subm_sent_sub');
    }

    public function transmittalSentFile()
    {
        return $this->belongsToMany('App\Models\File', 'transmittals_logs',
            'version_id', 'file_id')
            ->where('file_type_id', '=', 7)
            ->where('date_type', '=', 'sent_appr');
    }
}
