<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Master_Format extends Model {

	protected $table = 'master_format';
    protected $fillable = array('number','title', 'master_format_type_id');

    public function scopeNumbers($query, $type, $masterFormatTypeId = null)
    {
        if (!empty($masterFormatTypeId)) {
            $query = $query->where('master_format_type_id', '=', $masterFormatTypeId);
        }

        return $query->where('number', 'LIKE', $type.'%')->orderby('number')->take(10)->get();
    }

    public function scopeTitles($query, $type, $masterFormatTypeId = null)
    {
        if (!empty($masterFormatTypeId)) {
            $query = $query->where('master_format_type_id', '=', $masterFormatTypeId);
        }

        return $query->where('title', 'LIKE', $type.'%')->orderby('title')->take(10)->get();
    }

    public function scopeGetEntryByNumber($query, $type)
    {
        return $query->where('number', '=', $type)->first();
    }

    public function scopeGetEntryByTitle($query, $type)
    {
        return $query->where('title', '=', $type)->first();
    }

}
