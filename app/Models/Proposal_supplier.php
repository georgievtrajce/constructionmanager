<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Proposal_supplier extends Model
{
    protected $table = 'proposal_suppliers';
    protected $fillable = array(
        'proj_bidder_id',
        'ab_id',
        'ab_cont_id',
        'prop_id',
        'proposal_1',
        'proposal_2',
        'proposal_final',
        'addr_id'
    );

    function project_bidder(){
        return $this->hasOne('App\Models\Project_bidder','id','proj_bidder_id');
    }

    function bidder_permissions(){
        return $this->hasMany('App\Models\Proposal_supplier_permission', 'prop_supp_id', 'id');
    }

    function address(){
        return $this->hasOne('App\Models\Address','id','addr_id');
    }

    function address_book(){
        return $this->hasOne('App\Models\Address_book','id','ab_id');
    }

    function address_book_contact(){
        return $this->hasOne('App\Models\Ab_contact','id','ab_cont_id');
    }

    function files(){
        return $this->hasMany('App\Models\Proposal_file', 'prop_supp_id', 'id');
    }

    function proposal(){
        return $this->hasOne('App\Models\Proposal','id','prop_id');
    }

    function delete(){
        // delete all associated files
        $files = $this->files()->get();
        foreach ($files as $file) {
            //delete all permissions for files
            $file->delete();
        }
        $this->files()->delete();

        return parent::delete();
    }

}