<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proposal_supplier_permission extends Model {

    protected $table = 'proposal_supplier_permissions';
    protected $fillable = array(
        'bidder_id',
        'ab_id',
        'proj_id',
        'comp_parent_id',
        'comp_child_id',
        'read',
        'pco_permission_type',
        'write',
        'delete',
        'entity_type',
        'entity_id'
    );

    public function module()
    {
        return $this->hasOne('App\Models\Module', 'id', 'entity_id');
    }

    public function fileType()
    {
        return $this->hasOne('App\Models\File_type', 'id', 'entity_id');
    }

    function bidder(){
        return $this->belongsTo('App\Models\Proposal_supplier', 'bidder_id', 'id');
    }

}
