<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mf_address_book extends Model {

    protected $table = 'mf_address_book';
    protected $fillable = array('ab_id','mf_id');

}
