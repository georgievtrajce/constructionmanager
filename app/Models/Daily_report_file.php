<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_file extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        'file_type_id',
        'file_id',
        'name',
        'description',
        'file_name',
        'file_mime_type',
        'created_at',
        'updated_at'
    ];

    public function projectFile()
    {
        return $this->hasOne('App\Models\Project_file', 'id', 'file_id');
    }

}
