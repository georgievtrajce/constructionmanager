<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Task extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'parent_id','title', 'due_date', 'status', 'mf_id', 'note', 'type_id', 'project_id', 'pco_id', 'rfi_id',
        'submittal_id', 'contract_id', 'bid_id', 'comp_id', 'added_by'];

    public function project()
    {
        return $this->hasOne('App\Models\Project','id','project_id');
    }

    public function pco()
    {
        return $this->hasOne('App\Models\Pco','id','pco_id');
    }

    public function rfi()
    {
        return $this->hasOne('App\Models\Rfi','id','rfi_id');
    }

    public function submittal()
    {
        return $this->hasOne('App\Models\Submittal','id','submittal_id');
    }

    public function contract()
    {
        return $this->hasOne('App\Models\Contract','id','contract_id');
    }

    public function bid()
    {
        return $this->hasOne('App\Models\Bid','id','bid_id');
    }

    public function material()
    {
        return $this->hasOne('App\Models\Expediting_report','id','material_id');
    }

    public function subtasks()
    {
        return $this->hasMany('App\Models\Task','parent_id','id')
            ->with('taskUsers.users.company')
            ->with('taskUsers.abUsers.addressBook');
    }

    public function taskUsers()
    {
        return $this->hasMany('App\Models\TasksUser','task_id','id');
    }

    public function addedByUser()
    {
        return $this->hasOne('App\Models\User','id','added_by');
    }

    public function taskFiles()
    {
        return $this->hasMany('App\Models\TasksFile','task_id','id');
    }
}
