<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Address_book extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'address_book';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address_id', 'mf_id', 'owner_comp_id', 'synced_comp_id', 'note', 'total_rating'];

    //Relationships
    public function default_categories()
    {
            return $this->belongsToMany('App\Models\Default_category','ab_default_categories','ab_id','default_category_id');
    }

    public function custom_categories()
    {
            return $this->belongsToMany('App\Models\Custom_category','ab_custom_categories','ab_id','custom_category_id');
    }

    public function submittal()
    {
        return $this->belongsToMany('App\Models\Submittal','sub_id','id');
    }

    public function master_format_items()
    {
        return $this->belongsToMany('App\Models\Master_Format','mf_address_book','ab_id','mf_id');
    }

    public function addresses()
    {
        return $this->hasMany('App\Models\Address','ab_id','id');
    }

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact','ab_id','id');
    }

    public function address_book()
    {
        return $this->hasOne('App\Models\Company','id','synced_comp_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','synced_comp_id');
    }

    //address book entry relation with companies module
    public function companies()
    {
        return $this->hasMany('App\Models\Project_company','ab_id','id');
    }

    //address book entry relation with companies module (updated)
    public function project_company()
    {
        return $this->hasOne('App\Models\Project_company','ab_id','id');
    }

    //address book entry relation with proposals module (proposal suppliers)
    public function proposal_suppliers()
    {
        return $this->hasMany('App\Models\Proposal_supplier','ab_id','id');
    }

    //address book entry relation with contracts module
    public function contracts()
    {
        return $this->hasMany('App\Models\Contract','ab_id','id');
    }

    //address book entry relation with submittals module
    public function submittals()
    {
        return $this->hasMany('App\Models\Submittal','sub_id','id');
    }

    //address book entry relation with materials and services module
    public function materials_and_services()
    {
        return $this->hasMany('App\Models\Expediting_report','ab_id','id');
    }

    //address book entry relation with project owner
    public function project_owner()
    {
        return $this->hasMany('App\Models\Project','owner_id','id');
    }

    //address book entry relation with project architect
    public function project_architect()
    {
        return $this->hasMany('App\Models\Project','architect_id','id');
    }

    //select only id and title of addresses. used in autocomplete in proposals
    public function addresses_title()
    {
        return $this->hasMany('App\Models\Address','ab_id','id')->select(array('id', 'office_title'));
    }
    //select only id and name of contacts. used in autocomplete in proposals
    public function contacts_name()
    {
        return $this->hasMany('App\Models\Contact','ab_id','id')->select(array('id', 'name'));
    }

    // override existing delete method.
    // invoke when we call $addressBookEntry->delete().
    public function delete()
    {
        // delete all associated default categories
        $this->default_categories()->detach();

        // delete all associated custom categories
        $this->custom_categories()->detach();

        // delete all associated master format items
        $this->master_format_items()->detach();

        // delete all associated addresses
        $this->addresses()->delete();

        // delete all associated contacts
        $this->contacts()->delete();

        // delete the address book entry
        return parent::delete();
    }

    //Scopes
    /**
     * @param $query
     * @param $id
     * @param $companyId
     * @return mixed
     */
    public function scopeGetBasicInfo($query, $id, $companyId)
    {
        return $query->with('default_categories')->with('custom_categories')->with('master_format_items')
            ->with('address_book')->with('contacts')->where('owner_comp_id','=',$companyId)->where('id','=',$id)->first();
    }

    /**
     * @param $query
     * @param $companyId
     * @return mixed
     */

    public function scopeAllEntriesWithMasterFormat($query, $companyId)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);

        if ($sortArr[0] == 'mf.number') {
            $query = $query->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        if ($sortArr[0] == 'categories.name') {

            $query = $query->leftJoin(DB::raw('(Select custom_categories.name as name, ab_custom_categories.ab_id  from ab_custom_categories, custom_categories WHERE ab_custom_categories.custom_category_id = custom_categories.id UNION Select default_categories.name as name, ab_default_categories.ab_id  from ab_default_categories, default_categories WHERE ab_default_categories.default_category_id = default_categories.id) as categories'), function ($join) {
                $join->on('address_book.id', '=', 'categories.ab_id');
            });
        }

        return $query->with('default_categories')
                     ->with('custom_categories')
                     ->with('master_format_items')
                     ->with('company.projects')
                     ->where('owner_comp_id','=',$companyId);
    }

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeWhereDefaultCategoryExist($query, $id)
    {
        return $query->where('owner_comp_id','=',Auth::user()->comp_id)->whereHas('default_categories', function ($q) use ($id) {
                $q->where('default_category_id', '=', $id);
        })->with('custom_categories')->orderBy('id','DESC');
    }

    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function scopeWhereCustomCategoryExist($query, $id)
    {
        return $query->where('owner_comp_id','=',Auth::user()->comp_id)->whereHas('custom_categories', function ($q) use ($id) {
                $q->where('custom_category_id', '=', $id);
        })->with('default_categories')->orderBy('id','DESC');
    }

    /**
     * @param $query
     * @param $segment
     * @param $id
     * @return mixed
     */
    public function scopeMasterFormatNumberSegmentFilter($query, $segment, $id)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);

        if ($sortArr[0] == 'mf.number') {
            $query = $query->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        if ($sortArr[0] == 'categories.name') {

            $query = $query->leftJoin(DB::raw('(Select custom_categories.name as name, ab_custom_categories.ab_id  from ab_custom_categories, custom_categories WHERE ab_custom_categories.custom_category_id = custom_categories.id UNION Select default_categories.name as name, ab_default_categories.ab_id  from ab_default_categories, default_categories WHERE ab_default_categories.default_category_id = default_categories.id) as categories'), function ($join) {
                $join->on('address_book.id', '=', 'categories.ab_id');
            });
        }

        return $query->with('default_categories')
                     ->with('custom_categories')
                     ->where('owner_comp_id','=',$id)
                     ->whereHas('master_format_items', function($q) use ($segment)
        {
            $q->where('number', 'like', $segment.'%');

        });
    }

    /**
     * @param $query
     * @param $segment
     * @param $id
     * @return mixed
     */
    public function scopeMasterFormatTitleSegmentFilter($query, $segment, $id)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);

        if ($sortArr[0] == 'mf.number') {
            $query = $query->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        if ($sortArr[0] == 'categories.name') {

            $query = $query->leftJoin(DB::raw('(Select custom_categories.name as name, ab_custom_categories.ab_id  from ab_custom_categories, custom_categories WHERE ab_custom_categories.custom_category_id = custom_categories.id UNION Select default_categories.name as name, ab_default_categories.ab_id  from ab_default_categories, default_categories WHERE ab_default_categories.default_category_id = default_categories.id) as categories'), function ($join) {
                $join->on('address_book.id', '=', 'categories.ab_id');
            });
        }

        return $query->with('default_categories')
                     ->with('custom_categories')
                     ->where('owner_comp_id','=',$id)
                     ->whereHas('master_format_items', function($q) use ($segment)
                    {
                        $q->where('title', 'like', '%'.$segment.'%');

                    });
    }

    /**
     * @param $query
     * @param $number
     * @param $id
     * @return mixed
     */
    public function scopeMasterFormatNumberFilter($query, $number, $id)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);

        if ($sortArr[0] == 'mf.number') {
            $query = $query->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        if ($sortArr[0] == 'categories.name') {

            $query = $query->leftJoin(DB::raw('(Select custom_categories.name as name, ab_custom_categories.ab_id  from ab_custom_categories, custom_categories WHERE ab_custom_categories.custom_category_id = custom_categories.id UNION Select default_categories.name as name, ab_default_categories.ab_id  from ab_default_categories, default_categories WHERE ab_default_categories.default_category_id = default_categories.id) as categories'), function ($join) {
                $join->on('address_book.id', '=', 'categories.ab_id');
            });
        }

        return $query->with('default_categories')->with('custom_categories')->where('owner_comp_id','=',$id)->whereHas('master_format_items', function($q) use ($number)
        {
            $q->where('number', '=', $number);

        });
    }

    /**
     * @param $query
     * @param $title
     * @param $id
     * @return mixed
     */
    public function scopeMasterFormatTitleFilter($query, $title, $id)
    {
        $sort = Input::get('sort', 'name-asc');
        $sortArr = explode('-', $sort);

        if ($sortArr[0] == 'mf.number') {
            $query = $query->leftJoin('mf_address_book as mab','address_book.id','=','mab.ab_id')
                ->leftJoin('master_format as mf','mab.mf_id','=','mf.id');
        }

        if ($sortArr[0] == 'categories.name') {

            $query = $query->leftJoin(DB::raw('(Select custom_categories.name as name, ab_custom_categories.ab_id  from ab_custom_categories, custom_categories WHERE ab_custom_categories.custom_category_id = custom_categories.id UNION Select default_categories.name as name, ab_default_categories.ab_id  from ab_default_categories, default_categories WHERE ab_default_categories.default_category_id = default_categories.id) as categories'), function ($join) {
                $join->on('address_book.id', '=', 'categories.ab_id');
            });
        }
        
        return $query->with('default_categories')->with('custom_categories')->where('owner_comp_id','=',$id)->whereHas('master_format_items', function($q) use ($title)
        {
            $q->where('title', '=', $title);

        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function master_format_item()
    {
            return $this->hasOne('App\Models\Master_Format', 'id', 'mf_id');
    }

}
