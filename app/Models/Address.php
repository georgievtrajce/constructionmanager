<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'addresses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['office_title', 'number', 'street', 'zip', 'city', 'state', 'comp_id', 'ab_id', 'user_id', 'proj_id', 'subcontractor_id'];

    function project(){
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

    public function address_book()
    {
        return $this->hasOne('App\Models\Address_book','id','ab_id');
    }

    public function address_book_contacts()
    {
        return $this->hasOne('App\Models\Address_book','id','ab_id');
    }

    //address book contacts addresses
    public function ab_contacts()
    {
        return $this->hasMany('App\Models\Ab_contact','address_id','id');
    }

    //project companies addresses
    public function project_companies()
    {
        return $this->hasMany('App\Models\Project_company','addr_id','id');
    }

    //project proposal suppliers addresses
    public function proposal_suppliers()
    {
        return $this->hasMany('App\Models\Proposal_supplier','addr_id','id');
    }

    //registered users addresses
    public function users()
    {
        return $this->hasMany('App\Models\User','address_id','id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

}
