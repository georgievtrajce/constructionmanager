<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project_files_distribution extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_files_distribution';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['project_file_id','ab_cont_id', 'user_id', 'created_at'];

    public function users()
    {
        return $this->hasMany('App\Models\User','id','user_id');
    }

    public function abUsers()
    {
        return $this->hasMany('App\Models\Ab_contact','id','ab_cont_id');
    }
}
