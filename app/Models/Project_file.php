<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Project_file extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'files';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ft_id','transmittal','user_id','comp_id','proj_id','name','number','file_name','version_date_connection','size'];

    //relations
    public function type()
    {
        return $this->hasOne('App\Models\File_type','id','ft_id');
    }

    public function submittal_version()
    {
        //return $this->belongsTo('App\Models\Submittal_version','id','file_id');
        return $this->belongsToMany('App\Models\Submittal_version','submittal_version_files', 'file_id', 'submittal_version_id');
    }

    public function proposal_file()
    {
        return $this->belongsTo('App\Models\Proposal_file','id','file_id');
    }

    public function contract_file()
    {
        return $this->belongsTo('App\Models\Contract_file','id','file_id');
    }

    public function user()
    {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

    public function project()
    {
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

    public function shares()
    {
        return $this->hasMany('App\Models\File_permission','file_id','id');
    }

    public function companies()
    {
        return $this->belongsToMany('App\Models\Company', 'file_permissions', 'file_id', 'comp_id');
    }

    public function share_company()
    {
        return $this->belongsToMany('App\Models\Company','file_permissions', 'file_id', 'share_comp_id');
    }

    public function delete()
    {
        // delete all permissions for file
        $this->shares()->delete();

        //delete file
        return parent::delete();
    }

    public function projectFileUsers()
    {
        return $this->hasMany('App\Models\Project_files_distribution','project_file_id','id');
    }
}
