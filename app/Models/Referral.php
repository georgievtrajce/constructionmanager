<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'referrals';
    protected $fillable = array('comp_from', 'comp_to', 'token', 'token_active', 'email');

    function companyFrom(){
        return $this->hasOne('App\Models\Company','id','comp_from');
    }
    function companyTo(){
        return $this->hasOne('App\Models\Company','id','comp_to');
    }

}