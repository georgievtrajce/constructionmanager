<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Contract_item extends Model
{
    protected $table = 'contract_items';
    protected $fillable = array('mf_title','mf_number', 'name','contr_id');


    function items(){
        return $this->hasMany('App\Models\Contract_item','contr_id','id');
    }

    function delete(){

        // delete all associated suppliers
        $this->items()->delete();

        return parent::delete();
    }

}