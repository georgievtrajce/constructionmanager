<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Daily_report_contractor_equipment extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'daily_report_equipment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proj_id',
        'comp_id',
        'user_id',
        'daily_report_id',
        'equipment',
        'quality',
        'selected_comp_id',
        'hours',
        'description',
        'created_at',
        'updated_at'
    ];

}
