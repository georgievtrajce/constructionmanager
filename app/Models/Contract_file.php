<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class Contract_file extends Model
{
    protected $table = 'contract_files';
    protected $fillable = array('contr_id', 'file_id', 'file_cost');

    function files(){
        return $this->hasOne('App\Models\File','id','file_id');
    }

    function smallFiles(){
        return $this->hasOne('App\Models\File','id','file_id')
            ->whereRaw('CAST(size AS UNSIGNED) < 250000000');
    }

    function delete(){
        // delete all associated files
        $this->files()->delete();

        return parent::delete();
    }

}