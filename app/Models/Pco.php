<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pco extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pcos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'comp_id',
        'user_id',
        'name',
        'number',
        'subject',
        'sent_via',
        'gc_id',
        'make_me_gc',
        'proj_id',
        'reason_for_change',
        'description_of_change',
        'recipient_id',
        'recipient_contact_id',
        'created_at',
        'updated_at'
    ];

    //relations
    public function project()
    {
        return $this->hasOne('App\Models\Project','id','proj_id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company','id','comp_id');
    }

    public function latest_subcontractor()
    {
        return $this->hasOne('App\Models\Pco_subcontractor_recipient','pco_id','id')->where('ab_subcontractor_id','!=',0)->latest();
    }

    public function subcontractors()
    {
        return $this->hasMany('App\Models\Pco_subcontractor_recipient','pco_id','id')->where('ab_subcontractor_id','!=',0)->orWhere('self_performed','!=',0);
    }

    public function recipient()
    {
        return $this->hasOne('App\Models\Pco_subcontractor_recipient','pco_id','id')->where('ab_recipient_id','!=',0);
    }

    public function general_contractor()
    {
        return $this->hasOne('App\Models\Address_book','id','gc_id');
    }

}
