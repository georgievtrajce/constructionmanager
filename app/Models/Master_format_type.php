<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Master_format_type extends Model {

	protected $table = 'master_format_types';
    protected $fillable = array('name','year');

}
