<?php namespace App\Exceptions;

use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException as ModelNotFoundException;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NotFoundHttpException;


class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
        $code = 500;
        $errorMessage = "Something went wrong. Please contact our administrator.";
		if ($e instanceof ModelNotFoundException)
		{
		    $code = 404;
			$errorMessage = "The page you requested does not exist.";

		}

		if ($e instanceof FileNotFoundException)
		{
            $code = 404;
            $errorMessage = "The file you are looking for does not exist. Please contact us if you have unexpected issues with your project files.";
        }

		if (($e instanceof NotFoundHttpException) || ($e instanceof BadMethodCallException))
		{
            $code = 405;
            $errorMessage = "The route you are looking for does not exist.";

		}

        Log::critical ('Critical error: '.$code, ['message' => $e->getMessage(), 'file' => $e->getFile()]);
        return parent::render($request, $e);
        return redirect()->action('ErrorController@show', compact('code', 'errorMessage'));
	}

}
