<?php namespace App\Services;

use App\Models\User;
use App\Modules\Registration\Implementations\WrapRegistration;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Contracts\Auth\Registrar as RegistrarContract;

class Registrar implements RegistrarContract {

	/**
	 * Get a validator for an incoming registration request.
	 *
	 * @param  array  $data
	 * @return \Illuminate\Contracts\Validation\Validator
	 */
	public function validator(array $data)
	{
		return Validator::make($data, [
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
			'company_name' => 'required|max:255',
			'company_logo' => 'mimes:jpeg,jpg,gif,png|max:2000',
			'company_addr_office_title_req' => 'required|max:255',
			'company_addr_street_req' => 'required|max:255',
			'company_addr_town_req' => 'required|max:45',
			'company_addr_state_req' => 'required||max:45',
			'company_addr_zip_code_req' => 'required|max:45',
			'admin_name' => 'required|max:255',
			'admin_title' => 'required|max:255',
			'admin_office_phone' => 'required|max:45',
			'subscription_type' => 'required|exists:subscription_types,id',
			'terms_agree' => 'required',
            'g-recaptcha-response' => 'required|recaptcha'
		]);
	}

	/**
	 * Create a new user instance after a valid registration.
	 *
	 * @param  array $data
	 * @return User
	 */
	public function create(array $data)
	{
		$sub_type = $data['subscription_type'];
		$data['account_type'] = Config::get('subscription_levels.account_type.payed');

		//implement different subscription level type registration
		$wrapReg = new WrapRegistration();
		$result = $wrapReg->doRegister(ClassMap::instance(Config::get('classmap.registration.'.$sub_type)), $data);
		return $result;
	}

}
