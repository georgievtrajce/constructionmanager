<?php
namespace App\Services;

use App\Models\Module;
use App\Models\Project;
use App\Models\Project_company;
use App\Models\Project_company_permission;
use App\Models\User_permission;
use App\Models\Project_permission;
use App\Models\File_type;
use Auth;
use Request;
use Config;

class Permissions {

    /**
     * @param $rwd - read, write or delete argument
     * @param $moduleOrFile - module or file for checking permissions
     * @return bool
     */
    public static function can($rwd, $moduleOrFile)
    {
        //get all modules
        $modulesArray = Module::where('id', '>', 0)->select('display_name')->get()->toArray();

        //get all files
        $filesArray = File_type::where('id', '>', 0)->select('display_name')->get()->toArray();

        $projectModules = array();
        $globalModules = array();
        $files = array();


        if (Auth::user()->hasRole('Company Admin')) {
            return true;
        }
        //make one dimensional arrays
        foreach($modulesArray as $module) {
            if (($module['display_name'] == 'address-book')) {
                array_push($globalModules, $module['display_name']);
            } else {
                array_push($projectModules, $module['display_name']);
            }

        }

        foreach($filesArray as $file) {
            array_push($files, $file['display_name']);
        }
        if (in_array($moduleOrFile, $globalModules)) {
            //if it is module, check with table modules
            $hasPermission = User_permission::join('modules', 'user_permissions.entity_id', '=', 'modules.id')
                ->select('user_permissions.'.$rwd)
                ->where('user_permissions.user_id', '=', Auth::user()->id)
                ->where('modules.display_name', '=', $moduleOrFile)
                ->first();
            if (!$hasPermission) {
                return false;
            }
            return $hasPermission[$rwd];

        } else if (in_array($moduleOrFile, $projectModules)) {
            $projectID = Request::segment(2);
            //if it is module, check with table modules
            $hasPermission = Project_permission::join('modules', 'project_permissions.entity_id', '=', 'modules.id')
                ->select('project_permissions.'.$rwd)
                ->where('project_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                ->where('project_permissions.user_id', '=', Auth::user()->id)
                ->where('project_permissions.proj_id', '=', $projectID)
                ->where('modules.display_name', '=', $moduleOrFile)
                ->first();

            if (!$hasPermission) {
                return false;
            }
            return $hasPermission[$rwd];


        } else if (in_array($moduleOrFile, $files)) {
            $projectID = Request::segment(2);
            //if it is file, check with table file types
            $hasPermission = Project_permission::join('file_types', 'project_permissions.entity_id', '=', 'file_types.id')
                ->where('project_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                ->where('project_permissions.user_id', '=', Auth::user()->id)
                ->where('project_permissions.proj_id', '=', $projectID)
                ->where('file_types.display_name', '=', $moduleOrFile)
                ->first();
            if (!$hasPermission) {
                return false;
            }
            return $hasPermission[$rwd];
        }
        return false;
    }

    public static function unshared($projectId)
    {
        $project = Project::where('id','=',$projectId)->with('projectSubcontractors')->with('projectBiddersSelf')->firstOrFail();

        if ($project->comp_id == Auth::user()->comp_id) {
            return true;
        }

        if (count($project->projectBiddersSelf) > 0) {
            return true;
        }

        foreach ($project->projectSubcontractors as $subcontractor) {
            if ($subcontractor->comp_child_id == Auth::user()->comp_id &&
                $subcontractor->status == Config::get('constants.projects.shared_status.unshared')) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param $rwd - read, write or delete argument
     * @param $module - module or file for checking permissions
     * @param $addressBookId - id from addressBook table
     * @return bool
     */
    public static function canSendNotification($rwd, $module, $addressBookId) {
        $projectID = Request::segment(2);
        $company = Project_company::where('ab_id', '=',  $addressBookId)->where('proj_id', '=', $projectID)->first();

        if($company) {
            $hasPermission = Project_company_permission::join('modules', 'project_company_permissions.entity_id', '=', 'modules.id')
                ->select('project_company_permissions.' . $rwd)
                ->where('project_company_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                ->where('project_company_permissions.proj_comp_id', '=', $company->id)
                ->where('project_company_permissions.proj_id', '=', $projectID)
                ->where('modules.display_name', '=', $module)
                ->first();

            if (!$hasPermission) {
                return false;
            }
            return $hasPermission[$rwd];
        }
        return false;
    }
}