<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 10/28/2015
 * Time: 10:44 AM
 */

namespace App\Services\Files;


use App\Models\File_type;
use App\Models\Project;
use App\Models\Project_file;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class StoreContractFile implements FileStoreInterface {

    private $userId;
    private $companyId;

    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
        $this->userId = Auth::user()->id;
    }

    /**
     * Storing new contract file
     * @param $params
     * @return mixed
     */
    public function storeFile($params)
    {
        $transferLimitation = new DataTransferLimitation();
        //check upload file allowance
        $uploadTransferAllowance = $transferLimitation->checkUploadTransferAllowance($params['filesize']);
        $storageAllowance = $transferLimitation->checkStorageAllowance($params['filesize']);
        if ($uploadTransferAllowance && $storageAllowance) {
            $fileType = File_type::where('display_name', '=', $params['projectFileType'])->first();

            if (empty($params['exsistingFileId'])) {
                $file = Project_file::create([
                    'ft_id' => $fileType->id,
                    'user_id' => $this->userId,
                    'comp_id' => $this->companyId,
                    'proj_id' => $params['projectId'],
                    'size' => $params['filesize']
                ]);
            } else {
                $file = Project_file::where('id', '=', $params['exsistingFileId'])->first();
                $disk = Storage::disk('s3');
                $filePath = 'company_' . $this->companyId . '/project_' . $file->proj_id . '/contracts/contract_' . $params['contractId'] . '/' . $file->file_name;
                if ($disk->exists($filePath)) {
                    $disk->delete($filePath);
                }
            }

            //get project file original name
            $originalName = $params['filename'];

            //parse the original name to get only the name without extension
            $parseOriginalName = substr($originalName, 0, strrpos($originalName, "."));

            //get current project upc
            $project = Project::where('id', '=', $params['projectId'])
                ->select(array('upc_code'))
                ->firstOrFail();

            //create new project file name
            $filename = $parseOriginalName . '_' . $project->upc_code . '_' . $file->id . '.' . $params['type'];

            $file->file_name = $filename;
            $file->save();

            $fullFilePath = 'company_' . $this->companyId . '/project_' . $params['projectId'] . '/contracts/contract_' . $params['contractId'] . '/' . $filename;
            $transferLimitation->increaseUploadTransfer($params['filesize']);

            return Response::json(array('not_allowed' => 0, 'full_file_path' => $fullFilePath, 'file_id' => $file->id, 'file_name' => $filename));
        }

        $type = "data transfer";
        if(!$storageAllowance) {
            $type = "storage";
        }

        return Response::json(array('not_allowed' => 1, 'not_allowed_type' => $type));
    }

}