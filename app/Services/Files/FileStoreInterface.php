<?php
namespace App\Services\Files;


interface FileStoreInterface {

    public function storeFile($params);

}