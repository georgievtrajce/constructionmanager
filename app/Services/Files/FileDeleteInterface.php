<?php
namespace App\Services\Files;


interface FileDeleteInterface
{
    public function deleteSingleModuleFile($params);
}