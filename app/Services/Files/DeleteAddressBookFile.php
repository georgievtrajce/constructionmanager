<?php
namespace App\Services\Files;


use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;

class DeleteAddressBookFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new FilesRepository();
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSingleAddressBookFile($params);
    }

}