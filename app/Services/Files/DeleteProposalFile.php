<?php
namespace App\Services\Files;


use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;

class DeleteProposalFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new ProposalsRepository(new ProjectSubcontractorsRepository(), new ProjectPermissionsRepository());
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteProposalFile($params);
    }

}