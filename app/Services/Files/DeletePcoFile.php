<?php
namespace App\Services\Files;


use App\Modules\Pcos\Repositories\PcosRepository;

class DeletePcoFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new PcosRepository();
    }

    /**
     * Delete single PCO file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSinglePcoFile($params);
    }

}