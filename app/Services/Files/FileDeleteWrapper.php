<?php
namespace App\Services\Files;

class FileDeleteWrapper
{
    /**
     * Get the delete file service (class)
     * @param FileDeleteInterface $deleteInterface
     * @param $params
     * @return mixed
     */
    public function getDeleteService(FileDeleteInterface $deleteInterface, $params)
    {
        return $deleteInterface->deleteSingleModuleFile($params);
    }
}