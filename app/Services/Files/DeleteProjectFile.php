<?php
namespace App\Services\Files;


use App\Modules\Project_files\Repositories\ProjectFilesRepository;

class DeleteProjectFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new ProjectFilesRepository();
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSingleProjectFile($params);
    }

}