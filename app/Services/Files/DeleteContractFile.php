<?php
namespace App\Services\Files;


use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;

class DeleteContractFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new ContractsRepository(new ProjectSubcontractorsRepository(), new ProjectPermissionsRepository());
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSingleContractFile($params);
    }

}