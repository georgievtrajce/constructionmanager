<?php
namespace App\Services\Files;


use App\Modules\Submittals\Repositories\SubmittalsRepository;

class DeleteSubmittalFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new SubmittalsRepository();
    }

    /**
     * Delete single submittal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSingleSubmittalFile($params);
    }

}