<?php
namespace App\Services\Files;


use App\Modules\Rfis\Repositories\RfisRepository;

class DeleteRfiFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new RfisRepository();
    }

    /**
     * Delete single RFI file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteSingleRfiFile($params);
    }

}