<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 10/28/2015
 * Time: 10:44 AM
 */

namespace App\Services\Files;


use App\Models\Ab_file;
use App\Models\File;
use App\Models\File_type;
use App\Models\Project;
use App\Models\Project_file;
use App\Models\TasksFile;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class StoreTasksFile implements FileStoreInterface {

    private $userId;
    private $companyId;

    public function __construct()
    {
        $this->companyId = Auth::user()->comp_id;
        $this->userId = Auth::user()->id;
    }

    /**
     * Storing new proposal file
     * @param $params
     * @return mixed
     */
    public function storeFile($params)
    {
        $transferLimitation = new DataTransferLimitation();
        //check upload file allowance
        $uploadTransferAllowance = $transferLimitation->checkUploadTransferAllowance($params['filesize']);
        $storageAllowance = $transferLimitation->checkStorageAllowance($params['filesize']);
        if ($uploadTransferAllowance && $storageAllowance) {

            if (empty($params['exsistingFileId'])) {
                $file = TasksFile::create([
                    'comp_id' => $this->companyId,
                    'size' => $params['filesize']
                ]);
            } else {
                $file = TasksFile::where('id', '=', $params['exsistingFileId'])->first();
                $disk = Storage::disk('s3');
                $filePath = 'company_' . $this->companyId . '/tasks/' . $file->name;
                if ($disk->exists($filePath)) {
                    $disk->delete($filePath);
                }
            }

            //get project file original name
            $originalName = $params['filename'];

            //parse the original name to get only the name without extension
            $parseOriginalName = substr($originalName, 0, strrpos($originalName, "."));

            //create new project file name
            $filename = $parseOriginalName . '_' . $file->id . '.' . $params['type'];

            $file->file_type = $params['type'];
            $file->file_name = $filename;
            $file->save();

            $fullFilePath = 'company_' . $this->companyId . '/tasks/' . $filename;
            $transferLimitation->increaseUploadTransfer($params['filesize']);

            return Response::json(array('not_allowed' => 0, 'full_file_path' => $fullFilePath, 'file_id' => $file->id, 'file_name' => $file->file_name, 'i' => $params['i']));
        }

        $type = "data transfer";
        if(!$storageAllowance) {
            $type = "storage";
        }

        return Response::json(array('not_allowed' => 1, 'not_allowed_type' => $type));
    }

}