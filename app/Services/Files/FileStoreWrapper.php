<?php
namespace App\Services\Files;

class FileStoreWrapper {

    public function implement(FileStoreInterface $storeInterface, $params)
    {
        return $storeInterface->storeFile($params);
    }

}