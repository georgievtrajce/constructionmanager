<?php
namespace App\Services\Files;


use App\Modules\Tasks\Repositories\TasksRepository;

class DeleteTasksFile implements FileDeleteInterface
{

    private $repo;

    public function __construct()
    {
        $this->repo = new TasksRepository();
    }

    /**
     * Delete single proposal file
     * @param $params
     * @return mixed
     */
    public function deleteSingleModuleFile($params)
    {
        return $this->repo->deleteTasksFile($params);
    }

}