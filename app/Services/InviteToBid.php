<?php
namespace App\Services;


use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Rhumsaa\Uuid\Uuid;

class InviteToBid
{

    private $proposalsRepo;
    private $sendEmail;

    public function __construct()
    {
        $this->proposalsRepo = new ProposalsRepository();
        $this->sendEmail = new Mailer();
    }

    /**
     * Update proposal suppliers table with invitation data
     * @param $bidder
     * @param $projectId
     * @param $token
     * @return mixed
     */
    public function properInviteToBid($bidder, $projectId, $token, $bidId)
    {
        $projectBidder = $this->proposalsRepo->storeProjectBidder($bidder, $projectId, $token);

        if (!is_null($projectBidder)) {
            $bidder->proj_bidder_id = $projectBidder->id;
            $bidder->save();
        }

        return $bidder;
    }

    /**
     * Update bidder status to already invited
     * @param $bidder
     * @param $acceptedBid
     * @return mixed
     */
    public function updateInvitedStatus($bidder, $acceptedBid)
    {
        //if the invitation is already accepted by the bidder for this specific project
        if (!is_null($acceptedBid)) {
            $bidder->invited_comp_id = $acceptedBid->invited_comp_id;
            $bidder->once_accepted = 1;
            $bidder->status = Config::get('constants.bids.status.accepted');
        } else {
            $bidder->status = Config::get('constants.bids.status.invited');
        }

        $bidder->save();
        return $bidder;
    }

    /**
     * Prepare array with data needed for the email
     * @param $invitationType
     * @param $data
     * @return array
     */
    public function prepareEmailData($invitationType, $data)
    {
        //prepare email array
        $emailData = [
            'subject' => ((!is_null($data['bidder']->proposal->project)) ? $data['bidder']->proposal->project->name : 'Cloud PM').' - '.trans('labels.emails.invite_to_bid'),
            'parentCompany' => Auth::user()->company->name,
            'project' => (!is_null($data['bidder']->proposal->project)) ? $data['bidder']->proposal->project->name : '',
            'name' => (!is_null($data['bidder']->address_book_contact)) ? $data['bidder']->address_book_contact->name : '',
            'email' => (!is_null($data['bidder']->address_book_contact)) ? $data['bidder']->address_book_contact->email : '',
            'scopeOfWork' => (!is_null($data['bidder']->proposal)) ? $data['bidder']->proposal->scope_of_work : '',
            'neededDate' => (!is_null($data['bidder']->proposal)) ? $data['bidder']->proposal->proposals_needed_by : '',
        ];

        if ($invitationType == Config::get('constants.bids.invitation_type.proper')) {
            $emailData['view'] = 'emails.invite_to_bid';
            $emailData['token'] = $data['token'];
            $emailData['parentCompanyId'] = Auth::user()->company->id;
            $emailData['bidderId'] = $data['bidder']->id;
            $emailData['contactId'] = (!is_null($data['bidder']->address_book_contact)) ? $data['bidder']->address_book_contact->id : '';

            //store referral
            $referralsRepository = new ReferralsRepository();
            $emailData['referralToken'] = $referralsRepository->insertReferral($emailData);
        } else {
            $emailData['view'] = 'emails.invite_to_bid_informational';
        }

        return $emailData;
    }

    /**
     * Proceed with already invited companies process
     * @param $emailArr
     * @param $projectId
     * @param $abId
     * @param $bidder
     * @return bool
     * @internal param $bidder
     */
    public function processAlreadyInvited($emailArr, $projectId, $abId, $bidder)
    {
        //prepare email array
        $emailData = $this->prepareEmailData(Config::get('constants.bids.invitation_type.informational'), $emailArr);

        //send email to company contact
        $this->sendEmail->inviteCompanyToBid($emailData);

        return true;
    }

    /**
     * If company is already existing as a bidder for the concrete project
     * @param $projectBidder
     * @param $emailArr
     * @param $bidder
     * @return bool
     */
    public function processAlreadyExistingBidder($projectBidder, $emailArr, $bidder)
    {
        //update project bidder
        $projectBidder->token = $emailArr['token'];
        $projectBidder->token_active = 1;
        $projectBidder->status = Config::get('constants.bids.status.pending');
        $projectBidder->save();

        //update poject bidder id in proposal suppliers
        $bidder->proj_bidder_id = $projectBidder->id;
        $bidder->save();

        //prepare email data
        $emailData = $this->prepareEmailData(Config::get('constants.bids.invitation_type.proper'), $emailArr);

        //send email to company contact
        $this->sendEmail->inviteCompanyToBid($emailData);

        return true;
    }

    /**
     * Proceed with not invited companies process
     * @param $emailArr
     * @param $projectId
     * @param $bidder
     * @param $bidId
     * @return bool
     */
    public function processNotInvited($emailArr, $projectId, $bidder, $bidId)
    {
        //invite company properly if it's not invited
        $this->properInviteToBid($bidder, $projectId, $emailArr['token'], $bidId);

        //prepare email data
        $emailData = $this->prepareEmailData(Config::get('constants.bids.invitation_type.proper'), $emailArr);

        //send email to company contact
        $this->sendEmail->inviteCompanyToBid($emailData);

        return true;
    }

    /**
     * Process  invitation process for bidders
     * @param $data
     * @return bool|null
     */
    public function invitationProcessing($data)
    {
        $bidder = $this->proposalsRepo->getBidder($data['proj_id'], $data['bid_id'], $data['bidder_id'], $data['ab_id']);
        
        //check if the bidder has a proper selected contact
        if (empty($bidder->ab_cont_id)) {
            return null;
        }

        //generate token
        $emailArr['token'] = Uuid::uuid1()->toString();

        $emailArr['bidder'] = $bidder;

        return $this->processNotInvited($emailArr, $data['proj_id'], $bidder, $data['bid_id']);
    }

}