<?php
namespace App\Services;

use App\Models\Module;
use App\Models\Project_subcontractor;
use App\Models\Project;
use App\Models\Project_company_permission;
use App\Models\File_type;
use App\Models\Proposal_supplier;
use App\Models\Proposal_supplier_permission;
use Auth;
use Request;
use Config;

class CompanyPermissions {

    /**
     * Return if a company can or can not view a module / file type for a shared project
     * @param $rwd - read, write or delete argument
     * @param $moduleOrFile - module or file for checking permissions
     * @return bool
     */
    public static function can($rwd, $moduleOrFile)
    {
        //get all modules
        $modulesArray = Module::where('id', '>', 0)->select('display_name')->get()->toArray();

        //get all files
        $filesArray = File_type::where('id', '>', 0)->select('display_name')->get()->toArray();

        $projectID = Request::segment(2);
        $project = Project::where('id', '=', $projectID)->first();

        $projectModules = array();

        $files = array();

        //if owner is logged in
        if (Auth::user()->comp_id == $project->comp_id) {
           return true;
        }
        //make one dimensional arrays
        foreach($modulesArray as $module) {
            if (($module['display_name'] != 'address-book')) {
                array_push($projectModules, $module['display_name']);
            }

        }
        foreach($filesArray as $file) {
            array_push($files, $file['display_name']);
        }

        if (in_array($moduleOrFile, $projectModules)) {
            //if it is module, check with table modules
            $hasPermission = Project_company_permission::join('modules', 'project_company_permissions.entity_id', '=', 'modules.id')
                ->select('project_company_permissions.'.$rwd)
                ->where('project_company_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                ->where('project_company_permissions.comp_child_id', '=', Auth::user()->comp_id)
                ->where('project_company_permissions.proj_id', '=', $projectID)
                ->where('modules.display_name', '=', $moduleOrFile)
                ->first();

            //check bids permissions
            $bidsPermissions = Proposal_supplier_permission::join('modules', 'proposal_supplier_permissions.entity_id', '=', 'modules.id')
                ->select('proposal_supplier_permissions.'.$rwd)
                ->where('proposal_supplier_permissions.entity_type', '=', Config::get('constants.entity_type.module'))
                ->where('proposal_supplier_permissions.comp_child_id', '=', Auth::user()->comp_id)
                ->where('proposal_supplier_permissions.proj_id', '=', $projectID)
                ->where('modules.display_name', '=', $moduleOrFile)
                ->first();

            if (is_null($hasPermission) && is_null($bidsPermissions)) {
                return false;
            }

            if (!is_null($hasPermission) && $hasPermission->$rwd != 0) {
                return Config::get('constants.module_permission_type.companies');
            }

            if (!is_null($bidsPermissions) && $bidsPermissions->$rwd != 0) {
                return Config::get('constants.module_permission_type.bids');
            }


        } else if (in_array($moduleOrFile, $files)) {
            $projectID = Request::segment(2);
            //if it is file, check with table file types
            $hasPermission = Project_company_permission::join('file_types', 'project_company_permissions.entity_id', '=', 'file_types.id')
                ->where('project_company_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                ->where('project_company_permissions.comp_child_id', '=', Auth::user()->comp_id)
                ->where('project_company_permissions.proj_id', '=', $projectID)
                ->where('file_types.display_name', '=', $moduleOrFile)
                ->first();

            //check bids permissions
            $bidsPermissions = Proposal_supplier_permission::join('file_types', 'proposal_supplier_permissions.entity_id', '=', 'file_types.id')
                ->select('proposal_supplier_permissions.'.$rwd)
                ->where('proposal_supplier_permissions.entity_type', '=', Config::get('constants.entity_type.file'))
                ->where('proposal_supplier_permissions.comp_child_id', '=', Auth::user()->comp_id)
                ->where('proposal_supplier_permissions.proj_id', '=', $projectID)
                ->where('file_types.display_name', '=', $moduleOrFile)
                ->first();

            if (is_null($hasPermission) && is_null($bidsPermissions)) {
                return false;
            }

            if (!is_null($hasPermission) && $hasPermission->$rwd != 0) {
                return Config::get('constants.module_permission_type.companies');
            }

            if (!is_null($bidsPermissions) && $bidsPermissions->$rwd != 0) {
                return Config::get('constants.module_permission_type.bids');
            }

        }
        return false;
    }

    /**
     * Check if a specific project has any subcontractors (is shared)
     * or bidders (companies which are invited to bid on this project)
     * @param $projectID
     * @return bool
     */
    public static function hasSubcontractors($projectID)
    {
        $subcontractor = Project_subcontractor::where('proj_id', '=', $projectID)
            ->where('status', '=', Config::get('constants.projects.shared_status.accepted'))
            ->first();

        $bidder = Proposal_supplier::whereHas('proposal', function($query) use ($projectID) {
                $query->where('proj_id', '=', $projectID);
            })
            ->whereHas('project_bidder', function($query) use ($projectID) {
                $query->where('status', '=', Config::get('constants.bids.status.accepted'));
            })
            ->first();

        if ($subcontractor || $bidder) {
            return true;
        }
        return false;
    }
}