<?php
namespace App\Services;

use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Invitations\Implementations\RejectSubcontractor;
use App\Modules\Invitations\Interfaces\AcceptInvitationInterface;
use App\Modules\Invitations\Interfaces\RejectInvitationInterface;
use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use Illuminate\Support\Facades\Auth;

class InviteCompany
{

    private $projectSubcontractorsRepo;
    private $projectCompaniesRepo;
    private $sendEmail;

    public function __construct()
    {
        $this->projectSubcontractorsRepo = new ProjectSubcontractorsRepository();
        $this->projectCompaniesRepo = new ProjectCompaniesRepository();
        $this->sendEmail = new Mailer();
    }

    /**
     * Prepare data array for invitation email
     * @param $projectCompany
     * @return array
     */
    public function prepareCompanyEmailData($projectCompany)
    {
        //prepare contact names and emails
        $contactsArr = [];

        if (!is_null($projectCompany)) {
            if (count($projectCompany->project_company_contacts)) {
                foreach ($projectCompany->project_company_contacts as $companyContact) {
                    if (!is_null($companyContact->contact)) {
                        $contactsArr[] = [
                            'contactId' => $companyContact->contact->id,
                            'emailAddress' => $companyContact->contact->email,
                            'name' => $companyContact->contact->name,
                        ];
                    }
                }
            }
        }
        return $contactsArr;
    }


    /**
     * Invite non registered company
     * @param $projectId
     * @param $projectCompanyId
     * @param $addressBookId
     * @return bool
     */
    public function inviteCompany($projectId, $projectCompanyId, $addressBookId)
    {
        //get project company
        $projectCompany = $this->projectCompaniesRepo->getProjectCompany($projectId, $projectCompanyId, $addressBookId);

        if (!is_null($projectCompany)) {
            //check if subcontractor is already stored
            if (is_null($projectCompany->project_subcontractor)) {
                //prepare contacts useful information
                $emailPersons = $this->prepareCompanyEmailData($projectCompany);

                if (count($emailPersons)) {
                    //store invited company as a subcontractor
                    $storedSubcontractor = $this->projectSubcontractorsRepo->storeInvitedSubcontractor($projectId, $projectCompanyId);

                    if (!is_null($storedSubcontractor)) {
                        //prepare contacts array with info for email sending
                        $contactsArr = [
                            'subject' => ((!is_null($projectCompany->project)) ? $projectCompany->project->name : 'Cloud PM').' - '.trans('labels.emails.invite_company_subject'),
                            'token' => $storedSubcontractor->token,
                            'parentCompany' => Auth::user()->company->name,
                            'parentCompanyId' => Auth::user()->company->id,
                            'projectCompanyId' => $projectCompanyId,
                            'project' => (!is_null($projectCompany->project)) ? $projectCompany->project->name : ''
                        ];

                        foreach ($emailPersons as $contact) {
                            //push contact email into array
                            $contactsArr['name'] = $contact['name'];

                            //push contact name into array
                            $contactsArr['email'] = $contact['emailAddress'];

                            //push contact id into array
                            $contactsArr['contactId'] = $contact['contactId'];

                            //store referral
                            $referralsRepository = new ReferralsRepository();
                            $contactsArr['referralToken'] = $referralsRepository->insertReferral($contactsArr);

                            //send email to company contact
                            $this->sendEmail->inviteCompany($contactsArr);
                        }
                        return true;
                    }
                }
                return null;
            }
        }
        return false;
    }

    /**
     * Accept different type of invitations to the application
     * (invitation for share, invitation to bid)
     * @param AcceptInvitationInterface $acceptInvitationInterface
     * @param $token
     * @param $email
     * @param $contactId
     * @param $parentCompanyId
     * @param $referralToken
     * @param $projectCompanyId
     * @return mixed
     */
    public function acceptInvitationWrapper(AcceptInvitationInterface $acceptInvitationInterface, $token, $email, $contactId, $parentCompanyId, $bidderId, $referralToken, $projectCompanyId)
    {
        $data['token'] = $token;
        $data['email'] = $email;
        $data['contactId'] = $contactId;
        $data['parentCompanyId'] = $parentCompanyId;
        $data['referralToken'] = $referralToken;
        $data['projectCompanyId'] = $projectCompanyId;
        $data['bidderId'] = $bidderId;
        return $acceptInvitationInterface->getInvitation($data);
    }

    public function rejectInvitationWrapper(RejectInvitationInterface $rejectInvitationInterface, $subToken, $refToken)
    {
        $data['subToken'] = $subToken;
        $data['refToken'] = $refToken;
        return $rejectInvitationInterface->reject($data);
    }

}