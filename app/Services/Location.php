<?php

namespace App\Services;

use App\Models\Zip;


class Location
{
    /**
     * Returns city data by zip code
     *
     * @param $zip
     * @return mixed
     */
    public function getCityByZipCode($zip)
    {
        return Zip::where('zip', '=', $zip)->first();
    }

    /**
     * Returns city data by name and state
     *
     * @param $city
     * @param $state
     * @return mixed
     */
    public function getCityByNameAndState($city, $state)
    {
        return Zip::where('city', '=', $city)->where('state', '=', strtoupper(substr($state, 0, 2)))->first();
    }
}