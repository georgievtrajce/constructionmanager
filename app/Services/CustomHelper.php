<?php
namespace App\Services;


use App\Models\Daily_report;
use App\Models\File_type;
use App\Models\Pco;
use App\Models\Project;
use App\Models\Project_file;
use App\Models\Project_subcontractor;
use App\Models\Rfi;
use App\Models\Submittal;
use Illuminate\Support\Facades\Auth;

class CustomHelper {

    /**
     * @param $bytes
     * @return string
     */
    public static function formatByteSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = round(($bytes / 1073741824), 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = round(($bytes / 1048576), 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = round(($bytes / 1024), 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    /**
     * @param $bytes
     * @return float|int
     */
    public static function returnByteSizeNumber($bytes)
    {
        if ($bytes >= 1073741824) {
            $number = round(($bytes / 1073741824), 2);
        } elseif ($bytes >= 1048576)
        {
            $number = round(($bytes / 1048576), 2);
        } elseif ($bytes >= 1024) {
            $number = round(($bytes / 1024), 2);
        } elseif ($bytes > 1) {
            $number = $bytes;
        } elseif ($bytes == 1) {
            $number = $bytes;
        } else {
            $number = 0;
        }

        return $number;
    }

    /**
     * @param $bytes
     * @return string
     */
    public static function returnByteSizeUnit($bytes)
    {
        if ($bytes >= 1073741824) {
            $unit = 'GB';
        } elseif ($bytes >= 1048576) {
            $unit = 'MB';
        } elseif ($bytes >= 1024) {
            $unit = 'KB';
        } elseif ($bytes > 1) {
            $unit = 'bytes';
        } elseif ($bytes == 1) {
            $unit = 'byte';
        } else {
            $unit = '0 bytes';
        }

        return $unit;
    }

    /**
     * @param $value
     * @param $unit
     * @return mixed
     */
    public static function convertInBytes($value, $unit)
    {
        if ($unit == 'GB') {
            $bytes = $value * 1073741824;
        } elseif ($unit == 'MB') {
            $bytes = $value * 1048576;
        } elseif ($unit == 'KB') {
            $bytes = $value * 1024;
        } else {
            $bytes = $value;
        }

        return $bytes;
    }

    /**
     * @param $type
     * @param $projectId
     * @param $companyId
     * @return int
     */
    public static function countProjectFiles($type, $projectId, $companyId)
    {
        if (!empty($type)) {
            $fileType = File_type::where('display_name','=',$type)
                ->firstOrFail();

            return Project_file::where('ft_id','=',$fileType->id)
                ->where('proj_id','=',$projectId)
                ->where('comp_id','=',$companyId)
                ->where('is_finished','=', 1)
                ->count();
        }

        return 0;
    }

    /**
     * @param $projectId
     * @param $companyId
     * @return int
     */
    public static function countDailyReportsFiles($projectId, $companyId)
    {
        if($projectId && $companyId) {
            return Daily_report::where('proj_id', '=', $projectId)
                ->where('comp_id', '=', $companyId)
                ->count();
        }
        return 0;
    }

    /**
     * @param $type
     * @param $projectId
     * @param $projectCreatorId
     * @param $companyId
     * @return int
     */
    public static function countSharedProjectFiles($type, $projectId, $projectCreatorId, $companyId)
    {
        if (!empty($type)) {
            $fileType = File_type::where('display_name','=',$type)
                ->firstOrFail();

            $permissions = CompanyPermissions::can('read', $type);

            if ($permissions) {
                return Project_file::where('ft_id', '=', $fileType->id)
                    ->where('proj_id', '=', $projectId)
                    ->count();
            } else {
                return Project_file::join('file_permissions', 'file_permissions.file_id', '=', 'files.id')
                    ->where('file_permissions.comp_id', '=', $companyId)
                    ->where('files.ft_id', '=', $fileType->id)
                    ->where('files.proj_id', '=', $projectId)
                    ->where('files.comp_id', '=', $projectCreatorId)
                    ->count();
            }
        }

        return 0;
    }

    /**
     * Count Received project files
     * @param $type
     * @param $projectId
     * @param $projectCreatorId
     * @param $companyId
     * @return int
     */
    public static function countReceivedProjectFiles($type, $projectId, $projectCreatorId, $companyId)
    {
        if (!empty($type)) {
            $fileType = File_type::where('display_name', '=', $type)
                ->firstOrFail();

            if (CompanyPermissions::can('read', $type)) {
                return Project_file::where('ft_id', '=', $fileType->id)
                    ->where('proj_id', '=', $projectId)
                    ->where('comp_id', '!=', $projectCreatorId)
                    ->count();
            } else {
                switch ($type) {
                    case "submittals":
                        return Submittal::where('submittals.proj_id','=',$projectId)
                            ->where('submittals.comp_id','!=',$projectCreatorId)
                            ->where('submittals.comp_id','!=',Auth::user()->comp_id)
                            ->rightJoin('submittal_permissions','submittal_permissions.subm_id','=','submittals.id')
                            ->count();
                        break;
                    case "rfis":
                        return Rfi::where('rfis.proj_id','=',$projectId)
                            ->where('rfis.comp_id','!=',$projectCreatorId)
                            ->where('rfis.comp_id','!=',Auth::user()->comp_id)
                            ->rightJoin('rfi_permissions','rfi_permissions.rfi_id','=','rfis.id')
                            ->count();
                        break;
                    case "pcos":
                        return Pco::where('pcos.proj_id', '=', $projectId)
                            ->where('pcos.comp_id', '!=', $projectCreatorId)
                            ->where('pcos.comp_id','!=',Auth::user()->comp_id)
                            ->rightJoin('pco_permissions','pco_permissions.pco_id','=','pcos.id')
                            ->count();
                        break;
                    case "contracts":
                        return 0; //TO DO...
                    case "proposals":
                        return 0; //TO DO...
                    case "expediting-report":
                        return 0; //TO DO...
                    default:
                        return Project_file::join('file_permissions', 'file_permissions.file_id', '=', 'files.id')
                            ->where('file_permissions.comp_id', '=', $companyId)
                            ->where('files.ft_id', '=', $fileType->id)
                            ->where('files.proj_id', '=', $projectId)
                            ->where('files.comp_id', '!=', $companyId)
                            ->where('files.comp_id', '!=', $projectCreatorId)
                            ->count();
                }
            }
        }
        return 0;
    }

    /**
     * @param $value
     * @return float
     */
    public static function amountToDouble($value)
    {
        return round(str_replace(",","",$value), 2);
    }

    /**
     * @param $projectId
     * @return mixed
     */
    public static function getCompanyParentForProject($projectId)
    {
        return Project_subcontractor::where('proj_id','=',$projectId)
                    ->where('comp_parent_id','=',Auth::user()->comp_id)
                    ->with('parent_company')
                    ->first();
    }

    /**
     * @param $type
     * @return array
     */
    public static function getReceivedFilesSortingDetails($type)
    {
        switch($type) {
            case "submittals":
                return ['file_id'=>'','name'=>trans('labels.file.name'),'number'=>trans('labels.files.file_number_code'),'created_at'=>trans('labels.files.upload_date'),'size'=>trans('labels.files.file_size')];
                break;
            case "rfis":
                return ['file_id'=>'','name'=>trans('labels.file.name'),'number'=>trans('labels.files.file_number_code'),'created_at'=>trans('labels.files.upload_date'),'size'=>trans('labels.files.file_size')];
                break;
            case "pcos":
                return ['file_id'=>'','name'=>trans('labels.file.name'),'number'=>trans('labels.files.file_number_code'),'created_at'=>trans('labels.files.upload_date'),'size'=>trans('labels.files.file_size')];
                break;
            default:
                return ['files.id'=>'','files.name'=>trans('labels.file.name'),'files.number'=>trans('labels.files.file_number_code'),'files.created_at'=>trans('labels.files.upload_date'),'files.size'=>trans('labels.files.file_size')];
        }
    }

    /**
     * Return logged in company role for the specified project
     * @param $project
     * @return string
     */
    public static function getCompanyRoleForProject($project)
    {
        if (count($project->projectSubcontractorsSelf) > 0) {
            return trans('labels.supplier_subcontractor');
        }

        if (count($project->projectBiddersSelf) > 0) {
            return trans('labels.bid.bidder');
        }

        return trans('labels.project.creator');
    }

}