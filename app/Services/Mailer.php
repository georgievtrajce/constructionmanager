<?php

namespace App\Services;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use Illuminate\Support\Facades\Auth;
use App\Models\Project_subcontractor;
use Illuminate\Support\Facades\Mail;



class Mailer {
    public function __construct()
    {
        $this->companiesRepository = new CompaniesRepository();
        $this->usersRepository = new UsersRepository();
    }

    public function sendMailSubcontractor($data)
    {
        //get data for receiver of email
        $childCompany = $this->companiesRepository->getCompanyWithUsersByID($data['comp_id']);
        //get project subcontractor record with project
        $projectSubcontractor = Project_subcontractor::with('parent_company')->with('project')
            ->where('comp_child_id', '=', $data['comp_id'])
            ->where('comp_parent_id', '=', Auth::user()->comp_id)
            ->where('proj_id','=',$data['proj_id'])
            ->firstOrfail();

        $adminEmail = '';
        $adminName = trans('messages.email.sendMailSubcontractor.name');

        for ($i=0; $i<sizeof($childCompany->users); $i++){
            if ($childCompany->users[$i]->hasRole('Company Admin')) {
                $adminEmail = $childCompany->users[$i]->email;
                $adminName = $childCompany->users[$i]->name;
                break;
            }
        }

        $user = $this->usersRepository->getUserByID(Auth::user()->id);

        //prepare data for sending email
        $mailData = array(
            'to' => $adminEmail,
            'subject' => trans('messages.email.sendMailSubcontractor.subject'),
            'name' => $adminName,
            'companyInfo' => $user->name . " (" . $user->company->name . ") via cloud-pm.com"
        );

        $view = 'emails.share_project_subcontractor';

        $viewData = array(
            'token' => $projectSubcontractor->token,
            'childCompany' => $childCompany->name,
            'parentCompany' => $projectSubcontractor->parent_company->name,
            'project' => $projectSubcontractor->project->name,
            'adminName' => $adminName,
            'projectCompanyId' => $data['project_company_id'],
        );

        return $this->sendMail($view, $viewData, $mailData);
    }

    /**
     * Invite non registered company contacts
     * @param $emailData
     * @return bool
     */
    public function inviteCompany($emailData)
    {
        return Mail::queue('emails.invite_company', $emailData, function($message) use ($emailData) {
            $message->to($emailData['email']);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * Invite company to bid on a project
     * @param $emailData
     * @return bool
     */
    public function inviteCompanyToBid($emailData)
    {
        return Mail::queue($emailData['view'], $emailData, function($message) use ($emailData) {
            $message->from('noreply@cloud-pm.com', Auth::user()->name . " (" . Auth::user()->company->name . ") via cloud-pm.com");
            $message->to($emailData['email']);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * send referral email
     * @param $data
     * @return mixed
     */
    public function sendReferral($data)
    {
        //get data for company that invites
        $invitingCompany = $this->companiesRepository->getCompanyWithUsersByID($data['company']);

        $user = $this->usersRepository->getUserByID(Auth::user()->id);

        //prepare email data
        $mailData = array(
            'to' => $data['email'],
            'subject' => trans('messages.email.sendReferral.subject'),
            'name' => trans('messages.email.sendReferral.name'),
            'companyInfo' => $user->name . " (" . $user->company->name . ") via cloud-pm.com"
        );

        //prepare view
        $view = 'emails.invite';

        //prepare data in view
        $viewData = array(
            'company' => $invitingCompany->name,
            'token'   => $data['token'],
        );

        //send mail
        return $this->sendMail($view, $viewData, $mailData);
    }

    /**
     * send email
     * @param $view - that appears in email
     * @param $viewData - data contained in emai view
     * @param $mailData - email, name etc
     * @return mixed - return if mail is sent
     */
    public function sendMail($view, $viewData, $mailData)
    {
        $sent = Mail::send($view, $viewData, function($message) use ($mailData) {
            $message->from('noreply@cloud-pm.com', (isset($mailData['companyInfo']) && !empty($mailData['companyInfo']))?$mailData['companyInfo']:'noreply@cloud-pm.com');
            $message->to($mailData['to'], $mailData['name']);
            $message->subject($mailData['subject']);
        });
        return ($sent);
    }

    /**
     * send email for created payments (from billing@cloud-pm.com)
     * @param $view - that appears in email
     * @param $viewData - data contained in emai view
     * @param $mailData - email, name etc
     * @return mixed - return if mail is sent
     */
    public function sendBillingMail($view, $viewData, $mailData)
    {
        $sent = Mail::send($view, $viewData, function($message) use ($mailData) {
            $message->from('billing@cloud-pm.com', (isset($mailData['companyInfo']) && !empty($mailData['companyInfo']))?$mailData['companyInfo']:'noreply@cloud-pm.com');
            $message->to($mailData['to'], $mailData['name']);
            $message->subject($mailData['subject']);
        });
        return ($sent);
    }

    public function sendChangeSubscriptionReceipt($data)
    {
        $user = $this->usersRepository->getUserByEmail($data['email']);
        $company = $this->companiesRepository->getCompany($data['companyId']);
        $subscription = $this->companiesRepository->getSubscriptionTypeByID($data['subscriptionId']);

        $mailData = array(
            'to' => $data['email'],
            'subject' => $data['subject'],
            'name' => $user->name,
        );

        //prepare data in view
        $viewData = array(
            'name'       => $user->name,
            'companyName' => $company->name,
            'subscription' => $subscription->name,
            'cardToken' => $data['cardToken'],
            'amount' => $data['amount'],
            'transactionId' => $data['transactionId']
        );
        return $this->sendBillingMail($data['view'], $viewData, $mailData);
    }

    /**
     * Send new task notification to users
     * @param $emailData
     * @return bool
     */
    public function sendNewTaskEmail($emailData, $mailData = [])
    {
        return Mail::queue('emails.new_task', $emailData, function($message) use ($emailData, $mailData) {
            $message->from('noreply@cloud-pm.com', (isset($mailData['companyInfo']) && !empty($mailData['companyInfo']))?$mailData['companyInfo']:'noreply@cloud-pm.com');
            $message->to($emailData['email']);
            $message->subject($emailData['project']." - Task: ".$emailData['title']);
        });
    }

    /**
     * Send expiration notification to task users
     *
     * @param $emailData
     * @return bool
     */
    public function sendTaskNotification($emailData)
    {
        $fromName = '';
        if (isset($emailData['addedByUserName']) && isset($emailData['senderCompanyName'])) {
            $fromName = $emailData['addedByUserName'] . " (" . $emailData['senderCompanyName'] . ") via cloud-pm.com";
        }


        return Mail::queue('emails.task_notification', $emailData, function($message) use ($emailData, $fromName) {
            $message->from('noreply@cloud-pm.com', (!empty($fromName))?$fromName:'noreply@cloud-pm.com');
            $message->to($emailData['email']);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * Send bid proposal notification to users
     *
     * @param $emailData
     * @param $email
     * @param array $mailData
     * @return bool
     */
    public function sendBidProposalNotification($emailData, $email, $mailData = [])
    {
        return Mail::queue('emails.bid_proposal_notification', $emailData, function($message) use ($emailData, $email, $mailData) {
            $message->from('noreply@cloud-pm.com', (isset($mailData['companyInfo']) && !empty($mailData['companyInfo']))?$mailData['companyInfo']:'noreply@cloud-pm.com');
            $message->to($email);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * Send submittal/rfi/pco proposal notification to users
     *
     * @param $emailData
     * @return bool
     */
    public function sendGeneralProposalNotification($emailData, $email, $mailData = [])
    {
        return Mail::queue('emails.general_proposal_notification', $emailData, function($message) use ($emailData, $email, $mailData) {
            $message->from('noreply@cloud-pm.com', (isset($mailData['companyInfo']) && !empty($mailData['companyInfo']))?$mailData['companyInfo']:'noreply@cloud-pm.com');
            $message->to($email);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * Send expiration notification to address book file users
     *
     * @param $emailData
     * @param array $mailData
     * @return bool
     */
    public function sendAddressBookNotification($emailData)
    {
        $fromName = '';
        if (isset($emailData['senderCompanyName'])) {
            $fromName = $emailData['senderCompanyName'] . " via cloud-pm.com";
        }
        
        return Mail::queue('emails.address_book_file_notification', $emailData, function($message) use ($emailData, $fromName) {
            $message->from('noreply@cloud-pm.com',  (!empty($fromName))?$fromName:'noreply@cloud-pm.com');
            $message->to($emailData['email']);
            $message->subject($emailData['subject']);
        });
    }

    /**
     * Send expiration notification to task users
     *
     * @param $emailData
     * @param array $mailData
     * @return bool
     */
    public function sendProjectBatchDownloadNotification($emailData)
    {
        return Mail::queue('emails.project_batch_file_notification', $emailData, function($message) use ($emailData) {
            $message->from('noreply@cloud-pm.com', 'noreply@cloud-pm.com');
            $message->to($emailData['email']);
            $message->subject($emailData['subject']);
        });
    }
}