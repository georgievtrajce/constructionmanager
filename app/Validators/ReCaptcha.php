<?php

namespace App\Validators;

use Guzzle\Http\Client;

class ReCaptcha
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $fields = [
            'secret' => env('GOOGLE_RECAPTCHA_SECRET'),
            'response' => $value
        ];

        $fields_string = http_build_query($fields);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $body = json_decode((string)$response);

        return $body->success;
    }
}