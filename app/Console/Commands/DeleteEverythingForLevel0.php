<?php namespace App\Console\Commands;

use App\Models\Project_file;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteEverythingForLevel0 extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'companies:delete-everything';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete everythig for level 0 after 14 days';

    /**
     * Execute the console command.
     *
     * @param CompaniesRepository $companiesRepository
     * @param ProjectRepository $projectRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param AddressBookRepository $addressBookRepository
     * @param FilesRepository $filesRepository
     * @return mixed
     */
	public function handle(CompaniesRepository $companiesRepository, ProjectRepository $projectRepository,
                           ProjectFilesRepository $projectFilesRepository, AddressBookRepository $addressBookRepository,
                           FilesRepository $filesRepository)
	{
        $companies = $companiesRepository->getAllCompaniesWithExpiredSubscriptionByNumberOfDay(14, 19);
        foreach ($companies as $company) {
            $projects = $projectRepository->getAllProjectsForCompanyId($company->id);

            //remove all files on s3
            $disk = Storage::disk('s3');
            $disk->deleteDirectory('company_'.$company->id.'/');

            //delete address book entries
            $addressBookRepository->deleteAllByCompanyId($company->id);

            $filesRepository->deleteAllAddressBookFilesByCompany($company->id);

            foreach ($projects as $project) {

                //delete all entries by project
                $projectFilesRepository->deleteProjectFilesByProjectId($project->id);

                //mark project as deleted
                $project->files_deleted = 1;
                $project->save();

                $projectRepository->deleteProject($project->id);
            }
        }
	}

}
