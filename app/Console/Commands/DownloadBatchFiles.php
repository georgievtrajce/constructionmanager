<?php namespace App\Console\Commands;

use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use App\Services\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;

class DownloadBatchFiles extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'projects:download-batch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Packs and downloads all project files.';


    /**
     * Execute the console command.
     * @param ContractsRepository $contractsRepository
     * @param ProposalsRepository $proposalsRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param Mailer $mailer
     * @param UsersRepository $usersRepository
     * @param ProjectRepository $projectRepository
     * @param FilesRepository $filesRepository
     * @param DataTransferLimitation $dataTransferLimitation
     */
    public function handle(ContractsRepository $contractsRepository, ProposalsRepository $proposalsRepository,
                           SubmittalsRepository $submittalsRepository, RfisRepository $rfisRepository,
                           PcosRepository $pcosRepository, ProjectFilesRepository $projectFilesRepository,
                           Mailer $mailer, UsersRepository $usersRepository, ProjectRepository $projectRepository,
                           FilesRepository $filesRepository, DataTransferLimitation $dataTransferLimitation)
    {
        $projects = $projectRepository->getProjectBatchRequests();

        foreach ($projects as $project) {

            $projectName =  $project->project->name.'_'. $project->proj_id.'_'. Carbon::now()->format('m-d-Y');

            $usedStorageByProject = $filesRepository->getUsedStorageBySmallFilesProjectAndCompany($project->proj_id, $project->comp_id);
            if ($dataTransferLimitation->checkDownloadTransferAllowance($usedStorageByProject, $project->proj_id)) {

                //contracts download
                $contracts = $contractsRepository->getAllContractsByProjectAndCompany($project->proj_id, $project->comp_id, 'id', 'ASC');
                foreach ($contracts as $contract) {
                    if (isset($contract->contractFiles)) {
                        foreach ($contract->contractFiles as $contractFile) {
                            $savedFile = $contractFile->smallFiles;
                            if (!empty($savedFile) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/contracts/contract_' . $contractFile->contr_id.'/'.$savedFile->file_name)) {
                                $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/contracts/contract_' . $contractFile->contr_id.'/'.$savedFile->file_name);
                                Storage::disk('local')->put('/batch/'. $projectName .'/contracts/'.$savedFile->file_name, $fileData);
                            }
                        }
                    }
                }

                //bids download
                $bids = $proposalsRepository->getAllProposalsByProjectAndCompany($project->proj_id, $project->comp_id, 'id', 'ASC');
                foreach ($bids as $bid) {
                    if (isset($bid->suppliers)) {
                        foreach ($bid->suppliers as $supplier) {
                            if (!empty($supplier->files)) {
                                foreach ($supplier->files as $file) {
                                    if (!empty($file) && !empty($file->smallFile) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/proposals/' . $file->smallFile->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/proposals/' . $file->smallFile->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/bids/bidder_'.$supplier->address_book->name. '/' . $file->smallFile->file_name, $fileData);
                                    }
                                }
                            }
                        }
                    }

                }

                //submittals download
                $submittals = $submittalsRepository->getAllSubmittalsForProject($project->proj_id, $project->comp_id);
                foreach ($submittals as $submittal) {
                    if (isset($submittal->submittals_versions)) {
                        foreach ($submittal->submittals_versions as $version) {
                            if (!empty($version->smallFile)) {
                                foreach ($version->smallFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/' . $file->file_name);

                                        $folderName = 'files';
                                        switch ($file->version_date_connection)
                                        {
                                            case 'rec_sub_file':
                                                $folderName = 'Subcontractor-in';
                                                break;
                                            case 'sent_appr_file':
                                                $folderName = 'Approval-out';
                                                break;
                                            case 'rec_appr_file':
                                                $folderName = 'Approval-in';
                                                break;
                                            case 'subm_sent_sub_file':
                                                $folderName = 'Subcontractor-out';
                                                break;
                                        }

                                        Storage::disk('local')->put('/batch/'. $projectName . '/submittals/'.$submittal->number.'_'.$submittal->name.'/'.'Cycle '.$version->cycle_no.'/'.$folderName.'/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSubmSentFile)) {
                                foreach ($version->transmittalSubmSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/transmittals/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/transmittals/' . $file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/submittals/'.$submittal->number.'_'.$submittal->name.'/'.'Cycle '.$version->cycle_no.'/Subcontractor-out/Transmittal_Subcontractor-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSentFile)) {
                                foreach ($version->transmittalSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/transmittals/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/submittals/transmittals/' .$file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/submittals/' .$submittal->number.'_'.$submittal->name.'/'.'Cycle '.$version->cycle_no.'/Approval-out/Transmittal_Approval-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }
                        }
                    }
                }

                //rfis download
                $rfis = $rfisRepository->getAllRFIsForProject($project->proj_id, $project->comp_id);
                foreach ($rfis as $rfi) {
                    if (isset($rfi->rfis_versions)) {
                        foreach ($rfi->rfis_versions as $version) {
                            if (!empty($version->smallFile)) {
                                foreach ($version->smallFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/' . $file->file_name);

                                        $folderName = 'files';
                                        switch ($file->version_date_connection)
                                        {
                                            case 'rec_sub_file':
                                                $folderName = 'Subcontractor-in';
                                                break;
                                            case 'sent_appr_file':
                                                $folderName = 'Approval-out';
                                                break;
                                            case 'rec_appr_file':
                                                $folderName = 'Approval-in';
                                                break;
                                            case 'subm_sent_sub_file':
                                                $folderName = 'Subcontractor-out';
                                                break;
                                        }

                                        Storage::disk('local')->put('/batch/'. $projectName . '/rfis/' .$rfi->number.'_'.$rfi->name.'/'.'Cycle '.$version->cycle_no.'/'.$folderName.'/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSubmSentFile)) {
                                foreach ($version->transmittalSubmSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/transmittals/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/transmittals/' . $file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/rfis/' . $rfi->number.'_'.$rfi->name.'/'.'Cycle '.$version->cycle_no.'/Subcontractor-out/Transmittal_Subcontractor-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSentFile)) {
                                foreach ($version->transmittalSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/transmittals/' .$file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/rfis/transmittals/' .$file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/rfis/' . $rfi->number.'_'.$rfi->name.'/'.'Cycle '.$version->cycle_no.'/Approval-out/Transmittal_Approval-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }
                        }
                    }
                }

                //pcos download
                $pcos = $pcosRepository->getAllPcosForProject($project->proj_id, $project->comp_id);
                foreach ($pcos as $pco) {
                    if (isset($pco->subcontractors)) {
                        foreach ($pco->subcontractors as $subcontractor) {
                            if (isset($subcontractor->subcontractor_versions)) {
                                foreach ($subcontractor->subcontractor_versions as $version) {
                                    if (!empty($version->smallFile)) {
                                        foreach ($version->smallFile as $file) {
                                            if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/' . $file->file_name)) {
                                                $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/' . $file->file_name);

                                                $folderName = 'files';
                                                switch ($file->version_date_connection)
                                                {
                                                    case 'rec_sub_file':
                                                        $folderName = 'Subcontractor-in';
                                                        break;
                                                    case 'sent_appr_file':
                                                        $folderName = 'Approval-out';
                                                        break;
                                                    case 'rec_appr_file':
                                                        $folderName = 'Approval-in';
                                                        break;
                                                    case 'subm_sent_sub_file':
                                                        $folderName = 'Subcontractor-out';
                                                        break;
                                                }

                                                Storage::disk('local')->put('/batch/'. $projectName . '/pcos/'.$pco->number.'_'.$pco->name.'/subcontractors/' .'Cycle '.$version->cycle_no.'/'.$folderName.'/' . $file->file_name, $fileData);
                                            }
                                        }
                                    }

                                    if (!empty($version->transmittalSubmSentFile)) {
                                        foreach ($version->transmittalSubmSentFile as $file) {
                                            if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' . $file->file_name)) {
                                                $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' . $file->file_name);
                                                Storage::disk('local')->put('/batch/'. $projectName . '/pcos/' . $pco->number.'_'.$pco->name.'/subcontractors/'.'/'.'Cycle '.$version->cycle_no.'/Subcontractor-out/Transmittal_Subcontractor-out/' . $file->file_name, $fileData);
                                            }
                                        }
                                    }

                                    if (!empty($version->transmittalSentFile)) {
                                        foreach ($version->transmittalSentFile as $file) {
                                            if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' .$file->file_name)) {
                                                $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' .$file->file_name);
                                                Storage::disk('local')->put('/batch/'. $projectName . '/pcos/' . $pco->number.'_'.$pco->name.'/subcontractors/'.'/'.'Cycle '.$version->cycle_no.'/Approval-out/Transmittal_Approval-out/' . $file->file_name, $fileData);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (isset($pco->recipient->recipient_versions)) {
                        foreach ($pco->recipient->recipient_versions as $version) {
                            if (!empty($version->smallFile)) {
                                foreach ($version->smallFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/' . $file->file_name);

                                        $folderName = 'files';
                                        switch ($file->version_date_connection)
                                        {
                                            case 'rec_sub_file':
                                                $folderName = 'Subcontractor-in';
                                                break;
                                            case 'sent_appr_file':
                                                $folderName = 'Approval-out';
                                                break;
                                            case 'rec_appr_file':
                                                $folderName = 'Approval-in';
                                                break;
                                            case 'subm_sent_sub_file':
                                                $folderName = 'Subcontractor-out';
                                                break;
                                        }

                                        Storage::disk('local')->put('/batch/'. $projectName . '/pcos/' .$pco->mf_number.'_'.$pco->name.'/recipient/' .'Cycle '.$version->cycle_no.'/'.$folderName.'/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSubmSentFile)) {
                                foreach ($version->transmittalSubmSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' . $file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' . $file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/pcos/' . $pco->mf_number.'_'.$pco->name.'/recipient/'.'/'.'Cycle '.$version->cycle_no.'/Subcontractor-out/Transmittal_Subcontractor-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }

                            if (!empty($version->transmittalSentFile)) {
                                foreach ($version->transmittalSentFile as $file) {
                                    if (!empty($file) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' .$file->file_name)) {
                                        $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/pcos/transmittals/' .$file->file_name);
                                        Storage::disk('local')->put('/batch/'. $projectName . '/pcos/' . $pco->mf_number.'_'.$pco->name.'/recipient/'.'/'.'Cycle '.$version->cycle_no.'/Approval-out/Transmittal_Approval-out/' . $file->file_name, $fileData);
                                    }
                                }
                            }
                        }
                    }

                }

                //project files download
                $projectFileTypes = $projectFilesRepository->getAllProjectFilesModuleTypes();
                foreach ($projectFileTypes as $projectFileType) {
                    $projectFiles = $projectFilesRepository->getAllSmallProjectFilesByTypeAndCompany($project->proj_id, $project->comp_id, $projectFileType->display_name, 'id', 'ASC');
                    foreach ($projectFiles as $projectFile) {
                        if (!empty($projectFile) && Storage::disk('s3')->exists('company_' . $project->comp_id . '/project_' . $project->proj_id . '/'.$projectFileType->display_name.'/' . $projectFile->file_name)) {
                            $fileData = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $project->proj_id . '/'.$projectFileType->display_name.'/' . $projectFile->file_name);
                            Storage::disk('local')->put('/batch/'. $projectName . '/'.$projectFileType->display_name.'/' . $projectFile->file_name, $fileData);
                        }
                    }
                }

                //zip files
                // Get real path for our folder
                $rootPath = realpath('storage/app/batch/'. $projectName);

                if ($rootPath) {
                    // Initialize empty "delete list"
                    $filesToDelete = array();

                    $path = 'public/batch/';
                    if (!File::isDirectory($path)) {
                        File::makeDirectory($path, 0755, true, true);
                    }

                    // Initialize archive object
                    $zip = new ZipArchive();
                    $zip->open($path. $projectName .'.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

                    // Create recursive directory iterator
                    $files = new RecursiveIteratorIterator(
                        new RecursiveDirectoryIterator($rootPath),
                        RecursiveIteratorIterator::LEAVES_ONLY
                    );

                    foreach ($files as $name => $file)
                    {
                        // Skip directories (they would be added automatically)
                        if (!$file->isDir())
                        {
                            // Get real and relative path for current file
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($rootPath) + 1);

                            // Add current file to archive
                            $zip->addFile($filePath, $relativePath);

                            $filesToDelete[] = $filePath;
                        }
                    }

                    // Zip archive will be created only after closing object
                    $zip->close();

                    // Delete all files from "delete list"
                    foreach ($filesToDelete as $file)
                    {
                        unlink($file);
                    }

                    $project->generated = 1;
                    $project->total_file_size = $usedStorageByProject;
                    $project->save();

                    $dataTransferLimitation->increaseDownloadTransfer($usedStorageByProject, $project->comp_id);

                    //send email notification
                    if (!empty($project->user_id)) {
                        $user = $usersRepository->getUserByID($project->user_id);
                        $projectSaved = $projectRepository->getProjectRecord($project->proj_id);
                        if ($user && $projectSaved) {

                            $emailData = ['email' => $user->email,
                                'subject' => 'Project files ready for download',
                                'url' => url('batch/'.$projectName .'.zip'),
                                'projectName' => $projectSaved->name
                            ];

                            $mailer->sendProjectBatchDownloadNotification($emailData);

                            $project->sent = 1;
                            $project->save();
                        }
                    }
                } else {
                    $project->delete();
                }

            }

        }
    }


}
