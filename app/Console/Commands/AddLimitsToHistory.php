<?php namespace App\Console\Commands;

use App\Models\Project_file;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AddLimitsToHistory extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'companies:history-limits';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Adds limits to comapnies in history table';

    /**
     * Execute the console command.
     *
     * @param CompaniesRepository $companiesRepository
     * @return mixed
     */
	public function handle(CompaniesRepository $companiesRepository)
	{
        $companiesRepository->AddLimitsForAllCompaniesToHistory();

	}

}
