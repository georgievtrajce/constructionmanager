<?php namespace App\Console\Commands;

use App\Models\Ab_file;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Services\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendNotificationsAddressBookFiles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'address-book:send-notifications';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends notifications for address book files to selected users.';

    /**
     * Execute the console command.
     *
     * @param AddressBookRepository $addressBookRepository
     * @param Mailer $mailer
     * @return mixed
     */
	public function handle(AddressBookRepository $addressBookRepository, Mailer $mailer)
	{
		$users = $addressBookRepository->getAddressBookContactsForNotifications();

        foreach ($users as $user) {

            $emailData = [
                'subject' => $user->companyName.' - Document: '.$user->fileName,
                'companyName' => $user->companyName,
                'fileName' => $user->fileName,
                'senderCompanyName' => $user->senderCompanyName,
                'fileId' => $user->fileId,
                'fileDiskName' => $user->fileDiskName,
                'addressBookId' => $user->addressBookId,
                'senderCompanyId' => $user->senderCompanyId,
                'dueDate' => empty($user->dueDate) ? '' : Carbon::parse($user->dueDate)->format('Y-m-d'),
            ];

            $assignees = [];
            if (!empty($user->userEmail)) {
                $emailData['email'] = $user->userEmail;
                $emailData['name'] = $user->userName;
                $assignees[] = $user->userName.' - '.$user->senderCompanyName;
            } else {
                $emailData['email'] = $user->contactEmail;
                $emailData['name'] = $user->contactName;
                $assignees[] = $user->contactName.' - '.$user->companyName;
            }
            $emailData['assignees'] = $assignees;

            $mailer->sendAddressBookNotification($emailData);
        }

	}

}
