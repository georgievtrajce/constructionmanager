<?php namespace App\Console\Commands;

use App\Modules\Tasks\Repositories\TasksRepository;
use App\Services\Mailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendNotificationsTasks extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'tasks:send-notifications';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sends notifications for tasks to users selected in task.';

    /**
     * Execute the console command.
     *
     * @param TasksRepository $tasksRepository
     * @param Mailer $mailer
     * @return mixed
     */
	public function handle(TasksRepository $tasksRepository, Mailer $mailer)
	{
        $users = $tasksRepository->getTasksContactsForNotifications();

        foreach ($users as $user) {

            $emailData = [
                'subject' => $user->projectName.' - Task: '.$user->taskName,
                'userCompanyName' => $user->userCompanyName,
                'contactCompanyName' => $user->contactCompanyName,
                'senderCompanyName' => $user->senderCompanyName,
                'addedByUserName' => $user->addedByUserName,
                'addedByUserEmail' => $user->addedByUserEmail,
                'addedByUserTitle' => $user->addedByUserTitle,
                'addedByUserOfficePhone' => $user->addedByUserOfficePhone,
                'taskNote' => $user->taskNote,
                'taskName' => $user->taskName,
                'dueDate' => empty($user->dueDate) ? '' : Carbon::parse($user->dueDate)->format('Y-m-d'),
            ];

            $assignees = [];
            if (!empty($user->userEmail)) {
                $emailData['email'] = $user->userEmail;
                $emailData['name'] = $user->userName;
                $assignees[] = $user->userName.' - '.$user->userCompanyName;
            } else {
                $emailData['email'] = $user->contactEmail;
                $emailData['name'] = $user->contactName;
                $assignees[] = $user->contactName.' - '.$user->contactCompanyName;
            }
            $emailData['assignees'] = $assignees;

            $mailer->sendTaskNotification($emailData);
        }
	}

}
