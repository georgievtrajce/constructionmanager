<?php namespace App\Console\Commands;

use App\Models\Project_file;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ChangeLevelToLevel0 extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'companies:deactivate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Change level of companies with expired subscription';

    /**
     * Execute the console command.
     *
     * @param CompaniesRepository $companiesRepository
     * @return mixed
     */
	public function handle(CompaniesRepository $companiesRepository)
	{
        $companiesRepository->changeAllExpiredCompaniesToLevel0();

	}

}
