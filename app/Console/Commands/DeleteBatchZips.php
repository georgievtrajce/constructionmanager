<?php namespace App\Console\Commands;

use App\Models\Project_file;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteBatchZips extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'projects:delete-batch-zips';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete batch zips after some period of time';

    /**
     * Execute the console command.
     *
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
	public function handle(ProjectRepository $projectRepository)
	{
        $zipFiles = $projectRepository->getProjectBatchExpiredFiles(1);

        foreach ($zipFiles as $zipFile) {

            $projectFile = $zipFile->proj_id.'_'. $zipFile->project->name.'_'. Carbon::createFromFormat('Y-m-d H:i:s', $zipFile->updated_at)->format('m-d-Y');

            $filePath = realpath('public/batch/'. $projectFile .'.zip');
            if ($filePath) {
                unlink($filePath);

                $projectRepository->deleteBatchFile($zipFile->id);
            }
        }
	}

}
