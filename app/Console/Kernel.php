<?php namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        'App\Console\Commands\SendNotificationsTasks',
        'App\Console\Commands\SendNotificationsAddressBookFiles',
        'App\Console\Commands\DeleteProjects',
        'App\Console\Commands\ChangeLevelToLevel0',
        'App\Console\Commands\DeleteEverythingForLevel0',
        'App\Console\Commands\DownloadBatchFiles',
        'App\Console\Commands\DeleteBatchZips',
        'App\Console\Commands\AddLimitsToHistory'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('address-book:send-notifications')->daily();
        $schedule->command('tasks:send-notifications')->daily();
        $schedule->command('projects:delete')->daily();
        $schedule->command('companies:deactivate')->daily();
        $schedule->command('companies:delete-everything')->daily();
        $schedule->command('projects:download-batch')->everyFiveMinutes();
        $schedule->command('projects:delete-batch-zips')->everyFiveMinutes();
        $schedule->command('companies:history-limits')->daily();
	}

}
