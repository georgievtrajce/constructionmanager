<?php
namespace App\Http\Controllers;

use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Http\Requests\ContractsRequest;
use App\Requests;

use Request;
use Input;
use Flash;
use Auth;

class ContractItemsController extends Controller
{
    private $contractsRepo;
    private $projectRepo;

    public function __construct(ContractsRepository $contractsRepo, ProjectRepository $projectRepo)
    {
        $this->middleware('auth');
        $this->middleware('only_contractor');
        $this->contractsRepo = $contractsRepo;
        $this->projectRepo = $projectRepo;
    }

    /**
     * @param $projectId
     * @param $contractId
     * @return \Illuminate\View\View
     */
    public function create($projectId, $contractId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $contract = $this->contractsRepo->getContract($contractId);

        return view('projects.contracts.items.create',compact('project','contract'));
    }

    /**
     * @param $projectId
     * @param $contractId
     * @param ContractsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, $contractId, ContractsRequest $request)
    {
        $data = $request->all();
        $contractItem = $this->contractsRepo->storeContractItem($contractId,$data);

        if ($contractItem) {
            Flash::success(trans('messages.projectItems.insert.success', ['item'=> 'contract item']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item'=> 'contract item']));
        }

        return redirect('projects/'.$projectId.'/contracts/');
    }

    /**
     * @param $projectId
     * @param $contractId
     * @param $contractItemId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($projectId, $contractId, $contractItemId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $item = $this->contractsRepo->getContractItem($contractItemId);

        if ($item) {
            return view('projects.contracts.items.edit',compact('project','item'));
        } else {
            Flash::error(trans('messages.projectItems.notFound', ['item'=> 'contract item']));
            return redirect('projects/'.$projectId.'/contracts/');
        }

    }

    /**
     * @param $projectId
     * @param $contractId
     * @param $contractItemId
     * @param ContractsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $contractId, $contractItemId, ContractsRequest $request)
    {
        $data = $request->all();
        $item = $this->contractsRepo->updateContractItem($contractItemId, $data);

        if ($item) {
            Flash::success(trans('messages.projectItems.update.success', ['item'=> 'contract item']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item'=> 'contract item']));
        }

        return redirect('projects/'.$projectId.'/contracts/');
    }


    /**
     * @param $projectId
     * @param $contractId
     * @param $contractItemId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectId, $contractId, $contractItemId)
    {
        $data = Input::all();
        $delete = $this->contractsRepo->deleteContractItem($data['ci_id']);

        if ($delete){
            Flash::success(trans('messages.projectItems.delete.success', ['item'=> 'contract item']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item'=> 'contract item']));
        }

        return redirect('projects/'.$projectId.'/contracts/');
    }
}