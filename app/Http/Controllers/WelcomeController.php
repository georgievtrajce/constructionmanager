<?php namespace App\Http\Controllers;

use App\Models\Subscription_type;
use Illuminate\Support\Facades\Config;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['firstSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-one'))->firstOrFail();
		$data['secondSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->firstOrFail();
		$data['thirdSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->firstOrFail();
		return view('home.welcome', $data);
	}

	/**
	 * Static contact us page
	 * @return \Illuminate\View\View
     */
	public function contactUs()
	{
		return view('home.contact');
	}

	/**
	 * Static products and pricing page
	 * @return \Illuminate\View\View
     */
	public function productsAndPricing()
	{
		$data['firstSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-one'))->firstOrFail();
		$data['secondSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->firstOrFail();
		$data['thirdSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->firstOrFail();
		return view('home.products-pricing', $data);
	}

	/**
	 * Static refund policy page
	 * @return \Illuminate\View\View
     */
	public function refundPolicy()
	{
		return view('home.refund-policy');
	}

	/**
	 * Static privacy policy page
	 * @return \Illuminate\View\View
     */
	public function privacyPolicy()
	{
		return view('home.privacy-policy');
	}

	/**
	 * Static terms of use page
	 * @return \Illuminate\View\View
     */
	public function termsOfUse()
	{
		return view('home.terms_of_use');
	}

	/**
	 * Static features page
	 * @return \Illuminate\View\View
     */
	public function features()
	{
		return view('home.features');
	}

}
