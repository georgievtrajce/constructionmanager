<?php
namespace App\Http\Controllers;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Address_book\Repositories\AddressBookRepository;

use Request;
use Input;
use Flash;
use Session;
use Auth;
use Config;

class SubcontractorsProjectsController extends Controller {

	private $projectRepo;
	private $addressBookRepo;

	public function __construct(ProjectRepository $projectRepo, AddressBookRepository $addressBookRepo)
	{
		$this->projectRepo= $projectRepo;
		$this->addressBookRepo= $addressBookRepo;
		$this->middleware('auth');
	}

	/**
	 * @param $id
	 * list all projects that logged in user has shared with subcontractor's $id
	 * @param string $status
	 * @return \Illuminate\View\View
	 */
	public function index($id, $status = 'active')
	{
		//default status is active
		$sort = Input::get('sort', 'name');
		$order = Input::get('order', 'asc');

		$addressBook = $this->addressBookRepo->getEntryBasicInfo($id);
		//get projects by company id

		//default status is active
		//if stated otherwise, get completed projects
		$active = 'active';

		if ($status == 'completed') {
			$active = 'inactive';
		}

		$projects = $this->projectRepo->getAllProjectsForSubcontractor($id, Config::get('constants.projects.status.'.$active), $sort, $order);

		$sortUrl = $status;
		$searchUrl = $status;

		return view('subcontractors.projects', compact('projects', 'sortUrl', 'searchUrl', 'addressBook'));
	}


	/**
	 * search and list projects by subcontractor's $id
	 * @param $id
	 * @param string $status
	 * @return \Illuminate\View\View
	 */
	public function search($id, $status = 'active')
	{
		$addressBook = $this->addressBookRepo->getEntryBasicInfo($id);

		//default search by unless selected
		$searchBy = Input::get('search_by', 'Name');
		$search = Input::get('search', '');


		$active = 'active';
		if ($status == 'completed') {
			$active = 'inactive';
		}

		$projects = $this->projectRepo->searchProjectsBySubcontractor($id, Config::get('constants.projects.status.'.$active), $searchBy, $search);
		$sortUrl = $status;
		$searchUrl = $status;

		return view('subcontractors.projects', compact('projects', 'sortUrl', 'searchUrl', 'addressBook'));
	}


}
