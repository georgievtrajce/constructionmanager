<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateAddressBookAddressesRequest;
use App\Modules\Addresses\Implementations\AddressesClassMap;
use App\Modules\Addresses\Implementations\AddressesWrapper;
use App\Modules\Addresses\Repositories\AddressesRepository;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class AddressesController extends Controller {

	public function __construct()
	{
		$this->middleware('module_write', ['only' => ['store','show','edit','update']]);
		$this->middleware('module_delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param $addressType
	 * @param $companyId
	 * @param AddressesWrapper $addressesWrapper
	 * @param AddressBookRepository $addressBookRepo
	 * @return Response
	 * @throws \Exception
	 */
	public function index($addressType, $companyId, AddressesWrapper $addressesWrapper, AddressBookRepository $addressBookRepo)
	{
		$data['addresses'] = $addressesWrapper->implementGetAddresses(ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.get')), $companyId);
		$data['basicInfo'] = $addressBookRepo->getEntryBasicInfo($companyId);
		$data['companyId'] = $companyId;
		return view('address_book.create-addresses', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $addressType
	 * @param $companyId
	 * @param CreateAddressBookAddressesRequest $request
	 * @param AddressesWrapper $addressesWrapper
	 * @param AddressesClassMap $map
	 * @return Response
	 * @throws \Exception
	 */
	public function store($addressType, $companyId, CreateAddressBookAddressesRequest $request, AddressesWrapper $addressesWrapper, AddressesClassMap $map)
	{
		$storeAddress = $addressesWrapper->implementStoreAddress(ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.store')), $companyId, $request->all());

		//get redirection page
		$redirectPage = $map->getIndexPageRedirect($addressType, $companyId);

		if ($storeAddress) {
			Flash::success(trans('messages.addressBook.store.success', ['item' => 'address']));
			//redirect
			return Redirect::to($redirectPage);
		}

		Flash::error(trans('messages.addressBook.store.error', ['item' => 'address']));
		//redirect
		return Redirect::to($redirectPage);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $addressType
	 * @param $companyId
	 * @param $addressId
	 * @param AddressesWrapper $addressesWrapper
	 * @param AddressBookRepository $addressBookRepo
	 * @return Response
	 * @throws \Exception
	 * @internal param int $id
	 */
	public function edit($addressType, $companyId, $addressId, AddressesWrapper $addressesWrapper, AddressBookRepository $addressBookRepo)
	{
		$data['addresses'] = $addressesWrapper->implementGetAddresses(ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.get')), $companyId);
		$data['address'] = $addressesWrapper->implementGetEditAddress(ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.get')), $companyId, $addressId);
		$data['basicInfo'] = $addressBookRepo->getEntryBasicInfo($companyId);
		$data['companyId'] = $companyId;
		$data['addressId'] = $addressId;

		$typeFolder = str_replace("-","_",$addressType);

		return view($typeFolder.'.edit-address', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $addressType
	 * @param $companyId
	 * @param $addressId
	 * @param CreateAddressBookAddressesRequest $request
	 * @param AddressesWrapper $addressesWrapper
	 * @return Response
	 * @throws \Exception
	 * @internal param int $id
	 */
	public function update($addressType, $companyId, $addressId, CreateAddressBookAddressesRequest $request, AddressesWrapper $addressesWrapper)
	{
		$response = $addressesWrapper->implementUpdateAddress(ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.update')), $companyId, $addressId, $request->all());

		if ($response) {
			Flash::success(trans('messages.addressBook.update.success', ['item' => 'Address']));
			return Redirect::to('/'.$addressType.'/'.$companyId.'/addresses/'.$addressId.'/edit');
		}

		Flash::error(trans('messages.addressBook.update.error', ['item' => 'Address']));
		return Redirect::to('/'.$addressType.'/'.$companyId.'/addresses/'.$addressId.'/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $addressType
	 * @param $companyId
	 * @param $addressId
	 * @param AddressesRepository $addressesRepository
	 * @param AddressesClassMap $map
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy($addressType, $companyId, $addressId, AddressesRepository $addressesRepository, AddressesClassMap $map)
	{
		$switchDeleteClass = ClassMap::instance(Config::get('classmap.addresses.'.$addressType.'.delete'));

        //split addresses if multiple
        $addressIdsArray = explode('-', $addressId);

        $errorCheck = false;
        foreach ($addressIdsArray as $id) {
            //check if this address is connected to some module entity
            $checkAddress = $switchDeleteClass->checkForDelete($id);

            if (is_null($checkAddress)) {
                $errorCheck = true;
            } else {
                //Delete address
                $addressesRepository->deleteAddress($id);
            }
        }

		//get redirection page
		$redirectPage = $map->getIndexPageRedirect($addressType, $companyId);

		//Don't delete the address if there are existing connected entities with this address
		if ($errorCheck) {
			Flash::info(trans('messages.addressBook.delete.info', ['item' => 'address']));
			//redirect
			return Redirect::to($redirectPage);
		}

		//Redirect with message for successful deleting
        Flash::success(trans('messages.addressBook.delete.success', ['item' => 'Address']));
        //redirect
        return Redirect::to($redirectPage);
	}

}
