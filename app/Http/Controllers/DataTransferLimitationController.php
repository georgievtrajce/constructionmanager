<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Mail\Repositories\MailRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class DataTransferLimitationController extends Controller {
    /**
     * @var MailRepository
     */
    private $mailRepository;

    /**
     * DataTransferLimitationController constructor.
     * @param MailRepository $mailRepository
     */
    public function __construct(MailRepository $mailRepository)
    {
        $this->mailRepository = $mailRepository;
    }

    /**
     * Increase upload data transfer limit (ajax route)
     * @param DataTransferLimitation $transferLimitation
     * @return bool
     */
    public function increaseUploadTransfer(DataTransferLimitation $transferLimitation)
    {
        $filesize = Input::get('filesize');

        if (!empty($filesize)) {
            //increase company upload transfer
            $transferLimitation->increaseUploadTransfer($filesize);

            return 'true';
        }
        return 'false';
    }

    /**
     * Increase download data transfer limit (ajax route)
     * @param DataTransferLimitation $transferLimitation
     * @return bool
     */
    public function increaseDownloadTransfer(DataTransferLimitation $transferLimitation)
    {
        $filesize = Input::get('filesize');

        if (!empty($filesize)) {
            //increase company upload transfer
            $transferLimitation->increaseDownloadTransfer($filesize);

            return 'true';
        }
        return 'false';
    }

}
