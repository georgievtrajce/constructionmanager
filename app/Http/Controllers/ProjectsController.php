<?php
namespace App\Http\Controllers;
use App\Http\Requests\ProjectsRequest;
use App\Models\Master_format_type;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Blog\Repositories\BlogRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project_companies\Implementations\ProjectCompanies;
use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Services\Location;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Laracasts\Flash\Flash;

class ProjectsController extends Controller {

	private $repository;
    /**
     * @var ProjectSubcontractorsRepository
     */
    private $projectSubcontractorRepo;
    /**
     * @var Location
     */
    private $location;
    /**
     * @var ProposalsRepository
     */
    private $proposalsRepository;

    /**
     * @var AddressBookRepository
     */
    private $addressBookRepository;

    /**
     * ProjectsController constructor.
     * @param ProjectRepository $projectRepo
     * @param ProjectSubcontractorsRepository $projectSubcontractorRepo
     * @param Location $location
     * @param ProposalsRepository $proposalsRepository
     * @param AddressBookRepository $addressBookRepository
     */
    public function __construct(ProjectRepository $projectRepo, ProjectSubcontractorsRepository $projectSubcontractorRepo,
                                Location $location, ProposalsRepository $proposalsRepository, AddressBookRepository $addressBookRepository)
	{
		$this->repository = $projectRepo;

		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		$this->middleware('auth');
		$this->middleware('level_zero_permit', [
			'only' => [
				'create',
				'store',
				'allUploadedFiles',
				'allBlogPosts',
				'edit',
				'changeProjectAdmin',
			]]);

		//$this->middleware('subscription_level_one', ['only' => ['completed','shared']]);
		//$this->middleware('subscription_level_two', ['only' => ['completed']]);
		$this->middleware('project_admin_restrictions', ['only' => ['show','edit','update','destroy']]);
		$this->middleware('module_write', ['only' => ['create','store','edit','update']]);
		$this->middleware('module_delete', ['only' => ['destroy']]);
        $this->projectSubcontractorRepo = $projectSubcontractorRepo;
        $this->location = $location;
        $this->proposalsRepository = $proposalsRepository;
        $this->addressBookRepository = $addressBookRepository;
    }

	public function index()
	{
		$sort = Input::get('sort','ID');
		$order = Input::get('order','DESC');

		//get projects by company id
		$projects = $this->repository->getAllProjects(Config::get('constants.projects.status.active'), $sort, $order);
		$sortUrl = '';
		$searchUrl = 'created';

		return view('projects.index', compact('projects', 'sortUrl', 'searchUrl'));
	}

	public function completed()
	{
		//default sort and order unless selected
		$sort = Input::get('sort','ID');
		$order = Input::get('order','asc');

		$projects = $this->repository->getAllProjects(Config::get('constants.projects.status.inactive'), $sort, $order);
		$sortUrl = 'completed';
		$searchUrl = 'completed';

		return view('projects.index', compact('projects', 'sortUrl', 'searchUrl'));
	}

	public function shared()
	{
		//default sort and order unless selected
		$sort = Input::get('sort','ID');
		$order = Input::get('order','asc');

		$projects = $this->repository->getSharedProjects($sort, $order);
		$sortUrl = 'shared';
		$searchUrl = 'shared';

		return view('projects.index', compact('projects', 'sortUrl', 'searchUrl'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $architectId = (!empty(Request::old('architect_id'))) ? Request::old('architect_id') : 0;
        $primeSubcontractorId = (!empty(Request::old('prime_subcontractor_id'))) ? Request::old('prime_subcontractor_id') : 0;
        $ownerId = (!empty(Request::old('owner_id'))) ? Request::old('owner_id') : 0;
	    $contractorId = (!empty(Request::old('general_contractor_id'))) ? Request::old('general_contractor_id') : 0;
	    $makeMeGeneralContractor = (!empty(Request::old('make_me_gc'))) ? 1 : 0;

        $masterFormatTypes = Master_format_type::orderBy('id', 'DESC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        $masterFormatTypes = $masterFormatTypesArray;

        $architectTransmittals = [];
        $primeSubcontractorTransmittals = [];
        $contractorTransmittals = [];
	    if($architectId) {
            $architectTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId(null, $architectId);
        }
        if($primeSubcontractorId) {
            $primeSubcontractorTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId(null, $primeSubcontractorId);
        }
        if($ownerId) {
            $ownerTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId(null, $ownerId);
        }
        if($contractorId || $makeMeGeneralContractor) {
            $contractorTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId(null, $contractorId, $makeMeGeneralContractor);
        }
		return view('projects.create', compact('architectTransmittals', 'contractorTransmittals', 'primeSubcontractorTransmittals', 'ownerTransmittals', 'masterFormatTypes'));
	}

    /**
     * store project with input parameters from request
     * @param ProjectsRequest $request
     * @param ProjectCompanies $projectCompanies
     * @return \Illuminate\Http\RedirectResponse
     */
	public function store(ProjectsRequest $request, ProjectCompanies $projectCompanies)
	{
		$storeProject = $this->repository->storeProject($request->all());

		if ($storeProject) {

		    $data = [
		        'proj_id' => $storeProject->id,
		        'architect_id' => $request->get('architect_id'),
                'prime_subcontractor_id' => $request->get('prime_subcontractor_id'),
                'owner_id' => $request->get('owner_id'),
		        'general_contractor_id' => $request->get('general_contractor_id'),
		        'make_me_gc' => $request->get('make_me_gc'),
		        'architect_transmittal_id' => $request->get('architect_transmittal_id'),
		        'prime_subcontractor_transmittal_id' => $request->get('prime_subcontractor_transmittal_id'),
		        'owner_transmittal_id' => $request->get('owner_transmittal_id'),
		        'contractor_transmittal_id' => $request->get('contractor_transmittal_id'),
		        'transmittal_flag' => $request->get('transmittal_flag')
            ];

		    $projectCompanies->generateProjectCompany($data);

			Flash::success(trans('messages.projectItems.insert.success', ['item' => 'project']));
            return Redirect::to(url('/projects/'.$storeProject->id));
		} else {
			Flash::error(trans('messages.projectItems.insert.error', ['item' => 'project']));
		}
		return redirect()->action('ProjectsController@index');
	}

	/**
	 * Store subcontractor info for a specific project
	 * @param $projectId
	 * @param ProjectsRequest $request
	 * @return mixed
     */
	public function subcontractorProjectInfoStore($projectId, ProjectsRequest $request)
	{
		$storeSubcontractorInfo = $this->repository->storeSubcontractorProjectInfo($projectId, $request->all());

		if ($storeSubcontractorInfo) {
			Flash::success(trans('messages.projectItems.insert.success', ['item' => 'project']));
		} else {
			Flash::error(trans('messages.projectItems.insert.error', ['item' => 'project']));
		}
		return redirect()->action('ProjectsController@index');
	}

    /**
     * get project by id
     * Display the specified resource.
     *
     * @param  int $id
     * @param FilesRepository $filesRepository
     * @param BlogRepository $blogRepository
     * @param PcosRepository $pcosRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param ProjectRepository $projectRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepository
     * @return Response
     */
	public function show($id, FilesRepository $filesRepository, BlogRepository $blogRepository,
                         PcosRepository $pcosRepository, SubmittalsRepository $submittalsRepository,
                         RfisRepository $rfisRepository, ProjectRepository $projectRepository,
                         ProjectPermissionsRepository $projectPermissionsRepository)
	{
		$number = 5;
		$files = $filesRepository->getLastUploadedFilesByProject($number, $id);
		$blogPosts = $blogRepository->getLastBlogPostsByProject($number, $id);
		$project = $this->repository->getProject($id);

        //calculate used storage
        $usedStorageByProject = $filesRepository->getUsedStorageByProject($id);
        if (!empty($usedStorageByProject>0)) {
            $base = log($usedStorageByProject) / log(1024);
            $suffix = array("bytes", "kb", "MB", "GB", "TB")[floor($base)];
            $sizeSum = round(pow(1024, $base - floor($base)), 2) .' '. $suffix;
        } else {
            $sizeSum = '0 bytes';
        }
        $transferLimitation = new DataTransferLimitation();
        $percentageSum = $transferLimitation->findStorageUsedAllowancePercentage($usedStorageByProject);

        //get submittals
        $submittals = $submittalsRepository->getAllSubmittalsForProject($id, Auth::user()->comp_id);
        $submittalsStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($submittals as $submittal) {
            if (!empty($submittal) && !empty($submittal->status_short_name)) {

                if(in_array($submittal->status_short_name, Config::get('constants.all_open'))) {
                    $submittalsStatistics['Open']++;
                } else if (in_array($submittal->status_short_name, Config::get('constants.all_closed'))) {
                    $submittalsStatistics['Closed']++;
                } else {
                    $submittalsStatistics['Draft']++;
                }
            }
        }

        //get RFIS
        $rfis = $rfisRepository->getAllRFIsForProject($id, Auth::user()->comp_id);
        $rfisStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($rfis as $rfi) {
            if (!empty($rfi) && !empty($rfi->status_short_name)) {
                if (isset($rfisStatistics[$rfi->status_short_name])) {
                    $rfisStatistics[$rfi->status_short_name]++;
                } else {
                    $rfisStatistics[$rfi->status_short_name] = 1;
                }
            }
        }

        //get pcos statistics
        $pcos = $pcosRepository->getAllPcosForProject($id, Auth::user()->comp_id);
        $pcoStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($pcos as $pco) {
            if (!empty($pco->recipient) && !empty($pco->recipient->latest_recipient_version) && !empty($pco->recipient->latest_recipient_version->status)) {

                if(in_array($pco->recipient->latest_recipient_version->status->short_name, Config::get('constants.all_open'))) {
                    $pcoStatistics['Open']++;
                } else if (in_array($pco->recipient->latest_recipient_version->status->short_name, Config::get('constants.all_closed'))) {
                    $pcoStatistics['Closed']++;
                } else {
                    $pcoStatistics['Draft']++;
                }
            }
        }

        //get city by zip code
        $city = null;
        if (!empty($project->address) && !empty($project->address->zip)) {
            $city = $this->location->getCityByZipCode(str_replace(' ','', $project->address->zip));
        }

        if (empty($city) && !empty($project->address) && !empty($project->address->city) && !empty($project->address->state)) {
            $city = $this->location->getCityByNameAndState($project->address->city, $project->address->state);
        }

        if (Auth::user()->comp_id != $project->comp_id && empty($city)) {
            if(!is_null($project->subcontractorAddress) && !empty($project->subcontractorAddress->zip)) {
                $city = $this->location->getCityByZipCode($project->subcontractorAddress->zip);
            }  else if (!empty($project->subcontractorAddress) && !empty($project->subcontractorAddress->city) && !empty($project->subcontractorAddress->state)) {
                $city = $this->location->getCityByNameAndState($project->subcontractorAddress->city, $project->subcontractorAddress->state);
            }
        }

        $projectDownloadRequest = $projectRepository->getBatchDownloadRequestByProject($project->id);

        $batchDownloadPermission = $projectPermissionsRepository->getUserPermissionBatchDownloadByProject($project->id, Auth::user()->id);

		return view('projects.view', compact('project', 'files', 'blogPosts', 'city', 'sizeSum', 'percentageSum',
            'pcoStatistics', 'submittalsStatistics', 'rfisStatistics', 'projectDownloadRequest', 'usedStorageByProject', 'batchDownloadPermission'));
	}

    /**
     * Generate pdf report from filtered or all listed submittals
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     * @throws \Exception
     */
    public function mergePdfReport($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, SubmittalsRepository $submittalsRepository)
    {
        //default settings
        $masterFormat = '';
        $subcontractor = '';
        $recipient = '';
        $sort = 'number';
        $order = 'asc';

        //set status to all open
        $status = 'all_open';

        //Get submittals
        $data['submittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.'submittals-report')), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

        //Get pcos
        $data['pcos'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.'pcos-report')), compact('masterFormat', 'recipient', 'status', 'projectId', 'sort', 'order'));

        //set status to Open for RFIs
        $status = '1';

        //Get rfis
        $data['rfis'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.'rfis-report')), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.report';

        //Set report title
        $data['mf_title'] = "";
        $data['sub_title'] = "";
        $data['status_title'] = "";

        //set status title
        $statusString = 'All Open';
        $data['status_title'] = "Status: ".$statusString;

        //Set report name
        $name = "Submittals, RFIs, PCOs";

        //Return generated report
        return $generator->generateReport('submittals-report', $data, $view, $name);
    }

	/**
	 * Get all uploaded files per project
	 * @param $id
	 * @param FilesRepository $filesRepository
	 * @return \Illuminate\View\View
     */
	public function allUploadedFiles($id, FilesRepository $filesRepository)
	{
		$files = $filesRepository->getAllUploadedFiles($id);
		$project = $this->repository->getProject($id);
		return view('projects.all_uploaded_files', compact('project', 'files'));
	}

	/**
	 * Get all blog posts per project
	 * @param $id
	 * @param BlogRepository $blogRepository
	 * @return \Illuminate\View\View
     */
	public function allBlogPosts($id, BlogRepository $blogRepository)
	{
		$blogPosts = $blogRepository->getAllBlogPosts($id);
		$project = $this->repository->getProject($id);
		return view('projects.all_blog_posts', compact('project', 'blogPosts'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//load project with info for editing

        $masterFormatTypes = Master_format_type::orderBy('id', 'ASC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        $masterFormatTypes = $masterFormatTypesArray;

		$project = $this->repository->getProject($id);
        $architectTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId($id, $project->architect_id);
        $contractorTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId($id, $project->general_contractor_id, $project->make_me_gc);
        $primeSubcontractorTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId($id, $project->prime_subcontractor_id);
        $ownerTransmittals = $this->addressBookRepository->getProjectTransmittalsByProjectId($id, $project->owner_id);
        if (Auth::user()->comp_id != $project->comp_id) {
			return redirect()->action('ProjectsController@index');
		}
		return view('projects.edit', compact('project','contractorTransmittals', 'architectTransmittals', 'primeSubcontractorTransmittals', 'ownerTransmittals', 'masterFormatTypes'));
	}

	/**
	 * Edit page for subcontractors info for a specific project
	 * @param $id
	 * @return \Illuminate\View\View
     */
	public function subcontractorProjectInfoEdit($id)
	{
		$project = $this->repository->getProject($id);

		if (Auth::user()->comp_id == $project->comp_id) {
			return redirect()->action('ProjectsController@index');
		}
		return view('projects.edit-subcontractor', compact('project'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param ProjectsRequest $request
     * @param ProjectCompanies $projectCompanies
     * @return Response
     */
	public function update($id, ProjectsRequest $request, ProjectCompanies $projectCompanies)
	{
		//update project data with input parameters from request
		$updateProject = $this->repository->updateProject($id, $request->all());

		if ($updateProject) {

            $data = [
                'proj_id' => $id,
                'architect_id' => $request->get('architect_id'),
                'prime_subcontractor_id' => ($request->get('prime_subcontractor') == '')?null:$request->get('prime_subcontractor_id'),
                'owner_id' => $request->get('owner_id'),
                'general_contractor_id' => $request->get('general_contractor_id'),
                'make_me_gc' => $request->get('make_me_gc'),
                'architect_transmittal_id' => $request->get('architect_transmittal_id'),
                'prime_subcontractor_transmittal_id' => $request->get('prime_subcontractor_transmittal_id'),
                'owner_transmittal_id' => $request->get('owner_transmittal_id'),
                'contractor_transmittal_id' => $request->get('contractor_transmittal_id'),
                'transmittal_flag' => $request->get('transmittal_flag'),
            ];
            $projectCompanies->generateProjectCompany($data);

			Flash::success(trans('messages.projectItems.update.success', ['item' => 'project']));
            return Redirect::to(url('/projects/'.$id));
		} else {
			Flash::error(trans('messages.projectItems.update.error', ['item'=> 'project']));
		}

		return Redirect::to('projects/'.$id.'/edit');
	}

	/**
	 * Update subcontractor info for a specific project
	 * @param $projectId
	 * @param ProjectsRequest $request
	 * @return mixed
     */
	public function subcontractorProjectInfoUpdate($projectId, ProjectsRequest $request)
	{
		//update project data with input parameters from request
		$updateProject = $this->repository->updateSubcontractorProjectInfo($projectId,$request->all());

		if ($updateProject) {
			Flash::success(trans('messages.projectItems.update.success', ['item' => 'project']));
		} else {
			Flash::error(trans('messages.projectItems.update.error', ['item'=> 'project']));
		}

		return Redirect::to('projects/'.$projectId.'/subcontractor/project-info/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $idsArray = explode('-', $id);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $jointIds = explode("|", $id);
            if (count($jointIds) > 1) {
                if (!empty($jointIds[1])) {
                    $response = $this->projectSubcontractorRepo->unshareProject($jointIds[0], $jointIds[1]);
                }

                if (!empty($jointIds[2])) {
                    $response = $this->proposalsRepository->unshareProject($jointIds[0], $jointIds[2]);
                }

            } else {
                $response = $this->repository->deleteProject($jointIds[0]);
            }

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        //Response is null if this address book company is connected with some other project module
        if ($errorEntry) {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'project']));
        }

        Flash::success(trans('messages.projectItems.delete.success', ['item' => 'project']));
		return redirect()->action('ProjectsController@index');
	}

    /**
     * Unshare the specified resource from storage. (Probably not used anymore)
     *
     * @param  int $id
     * @param $projectSubcontractorId
     * @return Response
     */
    public function destroyShared($id, $projectSubcontractorId)
    {
        $unShareProject = $this->projectSubcontractorRepo->unshareProject($id, $projectSubcontractorId);

        if ($unShareProject) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'project']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'project']));
        }

        return redirect()->action('ProjectsController@index');
    }

	/**
	 * Search projects
	 * @return \Illuminate\View\View
     */
	public function search()
	{
		$sort = Input::get('sort');
		$order = Input::get('order');

		//default search by unless selected
		$searchBy = Input::get('search_by','name');
		$search = Input::get('search','');

		$projectType = Request::segment(3);

		//search created projects
		if($projectType == 'created') {
			$projects = $this->repository->searchProjects(Config::get('constants.projects.status.active'), $searchBy, $search, $sort, $order);
			$sortUrl = '';
			$searchUrl = 'created';
		}

		//search completed projects
		if($projectType == 'completed') {
			$projects = $this->repository->searchProjects(Config::get('constants.projects.status.inactive'), $searchBy, $search, $sort, $order);
			$sortUrl = 'completed';
			$searchUrl = 'completed';
		}

		//search shared projects
		if($projectType == 'shared') {
			$projects = $this->repository->searchSharedProjects($searchBy, $search, $sort, $order);
			$sortUrl = 'shared';
			$searchUrl = 'shared';
		}

		return view('projects.index', compact('projects','sortUrl','searchUrl'));
	}

	/**
	 * Change Project Admin
     */
	public function changeProjectAdmin()
	{
		$userId = Input::get('userId');
		$projectId = Input::get('projectId');
        $removePermission = (Input::get('removePermission') == 'true')?true:false;

		if(!empty($userId) && !empty($projectId)) {
			$response = $this->repository->changeProjectAdmin($userId, $projectId, $removePermission);
			if ($response) {
				return Response::json(['success' => $response, 200]);
			}
			return Response::json(['error' => $response, 400]);
		}

		return Response::json(['error' => 'Not a proper request', 400]);
	}

    /**
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param FilesRepository $filesRepository
     * @param DataTransferLimitation $dataTransferLimitation
     * @return mixed
     */
    public function batchDownload($projectId, ProjectRepository $projectRepository,
                                  FilesRepository $filesRepository, DataTransferLimitation $dataTransferLimitation)
    {
        $usedStorageByProject = $filesRepository->getUsedStorageByProject($projectId);

        if ($dataTransferLimitation->checkDownloadTransferAllowance($usedStorageByProject, $projectId)) {
            $projectRepository->addBatchDownloadRequest($projectId);
            Flash::success(trans('messages.projects.request-for-batch-sent'));
        } else {
            Flash::error(trans('messages.projects.error-request-for-batch-sent'));
        }

        return Redirect::to(url('projects/'.$projectId));
    }

}
