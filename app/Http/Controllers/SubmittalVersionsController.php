<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AddSubmittalVersionRequest;
use App\Http\Requests\EditSubmittalVersionRequest;
use App\Http\Requests\EditSubmittalVersionTransmittalNotesRequest;
use App\Models\File_type;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Modules\Transmittals\Repositories\TransmittalsRepository;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class SubmittalVersionsController extends Controller {

    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    public function __construct(CompaniesRepository $companiesRepository)
	{
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->companiesRepository = $companiesRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $projectId
	 * @param $submittalId
	 * @param ProjectRepository $projectRepository
	 * @param SubmittalsRepository $submittalsRepository
	 * @return Response
	 */
	public function create($projectId, $submittalId, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository)
	{
	    $project = $projectRepository->getProject($projectId);
		$data['project'] = $project;
		$data['submittal'] = $submittalsRepository->getSubmittal($submittalId);

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

		//get all submittal versions statuses and prepare them to array for the view
		$statuses = $submittalsRepository->getAllSubmittalStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

        $data['submittedFor'] = ['Approval', 'Review', 'Record', 'Info', 'For Your Use'];

		//get status and submitted for from last version id
        $data['lastVersionStatus'] = 1;
        $data['lastSubmittedFor'] = $data['submittedFor'][0];
        if(count($data['submittal']['submittals_versions']) > 0){
            foreach ($data['submittal']['submittals_versions'] as $value) {
                if($value['id'] == $data['submittal']->last_version){
                    $data['lastVersionStatus'] = $value['status_id'];
                    $data['lastSubmittedFor'] = $value['submitted_for'];
                }
            }
        }

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;

		return view('projects.submittals.add-version', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $projectId
	 * @param $submittalId
	 * @param AddSubmittalVersionRequest $request
	 * @param SubmittalsRepository $submittalsRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return Response
	 */
	public function store($projectId, $submittalId, AddSubmittalVersionRequest $request,SubmittalsRepository $submittalsRepository, DataTransferLimitation $transferLimitation)
	{
		//Store submittal version
		$response = $submittalsRepository->storeSubmittalVersion($submittalId, $request->all());

		if($response) {
			//Successfully created submittal
			Flash::success(trans('messages.submittals.store.success', ['item' => 'Submittal version']));
            return Redirect::to('projects/'.$projectId.'/submittals/'.$submittalId.'/version/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'Submittal version']));
		return Redirect::to('projects/' . $projectId . '/submittals');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $submittalId
     * @param $versionId
     * @param SubmittalsRepository $submittalsRepository
     * @param ProjectRepository $projectRepository
     * @param TransmittalsRepository $transmittalsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @param ManageUsersRepository $manageUsersRepository
     * @return Response
     * @internal param int $id
     */
    public function edit(
        $projectId,
        $submittalId,
        $versionId,
        SubmittalsRepository $submittalsRepository,
        ProjectRepository $projectRepository,
        TransmittalsRepository $transmittalsRepository,
        CheckUploadedFilesInterface $checkUploadedFiles,
        ManageUsersRepository $manageUsersRepository
    ) {
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

        $projectUsers = $manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

		//get all submittal versions statuses and prepare them to array for the view
		$statuses = $submittalsRepository->getAllSubmittalStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

        $data['submittedFor'] = ['Approval', 'Review', 'Record', 'Info', 'For Your Use'];
		$data['submittalVersion'] = $submittalsRepository->getSubmittalVersion($versionId);
        if(isset($data['submittalVersion']['file']) && count($data['submittalVersion']['file']) > 0)
        {
            $data['submittalVersion']['email_transmittal_files'] = $checkUploadedFiles->checkAndOrderUploadedFilesForVersion($data['submittalVersion']['file'], Config::get('constants.submittals_versions'));
        }
		$data['rec_sub'] = ($data['submittalVersion']->rec_sub != 0) ? Carbon::parse($data['submittalVersion']->rec_sub)->format('m/d/Y') : '';
		$data['sent_appr'] = ($data['submittalVersion']->sent_appr != 0) ? Carbon::parse($data['submittalVersion']->sent_appr)->format('m/d/Y') : '';
		$data['rec_appr'] = ($data['submittalVersion']->rec_appr != 0) ? Carbon::parse($data['submittalVersion']->rec_appr)->format('m/d/Y') : '';
		$data['subm_sent_sub'] = ($data['submittalVersion']->subm_sent_sub != 0) ? Carbon::parse($data['submittalVersion']->subm_sent_sub)->format('m/d/Y') : '';

		//check if transmittals are generated
		$data['recTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.submittals') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.sent_appr'));
		$data['subTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.submittals') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.subm_sent_sub'));

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $distributionListContactsAppr = $submittalsRepository->getContactsForSubmittalVersion($versionId, 'appr');
        $selectedDistributionsAppr = [];
        $selectedDistributionsUsersAppr = [];
        foreach ($distributionListContactsAppr as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsAppr[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsersAppr[] = $contact->user_id;
            }
        }

        $distributionListContactsSubc = $submittalsRepository->getContactsForSubmittalVersion($versionId, 'subc');
        $selectedDistributionsSubc = [];
        $selectedDistributionsUsersSubc = [];
        foreach ($distributionListContactsSubc as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsSubc[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsersSubc[] = $contact->user_id;
            }

        }


        $data['selectedDistributionsAppr'] = $selectedDistributionsAppr;
        $data['selectedDistributionsSubc'] = $selectedDistributionsSubc;
        $data['selectedDistributionsUsersAppr'] = $selectedDistributionsUsersAppr;
        $data['selectedDistributionsUsersSubc'] = $selectedDistributionsUsersSubc;
        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;
        $data['projectUsers'] = $projectUserIds;

		return view('projects.submittals.edit-version', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $projectId
	 * @param $submittalId
	 * @param $versionId
	 * @param EditSubmittalVersionRequest $request
	 * @param SubmittalsRepository $submittalsRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return Response
	 * @internal param int $id
	 */
	public function update($projectId, $submittalId, $versionId, EditSubmittalVersionRequest $request, SubmittalsRepository $submittalsRepository, DataTransferLimitation $transferLimitation)
	{
		//Update submittal version
		$updateResponse = $submittalsRepository->updateSubmittalVersion($versionId, $request->all());
		if ($updateResponse) {
			//Successfully updated submittal
			Flash::success(trans('messages.submittals.update.success', ['item' => 'Submittal version']));
			return Redirect::to('projects/'.$projectId.'/submittals/'.$submittalId.'/version/'.$versionId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'Submittal version']));
		return Redirect::to('projects/'.$projectId.'/submittals/'.$submittalId.'/version/'.$versionId.'/edit');
	}

    /**
     * Update transmittal notes for submittal version
     *
     * @param $projectId
     * @param $submittalId
     * @param $versionId
     * @param EditSubmittalVersionRequest|EditSubmittalVersionTransmittalNotesRequest $request
     * @param SubmittalsRepository $submittalsRepository
     * @return Response
     */
    public function updateTransmittalNotes($projectId, $submittalId, $versionId, EditSubmittalVersionTransmittalNotesRequest $request, SubmittalsRepository $submittalsRepository)
    {
        //Update submittal version
        $submittalsRepository->updateSubmittalVersionTransmittalNotes($versionId, $request->all());
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $projectId
	 * @param $submittalId
	 * @param $versionId
	 * @param SubmittalsRepository $submittalsRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy($projectId, $submittalId, $versionId, SubmittalsRepository $submittalsRepository)
	{

        $idsArray = explode('-', $versionId);

        foreach ($idsArray as $id) {

            //select submittal version for file deleting
            $submittalVersion = $submittalsRepository->getSubmittalVersion($id);


            //delete submittal version files from s3
            if (count($submittalVersion->file)) {
                foreach ($submittalVersion->file as $file) {
                    $submittalsRepository->deleteSubmittalFile($projectId, $file->file_name);
                }
            }

            //check if it's a current version
            if ($submittalsRepository->lastVersionCheck($submittalId, $id)) {
                //change current submittal version
                $submittalsRepository->changeLastVersion($submittalId, $id);
            }

            //delete submittal version from database (with the file from files table)
            $submittalsRepository->deleteSubmittalVersion($id);
        }

		//Successfully deleted submittal version
		Flash::success(trans('messages.submittals.delete.success', ['item' => 'Submittal']));
		return Redirect::to('projects/'.$projectId.'/submittals');
	}

	/**
	 * Download submittal file
	 * @param $projectId
	 * @param $submittalId
	 * @param $versionId
	 * @param $fileName
	 * @param SubmittalsRepository $submittalsRepository
	 * @param FilesRepository $filesRepository
	 * @param ProjectRepository $projectRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return mixed
	 */
	public function download($projectId, $submittalId, $versionId, $fileName, SubmittalsRepository $submittalsRepository, FilesRepository $filesRepository, ProjectRepository $projectRepository, DataTransferLimitation $transferLimitation)
	{
		//Get submittal version
		$submittal = $submittalsRepository->getSubmittalVersion($versionId);

		//get file size
		$fileSize =  $submittal->file[0]->size;

		//check download file allowance
		if ($transferLimitation->checkDownloadTransferAllowance($fileSize)) {
			$project = $projectRepository->getProjectRecord($projectId);

			//Download submittal version file
			$file = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $projectId . '/submittals/' . $fileName);
			$filesRepository->logDownloadByName($fileName);

			//increase company download transfer
			$transferLimitation->increaseDownloadTransfer($fileSize);

			return Response::make($file, 200)->header('Content-type', 'application/*', 'Content-Disposition', 'attachment');
		}

		Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => trans('labels.submittal_global')]));
		return Redirect::back();
	}

}
