<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateProjectFileRequest;
use App\Http\Requests\UploadProjectFileRequest;
use App\Models\File_type;
use App\Models\Project_file;
use App\Modules\Blog\Repositories\BlogRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Daily_reports\Implementations\DailyReport;
use App\Modules\Daily_reports\Repositories\DailyReportsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Plupload\PluploadHandler;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Implementations\NotificationsWrapper;
use App\Modules\Project_files\Implementations\ProjectFilesMap;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Services\Files\FileDeleteWrapper;
use App\Services\Files\FileStoreWrapper;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use League\Flysystem\Exception;
use Symfony\Component\HttpFoundation\File\File;

class ProjectFilesController extends Controller {
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     *The request goes through project_files_write and project_files_delete middlewares.
     * This two middlewares checks the write and delete permissions of the company user
     * for managing with the project files
     * @param CompaniesRepository $companiesRepository
     */
	public function __construct(CompaniesRepository $companiesRepository)
	{
		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		$this->middleware('level_zero_permit',[
			'except' => [
				'downloadAllowance',
                'showProjectFile',
                'storeModuleFile',
                'storeProposalFile'
			]
		]);

		//$this->middleware('subscription_level_one');
		$this->middleware('project_files_write', ['only' => ['create','store','upload','edit','update','shareAll','shareSelected','unshare']]);
		$this->middleware('project_files_delete', ['only' => ['destroy']]);
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * Main uploaded project files listing method.
     * Lists all files from specific file type
     *
     * @param $projectId
     * @param $type
     * @param ProjectRepository $projectRepository
     * @param ProjectFilesRepository $filesRepository
     * @param ProjectFilesMap $map
     * @param DailyReportsRepository $dailyReportsRepository
     * @return Response
     */
	public function index($projectId, $type, ProjectRepository $projectRepository, ProjectFilesRepository $filesRepository, ProjectFilesMap $map, DailyReportsRepository $dailyReportsRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','files.id');
		$order = Input::get('order','desc');

        $data['type'] = $type;

		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		//get project files by type
        ($type == Config::get('constants.daily_report_file_type')) ? $sortTypes = 'files.id' : $sortTypes = $sort;
		$projectFiles = $filesRepository->getAllProjectFilesByType($projectId, $type, $sortTypes, $order);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$type)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;

		//create new project file link
		$data['createLink'] = $map->createLinkMap($projectId, $filesType->display_name);

		//add edit link to the different module type project files
		$data['projectFiles'] = $map->editLinkMap($projectId, $filesType->display_name, $projectFiles);

		//get daily reports
        if($type == Config::get('constants.daily_report_file_type')) {
            $sort = Input::get('sort','daily_reports.report_num');
            ($type == Config::get('constants.daily_report_file_type')) ? $sortDaily = Input::get('sort','daily_reports.report_num') : $sortDaily = 'daily_reports.report_num';
            $data['dailyReports'] = $dailyReportsRepository->getDailyReportByProjectId($projectId, $sortDaily, $order, 31);
        }
		return view('projects.project_files.index', $data);
	}

	/**
	 * List all shares of the uploaded project files
	 * @param $projectId
	 * @param $type
	 * @param ProjectRepository $projectRepository
	 * @param ProjectFilesRepository $filesRepository
	 * @param ProjectFilesMap $map
	 * @return \Illuminate\View\View
	 */
	public function uploadedFileShares($projectId, $type, ProjectRepository $projectRepository, ProjectFilesRepository $filesRepository, ProjectFilesMap $map)
	{
		//get project
		$data['project'] = $projectRepository->getProject($projectId);
        $data['type'] = $type;

		//get all companies with which the logged company have shared some of their uploaded project files
		$data['sharedProjectFiles'] = $filesRepository->getSharedProjectFilesByType($projectId, $type);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$type)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;

		//create new project file link
		$data['createLink'] = $map->createLinkMap($projectId, $filesType->display_name);

		return view('projects.project_files.uploaded-file-shares', $data);
	}

	/**
	 * Main received project files listing method.
	 * Lists all files from specific file type
	 * @param $projectId
	 * @param $type
	 * @param ProjectRepository $projectRepository
	 * @param ProjectFilesRepository $filesRepository
	 * @return \Illuminate\View\View
     */
	public function received($projectId, $type, ProjectRepository $projectRepository, ProjectFilesRepository $filesRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','files.id');
		$order = Input::get('order','desc');

		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		//get received project files by type
		$data['projectFiles'] = $filesRepository->getAllReceivedProjectFilesByType($projectId, $type, $sort, $order);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$type)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;
		return view('projects.project_files.received', $data);
	}

	/**
	 * List all shares of the received project files
	 * @param $projectId
	 * @param $type
	 * @param ProjectRepository $projectRepository
	 * @param ProjectFilesRepository $filesRepository
	 * @return \Illuminate\View\View
     */
	public function receivedFileShares($projectId, $type, ProjectRepository $projectRepository, ProjectFilesRepository $filesRepository)
	{
		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		//get all companies with which the logged company have shared some of their received project files
		$data['sharedProjectFiles'] = $filesRepository->getSharedReceivedProjectFilesByType($projectId, $type);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$type)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;
		return view('projects.project_files.received-file-shares', $data);
	}

	/**
	 * Show the form for uploading new project file.
	 *
	 * @param $projectId
	 * @param $type
	 * @param ProjectRepository $projectRepository
	 * @return Response
	 */
	public function create($projectId, $type, ProjectRepository $projectRepository)
	{
		//get project
		$project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$type)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;
		$data['companyId'] = Auth::user()->comp_id;

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;

		return view('projects.project_files.create', $data);
	}

	/**
	 * Store a newly uploaded file in storage.
	 *
	 * @param $projectId
	 * @param $fileType
	 * @param UploadProjectFileRequest $request
	 * @param ProjectFilesRepository $filesRepository
	 * @param BlogRepository $blogRepo
	 * @return Response
	 */
	public function store($projectId, $fileType, UploadProjectFileRequest $request, ProjectFilesRepository $filesRepository, BlogRepository $blogRepo)
	{
		//store  project file
		$projectFile = $filesRepository->storeOrUpdateProjectFile($fileType, $projectId, $request->all());

		//Successfully stored project file
		Flash::success(trans('messages.global.store.success'));
		return Redirect::to('projects/'.$projectId.'/project-files/'.$fileType.'/file/'.$projectFile->id.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified project file.
	 *
	 * @param $projectId
	 * @param $fileType
	 * @param $fileId
	 * @param ProjectRepository $projectRepository
	 * @param ProjectFilesRepository $filesRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function edit($projectId, $fileType, $fileId, ProjectRepository $projectRepository, ProjectFilesRepository $filesRepository)
	{
		//get project
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

		//get project file
		$data['projectFile'] = $filesRepository->getProjectFile($projectId, $fileId);

		//get selected type of files
		$filesType = File_type::where('display_name','=',$fileType)->first();
		$data['typeName'] = $filesType->name;
		$data['typeDisplay'] = $filesType->display_name;

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $distributionListContactsAppr = $filesRepository->getContactsForProjectFiles($fileId);
        $selectedDistributions = [];
        $selectedDistributionsUsers = [];
        foreach ($distributionListContactsAppr as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributions[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsers[] = $contact->user_id;
            }
        }

        $data['selectedDistributions'] = $selectedDistributions;
        $data['selectedDistributionsUsers'] = $selectedDistributionsUsers;
        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;



		return view('projects.project_files.edit', $data);
	}

	/**
	 * Update the specified project file in database.
	 *
	 * @param $projectId
	 * @param $fileType
	 * @param $fileId
	 * @param UpdateProjectFileRequest $request
	 * @param ProjectFilesRepository $filesRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function update($projectId, $fileType, $fileId, UpdateProjectFileRequest $request, ProjectFilesRepository $filesRepository)
	{
		//update project file
		$filesRepository->storeOrUpdateProjectFile($fileType, $projectId, $request->all(), $fileId);

		//Successfully updated project file
		Flash::success(trans('messages.global.update.success'));
		return Redirect::to('projects/'.$projectId.'/project-files/'.$fileType.'/file');
	}

	/**
	 * Delete specified project file
	 *
	 * @param $projectId
	 * @param $fileType
	 * @param $fileId
	 * @param ProjectFilesRepository $filesRepository
	 * @return Response
	 */
	public function destroy($projectId, $fileType, $fileId, ProjectFilesRepository $filesRepository, DailyReportsRepository $dailyReportsRepository)
	{
        $idsArray = explode('-', $fileId);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            if($fileType == Config::get('constants.daily_report_file_type')) {
                $dailyReportsRepository->deleteCompleteDailyReport($projectId, $id);
            } else {
                //check if the file is shared with any subcontractors
                //if it's shared do not delete it and return info message for the client
                if (count($filesRepository->getProjectFileShareRecords($id)) > 0) {
                    $errorEntry = true;
                }

                //get project file
                $projectFile = $filesRepository->getProjectFile($projectId, $id);

                //delete project file from s3
                $deletedFile = $filesRepository->deleteProjectFileAws($projectId, $fileType, $projectFile->file_name);

                //delete project file from database
                $filesRepository->deleteProjectFile($projectId, $id);
            }
        }

        if ($errorEntry) {
            //Successfully deleted project file
            Flash::info(trans('messages.project_files.delete.info'));
            return Redirect::to('projects/'.$projectId.'/project-files/'.$fileType);
        }

		//Successfully deleted project file
		Flash::success(trans('messages.global.delete.success'));
		return Redirect::to('projects/'.$projectId.'/project-files/'.$fileType.'/file');
	}

	/**
	 * Download project file
	 * @param $projectId
	 * @param $fileType
	 * @param $fileName
	 * @param ProjectFilesRepository $filesRepository
	 * @param ProjectFilesMap $filesMap
	 * @param FilesRepository $filesRepo
	 * @param DataTransferLimitation $transferLimitation
	 * @return mixed
	 */
	public function download($projectId, $fileType, $fileName, ProjectFilesRepository $filesRepository, ProjectFilesMap $filesMap, FilesRepository $filesRepo, DataTransferLimitation $transferLimitation)
	{
		//Get project file
		$projectFile = $filesRepository->getProjectFileByName($projectId, $fileName);

		//check download file allowance
		if ($transferLimitation->checkDownloadTransferAllowance($projectFile->size)) {
			//Download project file
			$file = $filesMap->downloadLinkMap($projectId, $fileType, $projectFile, $projectFile->company->id);
			$filesRepo->logDownloadByName($fileName);

			//increase company download transfer
			$transferLimitation->increaseDownloadTransfer($projectFile->size);

			//return Response::make($file, 200)->header('Content-type','application/*','Content-Disposition', 'attachment');
			$disk = Storage::disk('s3');
			if ($disk->exists($file)) {
				$command = $disk->getDriver()->getAdapter()->getClient()->getCommand('GetObject', [
					'Bucket'                     => Config::get('buckets.bucket'),
					'Key'                        => $file,
					'ResponseContentDisposition' => 'attachment;'
				]);
				$request = $disk->getDriver()->getAdapter()->getClient()->createPresignedRequest($command, '+5 minutes');


				return (string) $request->getUri();
			}

			//return false;
		}

		Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => trans('labels.Project')]));
		return Redirect::back();
	}

	/**
	 * Share selected files with all subcontractors
	 * (without general contractor)
	 * @param ProjectFilesRepository $filesRepository
	 * @return mixed
     */
	public function shareAll(ProjectFilesRepository $filesRepository)
	{
		//get selected files ids
		$selectedFiles = Input::get('selected_files');

		//get project id
		$projectId = Input::get('project_id');

		//share selected files
		$sharedFiles = $filesRepository->shareWithAllSubcontractors($projectId, $selectedFiles);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Share selected files with selected subcontractors
	 * (including general contractor as an option for selection)
	 * @param ProjectFilesRepository $filesRepository
	 * @return mixed
     */
	public function shareSelected(ProjectFilesRepository $filesRepository)
	{
		$selectedFiles = Input::get('selected_files');
		$selectedSubcontractors = Input::get('selected_subcontractors');
		$sharedFiles = $filesRepository->shareWithSelectedSubcontractors($selectedFiles, $selectedSubcontractors);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Unshare specified file with the specific company
	 * @param $projectId
	 * @param $fileId
	 * @param $shareCompanyId
	 * @param $receiveCompanyId
	 * @param ProjectFilesRepository $filesRepository
	 * @return mixed
     */
	public function unshare($projectId, $fileId, $shareCompanyId, $receiveCompanyId, ProjectFilesRepository $filesRepository)
	{
		$completeUnshare = $filesRepository->unshareFile($fileId, $shareCompanyId, $receiveCompanyId);

		if($completeUnshare) {
			//Successfully unshared project file
			Flash::success(trans('messages.project_files.unshare.success'));
			return Redirect::back();
		}

		//File unsharing was not successful
		Flash::error(trans('messages.project_files.unshare.error'));
		return Redirect::back();
	}

	/**
	 * Share all submittals with all subcontractors
	 * @param SubmittalsRepository $submittalsRepository
	 * @return mixed
     */
	public function shareAllSubmittals(SubmittalsRepository $submittalsRepository)
	{
		$selectedFiles = Input::get('selected_files');

		//get project id
		$projectId = Input::get('project_id');

		//share selected files
		$sharedFiles = $submittalsRepository->shareWithAllSubcontractors($projectId, $selectedFiles);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Share selected files with selected subcontractors
	 * (including general contractor as an option for selection)
	 * @param SubmittalsRepository $submittalsRepository
	 * @return mixed
	 * @internal param ProjectFilesRepository $filesRepository
	 */
	public function shareSelectedSubmittals(SubmittalsRepository $submittalsRepository)
	{
		$selectedFiles = Input::get('selected_files');
		$selectedSubcontractors = Input::get('selected_subcontractors');
		$sharedFiles = $submittalsRepository->shareWithSelectedSubcontractors($selectedFiles, $selectedSubcontractors);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Store file (this function is be used for all project files, contracts - the function is accessed with an ajax route)
	 * @param FileStoreWrapper $storeWrapper
	 * @return null
	 * @throws \Exception
     */
	public function storeFile(FileStoreWrapper $storeWrapper)
	{
		$filename = Input::get('filename');
		$filesize = Input::get('filesize');
		$type = Input::get('type');
		$projectId = Input::get('projectId');
		$exsistingFileId = Input::get('exsistingFileId');
		$fileType = Input::get('fileType');
		$supplierId = Input::get('supplierId');
		$contractId = Input::get('contractId');
		$projectFileType = Input::get('projectFileType');
        $i = Input::get('i');

		if(!empty($filename)) {
			return $storeWrapper->implement(ClassMap::instance(Config::get('classmap.store-file.'.$fileType)), compact("filename", "filesize", "type", "projectId", "exsistingFileId", "fileType", "projectFileType", "supplierId","contractId", "i"));
		}
		return NULL;
	}

	/**
	 * Store proposal file (this function is being used for proposal files - the function is accessed with an ajax route)
	 * @param FileStoreWrapper $storeWrapper
	 * @return null
	 * @throws \Exception
	 */
	public function storeProposalFile(FileStoreWrapper $storeWrapper)
	{
		$filename = Input::get('filename');
		$filesize = Input::get('filesize');
		$type = Input::get('type');
		$projectId = Input::get('projectId');
		$exsistingFileId = Input::get('exsistingFileId');
		$fileType = Input::get('fileType');
		$proposalItemNo = Input::get('proposal_item_no');
		$projectFileType = Input::get('projectFileType');

		if(!empty($filename)) {
			return $storeWrapper->implement(ClassMap::instance(Config::get('classmap.store-file.'.$fileType)), compact("filename", "filesize", "type", "projectId", "exsistingFileId", "fileType", "projectFileType", "proposalItemNo"));
		}
		return NULL;
	}

	/**
	 * Share selected rfi's with all subcontractors
	 * @param RfisRepository $rfisRepository
	 * @return mixed
     */
	public function shareAllRfis(RfisRepository $rfisRepository)
	{
		$selectedFiles = Input::get('selected_files');

		//get project id
		$projectId = Input::get('project_id');

		//share selected files
		$sharedFiles = $rfisRepository->shareWithAllSubcontractors($projectId, $selectedFiles);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Share selected rfi files with selected subcontractors
	 * (including general contractor as an option for selection)
	 * @param RfisRepository $rfisRepository
	 * @return mixed
	 * @internal param ProjectFilesRepository $filesRepository
	 */
	public function shareSelectedRfis(RfisRepository $rfisRepository)
	{
		$selectedFiles = Input::get('selected_files');
		$selectedSubcontractors = Input::get('selected_subcontractors');
		$sharedFiles = $rfisRepository->shareWithSelectedSubcontractors($selectedFiles, $selectedSubcontractors);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Share selected rfi's with all subcontractors
	 * @param PcosRepository $pcosRepository
	 * @return mixed
	 */
	public function shareAllPcos(PcosRepository $pcosRepository)
	{
		$selectedFiles = Input::get('selected_files');

		//get project id
		$projectId = Input::get('project_id');

		//share selected files
		$sharedFiles = $pcosRepository->shareWithAllSubcontractors($projectId, $selectedFiles);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Share selected rfi files with selected subcontractors
	 * (including general contractor as an option for selection)
	 * @param PcosRepository $pcosRepository
	 * @return mixed
	 * @internal param ProjectFilesRepository $filesRepository
	 */
	public function shareSelectedPcos(PcosRepository $pcosRepository)
	{
		$selectedFiles = Input::get('selected_files');
		$selectedSubcontractors = Input::get('selected_subcontractors');
		$sharedFiles = $pcosRepository->shareWithSelectedSubcontractors($selectedFiles, $selectedSubcontractors);
		if($sharedFiles) {
			return Response::json('success', 200);
		}
		return Response::json('error', 400);
	}

	/**
	 * Check logged in company download allowance
	 * @param ProjectFilesRepository $filesRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return mixed
     */
	public function downloadAllowance(ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
	{
		$fileId = Input::get('fileId');
        $fileType = Input::get('fileType');
        if ($fileType == 'address-book') {
            $file = $filesRepository->getAddressBookFileById($fileId);
        } else if ($fileType == 'tasks') {
            $file = $filesRepository->getTasksFileById($fileId);
        } else {
            $file = $filesRepository->getProjectFileById($fileId);
        }

		if ($file) {
            if ($fileType == 'address-book' || $fileType == 'tasks') {
                $company = $this->companiesRepository->getCompany(Auth::user()->comp_id);

                //check upload file allowance
                if ($transferLimitation->calculateExpectedTransfer($company->downloaded, $file->size,
                    $company->subscription_type->download_limit, $company)) {
                    return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));
                }
            } else {
                //check upload file allowance
                if ($transferLimitation->checkDownloadTransferAllowance($file->size, $file->proj_id)) {
                    return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));
                }
            }

		}
		return Response::json(array('allowed' => 0, 200));
	}

	/**
	 * Store module file before is uploaded on S3 and return new file custom name and full upload path
	 * this function handles the rfi's, submittals and pco's file uploads
	 * @param ProjectFilesRepository $filesRepository
	 * @return null
	 * @internal param SubmittalsRepository $submittalsRepository
	 */
	public function storeModuleFile(ProjectFilesRepository $filesRepository)
	{
		$filename = Input::get('filename');
		$filesize = Input::get('filesize');
		$type = Input::get('type');
		$projectId = Input::get('projectId');
		$module = Input::get('module');
		$shared = false;
		if ($module == 'shared') {
		    $module = Input::get('module1');
            $shared = true;
        }
		$exsistingFileId = Input::get('exsistingFileId');
		$versionDate = Input::get('versionDate');
        $version = Input::get('version');

		if(!empty($filename) && !$shared) {
			return $filesRepository->storeFile($filename, $filesize, $type, $projectId, $exsistingFileId, $versionDate, $module);
		} else if (!empty($filename) && $shared) {
            return $filesRepository->storeFileShared($filename, $filesize, $type, $projectId, $exsistingFileId, $versionDate, $module, $version);
        }
	}

	/**
	 * Delete specific module file
	 * @param FileDeleteWrapper $deleteWrapper
	 * @return null
	 * @throws \Exception
     */
	public function deleteFile(FileDeleteWrapper $deleteWrapper)
	{
		$fileId = Input::get('fileId');
		$fileType = Input::get('fileType');

		if(!empty($fileId)) {
			$response = $deleteWrapper->getDeleteService(ClassMap::instance(Config::get('classmap.delete-file.'.$fileType)), compact("fileType", "fileId"));
			if ($response) {
				return Response::json(['success' => $response, 200]);
			}
			return Response::json(['error' => $response, 400]);
		}

		return Response::json(['error' => 'No file id', 400]);
	}

	/**
	 * Send notification email
	 * @param MailRepository $mailRepository
	 * @param NotificationsWrapper $wrapper
	 * @param ProjectFilesRepository $projectFilesRepository
	 * @return mixed
	 * @throws \Exception
	 */
	public function sendEmail(MailRepository $mailRepository, NotificationsWrapper $wrapper,
                              ProjectFilesRepository $projectFilesRepository)
	{
	    $usersString = Input::get('users');
        $contactsString = Input::get('contacts');
		$type = Input::get('type');
		$projectFileType = Input::get('projectFileType');
		$projectId = Input::get('projectId');
		$fileId = Input::get('fileId');
		$filePath = Input::get('filePath');

		$userIds = empty($usersString)?[]:explode('|', $usersString);
        $contactIds =  empty($contactsString)?[]:explode('|', $contactsString);

		$emails = $wrapper->wrapCompanyEmails(ClassMap::instance(Config::get('classmap.email-notifications.'.$type)),
            compact('fileId', 'projectId', 'projectFileType', 'filePath', 'userIds', 'contactIds'));

        $userEmails = $wrapper->wrapUserEmails(ClassMap::instance(Config::get('classmap.email-notifications.'.$type)),
            compact('fileId', 'projectId', 'projectFileType', 'filePath', 'userIds', 'contactIds'));

        if (count($emails) > 0 || count($userEmails) > 0) {
			$projectFilesRepository->updatedEmailedFile($fileId, Config::get('constants.transmittal_emailed.yes'));
            $projectFile = $projectFilesRepository->getProjectFile($projectId, $fileId);
            $fileName = $projectFile->name;
            $projectName = $projectFile->project->name;
            $projectId = $projectFile->project->id;

            $sentTo = [];
            foreach ($emails as $email) {
                $sentTo[] = $email->name.' - '.$email->companyName.' ('.Carbon::now()->format('m/d/Y').')';
            }

            foreach ($userEmails as $userEmail) {
                $sentTo[] = $userEmail->name.' - '.$userEmail->companyName.' ('.Carbon::now()->format('m/d/Y').')';
            }

			foreach ($emails as $email) {
				$mailRepository->sendProjectFileNotification(compact('email','filePath','fileId','fileName','projectName', 'projectId', 'sentTo'));
			}

            foreach ($userEmails as $email) {
                $mailRepository->sendProjectFileNotification(compact('email','filePath','fileId','fileName','projectName', 'projectId', 'sentTo'));
            }

            $data['employees_users_in'] = $userIds;
            $data['employees_contacts_in'] =$contactIds;

            $projectFilesRepository->markFileSentTo($projectFile->id, $data);

			Flash::success(trans('messages.transmittals.notification_success'));
			return Response::json(['status' => 'success', 'message' => trans('labels.transmittals.notification_success'), 200]);
		}

		return Response::json(['status' => 'error', 'message' => trans('labels.files.project_files_notification_error'), 400]);
	}

	public function showProjectFile($projectId, ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
	{
		$fileId = Input::get('fileId');
		$filePath = Input::get('filePath');
        $filePath = urldecode($filePath);
        $filePath = str_replace('#', '%23', $filePath);
		$companyId = Input::get('companyId');

		//check download limit
        try {
            $file = $filesRepository->getProjectFileById($fileId);
        } catch (ModelNotFoundException $e) {
            $code = 404;
            $errorMessageLarge = "The file was replaced or deleted by the sender.";
            $errorMessageSmall = "Please look for another email or contact the sender to obtain the correct file.";
            return redirect()->action('ErrorController@showErrorPagePublic', compact('code',
                'errorMessageLarge', 'errorMessageSmall'));
        }

        $exists = Storage::disk('s3')->exists(urldecode($filePath));
        if (!$exists) {
            $code = 404;
            $errorMessageLarge = "The file was replaced or deleted by the sender.";
            $errorMessageSmall = "Please look for another email or contact the sender to obtain the correct file.";
            return redirect()->action('ErrorController@showErrorPagePublic', compact('code',
                'errorMessageLarge', 'errorMessageSmall'));
        }
        
		if ($file) {
            //check upload file allowance
            if ($transferLimitation->checkParentDownloadTransferAllowance($file->size, $file->proj_id)) {
                //return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));

                //increase download limit
                if (!empty($file->size)) {
                    //increase company upload transfer
                    $transferLimitation->increaseDownloadTransfer($file->size);
                }

                //show transmittal file
                return Redirect::to(env('AWS_CLOUD_FRONT')."/".$filePath);
                //$mime = Storage::disk('s3')->mimeType($filePath);
                //$file = Storage::disk('s3')->get($filePath);
                //return Response::make($file, 200)->header('Content-type',$mime,'Content-Disposition', 'attachment');
            } else {
                die(trans('messages.data_transfer_limitation.download_error', ['item' => 'Project']));
            }
		} else {
			Flash::error(trans('messages.transmittals.not_exist'));
		}

		return view('projects.errors.get_transmittal');
	}

    public function showBatchFile($projectId, $companyId, FilesRepository $filesRepository, DataTransferLimitation $dataTransferLimitation)
    {
       if(file_exists( public_path() . '/batch/batch_project_' . $projectId . '.zip')) {
           $usedStorageByProject = $filesRepository->getUsedStorageByProject($projectId);
           if ($dataTransferLimitation->checkDownloadTransferAllowance($usedStorageByProject, $projectId)) {
               $dataTransferLimitation->increaseDownloadTransfer($usedStorageByProject, $companyId);

               return Redirect::to(url('/batch/batch_project_' . $projectId . '.zip'));
           } else {
               Flash::error(trans('messages.projects.download-limit-exceeded'));
               return Redirect::to(url('projects/'.$projectId));
           }
       } else {
            die('file not ready');
       }
    }

    /**
     * Store address book file
     * @param FileStoreWrapper $storeWrapper
     * @return null
     * @throws \Exception
     */
    public function storeAddressBookFile(FileStoreWrapper $storeWrapper)
    {
        $filename = Input::get('filename');
        $filesize = Input::get('filesize');
        $type = Input::get('type');
        $contactId = Input::get('contactId');
        $exsistingFileId = Input::get('exsistingFileId');
        $fileType = Input::get('fileType');

        if(!empty($filename)) {
            return $storeWrapper->implement(ClassMap::instance(Config::get('classmap.store-file.'.$fileType)), compact("filename", "filesize", "type", "exsistingFileId", "fileType", "contactId"));
        }
        return NULL;
    }
}
