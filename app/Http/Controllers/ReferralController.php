<?php
namespace App\Http\Controllers;


use App\Http\Requests\ReferralRequest;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;

use App\Services\Mailer;
use Request;
use Input;
use Flash;
use Session;
use Auth;
use Response;

class ReferralController extends Controller
{
    private $referralsRepository;
    private $companiesRepository;

    public function __construct(ReferralsRepository $referralsRepository, CompaniesRepository $companiesRepository)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        $this->referralsRepository = $referralsRepository;
        $this->companiesRepository = $companiesRepository;
    }


    /**
     * load view for inserting email
     * @return \Illuminate\View\View
     */
    public function invite()
    {
        return view('company_profile.add-referral');

    }

    /**
     * send invite on email that is specified in invite form
     * @param ReferralRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function send(ReferralRequest $request, Mailer $mailer)
    {
        $input = $request->all();

        if ($this->referralsRepository->emailExists($input['email'])) {
            Flash::error(trans('messages.referral.insert.exists', ['email' => $input['email']]));
            return redirect('referral/invite');
        }
        //insert referral in db, returns token that we use in email
        $token = $this->referralsRepository->insertReferral($input);
        $input['token'] = $token;

        if ($token) {
            //if db record inserted, send email
            $invite = $mailer->sendReferral($input);
        }

        //show appropriate message
        if ($invite) {
            Flash::success(trans('messages.referral.insert.success', ['email' => $input['email']]));
        } else {
            Flash::error(trans('messages.referral.insert.error'));
        }

        return redirect('referral/invite');
    }

}