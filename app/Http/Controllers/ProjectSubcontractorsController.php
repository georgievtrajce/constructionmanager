<?php
namespace App\Http\Controllers;
use App\Http\Requests\ProjectsRequest;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;

use Illuminate\Support\Facades\Auth;
use Request;
use Input;
use Flash;
use Session;
use Config;

class ProjectSubcontractorsController extends Controller {

    private $projectSubcontractorsRepo;
    private $projectPermissionsRepo;
    private $projectRepo;

    public function __construct(ProjectSubcontractorsRepository $projectSubcontractorsRepo, ProjectRepository $projectRepo, ProjectPermissionsRepository $projectPermissionsRepo)
    {
        $this->middleware('auth');
        $this->projectSubcontractorsRepo = $projectSubcontractorsRepo;
        $this->projectRepo = $projectRepo;
        $this->projectPermissionsRepo = $projectPermissionsRepo;
    }

    public function updateAddress()
    {
        //update location address
        $data = Input::all();
        $this->projectSubcontractorsRepo->updateAddress($data);
    }

    /**
     * accept project by token
     * @param $token
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptProject($token, ProjectPermissionsRepository $projectPermissionsRepo)
    {
        //get project company id
        $projectCompanyId = Input::get('projectCompanyId');

        $this->projectSubcontractorsRepo->updateSubcontractorStatus($token, Config::get('constants.projects.shared_status.accepted'));
        $projectSubcontractor = $this->projectSubcontractorsRepo->getSubcontractorByToken($token);

        if (is_null($projectSubcontractor)) {
            Flash::error(trans('messages.projects.already_unshared'));
            return redirect('projects');
        }

        $this->projectSubcontractorsRepo->addProjectInfoToSubcontractor(['projectCompanyId' => $projectCompanyId]);

        //inital insert company permissions for that project as no permissions for anything, to child and parent
        //$projectPermissionsRepo->insertProjectCompanyPermissions($projectSubcontractor->proj_id, $projectSubcontractor->comp_parent_id, $projectSubcontractor->comp_child_id);

        //update child company (subcontractor) in project companies permissions
        $projectPermissionsRepo->updatePermissionsSubcontractor($projectCompanyId, $projectSubcontractor->proj_id, $projectSubcontractor->comp_parent_id, Auth::user()->comp_id);

        return redirect('projects');
    }

    /**
     * reject project by token
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function rejectProject($token)
    {
        $this->projectSubcontractorsRepo->updateSubcontractorStatus($token, Config::get('constants.projects.shared_status.rejected'));
        return redirect('projects');
    }
}