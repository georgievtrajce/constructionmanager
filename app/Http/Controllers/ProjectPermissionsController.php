<?php
namespace App\Http\Controllers;

use App\Http\Requests\ProjectPermissionRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use App\Requests;
use App\Models\User;
use App\Utilities\FileStorage;

use Request;
use Input;
use Flash;
use Auth;
use Config;

class ProjectPermissionsController extends Controller
{
    private $permissionsRepo;
    private $projectRepo;
    private $manageUsersRepository;
    private $companyRepo;
    /**
     * @var MailRepository
     */
    private $mailRepository;
    /**
     * @var ProjectFilesRepository
     */
    private $projectFilesRepository;

    public function __construct(ProjectPermissionsRepository $permissionsRepo, ManageUsersRepository $manageUsersRepository,
                                ProjectRepository $projectRepo, CompaniesRepository $companyRepo,
                                MailRepository $mailRepository, ProjectFilesRepository $projectFilesRepository)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        $this->middleware('sub_module_write', ['only' => ['companyPermissions','updateCompanyPermissions']]);
        $this->middleware('only_company_admin', ['except' => ['companyPermissions','updateCompanyPermissions']]);
        //$this->middleware('project_admin_restrictions', ['only' => ['edit','update','companyPermissions','updateCompanyPermissions']]);
        $this->permissionsRepo = $permissionsRepo;
        $this->manageUsersRepository = $manageUsersRepository;
        $this->projectRepo = $projectRepo;
        $this->companyRepo = $companyRepo;
        $this->mailRepository = $mailRepository;
        $this->projectFilesRepository = $projectFilesRepository;
    }

    /**
     * list all users in the company for project
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function index($projectID)
    {
        $project = $this->projectRepo->getProject($projectID);
        $users = $this->manageUsersRepository->getCompanyUsers(Auth::user()->comp_id, 300);
        $addresses = $this->manageUsersRepository->getCompanyAddresses(Auth::user()->comp_id);
        $selectedUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectID);

        $offices = [];
        foreach ($users as $user) {
            $offices[$user->address_id][] = $user;
            $user->is_selected = false;
            foreach ($selectedUsers as $selectedUser){
                if($user->id == $selectedUser->user_id) {
                    $user->is_selected = true;
                }
            }
        }

        return view('projects.permissions.index', compact('project', 'users', 'addresses', 'offices'));

    }

    /**
     * @param $projectID
     * @param ProjectPermissionRequest $request
     * @param UsersRepository $usersRepository
     * @return mixed
     */
    public function store($projectID, ProjectPermissionRequest $request, UsersRepository $usersRepository)
    {
        $requests = $request->only('user_ids');
        $officeId = $request->get('permissions_user_address_office');

        $selectedUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectID);
        $selectedDbUsers = [];
        foreach ($selectedUsers as $selectedUser) {
            $selectedDbUsers[] = $selectedUser->user_id;
        }

        if(count($requests['user_ids']) > 0)
        {
            //delete previous users
            $deleteDiffUserIds = array_diff($selectedDbUsers, $requests['user_ids']);
            if (count($deleteDiffUserIds) > 0) {
                foreach ($deleteDiffUserIds as $userId) {
                    $this->manageUsersRepository->deleteProjectPermissionUser($projectID, $userId);
                }
            }

            //insert new users
            $insertDiffUserIds = array_diff($requests['user_ids'], $selectedDbUsers);
            if (count($insertDiffUserIds) > 0) {
                foreach ($insertDiffUserIds as $userId) {
                    $this->manageUsersRepository->storeProjectPermissionUser($projectID, $userId);
                }
            }

            Flash::success(trans('messages.users.permissions.success'));
            return redirect()->to('projects/' . $projectID . '/permissions?officeId='.$officeId);
        } else {
            //if all of them are deselected
            if (count($selectedDbUsers) > 0) {
                foreach ($selectedDbUsers as $userId) {
                    $this->manageUsersRepository->deleteProjectPermissionUser($projectID, $userId);
                }

                Flash::success(trans('messages.users.permissions.success'));
                return redirect()->to('projects/' . $projectID . '/permissions?officeId='.$officeId);
            }
        }

        Flash::error(trans('messages.users.store.error'));
        return redirect()->to('projects/'.$projectID.'/permissions?officeId='.$officeId);
    }

    /**
     * list all permissions with checkboxes (checked and unchecked) for user for project
     * @param $projectID
     * @param $userID
     * @param UsersRepository $usersRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($projectID, $userID, UsersRepository $usersRepository)
    {
       $user = $usersRepository->getUserByID($userID);

        if ($user->hasRole('Company Admin')) {
            Flash::info(trans('messages.users.permissions.admin'));
            return redirect()->action('ProjectPermissionsController@index');
        }

        $project = $this->projectRepo->getProject($projectID);
        $user = $this->manageUsersRepository->getUser($userID);
        $permissions = $this->permissionsRepo->getUserPermissionsByProject($projectID, $userID);
        $batchPermissions = $this->permissionsRepo->getUserPermissionBatchDownloadByProject($projectID, $userID);

        return view('projects.permissions.partials.edit', compact('project', 'user', 'permissions', 'batchPermissions'));
    }

    /**
     * update permissions for user
     * @param $projectID
     * @param $userID
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectID, $userID)
    {
        $modules = Input::get('module_id');

        //entity type 1 = Modules
        $entityType = Config::get('constants.entity_type.module');

        $showCostPermission = null;

        //update user permissions for all modules
        for ($i=0; $i<sizeof($modules); $i++) {
            $readPermission = !is_null(Input::get('module_read.'.$i)) ? 1 : 0;
            $writePermission = !is_null(Input::get('module_write.'.$i)) ? 1 : 0;
            $deletePermission = !is_null(Input::get('module_delete.'.$i)) ? 1 : 0;
            $showCostPermission = !is_null(Input::get('module_show_cost.'.$i)) ? 1 : 0;
            $this->permissionsRepo->updateUserProjectPermissions($userID, $modules[$i], $readPermission, $writePermission, $deletePermission, $showCostPermission, $entityType, $projectID);
        }

        //batch permission
        $batchReadPermission = !is_null(Input::get('batch_read')) ? 1 : 0;
        $this->permissionsRepo->updateUserPermissionBatchDownloadByProject($projectID, $userID, $batchReadPermission, 0, 0);

        //get all project files ids
        $projectFiles = Input::get('project_file_id');

        //entity type 2 = Project Files
        $entityType = Config::get('constants.entity_type.file');

        //update user permissions for all project files
        for ($i=0; $i<sizeof($projectFiles); $i++) {
            $readPermission = !is_null(Input::get('project_file_read.'.$i)) ? 1 : 0;
            $writePermission = !is_null(Input::get('project_file_write.'.$i)) ? 1 : 0;
            $deletePermission = !is_null(Input::get('project_file_delete.'.$i)) ? 1 : 0;
            $this->permissionsRepo->updateUserProjectPermissions($userID, $projectFiles[$i], $readPermission, $writePermission, $deletePermission, $showCostPermission, $entityType, $projectID);
        }

        //Error message if the user wasn't created
        Flash::success(trans('messages.users.permissions.success'));
        return redirect('projects/'.$projectID.'/permissions/');
    }

    /**
     * list company permissions for project
     * @param $projectID
     * @param $projectCompanyID
     * @param ProjectCompaniesRepository $projectCompaniesRepository
     * @return \Illuminate\View\View
     * @internal param $companyID
     */
    public function companyPermissions($projectID, $projectCompanyID, ProjectCompaniesRepository $projectCompaniesRepository)
    {
        $project = $this->projectRepo->getProject($projectID);
        $company = $projectCompaniesRepository->getProjectCompany($project->id, $projectCompanyID);
        $permissions = $this->permissionsRepo->getCompanyPermissionsByProject($projectID, $projectCompanyID);
        return view('projects.companies.permissions', compact('project', 'permissions', 'company'));
    }

    /**
     * update company permissions for project
     * @param $projectID
     * @param $projectCompanyID
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $companyID
     */
    public function updateCompanyPermissions($projectID, $projectCompanyID, ProjectCompaniesRepository $projectCompaniesRepository)
    {
        $company = $projectCompaniesRepository->getProjectCompany($projectID, $projectCompanyID);
        $project = $this->projectRepo->getProjectRecord($projectID);

        $moduleNamesRecipientOld = $this->permissionsRepo->getCompanyPermissionsWriteModuleNames($projectID, $company->ab_id);

        $moduleNamesRecipientValuesOld = [];
        foreach ($moduleNamesRecipientOld as $item) {
            $moduleNamesRecipientValuesOld[] = $item->name;
        }

        //$modules = Input::get('module_id');
        $modulesRead = Input::get('module_read');
        $modulesWrite = Input::get('module_write');
        $pcoPermissionType = Input::get('pco_permission_type');

        //entity type 1 = modules
        $module = Config::get('constants.entity_type.module');

        //entity type 2 = Project Files
        $file = Config::get('constants.entity_type.file');

        //get all project files ids
        $projectFilesRead = Input::get('project_file_read');

        //get all module permissions
        $allModulePermissions = $this->permissionsRepo->getAllCompanyPermissionsByEntity($projectCompanyID, $module, $projectID);

        //update modules permissions
        if (is_null($modulesRead) && is_null($modulesWrite)) {
            //set all to 0
            foreach ($allModulePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);
                $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType, 0);
            }
        } else if (is_null($modulesWrite)) {
            $modulesArray = array_values($modulesRead);
            foreach ($allModulePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);
                if (in_array($singlePermission->entity_id, $modulesArray)) {
                    //set
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 1, $pcoPermissionType);
                } else {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType);
                }
            }
        } else if (is_null($modulesRead)) {
            $modulesArray = array_values($modulesWrite);
            foreach ($allModulePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);
                if (in_array($singlePermission->entity_id, $modulesArray)) {
                    //set
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType, 1);
                } else {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType, 0);
                }
            }
        } else {
            $modulesArray = array_values($modulesRead);
            $modulesWriteArray = array_values($modulesWrite);
            foreach ($allModulePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);

                if (in_array($singlePermission->entity_id, $modulesArray) && in_array($singlePermission->entity_id, $modulesWriteArray)) {
                    //set
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 1, $pcoPermissionType, 1);
                } else if (in_array($singlePermission->entity_id, $modulesArray)) {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 1, $pcoPermissionType);
                } else {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType);
                }
            }
        }

        //get project files permissions
        $allFilePermissions = $this->permissionsRepo->getAllCompanyPermissionsByEntity($projectCompanyID, $file, $projectID);

        //update project files permissions
        if (is_null($projectFilesRead)) {
            //set all to 0
            foreach ($allFilePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);
                $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType);
            }
        } else {
            $filesArray = array_values($projectFilesRead);
            foreach ($allFilePermissions as $singlePermission) {
                $syncedCompId = $this->getSyncedCompanyId($singlePermission);
                if (in_array($singlePermission->entity_id, $filesArray)) {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 1, $pcoPermissionType);
                } else {
                    $this->permissionsRepo->updateProjectCompanyPermission($syncedCompId, $singlePermission->id, 0, $pcoPermissionType);
                }
            }
        }

        if (!empty($company->ab_id)) {
            //send notification to default transmittal recipient
            $moduleNamesRecipient = $this->permissionsRepo->getCompanyPermissionsWriteModuleNames($projectID, $company->ab_id);

            $moduleNamesRecipientValues = [];
            foreach ($moduleNamesRecipient as $item) {
                $moduleNamesRecipientValues[] = $item->name;
            }

            if (!empty($moduleNamesRecipientValues) && !(count($moduleNamesRecipientValues) == count($moduleNamesRecipientValuesOld) && !array_diff($moduleNamesRecipientValues, $moduleNamesRecipientValuesOld))) {

                $companyContactRecipients = [];
                foreach ($company->project_company_contacts as $contact) {
                    $companyContactRecipients[] = $contact->ab_cont_id;
                }

                $subcontractors = $this->projectFilesRepository->getContractByIdArray($companyContactRecipients);
                $data = [
                    'projectName' => $project->name,
                    'moduleNames' => $moduleNamesRecipientValues,
                    'contacts' => $subcontractors
                ];

                $this->mailRepository->sendPermissionWriteRecipientNotification($data);
            }
        }

        //to be added true - false for updated permissions and error message if false
        Flash::success(trans('messages.users.permissions.success'));

        return redirect(url('projects/'.$projectID.'/companies/'.$company->ab_id.'/project-company/'.$projectCompanyID.'/office/'.$company->addr_id));
    }

    private function getSyncedCompanyId($singlePermission) {
        if (!empty($singlePermission->proj_comp_id)) {
            $projectCompany = \App\Models\Project_company::where('id', '=', $singlePermission->proj_comp_id)->first();

            if ($projectCompany) {
                $addressBookCompany = \App\Models\Address_book::where('id', '=', $projectCompany->ab_id)->first();
                if ($addressBookCompany && !empty($addressBookCompany->synced_comp_id)) {
                    return $addressBookCompany->synced_comp_id;
                }
            }
        }
        return 0;
    }

}