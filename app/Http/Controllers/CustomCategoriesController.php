<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateCustomCategoryRequest;
use App\Modules\Custom_categories\Repositories\CustomCategoriesRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class CustomCategoriesController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
		$this->middleware('module_write', ['only' => ['create','store','show','update']]);
		$this->middleware('module_delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param CustomCategoriesRepository $customCategories
	 * @return Response
	 */
	public function index(CustomCategoriesRepository $customCategories)
	{
		$data['customCategories'] = $customCategories->getAllCategories();
		return view('custom_categories.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('custom_categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateCustomCategoryRequest $request
	 * @param CustomCategoriesRepository $customCategories
	 * @return Response
	 */
	public function store(CreateCustomCategoryRequest $request, CustomCategoriesRepository $customCategories)
	{
		$returnCategory = $customCategories->storeCategory($request->all());

		if ($returnCategory) {
			Flash::success(trans('messages.addressBook.store.success', ['item' => 'Category']));
			return Redirect::to('custom-categories');
		}

		Flash::error(trans('messages.addressBook.store.error', ['item' => 'Category']));
		return Redirect::to('custom-categories');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @param CustomCategoriesRepository $customCategories
	 * @return Response
	 */
	public function edit($id, CustomCategoriesRepository $customCategories)
	{
		$data['category'] = $customCategories->getCategory($id);
		return view('custom_categories.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param CustomCategoriesRepository $customCategories
	 * @param CreateCustomCategoryRequest $request
	 * @return Response
	 */
	public function update($id, CustomCategoriesRepository $customCategories, CreateCustomCategoryRequest $request)
	{
		$customCategories->updateCategory($id, $request->all());

		if ($customCategories) {
			Flash::success(trans('messages.addressBook.update.success', ['item' => 'Category']));
			return Redirect::to('custom-categories');
		}

		Flash::error(trans('messages.addressBook.update.error', ['item' => 'Category']));
		return Redirect::to('custom-categories');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @param CustomCategoriesRepository $customCategories
	 * @return Response
	 */
	public function destroy($id, CustomCategoriesRepository $customCategories)
	{
        //split contacts if multiple
        $idsArray = explode('-', $id);

        $errorCheck = false;
        foreach ($idsArray as $itemId) {
            //check dependency
            $checkCategory = $customCategories->checkCategoryDependency($itemId);

            if (!empty($checkCategory['address_book_entries'])) {
                $errorCheck = true;
            } else {
                $customCategories->deleteCategory($itemId);
            }
        }

        if ($errorCheck) {
            //If the category is connected to some company from the company address book, the category can not be deleted
            Flash::info(trans('messages.customCategories.delete.info'));
            return Redirect::to('custom-categories');
        }

        Flash::success(trans('messages.addressBook.delete.success', ['item' => 'Category']));
        return Redirect::to('custom-categories');
	}

}
