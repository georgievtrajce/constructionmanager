<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Transactions\Repositories\TransactionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TransactionsController extends Controller {

    /**
     * Listing page for the transmittals (super admin page)
     * @param TransactionsRepository $transactionsRepository
     * @return \Illuminate\View\View
     */
    public function index(TransactionsRepository $transactionsRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort','transactions.id');
        $order = Input::get('order','desc');
        $data['transactions'] = $transactionsRepository->getAllTransactions($sort, $order);

        return view('transactions.index', $data);
    }

}
