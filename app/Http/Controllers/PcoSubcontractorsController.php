<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PcoSubcontractorsRequest;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Files\Implementations\CheckUploadedFiles;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class PcoSubcontractorsController extends Controller {

    const ENTITY_ID = 10;

	public function __construct()
	{
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 */
	public function create($projectId, $pcoId, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
	{
		//get the proper project
		$data['project'] = $projectRepository->getProject($projectId);

		//get the proper pco
		$data['pco'] = $pcosRepository->getPco($pcoId);

		return view('projects.pcos.create-subcontractor', $data);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param PcoSubcontractorsRequest $request
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @return Response
     */
	public function store($projectId, $pcoId, PcoSubcontractorsRequest $request, PcosRepository $pcosRepository,
                          ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                          ProjectPermissionsRepository $projectPermissionsRepo)
	{
        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);

        //Update submittal
        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor)) ? $projectCompanyPermissionsSubcontractor->write : 0;

		//Store pco subcontractor
		$response = $pcosRepository->storePcoSubcontractor($pcoId, $dataInput);

		if ($response) {

            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.pcos'),
                'moduleId' => $pcoId,
                'name' => $dataInput['name']
            ];

            if (!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully created pco subcontractor
			Flash::success(trans('messages.submittals.store.success', ['item' => 'PCO Subcontractor']));
			//return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit');
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/subcontractors/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'PCO Subcontractor']));
		return Redirect::to('projects/'.$projectId.'/pcos'.$pcoId.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $pcoId
     * @param $pcoSubcontractorId
     * @param PcosRepository $pcosRepository
     * @param ProjectRepository $projectRepository
     * @param ContactsRepository $contactsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return Response
     * @internal param int $id
     */
	public function edit($projectId, $pcoId, $pcoSubcontractorId, PcosRepository $pcosRepository, ProjectRepository $projectRepository, ContactsRepository $contactsRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		//get the proper project
		$data['project'] = $projectRepository->getProject($projectId);

		//Store pco subcontractor
		$data['pcoSubcontractor'] = $pcosRepository->getPcoSubcontractor($pcoId, $pcoSubcontractorId);
        $data['pcoSubcontractor'] = $checkUploadedFiles->checkAndOrderUploadedFiles($data['pcoSubcontractor'], Config::get('constants.subcontractor_versions'));
		$data['projectSubcontractorContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['pcoSubcontractor']->ab_subcontractor_id);

		return view('projects.pcos.edit-subcontractor', $data);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param $pcoSubcontractorId
     * @param PcoSubcontractorsRequest $request
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return Response
     * @internal param int $id
     */
	public function update($projectId, $pcoId, $pcoSubcontractorId, PcoSubcontractorsRequest $request, PcosRepository $pcosRepository,
                           ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                           ProjectPermissionsRepository $projectPermissionsRepo)
	{
        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);

        //Update pco subcontractor
        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor)) ? $projectCompanyPermissionsSubcontractor->write : 0;
        $pco = $pcosRepository->updatePcoSubcontractor($pcoId, $pcoSubcontractorId, $dataInput);

		if ($pco) {

            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.pcos'),
                'moduleId' => $pcoId,
                'name' => $dataInput['name']
            ];

            if (!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully updated pco
			Flash::success(trans('messages.submittals.update.success', ['item' => 'PCO Subcontractor']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/subcontractors/'.$pcoSubcontractorId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'PCO Subcontractor']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit/');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param $pcoSubcontractorId
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy($projectId, $pcoId, $pcoSubcontractorId, PcosRepository $pcosRepository)
	{
        $idsArray = explode('-', $pcoSubcontractorId);

        $errorEntry = false;
        foreach ($idsArray as $id) {

            //select pco versions for file deleting
            $pcoSubcontractor = $pcosRepository->getPcoSubcontractor($pcoId, $id);

            //delete pco version files from s3
            if (count($pcoSubcontractor->subcontractor_versions)) {
                foreach ($pcoSubcontractor->subcontractor_versions as $pcoVersion) {
                    foreach ($pcoVersion->files as $versionFile) {
                        //delete pco version files
                        $pcosRepository->deletePcoFile($projectId, $versionFile->file->file_name);
                    }
                }
            }

            //delete pcos and its versions from database
            $response = $pcosRepository->deletePcoSubcontractor($id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

		//Successfully deleted pco
		if (!$errorEntry) {
			Flash::success(trans('messages.submittals.delete.success', ['item' => 'PCO subcontractor']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit/');
		}

		//Not successful deleting
		Flash::error(trans('messages.submittals.delete.error', ['item' => 'PCO subcontractor']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit/');
	}

}
