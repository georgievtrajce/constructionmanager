<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ManageCompanyUsersRequest;
use App\Http\Requests\UpdateCompanyUserRequest;
use App\Modules\Manage_users\Implementations\StoreCompanyUserWrapper;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class ManageUsersController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');

		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		//$this->middleware('level_zero_permit');

		$this->middleware('company_admin');
		$this->middleware('company_admin_restrictions', ['only' => ['getPermissions','postPermissions']]);
	}

	/**
	 * Display a listing of users.
	 *
	 * @param ManageUsersRepository $manageUsersRepository
	 * @return Response
	 */
	public function index(ManageUsersRepository $manageUsersRepository)
	{
		$data['companyUsers'] = $manageUsersRepository->getCompanyUsers(Auth::user()->comp_id, 300);
		return view('manage_users.index', $data);
	}

	/**
	 * Get specified users permissions for listing
	 * @param $userId
	 * @param ManageUsersRepository $manageUsersRepository
	 * @return \Illuminate\View\View
     */
	public function getPermissions($userId, ManageUsersRepository $manageUsersRepository)
	{
		//get company user
		$data['user'] = $manageUsersRepository->getCompanyUser($userId);

		//get all modules
		$data['modules'] = $manageUsersRepository->getAllModules();

		//get all project file types
		$data['projectFiles'] = $manageUsersRepository->getProjectFileTypes();

		return view('manage_users.permissions', $data);
	}

    /**
     * Give or remove a permission for a specified user
     * @param $userId
     * @param ManageUsersRepository $manageUsersRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepository
     * @return mixed
     */
	public function postPermissions($userId, ManageUsersRepository $manageUsersRepository, ProjectPermissionsRepository $projectPermissionsRepository)
	{
		//get all modules ids
		$modules = Input::get('module_id');

		//entity type 1 = Modules
		$entityType = 1;

		//update user permissions for all modules
		for ($i=0; $i<sizeof($modules); $i++) {
			$readPermission = !is_null(Input::get('module_read.'.$i)) ? 1 : 0;
			$writePermission = !is_null(Input::get('module_write.'.$i)) ? 1 : 0;
			$deletePermission = !is_null(Input::get('module_delete.'.$i)) ? 1 : 0;
			$manageUsersRepository->updateUserPermissions($userId, $modules[$i], $readPermission, $writePermission, $deletePermission, $entityType);

			if ($modules[$i] == 13) {
			    //change permissions to all existing projects
                $projectPermissionsRepository->updateUserPermissionsForAllProjects($userId, $readPermission, $writePermission, $deletePermission, [1, 2]);
            }
		}

		//get all project files ids
		$projectFiles = Input::get('project_file_id');

		//entity type 2 = Project Files
		$entityType = 2;

		//update user permissions for all project files
		for ($i=0; $i<sizeof($projectFiles); $i++) {
			$readPermission = !is_null(Input::get('project_file_read.'.$i)) ? 1 : 0;
			$writePermission = !is_null(Input::get('project_file_write.'.$i)) ? 1 : 0;
			$deletePermission = !is_null(Input::get('project_file_delete.'.$i)) ? 1 : 0;
			$manageUsersRepository->updateUserPermissions($userId, $projectFiles[$i], $readPermission, $writePermission, $deletePermission, $entityType);
		}

        $data['company_admin'] = Input::get('company_admin');
        $data['project_admin'] = Input::get('project_admin');
        $user = $manageUsersRepository->getCompanyUser($userId);
        $manageUsersRepository->updateCompanyUserAdminPermissions($user, $data);
        if(isset($data['company_admin'])) {
            $sendMailArray = [
                'email' => $user->email,
                'name' => $user->name
            ];

            //Send email
            $manageUsersRepository->sendChangeCompanyAdminEmail($sendMailArray);

            Flash::success(trans('messages.users.permissions.success'));
            if (Auth::user()->company->subscription_type->name == Config::get('subscription_levels.level-one-title')) {
                return Redirect::to('/projects');
            }
            return Redirect::to('/home');
        } else if(isset($data['project_admin'])){
            Flash::success(trans('messages.users.permissions.success'));
            return Redirect::to('/manage-users');
        }

		//Error message if the user wasn't created
		Flash::success(trans('messages.users.permissions.success'));
		return Redirect::to('manage-users/'.$userId.'/permissions');
	}

	/**
	 * Show the form for creating a new user.
	 *
	 * @param ManageUsersRepository $manageUsersRepository
	 * @return Response
	 */
	public function create(ManageUsersRepository $manageUsersRepository)
	{
		$data['companyAddresses'] = $manageUsersRepository->getCompanyAddresses(Auth::user()->comp_id);
		return view('manage_users.create', $data);
	}

	/**
	 * Store a newly created user in database.
	 *
	 * @param ManageCompanyUsersRequest $request
	 * @param StoreCompanyUserWrapper $wrapStore
	 * @return Response
	 */
	public function store(ManageCompanyUsersRequest $request, StoreCompanyUserWrapper $wrapStore)
	{
		$storeUser = $wrapStore->createCompanyUser(Auth::user()->comp_id, $request->all());

		if ($storeUser) {

			//Successfully created user message
			Flash::success(trans('messages.users.store.success'));
			return Redirect::to('manage-users');
		}

		//Error message if the user wasn't created
		Flash::error(trans('messages.users.store.error'));
		return Redirect::to('manage-users');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified user.
	 *
	 * @param  int $id
	 * @param ManageUsersRepository $manageUsersRepository
	 * @return Response
	 */
	public function edit($id, ManageUsersRepository $manageUsersRepository)
	{
		$data['user'] = $manageUsersRepository->getCompanyUser($id);
		$data['companyAddresses'] = $manageUsersRepository->getCompanyAddresses(Auth::user()->comp_id);
		return view('manage_users.edit', $data);
	}

	/**
	 * Update the specified user in database table.
	 *
	 * @param  int $id
	 * @param ManageUsersRepository $manageUsersRepository
	 * @param UpdateCompanyUserRequest $request
	 * @return Response
	 */
	public function update($id, ManageUsersRepository $manageUsersRepository, UpdateCompanyUserRequest $request)
	{
		$response = $manageUsersRepository->updateCompanyUser($id, $request->all());

		if ($response) {

			//Successful updating of a company user
			Flash::success(trans('messages.users.update.success'));
			return Redirect::to('manage-users');
		}

		//Error message if the update doesn't happen
		Flash::error(trans('messages.users.update.error'));
		return Redirect::to('manage-users');
	}

	/**
	 * Remove the specified user from database table.
	 * This is not regular deleting. Only updates the active field in 0
	 * so the user becomes inactive
	 *
	 * @param  int $id
	 * @param ManageUsersRepository $manageUsersRepository
	 * @return Response
	 */
	public function destroy($id, ManageUsersRepository $manageUsersRepository)
	{
        //split ids if multiple
        $ids = explode('-', $id);

        $errorCheck = false;
        foreach ($ids as $id) {
            $response = $manageUsersRepository->deleteCompanyUser($id);

            if (is_null($response)) {
                $errorCheck = true;
            }
        }

		//Successful deleting (only changes the active field in 0)
		if (!$errorCheck) {
			Flash::success(trans('messages.users.delete.success'));
			return Redirect::to('manage-users');
		}

		//Error message if the deleting doesn't happen
		Flash::error(trans('messages.users.delete.error'));
		return Redirect::to('manage-users');
	}

}
