<?php
namespace App\Http\Controllers;

use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Http\Requests\ContractsRequest;
use App\Requests;
use App\Utilities\ClassMap;
use App\Utilities\FileStorage;

use Illuminate\Support\Facades\Config;
use Request;
use Input;
use Flash;
use Auth;

class ContractsController extends Controller
{
    private $contractsRepo;
    private $projectRepo;
    private $filesRepo;

    public function __construct(ContractsRepository $contractsRepo, ProjectRepository $projectRepo, FilesRepository $filesRepo)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        //$this->middleware('subscription_level_one');
        //$this->middleware('only_contractor');
        $this->middleware('sub_module_write', ['only' => ['create','store','update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->contractsRepo = $contractsRepo;
        $this->projectRepo = $projectRepo;
        $this->filesRepo = $filesRepo;
    }

    /**
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function index($projectId)
    {
        $sort = Input::get('sort','ID');
        $order = Input::get('order','asc');
        $contracts = $this->contractsRepo->getContractsByProject($projectId, $sort, $order);
        $project = $this->projectRepo->getProject($projectId);

        return view('projects.contracts.index',compact('project','contracts'));
    }

    /**
     * Filter contracts
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filter($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $company = Input::get('company_id');
        $name = Input::get('name');
        $order = Input::get('order');
        $sort = Input::get('sort');

        //Set filter type
        $type = 'contracts';

        //implement contracts filter
        $data['contracts'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'company', 'name', 'projectId', 'sort', 'order'));
        $data['project'] = $projectRepository->getProject($projectId);

        return view('projects.contracts.index', $data);

    }

    /**
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function create($projectId)
    {
        $project = $this->projectRepo->getProject($projectId);

        return view('projects.contracts.create',compact('project'));
    }

    /**
     * @param $projectId
     * @param ContractsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, ContractsRequest $request)
    {
        $data = $request->all();
        $contract = $this->contractsRepo->storeContract($projectId, $data);
        if ($contract) {
            Flash::success(trans('messages.projectItems.insert.success', ['item'=> 'contract']));
            return redirect('projects/'.$projectId.'/contracts/'.$contract.'/edit');
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item'=> 'contract']));
            return redirect('projects/'.$projectId.'/contracts');
        }


    }

    /**
     * @param $projectId
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $id)
    {
        $project = $this->projectRepo->getProject($projectId);
        $contract = $this->contractsRepo->getContract($id);

        return view('projects.contracts.edit',compact('project', 'contract'));
    }

    /**
     * @param $projectId
     * @param $id
     * @param ContractsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $id, ContractsRequest $request)
    {
        $data = $request->all();
        $contract = $this->contractsRepo->updateContract($id, $data);

        if ($contract){
            Flash::success(trans('messages.projectItems.update.success', ['item'=> 'contract']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item'=> 'contract']));
        }

        return redirect('projects/'.$projectId.'/contracts/');
    }

    /**
     * @param $projectId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function show($projectId, $id)
    {
        return redirect('projects/'.$projectId.'/contracts/');
    }

    /**
     * Delete contract from database
     * @param $projectId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectId, $id)
    {
        $idsArray = explode('-', $id);

        $errorEntry = false;
        foreach ($idsArray as $contractId) {

            $files = $this->contractsRepo->getContractFiles($projectId, $contractId);
            $deleteFile = true;
            foreach ($files as $file) {
                $params = array('proj_id'=>$projectId, 'contr_id'=>$contractId);
                //get aws path
                $path = FileStorage::path('contract', $params);
                //delete file by name and path
                $deleteFile = $this->filesRepo->deleteFile($path, $file->file_name);
            }

            $delete = $this->contractsRepo->deleteContract($contractId);

            if ($delete && $deleteFile) {
                //success
            } else {
                $errorEntry = false;
            }
        }

        if ($errorEntry) {
            Flash::error(trans('messages.projectItems.delete.error', ['item'=> 'contract']));
        } else {
            Flash::success(trans('messages.projectItems.delete.success', ['item'=> 'contract']));
        }

        return redirect('projects/'.$projectId.'/contracts/');
    }

}