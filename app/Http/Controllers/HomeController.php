<?php
namespace App\Http\Controllers;

use Auth;
use Config;

use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Blog\Repositories\BlogRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Files\Repositories\FilesRepository;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @param ProjectRepository $projectRepository
	 * @param BlogRepository $blogRepository
	 * @param UsersRepository $usersRepository
	 * @param AddressBookRepository $addressBookRepository
	 * @param FilesRepository $filesRepository
	 * @return Response
	 */
	public function index(ProjectRepository $projectRepository, BlogRepository $blogRepository, UsersRepository $usersRepository, AddressBookRepository $addressBookRepository, FilesRepository $filesRepository)
	{
		//if it's super admin
		if (Auth::user()->hasRole(Config::get('constants.roles.super_admin'))) {
			return redirect('/list-companies/');
		}

		$number = 5;

		$projects = $projectRepository->getLastProjectsByCompany($number);
		$blogPosts = $blogRepository->getLastBlogPostsByCompany(Config::get('constants.recent_files.blog'));
		$users = $usersRepository->getLastUsersByCompany($number);
		$addressBookEntries = $addressBookRepository->getLastAddressBookEntries($number);
		$files = $filesRepository->getLastUploadedFilesByCompany($number);

		return view('home.home', compact('projects', 'blogPosts', 'users', 'addressBookEntries', 'files'));
	}

	/**
	 * Get all uploaded files for the specified company (permission dependent)
	 * @param FilesRepository $filesRepository
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
	public function latestUploadedFiles(FilesRepository $filesRepository)
	{
		//if it's super admin
		if (Auth::user()->hasRole(Config::get('constants.roles.super_admin'))) {
			return redirect('/list-companies/');
		}

		$projectId = null;

		$data['files'] = $filesRepository->getAllUploadedFiles($projectId);
		return view('home.all_uploaded_files', $data);
	}

	/**
	 * Get all blog posts for the specified company (permission dependent)
	 * @param BlogRepository $blogRepository
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
	public function latestBlogPosts(BlogRepository $blogRepository)
	{
		//if it's super admin
		if (Auth::user()->hasRole(Config::get('constants.roles.super_admin'))) {
			return redirect('/list-companies/');
		}

		$projectId = null;

		$data['blogPosts'] = $blogRepository->getAllBlogPosts($projectId);
		return view('home.all_blog_posts', $data);
	}

}
