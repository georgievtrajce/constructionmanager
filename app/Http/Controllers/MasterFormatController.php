<?php namespace App\Http\Controllers;
use App\Http\Requests\MasterFormatTypeRequest;
use App\Models\Master_Format;
use App\Http\Controllers\Controller;
use App\Http\Requests\MasterFormatRequest;
use App\Models\Master_format_type;
use Request;
use Input;
use Excel;
use Form;
use Paginator;
use Flash;
/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/10/2015
 * Time: 6:56 PM
 */

class MasterFormatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     */
    public function index()
    {
        $data = array();

        $masterFormatTypes = Master_format_type::orderBy('id', 'ASC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        if (Input::get('master_format_type')) {
            $masterFormatFilter = Input::get('master_format_type');
        } else {
            $firstMasterFormatType = $masterFormatTypes->first();
            $masterFormatFilter = $firstMasterFormatType->id;
        }


        $tempMasterFormat = Master_Format::where('master_format_type_id', '=', $masterFormatFilter);

        if (Input::get('number')) {
            $tempMasterFormat->where('number', 'LIKE', '%'.Input::get('number') .'%');
        }

        if (Input::get('name')) {
            $tempMasterFormat->where('title', 'LIKE', '%'.Input::get('name') .'%');
        }

        $sort = Input::get('sort', 'ID');
        $order = Input::get('order', 'asc');

        $tempMasterFormat->orderBy($sort, $order);

        $paginatedMasterFormatTypes = Master_format_type::orderBy('id', 'ASC')->paginate(100);

        $masterFormat = $tempMasterFormat->paginate(100);
        $masterFormat->setPath('masterformat');
        $data['masterFormat'] = $masterFormat;
        $data['paginatedMasterFormat'] = $paginatedMasterFormatTypes;
        $data['masterFormatTypes'] = $masterFormatTypesArray;
        return view('masterformat.view', $data);
    }

    public function upload()
    {
        $data = array();
        $masterFormatTypes = Master_format_type::orderBy('id', 'ASC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        $data['masterFormatTypes'] = $masterFormatTypesArray;
        return view('masterformat.import', $data);
    }

    public function import(MasterFormatRequest $request)
    {
        $masterFormat = $request->file('master_format');

        if (isset($masterFormat)) {
            if ($masterFormat->getClientOriginalExtension() !='csv') {
                //handle if file is not csv
                Flash::error(trans('messages.masterFormat.invalidType'));
                return redirect()->action('MasterFormatController@upload');
            }
            Excel::load($masterFormat, function ($reader) use ($request) {
                $reader->noHeading()->each(function ($row) use ($request) {
                    $mfNumber = str_replace(" ", "", $row[0]);
                    $mfTitle = $row[1];
                    Master_Format::firstOrCreate(array('master_format_type_id' => $request->master_format_type, 'number' => $mfNumber,'title' => $mfTitle));
                });
            });
            Flash::success(trans('messages.masterFormat.import.success'));
            return redirect()->action('MasterFormatController@index');
        }

    }

    public function edit($id)
    {
        $data = array();
        $masterFormatTypes = Master_format_type::orderBy('id', 'ASC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        $data['masterFormatTypes'] = $masterFormatTypesArray;
        $item = Master_Format::where('id', '=', $id)->firstOrFail();
        $data['item'] = $item;
        return view('masterformat.edit',$data);
    }

    public function update($id, MasterFormatRequest $request)
    {
        $masterFormat = Master_Format::where('id', '=', $id)->firstOrFail();
        $masterFormat->title = $request->title;
        $masterFormat->number = $request->number;
        $masterFormat->master_format_type_id = $request->master_format_type;

        if ($masterFormat->update()) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'master format item']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'master format item']));
        }

        return redirect()->action('MasterFormatController@index');
    }

    public function create()
    {
        $masterFormatTypes = Master_format_type::orderBy('id', 'ASC')->get();

        $masterFormatTypesArray = [];
        foreach ($masterFormatTypes as $masterFormatType) {
            $masterFormatTypesArray[$masterFormatType->id] = $masterFormatType->name;
        }

        $data['masterFormatTypes'] = $masterFormatTypesArray;

        return view('masterformat.add', $data);
    }

    public function store(MasterFormatRequest $request)
    {
        $masterFormat = Master_Format::firstOrCreate(array('master_format_type_id' => $request->master_format_type, 'number' => $request->number, 'title' => $request->title));
        if ($masterFormat) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'master format item']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'master format item']));
        }

        return redirect()->action('MasterFormatController@index');
    }

    public function createMasterFormatType()
    {
        return view('masterformat.addType');
    }

    public function storeMasterFormatType(MasterFormatTypeRequest $request)
    {
        $masterFormatType = Master_format_type::firstOrCreate(array('name' => $request->title, 'year' => $request->year));
        if ($masterFormatType) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'master format version']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'master format version']));
        }

        return redirect()->action('MasterFormatController@index', ['view' => 'type']);
    }

    public function editMasterFormatType($id)
    {
        $item = Master_format_type::where('id', '=', $id)->firstOrFail();
        $data['item'] = $item;
        return view('masterformat.editType',$data);
    }

    public function updateMasterFormatType($id, MasterFormatTypeRequest $request)
    {
        $masterFormat = Master_format_type::where('id', '=', $id)->firstOrFail();
        $masterFormat->name = $request->title;
        $masterFormat->year = $request->year;

        if ($masterFormat->update()) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'master format version']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'master format version']));
        }

        return redirect()->action('MasterFormatController@index', ['view' => 'type']);
    }

    public function destroy($ids)
    {
        $idsArray = explode('-', $ids);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $response = Master_Format::where('id', '=', $id)->delete();

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'master format item']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'master format item']));
        }

        return redirect()->action('MasterFormatController@index');
    }

    public function destroyMasterFormatType($ids)
    {
        $idsArray = explode('-', $ids);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $response = Master_format_type::where('id', '=', $id)->delete();

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'master format version']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'master format version']));
        }

        return redirect()->action('MasterFormatController@index', ['view' => 'type']);
    }

}
