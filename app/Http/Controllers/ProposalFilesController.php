<?php
namespace App\Http\Controllers;

use App\Modules\Blog\Repositories\BlogRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Http\Requests\FilesRequest;
use App\Requests;
use App\Utilities\FileStorage;

use Illuminate\Support\Facades\Redirect;
use Request;
use Input;
use Flash;
use Auth;

class ProposalFilesController extends Controller
{
    private $proposalsRepo;
    private $projectRepo;
    private $filesRepo;

    public function __construct(ProposalsRepository $proposalsRepo, ProjectRepository $projectRepo, FilesRepository $filesRepo)
    {
        $this->middleware('auth');
        $this->proposalsRepo = $proposalsRepo;
        $this->projectRepo = $projectRepo;
        $this->filesRepo = $filesRepo;
    }

    /**
     * list all proposals for a proposal supplier record
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @return \Illuminate\View\View
     */
    public function index($projectId, $id, $supplierId)
    {

        $project = $this->projectRepo->getProject($projectId);

        $files = $this->proposalsRepo->getProposalFiles($projectId, $id, $supplierId);
        return view('projects.proposals.files.index', compact('files','project','id','supplierId'));
    }

    /**
     * store file record to db and file to aws
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param FilesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, $id, $supplierId, FilesRequest $request)
    {
        $data = $request->all();

        //insert record in files table
        $this->filesRepo->storeFileRecord($data);

        //insert record in proposals table
        $this->proposalsRepo->storeFile($supplierId, $data['file_id']);

        Flash::success(trans('messages.projectFiles.upload.success', ['item'=> 'proposal file']));

        return redirect('projects/'.$projectId.'/proposals/'.$id.'/suppliers/'.$supplierId.'/files');

    }

    /**
     * download file, if pdf open in new tab
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param $name
     * @param ProjectFilesRepository $filesRepository
     * @param DataTransferLimitation $transferLimitation
     * @return mixed
     */
    public function show($projectId, $id, $supplierId, $name, ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
    {
        //get proposal file
        $proposalFile = $filesRepository->getProjectFileByName($projectId, $name);

        //check download file allowance
        if ($transferLimitation->checkDownloadTransferAllowance($proposalFile->size)) {
            $params = array('proj_id' => $projectId,
                'prop_id' => $id,
                'supp_id' => $supplierId);

            $path = FileStorage::path('proposal', $params);

            $file = $this->filesRepo->getFile($path, $name);
            $this->filesRepo->logDownloadByName($name);

            //increase company download transfer
            $transferLimitation->increaseDownloadTransfer($proposalFile->size);

            return $file;
        }

        Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => trans('labels.Proposal')]));
        return Redirect::back();
    }

    /**
     * load edit view with proposal and file data
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param $fileId
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $id, $supplierId, $fileId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $supplier = $this->proposalsRepo->getSupplier($supplierId);
        $file = $this->proposalsRepo->getProposalFile($projectId, $fileId, $supplierId);

        return view('projects.proposals.files.edit',compact('file','project','supplier'));
    }

    /**
     * delete file and db records
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param $fileId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectId, $id, $supplierId, $fileId)
    {
        //get aws path
        $params = array('proj_id'=>$projectId,
            'prop_id' => $id,
            'supp_id' => $supplierId);

        $path = FileStorage::path('proposal', $params);

        //delete file by name and path
        $deleteFile = $this->filesRepo->deleteFile($path, Input::get('file_name'));

        if ($deleteFile) {
            $this->proposalsRepo->deleteFile($fileId);
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'proposal file']));
        } else {
            //else notify user
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'proposal file']));
        }

        return redirect('projects/'.$projectId.'/proposals/'.$id.'/suppliers/'.$supplierId.'/files');
    }

    /**
     * update file and record
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param $fileId
     * @param FilesRequest $request
     * @param DataTransferLimitation $transferLimitation
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $id, $supplierId, $fileId, FilesRequest $request, DataTransferLimitation $transferLimitation)
    {
        $data = $request->all();

        $fileUpdated = $this->proposalsRepo->updateProposalFile($fileId, $data);

        if ($fileUpdated) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'proposal file']));
        } else {
            //else notify user
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'proposal file']));
        }

        return redirect('projects/'.$projectId.'/proposals/'.$id.'/suppliers/'.$supplierId.'/files');
    }
}