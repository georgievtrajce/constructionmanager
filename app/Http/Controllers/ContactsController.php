<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateAddressBookContactsRequest;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Contacts\Implementations\ContactsWrapper;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;

class ContactsController extends Controller {

	public function __construct()
	{
		$this->middleware('module_write', ['only' => ['store','show','edit','update']]);
		$this->middleware('module_delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param $contactType
	 * @param $companyId
	 * @param ContactsWrapper $contactsWrapper
	 * @param ContactsRepository $repository
	 * @param AddressBookRepository $addressBookRepo
	 * @return Response
	 * @throws \Exception
	 */
	public function index($contactType, $companyId, ContactsWrapper $contactsWrapper, ContactsRepository $repository, AddressBookRepository $addressBookRepo)
	{
		$data['contacts'] = $contactsWrapper->implementGetContacts(ClassMap::instance(Config::get('classmap.contacts.'.$contactType.'.get')), $companyId);
		$data['contactAddresses'] = $repository->getAddressBookAddresses($companyId);
		$data['basicInfo'] = $addressBookRepo->getEntryBasicInfo($companyId);
		$data['companyId'] = $companyId;
		return view('address_book.create-contacts', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $contactType
	 * @param $companyId
	 * @param CreateAddressBookContactsRequest $request
	 * @param ContactsWrapper $contactsWrapper
	 * @return Response
	 */
	public function store($contactType, $companyId, CreateAddressBookContactsRequest $request, ContactsWrapper $contactsWrapper)
	{
		$response = $contactsWrapper->implementStoreContacts(ClassMap::instance(Config::get('classmap.contacts.'.$contactType.'.store')), $companyId, $request->all());

		if ($response) {
			Flash::success(trans('messages.addressBook.store.success', ['item' => 'contact']));
			return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts');
		}

		Flash::error(trans('messages.addressBook.store.error', ['item' => 'Contact']));
		return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $contactType
	 * @param $companyId
	 * @param $contactId
	 * @param ContactsWrapper $contactsWrapper
	 * @param ContactsRepository $repository
	 * @param AddressBookRepository $addressBookRepo
	 * @return Response
	 * @throws \Exception
	 */
	public function edit($contactType, $companyId, $contactId, ContactsWrapper $contactsWrapper, ContactsRepository $repository, AddressBookRepository $addressBookRepo)
	{
		$data['contacts'] = $contactsWrapper->implementGetContacts(ClassMap::instance(Config::get('classmap.contacts.'.$contactType.'.get')), $companyId);
		$data['contact'] = $contactsWrapper->implementGetEditContact(ClassMap::instance(Config::get('classmap.contacts.'.$contactType.'.get')), $companyId, $contactId);
		$data['contactAddresses'] = $repository->getAddressBookAddresses($companyId);
		$data['basicInfo'] = $addressBookRepo->getEntryBasicInfo($companyId);
		$data['companyId'] = $companyId;
		$data['contactId'] = $contactId;

		$typeFolder = str_replace("-","_",$contactType);

		return view($typeFolder.'.edit-contact', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $contactType
	 * @param $companyId
	 * @param $contactId
	 * @param CreateAddressBookContactsRequest $request
	 * @param ContactsWrapper $contactsWrapper
	 * @return Response
	 */
	public function update($contactType, $companyId, $contactId, CreateAddressBookContactsRequest $request, ContactsWrapper $contactsWrapper)
	{
		$response = $contactsWrapper->implementUpdateContact(ClassMap::instance(Config::get('classmap.contacts.'.$contactType.'.edit')), $companyId, $contactId, $request->all());

		if ($response) {
			Flash::success(trans('messages.addressBook.update.success', ['item' => 'Contact']));
			return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts/'.$contactId.'/edit');
		}

		Flash::error(trans('messages.addressBook.update.error', ['item' => 'Contact']));
		return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts/'.$contactId.'/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $contactType
	 * @param $companyId
	 * @param $contactId
	 * @param ContactsRepository $contactsRepository
	 * @return Response
	 */
	public function destroy($contactType, $companyId, $contactId, ContactsRepository $contactsRepository)
	{
        //split contacts if multiple
        $contactsIdsArray = explode('-', $contactId);

        $errorCheck = false;
        foreach ($contactsIdsArray as $id) {
            //delete contact
            $response = $contactsRepository->deleteContact($id);

            if ($response) {
                $errorCheck = false;
            } else {
                $errorCheck = true;
            }
        }

		if (!$errorCheck) {
			Flash::success(trans('messages.addressBook.delete.success', ['item' => 'Contact']));
			return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts/');
		}

		Flash::error(trans('messages.addressBook.delete.error', ['item' => 'Contact']));
		return Redirect::to('/'.$contactType.'/'.$companyId.'/contacts/');
	}

}
