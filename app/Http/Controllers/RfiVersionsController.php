<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AddRfiVersionRequest;
use App\Http\Requests\EditRfiVersionRequest;
use App\Http\Requests\EditRfiVersionTransmittalNotesRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Transmittals\Repositories\TransmittalsRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class  RfiVersionsController extends Controller {

    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    public function __construct(CompaniesRepository $companiesRepository)
	{
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->companiesRepository = $companiesRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $projectId
	 * @param $rfiId
	 * @param ProjectRepository $projectRepository
	 * @param RfisRepository $rfisRepository
	 * @return Response
	 */
	public function create($projectId, $rfiId, ProjectRepository $projectRepository, RfisRepository $rfisRepository)
	{
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;
		$data['rfi'] = $rfisRepository->getRfi($rfiId);

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $rfisRepository->getAllRfiStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

        //get status from last version id
        $data['lastVersionStatus'] = 1;
        if(count($data['rfi']['rfis_versions']) > 0){
            foreach ($data['rfi']['rfis_versions'] as $value) {
                if($value['id'] == $data['rfi']->last_version){
                    $data['lastVersionStatus'] = $value['status_id'];
                }
            }
        }

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;

		return view('projects.rfis.add-version', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $projectId
	 * @param $rfiId
	 * @param AddRfiVersionRequest $request
	 * @param RfisRepository $rfisRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return Response
	 */
	public function store($projectId, $rfiId, AddRfiVersionRequest $request,RfisRepository $rfisRepository, DataTransferLimitation $transferLimitation)
	{
		//Store rfi version
		$response = $rfisRepository->storeRfiVersion($rfiId, $request->all());

		if($response) {
			//Successfully created rfi
			Flash::success(trans('messages.submittals.store.success', ['item' => 'RFI version']));
			//return Redirect::to('projects/' . $projectId . '/rfis');
            return Redirect::to('projects/'.$projectId.'/rfis/'.$rfiId.'/version/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'RFI version']));
		return Redirect::to('projects/' . $projectId . '/rfis');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $rfiId
     * @param $versionId
     * @param RfisRepository $rfisRepository
     * @param ProjectRepository $projectRepository
     * @param TransmittalsRepository $transmittalsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @param ManageUsersRepository $manageUsersRepository
     * @return Response
     * @internal param int $id
     */
    public function edit(
        $projectId,
        $rfiId,
        $versionId,
        RfisRepository $rfisRepository,
        ProjectRepository $projectRepository,
        TransmittalsRepository $transmittalsRepository,
        CheckUploadedFilesInterface $checkUploadedFiles,
        ManageUsersRepository $manageUsersRepository
    ) {
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $projectUsers = $manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $rfisRepository->getAllRfiStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

		$data['rfiVersion'] = $rfisRepository->getRfiVersion($versionId);
		if(isset($data['rfiVersion']['file']) && count($data['rfiVersion']['file']) > 0)
        {
            $data['rfiVersion']['email_transmittal_files'] = $checkUploadedFiles->checkAndOrderUploadedFilesForVersion($data['rfiVersion']['file'], Config::get('constants.rfis_versions'));
        }
		$data['rec_sub'] = ($data['rfiVersion']->rec_sub != 0) ? Carbon::parse($data['rfiVersion']->rec_sub)->format('m/d/Y') : '';
		$data['sent_appr'] = ($data['rfiVersion']->sent_appr != 0) ? Carbon::parse($data['rfiVersion']->sent_appr)->format('m/d/Y') : '';
		$data['rec_appr'] = ($data['rfiVersion']->rec_appr != 0) ? Carbon::parse($data['rfiVersion']->rec_appr)->format('m/d/Y') : '';
		$data['subm_sent_sub'] = ($data['rfiVersion']->subm_sent_sub != 0) ? Carbon::parse($data['rfiVersion']->subm_sent_sub)->format('m/d/Y') : '';

		//check if transmittals are generated
		$data['recTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.rfis') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.sent_appr'));
		$data['subTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.rfis') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.subm_sent_sub'));

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $distributionListContactsAppr = $rfisRepository->getContactsForRfiVersion($versionId, 'appr');
        $selectedDistributionsAppr = [];
        $selectedDistributionsUsersAppr = [];
        foreach ($distributionListContactsAppr as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsAppr[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsersAppr[] = $contact->user_id;
            }
        }

        $distributionListContactsSubc = $rfisRepository->getContactsForRfiVersion($versionId, 'subc');
        $selectedDistributionsSubc = [];
        $selectedDistributionsUsersSubc = [];
        foreach ($distributionListContactsSubc as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsSubc[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsersSubc[] = $contact->user_id;
            }

        }


        $data['selectedDistributionsAppr'] = $selectedDistributionsAppr;
        $data['selectedDistributionsSubc'] = $selectedDistributionsSubc;
        $data['selectedDistributionsUsersAppr'] = $selectedDistributionsUsersAppr;
        $data['selectedDistributionsUsersSubc'] = $selectedDistributionsUsersSubc;
        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;
        $data['projectUsers'] = $projectUserIds;

		return view('projects.rfis.edit-version', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $projectId
	 * @param $rfiId
	 * @param $versionId
	 * @param EditRfiVersionRequest $request
	 * @param RfisRepository $rfisRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return Response
	 * @internal param int $id
	 */
	public function update($projectId, $rfiId, $versionId, EditRfiVersionRequest $request, RfisRepository $rfisRepository, DataTransferLimitation $transferLimitation)
	{
		//Update rfi version
		$updateResponse = $rfisRepository->updateRfiVersion($versionId, $request->all());
		if ($updateResponse) {
			//Successfully updated rfi
			Flash::success(trans('messages.submittals.update.success', ['item' => 'RFI version']));
			return Redirect::to('projects/'.$projectId.'/rfis/'.$rfiId.'/version/'.$versionId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'RFI version']));
		return Redirect::to('projects/'.$projectId.'/rfis/'.$rfiId.'/version/'.$versionId.'/edit');
	}

    /**
     * Update transmittal notes for rfi version
     *
     * @param $projectId
     * @param $rfiId
     * @param $versionId
     * @param EditRfiVersionRequest|EditRfiVersionTransmittalNotesRequest $request
     * @param RfisRepository $rfisRepository
     * @return Response
     * @internal param int $id
     */
    public function updateTransmittalNotes($projectId, $rfiId, $versionId, EditRfiVersionTransmittalNotesRequest $request, RfisRepository $rfisRepository)
    {
        //Update rfi version
        $rfisRepository->updateRfiVersionVersionTransmittalNotes($versionId, $request->all());
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $projectId
	 * @param $rfiId
	 * @param $versionId
	 * @param RfisRepository $rfisRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy($projectId, $rfiId, $versionId, RfisRepository $rfisRepository)
	{
        $idsArray = explode('-', $versionId);

        foreach ($idsArray as $id) {
            //select rfi version for file deleting
            $rfiVersion = $rfisRepository->getRfiVersion($id);

            //delete rfi version files from s3
            if (count($rfiVersion->file)) {
                foreach ($rfiVersion->file as $file) {
                    $rfisRepository->deleteRfiFile($projectId, $file->file_name);
                }
            }

            //check if it's a current version
            if ($rfisRepository->lastVersionCheck($rfiId, $id)) {
                //change current rfi version
                $rfisRepository->changeLastVersion($rfiId, $id);
            }

            //delete rfi version from database (with the file from files table)
            $response = $rfisRepository->deleteRfiVersion($id);
        }


		//Successfully deleted rfi version
		Flash::success(trans('messages.submittals.delete.success', ['item' => 'RFI']));
		return Redirect::to('projects/'.$projectId.'/rfis');
	}

	/**
	 * Download rfi file
	 * @param $projectId
	 * @param $versionId
	 * @param $fileName
	 * @param RfisRepository $rfisRepository
	 * @param FilesRepository $filesRepository
	 * @param ProjectRepository $projectRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return mixed
	 */
	public function download($projectId, $rfiId, $versionId, $fileName, RfisRepository $rfisRepository, FilesRepository $filesRepository, ProjectRepository $projectRepository, DataTransferLimitation $transferLimitation)
	{
		//Get rfi version
		$rfi = $rfisRepository->getRfiVersion($versionId);

		//get file size
		$fileSize =  $rfi->file[0]->size;

		//check download file allowance
		if ($transferLimitation->checkDownloadTransferAllowance($fileSize)) {
			$project = $projectRepository->getProjectRecord($projectId);

			//Download rfi version file
			$file = Storage::disk('s3')->get('company_' . $project->comp_id . '/project_' . $projectId . '/rfis/' . $fileName);
			$filesRepository->logDownloadByName($fileName);

			//increase company download transfer
			$transferLimitation->increaseDownloadTransfer($fileSize);

			return Response::make($file, 200)->header('Content-type', 'application/*', 'Content-Disposition', 'attachment');
		}

		Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => trans('labels.rfi')]));
		return Redirect::back();
	}

}
