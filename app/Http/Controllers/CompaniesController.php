<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\UpdateCompanyProfileRequest;
use App\Models\Subscription_type;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Utilities\FreeSubscriptionMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Laracasts\Flash\Flash;
use Input;
use Config;

class CompaniesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @param $companyId
     * @param CompaniesRepository $repository
     * @param FilesRepository $filesRepository
     * @return Response
     */
	public function index($companyId, CompaniesRepository $repository, FilesRepository $filesRepository)
	{
		$company = $repository->getCompany($companyId);

        $filesUsedStorage = $filesRepository->getUsedStorageForProjectFiles();
        $addressBookDocumentsUsedStorage = $filesRepository->getUsedStorageForAddressBookFiles();
        $tasksFilesUsedStorage = $filesRepository->getUsedStorageForTasksFiles();
        $currentStorage = (float)$filesUsedStorage + (float)$addressBookDocumentsUsedStorage + (float)$tasksFilesUsedStorage;
		$data['referralPointsLabel'] = $this->checkReferralPoints($company->ref_points);
		$data['company'] = $company;
		$data['usedStorage'] = $currentStorage;
		return view('company_profile.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $companyId
	 * @return Response
	 */
	public function addAddress($companyId)
	{
		$data['companyId'] = $companyId;
		return view('company_profile.add-address', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param UpdateCompanyProfileRequest $request
	 * @param CompaniesRepository $repository
	 * @return Response
	 */
	public function update($id, UpdateCompanyProfileRequest $request, CompaniesRepository $repository)
	{
		if ($request->file('logo')) {
			$repository->updateCompanyLogo($request->file('logo'), $id);
		}

		$repository->updateCompany($id, $request->all());

		Flash::success(trans('messages.company_profile.update.success'));
		return Redirect::to('/company-profile/'.$id);
	}

	/**
	 * Get change subscription level view
	 * @param $companyId
	 * @param CompaniesRepository $repository
	 * @return \Illuminate\View\View
     */
	public function getChangeSubscriptionLevel($companyId, CompaniesRepository $repository)
	{
		$company = $repository->getCompany($companyId);

		//get the three available application subscription levels
		$data['firstSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-one'))->firstOrFail();
		$data['secondSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->firstOrFail();
		$data['thirdSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->firstOrFail();

		//get available company's referral points
		$data['referralPointsLabel'] = $this->checkReferralPoints($company->ref_points);
		$data['company'] = $company;
		return view('company_profile.change-subscription-level', $data);
	}

	/**
	 * Change company subscription level using referral points
	 * @param $companyId
	 * @param CompaniesRepository $repository
	 * @return mixed
     */
	public function changeSubscriptionLevelByPoints($companyId, CompaniesRepository $repository)
	{
		$changeSubscription = $repository->changeSubscriptionLevelByPoints($companyId, Input::get('subs_id'));

		if ($changeSubscription == 1) {
			if (Input::has('type')) {
				Flash::success(trans('messages.company_profile.renew_subscription.success'));
			} else {
				Flash::success(trans('messages.company_profile.update_subscription.success'));
			}
		} elseif ($changeSubscription == false) {
			Flash::error(trans('messages.company_profile.update_subscription.not_enough_points'));
		} else {
			Flash::error(trans('messages.company_profile.update_subscription.error'));
		}
		return Redirect::to('/company-profile/'.$companyId);
	}

	/**
	 * Check company's referral points and allowance for a specific subscription level
	 * (it returns a string needed for representing a message on view)
	 * @param $points
	 * @return string
     */
	protected function checkReferralPoints($points)
	{
		$secondSubscription = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->first();
		$thirdSubscription = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->first();

		$labelData = array('points' => $points, 'level2' => $secondSubscription->name, 'level3' => $thirdSubscription->name);
		if ($points < $secondSubscription->ref_points_needed) {
			$label = trans('labels.ref_points.not_enough');
		}
		else if (($secondSubscription->ref_points_needed <= $points) && ($thirdSubscription->ref_points_needed > $points)) {
			if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-one')) {
				$label = trans('labels.ref_points.only_level2', $labelData);
			} else if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-two')) {
				$label = trans('labels.ref_points.only_level2', $labelData);
			} else {
				$label = trans('labels.ref_points.not_enough');
			}
		}
		else {
			if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-three')) {
				$level3years = floor($points / $thirdSubscription->ref_points_needed);
				$labelData['level3years'] = $level3years;
				$labelData['level3plural'] = $level3years > 1 ? 's' : '';
				$label = trans('labels.ref_points.only_level3', $labelData);
			} else {
				$level2years = floor($points / $secondSubscription->ref_points_needed);
				$level3years = floor($points / $thirdSubscription->ref_points_needed);
				$labelData['level2years'] = $level2years;
				$labelData['level2plural'] = $level2years > 1 ? 's' : '';
				$labelData['level3years'] = $level3years;
				$labelData['level3plural'] = $level3years > 1 ? 's' : '';
				$label = trans('labels.ref_points.level2_and_level3', $labelData);
			}
		}

		return $label;
	}

}
