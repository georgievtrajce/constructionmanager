<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AddAddressBookDocumentRequest;
use App\Http\Requests\AddRatingRequest;
use App\Http\Requests\CreateAddressBookAddressesRequest;
use App\Http\Requests\CreateAddressBookContactsRequest;
use App\Http\Requests\CreateAddressBookEntryRequest;
use App\Modules\Address_book\Implementations\CategoryFilterWrapper;
use App\Modules\Address_book\Implementations\MasterFormatFilterWrapper;
use App\Modules\Address_book\Implementations\StoreBasicInfo;
use App\Modules\Address_book\Implementations\UpdateBasicInfo;
use App\Modules\Address_book\Interfaces\EditAddressBookEntryInterface;
use App\Modules\Address_book\Interfaces\StoreAddressBookEntryInterface;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;
use LynX39\LaraPdfMerger\PDFManage;

class AddressBookController extends Controller {
    /**
     * @var AddressBookRepository
     */
    private $addressBookRepo;
    /**
     * @var ProjectRepository
     */
    private $projectRepo;
    /**
     * @var FilesRepository
     */
    private $filesRepo;
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     * Address Book controller constructor
     * Module middlewares for write permissions and delete permissions
     * @param AddressBookRepository $addressBookRepo
     * @param ProjectRepository $projectRepo
     * @param FilesRepository $filesRepo
     * @param CompaniesRepository $companiesRepository
     */
	public function __construct(AddressBookRepository $addressBookRepo, ProjectRepository $projectRepo,
                                FilesRepository $filesRepo, CompaniesRepository $companiesRepository)
	{
		$this->middleware('auth' ,['except' => 'showAddressBookFile']);

		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		$this->middleware('level_zero_permit', ['except' => 'showAddressBookFile']);

		//$this->middleware('subscription_level_one');
		$this->middleware('module_write', ['only' => ['create','store','show','update']]);
		$this->middleware('module_delete', ['only' => ['destroy']]);
        $this->addressBookRepo = $addressBookRepo;
        $this->projectRepo = $projectRepo;
        $this->filesRepo = $filesRepo;
        $this->companiesRepository = $companiesRepository;
    }

	/**
	 * Global store that currently in function for basic info store because of the different implementations for
	 * addresses and contacts
	 * @param StoreAddressBookEntryInterface $store
	 * @param $data
	 * @param null $company
	 * @return mixed
     */
	private function globalStore(StoreAddressBookEntryInterface $store, $data, $company = NULL)
	{
		return $store->postData($data, $company);
	}

	/**
	 * Global update that currently in function for basic info store because of the different implementations for
	 * addresses and contacts
	 * @param EditAddressBookEntryInterface $edit
	 * @param $data
	 * @param $id
	 * @return mixed
	 */
	private function globalUpdate(EditAddressBookEntryInterface $edit, $data, $id)
	{
		return $edit->editData($id, $data);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @param AddressBookRepository $addressBook
	 * @return Response
	 */
	public function index(AddressBookRepository $addressBook)
	{
	    $data['section'] = 'all';
		$data['category'] = Config::get('constants.address_book_categories.all');
		$data['addressBookEntries'] = $addressBook->getAllEntries();
		$data['defaultCategories'] = $addressBook->getDefaultCategories();
		$data['customCategories'] = $addressBook->getCustomCategories();
		$data['showUndo'] = !$addressBook->checkIfEmptyImportHistory();
		return view('address_book.index', $data);
	}

	/**
	 * @param $type
	 * @param $id
	 * @param CategoryFilterWrapper $categoryFilterWrapper
	 * @param AddressBookRepository $addressBook
	 * @return \Illuminate\View\View
     */

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param AddressBookRepository $categories
	 * @return Response
	 */
	public function create(AddressBookRepository $categories)
	{
		$data['defaultCategories'] = $categories->getDefaultCategories();
		$data['customCategories'] = $categories->getCustomCategories();

		return view('address_book.create-basic', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateAddressBookEntryRequest $request
	 * @param StoreBasicInfo $basicInfo
	 * @return
	 */
	public function store(CreateAddressBookEntryRequest $request, StoreBasicInfo $basicInfo)
	{
		$returnBasic = $this->globalStore($basicInfo, $request->all());

		if ($returnBasic) {
			return Redirect::to('/address-book/'.$returnBasic->id.'/addresses');
		}

		Flash::error(trans('messages.addressBook.store.error', ['item' => 'Address book company']));
		return Redirect::to('/address-book/'.$returnBasic->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @param AddressBookRepository $addressBook
	 * @return Response
	 */
	public function show($id, AddressBookRepository $addressBook)
	{
		$data['basicInfo'] = $addressBook->getEntryBasicInfo($id);
		$data['defaultCategories'] = $addressBook->getDefaultCategories();
		$data['customCategories'] = $addressBook->getCustomCategories();

		//Get default categories
		$data['defaultCategoriesIds'] = [];
		foreach ($data['basicInfo']->default_categories as $category) {
			$data['defaultCategoriesIds'][] = $category->id;
		}

		//Get custom categories for this company
		$data['customCategoriesIds']= [];
		if (!empty($data['customCategories'][0]) && count($data['basicInfo']->custom_categories) > 0) {
			foreach ($data['basicInfo']->custom_categories as $category) {
				$data['customCategoriesIds'][] = $category->id;
			}
		}

		return view('address_book.edit-basic', $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int $id
	 * @param CreateAddressBookEntryRequest $request
	 * @param UpdateBasicInfo $updateBasicInfo
	 * @return Response
	 */
	public function update($id, CreateAddressBookEntryRequest $request, UpdateBasicInfo $updateBasicInfo)
	{
		$returnBasicInfo = $this->globalUpdate($updateBasicInfo, $request->all(), $id);

		if ($returnBasicInfo) {
			Flash::success(trans('messages.addressBook.update.success', ['item' => 'Basic info']));
			return Redirect::to('/address-book/'.$id);
		}
		Flash::error(trans('messages.addressBook.update.error', ['item' => 'Basic info']));
		return Redirect::to('/address-book/'.$id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 * @param AddressBookRepository $addressBookRepository
	 * @return Response
	 */
	public function destroy($ids, AddressBookRepository $addressBookRepository)
	{
	    $idsArray = explode('-', $ids);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $response = $addressBookRepository->deleteEntry($id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

		//Response is null if this address book company is connected with some other project module
		if ($errorEntry) {
			Flash::info(trans('messages.addressBook.delete.info', ['item' => 'address book company']));
			return Redirect::to('/address-book');
		}

		//Successful delete
		Flash::success(trans('messages.addressBook.delete.success', ['item' => 'Address book company']));
		return Redirect::to('/address-book');
	}

	/**
	 * Filter the address book entries by category
	 * @param $type
	 * @param $id
	 * @param CategoryFilterWrapper $categoryFilterWrapper
	 * @param AddressBookRepository $addressBook
	 * @return \Illuminate\View\View
     */
	public function categoryFilter($type, $id, CategoryFilterWrapper $categoryFilterWrapper, AddressBookRepository $addressBook)
	{
        $data['section'] = 'category-filter';
		$categoryType = Request::segment(2);
		$categoryId = Request::segment(3);
		$category = $addressBook->getCurrentCategory($categoryType, $categoryId);
		$data['category'] = $category->name;
		$data['addressBookEntries'] = $categoryFilterWrapper->prepareFilter(ClassMap::instance(Config::get('classmap.address-book.'.$type)), $id);
		$data['defaultCategories'] = $addressBook->getDefaultCategories();
		$data['customCategories'] = $addressBook->getCustomCategories();
        $data['showUndo'] = !$addressBook->checkIfEmptyImportHistory();
		return view('address_book.index', $data);
	}

	/**
	 * Filter the address book entries by master format number and title
	 * @param MasterFormatFilterWrapper $masterFormatFilterWrapper
	 * @param AddressBookRepository $addressBook
	 * @return \Illuminate\View\View
     */
	public function masterFormatFilter(MasterFormatFilterWrapper $masterFormatFilterWrapper, AddressBookRepository $addressBook)
	{
        $data['section'] = 'master-filter';
		$data['category'] = Config::get('constants.address_book_categories.all');
		$searchData = [
			'search' => Input::get('search'),
			'number' => Input::get('number'),
			'title' => Input::get('name'),
            'companyName' => Input::get('comp_name')
		];

		if (empty($searchData['companyName']) && empty($searchData['search']) && empty($searchData['number']) && empty($searchData['name'])) {
            $data['addressBookEntries'] = $addressBook->getAllEntries();
        } else {
            $data['addressBookEntries'] = $masterFormatFilterWrapper->getData($searchData);
        }

		$data['defaultCategories'] = $addressBook->getDefaultCategories();
		$data['customCategories'] = $addressBook->getCustomCategories();
        $data['showUndo'] = !$addressBook->checkIfEmptyImportHistory();
		return view('address_book.index', $data);
	}

	/**
	 * Get company by UAC
	 * @param CompaniesRepository $companiesRepository
	 * @return mixed
     */
	public function validateUac(CompaniesRepository $companiesRepository)
	{
		$uac = Input::get('uac');

		if (!empty($uac)) {
			$company = $companiesRepository->getCompanyByUac($uac);

			if ($company) {

				if ($company->id == Auth::user()->comp_id) {
					return Response::json(trans('messages.addressBook.sync_company.your_uac'));
				}
				return Response::json($company);
			}

			return Response::json(trans('messages.addressBook.sync_company.invalid'));
		}
		return Response::json(trans('messages.addressBook.sync_company.enter_valid'));
	}

	/**
	 * View address book company with all its details, offices and contacts
	 * @param $id
	 * @param AddressBookRepository $addressBookRepository
	 * @return \Illuminate\View\View
     */
	public function view($id, AddressBookRepository $addressBookRepository)
	{
        $data['addressBookFiles'] = $this->filesRepo->getAllAddressBookFiles($id, Config::get('constants.pagination.address_book_files'));
		$data['addressBookCompany'] = $addressBookRepository->getAddressBookCompanyData($id);
		return view('address_book.view', $data);
	}

	/**
	 * Save specific address book company details as pdf document
	 * @param $id
	 * @param FilterDataWrapper $wrapper
	 * @param ReportGenerator $generator
	 * @return mixed
	 * @throws \Exception
     */
	public function viewReport($id, FilterDataWrapper $wrapper, ReportGenerator $generator)
	{

        ini_set('max_execution_time', 500);
        ini_set('xdebug.max_nesting_level', 500);

        $showDocuments = (Input::get('documents') == 'true')?true:false;
		//Set report type
		$type = 'address-book-company';

		//Get address book company data
		$data['addressBookCompany'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), $id);

        //get documents
        $data['addressBookFiles'] = $this->filesRepo->getAllAddressBookFiles($id, Config::get('constants.pagination.address_book_files'));

        //Set report view
		$view = 'address_book.company-report';

		//Set report name
		$name = "Company_View";

        $folderTemp = storage_path().'/app/'.'temp';
        if(!File::exists($folderTemp)) {
            File::makeDirectory($folderTemp);
        }

        $folder = $folderTemp.'/user'.Auth::user()->id;
        if(!File::exists($folder)) {
            File::makeDirectory($folder);
        } else {
            File::cleanDirectory($folder);
        }

        $pathArray = [];
        $pathOrientation = [];
        return $generator->generateReport($type, $data, $view, $name);
        /*
        $pdf = $generator->generateReportRaw($type, $data, $view, $name);
        $storePath = 'temp/user'.Auth::user()->id.'/'.'contact-info-'.Auth::user()->comp_id.$id.'.pdf';
        $pdf->save(storage_path().'/app/'.$storePath);
        //Storage::disk('local')->put($storePath, $pdf->stream());
        $pathArray[] = $storePath;
        $pathOrientation[] = 'P';

        if ($showDocuments) {
            $documents = $this->filesRepo->getAllAddressBookPdfFiles($id);
            foreach ($documents as $document) {
                $path = "company_".$document->comp_id."/contact_".$id."/address_book_documents/".$document->file_name;
                $file =  Storage::disk('s3')->get($path);
                $storePath = 'temp/user'.Auth::user()->id.'/'.$document->comp_id.$id.$document->file_name;
                Storage::disk('local')->put($storePath, $file);
                $pathArray[] = $storePath;
                $pathOrientation[] = $document->file_orientation;
            }


        }

        $pdf = new PDFManage();
        $i = 0;
        foreach ($pathArray as $pathPdf) {
            $pdf->addPDF(storage_path().'/app/'.$pathPdf, 'all', $pathOrientation[$i]);
            $i++;
        }

        return $pdf->merge('browser', 'company-with-documents.pdf');*/
	}

	/**
	 * Get company office and its details and contacts report
	 * @param $id
	 * @param FilterDataWrapper $wrapper
	 * @param ReportGenerator $generator
	 * @return mixed
	 * @throws \Exception
     */
	public function officeReport($id, FilterDataWrapper $wrapper, ReportGenerator $generator)
	{
		//Set report type
		$type = 'company-office';

		//Get address book company data
		$data['companyOffice'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), $id);

        /*//get rating for company
        $projectRating = $this->projectRepo->getAllRatingsPerProject($id);
        $totalRating = 0;
        foreach($projectRating as $rating) {
            if (!empty($rating->project_rating)) {
                $totalRating += $rating->project_rating;
            }
        }
        if ($totalRating > 0) {
            $totalRating = round(($totalRating/count($projectRating)),2);
        } else {
            $totalRating = 'not rated';
        }

        $data['totalRating'] = $totalRating;*/

		//Set report view
		$view = 'address_book.office-report';

		//Set report name
		$name = "Office_View";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

	/**
	 * View single office contact
	 * @param $companyId
	 * @param $contactId
	 * @param ContactsRepository $contactsRepository
	 * @return \Illuminate\View\View
     */
	public function viewContact($companyId, $contactId, ContactsRepository $contactsRepository)
	{
		$data['addressBookContact'] = $contactsRepository->getContact($companyId, $contactId);
		return view('address_book.view-contact', $data);
	}

	/**
	 * Create pdf contacts report
	 * @param $companyId
	 * @param $contactId
	 * @param FilterDataWrapper $wrapper
	 * @param ReportGenerator $generator
	 * @return mixed
	 * @throws \Exception
     */
	public function contactReport($companyId, $contactId, FilterDataWrapper $wrapper, ReportGenerator $generator)
	{
		//Set report type
		$type = 'company-contact';

		//Get address book company data
		$data['companyContact'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), $contactId);

		//Set report view
		$view = 'address_book.contact-report';

		//Set report name
		$name = "Contact_View";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

    /**
     * Shows rating page for company by company ID
     *
     * @param $companyId
     * @param null $projectId
     * @return \Illuminate\View\View
     */
    public function showRating($companyId, $projectId = null)
    {
        $basicInfo = $this->addressBookRepo->getEntryBasicInfo($companyId);
        $projects = $this->projectRepo->getAllProjectsForContact($companyId);
        $companyRating = false;
        if (!empty($projectId)) {
            $companyRating = $this->addressBookRepo->getCompanyRatingForProject($companyId, $projectId);
            $projectRating = $this->projectRepo->getAllRatingsPerProject($projectId);
        }

        return view('address_book.view-rating', compact('companyId', 'basicInfo', 'projects', 'projectId',
            'companyRating', 'projectRating'));
    }

    /**
     * Used for adding rating to companies for a project
     *
     * @param $companyId
     * @param $projectId
     * @param AddRatingRequest $request
     * @return
     */
    public function addRating($companyId, $projectId, AddRatingRequest $request)
    {
        $data = [];
        $data['price'] = (!empty(Input::get('cc_price')) || Input::get('cc_price') != '')?Input::get('cc_price'):null;
        $data['quality_of_work'] = (!empty(Input::get('cc_quality_of_work'))|| Input::get('cc_quality_of_work') != '')?Input::get('cc_quality_of_work'):null;
        $data['quality_of_materials'] = (!empty(Input::get('cc_quality_of_materials'))|| Input::get('cc_quality_of_materials') != '')?Input::get('cc_quality_of_materials'):null;
        $data['expertise'] = (!empty(Input::get('cc_expertise'))|| Input::get('cc_expertise') != '')?Input::get('cc_expertise'):null;
        $data['problems_resolution'] = (!empty(Input::get('cc_problems_resolution'))|| Input::get('cc_problems_resolution') != '')?Input::get('cc_problems_resolution'):null;
        $data['safety'] = (!empty(Input::get('cc_safety'))|| Input::get('cc_safety') != '')?Input::get('cc_safety'):null;
        $data['cleanness'] = (!empty(Input::get('cc_cleanness'))|| Input::get('cc_cleanness') != '')?Input::get('cc_cleanness'):null;
        $data['organized_and_professional'] = (!empty(Input::get('cc_organized_professional'))|| Input::get('cc_organized_professional') != '')?Input::get('cc_organized_professional'):null;
        $data['communication_skills'] = (!empty(Input::get('cc_communication_skills'))|| Input::get('cc_communication_skills') != '')?Input::get('cc_communication_skills'):null;
        $data['completed_on_time'] = (!empty(Input::get('cc_completed_on_time'))|| Input::get('cc_completed_on_time') != '')?Input::get('cc_completed_on_time'):null;

        //calculate avg from ratings
        $sum = 0;
        $numChanged = 0;
        foreach ($data as $item) {
            if ($item != null) {
                $numChanged++;
                $sum += $item;
            }
        }
        $data['project_rating'] = ($numChanged > 0)?($sum/$numChanged):null;

        //add or update rating
        $this->addressBookRepo->addOrUpdateCompanyRatingForProject($companyId, $projectId, $data);

        //update total rating for company
        $projects = $this->projectRepo->getAllProjectsForContact($companyId);
        $totalRating = 0;
        $numProjects = 0;
        foreach ($projects as $project) {
            if (!empty($project->project_rating)) {
                $numProjects++;
                $totalRating += $project->project_rating;
            }
        }
        $this->addressBookRepo->updateAddressBook($companyId, ['total_rating' => round($totalRating/$numProjects, 2)]);

        //Successful delete
        Flash::success(trans('messages.addressBook.rating.success'));
        return Redirect::to(URL('/address-book/'.$companyId.'/ratings/'.$projectId));
    }

    /**
     * PDF report
     * @param AddressBookRepository $addressBook
     * @param ReportGenerator $generator
     * @param MasterFormatFilterWrapper $masterFormatFilterWrapper
     * @return mixed
     */
    public function report(AddressBookRepository $addressBook, ReportGenerator $generator, MasterFormatFilterWrapper $masterFormatFilterWrapper)
    {
        $data['addressBookEntries'] = $addressBook->getAllEntriesNoPagination();

        return $this->generateReport($data, $generator);
    }

    /**
     * PDF report
     * @param $type
     * @param $id
     * @param CategoryFilterWrapper $categoryFilterWrapper
     * @param AddressBookRepository $addressBook
     * @param ReportGenerator $generator
     * @return mixed
     * @internal param ReportGenerator $generator
     * @internal param MasterFormatFilterWrapper $masterFormatFilterWrapper
     */
    public function reportCategoryFilter($type, $id, CategoryFilterWrapper $categoryFilterWrapper, AddressBookRepository $addressBook, ReportGenerator $generator)
    {
        $categoryType = Request::segment(2);
        $categoryId = Request::segment(3);
        $category = $addressBook->getCurrentCategory($categoryType, $categoryId);
        $data['category'] = 'Category: '.$category->name;
        $data['addressBookEntries'] = $categoryFilterWrapper->prepareFilterNotPaginated(ClassMap::instance(Config::get('classmap.address-book.'.$type)), $id);


        return $this->generateReport($data, $generator);
    }

    /**
     * Generate pdf report for master format filter
     *
     * @param MasterFormatFilterWrapper $masterFormatFilterWrapper
     * @param AddressBookRepository $addressBook
     * @param ReportGenerator $generator
     * @return \Illuminate\View\View
     */
    public function reportMasterFormatFilter(MasterFormatFilterWrapper $masterFormatFilterWrapper, ReportGenerator $generator)
    {
        $searchData = [
            'search' => Input::get('search'),
            'number' => Input::get('number'),
            'title' => Input::get('name'),
        ];
        $data['searchData'] = $searchData;
        $data['addressBookEntries'] = $masterFormatFilterWrapper->getData($searchData);

        return $this->generateReport($data, $generator);
    }

    public function generateReport($data, $generator)
    {
        //Set report type
        $type = 'address-book-report';

        //Set report view
        $view = 'address_book.report';

        //Set report name
        $name = "Address book";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * Displays document listing page
     *
     * @param $abId
     * @return \Illuminate\View\View
     */
    public function listDocuments($abId)
    {
        $basicInfo = $this->addressBookRepo->getEntryBasicInfo($abId);
        $addressBookFiles = $this->filesRepo->getAllAddressBookFiles($abId, Config::get('constants.pagination.address_book_files'));
        return view('address_book.view-documents', compact('basicInfo', 'addressBookFiles', 'abId'));
    }

    public function createFile($abId)
    {
        $basicInfo = $this->addressBookRepo->getEntryBasicInfo($abId);
        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        return view('address_book.create-document', compact('basicInfo', 'abId', 'myCompany'));
    }

    /**
     * Store a newly uploaded file in storage.
     *
     * @param $abId
     * @param AddAddressBookDocumentRequest $request
     * @return Response
     */
    public function storeFile($abId, AddAddressBookDocumentRequest $request)
    {

        $fileId = Input::get('address_book_file_id', false);
        if ($fileId) {
            $data['ab_id'] = $abId;
            $data['name'] = Input::get('file_name', '');
            $data['file_orientation'] = Input::get('file_orientation', 'P');
            $data['number'] = Input::get('file_number', '');
            $data['expiration_date'] = empty(Input::get('file_expiration_date')) ? '' : Carbon::parse(Input::get('file_expiration_date'))->format('Y-m-d');
            $data['notification_days'] = (!empty(Input::get('daysNotify')))?intval(Input::get('daysNotify')):null;
            $data['notification'] = (!empty(Input::get('daysNotify')))?1:null;
            $usersDistribution = Input::get('employees_users');
            $abContactsDistribution = Input::get('employees_users_ab');

            //store  project file
            $this->filesRepo->updateAddressBookFile($fileId, $data, $usersDistribution, $abContactsDistribution);

            //Successfully stored project file
            Flash::success(trans('messages.global.store.success'));
            return Redirect::to('address-book/'.$abId.'/files');
        }
        //Error
        Flash::success(trans('messages.global.store.error'));
        return Redirect::to('address-book/'.$abId.'/files');
    }

    /**
     * Display resource for adding address book documents
     *
     * @param $abId
     * @param $documentId
     * @return \Illuminate\View\View
     */
    public function editFile($abId, $documentId)
    {
        $basicInfo = $this->addressBookRepo->getEntryBasicInfo($abId);
        $addressBookFile = $this->filesRepo->getAddressBookFile($documentId);
        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

        $distributionListContacts = $this->addressBookRepo->getContactsForAddressBookFile($basicInfo->id);
        $selectedDistributionsContacts = [];
        $selectedDistributionsUsers = [];
        foreach ($distributionListContacts as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsContacts[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsers[] = $contact->user_id;
            }
        }

        return view('address_book.create-document', compact('basicInfo', 'abId', 'addressBookFile',
            'myCompany', 'selectedDistributionsContacts', 'selectedDistributionsUsers'));
    }

    public function deleteFile($addressBookId, $documentId)
    {
        //split document ids if multiple
        $documentIdsArray = explode('-', $documentId);

        foreach ($documentIdsArray as $id) {

            $document = $this->filesRepo->getAddressBookFile($id);

            $this->filesRepo->deleteAddressBookFileFromS3($addressBookId, $id);

            $this->filesRepo->deleteSingleAddressBookFile(['fileId' => $id]);
        }

        //Successfully deleted submittal version
        Flash::success(trans('messages.addressBook.delete.success', ['item' => 'Document']));
        return Redirect::to('address-book/'.$addressBookId.'/files');
    }

    /**
     * Used to undo the last action done by the user
     */
    public function undoLastAction()
    {
        $this->addressBookRepo->undoLastImportAction();

        //Successfully deleted submittal version
        Flash::success(trans('messages.addressBook.import.undo'));
        return Redirect::to('address-book');
    }

    /**
     * Show address book file in browser
     * @param $projectId
     * @param ProjectFilesRepository $filesRepository
     * @param DataTransferLimitation $transferLimitation
     * @return \Illuminate\View\View
     */
    public function showAddressBookFile(FilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
    {
        $fileId = Input::get('fileId');
        $path = Input::get('path');

        //check download limit
        $file = $filesRepository->getAddressBookFile($fileId);

        if ($file) {

                //check upload file allowance
                if ($transferLimitation->calculateExpectedTransfer($file->company->downloaded, $file->size,
                    $file->company->subscription_type->download_limit, $file->company)) {
                    //return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));

                    //increase download limit
                    if (!empty($file->size)) {
                        //increase company upload transfer
                        $transferLimitation->increaseDownloadTransfer($file->size);
                    }

                    if ($file->file_type == 'pdf') {
                        //show transmittal file
                        $file = Storage::disk('s3')->get($path);
                        return Response::make($file, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
                    } else if ($file->file_type == 'txt') {
                        $file = Storage::disk('s3')->get($path);
                        return Response::make($file, 200)->header('Content-type','text/plain','Content-Disposition', 'attachment');
                    } else {
                        $path = str_replace('#', '%23', $path);
                        return Redirect::to(env('AWS_CLOUD_FRONT')."/".$path);
                    }

                } else {
                    Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => 'Address book file']));
                }
        } else {
            Flash::error(trans('messages.transmittals.not_exist'));
        }

        die('Error');
    }

    /**
     * @param Request $request
     * @param AddressBookRepository $addressBookRepository
     */
    public function getProjectTransmittals(AddressBookRepository $addressBookRepository)
    {
        $projectId = Input::get('projectId');
        $addressBookId = Input::get('addressBookId');
        $isGeneralContractor = Input::get('isGeneralContractor');
        $transmittals = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $addressBookId, $isGeneralContractor);

        if($transmittals) {
            return Response::json(array('error' => 0, 'response' => $transmittals));
        }
        return Response::json(array('error' => 1));
    }

}
