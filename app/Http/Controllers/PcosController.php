<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PcosRequest;
use App\Models\Address;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Interfaces\ProjectInterface;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Tasks\Repositories\TasksRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class PcosController extends Controller {

	private $userId;
	private $companyId;
    /**
     * @var TasksRepository
     */
    private $tasksRepository;
    /**
     * @var ProjectPermissionsRepository
     */
    private $permissionsRepository;

	public function __construct(TasksRepository $tasksRepository, ProjectPermissionsRepository $permissionsRepository)
	{
		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		$this->middleware('level_zero_permit');

		$this->middleware('subscription_level_one');
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);

        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
        $this->tasksRepository = $tasksRepository;
        $this->permissionsRepository = $permissionsRepository;
	}

    /**
     * Display a listing of the resource.
     *
     * @param $projectId
     * @param PcosRepository $pcosRepository
     * @param ProjectRepository $projectRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return Response
     * @internal param RfisRepository $rfisRepository
     */
	public function index($projectId, PcosRepository $pcosRepository, ProjectRepository $projectRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','number');
		$order = Input::get('order','asc');

        //get all pco versions statuses and prepare them to array for the view
        $statuses = $pcosRepository->getAllPcoStatuses();
        $pcosStatistics = [];
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
            $pcosStatistics[$status->name] = 0;
        }
        //dd($pcosStatistics);

		//get all project pcos
		$data['pcos'] = $pcosRepository->getProjectPcos($projectId, $sort, $order);

		foreach ($data['pcos'] as $pcoKey => $pco){
            foreach ($pco['subcontractors'] as $key => $subcontractor){
                $data['pcos'][$pcoKey]['subcontractors'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($subcontractor, Config::get('constants.subcontractor_versions'));
            }
            $recipient = $checkUploadedFiles->checkAndOrderUploadedFiles($pco['recipient'], Config::get('constants.recipient_versions'));
            $data['pcos'][$pcoKey]->setRelation('recipient', $recipient);

            if (isset($pcosStatistics[trim($pco->version_status_name)])) {
                $pcosStatistics[trim($pco->version_status_name)]++;
            }
        }


		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		$data['status'] = 'All';
        $data['pcosStatistics'] = $pcosStatistics;

		return view('projects.pcos.index', $data);
	}

	/**
	 * Pcos filter
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @return \Illuminate\View\View
	 * @internal param RfisRepository $submittalsRepository
	 */
	public function filter($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort');
		$order = Input::get('order');

		//get filter parameters
		$masterFormat = Input::get('master_format_search');
		$recipient = Input::get('sub_id');
        $name = Input::get('name');
		$status = Input::get('status');

        $statuses = $pcosRepository->getAllPcoStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        $pcosStatistics = [];
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
            $pcosStatistics[$versionStatus->name] = 0;
        }

		//Set filter type
		$type = 'pcos';

		//implement pcos filter
		$data['pcos'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'recipient', 'name', 'status', 'projectId', 'sort' , 'order'));
		$data['project'] = $projectRepository->getProject($projectId);

        foreach ($data['pcos'] as $pcoKey => $pco){
            if (isset($pcosStatistics[trim($pco->version_status_name)])) {
                $pcosStatistics[trim($pco->version_status_name)]++;
            }
        }

		$data['status'] = $status;
        $data['pcosStatistics'] = $pcosStatistics;

		return view('projects.pcos.index', $data);
	}

	/**
	 * Generate pdf report from filtered or all listed pcos
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param ReportGenerator $generator
	 * @param PcosRepository $pcosRepository
	 * @return mixed
	 * @throws \Exception
	 * @internal param RfisRepository $rfisRepository
	 */
	public function pdfReport($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, PcosRepository $pcosRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort');
		$order = Input::get('order');

		//get filter parameters
		$masterFormat = Input::get('report_mf');
		$recipient = Input::get('report_sub_id');
		$recipientName = Input::get('report_recipient_name');
		$statusId = Input::get('report_status');
		$data['printWithNotes'] = Input::get('print_with_notes');

		//get pco version status by id
		$status = ($statusId == 'All') ? 'All' : $statusId;

		//Set report type
		$type = 'pcos-report';

		//Get pcos
		$data['pcos'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'recipient', 'status', 'projectId', 'sort', 'order'));

		//Get project
		$data['project'] = $projectRepository->getProject($projectId);

		//Set report view
		$view = 'projects.pcos.report';

		//Set report title
		$data['mf_title'] = "";
		$data['sub_title'] = "";
		$data['status_title'] = "";

		//Create full report title
		if ($masterFormat != '') {
			$data['mf_title'] = ' Master Format Number or Title: '.$masterFormat.", ";
		}
		if ($recipientName != '') {
			$data['sub_title'] = 'Recipient: '.$recipientName.", ";
		}

		//get status from database table
		if ($statusId == 'All') {
			$statusString = 'All';
		} else if ($statusId == 'all_open') {
			$statusString = 'All Open';
		} else {
			$statusRow = $pcosRepository->getStatusById($statusId);
			$statusString = $statusRow->name;
		}


		$data['status_title'] = 'Status: '.$statusString;

		//Set report name
		$name = "PCO's";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $projectId
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 * @internal param PcosRepository $rfisRepository
	 */
	public function create($projectId, ProjectRepository $projectRepository, PcosRepository $pcosRepository,
                           ProjectInterface $projectModule, AddressBookRepository $addressBookRepository)
	{
		$data['project'] = $projectRepository->getProject($projectId);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);
        $numbers = $pcosRepository->getGeneratedNumbers($projectId);
        $data['project']['generated_number'] = $projectModule->generateNumber($numbers, $data['project']['pco_counter']);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $pcosRepository->getAllPcoStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['reasonsForChange'] = ['Design Change', 'Drawing/Specification Omission', 'Owner Request',
            'Contractor Substitution', 'Code Compliance', 'Unforeseen conditions'];

		return view('projects.pcos.create', $data);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param $projectId
     * @param PcosRequest $pcosRequest
     * @param PcosRepository $pcosRepository
     * @return Response
     */
	public function store($projectId, PcosRequest $pcosRequest, PcosRepository $pcosRepository)
	{
		$response = $pcosRepository->storeProjectPco($projectId, $pcosRequest->all());

		if ($response) {
			//Successfully created rfi
			Flash::success(trans('messages.submittals.store.success', ['item' => 'PCO']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'PCO']));
		return Redirect::to('projects/'.$projectId.'/pcos');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $pcoId
     * @param ProjectRepository $projectRepository
     * @param PcosRepository $pcosRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return Response
     * @internal param int $id
     */
	public function edit($projectId, $pcoId, ProjectRepository $projectRepository, PcosRepository $pcosRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		$data['project'] = $projectRepository->getProject($projectId);
		$data['pco'] = $pcosRepository->getPco($pcoId);
		foreach ($data['pco']['subcontractors'] as $key => $subcontractor){
            $data['pco']['subcontractors'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($subcontractor, Config::get('constants.subcontractor_versions'));
        }
		$recipient = $checkUploadedFiles->checkAndOrderUploadedFiles($data['pco']['recipient'], Config::get('constants.recipient_versions'));
        $data['pco']->setRelation('recipient', $recipient);

        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['reasonsForChange'] = ['Design Change', 'Drawing/Specification Omission', 'Owner Request',
            'Contractor Substitution', 'Code Compliance', 'Unforeseen conditions'];
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $data['page'] = 'pcos-tasks';
        $subcontractorTaskIds = $this->tasksRepository->getAllTasksSubcontructor();
        $data['subcontractorTaskIds'] = array_column($subcontractorTaskIds, 'task_id');
        $data['userCanWrite'] = $this->permissionsRepository->userHasAtLeastOneWritePermission(12);
        $data['userCanDelete'] = $this->permissionsRepository->userHasAtLeastOneDeletePermission(12);
        $data['activeInnerTab'] = 'tasks';
        $data['tasks'] = $this->tasksRepository->getAllProjectTasksByPco($pcoId, $orderBy, $orderDir);

		return view('projects.pcos.edit', $data);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param PcosRequest $request
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @return Response
     * @internal param $rfiId
     * @internal param int $id
     */
	public function update($projectId, $pcoId, PcosRequest $request, PcosRepository $pcosRepository,
                           ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository)
	{
		//Update pco
		$pco = $pcosRepository->updatePco($pcoId, $request->all());

		if ($pco) {
			//Successfully updated pco
			Flash::success(trans('messages.submittals.update.success', ['item' => 'PCO']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'PCO']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 */
	public function destroy($projectId, $pcoId, PcosRepository $pcosRepository)
	{
        $idsArray = explode('-', $pcoId);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            //select pco versions for file deleting
            $pcoSubcontractor = $pcosRepository->getPco($id);

            foreach ($pcoSubcontractor->subcontractors as $subcontractor) {
                foreach ($subcontractor->subcontractor_versions as $pcoVersion) {
                    foreach ($pcoVersion->files as $versionFile) {
                        //delete pco version files
                        $pcosRepository->deletePcoFile($projectId, $versionFile->file->file_name);
                    }
                }
            }

            //delete pco with all its subcontractors, recipient and its versions and files from database
            $response = $pcosRepository->deletePco($id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

		//Successfully deleted pco
		if (!$errorEntry) {
			Flash::success(trans('messages.submittals.delete.success', ['item' => 'PCO']));
		} else {
            //Not successful deleting
		    Flash::error(trans('messages.submittals.delete.error', ['item' => 'PCO']));
        }

		return Redirect::to('projects/'.$projectId.'/pcos');
	}

	/**
	 * Get all shares of the pcos's for all shared companies
	 * @param $projectId
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @return \Illuminate\View\View
	 * @internal param SubmittalsRepository $submittalsRepository
	 */
	public function shares($projectId, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
	{
		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		//get all companies with which the logged company have shared some of their uploaded project files
		$data['sharedPcos'] = $pcosRepository->getSharedPcos($projectId, Config::get('constants.company_type.creator'), $data['project']->comp_id);

		return view('projects.pcos.pcos-shares', $data);
	}

	/**
	 * Unshare specified pco with the specific company
	 * @param $projectId
	 * @param $pcoIds
	 * @param PcosRepository $pcosRepository
	 * @return mixed
	 */
	public function unshare($projectId, $pcoIds, PcosRepository $pcosRepository)
	{
        $idsArray = explode('-', $pcoIds);

        $errorEntry = false;
        foreach ($idsArray as $ids) {

            $jointIds = explode("|", $ids);

            $response = $pcosRepository->unsharePco($jointIds[0], $jointIds[1], $jointIds[2]);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        if(!$errorEntry) {
            //Successfully unshared project submittal
            Flash::success(trans('messages.submittals.unshare.success'));
        } else {
            //File unsharing was not successful
            Flash::error(trans('messages.submittals.unshare.error'));
        }

        //projects/15/submittals/shares
        return Redirect::to(URL('projects/'.$projectId.'/pcos/shares'));
	}

	public function generateSubcontractorTransmittal($projectId, TransmittalsWrapper $wrapper, PcosRepository $pcosRepository)
	{
		$pcoId = Input::get('pco_id');
		$versionId = Input::get('version_id');
		$subcontractorRecipientId = Input::get('subcontractor_recipient_id');

		//get pco version with the proper pco
		$data['pcoVersion'] = $pcosRepository->getSubcontractorVersionForTransmittal($versionId);

		//get pco name
		if (!is_null($data['pcoVersion']->subcontractor)) {
			$pcoName = $data['pcoVersion']->subcontractor->pco->name;
		} else {
			$pcoName = $data['pcoVersion']->recipient->pco->name;
		}

		//Set report type
		$type = Config::get('constants.transmittal_type.pco');

		//Set report view
		$view = Config::get('constants.transmittal_view.pco_subcontractor');

		//return generated pdf
		return $wrapper->wrap(ClassMap::instance(Config::get('classmap.transmittals.'.$type)), $view, $data, compact('pcoName', 'versionId'));
	}

	public function generateRecipientTransmittal($projectId, TransmittalsWrapper $wrapper, PcosRepository $pcosRepository)
	{
		$pcoId = Input::get('pco_id');
		$versionId = Input::get('version_id');
		$subcontractorRecipientId = Input::get('subcontractor_recipient_id');

		//get pco version with the proper pco
		$data['pcoVersion'] = $pcosRepository->getRecipientVersionForTransmittal($versionId);

		//get pco name
		if (!is_null($data['pcoVersion']->subcontractor)) {
			$pcoName = $data['pcoVersion']->subcontractor->pco->name;
		} else {
			$pcoName = $data['pcoVersion']->recipient->pco->name;
		}

		//Set report type
		$type = Config::get('constants.transmittal_type.pco');

		//Set report view
		$view = Config::get('constants.transmittal_view.pco_recipient');

		//return generated pdf
		return $wrapper->wrap(ClassMap::instance(Config::get('classmap.transmittals.'.$type)), $view, $data, compact('pcoName', 'versionId'));
	}

	public function getTransmittals($projectId, PcosRepository $pcosRepository, ProjectRepository $projectRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','transmittals_logs.id');
		$order = Input::get('order','asc');

		//get all pco versions statuses and prepare them to array for the view
		$statuses = $pcosRepository->getAllPcoStatuses();
		$data['statuses']['All'] = 'All';
		$data['statuses']['all_open'] = 'All Open';
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

		$data['project'] = $projectRepository->getProject($projectId);

		//Get version transmittals
		$data['transmittals'] = $pcosRepository->getTransmittals($projectId, $sort, $order);

		$data['status'] = 'All';

		return view('projects.pcos.transmittals-log', $data);
	}

	public function filterTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
	{
		//get filter parameters
		$masterFormat = Input::get('master_format_search');
		$sentTo = Input::get('company_id');
		$status = Input::get('status');

		//Set filter type
		$type = 'pco-transmittals-filter';

		//implement rfis filter
		$data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'sentTo', 'status', 'projectId'));
		$data['project'] = $projectRepository->getProject($projectId);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $pcosRepository->getAllPcoStatuses();
		$data['statuses']['All'] = 'All';
		$data['statuses']['all_open'] = 'All Open';
		foreach ($statuses as $versionStatus) {
			$data['statuses'][$versionStatus->id] = $versionStatus->name;
		}

		$data['status'] = $status;

		return view('projects.pcos.transmittals-log', $data);

	}

	/**
	 * Get list of transmittals as a report
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @param ReportGenerator $generator
	 * @return mixed
	 * @throws \Exception
	 */
	public function reportTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, PcosRepository $pcosRepository, ReportGenerator $generator)
	{
		//get filter parameters
		$sort = Input::get('report_sort');
		$order = Input::get('report_order');
		$masterFormat = Input::get('report_mf');
		$sentTo = Input::get('report_comp_id');
		$sentToName = Input::get('report_sent_to');
		$statusId = Input::get('report_status');

		//get pco version status by id
		$status = ($statusId == 'All') ? 'All' : $statusId;

		//Set report type
		$type = 'pco-transmittals';

		//Get pcos
		$data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('sort', 'order', 'masterFormat', 'sentTo', 'status', 'projectId'));

		//Get project
		$data['project'] = $projectRepository->getProject($projectId);

		//Set report view
		$view = 'projects.pcos.transmittals-report';

		//Set report title
		$data['title'] = '';

		//Create full report title
		if ($masterFormat != '') {
			$data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
		}
		if ($sentToName != '') {
			$data['title'] = $data['title'].'Recipient: '.$sentToName.", ";
		}

		//get status from database table
		if ($statusId == 'All') {
			$statusString = 'All';
		} else if ($statusId == 'all_open') {
			$statusString = 'All Open';
		} else {
			$statusRow = $pcosRepository->getStatusById($statusId);
			$statusString = $statusRow->name;
		}

		$data['title'] = $data['title'].'Status: '.$statusString;

		//Set report name
		$name = "Transmittal_Log_PCO's";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

	public function deleteTransmittal($projectId, $transmittalId, PcosRepository $pcosRepository)
	{
        $idsArray = explode('-', $transmittalId);

        foreach ($idsArray as $id) {
            $transmittal = $pcosRepository->getTransmittal($id);

            $pcosRepository->deleteTransmittalFromS3($projectId, $transmittal->file_id);

            $pcosRepository->deleteTransmittal($id);
        }

		//Successfully deleted submittal version
		Flash::success(trans('messages.submittals.delete.success', ['item' => 'Transmittal']));
		return Redirect::to('projects/'.$projectId.'/pcos/transmittals');
	}

}
