<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateFreeAccountRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Registration\Implementations\WrapRegistration;
use App\Modules\Subscription_levels\Repositories\SubscriptionLevelsRepository;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Input;

use App\Modules\List_companies_users\Repositories\ListCompaniesUsersRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ListCompaniesUsers extends Controller {
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    /**
     * ListCompaniesUsers constructor.
     * @param CompaniesRepository $companiesRepository
     */
    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->middleware('auth');
        $this->middleware('only_super_admin');
        $this->companiesRepository = $companiesRepository;
    }

    /**
     * List all registered companies
     * @param ListCompaniesUsersRepository $repository
     * @param SubscriptionLevelsRepository $subscriptionLevelsRepository
     * @return \Illuminate\View\View
     */
    public function index(ListCompaniesUsersRepository $repository, SubscriptionLevelsRepository $subscriptionLevelsRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort','id');
        $order = Input::get('order','desc');

        $registeredCompanies = $repository->getAllRegisteredCompanies($sort, $order);
        $subscriptionLevels = $subscriptionLevelsRepository->getSubscriptionLevels();

        //preparing subscription levels for filter select field
        $subscriptionsArr = [
            ''=>''
        ];
        foreach ($subscriptionLevels as $subscription) {
            $subscriptionsArr[$subscription->id] = $subscription->name;
        }

        return view('list_companies_users.index', compact('subscriptionsArr','registeredCompanies'));
    }

    /**
     * Filter companies
     * @param ListCompaniesUsersRepository $repository
     * @param SubscriptionLevelsRepository $subscriptionLevelsRepository
     * @return \Illuminate\View\View
     */
    public function filter(ListCompaniesUsersRepository $repository, SubscriptionLevelsRepository $subscriptionLevelsRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort');
        $order = Input::get('order');

        $status = Input::get('status');
        $accountType = Input::get('account_type');
        $subscriptionLevel = Input::get('subscription_level');

        $subscriptionLevels = $subscriptionLevelsRepository->getSubscriptionLevels();

        //preparing subscription levels for filter select field
        $subscriptionsArr = [
            ''=>''
        ];
        foreach ($subscriptionLevels as $subscription) {
            $subscriptionsArr[$subscription->id] = $subscription->name;
        }

        $registeredCompanies = $repository->filterCompanies($status, $accountType, $subscriptionLevel, $subscriptionLevels, $sort, $order);

        return view('list_companies_users.index', compact('subscriptionsArr','registeredCompanies'));
    }

    /**
     * Create new free account
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('list_companies_users.create');
    }

    /**
     * Store new free account
     * @param CreateFreeAccountRequest $request
     * @return mixed
     * @throws \Exception
     */
    public function store(CreateFreeAccountRequest $request)
    {
        $sub_type = $request->input('subscription_type');
        $data = $request->all();
        $data['account_type'] = Config::get('subscription_levels.account_type.free');

        //implement different subscription level type registration
        $wrapReg = new WrapRegistration();
        $result = $wrapReg->doRegister(ClassMap::instance(Config::get('classmap.registration.'.$sub_type)), $data);

        if (!is_null($result)) {
            //Successfully created account
            Flash::success(trans('messages.free_account.success'));
            return Redirect::to('list-companies');
        }

        Flash::error(trans('messages.free_account.error'));
        return Redirect::to('list-companies');
    }

    public function resetPoints($companyId, ListCompaniesUsersRepository $repository)
    {
        $company = $repository->getCompany($companyId);

        if ($company->subs_id == Config::get('subscription_levels.sales-agent')) {
            $response = $repository->resetCompanyReferralPoints($companyId);

            if ($response) {
                //return an error message
                Flash::success(trans('messages.free_account.points_reset_success'));
                return Redirect::to('list-companies');
            }
        }

        //return an error message
        Flash::error(trans('messages.free_account.not_a_free_account'));
        return Redirect::to('list-companies');
    }

    /**
     * View concrete company and list their users
     * @param $companyId
     * @param ListCompaniesUsersRepository $repository
     * @return \Illuminate\View\View
     */
    public function viewCompany($companyId, ListCompaniesUsersRepository $repository)
    {
        $data['company'] = $repository->getCompany($companyId);
        $data['company']->subscription_expire_date = !empty($data['company']->subscription_expire_date)?Carbon::parse($data['company']->subscription_expire_date)->toFormattedDateString():'--';
        $data['company']->subscription_payment_date = !empty($data['company']->subscription_payment_date)?Carbon::parse($data['company']->subscription_payment_date)->toFormattedDateString():'--';
        $data['created_at'] = Carbon::parse($data['company']['created_at'])->toFormattedDateString();
        return view('list_companies_users.view-company', $data);
    }

    /**
     * View concrete user
     * @param $companyId
     * @param $userId
     * @param ListCompaniesUsersRepository $repository
     * @return \Illuminate\View\View
     */
    public function viewUsers($companyId, $userId, ListCompaniesUsersRepository $repository)
    {
        $data['user'] = $repository->getCompanyUser($userId);
        $data['created_at'] = Carbon::parse($data['user']['created_at'])->toFormattedDateString();
        return view('list_companies_users.view-user', $data);
    }

    public function logs(ListCompaniesUsersRepository $repository)
    {
        $sort = Input::get('sort','ID');
        $order = Input::get('order','asc');
        $data['logs'] = $repository->getAllLogs($sort, $order);
        return view('list_companies_users.logs', $data);

    }
    public function viewAddresses($companyId, ListCompaniesUsersRepository $repository)
    {
        $data['company'] = $repository->getCompany($companyId);
        $data['company']->subscription_expire_date = Carbon::parse($data['company']->subscription_expire_date)->toFormattedDateString();
        $data['company']->subscription_payment_date = Carbon::parse($data['company']->subscription_payment_date)->toFormattedDateString();
        $data['created_at'] = Carbon::parse($data['company']['created_at'])->toFormattedDateString();
        return view('list_companies_users.view-addresses', $data);
    }

    /**
     * Get free account expire date
     * @param ListCompaniesUsersRepository $repository
     * @return mixed
     */
    public function getExpireDate(ListCompaniesUsersRepository $repository)
    {
        $companyId = Input::get('companyId');

        //get company
        $company = $repository->getCompany($companyId);

        if ($company) {
            return Response::json(['success' => $company, 200]);
        }
        return Response::json(['error' => $company, 400]);
    }

    /**
     * Save expire date for a specific company account
     * @param ListCompaniesUsersRepository $repository
     * @return mixed
     */
    public function saveExpireDate(ListCompaniesUsersRepository $repository)
    {
        $companyId = Input::get('companyId');
        $expireDate = Input::get('expireDate');

        //update expiry date
        $response = $repository->updateCompanyExpireDate($companyId, $expireDate);

        //if date field is empty
        if (is_null($response)) {
            return Response::json(['error' => 'Please select an expiry date', 400]);
        }

        //if update is successful
        if ($response) {
            return Response::json(['success' => $response, 200]);
        }

        //if update didn't happen
        return Response::json(['error' => 'Something went wrong! Please try again.', 400]);
    }

    /**
     * Save subscription type for a specific company account
     * @param ListCompaniesUsersRepository $repository
     * @return mixed
     */
    public function saveSubscriptionType(ListCompaniesUsersRepository $repository)
    {
        $companyId = Input::get('companyId');
        $subscriptionType = Input::get('subscriptionType');

        //update subscription type
        $response = $repository->updateCompanySubscriptionType($companyId, $subscriptionType);

        //if date field is empty
        if (is_null($response)) {
            return Response::json(['error' => 'Please select an subscription type', 400]);
        }

        //if update is successful
        if ($response) {
            return Response::json(['success' => $response, 200]);
        }

        //if update didn't happen
        return Response::json(['error' => 'Something went wrong! Please try again.', 400]);
    }

    public function destroy($ids)
    {
        $idsArray = explode('-', $ids);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $response = $this->companiesRepository->deleteCompany($id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }



        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'company']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'company']));
        }

        return redirect()->action('ListCompaniesUsers@index');
    }

}
