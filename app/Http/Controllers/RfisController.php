<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\EditRfiRequest;
use App\Http\Requests\RfiRequest;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Implementations\CheckUploadedFiles;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Project\Implementations\Project;
use App\Modules\Project\Interfaces\ProjectInterface;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Tasks\Repositories\TasksRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class RfisController extends Controller {

    const ENTITY_ID = 9;

    /**
     * @var TasksRepository
     */
    private $tasksRepository;
    /**
     * @var ProjectPermissionsRepository
     */
    private $permissionsRepository;

    public function __construct(TasksRepository $tasksRepository, ProjectPermissionsRepository $permissionsRepository)
	{
		//Middleware that check if the company has Level 0 subscription
		//Level 0 companies should have access only to the master files of the shared projects
		$this->middleware('level_zero_permit');

		$this->middleware('subscription_level_one');
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->tasksRepository = $tasksRepository;
        $this->permissionsRepository = $permissionsRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param $projectId
     * @param RfisRepository $rfisRepository
     * @param ProjectRepository $projectRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return Response
     */
	public function index($projectId, RfisRepository $rfisRepository, ProjectRepository $projectRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','number');
		$order = Input::get('order','asc');

        //get all rfi versions statuses and prepare them to array for the view
        $statuses = $rfisRepository->getAllRfiStatuses();
        $rfisStatistics = [];
        $data['statuses']['All'] = 'All';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
            $rfisStatistics[$status->name] = 0;
        }

        //get all project rfis
		$data['rfis'] = $rfisRepository->getProjectRfis($projectId, $sort, $order);

        foreach ($data['rfis'] as $key => $rfis){
            $data['rfis'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($rfis, Config::get('constants.rfis_versions'));

            $rfisStatistics[$rfis->version_status_name]++;
        }


		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		$data['status'] = 'All';
        $data['rfisStatistics'] = $rfisStatistics;

		return view('projects.rfis.index', $data);
	}

	/**
	 * Generate pdf report from filtered or all listed rfis
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param ReportGenerator $generator
	 * @param RfisRepository $rfisRepository
	 * @return mixed
	 */
	public function pdfReport($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, RfisRepository $rfisRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort');
		$order = Input::get('order');

		//get filter parameters
		$masterFormat = Input::get('report_mf');
		$subcontractor = Input::get('report_sub_id');
		$subcontractorName = Input::get('report_sub_name');
		$statusId = Input::get('report_status');
		$data['printWithNotes'] = Input::get('print_with_notes');

		//get rfi version status by id
		$status = ($statusId == 'All') ? 'All' : $statusId;

		//Set report type
		$type = 'rfis-report';

		//Get rfis
		$data['rfis'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

		//Get project
		$data['project'] = $projectRepository->getProject($projectId);

		//Set report view
		$view = 'projects.rfis.report';

		//Set report title
		$data['mf_title'] = "";
		$data['sub_title'] = "";
		$data['status_title'] = "";

		//Create full report title
		if ($masterFormat != '') {
			$data['mf_title'] = 'Master Format Number or Title: '.$masterFormat.", ";
		}
		if ($subcontractorName != '') {
			$data['sub_title'] = 'Subcontractor: '.$subcontractorName.", ";
		}

		//get status from database table
		if ($statusId == 'All') {
			$statusString = 'All';
		}
		else {
			$statusRow = $rfisRepository->getStatusById($statusId);
			$statusString = $statusRow->name;
		}


		$data['status_title'] = 'Status: '.$statusString;

		//Set report name
		$name = "RFI's";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

	/**
	 * Rfis filter
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param RfisRepository $rfisRepository
	 * @return \Illuminate\View\View
	 * @throws \Exception
	 * @internal param RfisRepository $submittalsRepository
	 */
	public function filter($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, RfisRepository $rfisRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		//default sort and order unless selected
		$sort = Input::get('sort');
		$order = Input::get('order');

		//get filter parameters
        $statuses = $rfisRepository->getAllRfiStatuses();
        $data['statuses']['All'] = 'All';
        $rfisStatistics = [];
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
            $rfisStatistics[$versionStatus->name] = 0;
        }

		$masterFormat = Input::get('master_format_search');
		$subcontractor = Input::get('sub_id');
        $name = Input::get('name');
		$status = Input::get('status');

		//Set filter type
		$type = 'rfis';

		//implement rfis filter
		$data['rfis'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'subcontractor', 'name', 'status', 'projectId', 'sort', 'order'));

        foreach ($data['rfis'] as $key => $rfis){
            $data['rfis'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($rfis, Config::get('constants.rfis_versions'));

            $rfisStatistics[$rfis->version_status_name]++;
        }

		$data['project'] = $projectRepository->getProject($projectId);

		$data['status'] = $status;
        $data['rfisStatistics'] = $rfisStatistics;

		return view('projects.rfis.index', $data);
	}

    /**
     * Create new rfi form
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param RfisRepository $rfisRepository
     * @param ProjectInterface $projectModule
     * @param AddressBookRepository $addressBookRepository
     * @return \Illuminate\View\View
     */
	public function create($projectId, ProjectRepository $projectRepository, RfisRepository $rfisRepository,
                           ProjectInterface $projectModule, AddressBookRepository $addressBookRepository)
	{
		$data['project'] = $projectRepository->getProject($projectId);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);
        $numbers = $rfisRepository->getGeneratedNumbers($projectId);
        $data['project']['generated_number'] = $projectModule->generateNumber($numbers, $data['project']['rfi_counter']);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $rfisRepository->getAllRfiStatuses();
		$data['statuses']= [];
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['rfiChange'] = ['Yes', 'No'];

		return view('projects.rfis.create', $data);
	}

    /**
     * Store new rfi
     * @param $projectId
     * @param RfiRequest $request
     * @param RfisRepository $rfisRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
	public function store($projectId, RfiRequest $request, RfisRepository $rfisRepository,
                          ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                          ProjectPermissionsRepository $projectPermissionsRepo,
                          ProjectRepository $projectRepository)
	{
		//Store project rfi

        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor))?$projectCompanyPermissionsSubcontractor->write:0;
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient))?$projectCompanyPermissionsRecipient->write:0;


        $response = $rfisRepository->storeProjectRfi($projectId, $dataInput);

		if ($response) {
            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.rfis'),
                'moduleId' => $response->id,
                'name' => $response->name
            ];

            if(!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //if there is no default user assigned to the project we need to send the email again
            if(!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully created rfi
			Flash::success(trans('messages.submittals.store.success', ['item' => 'RFI']));
			return Redirect::to('projects/'.$projectId.'/rfis/'.$response->id.'/version/'.$response->last_version.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'RFI']));
		return Redirect::to('projects/'.$projectId.'/rfis');
	}

    /**
     * Edit rfi form
     * @param $projectId
     * @param $rfiId
     * @param ProjectRepository $projectRepository
     * @param RfisRepository $rfisRepository
     * @param ContactsRepository $contactsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @param AddressBookRepository $addressBookRepository
     * @return \Illuminate\View\View
     * @internal param RfisRepository $submittalsRepository
     * @internal param $submittalId
     * @internal param RfisRepository $rfisRepository
     */
	public function edit($projectId, $rfiId, ProjectRepository $projectRepository, RfisRepository $rfisRepository, ContactsRepository $contactsRepository,
                         CheckUploadedFilesInterface $checkUploadedFiles, AddressBookRepository $addressBookRepository)
	{
		$data['project'] = $projectRepository->getProject($projectId);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['rfi'] = $rfisRepository->getRfi($rfiId);
		$data['rfi'] = $checkUploadedFiles->checkAndOrderUploadedFiles($data['rfi'], Config::get('constants.rfis_versions'));

		$data['projectSubcontractorContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['rfi']->sub_id);
		$data['projectRecipientContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['rfi']->recipient_id);
        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['rfiChange'] = ['Yes', 'No'];
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $data['page'] = 'rfis-tasks';
        $subcontractorTaskIds = $this->tasksRepository->getAllTasksSubcontructor();
        $data['subcontractorTaskIds'] = array_column($subcontractorTaskIds, 'task_id');
        $data['userCanWrite'] = $this->permissionsRepository->userHasAtLeastOneWritePermission(12);
        $data['userCanDelete'] = $this->permissionsRepository->userHasAtLeastOneDeletePermission(12);
        $data['activeInnerTab'] = 'tasks';
        $data['tasks'] = $this->tasksRepository->getAllProjectTasksByRfi($rfiId, $orderBy, $orderDir);

		return view('projects.rfis.edit', $data);
	}

    /**
     * Update rfi
     * @param $projectId
     * @param $rfiId
     * @param EditRfiRequest $request
     * @param RfisRepository $rfisRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @return mixed
     */
	public function update($projectId, $rfiId, EditRfiRequest $request, RfisRepository $rfisRepository,
                           ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                           ProjectPermissionsRepository $projectPermissionsRepo, ProjectRepository $projectRepository)
	{
        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

		//Update rfi
        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor))?$projectCompanyPermissionsSubcontractor->write:0;
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient))?$projectCompanyPermissionsRecipient->write:0;
		$rfi = $rfisRepository->updateRfi($rfiId, $dataInput);

		if ($rfi) {
            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.rfis'),
                'moduleId' => $rfiId,
                'name' => $rfi->name
            ];

            if(!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //if there is no default user assigned to the project we need to send the email again
            if(!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully updated rfi
			Flash::success(trans('messages.submittals.update.success', ['item' => 'RFI']));
			return Redirect::to('projects/'.$projectId.'/rfis/'.$rfiId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'RFI']));
		return Redirect::to('projects/'.$projectId.'/rfis/'.$rfiId.'/edit');
	}

	/**
	 * Delete rfi
	 * @param $projectId
	 * @param $rfiId
	 * @param RfisRepository $rfisRepository
	 * @return mixed
	 */
	public function destroy($projectId, $rfiId, RfisRepository $rfisRepository)
	{
        $idsArray = explode('-', $rfiId);

        $errorEntry = false;
        foreach ($idsArray as $id) {

            //select rfi versions for file deleting
            $rfiVersions = $rfisRepository->getRfiVersions($id);

            //delete rfi versions files from s3
            foreach ($rfiVersions as $rfiVersion) {
                foreach ($rfiVersion->file as $versionFile) {
                    //delete submittal versions files
                    $rfisRepository->deleteRfiFile($projectId, $versionFile->file_name);
                }
            }

            //delete rfis and its versions from database
            $response = $rfisRepository->deleteRfi($id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

		//Successfully deleted rfi
		if (!$errorEntry) {
			Flash::success(trans('messages.submittals.delete.success', ['item' => 'RFI']));
			return Redirect::to('projects/'.$projectId.'/rfis');
		}

		//Not successful deleting
		Flash::error(trans('messages.submittals.delete.error', ['item' => 'RFI']));
		return Redirect::to('projects/'.$projectId.'/rfis');
	}

	/**
	 * Get all shares of the rfi's for all shared companies
	 * @param $projectId
	 * @param ProjectRepository $projectRepository
	 * @param RfisRepository $rfisRepository
	 * @return \Illuminate\View\View
	 * @internal param SubmittalsRepository $submittalsRepository
	 */
	public function shares($projectId, ProjectRepository $projectRepository, RfisRepository $rfisRepository)
	{
		//get project
		$data['project'] = $projectRepository->getProject($projectId);

		//get all companies with which the logged company have shared some of their uploaded project files
		$data['sharedRfis'] = $rfisRepository->getSharedRfis($projectId, Config::get('constants.company_type.creator'), $data['project']->comp_id);

		return view('projects.rfis.rfis-shares', $data);
	}

	/**
	 * Unshare specified rfi with the specific company
	 * @param $projectId
	 * @param $rfiId
	 * @param $shareCompanyId
	 * @param $receiveCompanyId
	 * @param RfisRepository $rfisRepository
	 * @return mixed
	 * @internal param $submittalId
	 * @internal param SubmittalsRepository $submittalsRepository
	 * @internal param $fileId
	 * @internal param ProjectFilesRepository $filesRepository
	 */
	public function unshare($projectId, $rfiIds, RfisRepository $rfisRepository)
	{
        $idsArray = explode('-', $rfiIds);

        $errorEntry = false;
        foreach ($idsArray as $ids) {

            $jointIds = explode("|", $ids);

            $response = $rfisRepository->unshareRfi($jointIds[0], $jointIds[1], $jointIds[2]);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        if(!$errorEntry) {
            //Successfully unshared project submittal
            Flash::success(trans('messages.submittals.unshare.success'));
        } else {
            //File unsharing was not successful
            Flash::error(trans('messages.submittals.unshare.error'));
        }

        //projects/15/submittals/shares
        return Redirect::to(URL('projects/'.$projectId.'/rfis/shares'));
	}

	public function getTransmittals($projectId, RfisRepository $rfisRepository, ProjectRepository $projectRepository)
	{
		//default sort and order unless selected
		$sort = Input::get('sort','transmittals_logs.id');
		$order = Input::get('order','asc');

		$data['project'] = $projectRepository->getProject($projectId);

		//Get version transmittals
		$data['transmittals'] = $rfisRepository->getTransmittals($projectId, $sort, $order);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $rfisRepository->getAllRfiStatuses();
		$data['statuses']['All'] = 'All';
		foreach ($statuses as $status) {
			$data['statuses'][$status->id] = $status->name;
		}

		$data['status'] = 'All';

		return view('projects.rfis.transmittals-log', $data);
	}

	public function filterTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, RfisRepository $rfisRepository)
	{
		//get filter parameters
		$masterFormat = Input::get('master_format_search');
		$sentTo = Input::get('company_id');
		$status = Input::get('status');

		//Set filter type
		$type = 'rfi-transmittals-filter';

		//implement rfis filter
		$data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'sentTo', 'status', 'projectId'));
		$data['project'] = $projectRepository->getProject($projectId);

		//get all rfi versions statuses and prepare them to array for the view
		$statuses = $rfisRepository->getAllRfiStatuses();
		$data['statuses']['All'] = 'All';
		foreach ($statuses as $versionStatus) {
			$data['statuses'][$versionStatus->id] = $versionStatus->name;
		}

		$data['status'] = $status;

		return view('projects.rfis.transmittals-log', $data);

	}

	/**
	 * Get list of transmittals as a report
	 * @param $projectId
	 * @param FilterDataWrapper $wrapper
	 * @param ProjectRepository $projectRepository
	 * @param RfisRepository $rfisRepository
	 * @param ReportGenerator $generator
	 * @return mixed
	 * @throws \Exception
	 */
	public function reportTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, RfisRepository $rfisRepository, ReportGenerator $generator)
	{
		//get filter parameters
		$sort = Input::get('report_sort');
		$order = Input::get('report_order');
		$masterFormat = Input::get('report_mf');
		$sentTo = Input::get('report_comp_id');
		$sentToName = Input::get('report_sent_to');
		$statusId = Input::get('report_status');

		//get rfi version status by id
		$status = ($statusId == 'All') ? 'All' : $statusId;

		//Set report type
		$type = Config::get('constants.rfi_transmittals');

		//Get rfis
		$data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('sort', 'order', 'masterFormat', 'sentTo', 'status', 'projectId'));

		//Get project
		$data['project'] = $projectRepository->getProject($projectId);

		//Set report view
		$view = 'projects.rfis.transmittals-report';

		//Set report title
		$data['title'] = '';

		//Create full report title
		if ($masterFormat != '') {
			$data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
		}
		if ($sentToName != '') {
			$data['title'] = $data['title'].'Subcontractor: '.$sentToName.", ";
		}

		//get status from database table
		if ($statusId == 'All') {
			$statusString = 'All';
		}
		else {
			$statusRow = $rfisRepository->getStatusById($statusId);
			$statusString = $statusRow->name;
		}


		$data['title'] = $data['title'].'Status: '.$statusString;

		//Set report name
		$name = "Transmittal_Log_RFI's";

		//Return generated report
		return $generator->generateReport($type, $data, $view, $name);
	}

	public function deleteTransmittal($projectId, $transmittalId, RfisRepository $rfisRepository)
	{

        $idsArray = explode('-', $transmittalId);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $transmittal = $rfisRepository->getTransmittal($id);

            $response = $rfisRepository->deleteTransmittalFromS3($projectId, $transmittal->file_id);

            if ($response) {
                $rfisRepository->deleteTransmittal($id);

                $errorEntry = true;
            }
        }

		if (!$errorEntry) {
			//Successfully deleted submittal version
			Flash::success(trans('messages.submittals.delete.success', ['item' => 'Transmittal']));
			return Redirect::to('projects/'.$projectId.'/rfis/transmittals');
		}

		//Something went wrong
		Flash::success(trans('messages.submittals.delete.error', ['item' => 'Transmittal']));
		return Redirect::to('projects/'.$projectId.'/rfis/transmittals');
	}

}
