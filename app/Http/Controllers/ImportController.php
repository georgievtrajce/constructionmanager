<?php
namespace App\Http\Controllers;

use App\Http\Requests\FilesRequest;
use App\Http\Requests\MasterFormatRequest;
use App\Modules\Import\ImportFile;
use App\Modules\Import\ImportGoogleCSV;
use App\Modules\Import\ImportOutlookCSV;
use Request;
use Input;
use Excel;
use Flash;
/**
 * Created by PhpStorm.
 * User: kristina.stojchikj
 * Date: 3/3/2015
 * Time: 6:56 PM
 */

class ImportController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders your application's "dashboard" for users that
    | are authenticated. Of course, you are free to change or remove the
    | controller as you wish. It is just here to get your app started!
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the application dashboard to the user.
     * @param $type
     * @return \Illuminate\View\View
     */
    public function index($type)
    {
        $data['type'] = $type;
        return view('address_book.import-csv', $data);
    }

    /**
     * see what kind of file is uploaded and proceed to implementation
     * @param $type
     * @param FilesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function insert($type, FilesRequest $request)
    {

        $data = array();
        $data['notify'] = '';
        $outlookCsv = $request->file('outlook');
        $googleCsv = $request->file('google');

        //show message if no file
        if ((!isset($outlookCsv)) && (!isset($googleCsv))) {
            Flash::error(trans('messages.addressBook.import.fileRequired'));

        }

        if (isset($outlookCsv)) {
            if (strtolower($outlookCsv->getClientOriginalExtension()) != 'csv') {
                //handle if file is not csv
                Flash::error(trans('messages.addressBook.import.fileTypeError', ['type' => 'Outlook']));
                return redirect('address-book/import/'.$type);
            }
            //proceed to implementation
            $import =  new ImportFile($outlookCsv);
            $import->load(new ImportOutlookCSV());
            Flash::success(trans('messages.addressBook.import.success', ['type' => 'Outlook']));

        }

        if (isset($googleCsv)) {
            if (strtolower($googleCsv->getClientOriginalExtension()) !='csv') {
                //handle if file is not csv
                Flash::error(trans('messages.addressBook.import.fileTypeError', ['type' => 'Google']));
                return redirect('address-book/import/'.$type);
            }
            //proceed to implementation
            $import =  new ImportFile($googleCsv);
            $import->load(new ImportGoogleCSV());
            Flash::success(trans('messages.addressBook.import.success', ['type' => 'Google']));
        }
        return redirect('address-book');
    }


}
