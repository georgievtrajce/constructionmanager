<?php namespace App\Http\Controllers\Auth;

use App\Modules\Registration\Implementations\Payment;
use App\Modules\Registration\Repositories\RegisterRepository;
use App\Utilities\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Input;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Laracasts\Flash\Flash;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar $registrar
	 * @param Payment $payment
	 */
	public function __construct(Guard $auth, Registrar $registrar, Payment $payment)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->payment = $payment;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

    /**
     * Show the application registration form.
     *
     * @param RegisterRepository $registerRepository
     * @return \Illuminate\Http\Response
     */
	public function getRegister(RegisterRepository $registerRepository)
	{
		$data['subscriptionLevel'] = Input::get('subscription_level');
        $data['firstSubscription'] = $registerRepository->getSubscription('subscription_levels.level-one');
        $data['secondSubscription'] = $registerRepository->getSubscription('subscription_levels.level-two');
        $data['thirdSubscription'] = $registerRepository->getSubscription('subscription_levels.level-three');

        return view('auth.register', $data);
	}

	/**
	 * Handle a registration request for the application.
	 * overriden because if the register is via token, referrals need to be updated
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postRegister(Request $request)
	{
		$token = Input::get('referral_token');
		$subToken = Input::get('sub_token');
		$projectCompanyId = Input::get('projectCompanyId');
		$bidderId = Input::get('bidderId');

		$validator = $this->registrar->validator($request->all());

		if ($validator->fails()) {
			$this->throwValidationException(
				$request, $validator
			);
		}

		$registeredUser = $this->registrar->create($request->all());

		if ($registeredUser) {
			$response = $this->payment->startPaymentImplementation($registeredUser, $token, $subToken, $projectCompanyId, $bidderId, $request->input('subscription_type'));
			return view('layouts.confirmation', []);
                        //return $response;
		} else {
			Flash::error(trans('labels.confirmation.create.error'));
			return redirect()->back()->withInput();
		}
	}

	/**
	 * Activate user account
	 * @param $code
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
	public function activateAccount($code)
	{
		$token = Input::get('refToken');
		$subToken = Input::get('subToken');
		$projectCompanyId = Input::get('projectCompanyId');
		$bidderId = Input::get('bidderId');

		$registerRepository = new RegisterRepository();
		if($registerRepository->accountIsActive($code, $token, $subToken, $projectCompanyId, $bidderId)) {
			Flash::success(trans('labels.confirmation.activation.success'));
			return redirect('home');
		}

		Flash::error(trans('labels.confirmation.activation.error'));
		return redirect('home');
	}

	/**
	 * Handle a login request to the application.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$this->validate($request, [
			'email' => 'required|email', 'password' => 'required',
		]);

		$credentials = $request->only('email', 'password');

		if (Auth::attempt(array('email' => $credentials['email'], 'password' => $credentials['password']), $request->has('remember')))
		{
                    /*Check is it Confirmed*/
                    if (Auth::user()->confirmed == 0) {
                        Auth::Logout();
                        $request->session()->flush();
                        
                        return redirect($this->loginPath)
                            ->withInput($request->only('email', 'remember'))
                            ->withErrors([
                                    'email' => 'You need to confirm your registration before loged in!',
			]);
                    }
                    //if it's super admin
                    if (Auth::user()->hasRole(Config::get('constants.roles.super_admin'))) {
                            return redirect()->intended($this->redirectPath());
                    }

                    return redirect()->intended($this->redirectPath());
		}

		return redirect($this->loginPath())
			->withInput($request->only('email', 'remember'))
			->withErrors([
				'email' => $this->getFailedLoginMessage(),
			]);
	}
}
