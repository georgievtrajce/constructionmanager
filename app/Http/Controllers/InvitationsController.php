<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\User_profile\Repositories\UsersRepository;
use App\Services\InviteCompany;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Laracasts\Flash\Flash;

class InvitationsController extends Controller {

    private $inviteCompanyService;

    public function __construct()
    {
        //
    }

    /**
     * Accept method for the non registered but invited companies
     * @param $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function acceptInvitationForNonRegistered($data)
    {
        return redirect('auth/register/'.$data['referralToken'].'/'.$data['token'].'?projectCompanyId='.$data['projectCompanyId'].'&bidderId='.$data['bidderId']);
    }


    /**
     * Accept invitation method (available through companies module and bids module)
     * @param $data
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function acceptInvitationForRegistered($data)
    {
        if (Auth::check()) {
            $data['invitationType'] = Input::get('inviteType');
            $data['email'] = Input::get('email');
            $data['contactId'] = Input::get('contactId');
            $data['parentCompanyId'] = Input::get('parentCompanyId');
            $data['referralToken'] = Input::get('referralToken');

            //instance invite company service
            $this->inviteCompanyService = new InviteCompany();

            //implement invitation acceptance
            $response = $this->inviteCompanyService->acceptInvitationWrapper(ClassMap::instance(Config::get('classmap.invitations.accept.'.$data['invitationType'])),$data['token'], $data['email'], $data['contactId'], $data['parentCompanyId'], $data['bidderId'], $data['referralToken'], $data['projectCompanyId']);

            if ($response) {
                Flash::success(trans('messages.projects.successfully_accepted', ['item' => 'project']));
            } else {
                Flash::error(trans('messages.projects.already_unshared'));
            }

            return redirect('projects');
        }

        return redirect()->guest('auth/login');
    }


    /**
     * Global public accept after clicking the accept email link
     * @param $token
     * @param UsersRepository $usersRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function globalAccept($token, UsersRepository $usersRepository)
    {
        $data['invitationType'] = Input::get('inviteType');
        $data['email'] = Input::get('email');
        $data['contactId'] = Input::get('contactId');
        $data['parentCompanyId'] = Input::get('parentCompanyId');
        $data['referralToken'] = Input::get('referral_token');
        $data['projectCompanyId'] = Input::get('projectCompanyId');
        $data['bidderId'] = Input::get('bidderId');
        $data['token'] = $token;

        $existingUser = $usersRepository->getRegisteredUserByEmail($data['email']);

        //if user already exist
        if (is_null($existingUser)) {
            return $this->acceptInvitationForNonRegistered($data);
        }

        return $this->acceptInvitationForRegistered($data);
    }

    /**
     * Reject company invitation
     * @param $token
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Exception
     */
    public function rejectInvitation($token)
    {
        $data['invitationType'] = Input::get('inviteType');
        $data['referralToken'] = Input::get('referral_token');
        $data['token'] = $token;

        //instance invite company service
        $this->inviteCompanyService = new InviteCompany();

        //implement invitation acceptance
        $response = $this->inviteCompanyService->rejectInvitationWrapper(ClassMap::instance(Config::get('classmap.invitations.reject.'.$data['invitationType'])),$data['token'], $data['referralToken']);

        if ($response) {
            Flash::success(trans('messages.projects.reject.success'));
        } else {
            Flash::error(trans('messages.projects.reject.error'));
        }

        if (Auth::check()) {
            return redirect('projects');
        }

        return view('home.project_rejected');
    }

}
