<?php namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;

class ImageController extends Controller {

    public function resizeImageTinyMce(Request $request)
    {
        $data = $request->all();

        $imageManager = new ImageManager(); // use this if you don't want facade style code
        $image = $imageManager->make($data['file']);

        if ($image) {
            $image->resize(1000, 800, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $destinationPath = storage_path().'/app/';
            //save to disk temp
            if(!file_exists($destinationPath)) {
                File::makeDirectory($destinationPath);
            }

            $filename = 'image_'.time().'.jpg';
            $image->save($destinationPath.$filename);


            $uploadedImage = Storage::disk('local')->get($filename);

            $s3 = Storage::disk('s3');
            $filePath = 'company_'.Auth::user()->comp_id.'/text-editor-images/'.$filename;
            $s3->put($filePath, $image->stream()->__toString());

            return $error = [
                'status' => 'Success',
                'path' => $filePath,
                ];
        }

        return $error = [
            'status' => 'Error',
            'message' => 'Bad image',
        ];
    }
}
