<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\Auto_complete\Implementations\WrapAutoComplete;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;

class AutoCompleteController extends Controller {

    private $userId;
    private $companyId;

    public function __construct()
    {
        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
    }

    /**
     * Get results for auto complete
     * @return null|void
     */
    public function implementAutoComplete()
    {
        $query = Input::get('query');
        $type = Input::get('type');
        $projectId = 0;

        if (Input::has('projectId')) {
            $projectId = Input::get('projectId');
        }

        if (!empty($query) || $query == 0) {
            $implement = new WrapAutoComplete;
            return $implement->search(ClassMap::instance(Config::get('classmap.auto-complete.'.$type)), $query, $projectId);
        }
        return NULL;
    }

    /**
     * Get office contacts for a specific address book company
     * @param ContactsRepository $contactsRepository
     * @return mixed
     */
    public function getOfficeContacts(ContactsRepository $contactsRepository)
    {
        $officeId = Input::get('officeId');

        if (!empty($officeId)) {
            $officeContacts = $contactsRepository->getOfficeContacts($officeId);

            if (count($officeContacts)) {
                return Response::json(array('success' => 1, 'officeContacts' => $officeContacts, 'message' => 'Office contacts get successfully', 200));
            } else {
                return Response::json(array('success' => 2, 'officeContacts' => $officeContacts, 'message' => 'No available contacts for this office', 200));
            }

        }

        return Response::json(array('success' => 0, 'officeContacts' => null, 'message' => 'Invalid office id', 400));
    }
}
