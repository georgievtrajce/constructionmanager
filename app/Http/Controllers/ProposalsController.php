<?php
namespace App\Http\Controllers;

use App\Models\Proposal_file;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Http\Requests\ProposalsRequest;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Requests;
use App\Utilities\ClassMap;
use App\Utilities\FileStorage;

use Illuminate\Support\Facades\Config;
use Request;
use Input;
use Flash;
use Auth;

class ProposalsController extends Controller {

    private $proposalsRepo;
    private $projectRepo;

    public function __construct(ProposalsRepository $proposalsRepo, ProjectRepository $projectRepo)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        //$this->middleware('subscription_level_one');
        //$this->middleware('only_contractor');
        $this->middleware('sub_module_write', ['only' => ['create','store','update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->proposalsRepo = $proposalsRepo;
        $this->projectRepo = $projectRepo;
    }

    /**
     * load proposals with suppliers and project
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function index($projectID)
    {
        $sort = Input::get('sort','id');
        $order = Input::get('order','asc');

        if ($sort == 'proposal_final') {
            $proposals = $this->proposalsRepo->getProposalsByProjectSortedByFinalProposal($projectID, $sort, $order);
        } else {
            $proposals = $this->proposalsRepo->getProposalsByProject($projectID, $sort, $order);
        }

        $project = $this->projectRepo->getProject($projectID);

        return view('projects.proposals.index',compact('project','proposals'));
    }

    /**
     * Filter bid items
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filter($projectId, FilterDataWrapper $wrapper)
    {
        $sort = Input::get('sort','id');
        $order = Input::get('order','asc');

        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $status = Input::get('status');
        $bidName = Input::get('bid_name');
        $bidCompany = Input::get('bid_company');

        //Set filter type
        $type = 'proposals';

        //implement proposals filter
        $data['proposals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('sort', 'order', 'masterFormat', 'status', 'projectId', 'bidName', 'bidCompany'));
        $data['project'] = $this->projectRepo->getProject($projectId);

        return view('projects.proposals.index', $data);
    }

    /**
     * PDF report
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @return mixed
     * @throws \Exception
     */
    public function report($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator)
    {
        //get filter parameters
        $masterFormat = Input::get('report_mf');
        $status = Input::get('report_status');

        //get sorting parameters
        $sort = Input::get('report_sort');
        $order = Input::get('report_order');

        //Set report type
        $type = 'proposals-report';

        //Get bids
        $data['bids'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'status', 'projectId', 'sort', 'order'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.proposals.report';

        //Set report title
        $data['mf_title'] = "";
        $data['status_title'] = "";

        //Create full report title
        if ($masterFormat != '') {
            $data['mf_title'] = "Master Format Number or Title: ".$masterFormat.",";
        }

        //set status title
        $data['status_title'] = "Status: ".$status;

        //Set report name
        $name = "Bids";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * PDF report for bidders
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param ProposalsRepository $proposalsRepository
     * @return mixed
     */
    public function reportBidders($projectId, $bidId, ProjectRepository $projectRepository,
                                  ReportGenerator $generator, ProposalsRepository $proposalsRepository)
    {
        //Set report type
        $type = 'proposals-report';

        //Get bids
        $data['bid'] = $proposalsRepository->getProposal($bidId);

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.proposals.reportBidders';

        //Set report name
        $name = "Bids";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * load view for creating proposal for project
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function create($projectID)
    {
        $project = $this->projectRepo->getProject($projectID);

        return view('projects.proposals.create',compact('project'));
    }

    /**
     * store proposal with data
     * @param $id
     * @param ProposalsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($id, ProposalsRequest $request)
    {
        $input = $request->all();
        $proposal = $this->proposalsRepo->storeProposal($id, $input);

        if ($proposal) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'bid item']));
            return redirect('projects/'.$id.'/bids/'.$proposal->id.'/edit');
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'bid item']));
        }

        return redirect('projects/'.$id.'/bids/');
    }

    /**
     * show proposal
     * currently not used
     * @param $projectID
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($projectID, $id)
    {
        $proposal = $this->proposalsRepo->getProposal($id);
        $project = $this->projectRepo->getProject($projectID);

        return view('projects.proposals.edit',compact('project','proposal'));
    }


    /**
     *
     * get proposal for ediitng
     * @param $projectID
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($projectID, $id)
    {
        $proposal = $this->proposalsRepo->getProposal($id);
        $project = $this->projectRepo->getProject($projectID);

        return view('projects.proposals.edit',compact('project','proposal'));
    }

    /**
     * update proposal with data from request
     * @param $projectID
     * @param $id
     * @param ProposalsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectID, $id, ProposalsRequest $request)
    {
        $data = $request->all();
        $proposal = $this->proposalsRepo->updateProposal($id, $projectID, $data);

        if ($proposal) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'bid item']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'bid item']));
        }

        return redirect('projects/'.$projectID.'/bids/'.$id.'/edit');
    }

    /**
     * delete proposal with suppliers
     * @param $projectID
     * @param $id
     * @param FilesRepository $filesRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectID, $id, FilesRepository $filesRepository)
    {
        $idsArray = explode('-', $id);

        $errorEntry = false;
        foreach ($idsArray as $bidId) {

            $suppliers = $this->proposalsRepo->getSuppliersIDsByProposal($bidId);
            for ($i=0; $i<sizeof($suppliers); $i++) {
                $proposalFiles = Proposal_file::where('prop_supp_id','=',$suppliers[$i])
                    ->with('file')
                    ->get();
                if (count($proposalFiles)) {
                    foreach ($proposalFiles as $proposalFile) {
                        if (!is_null($proposalFile->file)) {
                            //delete file by name and path
                            $filesRepository->deleteFile('company_'.Auth::user()->company->id.'/project_'.$projectID.'/proposals', $proposalFile->file->file_name);
                        }
                    }
                }
            }

            $proposal = $this->proposalsRepo->deleteProposal($bidId, $projectID);

            if (empty($proposal)) {
                $errorEntry = false;
            }
        }

        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'bid item']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'bi item']));
        }

        return redirect('projects/'.$projectID.'/bids/');
    }

}