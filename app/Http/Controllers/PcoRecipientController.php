<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PcoSubcontractorsRequest;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class PcoRecipientController extends Controller {

    const ENTITY_ID = 10;

	public function __construct()
	{
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

    /**
     * Show the form for creating a new resource.
     *
     * @param $projectId
     * @param $pcoId
     * @param ProjectRepository $projectRepository
     * @param PcosRepository $pcosRepository
     * @param AddressBookRepository $addressBookRepository
     * @return Response
     */
	public function create($projectId, $pcoId, ProjectRepository $projectRepository, PcosRepository $pcosRepository, AddressBookRepository $addressBookRepository)
	{
		//get the proper project
		$data['project'] = $projectRepository->getProject($projectId);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);

		//get the proper pco
		$data['pco'] = $pcosRepository->getPco($pcoId);

		return view('projects.pcos.create-recipient', $data);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param PcoSubcontractorsRequest $request
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return Response
     */
	public function store($projectId, $pcoId, PcoSubcontractorsRequest $request, PcosRepository $pcosRepository,
                          ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                          ProjectPermissionsRepository $projectPermissionsRepo, ProjectRepository $projectRepository)
	{
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

        //Store pco subcontractor
        $dataInput = $request->all();
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient)) ? $projectCompanyPermissionsRecipient->write : 0;
        $response = $pcosRepository->storePcoRecipient($pcoId, $dataInput);

		if ($response) {


            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.pcos'),
                'moduleId' => $pcoId
            ];

            if (!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully created pco recipient
			Flash::success(trans('messages.submittals.store.success', ['item' => 'Pco Recipient']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/recipient/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'Pco Recipient']));
		return Redirect::to('projects/'.$projectId.'/pcos'.$pcoId.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $pcoId
     * @param $pcoRecipientId
     * @param PcosRepository $pcosRepository
     * @param ProjectRepository $projectRepository
     * @param ContactsRepository $contactsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return Response
     */
	public function edit($projectId, $pcoId, $pcoRecipientId, PcosRepository $pcosRepository, ProjectRepository $projectRepository, ContactsRepository $contactsRepository, CheckUploadedFilesInterface $checkUploadedFiles)
	{
		//get the proper project
		$data['project'] = $projectRepository->getProject($projectId);

		//Store pco subcontractor
		$data['pcoRecipient'] = $pcosRepository->getPcoRecipient($pcoId, $pcoRecipientId);
        $data['pcoRecipient'] = $checkUploadedFiles->checkAndOrderUploadedFiles($data['pcoRecipient'], Config::get('constants.recipient_versions'));
		$data['projectRecipientContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['pcoRecipient']->ab_recipient_id);

		return view('projects.pcos.edit-recipient', $data);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param $pcoRecipientId
     * @param PcoSubcontractorsRequest $request
     * @param PcosRepository $pcosRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return Response
     */
	public function update($projectId, $pcoId, $pcoRecipientId, PcoSubcontractorsRequest $request, PcosRepository $pcosRepository,
                           ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                           ProjectPermissionsRepository $projectPermissionsRepo, ProjectRepository $projectRepository)
	{
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

        //Update pco subcontractor
        $dataInput = $request->all();
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient)) ? $projectCompanyPermissionsRecipient->write : 0;
		$pco = $pcosRepository->updatePcoRecipient($pcoId, $pcoRecipientId, $dataInput);

		if ($pco) {

            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.pcos'),
                'moduleId' => $pcoId
            ];

            if (!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

			//Successfully updated pco
			Flash::success(trans('messages.submittals.update.success', ['item' => 'Pco Recipient']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/recipient/'.$pcoRecipientId.'/edit');
		}

		Flash::error(trans('messages.submittals.update.error', ['item' => 'Pco Recipient']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/recipient/'.$pcoRecipientId.'/edit');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
