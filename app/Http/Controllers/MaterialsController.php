<?php
namespace App\Http\Controllers;

use App\Modules\Materials_and_services\Repositories\MaterialsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Http\Requests\MaterialsRequest;
use App\Requests;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Utilities\ClassMap;
use Illuminate\Support\Facades\Config;

use Request;
use Input;
use Flash;
use Auth;

class MaterialsController extends Controller {

    private $materialsRepo;
    private $projectRepo;
    private $submittalsRepo;

    public function __construct(MaterialsRepository $materialsRepo, ProjectRepository $projectRepo, SubmittalsRepository $submittalsRepo)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        //$this->middleware('only_contractor', ['except' => ['report']]);
        $this->middleware('sub_module_write', ['only' => ['create','store','update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->materialsRepo = $materialsRepo;
        $this->projectRepo = $projectRepo;
        $this->submittalsRepo = $submittalsRepo;

    }

    /**
     * load materials and project
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function index($projectId)
    {

        $sort = Input::get('sort', 'ID');
        $order = Input::get('order', 'asc');

        $materials = $this->materialsRepo->getMaterialsByProject($projectId, $sort, $order);
        $materialStatuses = $this->materialsRepo->getStatuses();

        $project = $this->projectRepo->getProject($projectId);
        $statuses = $this->submittalsRepo->getSubmittalStatuses();
        return view('projects.materials.index', compact('project','materials','statuses','materialStatuses'));

    }

    /**
     * load view for creating materials for project
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function create($projectId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $statuses = $this->submittalsRepo->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepo->getStatuses();
        return view('projects.materials.create', compact('project','statuses','materialStatuses'));
    }

    /**
     * store material/service with data
     * @param $id
     * @param MaterialsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($id, MaterialsRequest $request)
    {
        $data = $request->all();

        $material = $this->materialsRepo->storeMaterial($id, $data);

        if ($material) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'material/service']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'material/service']));
        }

        return redirect('projects/'.$id.'/expediting-report/');
    }

    /**
     * show material/service
     * currently not used
     * should be deleted and handled in exceptions
     * @param $projectId
     * @param $id
     * @return \Illuminate\View\View
     */
    public function show($projectId, $id)
    {
        $material = $this->materialsRepo->getMaterial($id);

        $project = $this->projectRepo->getProject($projectId);
        $statuses = $this->submittalsRepo->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepo->getStatuses();

        return view('projects.materials.edit',compact('project', 'material', 'statuses', 'materialStatuses'));
    }


    /**
     *
     * get material/service for editing
     * @param $projectId
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $id)
    {
        $material = $this->materialsRepo->getMaterial($id);
        $project = $this->projectRepo->getProject($projectId);
        $statuses = $this->submittalsRepo->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepo->getStatuses();

        return view('projects.materials.edit', compact('project', 'material', 'statuses', 'materialStatuses'));
    }

    /**
     * update material/service with data from request
     * @param $projectId
     * @param $id
     * @param MaterialsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $id, MaterialsRequest $request)
    {
        $data = $request->all();

        $update = $this->materialsRepo->updateMaterial($id, $data);

        if ($update) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'material/service']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'material/service']));
        }

        return redirect('projects/'.$projectId.'/expediting-report/');

    }

    /**
     * delete material/service
     * @param $projectId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectId, $id)
    {
        $idsArray = explode('-', $id);

        $errorEntry = false;
        foreach ($idsArray as $materialId) {
            $response = $this->materialsRepo->deleteMaterial($materialId);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }


        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'material/service']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'material/service']));
        }
        return redirect('projects/'.$projectId.'/expediting-report/');
    }

    /**
     * filter records for printing by GET input parameters
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     * @throws \Exception
     */

    public function filter($projectId, FilterDataWrapper $wrapper)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $name = Input::get('name');
        $location = Input::get('location');
        $status = Input::get('status');
        $erStatus = Input::get('er_status');
        $sort = Input::get('sort','id');
        $order = Input::get('order','asc');

        //Set filter type
        $type = 'materials';

        //implement submittals filter
        $materials = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat',
            'subcontractor', 'location', 'name', 'status', 'projectId', 'erStatus','sort','order'));
        $project = $this->projectRepo->getProject($projectId);
        $statuses = $this->submittalsRepo->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepo->getStatuses();

        return view('projects.materials.index', compact('materials', 'project', 'status', 'statuses', 'materialStatuses'));

    }

    /**
     * open pdf file in new tab
     * generate report by filtered parameters
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ReportGenerator $generator
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     * @throws \Exception
     */
    public function report($projectId, FilterDataWrapper $wrapper, ReportGenerator $generator, SubmittalsRepository $submittalsRepository)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $subcontractorName = Input::get('supplier_subcontractor');
        $status = Input::get('status');
        $erStatus = Input::get('erStatus');
        $sort = Input::get('sort','id');
        $order = Input::get('order','asc');

        //Set report type
        $type = 'materials';

        //Get materials/services
        $data['materials'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'erStatus', 'sort', 'order'));
        //Get project
        $data['project'] = $this->projectRepo->getProject($projectId);
        $data['statuses'] = $this->submittalsRepo->getSubmittalStatuses();
        $data['materialStatuses'] = $this->materialsRepo->getStatuses();

        //Set report view
        $view = 'projects.materials.report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if($masterFormat != '') {
            $data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
        }
        if($subcontractorName != '') {
            $data['title'] = $data['title'].'Subcontractor: '.$subcontractorName.", ";
        }

        //get status from database table
        if ($status == '') {
            $statusString = 'All';
        } else {
            $statusRow = $submittalsRepository->getStatusById($status);
            $statusString = $statusRow->name;
        }

        $data['title'] = $data['title'].'Status: '.$statusString;

        //Set report name
        $name = "Expediting_Report";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }
}