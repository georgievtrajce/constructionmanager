<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AddSubmittalVersionRequest;
use App\Http\Requests\EditSubmittalRequest;
use App\Http\Requests\EditSubmittalVersionRequest;
use App\Http\Requests\SubmittalRequest;
use App\Models\Address;
use App\Models\File_type;
use App\Models\Project;
use App\Models\Project_file;
use App\Models\Submittal;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Contacts\Repositories\ContactsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Files\Implementations\CheckUploadedFiles;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Project\Interfaces\ProjectInterface;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Implementations\ProjectFilesMap;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Tasks\Repositories\TasksRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class SubmittalsController extends Controller
{

    const ENTITY_ID = 3;

    private $userId;
    private $companyId;
    /**
     * @var TasksRepository
     */
    private $tasksRepository;
    /**
     * @var ProjectPermissionsRepository
     */
    private $permissionsRepository;

    public function __construct(TasksRepository $tasksRepository, ProjectPermissionsRepository $permissionsRepository)
    {
        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        //$this->middleware('subscription_level_one');
        //$this->middleware('only_contractor');
        $this->middleware('sub_module_write', ['only' => ['create', 'store', 'update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);

        if (!empty(Auth::user())) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }
        $this->tasksRepository = $tasksRepository;
        $this->permissionsRepository = $permissionsRepository;
    }

    /**
     * Submittals home page (list all submittals with paging)
     * @param $projectId
     * @param SubmittalsRepository $submittalsRepository
     * @param ProjectRepository $projectRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function index($projectId, SubmittalsRepository $submittalsRepository, ProjectRepository $projectRepository, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        //default sort and order unless selected
        $sort = Input::get('sort', 'number');
        $order = Input::get('order', 'asc');

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $submittalsRepository->getAllSubmittalStatuses();
        $submittalsStatistics = [];
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
            $submittalsStatistics[$status->name] = 0;
        }

        //get all project submittals
        $data['submittals'] = $submittalsRepository->getProjectSubmittals($projectId, $sort, $order);

        foreach ($data['submittals'] as $key => $submittal) {
            $data['submittals'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($submittal, Config::get('constants.submittals_versions'));

            $submittalsStatistics[$submittal->version_status_name]++;
        }

        //get project
        $data['project'] = $projectRepository->getProject($projectId);

        $data['status'] = 'All';
        $data['submittalsStatistics'] = $submittalsStatistics;

        return view('projects.submittals.index', $data);
    }

    /**
     * Generate pdf report from filtered or all listed submittals
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     * @throws \Exception
     */
    public function pdfReport($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, SubmittalsRepository $submittalsRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort');
        $order = Input::get('order');

        //get filter parameters
        $masterFormat = Input::get('report_mf');
        $subcontractor = Input::get('report_sub_id');
        $subcontractorName = Input::get('report_sub_name');
        $statusId = Input::get('report_status');
        $data['printWithNotes'] = Input::get('print_with_notes');

        //get submittal version status by id
        $status = ($statusId == 'All') ? 'All' : $statusId;

        //Set report type
        $type = 'submittals-report';

        //Get submittals
        $data['submittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.' . $type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.submittals.report';

        //Set report title
        $data['mf_title'] = "";
        $data['sub_title'] = "";
        $data['status_title'] = "";

        //Create full report title
        if ($masterFormat != '') {
            $data['mf_title'] = "Master Format Number or Title: " . $masterFormat . ",";
        }
        if ($subcontractorName != '') {
            $data['sub_title'] = "Subcontractor: " . $subcontractorName . ",";
        }

        //get status from database table
        if ($statusId == 'All') {
            $statusString = 'All';
        } else if ($statusId == 'all_open') {
            $statusString = 'All Open';
        } else {
            $statusRow = $submittalsRepository->getStatusById($statusId);
            $statusString = $statusRow->name;
        }

        //set status title
        $data['status_title'] = "Status: " . $statusString;

        //Set report name
        $name = "Submittals";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * Submittals filter
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filter($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        //default sort and order unless selected
        $sort = Input::get('sort');
        $order = Input::get('order');

        //get filter parameters
        $statuses = $submittalsRepository->getAllSubmittalStatuses();
        $submittalsStatistics = [];
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
            $submittalsStatistics[$versionStatus->name] = 0;
        }

        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $name = Input::get('name');
        $status = Input::get('status');

        //Set filter type
        $type = 'submittals';

        //implement submittals filter
        $data['submittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.' . $type)),
            compact('masterFormat', 'subcontractor', 'name', 'status', 'projectId', 'sort', 'order'));

        foreach ($data['submittals'] as $key => $submittal) {
            $data['submittals'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($submittal, Config::get('constants.submittals_versions'));

            $submittalsStatistics[$submittal->version_status_name]++;
        }

        $data['project'] = $projectRepository->getProject($projectId);

        $data['status'] = $status;
        $data['submittalsStatistics'] = $submittalsStatistics;

        return view('projects.submittals.index', $data);

    }

    /**
     * Create new submittal form
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param ProjectInterface $projectModule
     * @param AddressBookRepository $addressBookRepository
     * @return \Illuminate\View\View
     */
    public function create($projectId, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository,
                           ProjectInterface $projectModule, AddressBookRepository $addressBookRepository)
    {
        $data['project'] = $projectRepository->getProject($projectId);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);
        $numbers = $submittalsRepository->getGeneratedNumbers($projectId);
        $data['project']['generated_number'] = $projectModule->generateNumber($numbers, $data['project']['submittals_counter']);

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $submittalsRepository->getAllSubmittalStatuses();
        $data['statuses'] = [];
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
        }

        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['submittedFor'] = ['Approval', 'Review', 'Record', 'Info', 'For Your Use'];
        $data['submittalType'] = ['Shop Drawings', 'Product Data', 'Samples', 'Test Reports', 'Drawings',
            'Specifications', 'Letter'];

        return view('projects.submittals.create', $data);
    }

    /**
     * Store new submittal
     * @param $projectId
     * @param SubmittalRequest $request
     * @param SubmittalsRepository $submittalsRepository
     * @param DataTransferLimitation $transferLimitation
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
    public function store($projectId, SubmittalRequest $request, SubmittalsRepository $submittalsRepository,
                          DataTransferLimitation $transferLimitation, ProjectFilesRepository $projectFilesRepository,
                          MailRepository $mailRepository, ProjectPermissionsRepository $projectPermissionsRepo,
                          ProjectRepository $projectRepository)
    {
        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

        //Update submittal
        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor)) ? $projectCompanyPermissionsSubcontractor->write : 0;
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient)) ? $projectCompanyPermissionsRecipient->write : 0;

        $response = $submittalsRepository->storeProjectSubmittal($projectId, $dataInput);

        if ($response) {
            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.submittals'),
                'moduleId' => $response->id,
                'name' => $response->name
            ];


            if (!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //if there is no default user assigned to the project we need to send the email again
            if (!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //Successfully created submittal
            Flash::success(trans('messages.submittals.store.success', ['item' => 'Submittal']));
            return Redirect::to(url('projects/' . $projectId . '/submittals/' . $response->id . '/version/' . $response->last_version . '/edit'));
        }

        Flash::error(trans('messages.submittals.store.error', ['item' => 'Submittal']));
        return Redirect::to('projects/' . $projectId . '/submittals');
    }

    /**
     * Edit submittal form
     * @param $projectId
     * @param $submittalId
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param ContactsRepository $contactsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @param AddressBookRepository $addressBookRepository
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $submittalId, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository,
                         ContactsRepository $contactsRepository, CheckUploadedFilesInterface $checkUploadedFiles, AddressBookRepository $addressBookRepository)
    {
        $data['project'] = $projectRepository->getProject($projectId);
        $data['contractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->general_contractor_id, $data['project']->make_me_gc);
        $data['architectProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->architect_id);
        $data['primeSubcontractorProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->prime_subcontractor_id);
        $data['ownerProjectTransmittals'] = $addressBookRepository->getProjectTransmittalsByProjectId($projectId, $data['project']->owner_id);
        $data['submittal'] = $submittalsRepository->getSubmittal($submittalId);
        $data['submittal'] = $checkUploadedFiles->checkAndOrderUploadedFiles($data['submittal'], Config::get('constants.submittals_versions'));
        //TODO - getProjectSubcontractorContacts() not working properly
        $data['projectSubcontractorContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['submittal']->sub_id);
        $data['projectRecipientContacts'] = $contactsRepository->getProjectSubcontractorContacts($projectId, $data['submittal']->recipient_id);
        $data['sentVia'] = ['Email', 'Mail', 'Hand Delivered'];
        $data['submittalType'] = ['Shop Drawings', 'Product Data', 'Samples', 'Test Reports', 'Drawings',
            'Specifications', 'Letter'];
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $data['page'] = 'submittal-tasks';
        $subcontractorTaskIds = $this->tasksRepository->getAllTasksSubcontructor();
        $data['subcontractorTaskIds'] = array_column($subcontractorTaskIds, 'task_id');
        $data['userCanWrite'] = $this->permissionsRepository->userHasAtLeastOneWritePermission(12);
        $data['userCanDelete'] = $this->permissionsRepository->userHasAtLeastOneDeletePermission(12);
        $data['activeInnerTab'] = 'tasks';
        $data['tasks'] = $this->tasksRepository->getAllProjectTasksBySubmittal($submittalId, $orderBy, $orderDir);

        return view('projects.submittals.edit', $data);
    }

    /**
     * Update submittal
     * @param $projectId
     * @param $submittalId
     * @param EditSubmittalRequest $request
     * @param SubmittalsRepository $submittalsRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectPermissionsRepository $projectPermissionsRepo
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
    public function update($projectId, $submittalId, EditSubmittalRequest $request, SubmittalsRepository $submittalsRepository,
                           ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository,
                           ProjectPermissionsRepository $projectPermissionsRepo, ProjectRepository $projectRepository)
    {

        $projectCompanyPermissionsSubcontractor = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('sub_id'), self::ENTITY_ID);
        $projectCompanyPermissionsRecipient = $projectPermissionsRepo->getCompanyPermissionsByProjectAndAddressBookCompany($projectId, $request->get('recipient_id'), self::ENTITY_ID);

        //check if there is a recipient already set as a default
        $project = $projectRepository->getProjectRecord($projectId);
        $recipientId = null;
        if ($project) {
            if (!empty($project->owner_transmittal_id)) {
                $recipientId = $project->owner_transmittal_id;
            }
            if (!empty($project->contractor_transmittal_id)) {
                $recipientId = $project->contractor_transmittal_id;
            }
            if (!empty($project->architect_transmittal_id)) {
                $recipientId = $project->architect_transmittal_id;
            }
            if (!empty($project->prime_subcontractor_transmittal_id)) {
                $recipientId = $project->prime_subcontractor_transmittal_id;
            }
        }

        //Update submittal
        $dataInput = $request->all();
        $dataInput['send_notif_subcontractor'] = (isset($projectCompanyPermissionsSubcontractor)) ? $projectCompanyPermissionsSubcontractor->write : 0;
        $dataInput['send_notif_recipient'] = (isset($projectCompanyPermissionsRecipient)) ? $projectCompanyPermissionsRecipient->write : 0;
        $submittal = $submittalsRepository->updateSubmittal($submittalId, $dataInput);

        if ($submittal) {

            $data = [
                'projectName' => $request->get('project_name'),
                'moduleName' => Config::get('constants.modules.submittals'),
                'moduleId' => $submittal->id,
                'name' => $submittal->name
            ];

            if (!empty($dataInput['send_notif_subcontractor']) && !empty($request->get('sub_contact'))) {
                $subcontractor = $projectFilesRepository->getContractByIdArray([$request->get('sub_contact')]);
                $data['contact'] = $subcontractor->toArray()[0];
                $data['type'] = Config::get('constants.upload.subcontractor');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //if there is no default user assigned to the project we need to send the email again
            if (!empty($dataInput['send_notif_recipient']) && !empty($request->get('recipient_contact')) && empty($recipientId)) {
                $recipient = $projectFilesRepository->getContractByIdArray([$request->get('recipient_contact')]);
                $data['contact'] = $recipient->toArray()[0];
                $data['type'] = Config::get('constants.upload.recipient');
                $mailRepository->sendTransmittalUpdateNotification($data);
            }

            //Successfully updated submittal
            Flash::success(trans('messages.submittals.update.success', ['item' => 'Submittal']));
            return Redirect::to('projects/' . $projectId . '/submittals/' . $submittalId . '/edit');
        }

        Flash::error(trans('messages.submittals.update.error', ['item' => 'Submittal']));
        return Redirect::to('projects/' . $projectId . '/submittals/' . $submittalId . '/edit');
    }

    /**
     * Delete submittal
     * @param $projectId
     * @param $submittalId
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     */
    public function destroy($projectId, $submittalId, SubmittalsRepository $submittalsRepository)
    {

        $idsArray = explode('-', $submittalId);

        $errorEntry = false;
        foreach ($idsArray as $id) {

            //select submittal versions for file deleting
            $submittalVersions = $submittalsRepository->getSubmittalVersions($id);

            //delete submittal versions files from s3
            foreach ($submittalVersions as $submittalVersion) {
                foreach ($submittalVersion->file as $versionFile) {
                    //delete submittal versions files
                    $submittalsRepository->deleteSubmittalFile($projectId, $versionFile->file_name);
                }
            }

            //delete submittals and its versions from database
            $response = $submittalsRepository->deleteSubmittal($id);


            if (is_null($response)) {
                $errorEntry = true;
            }
        }


        //Successfully deleted submittal
        if (!$errorEntry) {
            Flash::success(trans('messages.submittals.delete.success', ['item' => 'Submittal']));
            return Redirect::to('projects/' . $projectId . '/submittals');
        }

        //Not successful deleting
        Flash::error(trans('messages.submittals.delete.error', ['item' => 'Submittal']));
        return Redirect::to('projects/' . $projectId . '/submittals');

    }

    /**
     * Get all shares of the submittals for all shared companies
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @return \Illuminate\View\View
     */
    public function shares($projectId, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository)
    {
        //get project
        $data['project'] = $projectRepository->getProject($projectId);

        //get all companies with which the logged company have shared some of their uploaded project files
        $data['sharedSubmittals'] = $submittalsRepository->getSharedSubmittals($projectId, Config::get('constants.company_type.creator'), $data['project']->comp_id);

        return view('projects.submittals.submittals-shares', $data);
    }

    /**
     * Unshare specified submittal with the specific company
     * @param $projectId
     * @param $submittalId
     * @param $shareCompanyId
     * @param $receiveCompanyId
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     * @internal param $fileId
     * @internal param ProjectFilesRepository $filesRepository
     */
    public function unshare($projectId, $submittalIds, SubmittalsRepository $submittalsRepository)
    {
        $idsArray = explode('-', $submittalIds);

        $errorEntry = false;
        foreach ($idsArray as $ids) {

            $jointIds = explode("|", $ids);

            $response = $submittalsRepository->unshareSubmittal($jointIds[0], $jointIds[1], $jointIds[2]);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

        if (!$errorEntry) {
            //Successfully unshared project submittal
            Flash::success(trans('messages.submittals.unshare.success'));
        } else {
            //File unsharing was not successful
            Flash::error(trans('messages.submittals.unshare.error'));
        }

        //projects/15/submittals/shares
        return Redirect::to(URL('projects/' . $projectId . '/submittals/shares'));
    }

    /**
     * Get submittals transmittals
     * @param $projectId
     * @param SubmittalsRepository $submittalsRepository
     * @param ProjectRepository $projectRepository
     * @return \Illuminate\View\View
     */
    public function getTransmittals($projectId, SubmittalsRepository $submittalsRepository, ProjectRepository $projectRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort', 'transmittals_logs.id');
        $order = Input::get('order', 'asc');

        $data['project'] = $projectRepository->getProject($projectId);

        //Get version transmittals
        $data['transmittals'] = $submittalsRepository->getTransmittals($projectId, $sort, $order);

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $submittalsRepository->getAllSubmittalStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
        }

        $data['status'] = 'All';

        return view('projects.submittals.transmittals-log', $data);
    }

    /**
     * Filter submittals transmittals by given parameters
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filterTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $sentTo = Input::get('company_id');
        $status = Input::get('status');

        //Set filter type
        $type = 'submittal-transmittals-filter';

        //implement submittals filter
        $data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.' . $type)), compact('masterFormat', 'sentTo', 'status', 'projectId'));
        $data['project'] = $projectRepository->getProject($projectId);

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $submittalsRepository->getAllSubmittalStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
        }

        $data['status'] = $status;

        return view('projects.submittals.transmittals-log', $data);

    }

    /**
     * Get list of transmittals as a report
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param ReportGenerator $generator
     * @return mixed
     * @throws \Exception
     */
    public function reportTransmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository, ReportGenerator $generator)
    {
        //get filter parameters
        $sort = Input::get('report_sort');
        $order = Input::get('report_order');
        $masterFormat = Input::get('report_mf');
        $sentTo = Input::get('report_comp_id');
        $sentToName = Input::get('report_sent_to');
        $statusId = Input::get('report_status');

        //get submittal version status by id
        $status = ($statusId == 'All') ? 'All' : $statusId;

        //Set report type
        $type = 'submittal-transmittals';

        //Get submitta
        $data['transmittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.' . $type)), compact('sort', 'order', 'masterFormat', 'sentTo', 'status', 'projectId'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.submittals.transmittals-report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if ($masterFormat != '') {
            $data['title'] = $data['title'] . ' Master Format Number and Title: ' . $masterFormat . ", ";
        }
        if ($sentToName != '') {
            $data['title'] = $data['title'] . 'Subcontractor: ' . $sentToName . ", ";
        }

        //get status from database table
        if ($statusId == 'All') {
            $statusString = 'All';
        } else if ($statusId == 'all_open') {
            $statusString = 'All Open';
        } else {
            $statusRow = $submittalsRepository->getStatusById($statusId);
            $statusString = $statusRow->name;
        }


        $data['title'] = $data['title'] . 'Status: ' . $statusString;

        //Set report name
        $name = "Transmittal_Log_Submittals";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    public function deleteTransmittal($projectId, $transmittalId, SubmittalsRepository $submittalsRepository)
    {
        $idsArray = explode('-', $transmittalId);

        $errorEntry = false;
        foreach ($idsArray as $id) {

            $transmittal = $submittalsRepository->getTransmittal($id);

            $submittalsRepository->deleteTransmittalFromS3($projectId, $transmittal->file_id);

            $submittalsRepository->deleteTransmittal($id);
        }

        //Successfully deleted submittal version
        Flash::success(trans('messages.submittals.delete.success', ['item' => 'Transmittal']));
        return Redirect::to('projects/' . $projectId . '/submittals/transmittals');
    }
}
