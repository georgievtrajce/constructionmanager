<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\SubscriptionLevelUpdateRequest;
use App\Modules\Subscription_levels\Repositories\SubscriptionLevelsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class SubscriptionLevelsController extends Controller {

    /**
     * Get all subscription levels
     * @param SubscriptionLevelsRepository $subscriptionLevelsRepository
     * @return \Illuminate\View\View
     */
    public function index(SubscriptionLevelsRepository $subscriptionLevelsRepository)
    {
        $data['subscriptionLevels'] = $subscriptionLevelsRepository->getSubscriptionLevels();
        return view('subscription_levels.index', $data);
    }

    /**
     * Edit specified subscription level data
     * @param $subscriptionLevelId
     * @param SubscriptionLevelsRepository $subscriptionLevelsRepository
     * @return \Illuminate\View\View
     */
    public function edit($subscriptionLevelId, SubscriptionLevelsRepository $subscriptionLevelsRepository)
    {
        $data['subscriptionLevel'] = $subscriptionLevelsRepository->getSubscriptionLevel($subscriptionLevelId);
        return view('subscription_levels.edit', $data);
    }

    public function update($subscriptionLevelId, SubscriptionLevelUpdateRequest $request, SubscriptionLevelsRepository $subscriptionLevelsRepository)
    {
        //Update subscription level
        $subscriptionLevel = $subscriptionLevelsRepository->updateSubscriptionLevel($subscriptionLevelId, $request->all());

        if ($subscriptionLevel) {
            //Successfully updated subscription level
            Flash::success(trans('messages.submittals.update.success', ['item' => 'Subscription level']));
            return Redirect::to('subscription-levels/'.$subscriptionLevelId.'/edit');
        }

        Flash::error(trans('messages.submittals.update.error', ['item' => 'Subscription level']));
        return Redirect::to('subscription-levels/'.$subscriptionLevelId.'/edit');
    }

}
