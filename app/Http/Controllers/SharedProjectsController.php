<?php
namespace App\Http\Controllers;
use App\Http\Requests\ProposalsRequest;
use App\Models\File_type;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Files\Implementations\CheckUploadedFiles;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Filter\Implementations\FilterDataWrapper;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Materials_and_services\Repositories\MaterialsRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Blog\Repositories\BlogRepository;
use App\Models\Project;

use App\Modules\Project_files\Implementations\ProjectFilesMap;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use App\Services\CompanyPermissions;
use App\Services\Location;
use App\Services\Mailer;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Laracasts\Flash\Flash;
use Session;

class SharedProjectsController extends Controller
{

    private $repository;
    private $blogRepository;
    private $submittalsRepository;
    private $rfisRepository;
    private $pcosRepository;
    private $materialsRepository;
    private $contractsRepository;
    private $proposalsRepository;
    /**
     * @var Location
     */
    private $location;
    /**
     * @var AddressBookRepository
     */
    private $addressBookRepository;
    /**
     * @var ProjectPermissionsRepository
     */
    private $permissionsRepository;
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;
    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var ManageUsersRepository
     */
    private $manageUsersRepository;

    /**
     * SharedProjectsController constructor.
     * @param ProjectRepository $projectRepo
     * @param BlogRepository $blogRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param PcosRepository $pcosRepository
     * @param MaterialsRepository $materialsRepository
     * @param ContractsRepository $contractsRepository
     * @param AddressBookRepository $addressBookRepository
     * @param ProposalsRepository $proposalsRepository
     * @param Location $location
     * @param ProjectPermissionsRepository $permissionsRepository
     * @param CompaniesRepository $companiesRepository
     * @param UsersRepository $usersRepository
     * @param ManageUsersRepository $manageUsersRepository
     */
    public function __construct(ProjectRepository $projectRepo, BlogRepository $blogRepository,
                                SubmittalsRepository $submittalsRepository, RfisRepository $rfisRepository,
                                PcosRepository $pcosRepository, MaterialsRepository $materialsRepository,
                                ContractsRepository $contractsRepository, AddressBookRepository $addressBookRepository,
                                ProposalsRepository $proposalsRepository, Location $location,
                                ProjectPermissionsRepository $permissionsRepository, CompaniesRepository $companiesRepository,
                                UsersRepository $usersRepository, ManageUsersRepository $manageUsersRepository)
    {
        $this->repository = $projectRepo;
        $this->blogRepository = $blogRepository;
        $this->submittalsRepository = $submittalsRepository;
        $this->rfisRepository = $rfisRepository;
        $this->pcosRepository = $pcosRepository;
        $this->materialsRepository = $materialsRepository;
        $this->contractsRepository = $contractsRepository;
        $this->proposalsRepository = $proposalsRepository;
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit', [
            'only' => [
                'receivedFiles'
            ]
        ]);

        $this->middleware('unshared_project');
        $this->location = $location;
        $this->addressBookRepository = $addressBookRepository;
        $this->permissionsRepository = $permissionsRepository;
        $this->companiesRepository = $companiesRepository;
        $this->usersRepository = $usersRepository;
        $this->manageUsersRepository = $manageUsersRepository;
    }

    /**
     * handle requests to all shared info modules
     * @param $projectID
     * @param $uri
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function index($projectID, $uri) {
        //according to URI, redirect to appropriate function
        switch ($uri) {
            case "submittals":
                return ($this->submittals($projectID));
            case "rfis":
                return ($this->rfis($projectID));
            case "pcos":
                return ($this->pcos($projectID));
            case "contracts":
                return ($this->contracts($projectID));
            case "bids":
                return ($this->proposals($projectID));
            case "expediting-report":
                return ($this->expeditingReports($projectID));
            default:
                return ($this->projectInfo($projectID));
        }
    }

    /**
     * function for loading shared project info
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function projectInfo($projectID)
    {
        $project = $this->repository->getProject($projectID);
        $blogPosts = $this->blogRepository->getBlogPostsByProject($projectID);

        $id = $projectID;
        //get submittals
        $submittals = $this->submittalsRepository->getAllSubmittalsForProject($id, Auth::user()->comp_id);
        $submittalsStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($submittals as $submittal) {
            if (!empty($submittal) && !empty($submittal->status_short_name)) {

                if(in_array($submittal->status_short_name, Config::get('constants.all_open'))) {
                    $submittalsStatistics['Open']++;
                } else if (in_array($submittal->status_short_name, Config::get('constants.all_closed'))) {
                    $submittalsStatistics['Closed']++;
                } else {
                    $submittalsStatistics['Draft']++;
                }
            }
        }

        //get RFIS
        $rfis = $this->rfisRepository->getAllRFIsForProject($id, Auth::user()->comp_id);
        $rfisStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($rfis as $rfi) {
            if (!empty($rfi) && !empty($rfi->status_short_name)) {
                if (isset($rfisStatistics[$rfi->status_short_name])) {
                    $rfisStatistics[$rfi->status_short_name]++;
                } else {
                    $rfisStatistics[$rfi->status_short_name] = 1;
                }
            }
        }

        //get pcos statistics
        $pcos = $this->pcosRepository->getAllPcosForProject($id, Auth::user()->comp_id);
        $pcoStatistics = ['Open' => 0, 'Closed' => 0, 'Draft' => 0];
        foreach ($pcos as $pco) {
            if (!empty($pco->recipient) && !empty($pco->recipient->latest_recipient_version) && !empty($pco->recipient->latest_recipient_version->status)) {

                if(in_array($pco->recipient->latest_recipient_version->status->short_name, Config::get('constants.all_open'))) {
                    $pcoStatistics['Open']++;
                } else if (in_array($pco->recipient->latest_recipient_version->status->short_name, Config::get('constants.all_closed'))) {
                    $pcoStatistics['Closed']++;
                } else {
                    $pcoStatistics['Draft']++;
                }
            }
        }

        //get city by zip code
        $city = null;
        if (!empty($project->address) && !empty($project->address->zip)) {
            $city = $this->location->getCityByZipCode($project->address->zip);
        }

        if (empty($city) && !empty($project->address) && !empty($project->address->city) && !empty($project->address->state)) {
            $city = $this->location->getCityByNameAndState($project->address->city, $project->address->state);
        }

        if (Auth::user()->comp_id != $project->comp_id  && empty($city)) {
            if(!is_null($project->subcontractorAddress) && !empty($project->subcontractorAddress->zip)) {
                $city = $this->location->getCityByZipCode($project->subcontractorAddress->zip);
            }  else if (!empty($project->subcontractorAddress) && !empty($project->subcontractorAddress->city) && !empty($project->subcontractorAddress->state)) {
                $city = $this->location->getCityByNameAndState($project->subcontractorAddress->city, $project->subcontractorAddress->state);
            }
        }

        //return view('sharedProjects.view', compact('project'));
        return view('sharedProjects.view', compact('project', 'blogPosts','city',
            'pcoStatistics', 'submittalsStatistics', 'rfisStatistics'));
    }

    /**
     * function for loading shared submittals
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function submittals($projectID)
    {
        $project = $this->repository->getProject($projectID);
        $sort = Input::get('sort','number');
        $order = Input::get('order','asc');

        //get all project submittals
        $data['submittals'] = $this->submittalsRepository->getSharedProjectSubmittals($projectID, $sort, $order, 50);

        if (is_null($data['submittals'])) {
            $dataMessage = trans('labels.shared.noRecords');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        if (!$data['submittals']) {
            $dataMessage = trans('labels.shared.notAllowed');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        $checkUploadedFiles = new CheckUploadedFiles();
        foreach ($data['submittals'] as $key => $submittal){
            $data['submittals'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($submittal,  Config::get('constants.submittals_versions'));
        }

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $this->submittalsRepository->getAllSubmittalStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
        }

        //get project
        $data['project'] = $this->repository->getProject($projectID);

        $data['status'] = 'All';

        return view('sharedProjects.submittals.index', $data);
    }

    /**
     * Get all shares of the submittals
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param SubmittalsRepository $submittalsRepository
     * @return \Illuminate\View\View
     */
    public function submittalShares($projectId, ProjectRepository $projectRepository, SubmittalsRepository $submittalsRepository)
    {
        //get project
        $data['project'] = $projectRepository->getProject($projectId);

        //get all companies with which the logged company have shared some of their uploaded project files
        $data['sharedSubmittals'] = $submittalsRepository->getSharedSubmittals($projectId, Config::get('constants.company_type.subcontractor'), $data['project']->comp_id);

        return view('sharedProjects.submittals.submittal-shares', $data);
    }

    /**
     * function for loading shared rfis
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function rfis($projectID)
    {
        $project = $this->repository->getProject($projectID);
        $sort = Input::get('sort','number');
        $order = Input::get('order','asc');

        //get all project rfis
        $data['rfis'] = $this->rfisRepository->getSharedProjectRfis($projectID, $sort, $order, 50);

        if (is_null($data['rfis'])) {
            $dataMessage = trans('labels.shared.noRecords');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        if (!$data['rfis']) {
            $dataMessage = trans('labels.shared.notAllowed');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        $checkUploadedFiles = new CheckUploadedFiles();
        foreach ($data['rfis'] as $key => $rfi){
            $data['rfis'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($rfi,  Config::get('constants.rfis_versions'));
        }

        //get all rfi versions statuses and prepare them to array for the view
        $statuses = $this->rfisRepository->getAllRfiStatuses();
        $data['statuses']['All'] = 'All';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
        }

        //get project
        $data['project'] = $this->repository->getProject($projectID);

        $data['status'] = 'All';

        return view('sharedProjects.rfis.index', $data);
    }

    /**
     * Get all shares of the rfi's for all shared companies
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param RfisRepository $rfisRepository
     * @return \Illuminate\View\View
     * @internal param SubmittalsRepository $submittalsRepository
     */
    public function rfiShares($projectId, ProjectRepository $projectRepository, RfisRepository $rfisRepository)
    {
        //get project
        $data['project'] = $projectRepository->getProject($projectId);

        //get all companies with which the logged company have shared some of their uploaded project files
        $data['sharedRfis'] = $rfisRepository->getSharedRfis($projectId, Config::get('constants.company_type.subcontractor'), $data['project']->comp_id);

        return view('sharedProjects.rfis.rfi-shares', $data);
    }

    /**
     * function for loading shared rfis
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function pcos($projectID)
    {
        $project = $this->repository->getProject($projectID);
        $sort = Input::get('sort','number');
        $order = Input::get('order','asc');

        //get all project pcos
        $responseData = $this->pcosRepository->getSharedProjectPcos($projectID, $sort, $order, 50);
        $data['pcos'] = $responseData['sharedPcos'];
        $data['pcoPermissionsType'] = $responseData['pcoPermissionsType'];

        if (is_null($data['pcos'])) {
            $dataMessage = trans('labels.shared.noRecords');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        if (!$data['pcos']) {
            $dataMessage = trans('labels.shared.notAllowed');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        $checkUploadedFiles = new CheckUploadedFiles();

        foreach ($data['pcos'] as $pcoKey => $pco) {
            foreach ($pco['subcontractors'] as $key => $subcontractor){
                $data['pcos'][$pcoKey]['subcontractors'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($subcontractor, Config::get('constants.subcontractor_versions'));
            }
            $recipient = $checkUploadedFiles->checkAndOrderUploadedFiles($data['pcos'][$pcoKey]['recipient'], Config::get('constants.recipient_versions'));
            $data['pcos'][$pcoKey]->setRelation('recipient', $recipient);
        }

        //get all rfi versions statuses and prepare them to array for the view
        $statuses = $this->pcosRepository->getAllPcoStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
        }

        //get project
        $data['project'] = $this->repository->getProject($projectID);

        $data['status'] = 'All';

        return view('sharedProjects.pcos.index', $data);
    }

    /**
     * Get all shares of the pcos's for all shared companies
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param PcosRepository $pcosRepository
     * @return \Illuminate\View\View
     * @internal param SubmittalsRepository $submittalsRepository
     */
    public function pcoShares($projectId, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
    {
        //get project
        $data['project'] = $projectRepository->getProject($projectId);

        //get all companies with which the logged company have shared some of their uploaded project files
        $data['sharedPcos'] = $pcosRepository->getSharedPcos($projectId, Config::get('constants.company_type.subcontractor'), $data['project']->comp_id);

        return view('sharedProjects.pcos.pco-shares', $data);
    }

    /**
     * Get master submittals
     * @param $projectID
     * @param $submittalID
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function submittalVersions($projectID, $submittalID, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        $project = $this->repository->getProject($projectID);
        $submittal = $this->submittalsRepository->getSubmittal($submittalID);
        $submittal = $checkUploadedFiles->checkAndOrderUploadedFiles($submittal,  Config::get('constants.submittals_versions'));

        return view('sharedProjects.submittals.versions', compact('project', 'submittal'));

    }

    /**
     * Get master submittal version
     * @param $projectId
     * @param $submittalId
     * @param $versionId
     * @return \Illuminate\View\View
     */
    public function viewSubmittalVersion($projectId, $submittalId, $versionId)
    {
        $project = $this->repository->getProject($projectId);
        $submittalVersion = $this->submittalsRepository->getSubmittalVersion($versionId);

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $parentCompany = $this->companiesRepository->getCompanyWithUsersByID($project->comp_id);
        $projectUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

        $distributionListSubcontractor = $this->submittalsRepository->getSubmittalSubcontractorRecipientUserDistribution($submittalVersion->id, true);
        $distributionListRecipient = $this->submittalsRepository->getSubmittalSubcontractorRecipientUserDistribution($submittalVersion->id);


        return view('sharedProjects.submittals.view-version', compact('project', 'submittalVersion',
            'myCompany', 'projectUserIds', 'parentCompany', 'distributionListSubcontractor', 'distributionListRecipient'));
    }

    /**
     * @param $projectID
     * @param $rfiID
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function rfiVersions($projectID, $rfiID, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        $project = $this->repository->getProject($projectID);
        $rfi = $this->rfisRepository->getRfi($rfiID);
        $rfi = $checkUploadedFiles->checkAndOrderUploadedFiles($rfi,  Config::get('constants.rfis_versions'));

        return view('sharedProjects.rfis.versions', compact('project', 'rfi'));
    }

    /**
     * Get master rfi version
     * @param $projectId
     * @param $rfiId
     * @param $versionId
     * @return \Illuminate\View\View
     */
    public function viewRfiVersion($projectId, $rfiId, $versionId)
    {
        $project = $this->repository->getProject($projectId);
        $rfiVersion = $this->rfisRepository->getRfiVersion($versionId);

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $parentCompany = $this->companiesRepository->getCompanyWithUsersByID($project->comp_id);

        $projectUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

        $distributionListSubcontractor = $this->rfisRepository->getRfiSubcontractorRecipientUserDistribution($rfiVersion->id, true);
        $distributionListRecipient = $this->rfisRepository->getRfiSubcontractorRecipientUserDistribution($rfiVersion->id);



        return view('sharedProjects.rfis.view-version', compact('project', 'rfiVersion',
            'myCompany', 'parentCompany', 'projectUserIds', 'distributionListSubcontractor', 'distributionListRecipient'));
    }

    /**
     * Get shared pco details with subcontractors and recipient
     * @param $projectID
     * @param $pcoID
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function pcoDetails($projectID, $pcoID, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        $project = $this->repository->getProject($projectID);
        $responseData = $this->pcosRepository->getSharedPco($projectID, $pcoID);
        $pco = $responseData['pco'];
        foreach ($pco['subcontractors'] as $key => $subcontractor) {
            $pco['subcontractors'][$key] = $checkUploadedFiles->checkAndOrderUploadedFiles($subcontractor, Config::get('constants.subcontractor_versions'));
        }
        $pcoPermissionsType = $responseData['pcoPermissionsType'];

        return view('sharedProjects.pcos.details', compact('project', 'pco', 'pcoPermissionsType'));
    }

    /**
     * Get pco subcontractor with all its versions and last uploaded files
     * @param $projectId
     * @param $pcoId
     * @param $subcontractorId
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function pcoSubcontractorVersions($projectId, $pcoId, $subcontractorId, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        $project = $this->repository->getProject($projectId);
        $subcontractor = $this->pcosRepository->getPcoSubcontractor($pcoId, $subcontractorId);
        $subcontractor = $checkUploadedFiles->checkAndOrderUploadedFiles($subcontractor,  Config::get('constants.subcontractor_versions'));

        return view('sharedProjects.pcos.versions', compact('project', 'subcontractor'));
    }

    /**
     * Get pco recipient with all its versions and last uploaded files
     * @param $projectId
     * @param $pcoId
     * @param $recipientId
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @return \Illuminate\View\View
     */
    public function pcoRecipientVersions($projectId, $pcoId, $recipientId, CheckUploadedFilesInterface $checkUploadedFiles)
    {
        $project = $this->repository->getProject($projectId);
        $recipient = $this->pcosRepository->getPcoRecipient($pcoId, $recipientId);
        $recipient = $checkUploadedFiles->checkAndOrderUploadedFiles($recipient,  Config::get('constants.recipient_versions'));

        return view('sharedProjects.pcos.recipient-versions', compact('project', 'recipient'));
    }

    /**
     * Get master pco version
     * @param $projectId
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @return \Illuminate\View\View
     */
    public function viewPcoVersion($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId)
    {
        $project = $this->repository->getProject($projectId);
        $pco = $this->pcosRepository->getPco($pcoId);
        $pcoVersion = $this->pcosRepository->getPcoVersion($versionId);

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $parentCompany = $this->companiesRepository->getCompanyWithUsersByID($project->comp_id);

        $projectUsers = $this->manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

        $distributionListSubcontractor = $this->pcosRepository->getPcoSubcontractorRecipientUserDistribution($pcoVersion->id, true);
        $distributionListRecipient = $this->pcosRepository->getPcoSubcontractorRecipientUserDistribution($pcoVersion->id);

        return view('sharedProjects.pcos.view-version', compact('project', 'pcoVersion',
            'pcoSubcontractorRecipientId', 'pco', 'versionType', 'myCompany', 'parentCompany', 'projectUserIds',
            'distributionListSubcontractor', 'distributionListRecipient'));
    }

    /**
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     */
    public function filterSubmittals($projectId, FilterDataWrapper $wrapper)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $name = Input::get('name');
        $status = Input::get('status');

        //Set filter type
        $type = 'submittals-shared';

        //implement submittals filter
        $data['submittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'subcontractor', 'name', 'status', 'projectId'));
        $data['project'] = $this->repository->getProject($projectId);

        //get all submittal versions statuses and prepare them to array for the view
        $statuses = $this->submittalsRepository->getAllSubmittalStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
        }

        $data['status'] = $status;

        return view('sharedProjects.submittals.index', $data);

    }

    /**
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     */
    public function filterRfis($projectId, FilterDataWrapper $wrapper)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $name = Input::get('name');
        $status = Input::get('status');

        //Set filter type
        $type = 'rfis-shared';

        //implement rfis filter
        $data['rfis'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'subcontractor', 'name', 'status', 'projectId'));
        $data['project'] = $this->repository->getProject($projectId);

        //get all rfi versions statuses and prepare them to array for the view
        $statuses = $this->rfisRepository->getAllRfiStatuses();
        $data['statuses']['All'] = 'All';
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
        }

        $data['status'] = $status;

        return view('sharedProjects.rfis.index', $data);
    }

    /**
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     */
    public function filterPcos($projectId, FilterDataWrapper $wrapper)
    {
        $sort = Input::get('sort');
        $order = Input::get('order');

        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $recipient = Input::get('sub_id');
        $name = Input::get('name');
        $status = Input::get('status');

        //Set filter type
        $type = 'pcos-shared';

        //implement pcos filter
        $responseData = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'recipient', 'name', 'status', 'projectId', 'sort', 'order'));
        $data['pcos'] = $responseData['sharedPcos'];
        $data['pcoPermissionsType'] = $responseData['pcoPermissionsType'];
        $data['project'] = $this->repository->getProject($projectId);

        //get all pco versions statuses and prepare them to array for the view
        $statuses = $this->pcosRepository->getAllPcoStatuses();
        $data['statuses']['All'] = 'All';
        $data['statuses']['all_open'] = 'All Open';
        foreach ($statuses as $versionStatus) {
            $data['statuses'][$versionStatus->id] = $versionStatus->name;
        }

        $data['status'] = $status;

        return view('sharedProjects.pcos.index', $data);
    }

    /**
     * function for loading shared reports
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function expeditingReports($projectID)
    {
        $project = $this->repository->getProject($projectID);
        $sort = Input::get('sort', 'ID');
        $order = Input::get('order', 'asc');

        $materials = $this->materialsRepository->getSharedMaterialsByProject($projectID, $sort, $order);

        if (is_null($materials)) {
            $dataMessage = trans('labels.shared.noRecords');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        if (!$materials) {
            $dataMessage = trans('labels.shared.notAllowed');
            return view('sharedProjects.notAllowed', compact('project','dataMessage'));
        }

        $statuses = $this->submittalsRepository->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepository->getStatuses();

        return view('sharedProjects.materials.index', compact('project', 'materials', 'statuses', 'materialStatuses'));
    }

    /**
     * filter materials
     * @param $projectID
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filterMaterials($projectID, FilterDataWrapper $wrapper)
    {
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $name = Input::get('name');
        $location = Input::get('location');
        $status = Input::get('status');
        $erStatus = Input::get('er_status');

        //Set filter type
        $type = 'materials-shared';
        $projectId = $projectID;
        //implement materials filter
        $materials = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)),
            compact('masterFormat', 'subcontractor', 'status', 'name', 'location', 'projectId', 'erStatus'));
        $project = $this->repository->getProject($projectID);
        $statuses = $this->submittalsRepository->getSubmittalStatuses();
        $materialStatuses = $this->materialsRepository->getStatuses();

        return view('sharedProjects.materials.index', compact('materials', 'project', 'status', 'statuses', 'materialStatuses'));
    }


    /**
     * function for loading project files
     * @param $projectID
     * @param $fileType
     * @param ProjectFilesRepository $filesRepository
     * @param ProjectFilesMap $map
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function projectFiles($projectID, $fileType, ProjectFilesRepository $filesRepository, ProjectFilesMap $map)
    {
        $project = $this->repository->getProjectRecord($projectID);
        $type = $filesRepository->getFileTypeByDisplayName($fileType);
        $sort = Input::get('sort','files.id');
        $order = Input::get('order','desc');

        //new business logic
        //if module is allowed get all files of type
        $permissions = CompanyPermissions::can('read', $type->display_name);
        if ($permissions) {
            $projectFiles = $filesRepository->getAllAllowedProjectFilesByType($projectID, $project->comp_id, $type->display_name, $sort, $order, Config::get('constants.shared_files.master'), $permissions);
        }
        //if module is not allowed get only shared files
        else {
            $projectFiles = $filesRepository->getOnlySharedProjectFilesByType($projectID, $project->comp_id, $type->display_name, $sort, $order, Config::get('constants.shared_files.master'));
        }
        //create new project file link
        $createLink = $map->createLinkMap($projectID, $type->display_name);


        return view('sharedProjects.project_files.index', compact('project', 'type', 'projectFiles', 'createLink'));
    }

    /**
     * get shared project files
     * @param $projectID
     * @param $fileType
     * @param ProjectFilesRepository $filesRepository
     * @param ProjectFilesMap $map
     * @return \Illuminate\View\View
     */
    public function projectFileShares($projectID, $fileType, ProjectFilesRepository $filesRepository, ProjectFilesMap $map)
    {
        $project = $this->repository->getProjectRecord($projectID);
        $type = $filesRepository->getFileTypeByDisplayName($fileType);

        //get all files shared with me, that I have shared further
        $projectFiles = $filesRepository->getSharedReceivedProjectFilesByType($projectID, $type->display_name);

        //create new project file link
        $createLink = $map->createLinkMap($projectID, $type->display_name);

        return view('sharedProjects.project_files.shares', compact('project', 'type', 'projectFiles', 'createLink'));
    }

    /**
     * Get received files specified by type
     * @param $projectID
     * @param $fileType
     * @param ProjectFilesRepository $filesRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param PcosRepository $pcosRepository
     * @return \Illuminate\View\View|string
     */
    public function receivedFiles($projectID, $fileType, ProjectFilesRepository $filesRepository, SubmittalsRepository $submittalsRepository, RfisRepository $rfisRepository, PcosRepository $pcosRepository)
    {
        $receivedFilesType = Request::segment(5);

        $project = $this->repository->getProjectRecord($projectID);
        $type = $filesRepository->getFileTypeByDisplayName($fileType);
        $sort = Input::get('sort','files.id');
        $order = Input::get('order','desc');

        switch ($receivedFilesType) {
            case "submittals":
                $projectFiles = $submittalsRepository->getReceivedSubmittals($projectID, $project->comp_id, $sort, $order);
                break;
            case "rfis":
                $projectFiles = $rfisRepository->getReceivedRfis($projectID, $project->comp_id, $sort, $order);
                break;
            case "pcos":
                $projectFiles = $pcosRepository->getReceivedPcos($projectID, $project->comp_id, $sort, $order);
                break;
            default:
                $projectFiles = $filesRepository->getOnlySharedProjectFilesByType($projectID, $project->comp_id, $type->display_name, $sort, $order, Config::get('constants.shared_files.received'));
        }

        return view('sharedProjects.project_files.received-index', compact('project', 'type', 'projectFiles'));
    }


    /**
     * get shared contracts
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function contracts($projectID)
    {
        $project = $this->repository->getProjectRecord($projectID);

        $sort = Input::get('sort','ID');
        $order = Input::get('order','asc');
        $contracts = $this->contractsRepository->getSharedContractsByProject($projectID, $project->comp_id, $sort, $order);
        if (!$contracts) {
                return view('sharedProjects.notAllowed', compact('project'));
        }
        $project = $this->repository->getProject($projectID);
        return view('sharedProjects.contracts.index',compact('project','contracts'));
    }

    /**
     * get shared contract files
     * @param $projectID
     * @param $contractID
     * @return \Illuminate\View\View
     */
    public function contractFiles($projectID, $contractID)
    {
        $project = $this->repository->getProject($projectID);
        $contract = $this->contractsRepository->getSharedContract($projectID, $contractID);
        $files = $this->contractsRepository->getSharedContractFiles($projectID, $contractID);

        return view('sharedProjects.contracts.files',compact('files','project','contract'));
    }

    /**
     * Get master proposals
     * @param $projectID
     * @return \Illuminate\View\View
     */
    public function proposals($projectID)
    {
        $moduleId = $this->proposalsRepository->getModuleId();
        $companyPermissions = $this->permissionsRepository->checkModulePermissions($projectID, $moduleId, Auth::user()->comp_id);

        if (!$companyPermissions) {
            return view('errors.unauthorized');
        }

        $project = $this->repository->getProjectRecord($projectID);

        $sort = Input::get('sort','ID');
        $order = Input::get('order','asc');
        $proposals = $this->proposalsRepository->getSharedProposalsByProject($projectID, $project->comp_id, $sort, $order);
        if (!$proposals) {
            return view('sharedProjects.notAllowed', compact('project'));
        }
        $project = $this->repository->getProject($projectID);
        return view('sharedProjects.proposals.index',compact('project','proposals'));
    }

    /**
     * Filter bid items
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function filterBids($projectId, FilterDataWrapper $wrapper)
    {
        $sort = Input::get('sort','id');
        $order = Input::get('order','asc');

        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $status = Input::get('status');
        $bidName = Input::get('bid_name');
        $bidCompany = Input::get('bid_company');

        //Set filter type
        $type = 'proposals';

        //implement proposals filter
        $data['proposals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('sort', 'order', 'masterFormat', 'status', 'projectId', 'bidName', 'bidCompany'));
        $data['project'] = $this->repository->getProject($projectId);

        return view('sharedProjects.proposals.index', $data);
    }

    /**
     * Show master bid item
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function showBid($projectId, $id)
    {
        $moduleId = $this->proposalsRepository->getModuleId();
        $companyPermissions = $this->permissionsRepository->checkModulePermissions($projectId, $moduleId, Auth::user()->comp_id);

        if (!$companyPermissions) {
            return view('errors.unauthorized');
        }

        $companyReadPermissions = $this->permissionsRepository->checkWriteModulePermissions($projectId, $moduleId, Auth::user()->comp_id);

        $proposal = $this->proposalsRepository->getProposal($id);
        $project = $this->repository->getProject($projectId);

        $addressBookEntry = $this->addressBookRepository->getAddressBookEntryBySyncedCompany(Auth::user()->comp_id, $project->comp_id);
        if ($addressBookEntry) {
            $supplier = $this->proposalsRepository->getSharedSupplierByAbId($projectId, $id, $addressBookEntry->id);
            $address_book = $addressBookEntry;
        } else {
            $supplier = null;
            $address_book = null;
        }

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $parentCompany = $this->companiesRepository->getCompanyWithUsersByID($project->comp_id);

        if (!empty($supplier)) {
            $distributionList1 = $this->proposalsRepository->getProposalUserDistributionByBidder($supplier->id,1);
            $distributionList2 = $this->proposalsRepository->getProposalUserDistributionByBidder($supplier->id,2);
            $distributionList3 = $this->proposalsRepository->getProposalUserDistributionByBidder($supplier->id,3);
        }

        return view('sharedProjects.proposals.view',compact('project','proposal', 'supplier',
            'address_book', 'companyReadPermissions', 'myCompany', 'parentCompany', 'distributionList1',
            'distributionList2', 'distributionList3'));
    }

    /**
     * load view for proposal supplier record
     * @param $projectId
     * @param $id
     * @param $bidderId
     * @return \Illuminate\View\View
     */
    public function showBidder($projectId, $id, $bidderId)
    {
        $moduleId = $this->proposalsRepository->getModuleId();
        $companyPermissions = $this->permissionsRepository->checkModulePermissions($projectId, $moduleId, Auth::user()->comp_id);

        if (!$companyPermissions) {
            return view('errors.unauthorized');
        }

        $companyReadPermissions = $this->permissionsRepository->checkWriteModulePermissions($projectId, $moduleId, Auth::user()->comp_id);

        $supplier = $this->proposalsRepository->getSharedSupplier($projectId, $bidderId);
        $project = $this->repository->getProject($projectId);
        //load address book entries with contacts and addresses
        $address_book = $this->addressBookRepository->getEntryContactsAddresses($supplier->ab_id);

        if (is_null($supplier)) {
            return view('errors.unauthorized');
        }

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        $parentCompany = $this->companiesRepository->getCompanyWithUsersByID($project->comp_id);

        $distributionList1 = $this->proposalsRepository->getProposalUserDistributionByBidder($bidderId,1);
        $distributionList2 = $this->proposalsRepository->getProposalUserDistributionByBidder($bidderId,2);
        $distributionList3 = $this->proposalsRepository->getProposalUserDistributionByBidder($bidderId,3);

        return view('sharedProjects.proposals.view-supplier',compact('supplier','project','address_book',
            'companyReadPermissions', 'myCompany', 'parentCompany', 'distributionList1', 'distributionList2', 'distributionList3'));
    }

    /**
     * send notification email for new proposal
     * @param $id
     * @param Mailer $mailer
     * @return Redirect
     */
    public function sendBidProposal($projectId, $id, $bidderId, $proposalNumber, Mailer $mailer)
    {
        if ($bidderId) {
            $checkedUsers = Request::input('values');
            $fileId = Request::input('fileId');
            $filePath = Request::input('filePath');
            $filePath = str_replace('#', '%23', $filePath);

            $project = $this->repository->getProject($projectId);
            $bid = $this->proposalsRepository->getProposalById($id);

            $sentTo = [];
            $sentToUsers = [];
            foreach ($checkedUsers as $userId) {
                $user = $this->usersRepository->getUserByID($userId);
                $sentTo[] = $user->name.' - '.$user->name.' ('.Carbon::now()->format('m/d/Y').')';
                $sentToUsers[] = $user;
            }

            $emailData = [
                'bidId' => $bid->id,
                'subject' => $project->name.' - Bids: '.$bid->name,
                'bidName' => $bid->name,
                'projectName' => $project->name,
                'projectId' => $project->id,
                'companyId' => '',
                'sentTo' => $sentTo,
                'fileId' => $fileId,
                'filePath' => $filePath
            ];

            $user = $this->usersRepository->getUserByID(Auth::user()->id);

            //prepare data for sending email
            $mailData = array(
                'companyInfo' => $user->name . " (" . $user->company->name . ") via cloud-pm.com"
            );

            foreach ($sentToUsers as $user) {
                $mailer->sendBidProposalNotification($emailData, $user->email, $mailData);
                $this->proposalsRepository->addProposalUserDistribution($bidderId, $user->id, $proposalNumber);
            }

            //show appropriate message
            Flash::success(trans('messages.bidsProposals.notifications.success'));
            return Response::json(['status' => 'success', 'message' => trans('messages.bidsProposals.notifications.success'), 200]);
        }
    }

    /**
     * send notification email for new proposal
     * @param $id
     * @param Mailer $mailer
     * @return Redirect
     */
    public function sendProposalNoificationsPco($projectId, $module, $id, $subcontractorId, $versionId, $type, Mailer $mailer)
    {
        return $this->sendProposalNoifications($projectId, $module, $id, $versionId, $type, $mailer);
    }

    /**
     * send notification email for new proposal
     * @param $projectId
     * @param $module
     * @param $id
     * @param $versionId
     * @param $type
     * @param Mailer $mailer
     * @param ProjectFilesRepository $projectFilesRepository
     * @return Redirect
     */
    public function sendProposalNoifications($projectId, $module, $id, $versionId, $type, Mailer $mailer, ProjectFilesRepository $projectFilesRepository)
    {
        if ($versionId) {
            $checkedUsers = Request::input('values');
            $fileId = Request::input('fileId');
            $filePath = Request::input('filePath');
            $filePath = str_replace('#', '%23', $filePath);

            $project = $this->repository->getProject($projectId);
            if ($module == 'submittals') {
                $moduleItem = $this->submittalsRepository->getSubmittal($id);
                $moduleName = 'submittal';
            } else if ($module == 'rfis') {
                $moduleItem = $this->rfisRepository->getRfi($id);
                $moduleName = 'rfi';
            } else if ($module == 'pcos') {
                $moduleItem = $this->pcosRepository->getPco($id);
                $moduleName = 'pco';
            } else {
                return null;
            }


            $sentTo = [];
            $sentToUsers = [];
            foreach ($checkedUsers as $userId) {
                $user = $this->usersRepository->getUserByID($userId);
                $sentTo[] = $user->name.' - '.$user->name.' ('.Carbon::now()->format('m/d/Y').')';
                $sentToUsers[] = $user;
            }

            $projectFile = $projectFilesRepository->getProjectFile($projectId, $fileId);
            $fileName = $projectFile->file_name;

            $emailData = [
                'itemId' => $moduleItem->id,
                'subject' => $project->name.' - '.strtoupper($moduleName).': '.$moduleItem->name,
                'itemName' => $moduleItem->name,
                'projectName' => $project->name,
                'fileName' => $fileName,
                'projectId' => $project->id,
                'companyId' => '',
                'sentTo' => $sentTo,
                'fileId' => $fileId,
                'filePath' => $filePath,
                'module' => $moduleName
            ];

            $user = $this->usersRepository->getUserByID(Auth::user()->id);

            //prepare data for sending email
            $mailData = array(
                'companyInfo' => $user->name . " (" . $user->company->name . ") via cloud-pm.com"
            );

            foreach ($sentToUsers as $user) {
                $mailer->sendGeneralProposalNotification($emailData, $user->email, $mailData);

                if ($module == 'submittals') {
                    $moduleItem = $this->submittalsRepository->addProposalUserDistribution($versionId, $user->id, ($type == 'subcontractor'));
                } else if ($module == 'rfis') {
                    $moduleItem = $this->rfisRepository->addProposalUserDistribution($versionId, $user->id, ($type == 'subcontractor'));
                } else if ($module == 'pcos') {
                    $moduleItem = $this->pcosRepository->addProposalUserDistribution($versionId, $user->id, ($type == 'subcontractor'));
                }
            }

            //show appropriate message
            Flash::success(trans('messages.bidsProposals.notifications.success'));
            return Response::json(['status' => 'success', 'message' => trans('messages.bidsProposals.notifications.success'), 200]);
        }
    }

    /**
     * update supplier
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param ProposalsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateBidder($projectId, $id, $supplierId, ProposalsRequest $request)
    {
        $moduleId = $this->proposalsRepository->getModuleId();
        $companyPermissions = $this->permissionsRepository->checkWriteModulePermissions($projectId, $moduleId, Auth::user()->comp_id);

        if (!$companyPermissions) {
            return view('errors.unauthorized');
        }

        $data = $request->all();
        $supplier = $this->proposalsRepository->updateSupplier($supplierId, $data);

        if ($supplier) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'bidder']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'bidder']));
        }

        Flash::success('The Bid Item was successfully updated.');

        return redirect('projects/'.$projectId.'/shared/bids/'.$id);

    }

    /**
     * Generate shared submittals pdf report
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param SubmittalsRepository $submittalsRepository
     * @return mixed
     * @throws \Exception
     */
    public function savePdfSubmittals($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, SubmittalsRepository $submittalsRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort','version_submittal_no');
        $order = Input::get('order','asc');

        //get filter parameters
        $masterFormat = Input::get('report_mf');
        $subcontractor = Input::get('report_sub_id');
        $subcontractorName = Input::get('report_sub_name');
        $statusId = Input::get('report_status');
        $data['printWithNotes'] = Input::get('print_with_notes');

        //get submittal version status by id
        $status = ($statusId == 'All') ? 'All' : $statusId;

        //Set report type
        $type = 'submittals-shared-report';

        //Get submittals
        $data['submittals'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.submittals.report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if ($masterFormat != '') {
            $data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
        }
        if ($subcontractorName != '') {
            $data['title'] = $data['title'].'Subcontractor: '.$subcontractorName.", ";
        }

        //get status from database table
        if ($statusId == 'All') {
            $statusString = 'All';
        } else if ($statusId == 'all_open') {
            $statusString = 'All Open';
        } else {
            $statusRow = $submittalsRepository->getStatusById($statusId);
            $statusString = $statusRow->name;
        }


        $data['title'] = $data['title'].'Status: '.$statusString;

        //Set report name
        $name = "Submittals";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * Generate shared rfis pdf report
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param RfisRepository $rfisRepository
     * @return mixed
     * @throws \Exception
     */
    public function savePdfRfis($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, RfisRepository $rfisRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort','version_rfi_no');
        $order = Input::get('order','asc');

        //get filter parameters
        $masterFormat = Input::get('report_mf');
        $subcontractor = Input::get('report_sub_id');
        $subcontractorName = Input::get('report_sub_name');
        $statusId = Input::get('report_status');
        $data['printWithNotes'] = Input::get('print_with_notes');

        //get rfi version status by id
        $status = ($statusId == 'All') ? 'All' : $statusId;

        //Set report type
        $type = 'rfis-shared-report';

        //Get rfis
        $data['rfis'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'sort', 'order'));

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'projects.rfis.report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if ($masterFormat != '') {
            $data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
        }
        if ($subcontractorName != '') {
            $data['title'] = $data['title'].'Subcontractor: '.$subcontractorName.", ";
        }

        //get status from database table
        if ($statusId == 'All') {
            $statusString = 'All';
        } else {
            $statusRow = $rfisRepository->getStatusById($statusId);
            $statusString = $statusRow->name;
        }


        $data['title'] = $data['title'].'Status: '.$statusString;

        //Set report name
        $name = "RFI's";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * Generate shared pcos pdf report
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ProjectRepository $projectRepository
     * @param ReportGenerator $generator
     * @param PcosRepository $pcosRepository
     * @return mixed
     * @throws \Exception
     */
    public function savePdfPcos($projectId, FilterDataWrapper $wrapper, ProjectRepository $projectRepository, ReportGenerator $generator, PcosRepository $pcosRepository)
    {
        //default sort and order unless selected
        $sort = Input::get('sort');
        $order = Input::get('order');

        //get filter parameters
        $masterFormat = Input::get('report_mf');
        $recipient = Input::get('report_sub_id');
        $recipientName = Input::get('report_sub_name');
        $statusId = Input::get('report_status');
        $data['printWithNotes'] = Input::get('print_with_notes');

        //get pcos version status by id
        $status = ($statusId == 'All') ? 'All' : $statusId;

        //Set report type
        $type = 'pcos-shared-report';

        //get response
        $responseData = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'recipient', 'status', 'projectId', 'sort', 'order'));

        //Get pcos
        $data['pcos'] = $responseData['sharedPcos'];

        //get proper permission type
        $data['pcoPermissionsType'] = $responseData['pcoPermissionsType'];

        //Get project
        $data['project'] = $projectRepository->getProject($projectId);

        //Set report view
        $view = 'sharedProjects.pcos.report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if ($masterFormat != '') {
            $data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
        }
        if ($recipientName != '') {
            $data['title'] = $data['title'].'Recipient: '.$recipientName.", ";
        }

        //get status from database table
        if ($statusId == 'All') {
            $statusString = 'All';
        } else if ($statusId == 'all_open') {
            $statusString = 'All Open';
        } else {
            $statusRow = $pcosRepository->getStatusById($statusId);
            $statusString = $statusRow->name;
        }

        $data['title'] = $data['title'].'Status: '.$statusString;

        //Set report name
        $name = "PCO's";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * open pdf file in new tab
     * generate report by filtered parameters
     * @param $projectId
     * @param FilterDataWrapper $wrapper
     * @param ReportGenerator $generator
     * @return mixed
     * @throws \Exception
     */
    public function reportMaterials($projectId, FilterDataWrapper $wrapper, ReportGenerator $generator)
    {
        //get filter parameters
        $masterFormat = Input::get('master_format_search');
        $subcontractor = Input::get('sub_id');
        $subcontractorName = Input::get('supplier_subcontractor');
        $status = Input::get('status');
        $erStatus = Input::get('erStatus');

        //Set report type
        $type = 'materials-shared-report';

        //Get materials/services
        $data['materials'] = $wrapper->implementFilter(ClassMap::instance(Config::get('classmap.filter.'.$type)), compact('masterFormat', 'subcontractor', 'status', 'projectId', 'erStatus'));
        //Get project
        $data['project'] = $this->repository->getProject($projectId);
        $data['statuses'] = $this->submittalsRepository->getSubmittalStatuses();
        $data['materialStatuses'] = $this->materialsRepository->getStatuses();

        //Set report view
        $view = 'projects.materials.report';

        //Set report title
        $data['title'] = '';

        //Create full report title
        if($masterFormat != '') {
            $data['title'] = $data['title'].' Master Format Number and Title: '.$masterFormat.", ";
        }
        if($subcontractorName != '') {
            $data['title'] = $data['title'].'Subcontractor: '.$subcontractorName.", ";
        }
        $data['title'] = $data['title'].'Status: '.$status;

        //Set report name
        $name = "Expediting_Report";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * Change (update) subcontractor's project number for the shared project
     * @param ProjectRepository $projectRepository
     */
    public function changeProjectNumber(ProjectRepository $projectRepository)
    {
        //get parameters
        $projectNumberId = Input::get('sub_proj_number_id');
        $projectNumber = Input::get('sub_proj_number');

        $response = $projectRepository->updateSubcontractorProjectNumber($projectNumberId, $projectNumber);

        if ($response) {
            return Response::json(array('error' => 0, 'number' => $response->sub_proj_number));
        }

        return Response::json(array('error' => 1, 'number' => 'false'));
    }

    /**
     * Get shared project number
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
    public function getProjectNumber(ProjectRepository $projectRepository)
    {
        //get parameters
        $projectNumberId = Input::get('sub_proj_number_id');

        $response = $projectRepository->getSubcontractorProjectNumber($projectNumberId);

        if ($response) {
            return Response::json(array('error' => 0, 'number' => $response->sub_proj_number));
        }

        return Response::json(array('error' => 1, 'number' => 'false'));
    }
}