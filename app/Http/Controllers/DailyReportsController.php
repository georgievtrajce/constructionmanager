<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\DailyRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Daily_reports\Implementations\DailyReport;
use App\Modules\Daily_reports\Repositories\DailyReportsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class DailyReportsController extends Controller {


    public function __construct(CompaniesRepository $companiesRepository)
    {
        $this->middleware('auth' ,['except' => 'showReport']);

        $this->middleware('daily_reports_write', ['only' => ['create','store','editReportRow','edit','update','copyLastReport','uploadDailyReportFile','storeDailyReportFile','editDailyReportFile','updateDailyReportFile']]);
        $this->middleware('daily_reports_delete', ['only' => ['destroy', 'deleteReportRow', 'deleteFile']]);

        if (Auth::user() != null) {
            $this->companyId = Auth::user()->comp_id;
            $this->userId = Auth::user()->id;
        }

        $this->companiesRepository = $companiesRepository;
    }

    /**
     * @param $projectId
     * @param DailyRequest $request
     * @param DailyReportsRepository $dailyReportsRepository
     * @return mixed
     */
    public function store($projectId, DailyRequest $request, DailyReportsRepository $dailyReportsRepository)
    {
        $dailyReport = $dailyReportsRepository->storeGeneralInfo($projectId, $request->all());

        if ($dailyReport) {
            Flash::success(trans('messages.daily_reports.store.success', ['item' => 'Daily Report']));
            return Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReport->id.'/edit');
        }

        Flash::error(trans('messages.daily_reports.store.error', ['item' => 'Daily Report']));
        return Redirect::to('projects/'.$projectId.'/daily-report/create');
    }

    /**
     * @param $projectId
     * @param $dailyReportId
     * @param ProjectRepository $projectRepository
     * @param DailyReportsRepository $dailyReportsRepository
     * @param ManageUsersRepository $manageUsersRepository
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $dailyReportId, ProjectRepository $projectRepository, DailyReportsRepository $dailyReportsRepository, ManageUsersRepository $manageUsersRepository)
    {
        $data['project'] = $projectRepository->getProject($projectId);
        $data['daily_report'] = $dailyReportsRepository->getDailyReport($dailyReportId);
        $data['daily_report']['day_of_week'] = Config::get('constants.carbon_days.'.Carbon::parse($data['daily_report']['date'])->dayOfWeek);

        $data['my_company'] = $projectRepository->getProjectWithUserPermissions($projectId);
        $projectPermissionUsers = $manageUsersRepository->getProjectPermissionUsers($projectId);

        //users with privileges from project permissions
        $projectPermissionUsersArray = [];
        if(count($projectPermissionUsers) > 0)
        foreach ($projectPermissionUsers as $projectPermissionUser){
            $projectPermissionUsersArray[] = $projectPermissionUser->user_id;
        }

        $data['companies'] = [];
        $data['users'] = [];
        $data['project_companies'] = [];
        //my company on first option
        $data['project_companies'][$data['my_company']->company->id] = $data['my_company']->company->name;
        foreach ($data['project']->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $data['companies'][$subContractor->id] = $subContractor->address_book->name;
                $data['users'][$subContractor->id] = $subContractor->project_company_contacts;
                $data['project_companies'][$subContractor->id] = $subContractor->address_book->name;
            }
        }

        $users = $manageUsersRepository->getCompanyUsers(Auth::user()->comp_id);
        $data['worker_offices'] = [];
        $data['worker_users'] = [];
        $data['worker_titles'] = [];
        //current logged user
        $data['worker_users'][$data['my_company']->company->currentUser->address_id][$data['my_company']->company->currentUser->id] = $data['my_company']->company->currentUser->name;
        $data['worker_titles'][$data['my_company']->company->currentUser->address_id][$data['my_company']->company->currentUser->id] = $data['my_company']->company->currentUser->title;
        foreach ($users as $user) {
            $data['worker_offices'][$user->address_id] = ['id' => $user->address_id, 'name' => $user->address->office_title];
            if(in_array($user->id, $projectPermissionUsersArray)) {
                $data['worker_users'][$user->address_id][$user->id] = $user->name;
                $data['worker_titles'][$user->address_id][$user->id] = $user->title;
            }
        }
        $data['worker_offices'] = array_values($data['worker_offices']);

        return view('projects.daily_reports.edit', $data);
    }

    /**
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @return \Illuminate\View\View
     */
    public function create($projectId, ProjectRepository $projectRepository, DailyReportsRepository $dailyReportsRepository)
    {
        //get project
        $data['project'] = $projectRepository->getProject($projectId);
        $data['report_num'] = $dailyReportsRepository->getGeneratedNumbers($projectId);

        return view('projects.daily_reports.create', $data);
    }

    /**
     * @param $projectId
     * @param $dailyReportId
     * @param DailyRequest $request
     * @param DailyReportsRepository $dailyReportsRepository
     * @return mixed
     */
    public function update($projectId, $dailyReportId, DailyRequest $request, DailyReportsRepository $dailyReportsRepository)
    {
        $currentStep = $request->input('current_step');
        $updateId = $request->input('update');

        switch ($currentStep) {
            case 1: //general-info
                $dailyReport = $dailyReportsRepository->updateGeneralInfo($projectId, $dailyReportId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=1');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=1');
                break;
            case 2: //weather-info
                $dailyReport = $dailyReportsRepository->updateWeatherAndGroundConditions($dailyReportId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=2');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=2');
                break;
            case 3: //contractor
                $data = $request->all();
                //Tab 1
                if($request->input('contractor_tab') == Config::get('constants.workers-with-name')) {
                    (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeContractorLabor($projectId, $dailyReportId, $data) :
                        $dailyReport = $dailyReportsRepository->updateContractorLabor($projectId, $dailyReportId, $updateId, $data);
                    $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=3&tab=1');
                    $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=3&tab=1');
                }
                //Tab 2
                else {
                    (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeContractorTradeLabor($projectId, $dailyReportId, $data) :
                        $dailyReport = $dailyReportsRepository->updateContractorTradeLabor($projectId, $dailyReportId, $updateId, $data);
                    $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=3&tab=2');
                    $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=3&tab=2');
                }
                break;
            case 4: //subcontractor-contractor
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeSubContractorLabor($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateSubContractorLabor($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=4');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=4');
                break;
            case 5: //equipment-on-site
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeEquipmentOnSite($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateEquipmentOnSite($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=5');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=5');
                break;
            case 6: //construction-materials
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeConstructionMaterials($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateConstructionMaterials($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=6');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=6');
                break;
            case 7: //testing-inspections
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeInspections($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateInspections($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=7');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=7');
                break;
            case 8: //safety-observations
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeObservations($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateObservations($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=8');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=8');
                break;
            case 9: //verbal-instruction
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeVerbalInstructions($projectId, $dailyReportId, $request->all()) :
                    $dailyReport = $dailyReportsRepository->updateVerbalInstructions($projectId, $dailyReportId, $updateId, $request->all());
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=9');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=9');
                break;
            case 10: //general-notes
                $dailyReport = $dailyReportsRepository->updateGeneralNotes($dailyReportId, $request->only('general_notes'));
                $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=10');
                $redirectError = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=10');
                break;
            case 11: //gallery
            case 12: //attachments
                $data = $request->all();
                ($currentStep == 11) ? $data['file_type_id'] = Config::get('constants.daily_report_image_file_type_id') : $data['file_type_id'] = Config::get('constants.daily_report_pdf_file_type_id');
                ($currentStep == 11) ? $data['daily_type'] = 'image' : $data['daily_type'] = 'pdf';
                (empty($updateId)) ? $dailyReport = $dailyReportsRepository->storeFiles($projectId, $dailyReportId, $data) :
                    $dailyReport = $dailyReportsRepository->updateDailyReportFile($projectId, $dailyReportId, $updateId, $data);
                ($currentStep == 11) ? $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=11') :
                    $redirectSuccess = Redirect::to('projects/'.$projectId.'/daily-report/'.$dailyReportId.'/edit?step=12');
                    $redirectError = Redirect::to('projects/'.$projectId.'/project-files/test-reports/file');
                break;
            default:
                $dailyReport = false;
                break;
        }

        if ($dailyReport) {
            //Successfully updated submittal
            Flash::success(trans('messages.daily_reports.update.success', ['item' => 'Daily Report']));
            return $redirectSuccess;
        }

        Flash::error(trans('messages.daily_reports.update.error', ['item' => 'Daily Report']));
        return $redirectError;
    }

    public function editReportRow(Request $request, DailyReportsRepository $dailyReportsRepository)
    {
        $type = Input::get('type');
        $projectId = Input::get('projectId');
        $dailyReportId = Input::get('dailyReportId');
        $rowId = Input::get('rowId');
        $step = Input::get('step');
        $tab = Input::get('tab'); //only for step 3

        switch ($step) {
            case 3: //contractor
                if($tab == 1) {
                    $dailyReport = $dailyReportsRepository->getContractorLabor($rowId);
                } else {
                    $dailyReport = $dailyReportsRepository->getContractorTradeLabor($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 4: //subcontractor-contractor
                $dailyReport = $dailyReportsRepository->getSubContractorLaborRow($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 5: //equipment-on-site
                $dailyReport = $dailyReportsRepository->getEquipmentOnSite($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 6: //construction-materials
                $dailyReport = $dailyReportsRepository->getConstructionMaterials($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 7: //testing-inspections
                $dailyReport = $dailyReportsRepository->getInspections($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 8: //safety-observations
                $dailyReport = $dailyReportsRepository->getObservations($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 9: //verbal-instruction
                $dailyReport = $dailyReportsRepository->getVerbalInstructions($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            case 12: //attachments
                $dailyReport = $dailyReportsRepository->getDailyReportFile($rowId);
                return Response::json(array('error' => 0, 'message' => 'Success', 'response' => $dailyReport));
                break;
            default:
                return Response::json(array('error' => 1, 'message' => 'No data found'));
                break;
        }
    }

    public function deleteReportRow(Request $request, DailyReportsRepository $dailyReportsRepository)
    {
        $type = Input::get('type');
        $projectId = Input::get('projectId');
        $dailyReportId = Input::get('dailyReportId');
        $rowId = Input::get('rowId');
        $step = Input::get('step');
        $tab = Input::get('tab'); //only for step 3

        switch ($step) {
            case 3: //contractor
                if($tab == 1) {
                    if($type == 'multi')
                    {
                        $rowIdsArray = explode('-', $rowId);
                        $dailyReportsRepository->deleteContractorMultipleRows($rowIdsArray);

                    } else {
                        $dailyReportsRepository->deleteContractorLaborRow($rowId);
                    }
                } else if($tab == 2) {
                    if($type == 'multi')
                    {
                        $rowIdsArray = explode('-', $rowId);
                        $dailyReportsRepository->deleteContractorTradeMultipleRows($rowIdsArray);

                    } else {
                        $dailyReportsRepository->deleteContractorLaborTradeRow($rowId);
                    }
                }

                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step, 'tab' => $tab));
                break;
            case 4: //subcontractor-contractor
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteSubContractorMultipleRows($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteSubContractorLaborRow($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 5: //equipment-on-site
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteMultipleEquipmentOnSite($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteEquipmentOnSite($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 6: //construction-materials
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteMultipleConstructionMaterials($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteConstructionMaterials($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 7: //testing-inspections
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteMultipleInspections($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteInspections($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 8: //safety-observations
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteMultipleObservations($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteObservations($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 9: //verbal-instruction
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    $dailyReportsRepository->deleteMultipleVerbalInstructions($rowIdsArray);

                } else {
                    $dailyReportsRepository->deleteVerbalInstructions($rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            case 11: //gallery
            case 12: //attachments
                if($type == 'multi')
                {
                    $rowIdsArray = explode('-', $rowId);
                    foreach ($rowIdsArray as $id)
                    {
                        $dailyReportsRepository->deleteDailyReportFile($projectId, $dailyReportId, $id);
                    }
                } else {
                    $dailyReportsRepository->deleteDailyReportFile($projectId, $dailyReportId, $rowId);
                }
                return Response::json(array('error' => 0, 'message' => 'Success', 'step' => $step));
                break;
            default:
                return Response::json(array('error' => 1, 'message' => 'No data found'));
                break;
        }
    }

    public function copyLastReport($projectId, DailyReport $dailyReport)
    {
        $copiedLastReport = $dailyReport->copyLastReport($projectId);

        if($copiedLastReport) {
            Flash::success(trans('messages.daily_reports.copy_report.success', ['item' => 'Daily Report']));
            return Redirect::to('projects/'.$projectId.'/daily-report/'.$copiedLastReport->id.'/edit');
        }

        Flash::error(trans('messages.daily_reports.copy_report.error', ['item' => 'Daily Report']));
        return Redirect::to('projects/'.$projectId.'/project-files/daily-reports/file');
    }

    /**
     * @param $projectId
     * @param ProjectRepository $projectRepository
     * @param DailyReportsRepository $dailyReportsRepository
     * @return \Illuminate\View\View
     */
    public function uploadDailyReportFile($projectId, ProjectRepository $projectRepository, DailyReportsRepository $dailyReportsRepository)
    {
        $data['project'] = $projectRepository->getProject($projectId);
        $data['report_num'] = $dailyReportsRepository->getGeneratedNumbers($projectId);
        return view('projects.daily_reports.upload-file', $data);
    }

    public function storeDailyReportFile($projectId, DailyRequest $request, DailyReportsRepository $dailyReportsRepository)
    {
        $data = $request->all();
        $data['is_uploaded'] = 1;
        $data['daily_type'] = Config::get('constants.daily_report_short_file_type');
        $data['file_type_id'] = Config::get('constants.daily_report_file_type_id');
        $data['name'] = Config::get('constants.daily_report_uploaded');

        $dailyReport = $dailyReportsRepository->storeGeneralInfo($projectId, $data);

        if($dailyReport) {
            $dailyReportFile = $dailyReportsRepository->storeFiles($projectId, $dailyReport->id, $data);

            if($dailyReportFile) {
                Flash::success(trans('messages.daily_reports.update.success', ['item' => 'Daily Report']));
                return Redirect::to('projects/'.$projectId.'/project-files/daily-reports/file');
            }
        }

        Flash::error(trans('messages.daily_reports.update.error', ['item' => 'Daily Report']));
        return Redirect::to('projects/'.$projectId.'/daily-report/upload-file');
    }

    public function editDailyReportFile($projectId, $dailyReportId, ProjectRepository $projectRepository, DailyReportsRepository $dailyReportsRepository)
    {
        $data['project'] = $projectRepository->getProject($projectId);
        $data['daily_report'] = $dailyReportsRepository->getDailyReport($dailyReportId);
        $data['daily_report']['day_of_week'] = Config::get('constants.carbon_days.'.Carbon::parse($data['daily_report']['date'])->dayOfWeek);

        return view('projects.daily_reports.upload-file', $data);
    }

    public function updateDailyReportFile($projectId, $dailyReportId, DailyRequest $request, DailyReportsRepository $dailyReportsRepository)
    {
        $data = $request->all();
        $fileId = $data['report_file_id'];
        $reportFile = (isset($data['report_file'])) ? $data['report_file'] : null;
        $data['daily_type'] = Config::get('constants.daily_report_short_file_type');
        $data['file_type_id'] = Config::get('constants.daily_report_file_type_id');
        $data['name'] = Config::get('constants.daily_report_uploaded');
        $dailyReportFile = null;

        $dailyReport = $dailyReportsRepository->updateGeneralInfo($projectId, $dailyReportId, $data);

        if($dailyReport) {

            if($reportFile) {
                //delete previous file
                if($fileId) $dailyReportsRepository->deleteDailyReportFile($projectId, $dailyReportId, $fileId);
                //upload new file
                $dailyReportFile = $dailyReportsRepository->storeFiles($projectId, $dailyReportId, $data);
            } else {
                //not updated
                $dailyReportFile = true;
            }
        }

        if($dailyReportFile) {
            Flash::success(trans('messages.daily_reports.update.success', ['item' => 'Daily Report']));
            return Redirect::to('projects/'.$projectId.'/project-files/daily-reports/file');
        }

        Flash::error(trans('messages.daily_reports.update.error', ['item' => 'Daily Report']));
        return Redirect::to('projects/'.$projectId.'/daily-report/edit-file');
    }

    public function deleteFile(DailyReportsRepository $dailyReportsRepository)
    {
        $fileIds = Input::get('fileIds');
        $projectId = Input::get('projectId');
        $dailyReportId = Input::get('dailyReportId');

        //split document ids if multiple
        $filesIdsArray = explode('-', $fileIds);

        foreach ($filesIdsArray as $id) {
            $dailyReportsRepository->deleteDailyReportFile($projectId, $dailyReportId, $id);
        }

        //Successfully deleted
        Flash::success(trans('messages.daily_reports.delete.success', ['item' => 'Daily Report']));
        return Redirect::to('projects/'.$projectId.'/daily-reports');
    }

    /**
     * Generate transmittal for specified version
     * @param DailyReport $dailyReport
     * @return mixed
     */
    public function generateReport(DailyReport $dailyReport)
    {
        set_time_limit(500);
        ini_set('max_execution_time', 500);
        ini_set('xdebug.max_nesting_level', 500);

        $projectId = Input::get('projectId');
        $dailyReportId = Input::get('dailyReportId');
        $addDocuments = Input::get('addDocuments');

        $data = $dailyReport->databaseValues($dailyReportId, $projectId, $addDocuments);

        if($data)
        {
            //daily report file id
            $fileId = $data['file']->file_id;
            $name = $data['name'];
//            $generatedFile = $dailyReport->generate($data['view'], $data, compact('dailyReportId', 'projectId','fileId', 'name'));
            $generatedFile = $dailyReport->mergeAndGenerateReport($data['view'], $data, compact('dailyReportId', 'projectId','fileId', 'name'));
            return $generatedFile;
        }
        return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
    }

    /**
     * Show transmittal file in browser
     * @param $projectId
     * @param ProjectFilesRepository $filesRepository
     * @param DataTransferLimitation $transferLimitation
     * @return \Illuminate\View\View
     */
    public function showReport($projectId, ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
    {
        $fileId = Input::get('fileId');
        $transmittalPath = Input::get('transmittalPath');
        $companyId = Input::get('companyId');
        $transmittalId = Input::get('transmittalId');

        //check download limit
        $file = $filesRepository->getProjectFileById($fileId);

        if ($file) {

            if (Route::getCurrentRoute()->getPath() == "projects/{projectID}/get-transmittal-public") { //public transmittal
                //check upload file allowance
                if ($transferLimitation->checkParentDownloadTransferAllowance($file->size, $file->proj_id)) {

                    //increase download limit
                    if (!empty($file->size)) {
                        //increase company upload transfer
                        $transferLimitation->increaseDownloadTransfer($file->size);
                    }

                    //show transmittal file
                    $file = Storage::disk('s3')->get($transmittalPath);
                    return Response::make($file, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
                } else {
                    die(trans('messages.data_transfer_limitation.download_error', ['item' => 'Daily Report']));
                }
            } else {
                //check upload file allowance
                if ($transferLimitation->checkParentDownloadTransferAllowance($file->size, $file->proj_id)) {

                    //increase download limit
                    if (!empty($file->size)) {
                        //increase company upload transfer
                        $transferLimitation->increaseDownloadTransfer($file->size);
                    }

                    //show transmittal file
                    $file = Storage::disk('s3')->get($transmittalPath);
                    return Response::make($file, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
                } else {
                    Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => 'Transmittal']));
                }
            }
        } else {
            Flash::error(trans('messages.daily_reports.notifications.not_exist'));
        }

        return view('projects.errors.get_transmittal');
    }


    /**
     * @param DailyReportsRepository $dailyReportsRepository
     * @param ProjectFilesRepository $projectFilesRepository
     * @param MailRepository $mailRepository
     * @param ProjectRepository $projectRepository
     * @return mixed
     */
    public function sendEmail(DailyReportsRepository $dailyReportsRepository, ProjectFilesRepository $projectFilesRepository, MailRepository $mailRepository, ProjectRepository $projectRepository)
    {
        $users = Input::get('users');
        $contacts = Input::get('contacts');
        $projectId = Input::get('projectId');
        $dailyReportId = Input::get('dailyReportId');
        $fileId = Input::get('fileId');
        $filePath = Input::get('filePath');

        $userIds = empty($users)?[]:explode('|', $users);
        $contactsIds = empty($contacts)?[]:explode('|', $contacts);

        if (count($userIds) > 0 || count($contactsIds) > 0 ) {

            $project = $projectRepository->getProject($projectId);
            $dailyReport = $dailyReportsRepository->getDailyReport($dailyReportId);
            $subject = sprintf(Config::get('constants.generate_daily_report_name'), $project->name, Carbon::parse($dailyReport->date)->format('m/d/Y'), Config::get('constants.carbon_days.'.Carbon::parse($dailyReport->date)->dayOfWeek));

            if(count($userIds) > 0)
            {
                //get project users
                $userEmails = $projectFilesRepository->getProjectFilesUserEmails($userIds);

                //update users in db
                $dailyReportsRepository->insertEmailedNotifications($dailyReportId, $projectId, $fileId, $userIds);

                foreach ($userEmails as $email) {
                    $mailRepository->sendDailyReportNotification(compact('email', 'project', 'subject', 'projectId', 'dailyReportId',
                        'fileId', 'filePath'));
                }
            }

            if(count($contactsIds) > 0)
            {
                //get contracts
                $contractEmails = $projectFilesRepository->getContractByIdArray($contactsIds);

                //update users in db
                $dailyReportsRepository->insertEmailedNotifications($dailyReportId, $projectId, $fileId, $contactsIds, true);

                foreach ($contractEmails as $email) {
                    $mailRepository->sendDailyReportNotification(compact('email', 'project', 'subject', 'projectId', 'dailyReportId',
                        'fileId', 'filePath'));
                }
            }

            Flash::success(trans('messages.daily_reports.notifications.success'));
            return Response::json(['status' => 'success', 'message' => trans('labels.daily-report.notification_success'), 200]);
        }

        Flash::error(trans('messages.daily_reports.notifications.not_exist'));
        return Response::json(['status' => 'error', 'message' => trans('labels.daily-report.notification_error'), 400]);
    }

}
