<?php namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Materials_and_services\Repositories\MaterialsRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Reports\Implementations\ReportGenerator;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Tasks\Repositories\TasksRepository;
use App\Modules\User_profile\Repositories\UsersRepository;
use App\Services\Files\FileStoreWrapper;
use App\Services\Mailer;
use App\Utilities\ClassMap;
use Illuminate\Contracts\Validation\UnauthorizedException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Laracasts\Flash\Flash;

class TasksController extends Controller {


    /**
     * @var TasksRepository
     */
    private $tasksRepository;
    /**
     * @var ProjectRepository
     */
    private $projectRepository;
    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;
    /**
     * @var ProjectCompaniesRepository
     */
    private $projectCompaniesRepository;
    /**
     * @var ProposalsRepository
     */
    private $proposalsRepository;
    /**
     * @var ContractsRepository
     */
    private $contractsRepository;
    /**
     * @var SubmittalsRepository
     */
    private $submittalsRepository;
    /**
     * @var RfisRepository
     */
    private $rfisRepository;
    /**
     * @var PcosRepository
     */
    private $pcosRepository;
    /**
     * @var MaterialsRepository
     */
    private $materialsRepository;
    /**
     * @var ProjectPermissionsRepository
     */
    private $permissionsRepository;
    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * TasksController constructor.
     * @param TasksRepository $tasksRepository
     * @param ProjectRepository $projectRepository
     * @param CompaniesRepository $companiesRepository
     * @param ProjectCompaniesRepository $projectCompaniesRepository
     * @param ProposalsRepository $proposalsRepository
     * @param ContractsRepository $contractsRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param PcosRepository $pcosRepository
     * @param MaterialsRepository $materialsRepository
     * @param ProjectPermissionsRepository $permissionsRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(TasksRepository $tasksRepository, ProjectRepository $projectRepository,
                                CompaniesRepository $companiesRepository, ProjectCompaniesRepository $projectCompaniesRepository,
                                ProposalsRepository $proposalsRepository, ContractsRepository $contractsRepository,
                                SubmittalsRepository $submittalsRepository, RfisRepository $rfisRepository,
                                PcosRepository $pcosRepository, MaterialsRepository $materialsRepository,
                                ProjectPermissionsRepository $permissionsRepository, UsersRepository $usersRepository)
	{
        $this->tasksRepository = $tasksRepository;
        $this->projectRepository = $projectRepository;
        $this->companiesRepository = $companiesRepository;
        $this->projectCompaniesRepository = $projectCompaniesRepository;
        $this->proposalsRepository = $proposalsRepository;
        $this->contractsRepository = $contractsRepository;
        $this->submittalsRepository = $submittalsRepository;
        $this->rfisRepository = $rfisRepository;
        $this->pcosRepository = $pcosRepository;
        $this->materialsRepository = $materialsRepository;
        $this->permissionsRepository = $permissionsRepository;
        $this->usersRepository = $usersRepository;
    }

	public function index()
	{
        $page = 'all-tasks';
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $filterBy = Input::get('search_by', '');
        $filterText = Input::get('search_text', '');

        $tasksStatistics = ['Open' => 0, 'Closed' => 0];

        $writeProjects = $this->permissionsRepository->getUserProjectsWritePermissionsForModule(12);
        $writeArray = [];
        foreach ($writeProjects as $writeProject) {
            $writeArray[] = $writeProject->proj_id;
        }

        $readProjects = $this->permissionsRepository->getUserProjectsReadPermissionsForModule(12);
        $readArray = [];
        foreach ($readProjects as $readProject) {
            $readArray[] = $readProject->proj_id;
        }

        $tasks = $this->tasksRepository->getAllTasks($filterBy, $filterText, $orderBy, $orderDir, $readArray);

        $tasksWithoutPaginate = $this->tasksRepository->getAllTasks($filterBy, $filterText, $orderBy, $orderDir, $readArray, false);
        foreach ($tasksWithoutPaginate as $task) {
            if ($task->status === 1) {
                $tasksStatistics['Open']++;
            } else {
                $tasksStatistics['Closed']++;
            }
        }

        $userCanWrite = $this->permissionsRepository->userHasAtLeastOneWritePermission(12);
        $userCanDelete = $this->permissionsRepository->userHasAtLeastOneDeletePermission(12);

        $filter = [ 'tasks.title' => trans('labels.tasks.title'),
                    'projects.name' => trans('labels.tasks.project'),
                    'type' => trans('labels.tasks.type'),
                    'assignee' => trans('labels.tasks.assignee')];

        $subcontractorTaskIds = [];

		return view('tasks.index', compact('tasks', 'page', 'userCanWrite', 'userCanDelete', 'writeArray', 'filter', 'subcontractorTaskIds', 'tasksStatistics'));
	}


	public function create()
	{
        $writeProjects = $this->permissionsRepository->getUserProjectsWritePermissionsForModule(12);
        $writeArray = [];
        foreach ($writeProjects as $writeProject) {
            $writeArray[] = $writeProject->proj_id;
        }

	    $projects = $this->projectRepository->getAllProjectsForCompany(1, 'name', 'asc', $writeArray);
	    if (!empty(Auth::user()->comp_id)) {
            $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        }
        //$subContractors = $this->projectCompaniesRepository->getAllDistinctProjectCompanies(null);

        $items = [];
        $selectedItem = null;
        $typeId = Input::get('module_type');
        $projectId = Input::get('project');
        if (!empty($typeId) && !empty($projectId)) {
            switch ($typeId) {
                case 4: //bids
                    $items = $this->proposalsRepository->getAllProposalsByProject($projectId, 'name', 'asc');
                    break;
                case 5: //contracts
                    $items = $this->contractsRepository->getAllContractsByProject($projectId, 'contracts.name', 'asc');
                    break;
                case 3: //submittals
                    $items = $this->submittalsRepository->getAllSubmittalsForProject($projectId, Auth::user()->comp_id);
                    break;
                case 9: //rfis
                    $items = $this->rfisRepository->getAllRFIsForProject($projectId, Auth::user()->comp_id);
                    break;
                case 10: //pcos
                    $items = $this->pcosRepository->getAllPcosForProject($projectId, Auth::user()->comp_id);
                    break;
                case 6: //materials
                    $items = $this->materialsRepository->getAllMaterialsByProject($projectId, 'name', 'asc');
                    break;
            }
            $selectedItem = Input::get('module_item');
        }


        $projectFile = null;
        $companies = [];
	    $users = [];
	    $projectsCompanies = [];
	    foreach ($projects as $project) {
            foreach ($project->projectCompanies as $subContractor)
            {
                if (!empty($subContractor->address_book)) {
                    $companies[$subContractor->id] = $subContractor->address_book->name;
                    $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                    $users[$subContractor->id] = $subContractor->project_company_contacts;
                }
            }
        }

		return view('tasks.create', compact('projects', 'myCompany', 'companies', 'users', 'projectsCompanies', 'items', 'selectedItem', 'projectFile'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return
	 */
	public function store(TaskRequest $request)
	{
        $response = $this->tasksRepository->storeEditTask($request->all());

        if ($response) {
            //Successfully created task
            Flash::success(trans('messages.tasks.store.success', ['item' => 'Task']));
            return Redirect::to('tasks/'.$response->id.'/edit');
        }

        Flash::error(trans('messages.tasks.store.error', ['item' => 'Task']));
        return Redirect::to('tasks');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function show($id)
	{

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 * @return Response
	 */
	public function edit($id)
	{
        $task = $this->tasksRepository->getTaskById($id);

        $writePermission = $this->permissionsRepository->getUserProjectsWritePermissionsForModuleAndProject(12, $task->project_id);
        if ((!Auth::user()->hasRole('Company Admin')) && !(Auth::user()->hasRole('Project Admin') && $task->project->proj_admin == Auth::user()->id) && !$writePermission) {
            die('Unauthorized.');
        }

        $writeProjects = $this->permissionsRepository->getUserProjectsWritePermissionsForModule(12);
        $writeArray = [];
        foreach ($writeProjects as $writeProject) {
            $writeArray[] = $writeProject->proj_id;
        }

        $projects = $this->projectRepository->getAllProjectsForCompany(1, 'name', 'asc', $writeArray);
        if (!empty(Auth::user()->comp_id)) {
            $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);
        }

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($projects as $project) {
            foreach ($project->projectCompanies as $subContractor)
            {
                if (!empty($subContractor->address_book)) {
                    $companies[$subContractor->id] = $subContractor->address_book->name;
                    $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                    $users[$subContractor->id] = $subContractor->project_company_contacts;
                }
            }
        }

        //module types
        $items = [];
        $selectedItem = null;
        if (!empty($task->type_id) && !empty($task->project_id)) {
            switch ($task->type_id) {
                case 4: //bids
                    $items = $this->proposalsRepository->getAllProposalsByProject($task->project_id, 'name', 'asc');
                    $selectedItem = (isset($task->bid_id))?$task->bid_id:null;
                    break;
                case 5: //contracts
                    $items = $this->contractsRepository->getAllContractsByProject($task->project_id, 'contracts.name', 'asc');
                    $selectedItem = (isset($task->contracts_id))?$task->contracts_id:null;
                    break;
                case 3: //submittals
                    $items = $this->submittalsRepository->getAllSubmittalsForProject($task->project_id, Auth::user()->comp_id);
                    $selectedItem = (isset($task->submittal_id))?$task->submittal_id:null;
                    break;
                case 9: //rfis
                    $items = $this->rfisRepository->getAllRFIsForProject($task->project_id, Auth::user()->comp_id);
                    $selectedItem = (isset($task->rfi_id))?$task->rfi_id:null;
                    break;
                case 10: //pcos
                    $items = $this->pcosRepository->getAllPcosForProject($task->project_id, Auth::user()->comp_id);
                    $selectedItem = (isset($task->pco_id))?$task->pco_id:null;
                    break;
                case 6: //materials
                    $items = $this->materialsRepository->getAllMaterialsByProject($task->project_id, 'name', 'asc');
                    $selectedItem = (isset($task->material_id))?$task->material_id:null;
                    break;
            }
        }

        //assigned users
        $assignedUsers = [];
        $assignedContacts = [];
        if (!empty($task->taskUsers)) {
            foreach ($task->taskUsers as $taskUser) {
                if (!empty($taskUser->user_id)) {
                    $assignedUsers[] = $taskUser->user_id;
                }

                if (!empty($taskUser->ab_cont_id)) {
                    $assignedContacts[] = $taskUser->ab_cont_id;
                }
            }
        }

        $projectFile = null;
        if (!empty($task->taskFiles->first())) {
            $projectFile = $task->taskFiles->first();
        }

        $project = $task->project;

        return view('tasks.create', compact('task', 'projects', 'myCompany', 'companies', 'users', 'items', 'projectsCompanies',
            'selectedItem', 'assignedUsers', 'assignedContacts', 'projectFile', 'project'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param TaskRequest $request
     * @return Response
     */
	public function update($id, TaskRequest $request)
	{
        $response = $this->tasksRepository->storeEditTask($request->all(), $id);

        if ($response) {
            //Successfully updated task
            Flash::success(trans('messages.tasks.update.success', ['item' => 'Task']));
            return Redirect::back();//Redirect::to('tasks');
        }

        Flash::error(trans('messages.tasks.update.error', ['item' => 'Task']));
        return Redirect::back();//Redirect::to('tasks');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $ids
	 * @return Response
	 */
	public function destroy($ids)
	{
        $idsArray = explode('-', $ids);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            $response = $this->tasksRepository->deleteEntry($id);

            if (!$response) {
                $errorEntry = true;
            }
        }

        if ($errorEntry) {
            Flash::error(trans('messages.tasks.delete.info', ['item' => 'Tasks']));
            return Redirect::back();//Redirect::to('/tasks');
        }

        //Successful delete
        Flash::success(trans('messages.tasks.delete.success', ['item' => 'Tasks']));
        return Redirect::back();//Redirect::to('/tasks');
	}

    /**
     * Displays all user tasks
     *
     * @return \Illuminate\View\View
     */
    public function myTasks()
    {
        $page = 'my-tasks';
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $filterBy = Input::get('search_by', '');
        $filterText = Input::get('search_text', '');

        $tasks = $this->tasksRepository->getAllMyTasks($filterBy, $filterText, $orderBy, $orderDir);

        $subcontractorTaskIds = [];

        $filter = [ 'tasks.title' => trans('labels.tasks.title'),
                    'projects.name' => trans('labels.tasks.project'),
                    'type' => trans('labels.tasks.type'),
                    'assignee' => trans('labels.tasks.assignee')];

        return view('tasks.my-tasks', compact('tasks', 'page', 'filter', 'subcontractorTaskIds'));
    }

    /**
     * Displays all project tasks
     *
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function projectTasks($projectId)
    {
        $project = $this->projectRepository->getProject($projectId);
        $readPermission = $this->permissionsRepository->getUserProjectsReadPermissionsForModuleAndProject(12, $projectId);
        if ((!Auth::user()->hasRole('Company Admin')) && !(Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) && !$readPermission) {
            die('Unauthorized.');
        }

        $page = 'project-tasks';
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $filterBy = Input::get('search_by', '');
        $filterText = Input::get('search_text', '');

        $writeProjects = $this->permissionsRepository->getUserProjectsWritePermissionsForModule(12);
        $writeArray = [];
        foreach ($writeProjects as $writeProject) {
            $writeArray[] = $writeProject->proj_id;
        }

        $subcontractorTaskIds = $this->tasksRepository->getAllTasksSubcontructor();
        $subcontractorTaskIds = array_column($subcontractorTaskIds, 'task_id');

        $tasks = $this->tasksRepository->getAllProjectTasks($projectId, $filterBy, $filterText, $orderBy, $orderDir);
        $userCanWrite = $this->permissionsRepository->userHasAtLeastOneWritePermission(12);
        $userCanDelete = $this->permissionsRepository->userHasAtLeastOneDeletePermission(12);

        $tasksStatistics = ['Open' => 0, 'Closed' => 0];
        $tasksWithoutPaginate = $this->tasksRepository->getAllProjectTasks($projectId, $filterBy, $filterText, $orderBy, $orderDir, false);
        foreach ($tasksWithoutPaginate as $task) {
            if ($task->status === 1) {
                $tasksStatistics['Open']++;
            } else {
                $tasksStatistics['Closed']++;
            }
        }

        $filter = [ 'tasks.title' => trans('labels.tasks.title'),
                    'type' => trans('labels.tasks.type'),
                    'assignee' => trans('labels.tasks.assignee')];

        return view('projects.tasks.project-tasks', compact('tasks', 'page', 'project', 'userCanWrite',
            'userCanDelete', 'writeArray', 'filter', 'subcontractorTaskIds', 'tasksStatistics'));
    }

    /**
     * Store file (this function is be used for all project files, contracts - the function is accessed with an ajax route)
     * @param FileStoreWrapper $storeWrapper
     * @return null
     * @throws \Exception
     */
    public function storeFile(FileStoreWrapper $storeWrapper)
    {
        $filename = Input::get('filename');
        $filesize = Input::get('filesize');
        $type = Input::get('type');
        $exsistingFileId = Input::get('exsistingFileId');
        $i = Input::get('i');

        if(!empty($filename)) {
            return $storeWrapper->implement(ClassMap::instance(Config::get('classmap.store-file.tasks')), compact("filename", "filesize", "type", "exsistingFileId", "i"));
        }
        return NULL;
    }

    /**
     * PDF report
     * @param string $type
     * @param ReportGenerator $generator
     * @return mixed
     */
    public function report($type = 'all', ReportGenerator $generator)
    {
        ini_set('memory_limit', '512M');
        //filters
        $orderBy = Input::get('sort', 'created_at');
        $orderDir = Input::get('order', 'desc');
        $filterBy = Input::get('search_by', '');
        $filterText = Input::get('search_text', '');
        if ($filterBy == "type_id") {
            $filterText = Input::get('search_module', '');
        }

        $readProjects = $this->permissionsRepository->getUserProjectsReadPermissionsForModule(12);
        $readArray = [];
        foreach ($readProjects as $readProject) {
            $readArray[] = $readProject->proj_id;
        }

        if ($type == 'mine') {
            $page = 'my-tasks';
            $tasks = $this->tasksRepository->getAllMyTasks($filterBy, $filterText, $orderBy, $orderDir, false);
        } else if ($type == 'project' && !empty(Input::get("projectId"))) {
            $projectId = Input::get("projectId");
            $page = 'project-tasks';
            $tasks = $this->tasksRepository->getAllProjectTasks($projectId, $filterBy, $filterText, $orderBy, $orderDir, false);
        } else {
            $page = 'all-tasks';
            $tasks = $this->tasksRepository->getAllTasks($filterBy, $filterText, $orderBy, $orderDir, $readArray, false);
        }

        $data['tasks'] = $tasks;
        $data['page'] = $page;
        $data['filterBy'] = $filterBy;
        $data['filterText'] = $filterText;

        return $this->generateReport($data, $generator);
    }

    /**
     * PDF report
     * @param $id
     * @param ReportGenerator $generator
     * @return mixed
     */
    public function reportSingle($id, ReportGenerator $generator)
    {
        $task = $this->tasksRepository->getTaskById($id);
        $data['task'] = $task;

        $item = null;
        switch ($task->type_id) {
            case 4: //bids
                $item = $this->proposalsRepository->getProposal($task->bid_id);
                break;
            case 5: //contracts
                $item = $this->contractsRepository->getContract($task->contract_id);
                break;
            case 3: //submittals
                $item = $this->submittalsRepository->getSubmittal($task->submittal_id);
                break;
            case 9: //rfis
                $item = $this->rfisRepository->getRfi($task->rfi_id);
                break;
            case 10: //pcos
                $item = $this->pcosRepository->getPco($task->pco_id);
                break;
            case 6: //materials
                $item = $this->materialsRepository->getMaterial($task->material_id);
                break;
        }
        $data['item'] = $item;

        return $this->generateSingleReport($data, $generator);
    }

    public function generateReport($data, $generator)
    {
        //Set report type
        $type = 'tasks-report';

        //Set report view
        $view = 'tasks.report';

        //Set report name
        $name = "Tasks";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    public function generateSingleReport($data, $generator)
    {
        //Set report type
        $type = 'tasks-single-report';

        //Set report view
        $view = 'tasks.report-single';

        //Set report name
        $name = "Tasks";

        //Return generated report
        return $generator->generateReport($type, $data, $view, $name);
    }

    /**
     * send notification email for new task
     * @param $id
     * @param Mailer $mailer
     * @return Redirect
     */
    public function send($id, Mailer $mailer)
    {
        $task = $this->tasksRepository->getTaskById($id);

        $user = $this->usersRepository->getUserByID(Auth::user()->id);

        //prepare data for sending email
        $mailData = array(
            'companyInfo' => $user->name . " (" . $user->company->name . ") via cloud-pm.com"
        );

        $emailData = [
            'taskId' => $task->id,
            'subject' => 'Cloud PM - New task was assigned to you.',
            'company' => Auth::user()->company->name,
            'title' => $task->title,
            'taskNote' => $task->note,
            'addedBy' => $task->addedByUser->name,
            'project' => (!is_null($task->project)) ? $task->project->name : '',
            'dueDate' => $task->due_date,
            'task' => $task
        ];

        if (!empty($task->taskUsers)) {
            foreach ($task->taskUsers as $taskUser) {
                if (!empty($taskUser->users)) {
                    foreach ($taskUser->users as $user) {
                        $emailData['email'] = $user->email;
                        $emailData['name'] = $user->name;
                        if (!empty($user->email)) {
                            $mailer->sendNewTaskEmail($emailData, $mailData);
                            $this->tasksRepository->addTaskUserDistribution($taskUser->id);
                        }
                    }

                }

                if (!empty($taskUser->abUsers)) {
                    foreach ($taskUser->abUsers as $user) {
                        $emailData['email'] = $user->email;
                        $emailData['name'] = $user->name;
                        if (!empty($user->email)) {
                            $mailer->sendNewTaskEmail($emailData, $mailData);
                            $this->tasksRepository->addTaskUserDistribution($taskUser->id);
                        }
                    }
                }
            }
        }

        //show appropriate message
        Flash::success(trans('messages.tasks.notifications.success'));
        return Response::json(['status' => 'success', 'message' => trans('messages.tasks.notifications.success'), 200]);
    }
}
