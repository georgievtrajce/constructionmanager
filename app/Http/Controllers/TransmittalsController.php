<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\File_type;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Mail\Repositories\MailRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use App\Modules\Transmittals\Implementations\TransmittalsWrapper;
use App\Modules\Transmittals\Repositories\TransmittalsRepository;
use App\Utilities\ClassMap;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Laracasts\Flash\Flash;

class TransmittalsController extends Controller {

	/**
	 * Check if transmittal already exist
	 * @param TransmittalsWrapper $wrapper
	 * @param TransmittalsRepository $transmittalsRepository
	 * @return mixed
     */
	public function checkExistingTransmittal(TransmittalsWrapper $wrapper, TransmittalsRepository $transmittalsRepository)
	{
		$fileType = Input::get('type');
		$versionId = Input::get('versionId');
		$projectId = Input::get('projectId');
		$transmittalDate = Input::get('transmittalDate');
		$databaseType = Input::get('databaseType');

		$exist = $transmittalsRepository->checkExisting($fileType, $versionId, $projectId, $transmittalDate);

		if ($exist) {
			return Response::json(['response' => true, 200]);
		}

		return Response::json(['response' => false, 200]);
	}

	/**
	 * Generate transmittal for specified version
	 * @param TransmittalsWrapper $wrapper
	 * @param TransmittalsRepository $transmittalsRepository
	 * @return mixed
	 * @throws \Exception
	 */
	public function generateRecipientTransmittal(TransmittalsWrapper $wrapper, TransmittalsRepository $transmittalsRepository)
	{
        set_time_limit(240);
		$fileType = Input::get('type');
		$databaseType = Input::get('databaseType');
		$versionId = Input::get('versionId');
		$projectId = Input::get('projectId');
		$transmittalDate = Input::get('recTransmittalDate');

		$storedFile = $wrapper->wrapDatabaseData(ClassMap::instance(Config::get('classmap.transmittals-database.'.$databaseType)), $fileType, $versionId, $projectId, $transmittalDate);

		//save transmittal log in database
		if ($storedFile) {
			$fileId = $storedFile['file']->id;
			$name = $storedFile['name'];
			$transmittalLog = $transmittalsRepository->storeTransmittalLog($projectId, $fileId, $storedFile['fileType']->id, $versionId, $transmittalDate);

			if ($transmittalLog) {
				//return generated pdf
				$generatedFile = $wrapper->wrap(ClassMap::instance(Config::get('classmap.transmittals.'.$storedFile['type'])), $storedFile['view'], $storedFile, compact('projectId', 'fileId', 'name', 'versionId', 'transmittalDate'));
				return $generatedFile;
			}
		}
		return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
	}

	/**
	 * Generate transmittal for specified version
	 * @param TransmittalsWrapper $wrapper
	 * @param TransmittalsRepository $transmittalsRepository
	 * @return mixed
	 * @throws \Exception
	 * @internal param ProjectFilesRepository $filesRepository
	 */
	public function generateSubcontractorTransmittal(TransmittalsWrapper $wrapper, TransmittalsRepository $transmittalsRepository)
	{
        set_time_limit(240);
		$fileType = Input::get('type');
		$databaseType = Input::get('databaseType');
		$versionId = Input::get('versionId');
		$projectId = Input::get('projectId');
		$transmittalDate = Input::get('subTransmittalDate');

		$storedFile = $wrapper->wrapDatabaseData(ClassMap::instance(Config::get('classmap.transmittals-database.'.$databaseType)), $fileType, $versionId, $projectId, $transmittalDate);

		//save transmittal log in database
		if ($storedFile) {
			$fileId = $storedFile['file']->id;
			$name = $storedFile['name'];
			$transmittalLog = $transmittalsRepository->storeTransmittalLog($projectId, $fileId, $storedFile['fileType']->id, $versionId, $transmittalDate);

			if ($transmittalLog) {
				//return generated pdf
				$generatedFile = $wrapper->wrap(ClassMap::instance(Config::get('classmap.transmittals.'.$storedFile['type'])), $storedFile['view'], $storedFile, compact('projectId', 'fileId', 'name', 'versionId', 'transmittalDate'));
				return $generatedFile;
			}
		}
		return Response::json(array('generate' => 0, 'message' => trans('messages.data_transfer_limitation.something_wrong')));
	}

	/**
	 * Send email notification
	 * @param MailRepository $mailRepository
	 * @param TransmittalsWrapper $wrapper
	 * @param TransmittalsRepository $transmittalsRepository
	 * @return mixed
	 * @throws \Exception
     */
	public function sendEmail(MailRepository $mailRepository, TransmittalsWrapper $wrapper,
                              TransmittalsRepository $transmittalsRepository, SubmittalsRepository $submittalsRepository)
	{
        $usersString = Input::get('users');
        $contactsString = Input::get('contacts');
		$type = Input::get('type');
		$entryId = Input::get('entryId');
        $versionId = Input::get('versionId');
		$subRec = Input::get('subRec');
		$subRecId = Input::get('subRecId');
		$projectId = Input::get('projectId');
		$transmittalId = Input::get('transmittalId');
		$fileId = Input::get('fileId');
		$transmittalPath = Input::get('transmittalPath');
        $fileUploadedId = Input::get('fileUploadedId');
        $fileUploadedPath = Input::get('filePath');

        $userIds = empty($usersString)?[]:explode('|', $usersString);
        $contactIds =  empty($contactsString)?[]:explode('|', $contactsString);

		$emails = $wrapper->wrapCompanyEmails(ClassMap::instance(Config::get('classmap.transmittals-notifications.'.$type)),
            compact('entryId', 'fileId', 'subRec', 'subRecId', 'projectId', 'transmittalPath', 'versionId',
                'userIds', 'contactIds'));

        $userEmails = $wrapper->wrapUserEmails(ClassMap::instance(Config::get('classmap.transmittals-notifications.'.$type)),
            compact('entryId', 'fileId', 'subRec', 'subRecId', 'projectId', 'transmittalPath', 'versionId',
                'userIds', 'contactIds'));

        if (count($emails) > 0 || count($userEmails) > 0) {
			$transmittalsRepository->updatedEmailedTransmittal($transmittalId, Config::get('constants.transmittal_emailed.yes'));


			foreach ($emails as $email) {
			    /*if (!empty($email->comp_child_id)) {
                    $mailRepository->sendTransmittalNotification(compact('email','fileId','transmittalPath',
                        'transmittalId', 'fileUploadedId', 'fileUploadedPath', 'emails', 'userEmails'));
                } else {*/
                    $mailRepository->sendTransmittalNotificationPublic(compact('email','fileId','transmittalPath',
                        'transmittalId', 'fileUploadedId', 'fileUploadedPath', 'emails', 'userEmails'));
                //}
			}

            foreach ($userEmails as $email) {
                /*if (!empty($email->comp_child_id)) {
                    $mailRepository->sendTransmittalNotification(compact('email','fileId','transmittalPath',
                        'transmittalId', 'fileUploadedId', 'fileUploadedPath', 'emails', 'userEmails'));
                } else {*/
                    $mailRepository->sendTransmittalNotificationPublic(compact('email','fileId','transmittalPath',
                        'transmittalId', 'fileUploadedId', 'fileUploadedPath', 'emails', 'userEmails'));
                //}
            }

            $wrapper->wrapMarkSent(ClassMap::instance(Config::get('classmap.transmittals-notifications.'.$type)),
                compact('entryId', 'fileId', 'subRec', 'subRecId', 'projectId', 'transmittalPath', 'versionId',
                    'userIds', 'contactIds'));

			Flash::success(trans('messages.transmittals.notification_success'));
			return Response::json(['status' => 'success', 'message' => trans('labels.transmittals.notification_success'), 200]);
		}

		return Response::json(['status' => 'error', 'message' => trans('labels.transmittals.notification_error'), 400]);
	}

	/**
	 * Show transmittal file in browser
	 * @param $projectId
	 * @param ProjectFilesRepository $filesRepository
	 * @param DataTransferLimitation $transferLimitation
	 * @return \Illuminate\View\View
     */
	public function showTransmittal($projectId, ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
	{
		$fileId = Input::get('fileId');
		$transmittalPath = Input::get('transmittalPath');
		$companyId = Input::get('companyId');
		$transmittalId = Input::get('transmittalId');

		//check download limit
		$file = $filesRepository->getProjectFileById($fileId);

		if ($file) {

		    if (Route::getCurrentRoute()->getPath() == "projects/{projectID}/get-transmittal-public") { //public transmittal
                //check upload file allowance
                if ($transferLimitation->checkParentDownloadTransferAllowance($file->size, $file->proj_id)) {
                    //return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));

                    //increase download limit
                    if (!empty($file->size)) {
                        //increase company upload transfer
                        $transferLimitation->increaseDownloadTransfer($file->size);
                    }

                    //show transmittal file
                    $file = Storage::disk('s3')->get($transmittalPath);
                    return Response::make($file, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
                } else {
                    die(trans('messages.data_transfer_limitation.download_error', ['item' => 'Transmittal']));
                }
		    } else {
                //check upload file allowance
                if ($transferLimitation->checkParentDownloadTransferAllowance($file->size, $file->proj_id)) {
                    //return Response::json(array('allowed' => 1, 'filesize' => $file->size, 200));

                    //increase download limit
                    if (!empty($file->size)) {
                        //increase company upload transfer
                        $transferLimitation->increaseDownloadTransfer($file->size);
                    }

                    //show transmittal file
                    $file = Storage::disk('s3')->get($transmittalPath);
                    return Response::make($file, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
                } else {
                    Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => 'Transmittal']));
                }
            }
		} else {
			Flash::error(trans('messages.transmittals.not_exist'));
		}

		return view('projects.errors.get_transmittal');
	}

}
