<?php
namespace App\Http\Controllers;

use App\Http\Requests\InviteToBidRequest;
use App\Modules\Address_book\Repositories\AddressBookRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Http\Requests\ProposalsRequest;
use App\Requests;
use App\Services\InviteToBid;
use App\Utilities\FileStorage;

use Illuminate\Support\Facades\Config;
use Laracasts\Flash\Flash;
use Request;
use Input;
use Auth;

class ProposalSuppliersController extends Controller
{
    private $proposalsRepo;
    private $projectRepo;
    private $filesRepo;
    private $addressBookRepo;

    public function __construct(ProposalsRepository $proposalsRepo, ProjectRepository $projectRepo, AddressBookRepository $addressBookRepo, FilesRepository $filesRepo)
    {
        $this->middleware('auth');
        $this->middleware('sub_module_write', ['only' => ['create','store','update','getPermissions','updatePermissions']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->proposalsRepo = $proposalsRepo;
        $this->projectRepo = $projectRepo;
        $this->addressBookRepo = $addressBookRepo;
        $this->filesRepo = $filesRepo;
    }

    /**
     * load suppliers for proposal
     * this function is currently not used by may be used if it is necessary to load just suppliers for proposal
     * @param $projectId
     * @param $id
     * @return \Illuminate\View\View
     */
    public function index($projectId, $id)
    {
        return redirect('projects/'.$projectId.'/proposals/');
    }

    /**
     * create proposal supplier record for proposal
     * @param $projectId
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function create($projectId, $id)
    {
        $proposal = $this->proposalsRepo->getProposal($id);
        $project = $this->projectRepo->getProject($projectId);

        if ($proposal) {
            //if such proposal exists return view
            return view('projects.proposals.suppliers.create',compact('project','proposal'));
        }
        //else redirect to proposals
        else return redirect('projects/'.$projectId.'/bids/');
    }


    /**
     * store proposal supplier record
     * @param $projectId
     * @param $id
     * @param ProposalsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, $id, ProposalsRequest $request)
    {
        $data = $request->all();
        $supplier = $this->proposalsRepo->storeSupplier($id, $projectId, $data);

        if ($supplier) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'bidder']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'bidder']));
        }

        //return redirect('projects/'.$projectId.'/bids/');
        return redirect(URL('/projects/'.$projectId.'/bids/'.$id.'/bidders/'.$supplier->id.'/permissions'));
    }

    /**
     * load view for updating proposal supplier record
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $id, $supplierId)
    {
        $supplier = $this->proposalsRepo->getSupplier($supplierId);
        $proposal = $this->proposalsRepo->getProposal($id);
        $project = $this->projectRepo->getProject($projectId);
        //load address book entries with contacts and addresses
        $address_book = $this->addressBookRepo->getEntryContactsAddresses($supplier->ab_id);

        return view('projects.proposals.suppliers.edit',compact('supplier','project','address_book', 'proposal'));
    }


    /**
     * update supplier
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @param ProposalsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $id, $supplierId, ProposalsRequest $request)
    {
        $data = $request->all();
        $supplier = $this->proposalsRepo->updateSupplier($supplierId, $data);

        if ($supplier) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'bidder']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'bidder']));
        }

        Flash::success('Bidder was successfully updated.');

        return redirect('projects/'.$projectId.'/bids/'.$id.'/edit');
    }

    /**
     * delete proposal supplier
     * @param $projectId
     * @param $id
     * @param $supplierId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($projectId, $id, $supplierId)
    {
        $idsArray = explode('-', $supplierId);

        $errorEntry = false;
        foreach ($idsArray as $supplierIdArray) {
            $jointIds = explode("|", $supplierIdArray);
            $abId = $jointIds[1];

            $files = $this->proposalsRepo->getProposalFiles($projectId, $id, $jointIds[0]);

            $deleteFile = true;
            foreach ($files as $file) {
                $params = array('proj_id'=>$projectId,
                    'prop_id' => $id,
                    'supp_id' => $jointIds[0]);

                //delete file by name and path
                $deleteFile = $this->filesRepo->deleteFile('company_'.Auth::user()->company->id.'/project_'.$projectId.'/proposals', $file->file_name);
            }

            //check for other bidder records for the same project
            if ($this->proposalsRepo->checkMultipleTimesAddedBidder($projectId, $abId)) {
                $deleteSupplier = $this->proposalsRepo->deleteSupplier($jointIds[0]);
            } else {
                $deleteSupplier = $this->proposalsRepo->unshareProjectWithBidder($jointIds[0]);
            }

            if (empty($deleteSupplier) || empty($deleteFile)) {
                $errorEntry = true;
            }
        }

        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'bidder']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'bidder']));
        }

        return redirect('projects/'.$projectId.'/bids/');
    }

    /**
     * Invite company to bid
     * @param $projectId
     * @param $bidId
     * @param InviteToBidRequest $request
     * @param InviteToBid $inviteToBid
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function invite($projectId, $bidId, InviteToBidRequest $request, InviteToBid $inviteToBid)
    {
        $data = $request->all();
        $data['proj_id'] = $projectId;
        $data['bid_id'] = $bidId;

        //send invitation through custom service
        $invite = $inviteToBid->invitationProcessing($data);

        if (is_null($invite)) {
            Flash::error(trans('messages.bidder.invite.select_contact'));
        }

        if ($invite === true) {
            Flash::success(trans('messages.bidder.invite.success'));
        }

        if ($invite === false) {
            Flash::error(trans('messages.bidder.invite.error'));
        }

        return redirect('projects/'.$projectId.'/bids');
    }

    /**
     * Get bidder permissions page
     * @param $projectId
     * @param $bidId
     * @param $bidderId
     * @return \Illuminate\View\View
     */
    public function getPermissions($projectId, $bidId, $bidderId)
    {
        $bidder = $this->proposalsRepo->getSupplier($bidderId);
        $bidderPermissions = $this->proposalsRepo->getBidderWithPermissions($bidderId);
        $project = $this->projectRepo->getProject($projectId);
        return view('projects.proposals.suppliers.permissions', compact('project', 'bidderPermissions', 'bidder'));
    }

    /**
     * Update bidder permissions
     * @param $projectId
     * @param $bidId
     * @param $bidderId
     * @param ProjectPermissionsRepository $projectPermissionsRepository
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updatePermissions($projectId, $bidId, $bidderId, ProjectPermissionsRepository $projectPermissionsRepository)
    {
        //$modules = Input::get('module_id');
        $modulesRead = Input::get('module_read');
        $modulesWrite = Input::get('module_write');
        $pcoPermissionType = Input::get('pco_permission_type');

        //entity type 1 = modules
        $module = Config::get('constants.entity_type.module');

        //entity type 2 = Project Files
        $file = Config::get('constants.entity_type.file');

        //get all project files ids
        $projectFilesRead = Input::get('project_file_read');

        //get the concrete bidder
        $bidder = $this->proposalsRepo->getSupplier($bidderId);

        //get all module permissions
        $allModulePermissions = $projectPermissionsRepository->getAllBidderPermissionsByEntity($bidder->ab_id, $module, $projectId);

        //update modules permissions
        if (is_null($modulesRead) && is_null($modulesWrite)) {
            //set all to 0
            foreach ($allModulePermissions as $singlePermission) {
                $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType, 0);
            }
        } else if (is_null($modulesRead)) {
            $modulesWriteArray = array_values($modulesWrite);
            foreach ($allModulePermissions as $singlePermission) {
                if (in_array($singlePermission->entity_id, $modulesWriteArray)) {
                    $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType, 1);
                } else {
                    $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType);
                }
            }
        } else if (is_null($modulesWrite)) {
            $modulesReadArray = array_values($modulesRead);
            foreach ($allModulePermissions as $singlePermission) {
                if (in_array($singlePermission->entity_id, $modulesReadArray)) {
                    //set
                    $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 1, $pcoPermissionType);
                } else {
                        $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType);
                }
            }
        } else {
            $modulesReadArray = array_values($modulesRead);
            $modulesWriteArray = array_values($modulesWrite);
            foreach ($allModulePermissions as $singlePermission) {
                if (in_array($singlePermission->entity_id, $modulesReadArray)) {
                    //set
                    if (in_array($singlePermission->entity_id, $modulesWriteArray)) {
                        $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 1, $pcoPermissionType, 1);
                    } else {
                        $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 1, $pcoPermissionType);
                    }
                } else {
                    if (in_array($singlePermission->entity_id, $modulesWriteArray)) {
                        $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType, 1);
                    } else {
                        $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType);
                    }
                }
            }
        }

        //get project files permissions
        $allFilePermissions = $projectPermissionsRepository->getAllBidderPermissionsByEntity($bidder->ab_id, $file, $projectId);

        //update project files permissions
        if (is_null($projectFilesRead)) {
            //set all to 0
            foreach ($allFilePermissions as $singlePermission) {
                $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType);
            }
        } else {
            $filesArray = array_values($projectFilesRead);
            foreach ($allFilePermissions as $singlePermission) {
                if (in_array($singlePermission->entity_id, $filesArray)) {
                    $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 1, $pcoPermissionType);
                } else {
                    $projectPermissionsRepository->updateBidderPermission($singlePermission->id, 0, $pcoPermissionType);
                }
            }
        }

        //to be added true - false for updated permissions and error message if false
        Flash::success(trans('messages.users.permissions.success'));

        return redirect('projects/'.$projectId.'/bids/'.$bidId.'/edit');
    }

}