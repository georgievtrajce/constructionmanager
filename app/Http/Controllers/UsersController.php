<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\ChangeUserPasswordRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Modules\User_profile\Repositories\UsersRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Laracasts\Flash\Flash;

class UsersController extends Controller {

    /**
     * Get user profile form page
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('user_profile.index');
    }

    /**
     * Update user profile data
     * @param UpdateUserProfileRequest $request
     * @param UsersRepository $repository
     * @return mixed
     */
    public function update(UpdateUserProfileRequest $request, UsersRepository $repository)
    {
        $response = $repository->updateUserProfile($request->all());

        if ($response) {
            Flash::success(trans('messages.user_profile.update.success'));
            return Redirect::to('/user-profile');
        }

        Flash::error(trans('messages.user_profile.update.error'));
        return Redirect::to('/user-profile');
    }

    /**
     * Get change password form page
     * @return \Illuminate\View\View
     */
    public function changePassword()
    {
        return view('user_profile.change-password');
    }

    /**
     * Change user password
     * @param ChangeUserPasswordRequest $request
     * @param UsersRepository $repository
     * @return mixed
     */
    public function postPassword(ChangeUserPasswordRequest $request, UsersRepository $repository)
    {
        //Check the old password matching
        if (!Hash::check($request->input('old_password'), Auth::user()->password)) {
            Flash::error(trans('messages.user_profile.password.error'));
            return Redirect::to('/change-password');
        }

        //Change password
        $response = $repository->changePassword($request->all());

        if ($response) {
            Flash::success(trans('messages.user_profile.password.success'));
            return Redirect::to('/user-profile');
        }
    }

}
