<?php
namespace App\Http\Controllers;

use App\Modules\Project_companies\Repositories\ProjectCompaniesRepository;
use App\Modules\Project_permissions\Repositories\ProjectPermissionsRepository;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Http\Requests\ProjectsRequest;

use App\Services\InviteCompany;
use App\Services\Mailer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Request;
use Input;
use Flash;
use Session;

class ProjectCompaniesController extends Controller {

    private $projectRepo;
    private $projectCompanyRepo;
    private $projectSubcontractorRepo;
    private $projectPermissionsRepo;

    /**
     * @param ProjectCompaniesRepository $projectCompanyRepo
     * @param ProjectRepository $projectRepo
     * @param ProjectSubcontractorsRepository $projectSubcontractorRepo
     * @param ProjectPermissionsRepository $projectPermissionsRepository
     */
    public function __construct(ProjectCompaniesRepository $projectCompanyRepo, ProjectRepository $projectRepo,
                                ProjectSubcontractorsRepository $projectSubcontractorRepo,
                                ProjectPermissionsRepository $projectPermissionsRepository)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        //$this->middleware('subscription_level_one');
        $this->middleware('unshared_project', ['except' => ['updateAddress', 'unshare', 'share']]);
        $this->middleware('sub_module_write', ['only' => ['store','update','unshare','invite']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->projectRepo = $projectRepo;
        $this->projectCompanyRepo = $projectCompanyRepo;
        $this->projectSubcontractorRepo = $projectSubcontractorRepo;
        $this->projectPermissionsRepo = $projectPermissionsRepository;
    }

    /**
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function index($projectId)
    {
        //default sort and order unless selected
        $sort = Input::get('sort', 'pc_id');
        $order = Input::get('order', 'asc');

        //get project companies by project id
        $project = $this->projectRepo->getProject($projectId);
        $companies = $this->projectCompanyRepo->getProjectCompaniesSubcontractors($projectId, $sort, $order);
        $projectContractor = $this->projectCompanyRepo->getProjectContractor($projectId);

        return view('projects.companies.index', compact('project', 'companies', 'projectContractor'));
    }

    /**
     * @param ProjectsRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ProjectsRequest $request)
    {
        //insert project company for a project
        $data = $request->all();
        $addressBookId = $data['ab_id'];
        $id = $data['proj_id'];
        $company = $this->projectCompanyRepo->storeCompany($id, $data);

        if (!is_null($company)) {
            //initial insert company permissions for that project as no permissions for anything
            $this->projectPermissionsRepo->insertInitialProjectCompanyPermissions($id, Auth::user()->comp_id, $company->id);
        }

        $addressBookUri = '';
        if ($company) {
            $abContacts = $this->projectCompanyRepo->getABContacts($addressBookId, $company->addr_id);
            if (count($abContacts) > 0) {
                $addressBookUri = $addressBookId.'/project-company/'.$company->id.'/office/'.$company->addr_id;
            }
        }

        return redirect('projects/'.$id.'/companies/'.$addressBookUri);
    }

    /**
     * Des
     * @param $id
     * @param $projectCompanyId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @internal param $addressBookId
     */
    public function destroy($id, $projectCompanyId)
    {
        $idsArray = explode('-', $projectCompanyId);

        $errorEntry = false;
        foreach ($idsArray as $ids) {
            $jointIds = explode("|", $ids);

            //get project company contacts
            $projectCompanyContacts = $this->projectCompanyRepo->getProjectCompanyContacts($id, $jointIds[0]);

            //delete contacts assigned to this project if there are any
            if (count($projectCompanyContacts)) {
                foreach ($projectCompanyContacts as $contact) {
                    $contact->delete();
                }
            }

            //get project company permissions
            $projectCompanyPermissions = $this->projectPermissionsRepo->getCompanyPermissionsByProject($id, $jointIds[0]);

            //delete permissions assigned to this company for the specified project
            if (count($projectCompanyPermissions)) {
                foreach ($projectCompanyPermissions as $permission) {
                    $permission->delete();
                }
            }

            //if project is shared, also un-share
            if (!empty($jointIds[1])) {
                $this->projectSubcontractorRepo->unshareProject($id, $jointIds[1]);
            }

            //delete project company
            $response = $this->projectCompanyRepo->deleteProjectCompany($id, $jointIds[0]);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }




        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'project company']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'project company']));
        }
        return redirect('projects/'.$id.'/companies/');
    }

    /**
     * Show contacts with checkboxes for a company
     * @param $id
     * @param $addressBookId
     * @return \Illuminate\View\View
     */
    public function show($id, $addressBookId)
    {
        //load contacts for project companies
        $project = $this->projectRepo->getProject($id);
        $company = $this->projectCompanyRepo->getCompany($addressBookId);

        //project contacts from all companies
        $projectContacts = $this->projectCompanyRepo->getProjectContacts($id);

        //project contacts IDs from all companies
        $contactIDs = $this->projectCompanyRepo->getProjectContactsIDs($id);

        //address Book contacts for address book id
        $abContacts = $this->projectCompanyRepo->getABContacts($addressBookId, $company->addr_id);

        return view('projects.companies.contacts',compact('project','company','projectContacts','contactIDs','abContacts'));
    }

    /**
     * Show contacts that belongs to concrete company office
     * @param $id
     * @param $addressBookId
     * @param $projectCompanyId
     * @param int $officeId
     * @return \Illuminate\View\View
     */
    public function showOfficeContacts($id, $addressBookId, $projectCompanyId, $officeId = 0)
    {
        //load contacts for project companies
        $project = $this->projectRepo->getProject($id);
        $company = $this->projectCompanyRepo->getCompany($addressBookId);

        //project contacts from all companies
        //$projectContacts = $this->projectCompanyRepo->getProjectContacts($id, $projectCompanyId);

        //project contacts IDs from all companies
        $contactIDs = $this->projectCompanyRepo->getProjectContactsIDs($id, $projectCompanyId);

        //address Book contacts for address book id
        $abContacts = $this->projectCompanyRepo->getABContacts($addressBookId, $officeId);

        return view('projects.companies.contacts',compact('project','company','projectContacts','contactIDs','abContacts', 'projectCompanyId'));
    }

    /**
     * Insert project contact for a project
     * @param $id
     * @param $addressBookId
     * @param $projectCompanyId
     * @param $officeId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateOfficeContacts($id, $addressBookId, $projectCompanyId, $officeId)
    {
        $data['company_contacts'] = Input::get('company_contacts');
        $data['officeId'] = $officeId;

        $storeContacts = $this->projectCompanyRepo->storeContacts($id, $addressBookId, $projectCompanyId, $data);

        if ($storeContacts) {
            Flash::success(trans('messages.projectSubcontractor.storeContacts.success'));
            return redirect(url('projects/'.$id.'/companies'));
        } else {
            Flash::error(trans('messages.projectSubcontractor.storeContacts.error'));
        }

        return redirect(url('projects/'.$id.'/companies'));
    }


    /**
     * Share project with specified company
     * @param $id
     * @param $addressBookId
     * @param Mailer $mailer
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function share($id, $addressBookId, Mailer $mailer)
    {
        $data = Input::all();
        $data['proj_id'] = $id;

        $storeSubcontractor = $this->projectSubcontractorRepo->storeSubcontractor($id, $data);
        $mailer->sendMailSubcontractor($data);

        if ($storeSubcontractor) {
            Flash::success(trans('messages.projectSubcontractor.share.success', ['item' => 'subcontractor']));
        } else {
            Flash::error(trans('messages.projectSubcontractor.share.error', ['item' => 'subcontractor']));
        }

        return redirect('projects/'.$id.'/companies');
    }

    /**
     * Unshare project with the specified company
     * @param $id
     * @param $projectSubcontractorId
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function unshare($id, $projectSubcontractorId)
    {
        $unShareProject = $this->projectSubcontractorRepo->unshareProject($id, $projectSubcontractorId);

        if ($unShareProject) {
            Flash::success(trans('messages.projectSubcontractor.unShare.success'));
        } else {
            Flash::error(trans('messages.projectSubcontractor.unShare.error'));
        }
        return redirect('projects/'.$id.'/companies');
    }

    /**
     * Update Company address
     * @return mixed
     */
    public function updateAddress()
    {
        //update location address
        $data = Input::all();

        $response = $this->projectCompanyRepo->updateAddress($data);

        if ($response) {
            return Response::json(array('success' => $response->addr_id, 200));
        } else {
            return Response::json(array('error' => trans('messages.data_transfer_limitation.something_wrong'), 400));
        }

    }

    /**
     * Invite company to app and project
     * @param $projectId
     * @param $projectCompanyId
     * @param $addressBookId
     * @param InviteCompany $inviteCompany
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function invite($projectId, $projectCompanyId, $addressBookId, InviteCompany $inviteCompany)
    {
        //send invitation through custom service
        $invite = $inviteCompany->inviteCompany($projectId, $projectCompanyId, $addressBookId);

        if (is_null($invite)) {
            Flash::error(trans('messages.projectSubcontractor.invite.select_contact'));
        }

        if ($invite === true) {
            Flash::success(trans('messages.projectSubcontractor.invite.success'));
        }

        if ($invite === false) {
            Flash::error(trans('messages.projectSubcontractor.invite.error'));
        }

        return redirect('projects/'.$projectId.'/companies');
    }

}