<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Subscription_type;
use App\Models\User;
use App\Models\Company;
use App\Modules\Company_profile\Repositories\ReferralsRepository;
use App\Modules\Registration\Repositories\RegisterRepository;
use App\Services\Mailer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\Models\Role;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller {

    /**
     * Post company and user payment data to the Helcim hosted payment page
     * @param $companyId
     * @param $subscriptionId
     * @param $email
     * @param $companyName
     * @return mixed
     */
    public function postToHostedPage($companyId, $subscriptionId, $email, $companyName)
    {
        //select subscription level
        $subscriptionLevel = Subscription_type::where('id','=',$subscriptionId)->firstOrFail();

        $data['companyId'] = $companyId;
        $data['subscriptionId'] = $subscriptionId;
        $data['email'] = $email;
        $data['companyName'] = $companyName;
        $data['projectCompanyId'] = Input::get('projectCompanyId');
        $data['subToken'] = Input::get('subToken');
        $data['referralToken'] = Input::get('refToken');
        $data['subToken'] = Input::get('subToken');
        $data['projectCompanyId'] = Input::get('projectCompanyId');
        $data['bidderId'] = Input::get('bidderId');

        //check if request has type input field and value and if it has that means that
        // the request is for re-newing expired subscription
        if (Input::has('type')) {
            if (Input::get('type') == 'renewed') {
                $data['paymentType'] = 'renewed';
            } else {
                $data['paymentType'] = 'regular';
            }
        } else {
            $data['paymentType'] = 'regular';
        }

        $data['merchantId'] = Config::get('payment.merchant-id');
        $data['token'] = Config::get('payment.token');
        //$data['amount'] = Config::get('payment.subscription-level-'.$subscriptionId);
        $data['amount'] = number_format($subscriptionLevel->amount,2);

        return View::make('auth.post_hosted_page', $data);
    }


    /**
     * Calculate new amount that should be paid when a company
     * want to change their subscription level
     *
     * @param $companyId - id of subscribed company
     * @param $subscriptionId - id of new subscription
     * @param $email - email of admin
     * @param $companyName - company name
     * @param string $referralToken -optional
     * @return mixed
     */
    public function calculateAndPostToHostedPage($companyId, $subscriptionId, $email, $companyName, $referralToken = '')
    {
        $data['companyId'] = $companyId;
        $data['subscriptionId'] = $subscriptionId;
        $data['email'] = $email;
        $data['companyName'] = $companyName;
        $data['projectCompanyId'] = Input::get('projectCompanyId');
        $data['bidderId'] = Input::get('bidderId');
        $data['subToken'] = Input::get('subToken');
        $data['referralToken'] = 'None';
        $data['paymentType'] = 'change';

        $data['merchantId'] = env('PAY_ID');
        $data['token'] = env('PAY_TOKEN');

        //calculate new amount for paying
        $data['amount'] = $this->calculateNewAmount($data['subscriptionId']);

        return View::make('auth.post_hosted_page', $data);
    }

    /**
     * Check company's referral points and allowance for a specific subscription level
     * (it returns a string needed for representing a message on view)
     *
     * @param $points
     * @return string
     */
    protected function checkReferralPoints($points)
    {
        $secondSubscription = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->first();
        $thirdSubscription = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->first();

        $labelData = array('points' => $points, 'level2' => $secondSubscription->name, 'level3' => $thirdSubscription->name);
        if ($points < $secondSubscription->ref_points_needed) {
            $label = trans('labels.ref_points.not_enough');
        }
        else if (($secondSubscription->ref_points_needed <= $points) && ($thirdSubscription->ref_points_needed > $points)) {
            if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-one')) {
                $label = trans('labels.ref_points.only_level2', $labelData);
            } else if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-two')) {
                $label = trans('labels.ref_points.only_level2', $labelData);
            } else {
                $label = trans('labels.ref_points.not_enough');
            }
        }
        else {
            if (Auth::user()->company->subs_id == Config::get('subscription_levels.level-three')) {
                $level3years = floor($points / $thirdSubscription->ref_points_needed);
                $labelData['level3years'] = $level3years;
                $labelData['level3plural'] = $level3years > 1 ? 's' : '';
                $label = trans('labels.ref_points.only_level3', $labelData);
            } else {
                $level2years = floor($points / $secondSubscription->ref_points_needed);
                $level3years = floor($points / $thirdSubscription->ref_points_needed);
                $labelData['level2years'] = $level2years;
                $labelData['level2plural'] = $level2years > 1 ? 's' : '';
                $labelData['level3years'] = $level3years;
                $labelData['level3plural'] = $level3years > 1 ? 's' : '';
                $label = trans('labels.ref_points.level2_and_level3', $labelData);
            }
        }

        return $label;
    }

    /**
     * Redirect function for non active (not paid subscription) company
     * @return mixed
     */
    public function paySubscription()
    {
        //get the three available application subscription levels
        $data['firstSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-one'))->firstOrFail();
        $data['secondSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->firstOrFail();
        $data['thirdSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->firstOrFail();

        //get available company's referral points
        $data['referralPointsLabel'] = $this->checkReferralPoints(Auth::user()->company->ref_points);

        return View::make('auth.not_active_payment', $data);
    }

    /**
     * Re new the expired subscription level
     * @return mixed
     */
    public function reNewSubscription()
    {
        //get the three available application subscription levels
        $data['firstSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-one'))->firstOrFail();
        $data['secondSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-two'))->firstOrFail();
        $data['thirdSubscription'] = Subscription_type::where('id','=', Config::get('subscription_levels.level-three'))->firstOrFail();

        //get available company's referral points
        $data['referralPointsLabel'] = $this->checkReferralPoints(Auth::user()->company->ref_points);

        return View::make('auth.re_newed_subscription', $data);
    }

    /**
     * Get the response from the Helcim hosted payment page after payment
     * @param RegisterRepository $registerRepository
     * @param ReferralsRepository $referralsRepository
     * @param Mailer $mailer
     * @return View
     */
    public function getResponse(RegisterRepository $registerRepository, ReferralsRepository $referralsRepository, Mailer $mailer)
    {
        //get payment page response
        $response = (int)Input::get('response');

        //response = 1 means that Helcim payment was successfull
        if ($response === 1) {
            $data['paymentType'] = Input::get('paymentType');
            $data['email'] = Input::get('email');
            $data['companyId'] = Input::get('companyId');
            $data['projectCompanyId'] = Input::get('projectCompanyId');
            $data['bidderId'] = Input::get('bidderId');
            $data['subscriptionId'] = Input::get('subscriptionId');
            $data['cardToken'] = Input::get('cardToken');
            $data['amount'] = Input::get('amount');
            $data['transactionId'] = Input::get('transactionId');
            $data['referralToken'] = Input::get('referralToken');
            $data['subToken'] = Input::get('subToken');

            //store transaction in database
            $registerRepository->storeTransaction($data);

            //activate user
            $registeredUser = User::where('email','=',$data['email'])->where('comp_id','=',$data['companyId'])->firstOrFail();
            $registeredUser->active = 1;
            $registeredUser->save();

            //update company subscription level (because at register all companies are registered as subscription level 1), dates, etc
            $registerRepository->updateSubscriptionLevel($data['companyId'], $data['subscriptionId']);

            //redirect to proper pages with messages
            if ($data['paymentType'] == 'regular') {
                $data['view'] = 'emails.registration_receipt';
                $data['subject'] = 'Cloud PM -  Registration Receipt';

                //send confirmation email
                $dataArr = array(
                    'name' => $registeredUser->name,
                    'email' => $registeredUser->email,
                    'code' => $registeredUser->confirmation_code,
                    'token' => $data['referralToken'],
                    'subToken' => $data['subToken'],
                    'projectCompanyId' => $data['projectCompanyId'],
                    'bidderId' => $data['bidderId']
                );
                $registerRepository->sendConfirmationEmail($dataArr);

                //send receipt email
                $mailer->sendChangeSubscriptionReceipt($data);

                Flash::info(trans('labels.confirmation.create.email'));
                return view('auth.activate_account');
            } else if ($data['paymentType'] == 'change') {
                $data['view'] = 'emails.change_subscription';
                $data['subject'] = 'Cloud PM - Subscription Receipt';

                //check if there is queued referral for level 0
                $queuedReferral = $referralsRepository->getQueueReferral($data['companyId']);
                if(!is_null($queuedReferral) && !empty($queuedReferral->comp_from)) {
                    $referralsRepository->updateReferralPointsByCompanyId($queuedReferral, $data['subscriptionId']);
                    $queuedReferral->delete();
                }

                //send receipt email
                $mailer->sendChangeSubscriptionReceipt($data);
                Flash::info(trans('labels.change_subscription.success'));
                return Redirect::to('/company-profile/'.$data['companyId']);
            } else if ($data['paymentType'] == 'renewed') {
                $data['view'] = 'emails.change_subscription';
                $data['subject'] = 'Cloud PM - Subscription Receipt';

                //check if there is queued referral for level 0
                $queuedReferral = $referralsRepository->getQueueReferral(Auth::user()->comp_id);
                if(!is_null($queuedReferral) && !empty($queuedReferral->comp_from)) {
                    $referralsRepository->updateReferralPointsByCompanyId($queuedReferral, $data['subscriptionId']);
                    $queuedReferral->delete();
                }

                //send receipt email
                $mailer->sendChangeSubscriptionReceipt($data);
                Flash::info(trans('labels.renewed.success'));
                return Redirect::to('/company-profile/'.$data['companyId']);
            } else {
                Flash::error(trans('labels.bad_request'));
                return Redirect::to('/company-profile/'.$data['companyId']);
            }
        }
        //bad response handling
        if (Input::get('paymentType') == 'regular') {
            Flash::error(trans('labels.confirmation.create.error'));
            return view('auth.activate_account');
        } else if (Input::get('paymentType') == 'change') {
            Flash::error(trans('labels.change_subscription.error'));
            return Redirect::to('/company-profile/'.Auth::user()->comp_id);
        } else if (Input::get('paymentType') == 'renewed') {
            Flash::error(trans('labels.renewed.error'));
            return Redirect::to('/company-profile/'.Auth::user()->comp_id);
        } else {
            Flash::error(trans('labels.bad_request'));
            return Redirect::to('/company-profile/'.Auth::user()->comp_id);
        }
    }

    /**
     * Calculate new subscription amount that should be paid
     * @param $newSubscriptionId
     * @return float
     */
    public function calculateNewAmount($newSubscriptionId)
    {
        $totalSubscriptionDays = Config::get('payment.numberOfDays');

        $company = Company::where('id', '=', Auth::user()->comp_id)->first();
        $now = time();

        //expiry date from old subscription
        $subscriptionExpiryDate = strtotime($company->subscription_expire_date);

        //remaining days in old subscription
        $remainingDays = floor(($subscriptionExpiryDate - $now)/(60*60*24));

        //old subscription price
        $oldSubscriptionLevel = Subscription_type::where('id','=',$company->subs_id)->firstOrFail();
        $oldLevelPrice = $oldSubscriptionLevel->amount;

        //new subscription price
        $newSubscriptionLevel = Subscription_type::where('id','=',$newSubscriptionId)->firstOrFail();
        $newLevelPrice = $newSubscriptionLevel->amount;

        $amount = ($newLevelPrice/$totalSubscriptionDays * ($totalSubscriptionDays - $remainingDays)) + ($newLevelPrice/$totalSubscriptionDays - $oldLevelPrice/$totalSubscriptionDays) * ($remainingDays);

        return ceil($amount);

    }

}
