<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\BlogRequest;

use App\Modules\Blog\Repositories\BlogRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Models\Blog;
use App\Utilities\FileStorage;

use Request;
use Input;
use Flash;
use Auth;
use Config;

class BlogController extends Controller {

    private $blogRepo;
    private $projectRepo;
    private $filesRepo;

    public function __construct(BlogRepository $blogRepo, ProjectRepository $projectRepo, FilesRepository $filesRepo)
    {
        $this->middleware('auth');

        //Middleware that check if the company has Level 0 subscription
        //Level 0 companies should have access only to the master files of the shared projects
        $this->middleware('level_zero_permit');

        $this->middleware('unshared_project');
        $this->middleware('blog_permissions');
        $this->middleware('blog_write_delete_permissions', ['only' => ['create','store','edit','update','destroy']]);
        $this->middleware('sub_module_write', ['only' => ['create','store','edit','update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->blogRepo = $blogRepo;
        $this->projectRepo = $projectRepo;
        $this->filesRepo = $filesRepo;
    }

    /**
     * load blog posts with author
     * load last uploaded files
     * load index view
     * @param $projectId
     * @param CompaniesRepository $companyRepo
     * @return \Illuminate\View\View
     */
    public function index($projectId, CompaniesRepository $companyRepo)
    {
        $blogPosts = $this->blogRepo->getBlogPostsByProject($projectId);
        $project = $this->projectRepo->getProject($projectId);
        $author = $companyRepo->getCompanyAdminInfo($project->comp_id);
        //$files = $this->filesRepo->getLastUploadedFiles($projectId);

        return view('projects.blog.index',compact('project', 'blogPosts', 'author'));
    }

    /**
     * get project and load create view
     * @param $projectId
     * @return \Illuminate\View\View
     */
    public function create($projectId)
    {
        $project = $this->projectRepo->getProject($projectId);
        return view('projects.blog.create', compact('project'));
    }

    /**
     * store blog post data handled in validation request
     * @param $projectId
     * @param BlogRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, BlogRequest $request)
    {
        $data = $request->all();

        //store post
        $storePost = $this->blogRepo->storeBlogPost($projectId, $data);

        //success/error messages
        if ($storePost) {
            Flash::success(trans('messages.projectItems.insert.success', ['item' => 'blog post']));
        } else {
            Flash::error(trans('messages.projectItems.insert.error', ['item' => 'blog post']));
        }

        return redirect('projects/'.$projectId.'/blog/');
    }

    /**
     * @param $projectId
     * @param $blogPostID
     */
    public function show($projectId, $blogPostID, CompaniesRepository $companyRepo)
    {
        $blogPost = $this->blogRepo->getBlogPost($blogPostID);
        $project = $this->projectRepo->getProject($projectId);
        $author = $companyRepo->getCompanyAdminInfo($project->comp_id);
        if ($blogPost) {
            return view('projects.blog.view',compact('project','blogPost', 'author'));
        } else {
            Flash::error(trans('messages.projectItems.blog.post_not_found'));
            return redirect('projects/'.$projectId.'/blog/');
        }
    }

    /**
     * get post and project
     * load edit view
     * @param $projectId
     * @param $postId
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $postId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $blogPost = $this->blogRepo->getBlogPost($postId);

        return view('projects.blog.edit',compact('project','blogPost'));
    }


    /**
     * update blog post data handled in validation request
     * @param $projectId
     * @param $postId
     * @param BlogRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $postId, BlogRequest $request)
    {
        $data = $request->all();
        $updatePost = $this->blogRepo->updateBlogPost($postId,$data);

        //success/error messages
        if ($updatePost) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'blog post']));
        } else {
            Flash::error(trans('messages.projectItems.update.error', ['item' => 'blog post']));
        }

        return redirect('projects/'.$projectId.'/blog/');
    }

    /**
     * delete post by id
     * @param $projectId
     * @param $postID
     */
    public function destroy($projectId, $postId)
    {
        $deletePost =  $this->blogRepo->deleteBlogPost($postId);

        //success/error messages
        if ($deletePost) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'blog post']));
        } else {
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'blog post']));
        }

        return redirect('projects/'.$projectId.'/blog/');
    }

    /**
     * get file by name and type (download)
     * @param $projectId
     * @param $type
     * @param $name
     * @return mixed
     */
    public function getFile($projectId, $type, $name)
    {
        //get path from file name
        $path = FileStorage::pathFromName($type, $name, $projectId);

        //get file
        $file = $this->filesRepo->getFile($path, $name);
        return $file;
    }
}
