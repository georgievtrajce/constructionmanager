<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\PcoVersionRequest;
use App\Http\Requests\PcoVersionTransmittalNotesRequest;
use App\Modules\Company_profile\Repositories\CompaniesRepository;
use App\Modules\Files\Interfaces\CheckUploadedFilesInterface;
use App\Modules\Manage_users\Repositories\ManageUsersRepository;
use App\Modules\Pcos\Implementations\PcoVersionsWrapper;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Transmittals\Repositories\TransmittalsRepository;
use App\Utilities\ClassMap;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class PcoVersionsController extends Controller {

    /**
     * @var CompaniesRepository
     */
    private $companiesRepository;

    public function __construct(CompaniesRepository $companiesRepository)
	{
		$this->middleware('sub_module_write', ['only' => ['create','store','update']]);
		$this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->companiesRepository = $companiesRepository;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param $versionType
	 * @param $pcoSubcontractorRecipientId
	 * @param ProjectRepository $projectRepository
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 */
	public function create($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, ProjectRepository $projectRepository, PcosRepository $pcosRepository)
	{
		//get the proper project
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

		//get the proper pco
		$data['pco'] = $pcosRepository->getPco($pcoId);
		$data['pcoSubcontractorRecipient'] = $pcosRepository->getPcoSubcontractorRecipient($pcoId, $pcoSubcontractorRecipientId);

		$data['versionType'] = $versionType;
		$data['pcoSubcontractorRecipientId'] = $pcoSubcontractorRecipientId;
        $statuses = $pcosRepository->getAllPcoStatuses();
        $data['statuses']= [];
        $rurStatusId = 0;
        foreach ($statuses as $status) {
            $data['statuses'][$status->id] = $status->name;
            if(Config::get('constants.received_under_review') == $status->name) $rurStatusId = $status->id;
        }

        //get status from last version id
        $data['lastVersionStatus'] = 8; //Draft status
        if(isset($data['pcoSubcontractorRecipient']['type']) &&
            $data['pcoSubcontractorRecipient']['type'] == Config::get('constants.pco_subcontractor_or_recipient.recipient'))
        {
            if(isset($data['pco']['recipient']['latest_recipient_version']['status_id']))
            {
                //remove Received - Under Review from recipient status list
                if($rurStatusId > 0){
                    unset($data['statuses'][$rurStatusId]);
                }
            }
        }

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;

		return view('projects.pcos.create-version', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param $versionType
	 * @param $pcoSubcontractorRecipientId
	 * @param PcoVersionRequest $request
	 * @param PcosRepository $pcosRepository
	 * @param PcoVersionsWrapper $versionsWrapper
	 * @return Response
	 */
	public function store($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, PcoVersionRequest $request, PcosRepository $pcosRepository, PcoVersionsWrapper $versionsWrapper)
	{
		//Store pco subcontractor
		$response = $versionsWrapper->wrapVersionStore(ClassMap::instance(Config::get('classmap.pcos.'.$versionType)), $pcoId, $versionType, $pcoSubcontractorRecipientId, $request->all());

		if ($response) {
			//Successfully created pco
			Flash::success(trans('messages.submittals.store.success', ['item' => 'PCO Cycle']));
            return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$response->id.'/edit');
		}

		Flash::error(trans('messages.submittals.store.error', ['item' => 'PCO Cycle']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/edit');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param $id
	 * @return Response
	 * @internal param int $id
	 */
	public function show($id)
	{
		//
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param $projectId
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param ProjectRepository $projectRepository
     * @param PcosRepository $pcosRepository
     * @param TransmittalsRepository $transmittalsRepository
     * @param CheckUploadedFilesInterface $checkUploadedFiles
     * @param ManageUsersRepository $manageUsersRepository
     * @return Response
     * @internal param int $id
     */
    public function edit(
        $projectId,
        $pcoId,
        $versionType,
        $pcoSubcontractorRecipientId,
        $versionId,
        ProjectRepository $projectRepository,
        PcosRepository $pcosRepository,
        TransmittalsRepository $transmittalsRepository,
        CheckUploadedFilesInterface $checkUploadedFiles,
        ManageUsersRepository $manageUsersRepository
    )
	{
		//get the proper project
        $project = $projectRepository->getProject($projectId);
        $data['project'] = $project;

        $myCompany = $this->companiesRepository->getCompanyWithUsersByID(Auth::user()->comp_id);

        $projectUsers = $manageUsersRepository->getProjectPermissionUsers($projectId);

        $projectUserIds = [];
        foreach ($projectUsers as $projectUser) {
            $projectUserIds[] = $projectUser->user_id;
        }

		//get the proper pco
		$data['pco'] = $pcosRepository->getPco($pcoId);
		$data['pcoVersion'] = $pcosRepository->getPcoVersion($versionId);
        if(isset($data['pcoVersion']['recipient']) && isset($data['pcoVersion']['files']) && count($data['pcoVersion']['files']) > 0)
        {
            $data['pcoVersion']['email_transmittal_files'] = $checkUploadedFiles->checkAndOrderUploadedFilesForVersion($data['pcoVersion']['files'], Config::get('constants.recipient_versions'));
        }

        if(isset($data['pcoVersion']['subcontractor']) && isset($data['pcoVersion']['files']) && count($data['pcoVersion']['files']) > 0)
        {
            $data['pcoVersion']['email_transmittal_files'] = $checkUploadedFiles->checkAndOrderUploadedFilesForVersion($data['pcoVersion']['files'], Config::get('constants.subcontractor_versions'));
        }

		$data['rec_sub'] = ($data['pcoVersion']->rec_sub != 0) ? Carbon::parse($data['pcoVersion']->rec_sub)->format('m/d/Y') : '';
		$data['sent_appr'] = ($data['pcoVersion']->sent_appr != 0) ? Carbon::parse($data['pcoVersion']->sent_appr)->format('m/d/Y') : '';
		$data['rec_appr'] = ($data['pcoVersion']->rec_appr != 0) ? Carbon::parse($data['pcoVersion']->rec_appr)->format('m/d/Y') : '';
		$data['subm_sent_sub'] = ($data['pcoVersion']->subm_sent_sub != 0) ? Carbon::parse($data['pcoVersion']->subm_sent_sub)->format('m/d/Y') : '';

		$data['versionType'] = $versionType;
		$data['pcoSubcontractorRecipientId'] = $pcoSubcontractorRecipientId;
		$statuses = $pcosRepository->getAllPcoStatuses();
        $rurStatusId = 0;
        foreach ($statuses as $key => $status) {
            $data['statuses'][$status->id] = $status->name;
            if(Config::get('constants.received_under_review') == $status->name) $rurStatusId = $status->id;
        }
        if(isset($data['pcoVersion']['recipient'])) {
            //remove Received - Under Review from recipient status list
            if($rurStatusId > 0){
                unset($data['statuses'][$rurStatusId]);
            }
        }

		//check if transmittals are generated
		$data['recTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.pcos') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.sent_appr'));
		$data['subTransmittal'] = $transmittalsRepository->getTransmittal(Config::get('constants.pcos') ,$projectId, $versionId, Config::get('constants.transmittal_date_type.subm_sent_sub'));

        $companies = [];
        $users = [];
        $projectsCompanies = [];
        foreach ($project->projectCompanies as $subContractor)
        {
            if (!empty($subContractor->address_book)) {
                $companies[$subContractor->id] = $subContractor->address_book->name;
                $projectsCompanies[$subContractor->id] = $subContractor->proj_id;
                $users[$subContractor->id] = $subContractor->project_company_contacts;
            }
        }

        $distributionListContactsAppr = $pcosRepository->getContactsForPcoVersion($versionId, 'appr');
        $selectedDistributionsAppr = [];
        $selectedDistributionsUsersAppr = [];
        foreach ($distributionListContactsAppr as $contact) {
            if (!empty($contact->ab_cont_id)) {
                $selectedDistributionsAppr[] = $contact->ab_cont_id;
            } else {
                $selectedDistributionsUsersAppr[] = $contact->user_id;
            }
        }

        $data['selectedDistributionsAppr'] = $selectedDistributionsAppr;
        $data['selectedDistributionsUsersAppr'] = $selectedDistributionsUsersAppr;
        $data['companies'] = $companies;
        $data['projectsCompanies'] = $projectsCompanies;
        $data['users'] = $users;
        $data['myCompany'] = $myCompany;
        $data['projectUsers'] = $projectUserIds;

		return view('projects.pcos.edit-version', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param $versionType
	 * @param $pcoSubcontractorRecipientId
	 * @param $versionId
	 * @param PcoVersionRequest $request
	 * @param PcoVersionsWrapper $versionsWrapper
	 * @return Response
	 * @throws \Exception
	 * @internal param int $id
	 */
	public function update($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, PcoVersionRequest $request, PcoVersionsWrapper $versionsWrapper)
	{
		//Store pco subcontractor
		$response = $versionsWrapper->wrapVersionUpdate(ClassMap::instance(Config::get('classmap.pcos.'.$versionType)), $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $request->all());

		if ($response) {
			//Successfully created pco
			Flash::success(trans('messages.submittals.update.success', ['item' => 'PCO Cycle']));
            return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$versionId.'/edit');
        }

		Flash::error(trans('messages.submittals.update.error', ['item' => 'PCO Cycle']));

        return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$versionId.'/edit');
	}

    /**
     * Update the specified resource in storage.
     *
     * @param $projectId
     * @param $pcoId
     * @param $versionType
     * @param $pcoSubcontractorRecipientId
     * @param $versionId
     * @param PcoVersionRequest $request
     * @param PcoVersionsWrapper $versionsWrapper
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function updateTransmittalNotes($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, PcoVersionTransmittalNotesRequest $request, PcoVersionsWrapper $versionsWrapper)
    {
        //Store pco subcontractor
        $versionsWrapper->wrapVersionTransmittalNotesUpdate(ClassMap::instance(Config::get('classmap.pcos.'.$versionType)), $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, $request->all());
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param $projectId
	 * @param $pcoId
	 * @param $versionType
	 * @param $pcoSubcontractorRecipientId
	 * @param $versionId
	 * @param PcosRepository $pcosRepository
	 * @return Response
	 * @internal param int $id
	 */
	public function destroy($projectId, $pcoId, $versionType, $pcoSubcontractorRecipientId, $versionId, PcosRepository $pcosRepository)
	{
        $idsArray = explode('-', $versionId);

        $errorEntry = false;
        foreach ($idsArray as $id) {
            //select pco versions for file deleting
            $pcoVersion = $pcosRepository->getPcoVersion($id);

            //delete pco version files from s3
            foreach ($pcoVersion->files as $pcoVersionFile) {
                //delete pco version files
                $pcosRepository->deletePcoFile($projectId, $pcoVersionFile->file->file_name);
            }

            //delete pcos and its versions from database
            $response = $pcosRepository->deletePcoVersion($versionType, $pcoSubcontractorRecipientId, $id);

            if (is_null($response)) {
                $errorEntry = true;
            }
        }

		//Successfully deleted pco
		if (!$errorEntry) {
			Flash::success(trans('messages.submittals.delete.success', ['item' => 'PCO Cycle']));
			return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/edit');
		}

		//Not successful deleting
		Flash::error(trans('messages.submittals.delete.error', ['item' => 'PCO Cycle']));
		return Redirect::to('projects/'.$projectId.'/pcos/'.$pcoId.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/edit');
	}

}
