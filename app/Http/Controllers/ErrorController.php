<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ErrorController extends Controller {

    /**
     * Displays error page
     *
     * @param $code
     * @param $errorMessage
     * @return \Illuminate\View\View
     */
	public function show($code, $errorMessage)
	{
        return view('errors.error-page', compact('code', 'errorMessage'));
	}

    /**
     * Displays error page
     *
     * @param $code
     * @param $errorMessageLarge
     * @param $errorMessageSmall
     * @return \Illuminate\View\View
     */
    public function showErrorPagePublic($code, $errorMessageLarge, $errorMessageSmall)
    {
        return view('errors.error-page-file-missing', compact('code', 'errorMessageLarge', 'errorMessageSmall'));
    }
}
