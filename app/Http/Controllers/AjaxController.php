<?php
namespace App\Http\Controllers;

use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Materials_and_services\Repositories\MaterialsRepository;
use App\Modules\Pcos\Repositories\PcosRepository;
use App\Modules\Proposals\Repositories\ProposalsRepository;
use App\Modules\Rfis\Repositories\RfisRepository;
use App\Modules\Submittals\Repositories\SubmittalsRepository;
use Illuminate\Support\Facades\Auth;


class AjaxController extends Controller {

    /**
     * @var ProposalsRepository
     */
    private $proposalsRepository;
    /**
     * @var ContractsRepository
     */
    private $contractsRepository;
    /**
     * @var SubmittalsRepository
     */
    private $submittalsRepository;
    /**
     * @var RfisRepository
     */
    private $rfisRepository;
    /**
     * @var PcosRepository
     */
    private $pcosRepository;
    /**
     * @var MaterialsRepository
     */
    private $materialsRepository;

    /**
     * AjaxController constructor.
     * @param ProposalsRepository $proposalsRepository
     * @param ContractsRepository $contractsRepository
     * @param SubmittalsRepository $submittalsRepository
     * @param RfisRepository $rfisRepository
     * @param PcosRepository $pcosRepository
     * @param MaterialsRepository $materialsRepository
     */
    public function __construct(ProposalsRepository $proposalsRepository, ContractsRepository $contractsRepository,
                                SubmittalsRepository $submittalsRepository, RfisRepository $rfisRepository,
                                PcosRepository $pcosRepository, MaterialsRepository $materialsRepository)
    {
        $this->middleware('auth');
        $this->proposalsRepository = $proposalsRepository;
        $this->contractsRepository = $contractsRepository;
        $this->submittalsRepository = $submittalsRepository;
        $this->rfisRepository = $rfisRepository;
        $this->pcosRepository = $pcosRepository;
        $this->materialsRepository = $materialsRepository;
    }

    public function getAllModuleItems($projectId, $typeId)
    {
        $items = [];
        switch ($typeId) {
            case 4: //bids
                $items = $this->proposalsRepository->getAllProposalsByProject($projectId, 'name', 'asc');
                break;
            case 5: //contracts
                $items = $this->contractsRepository->getAllContractsByProject($projectId, 'contracts.name', 'asc');
                break;
            case 3: //submittals
                $items = $this->submittalsRepository->getAllSubmittalsForProject($projectId, Auth::user()->comp_id);
                break;
            case 9: //rfis
                $items = $this->rfisRepository->getAllRFIsForProject($projectId, Auth::user()->comp_id);
                break;
            case 10: //pcos
                $items = $this->pcosRepository->getAllPcosForProject($projectId, Auth::user()->comp_id);
                break;
            case 6: //materials
                $items = $this->materialsRepository->getAllMaterialsByProject($projectId, 'name', 'asc');
                break;
        }

        return view('ajax.items-options',compact('items'));
    }
}
