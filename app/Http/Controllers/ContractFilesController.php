<?php
namespace App\Http\Controllers;

use App\Modules\Contracts\Repositories\ContractsRepository;
use App\Modules\Data_transfer_limitation\Implementations\DataTransferLimitation;
use App\Modules\Project\Repositories\ProjectRepository;
use App\Modules\Files\Repositories\FilesRepository;
use App\Http\Requests\FilesRequest;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use App\Requests;
use App\Utilities\FileStorage;

use Illuminate\Support\Facades\Redirect;
use Request;
use Input;
use Flash;
use Auth;

class ContractFilesController extends Controller
{
    private $contractsRepo;
    private $projectRepo;
    private $filesRepo;

    public function __construct(ContractsRepository $contractsRepo, ProjectRepository $projectRepo, FilesRepository $filesRepo)
    {
        $this->middleware('auth');
        $this->middleware('sub_module_write', ['only' => ['create','store','edit','update']]);
        $this->middleware('sub_module_delete', ['only' => ['destroy']]);
        $this->contractsRepo = $contractsRepo;
        $this->projectRepo = $projectRepo;
        $this->filesRepo = $filesRepo;
    }

    /**
     * list all contracts for a project by company
     * @param $projectId
     * @param $contractId
     * @return \Illuminate\View\View
     */
    public function index($projectId, $contractId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $files = $this->contractsRepo->getContractFiles($projectId, $contractId);

        return view('projects.contracts.files.index',compact('files','project'));

    }

    /**
     * store file record to db and file to aws
     * @param $projectId
     * @param $id
     * @param FilesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store($projectId, $id, FilesRequest $request)
    {
        $data = $request->all();

        //insert record in files table
        $this->filesRepo->storeFileRecord($data);

        //insert record in contracts table
        $this->contractsRepo->storeFile($id,$data['file_id'], $data['file_cost']);

        Flash::success(trans('messages.projectFiles.upload.success', ['item'=> 'contract file']));

        return redirect('projects/'.$projectId.'/contracts/'.$id.'/edit');
    }

    /**
     * download file, if pdf open in new tab
     * @param $projectId
     * @param $id
     * @param $name
     * @param ProjectFilesRepository $filesRepository
     * @param DataTransferLimitation $transferLimitation
     * @return mixed
     */
    public function show($projectId, $id, $name, ProjectFilesRepository $filesRepository, DataTransferLimitation $transferLimitation)
    {
        //get proposal file
        $contractFile = $filesRepository->getProjectFileByName($projectId, $name);

        //check download file allowance
        if ($transferLimitation->checkDownloadTransferAllowance($contractFile->size)) {
            $params = array('proj_id'=>$projectId, 'contr_id' => $id);

            $path = FileStorage::path('contract', $params);

            $file = $this->filesRepo->getFile($path, $name);
            $this->filesRepo->logDownloadByName($name);

            //increase company download transfer
            $transferLimitation->increaseDownloadTransfer($contractFile->size);

            return $file;
        }

        Flash::error(trans('messages.data_transfer_limitation.download_error', ['item' => trans('labels.Contract')]));
        return Redirect::back();
    }

    /**
     * load edit view with contract and file data
     * @param $projectId
     * @param $id
     * @param $fileId
     * @return \Illuminate\View\View
     */
    public function edit($projectId, $id, $fileId)
    {
        $project = $this->projectRepo->getProject($projectId);
        $file = $this->contractsRepo->getContractFile($projectId, $fileId);

        return view('projects.contracts.files.edit',compact('file','project'));
    }

    /**
     * delete file and db records
     * @param $projectId
     * @param $id
     * @param $fileId
     * @return Redirect
     */
    public function destroy($projectId, $id, $fileId)
    {
        $params = array('proj_id'=>$projectId, 'contr_id'=>$id);

        //get aws path
        $path = FileStorage::path('contract', $params);

        $idsArray = explode('-', $fileId);

        $errorEntry = false;
        foreach ($idsArray as $contactFileid) {

            $file = $this->contractsRepo->getFile($contactFileid);

            //delete file by name and path
            $deleteFile = $this->filesRepo->deleteFile($path, $file->file_name);

            if ($deleteFile) {
                $this->contractsRepo->deleteFile($contactFileid);
            } else {
                $errorEntry = true;
            }
        }

        if (!$errorEntry) {
            Flash::success(trans('messages.projectItems.delete.success', ['item' => 'contract file']));
        } else {
            //else notify user
            Flash::error(trans('messages.projectItems.delete.error', ['item' => 'contract file']));
        }

        return redirect('projects/'.$projectId.'/contracts/'.$id.'/files');
    }

    /**
     * update file and record
     * @param $projectId
     * @param $id
     * @param $fileId
     * @param FilesRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($projectId, $id, $fileId, FilesRequest $request)
    {
        $data = $request->all();

        $contractFileCostUpdate = $this->contractsRepo->updateContractFileCost($id, $fileId, $data['file_cost']);

        $fileUpdated = $this->contractsRepo->updateContractFile($fileId, $data);

        if ($fileUpdated && !is_null($contractFileCostUpdate)) {
            Flash::success(trans('messages.projectItems.update.success', ['item' => 'contract file']));
        } else {
            //else notify user
            Flash::error(trans('messages.projectItems.update.success', ['item' => 'contract file']));
        }

        return redirect('projects/'.$projectId.'/contracts/'.$id.'/edit');
    }
}