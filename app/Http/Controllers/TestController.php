<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Project_file;
use App\Modules\Project_files\Repositories\ProjectFilesRepository;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class TestController extends Controller {

    public function uploadLocally()
    {
        $contents = Storage::disk('local')->get('test.pdf');
        $s3 = Storage::disk('s3');
        $s3Response = $s3->put('test.pdf', $contents);
        Log::debug('file uploaded on s3');
        return $s3Response;
    }

    public function getFileForStreaming()
    {
        $getFile = Storage::disk('s3')->get('test.pdf');
        $response = Response::make($getFile, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
        Log::debug('get file for streaming');
        return $response->send();
    }

	public function test(PDF $pdf)
    {
        set_time_limit(-1);
        Log::debug('process started');

//        //save file in database
//        Project_file::create([
//            'ft_id' => 999,
//            'user_id' => 999,
//            'comp_id' => 999,
//            'proj_id' => 999,
//            'file_name' => 'test.pdf',
//        ]);
//        Log::debug('file stored in database');

        //$savePdf = App::make('dompdf.wrapper');
        $pdf->loadView('test', [])->save(storage_path().'/app/test.pdf');
        Log::debug('file saved locally');

        $this->uploadLocally();

        $getFile = $this->getFileForStreaming();

        return $getFile;

//        $contents = Storage::disk('local')->get('test.pdf');
//        $s3 = Storage::disk('s3');
//        $s3Response = $s3->put('test.pdf', $contents);
//        Log::debug('file uploaded on s3');
//
//        $getFile = Storage::disk('s3')->get('test.pdf');
//        $response = Response::make($getFile, 200)->header('Content-type','application/pdf','Content-Disposition', 'attachment');
//        Log::debug('get file for streaming');
//        return $response->send();
//        die();
    }

}
