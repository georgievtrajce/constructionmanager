<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class PcosRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'name' => 'required|max:255',
			'number' => 'required|max:255',
			'cost' => 'regex:/^[0-9.,]+$/',
            'sub_id' => 'exists:address_book,id',
            'sub_contact' => 'exists:ab_contacts,id',
		];
		return $rules;
	}

}
