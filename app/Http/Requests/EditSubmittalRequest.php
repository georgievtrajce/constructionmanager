<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class EditSubmittalRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'master_format_number' => 'required|max:255',
			'master_format_title' => 'required|max:255',
			'name' => 'required|max:255',
			'number' => 'required|max:45',
			'subject' => 'max:255',
			'sent_via' => 'max:255',
			'submittal_type' => 'max:255',
			'quantity' => 'numeric',
			'sub_id' => 'exists:address_book,id',
			//'sub_office' => 'exists:addresses,id',
			'sub_contact' => 'exists:ab_contacts,id',
			'recipient_id' => 'exists:address_book,id',
			//'recipient_office' => 'exists:addresses,id',
			'recipient_contact' => 'exists:ab_contacts,id',
		];
	}

}
