<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProposalsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('projects/*/bids/*/bidders')) {
            $rules = [
                'prop_ab_id'  => 'required',
                'prop_addr'  => 'required',
                'cont_id'  => 'required',
                'proposal_1' => 'regex:/^[0-9.,]+$/',
                'proposal_2' => 'regex:/^[0-9.,]+$/',
                'proposal_final'=> 'regex:/^[0-9.,]+$/'
            ];
        }
        else if (Request::is('projects/*/bids/*/bidders/*')) {
            $rules = [
                'prop_ab_id'  => 'required',
                'addr_id'  => 'required',
                'ab_cont_id'  => 'required',
                'proposal_1' => 'regex:/^[0-9.,]+$/',
                'proposal_2' => 'regex:/^[0-9.,]+$/',
                'proposal_final'=> 'regex:/^[0-9.,]+$/'
            ];
        }
        else if (Request::is('projects/*/bids') || Request::is('projects/*/bids/*')){
            $rules = [
                'name'  => 'required|max:255',
                'master_format_number' => 'required|max:255|regex:/^\d*(?:\.\d{0,4})?$/',
                'master_format_title' => 'required|max:255',
                'value' => 'regex:/^[0-9.,]+$/',
                'unit_price'=> 'regex:/^[0-9.,]+$/',
                'status'=> 'required|max:15',
                'proposals_needed_by' => 'date',
            ];
        }
        else {
            $rules = array();
        }

        return $rules;
    }

}
