<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class MasterFormatRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if (Request::is('masterformat/import')) {
			return [
				'master_format'  => 'required|max:10000',
			];
		}
		else if (Request::is('masterformat/*')) {
			$id = Request::segment(2);
			return [
				'title'  => 'required|min:4',
				'number' => ['required','regex:(^[0-9]{6}|\.[0-9]{2}$)',  'unique:master_format,number,'.$id],
			];
		}
		else return [
			'title'  => 'required|min:4',
			'number' => ['required','regex:(^[0-9]{6}|\.[0-9]{2}$)',  'unique:master_format,number'],
		];
	}

}
