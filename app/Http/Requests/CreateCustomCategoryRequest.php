<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateCustomCategoryRequest extends Request {

	private $companyId;

	public function __construct()
	{
		$this->companyId = Auth::user()->comp_id;
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (!Auth::check())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255|unique:custom_categories,name,NULL,id,comp_id,'.$this->companyId
		];
	}

}
