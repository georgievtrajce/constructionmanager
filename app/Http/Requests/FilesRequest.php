<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class FilesRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('projects/*/contracts/*/files')) {
            $rules = ['name' => 'required|max:255',
                'number' => 'required|max:45',
                'file_id' => 'required|numeric|exists:files,id'];
        }
        else if (Request::is('projects/*/contracts/*/files/*')){
            $rules = ['name' => 'required|max:255',
                'number' => 'required|max:45'];
        }
        else if (Request::is('projects/*/proposals/*/suppliers/*/files')) {
            $rules = ['name' => 'required|max:255',
                'number' => 'required|max:45',
                'file_id' => 'required|numeric|exists:files,id'];
        }
        else if (Request::is('projects/*/proposals/*/suppliers/*/files/*')){
            $rules = ['name' => 'required|max:255',
                'number' => 'required|max:45'];
        }
        else {
            $rules = array();
        }

        return $rules;
    }

}
