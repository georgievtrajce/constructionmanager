<?php
namespace App\Http\Requests;
use Auth;
use App\Http\Requests\Request;

class ProjectsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('projects/*/companies')) {
            $rules = ['ab_id'  => 'required'];
        }
        else if (Request::is('projects/*/subcontractors')) {
            $rules = ['comp_id'  => 'required',
            'addresses' => 'required'];
        }
        else  {
            $rules = [
                'name' => 'required|max:255',
                'number' => 'required|max:45',
                'start_date' => 'date',
                'end_date' => 'date',
                'price' => 'regex:/^[0-9.,]+$/',
                'street' => 'max:255',
                'city'  => 'max:45',
                'state'  => 'max:45',
                'owner_id' => 'exists:address_book,id,owner_comp_id,'.Auth::user()->comp_id,
                'architect_id' => 'exists:address_book,id,owner_comp_id,'.Auth::user()->comp_id,
                'prime_subcontractor_id' => 'exists:address_book,id,owner_comp_id,'.Auth::user()->comp_id,
                'make_me_gc_id' => 'required_with:make_me_gc|exists:companies,id',
                'general_contractor_id' => 'required_with:general_contractor|exists:address_book,id',
            ];

            if(!empty(Request::get('contractor_transmittal_id')) && Request::get('contractor_transmittal_id') > 0) {
                $rules['contractor_transmittal_id'] = 'exists:ab_contacts,id';
            }
            if(!empty(Request::get('architect_transmittal_id')) && Request::get('architect_transmittal_id') > 0) {
                $rules['architect_transmittal_id'] = 'exists:ab_contacts,id';
            }
            if(!empty(Request::get('prime_subcontractor_transmittal_id')) && Request::get('prime_subcontractor_transmittal_id') > 0) {
                $rules['prime_subcontractor_transmittal_id'] = 'exists:ab_contacts,id';
            }
            if(!empty(Request::get('owner_transmittal_id')) && Request::get('owner_transmittal_id') > 0) {
                $rules['owner_transmittal_id'] = 'exists:ab_contacts,id';
            }
        }
        return $rules;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Please insert a Project Name',
            'number.required' => 'Please insert a Project Number'
        ];
    }

}
