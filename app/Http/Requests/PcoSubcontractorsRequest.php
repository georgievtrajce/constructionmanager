<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class PcoSubcontractorsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		if (Request::segment(5) == 'subcontractors') {
			$rules = [
				'master_format_number' => 'required|max:255',
				'master_format_title' => 'required|max:255',
				'name' => 'required|max:255',
				'sub_id' => 'required_without:self_performed|exists:address_book,id',
				//'sub_office' => 'exists:addresses,id',
				'sub_contact' => 'exists:ab_contacts,id',
			];
		} else {
			$rules = [
				'name' => 'required|max:255',
				'recipient_id' => 'required|exists:address_book,id',
				//'recipient_office' => 'exists:addresses,id',
				'recipient_contact' => 'exists:ab_contacts,id',
			];
		}

		return $rules;
	}

}
