<?php
namespace App\Http\Requests;
use Auth;
use App\Http\Requests\Request;

class ContractsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('projects/*/contracts/*/items')) {
            $rules = ['name' => 'required|max:255',
                'mf_number' => 'required|max:255|regex:/^\d{6}(?:\.\d\d)?$/',
                'mf_title' => 'required|max:255'];
        }
        else if (Request::is('projects/*/contracts/*/items/*')){
            $rules = ['name' => 'required|max:255',
                'mf_number' => 'required|max:255|regex:/^\d{6}(?:\.\d\d)?$/',
                'mf_title' => 'required|max:255'];
        }
        else if (Request::is('projects/*/contracts') || Request::is('projects/*/contracts/*')){
            $rules = [
                'contr_ab_id' => 'required|exists:address_book,id,owner_comp_id,'.Auth::user()->comp_id,
                'contr_ab' => 'required|max:255',
                'contr_price' => 'regex:/^[0-9.,]+$/',
                'number' => 'max:255',];
        }
        else {
            $rules = array();
        }

        return $rules;
    }


}
