<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateAddressBookAddressesRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (!Auth::check())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'office_title' => 'required|max:255',
			'ca_street' => 'required|max:255',
			'ca_town' => 'required|max:45',
			'ca_state' => 'required|max:45',
			'ca_zip' => 'required',
		];
	}

}
