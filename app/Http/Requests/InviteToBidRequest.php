<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class InviteToBidRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'bid_id' => 'required|exists:proposals,id',
			'bidder_id' => 'required|exists:proposal_suppliers,id',
			'ab_id' => 'required|exists:address_book,id',
		];
	}

}
