<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateFreeAccountRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'email' => 'required|email|max:255|unique:users',
			'company_name' => 'required|max:255',
			'company_logo' => 'mimes:jpeg,jpg,gif,png|max:2000',
			'company_addr_office_title_req' => 'required|max:255',
			'company_addr_street_req' => 'required|max:255',
			'company_addr_town_req' => 'required|max:45',
			'company_addr_state_req' => 'required||max:45',
			'company_addr_zip_code_req' => 'required|max:45',
			'admin_name' => 'required|max:255',
			'admin_title' => 'required|max:255',
			'admin_office_phone' => 'required|max:45',
			'subscription_type' => 'required|exists:subscription_types,id',
			'expiration_date' => 'required|date',
		];
	}

}
