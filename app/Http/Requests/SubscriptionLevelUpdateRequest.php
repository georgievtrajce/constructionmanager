<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class SubscriptionLevelUpdateRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'ref_points_needed' => 'numeric|digits_between:1,11',
			'ref_points' => 'numeric|digits_between:1,11',
			'upload_limit' => 'numeric|digits_between:1,255',
			'download_limit' => 'numeric|digits_between:1,255',
            'storage_limit' => 'numeric',
			'amount' => 'regex:/^[0-9.,]+$/',
		];
	}

}
