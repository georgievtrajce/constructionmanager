<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class DailyRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
	    $currentStep = Request::input('current_step');
//	    dd(Request::all());

	    switch ($currentStep) {
            case 1:
                return $rules = [
                    'report_num' => 'required|numeric',
                    'date' => 'required|date',
                ];
                break;
            case 2:
                return $rules = [
                    'temperature' => 'required|max:255',
                ];
                break;
            case 3:
                if(Request::input('contractor_tab') == Config::get('constants.workers-with-name')) {
                    return $rules = [
                        'worker_office' => 'required|max:50',
                        'worker_name' => 'required|max:50',
                        'title' => 'required|max:255',
                        'regular_hours' => 'required|numeric|digits_between:1,8',
                        'overtime_hours' => 'numeric|digits_between:1,8',
                    ];
                } else {
                    return $rules = [
                        'trade' => 'required|max:50',
                        'workers' => 'required|numeric',
                        'regular_hours' => 'required|numeric|digits_between:1,8',
                        'overtime_hours' => 'numeric|digits_between:1,8',
                    ];
                }
                break;
            case 4:
                return $rules = [
//                    'selected_comp_id' => 'required|numeric|exists:project_companies,id',
                    'selected_comp_id' => 'required|numeric',
                    'trade' => 'required|max:50',
                    'workers' => 'required|numeric',
                    'regular_hours' => 'required|numeric|digits_between:1,8',
                    'overtime_hours' => 'required|numeric|digits_between:1,8',
                ];
                break;
            case 5:
                return $rules = [
                    'equipment' => 'required|max:150',
                    'quality' => 'required|numeric',
                    'selected_comp_id' => 'required|numeric',
                    'hours' => 'required|numeric|digits_between:1,8',
                ];
                break;
            case 6:
                return $rules = [
                    'material_delivered' => 'required|max:150',
                    'quantity' => 'required',
                    'selected_comp_id' => 'required|numeric',
                ];
                break;
            case 7:
                $rules = [
                    'testing_performed' => 'required|max:150',
                    'mf_number' => 'required|max:255|regex:/^\d{6}(?:\.\d\d)?$/',
                    'mf_title' => 'required|max:255',
                    'selected_comp_id' => 'required|numeric',
                    'selected_employee_id' => 'required|numeric',
                ];

                (Request::input('selected_comp_id') > 0) ? $rules['selected_comp_id'] = 'required|numeric' : $rules['other_company'] = 'required';
                (Request::input('selected_employee_id') > 0) ? $rules['selected_employee_id'] = 'required|numeric' : $rules['other_employee'] = 'required';

                return $rules;
                break;
            case 8:
                return $rules = [
                    'safety_deficiencies_observed' => 'required|max:150',
                    'selected_comp_id' => 'required|numeric',
                ];
                break;
            case 9:
                return $rules = [
                    'selected_comp_id' => 'required|numeric',
                    'selected_employee_id' => 'required|numeric',
                ];
                break;
            case 10:
                return $rules = [
                    'general_notes' => 'required',
                ];
                break;
            case 11:
                return $rules = [
                    'name' => 'required|max:50',
                    'image_file' => 'required|image|mimes:jpeg,png,jpg,gif,bmp,svg|max:10000'
                ];
                break;
            case 12:
                return $rules = [
                    'name' => 'required|max:50',
                    'description' => 'required|max:100',
                    "pdf_file" => "required|mimes:pdf|max:10000"
                ];
                break;
            //upload file
            case 13:
                return $rules = [
                    'report_num' => 'required|numeric',
                    'date' => 'required|date',
                    "report_file" => "required|mimes:pdf|max:10000"
                ];
                break;
            //update file
            case 14:
                return $rules = [
                    'report_num' => 'required|numeric',
                    'date' => 'required|date',
                    "report_file" => "mimes:pdf|max:10000"
                ];
                break;
            default:
                //step1
                return $rules = [
                    'date' => 'required|date',
                    'report_num' => 'required|numeric',
                ];
                break;
        }
	}

    /**
     * Get the URL to redirect to on a validation error.
     *
     * @return string
     */
    protected function getRedirectUrl()
    {
        $url = $this->redirector->getUrlGenerator();
        $currentUrl = $url->previous();
        $step = Request::input('current_step');

        if($step && in_array($step, Config::get('constants.daily_report_steps'))) {
            //if currentUrl already have the "?step=" query string
            if($position = strpos($currentUrl, "?step=") != false) {
                $currentUrl = explode("?step=", $currentUrl);
                return $currentUrl[0] ."?step=$step";
            }
            return $currentUrl . "?step=$step";
        }
        return $currentUrl;
    }

}
