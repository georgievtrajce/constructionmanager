<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class AddSubmittalVersionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cycle_no' => 'required|numeric|max:10',
			'rec_sub' => 'date',
			'sent_appr' => 'date',
			'rec_appr' => 'date',
			'subm_sent_sub' => 'date',
			'rec_sub_file' => 'mimes:pdf|max:10000',
			'sent_appr_file' => 'mimes:pdf|max:10000',
			'rec_appr_file' => 'mimes:pdf|max:10000',
			'subm_sent_sub_file' => 'mimes:pdf|max:10000',
			'status' => 'required|exists:submittal_statuses,id',
            'version_appr_note' => 'max:1000',
            'version_subc_note' => 'max:1000',
		];
	}

}
