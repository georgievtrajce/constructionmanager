<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class EditRfiVersionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cycle_no' => 'required|numeric|max:10',
			'rec_sub' => 'date',
			'sent_appr' => 'date',
			'rec_appr' => 'date',
			'subm_sent_sub' => 'date',
			'status' => 'required|exists:rfi_statuses,id',
		];
	}

}
