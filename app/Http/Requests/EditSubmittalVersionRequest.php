<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class EditSubmittalVersionRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'cycle_no' => 'required|numeric|max:10',
			'rec_sub' => 'date',
			'sent_appr' => 'date',
			'rec_appr' => 'date',
			'rec_sub_file' => 'mimes:pdf|max:10000',
			'sent_appr_file' => 'mimes:pdf|max:10000',
			'rec_appr_file' => 'mimes:pdf|max:10000',
			'subm_sent_sub_file' => 'mimes:pdf|max:10000',
			'subm_sent_sub' => 'date',
			'status' => 'required|exists:submittal_statuses,id',
            'submitted_for' => 'max:255'
		];
	}

}
