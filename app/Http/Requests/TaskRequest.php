<?php
namespace App\Http\Requests;

class TaskRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('tasks') || Request::is('tasks/*')) {
            $rules = [
                'title' => 'required|max:255',
                'expiration-date' => 'required',
                'project' => 'required'
            ];
        }
        else {
            $rules = array();
        }

        return $rules;
    }

}
