<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class ManageCompanyUsersRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (!Auth::check())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'email' => 'required|email|unique:users,email',
			'title' => 'required|max:255',
			'office_phone' => 'required|max:45|regex:/^[0-9_~\-+!@#\$%\^&*\(\)\s]+$/',
			'cell_phone' => 'max:45|regex:/^[0-9_~\-+!@#\$%\^&*\(\)\s]+$/',
			'fax' => 'max:45|regex:/^[0-9_~\-+!@#\$%\^&*\(\)\s]+$/',
			'user_address_office' => 'required|exists:addresses,id',
		];
	}

}
