<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class AddRatingRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cc_price' => 'numeric|min:0|max:100',
			'cc_quality_of_work' => 'numeric|min:0|max:100',
			'quality_of_materials' => 'numeric|min:0|max:100',
			'expertise' => 'numeric|min:0|max:100',
			'problems_resolution' => 'numeric|min:0|max:100',
			'safety' => 'numeric|min:0|max:100',
			'cleanness' => 'numeric|min:0|max:100',
			'organized_and_professional' => 'numeric|min:0|max:100',
			'communication_skills' => 'numeric|min:0|max:100',
			'completed_on_time' => 'numeric|min:0|max:100',
		];
	}

}
