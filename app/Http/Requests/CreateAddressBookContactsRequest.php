<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class CreateAddressBookContactsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (!Auth::check())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'cc_name' => 'required|max:255',
			'cc_title' => 'required|max:255',
			'cc_address_office_name' => 'required',
			'cc_office_phone' => 'required', //|regex:/^[0-9_~\-+!@#\$%\^&*\(\)\s]+$/
			'cc_cell_phone' => 'required',
			'cc_email' => 'required|email',
		];
	}

}
