<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUserProfileRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		if (!Auth::check())
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'email' => 'required|email|unique:users,email,'.Auth::user()->id,
			'title' => 'required|max:255',
			'office_phone' => 'required|max:45',
			'cell_phone' => 'max:45',
			'fax' => 'max:45',
		];
	}

}
