<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class MaterialsRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (Request::is('projects/*/expediting-report') || Request::is('projects/*/expediting-report/*')) {
            $rules = ['mf_number'  => 'required|max:255',
                'mf_title'  => 'required|max:255',
                'name'  => 'required|max:255',
                'mas_sub_id'  => 'exists:address_book,id',
                'position'  => 'required',
                'er_status_id'  => 'exists:expediting_report_statuses,id',
                'status_id'  => 'required_if:subm_needed,1',
                'subm_needed'  => 'required_with:status_id',
                'need_by'  => 'required|date',
            ];
        }
        else  {
            $rules = [];
        }

        return $rules;
    }

}
