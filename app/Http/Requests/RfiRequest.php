<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class RfiRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|max:255',
			'subject' => 'max:255',
			'sent_via' => 'max:255',
			'is_change' => 'max:255',
			'number' => 'required|max:45',
			'cycle_no' => 'required|numeric|max:15',
			'master_format_number' => 'required|max:255',
			'master_format_title' => 'required|max:255',
			'sub_id' => 'exists:address_book,id',
			'sub_contact' => 'exists:ab_contacts,id',
			'rec_sub' => 'date',
			'sent_appr' => 'date',
			'rec_appr' => 'date',
			'subm_sent_sub' => 'date',
			//'status' => 'required|exists:rfi_statuses,id',
			'rec_sub_file' => 'mimes:pdf|max:10000',
			'sent_appr_file' => 'mimes:pdf|max:10000',
			'rec_appr_file' => 'mimes:pdf|max:10000',
			'subm_sent_sub_file' => 'mimes:pdf|max:10000',
		];
	}

}
