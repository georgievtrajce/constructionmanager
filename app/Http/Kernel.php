<?php namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

	/**
	 * The application's global HTTP middleware stack.
	 *
	 * @var array
	 */
	protected $middleware = [
		'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
		'Illuminate\Cookie\Middleware\EncryptCookies',
		'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
		'Illuminate\Session\Middleware\StartSession',
		'Illuminate\View\Middleware\ShareErrorsFromSession',
		'App\Http\Middleware\VerifyCsrfToken',
	];

	/**
	 * The application's route middleware.
	 *
	 * @var array
	 */
	protected $routeMiddleware = [
		'auth' => 'App\Http\Middleware\Authenticate',
		'auth.basic' => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
		'guest' => 'App\Http\Middleware\RedirectIfAuthenticated',
		'level_one_no_access' => 'App\Http\Middleware\LevelOneNoAccess',
		'company_admin' => 'App\Http\Middleware\CompanyAdmin',
		'only_super_admin' => 'App\Http\Middleware\OnlySuperAdmin',
		'only_company_admin' => 'App\Http\Middleware\OnlyCompanyAdmin',
		'project_creator' => 'App\Http\Middleware\ProjectCreator',
		'company' => 'App\Http\Middleware\CheckCompany',
		'only_contractor' => 'App\Http\Middleware\OnlyProjectContractor',
		'only_allowed_subcontractors' => 'App\Http\Middleware\OnlyAllowedSubcontractors',
		'module_read' => 'App\Http\Middleware\ModulePermissionsAuth',
		'module_write' => 'App\Http\Middleware\ModuleChangePermissions',
		'module_delete' => 'App\Http\Middleware\ModuleDeletePermissions',
		'sub_module_read' => 'App\Http\Middleware\SubModuleReadPermissions',
		'sub_module_write' => 'App\Http\Middleware\SubModuleWritePermissions',
		'sub_module_delete' => 'App\Http\Middleware\SubModuleDeletePermissions',
		'project_files_read' => 'App\Http\Middleware\ProjectFilesReadPermissions',
		'project_files_write' => 'App\Http\Middleware\ProjectFilesWritePermissions',
		'project_files_delete' => 'App\Http\Middleware\ProjectFilesDeletePermissions',
        'daily_reports_write' => 'App\Http\Middleware\DailyReportsWritePermissions',
        'daily_reports_delete' => 'App\Http\Middleware\DailyReportsDeletePermissions',
		'subscription_level_one' => 'App\Http\Middleware\SubscriptionLevelOne',
		'subscription_level_two' => 'App\Http\Middleware\SubscriptionLevelTwo',
		'super_admin_limitations' => 'App\Http\Middleware\SuperAdminForbidden',
		'blog_permissions' => 'App\Http\Middleware\BlogPermissions',
		'blog_write_delete_permissions' => 'App\Http\Middleware\BlogWriteDeletePermissions',
		'not_active' => 'App\Http\Middleware\NotActive',
		'active' => 'App\Http\Middleware\Active',
		'unshared_project' => 'App\Http\Middleware\UnsharedProject',
		'project_admin_restrictions' => 'App\Http\Middleware\ProjectAdminRestrictions',
		'level_zero_permit' => 'App\Http\Middleware\LevelZeroPermit',
        'company_admin_restrictions' => 'App\Http\Middleware\CompanyAdminRestrictions',
	];

}
