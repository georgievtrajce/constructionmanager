<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\Project;
use App\Models\Project_permission;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Config;

class SubModuleReadPermissions {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Auth::user()->hasRole('Company Admin')) {
            $displayName = !empty(Request::segment(3))?Request::segment(3):Request::input('module');
            $projectID = !empty(Request::segment(2))?Request::segment(2):Request::input('projectId');

			if (Auth::user()->hasRole('Project Admin') && (Project::where('id','=',$projectID)->pluck('proj_admin') == Auth::user()->id)) {
				return $next($request);
			}

            if (in_array($displayName, ['project-files', 'daily-report'])) {
                return $next($request);
            }


            if ($displayName) {
                $module = Module::where('display_name', '=', $displayName)->first();

                $permissions = Project_permission::where('user_id','=',Auth::user()->id)
                    ->where('proj_id', '=', $projectID)
                    ->where('entity_type', '=', Config::get('constants.entity_type.module'))
                    ->where('entity_id', '=', $module->id)
                    ->first();
                if(isset($permissions->read) &&  $permissions->read == 0) {
                    if ($request->ajax())
                    {
                        return response('Unauthorized.', 401);
                    }
                    else
                    {
                        return view('errors.unauthorized');
                    }
                }
            }
		}

		return $next($request);
	}

}
