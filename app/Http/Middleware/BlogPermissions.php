<?php namespace App\Http\Middleware;

use App\Models\Project;
use App\Models\Project_subcontractor;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Request;

class BlogPermissions {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$projectId = Request::segment(2);
		$project = Project::where('id','=',$projectId)->firstOrFail();

		if ($project->comp_id != $request->user()->comp_id) {
			$projectSubcontractors = Project_subcontractor::where('proj_id','=',$projectId)
				->where('comp_child_id','=',$request->user()->comp_id)->first();

				if (is_null($projectSubcontractors)) {
					return view('errors.unauthorized');
				}
		}

		return $next($request);
	}

}
