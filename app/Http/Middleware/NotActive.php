<?php namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class NotActive {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//check if user is super admin
		if ($request->user()->hasRole(Config::get('constants.roles.super_admin'))) {
			return $next($request);
		}

		//check if account is free but is not confirmed
		if ($request->user()->company->free_account == 1) {
			if ($request->user()->confirmed == 0) {
				return response()->view('errors.confirm_account_error');
			}
		}

		//check if user is not active
		if ($request->user()->active == 0) {
			return new RedirectResponse(url('pay-subscription'));
		}

		//check if subscription is expired
		if ($request->user()->company->subscription_expire_date <= Carbon::now()->toDateString()) {
			Flash::info(trans('labels.subscription_expire.info'));
			return new RedirectResponse(url('renew-subscription'));
		}

		return $next($request);
	}

}
