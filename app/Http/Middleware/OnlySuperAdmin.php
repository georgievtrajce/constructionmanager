<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class OnlySuperAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(Auth::user()->hasRole('Super Admin')) {

			//get super admin forbidden routes from config file into array
			$forbiddenRoutes = [
				Config::get('subscription_levels.address-book'),
				Config::get('subscription_levels.projects'),
				Config::get('subscription_levels.manage-users'),
				Config::get('subscription_levels.company-profile')
			];

			//return "no permissions" page if super admin tries to access some of the forbidden routes
			if(in_array(Request::segment(1), $forbiddenRoutes)) {
				return view('errors.unauthorized');
			}

			return $next($request);
		}

		return new RedirectResponse(url('/home'));
	}

}
