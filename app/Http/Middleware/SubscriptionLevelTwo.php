<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class SubscriptionLevelTwo {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(count(Auth::user()->company) && Auth::user()->company->subs_id == Config::get('subscription_levels.level-two')) {

			//return "no permissions" page if user tries to request the project modules
			$blog = Config::get('subscription_levels.projects.blog');
			$completedProjects = Config::get('subscription_levels.projects.completed');
			$secondUrlSegment = Request::segment(2);
			$thirdUrlSegment = Request::segment(3);

			//check for completed projects
			if ($secondUrlSegment == $completedProjects) {
				return view('errors.unauthorized');
			}

			//check for all other modules
			if ($thirdUrlSegment == $blog) {
				return view('errors.unauthorized');
			}
		}

		return $next($request);
	}

}
