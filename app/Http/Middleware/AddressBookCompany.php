<?php namespace App\Http\Middleware;

use App\Models\Address_book;
use App\Models\User;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use PhpSpec\Exception\Exception;

class AddressBookCompany {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(Request::segment(1) == 'address-book') {
			$id = Request::segment(3);
			$addressBookEntry = Address_book::where('id','=',$id)->first();
			if($addressBookEntry->owner_comp_id != Auth::user()->comp_id) {
				return new RedirectResponse(url('/address-book/create'));
			}
		}

		if(Request::segment(1) == 'company-profile') {
			$registeredCompany = User::where('id','=',Auth::user()->id)->first();
			if($registeredCompany->comp_id != Auth::user()->comp_id) {
				return new RedirectResponse(url('/company-profile/'.Auth::user()->comp_id));
			}
		}

		return $next($request);
	}

}
