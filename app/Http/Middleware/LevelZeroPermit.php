<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Config;

class LevelZeroPermit {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->user()->company->subs_id == Config::get('subscription_levels.level-zero') && $request->user()->company->subscription_type->amount == 0) {
			if ($request->ajax()) {
				return response('Unauthorized.', 401);
			} else {
				return view('errors.unauthorized');
			}
		}

		return $next($request);
	}

}
