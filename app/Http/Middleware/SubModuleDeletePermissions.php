<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\Project;
use App\Models\Project_permission;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Config;

class SubModuleDeletePermissions {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Auth::user()->hasRole('Company Admin')) {
			$displayName = Request::segment(3);
			$projectID = Request::segment(2);

			if (Auth::user()->hasRole('Project Admin') && (Project::where('id','=',$projectID)->pluck('proj_admin') == Auth::user()->id)) {
				return $next($request);
			}

			$module = Module::where('display_name','=',$displayName)->first();

			$permissions = Project_permission::where('user_id','=',Auth::user()->id)
				->where('entity_type','=', Config::get('constants.entity_type.module'))
				->where('entity_id', '=', $module->id)
				->where('proj_id', '=', $projectID)
				->first();
			if($permissions->delete == 0) {
				if ($request->ajax())
				{
					return response('Unauthorized.', 401);
				}
				else
				{
					return view('errors.unauthorized');
				}
			}
		}

		return $next($request);
	}

}
