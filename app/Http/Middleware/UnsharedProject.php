<?php namespace App\Http\Middleware;

use App\Models\Project;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class UnsharedProject {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$projectId = $request->segment(2);

		$project = Project::where('id','=',$projectId)->with('projectSubcontractors')->with('projectBiddersSelf')->firstOrFail();

		if ($project->comp_id == Auth::user()->comp_id) {
			return $next($request);
		}

        if (count($project->projectBiddersSelf) > 0) {
            return $next($request);
        }

		foreach ($project->projectSubcontractors as $subcontractor) {
			if ($subcontractor->comp_child_id == Auth::user()->comp_id && $subcontractor->status == Config::get('constants.projects.shared_status.unshared')) {
				if ($request->ajax()) {
					return response('Unauthorized.', 401);
				} else {
					return view('errors.unauthorized');
				}
			}
		}

		return $next($request);
	}

}
