<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class SubscriptionLevelOne {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(count(Auth::user()->company) && Auth::user()->company->subs_id == Config::get('subscription_levels.level-one')) {
			//return "no permissions" page if user tries to request the address book
			if(Request::segment(1) == Config::get('subscription_levels.address-book')) {
				return view('errors.unauthorized');
			}

			//return "no permissions" page if user tries to request the project modules
			$projects = Config::get('subscription_levels.projects');
			unset($projects['expediting-report']);

			//check for shared and completed projects
			if (in_array(Request::segment(2), $projects)) {
				return view('errors.unauthorized');
			}

			//check for all other modules
			if (in_array(Request::segment(3), $projects)) {
				return view('errors.unauthorized');
			}
		}

		return $next($request);
	}

}
