<?php namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Config;

class Active {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//check if user is super admin
		if ($request->user()->hasRole(Config::get('constants.roles.super_admin'))) {
			if ($request->ajax()) {
				return response('Unauthorized.', 401);
			} else {
				return view('errors.unauthorized');
			}
		}
		
		//check if the subscription level is free (in that case it should not has an expire date)
		if (is_null($request->user()->company->subscription_expire_date)) {
			//check if user is active
			if ($request->user()->active == 1) {
				if ($request->ajax()) {
					return response('Unauthorized.', 401);
				} else {
					return view('errors.unauthorized');
				}
			}
		}

		//check if subscription is expired
		if ($request->user()->company->subscription_expire_date > Carbon::now()->toDateString()) {
			if ($request->ajax()) {
				return response('Unauthorized.', 401);
			} else {
				return view('errors.unauthorized');
			}
		}

		return $next($request);
	}

}
