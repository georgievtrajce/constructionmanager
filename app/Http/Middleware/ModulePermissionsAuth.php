<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\Project;
use App\Models\User_permission;
use App\Models\Project_permission;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class ModulePermissionsAuth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$projectRoutes = array('', 'shared', 'completed', 'search');
		if (Auth::user()->hasRole(['Company Admin', 'Project Admin']) || (Request::segment(1) == 'projects') && (in_array(Request::segment(2), $projectRoutes))) {
			return $next($request);
		} else {
            $displayName = !empty(Request::segment(3))?Request::segment(3):Request::input('module');
            $projectID = !empty(Request::segment(2))?Request::segment(2):Request::input('projectId');

            if (in_array($displayName, ['project-files', 'daily-report'])) {
                return $next($request);
            }

			if (Input::get('project')) {
                $projectID = Input::get('project');
            }

            if ($displayName) {
                $module = Module::where('display_name', '=', $displayName)->first();
                if (($displayName == 'address-book')) {
                    $permissions = User_permission::where('user_id','=',Auth::user()->id)
                        ->where('entity_type', '=', 1)
                        ->where('entity_id', '=', $module->id)
                        ->first();
                } else {
                    $permissions = Project_permission::where('user_id', '=', Auth::user()->id)
                        ->where('proj_id', '=', $projectID)
                        ->where('entity_type', '=', 1)
                        ->where('entity_id', '=', $module->id)
                        ->first();
                }

                if(isset($permissions->read) && $permissions->read == 0) {
                    if ($request->ajax())
                    {
                        return response('Unauthorized.', 401);
                    }
                    else
                    {
                        return view('errors.unauthorized');
                    }
                }
            }

			return $next($request);
		}

	}


}
