<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\Project;
use App\Models\Project_permission;
use App\Models\User;
use App\Models\User_permission;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class ProjectAdminRestrictions {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//check if the logged in user has company admin role
		if (Auth::user()->hasRole('Company Admin')) {
			//check if the request is for manage user permissions
			if (Request::is('manage-users/*/permissions')) {
				//get the user id
				$userId = Request::segment(2);

				$user = User::where('id','=',$userId)->where('comp_id','=',Auth::user()->comp_id)->firstOrFail();

				//check if the user has project admin role assigned
				if ($user->hasRole('Project Admin')) {
					if ($request->ajax()) {
						return response('Unauthorized.', 401);
					} else {
						return view('errors.unauthorized');
					}
				}
			}
			//check if the request is for a specific project permissions
			else if (Request::is('projects/*/permissions/*')) {
				//get the user id
				$userId = Request::segment(4);

				//get the project id
				$projectId = Request::segment(2);

				//check if this user has the permission to manage the specified project
				if (Project::where('id','=',$projectId)->where('comp_id','=',Auth::user()->comp_id)->pluck('proj_admin') == $userId) {
					$user = User::where('id','=',$userId)->where('comp_id','=',Auth::user()->comp_id)->firstOrFail();

					//check if the user has project admin role assigned
					if ($user->hasRole('Project Admin')) {
						if ($request->ajax()) {
							return response('Unauthorized.', 401);
						} else {
							return view('errors.unauthorized');
						}
					}
				}
			}
		}

		//check if the logged in user has a project admin role
		if (Auth::user()->hasRole('Project Admin')) {
			 if (Request::is('projects/*')) {
				 //get the user id
				 $userId = Auth::user()->id;

				 //get the project id
				 $projectId = Request::segment(2);
				 //check if this user has the permission to manage the specified project
				 if (Project::where('id', '=', $projectId)->where('comp_id', '=', Auth::user()->comp_id)->pluck('proj_admin') == $userId) {
					 return $next($request);
				 } else {
					 $displayName = Request::segment(1);

					 //get the requested module
					 $module = Module::where('display_name', '=', $displayName)->first();

					 //if the accessed module is address book or custom categories
					 if (($displayName == 'address-book')) {
						 //get the user permissions
						 $permissions = User_permission::where('user_id','=',Auth::user()->id)
							 ->where('entity_type', '=', 1)
							 ->where('entity_id', '=', $module->id)
							 ->first();
					 }
					 else {
						 //get the user permissions
						 $permissions = Project_permission::where('user_id', '=', Auth::user()->id)
							 ->where('proj_id', '=', $projectId)
							 ->where('entity_type', '=', 1)
							 ->where('entity_id', '=', $module->id)
							 ->first();
					 }

					 //redirect to unauthorized page if the user has no read permissions
					 if($permissions->read == 0) {
						 if ($request->ajax())
						 {
							 return response('Unauthorized.', 401);
						 }
						 else
						 {
							 return view('errors.unauthorized');
						 }
					 }
				 }
			 }
		}

		return $next($request);
	}

}
