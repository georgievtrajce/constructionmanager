<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier {

	//add an array of Routes to skip CSRF check
	private $openRoutes = [
		'payment/response',
		'increase-upload-transfer',
		'check-download-allowance',
		'increase-download-transfer',
		'check-existing-transmittal',
		'generate-transmittal/recipient',
		'generate-transmittal/subcontractor',
		'send-transmittal-email-notification',
		'change-subcontractor-project-number',
		'get-subcontractor-project-number',
		'store-module-file',
		'store-proposal-file',
		'delete-file',
		'store-file',
		'change-project-admin',
		'list-companies/get-company-expire-date',
		'list-companies/save-company-expire-date',
		'subcontractors/address',
		'send-transmittal-email-notification',
		'send-project-file-email-notification',
		'get-project-file',
        'resize-image-tiny',
        'projects/*/shared/*/notifications/*',
        'send-daily-report-email-notification',
        'generate-daily-report',
        'edit-daily-report-row',
        'delete-daily-report-row',
        'projects/*/daily-report/*'
	];

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//add this condition
		foreach($this->openRoutes as $route) {

			if ($request->is($route)) {
				return $next($request);
			}
		}

		return parent::handle($request, $next);
	}

}
