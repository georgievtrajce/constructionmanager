<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\Project;
use App\Models\Project_permission;
use App\Models\User;
use App\Models\User_permission;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class CompanyAdminRestrictions {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//check if the logged in user has company admin role
		if (!Auth::user()->hasRole('Company Admin')) {
		    dd('tes');
			//check if the request is for manage user permissions
			if (Request::is('manage-users/*/permissions')) {
			    dd('test');
                return view('errors.unauthorized');
			}
		}

		return $next($request);
	}

}
