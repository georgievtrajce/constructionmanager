<?php namespace App\Http\Middleware;

use App\Models\Project;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class OnlyCompanyAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		//check if the logged in user is a company admin
		if(Auth::user()->hasRole('Company Admin')) {
			return $next($request);
		}

		//check if the logged in user is a project admin
		if(Auth::user()->hasRole('Project Admin')) {
			//get project id
			$projectId = Request::segment(2);

			//check if the logged in user is a project admin on the specified project
			if (Project::where('id','=',$projectId)->pluck('proj_admin') == Auth::user()->id) {
				return $next($request);
			}
		}

		//return unauthorized page
		if ($request->ajax())
		{
			return response('Unauthorized.', 401);
		}
		else
		{
			return view('errors.unauthorized');
		}
	}

}
