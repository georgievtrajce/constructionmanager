<?php namespace App\Http\Middleware;

use App\Models\Project;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Request;

class BlogWriteDeletePermissions {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$projectId = Request::segment(2);
		$project = Project::where('id','=',$projectId)->firstOrFail();

		if ($project->comp_id != $request->user()->comp_id) {
			return view('errors.unauthorized');
		}

		return $next($request);
	}

}
