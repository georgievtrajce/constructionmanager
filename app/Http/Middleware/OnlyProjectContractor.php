<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Project;

class OnlyProjectContractor {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = $request->segment(2);
        $project = Project::where('id', '=', $id)->first();

        $routes = array('', 'proposals', 'contracts', 'expediting-report', 'submittals');

        //dd($request->segment(3));
        if ($project->comp_id != Auth::user()->comp_id) {
           if(in_array($request->segment(3),$routes)) {
                return new RedirectResponse(url('/projects/'.$id));
           }
        }

        return $next($request);
    }

}
