<?php namespace App\Http\Middleware;

use App\Models\File_type;
use App\Models\Project;
use App\Models\Project_permission;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Config;

class DailyReportsWritePermissions {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Auth::user()->hasRole('Company Admin')) {
			$displayName = 'daily-reports';
			$projectID = Request::segment(2);

			if (Auth::user()->hasRole('Project Admin') && (Project::where('id','=',$projectID)->pluck('proj_admin') == Auth::user()->id)) {
				return $next($request);
			}

            $projectFileType = File_type::where('display_name', '=', $displayName)->first();

            $permissions = Project_permission::where('user_id', '=', Auth::user()->id)
                ->where('entity_type', '=', Config::get('constants.entity_type.file'))
                ->where('entity_id', '=', $projectFileType->id)
                ->where('proj_id', '=', $projectID)
                ->first();
            if($permissions->write == 0) {
                if ($request->ajax())
                {
                    return response('Unauthorized.', 401);
                }
                else
                {
                    return view('errors.unauthorized');
                }
            }
		}

		return $next($request);
	}

}
