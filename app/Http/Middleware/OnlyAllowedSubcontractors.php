<?php
namespace App\Http\Middleware;

use App\Models\Project_bidder;
use App\Models\Project_company_permission;
use App\Models\Project_subcontractor;
use App\Models\Proposal_supplier;
use App\Modules\Project_subcontractors\Repositories\ProjectSubcontractorsRepository;
use Closure;
use Illuminate\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Request;
use App\Models\Project;
use App\Models\Module;
use App\Models\File_type;

class OnlyAllowedSubcontractors
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $projectID = $request->segment(2);

        $projectPermission = Project_subcontractor::where('proj_id','=',$projectID)
            ->where('status','=',2)
            ->where('comp_child_id','=',Auth::user()->comp_id)
            ->orWhere('comp_parent_id', Auth::user()->comp_id)
            ->first();

        //check if logged in company is invited to bid on this project
        $bidder = Project_bidder::where('status','=',Config::get('constants.projects.shared_status.accepted'))
            ->where('proj_id','=',$projectID)
            ->where('invited_comp_id','=',Auth::user()->comp_id)
            ->orWhere('comp_parent_id', Auth::user()->comp_id)
            ->first();

        if ($projectPermission || $bidder) {
            //has permission
            return $next($request);
        }

        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            return view('errors.unauthorized');
        }
    }
}
