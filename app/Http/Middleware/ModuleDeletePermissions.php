<?php namespace App\Http\Middleware;

use App\Models\Module;
use App\Models\User_permission;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class ModuleDeletePermissions {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(!Auth::user()->hasRole('Company Admin')) {
			$displayName = Request::segment(1);

			$module = Module::where('display_name','=',$displayName)->first();

			$permissions = User_permission::where('user_id','=',Auth::user()->id)
				->where('entity_type','=',1)
				->where('entity_id','=',$module->id)
				->first();
			if($permissions->delete == 0) {
				if ($request->ajax())
				{
					return response('Unauthorized.', 401);
				}
				else
				{
					return view('errors.unauthorized');
				}
			}
		}

		return $next($request);
	}

}
