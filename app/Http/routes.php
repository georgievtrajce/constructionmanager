<?php


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('error-page/{code}/{errorMessage}', 'ErrorController@show');
Route::get('error-page-public/{code}/{errorMessageLarge}/{errorMessageSmall}', 'ErrorController@showErrorPagePublic');

Route::get('activate/{code}', 'Auth\AuthController@activateAccount');

//accept invitation through companies module
Route::get('invite/{token}/accept-invitation','ProjectSubcontractorsController@acceptProject');

//accept invitation through modules
Route::get('invite/{token}/accept','InvitationsController@globalAccept');

//reject invitation through modules
Route::get('invite/{token}/reject','InvitationsController@rejectInvitation');

Route::get('payment/hosted/{companyId}/{subscriptionId}/{email}/{companyName}', 'PaymentController@postToHostedPage');
Route::post('payment/response', 'PaymentController@getResponse');

Route::get('/', function () {
    return Redirect::to('https://cloud-pm.com');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/contact-us', 'WelcomeController@contactUs');
Route::get('/products-and-pricing', 'WelcomeController@productsAndPricing');
Route::get('/refund-policy', 'WelcomeController@refundPolicy');
Route::get('/privacy-policy', 'WelcomeController@privacyPolicy');
Route::get('/terms-of-use', 'WelcomeController@termsOfUse');
Route::get('/features', 'WelcomeController@features');

//public transmittal
Route::get('projects/{projectID}/get-transmittal-public', 'TransmittalsController@showTransmittal');
Route::get('projects/{projectID}/get-project-file-public', 'ProjectFilesController@showProjectFile');
Route::get('address-book/get-address-book-file-public', 'AddressBookController@showAddressBookFile');
Route::get('projects/{project_id}/daily-report/{id}/get-report-public', 'DailyReportsController@showReport');

Route::group(['middleware' => 'auth'], function() {

    Route::post('resize-image-tiny','ImageController@resizeImageTinyMce');

	//check if user is active (forbid these routes if the user is active - these routes are only for not active users active = 0)
	Route::group(['middleware' => 'active'], function() {
		Route::get('pay-subscription', 'PaymentController@paySubscription');
		Route::get('renew-subscription', 'PaymentController@reNewSubscription');
		Route::post('company-profile/{companyId}/pay-subscription-level/points', 'CompaniesController@changeSubscriptionLevelByPoints');
	});

	//check if user is not active (these routes are forbidden for non active users)
	Route::group(['middleware' => 'not_active'], function() {
		Route::get('home', 'HomeController@index');

		Route::get('home/all-uploaded-files', 'HomeController@latestUploadedFiles');
		Route::get('home/all-blog-posts', 'HomeController@latestBlogPosts');

		Route::any('get-subcontractor-project-number', 'SharedProjectsController@getProjectNumber');
		Route::any('change-subcontractor-project-number', 'SharedProjectsController@changeProjectNumber');

		//project files and contracts upload
		Route::any('store-file', 'ProjectFilesController@storeFile');

        Route::any('store-file-task', 'TasksController@storeFile');

        //address book files upload
        Route::any('store-address-book-file', 'ProjectFilesController@storeAddressBookFile');

		//bid files upload
		Route::any('store-proposal-file', 'ProjectFilesController@storeProposalFile');

		//download files - project files, bids, contracts, submittals
		Route::any('check-download-allowance', 'ProjectFilesController@downloadAllowance');

		//delete file
		Route::any('delete-file', 'ProjectFilesController@deleteFile');

		//increase upload and download transfer
		Route::any('increase-upload-transfer', 'DataTransferLimitationController@increaseUploadTransfer');
		Route::any('increase-download-transfer', 'DataTransferLimitationController@increaseDownloadTransfer');

		//change project admin
		Route::any('change-project-admin','ProjectsController@changeProjectAdmin');

		Route::group(['middleware' => 'super_admin_limitations'], function() {
			Route::group(['middleware' => 'module_read'], function() {
				//Address book
				Route::group(['middleware' => 'company'], function() {

					//Blog
					Route::resource('projects/{projectId}/blog', 'BlogController');

					//view address book company section routes
                    Route::get('address-book/import/undo-last-action', 'AddressBookController@undoLastAction');
                    Route::get('address-book/master-format/report', 'AddressBookController@reportMasterFormatFilter');
					Route::get('address-book/{id}/view', 'AddressBookController@view');
					Route::get('address-book/{id}/view/report', 'AddressBookController@viewReport');
                    Route::get('address-book/report','AddressBookController@report');

					Route::get('address-book/address/{id}/view/office-report', 'AddressBookController@officeReport');

					Route::get('address-book/{companyId}/contact/{contactId}/view', 'AddressBookController@viewContact');
					Route::get('address-book/{companyId}/contact/{contactId}/view/contact-report', 'AddressBookController@contactReport');

					//addresses
					Route::resource('{addressType}/{companyId}/addresses', 'AddressesController', ['except' => ['show']]);

					//contacts
					Route::resource('{contactType}/{companyId}/contacts', 'ContactsController', ['except' => ['show']]);

					Route::get('address-book/import/{type}', 'ImportController@index');
					Route::post('address-book/upload/{type}', 'ImportController@insert');

					Route::get('address-book/{id}/projects/search/{status?}','SubcontractorsProjectsController@search');
					Route::get('address-book/{id}/projects/{status?}', 'SubcontractorsProjectsController@index');

                    Route::get('address-book/{companyId}/ratings/{projectId?}', 'AddressBookController@showRating');
                    Route::post('address-book/{companyId}/ratings/{projectId}', 'AddressBookController@addRating');
                    Route::get('address-book/{abId}/files', 'AddressBookController@listDocuments');
                    Route::get('address-book/{abId}/files/create', 'AddressBookController@createFile');
                    Route::post('address-book/{abId}/files/create', 'AddressBookController@storeFile');
                    Route::get('address-book/{abId}/files/{documentId}/edit', 'AddressBookController@editFile');
                    Route::delete('address-book/{addressBookId}/files/{documentId}/delete', 'AddressBookController@deleteFile');

					Route::get('address-book/{category_type}/{category}', 'AddressBookController@categoryFilter');
                    Route::get('address-book/{category_type}/{category}/report', 'AddressBookController@reportCategoryFilter');
					Route::get('address-book/master-format', 'AddressBookController@masterFormatFilter');
                    Route::get('address-book/transmittals', 'AddressBookController@getProjectTransmittals');


                    Route::delete('address-book/{ids}', 'AddressBookController@destroy');
					Route::resource('address-book', 'AddressBookController');

				});

				Route::group(['middleware' => 'sub_module_read'], function() {

				    //Daily Reports
				    Route::post('projects/{project_id}/daily-report/{id}/edit-row', 'DailyReportsController@editTableRow');
				    Route::post('projects/{project_id}/daily-report/{id}/delete-row', 'DailyReportsController@deleteTableRow');
				    Route::any('send-daily-report-email-notification', 'DailyReportsController@sendEmail');
                    Route::any('projects/{project_id}/daily-report/copy-last-report', 'DailyReportsController@copyLastReport');
                    Route::any('projects/{project_id}/daily-report/upload-file', 'DailyReportsController@uploadDailyReportFile');
                    Route::any('projects/{project_id}/daily-report/store-file', 'DailyReportsController@storeDailyReportFile');
                    Route::any('projects/{project_id}/daily-report/{id}/edit-file', 'DailyReportsController@editDailyReportFile');
                    Route::any('projects/{project_id}/daily-report/{id}/update-file', 'DailyReportsController@updateDailyReportFile');
                    Route::get('projects/{project_id}/daily-report/{id}/get-report', 'DailyReportsController@showReport');
				    Route::any('generate-daily-report', 'DailyReportsController@generateReport');
				    Route::any('edit-daily-report-row', 'DailyReportsController@editReportRow');
				    Route::any('delete-daily-report-row', 'DailyReportsController@deleteReportRow');
				    Route::resource('projects/{project_id}/daily-report', 'DailyReportsController');

					//generate transmittals
					Route::any('check-existing-transmittal','TransmittalsController@checkExistingTransmittal');
					Route::any('generate-transmittal/recipient','TransmittalsController@generateRecipientTransmittal');
					Route::any('generate-transmittal/subcontractor','TransmittalsController@generateSubcontractorTransmittal');
					Route::any('send-transmittal-email-notification','TransmittalsController@sendEmail');
					Route::any('send-project-file-email-notification','ProjectFilesController@sendEmail');

					//submittals
					Route::any('store-module-file', 'ProjectFilesController@storeModuleFile');
					Route::any('delete-submittal-file', 'SubmittalsController@deleteSubmittalFile');

					Route::get('projects/{project_id}/submittals/filter','SubmittalsController@filter');
					Route::get('projects/{project_id}/submittals/transmittals','SubmittalsController@getTransmittals');
					Route::get('projects/{project_id}/submittals/transmittals/filter','SubmittalsController@filterTransmittals');
					Route::get('projects/{project_id}/submittals/transmittals/report','SubmittalsController@reportTransmittals');
					Route::post('projects/{project_id}/submittals/transmittals/{transmittal_id}/delete','SubmittalsController@deleteTransmittal');
					Route::get('projects/{project_id}/submittals/report','SubmittalsController@pdfReport');
					Route::get('projects/{project_id}/submittals/shares','SubmittalsController@shares');
					Route::post('projects/{project_id}/submittals/{submittalIds}/unshare','SubmittalsController@unshare');

					Route::resource('projects/{project_id}/submittals', 'SubmittalsController');

                    //merge pdf export
                    Route::get('projects/{project_id}/merge/report','ProjectsController@mergePdfReport');

                    //generate batch download
                    Route::get('projects/{project_id}/batch/download','ProjectsController@batchDownload');

					//submittals versions
					Route::resource('projects/{project_id}/submittals/{submittal_id}/version','SubmittalVersionsController');
                    Route::post('projects/{project_id}/submittals/{submittal_id}/version/{versionId}/notes','SubmittalVersionsController@updateTransmittalNotes');
					Route::get('projects/{project_id}/submittals/{submittal_id}/version/{version_id}/{file_name}','SubmittalVersionsController@download');

					//rfi's
					Route::get('projects/{project_id}/rfis/filter','RfisController@filter');
					Route::get('projects/{project_id}/rfis/transmittals','RfisController@getTransmittals');
					Route::get('projects/{project_id}/rfis/transmittals/filter','RfisController@filterTransmittals');
					Route::post('projects/{project_id}/rfis/transmittals/{transmittal_id}/delete','RfisController@deleteTransmittal');
					Route::get('projects/{project_id}/rfis/transmittals/report','RfisController@reportTransmittals');
					Route::get('projects/{project_id}/rfis/report','RfisController@pdfReport');
					Route::get('projects/{project_id}/rfis/shares','RfisController@shares');
					Route::post('projects/{project_id}/rfis/{rfiIds}/unshare','RfisController@unshare');

					Route::resource('projects/{project_id}/rfis', 'RfisController');

					//rfi's versions
					Route::resource('projects/{project_id}/rfis/{rfi_id}/version','RfiVersionsController');
                    Route::post('projects/{project_id}/rfis/{rfi_id}/version/{version_id}/notes','RfiVersionsController@updateTransmittalNotes');
					Route::get('projects/{project_id}/rfis/{rfi_id}/version/{version_id}/{file_name}','RfiVersionsController@download');

					//pco's
					Route::get('projects/{project_id}/pcos/filter','PcosController@filter');
					Route::get('projects/{project_id}/pcos/transmittals','PcosController@getTransmittals');
					Route::get('projects/{project_id}/pcos/transmittals/filter','PcosController@filterTransmittals');
					Route::post('projects/{project_id}/pcos/transmittals/{transmittal_id}/delete','PcosController@deleteTransmittal');
					Route::get('projects/{project_id}/pcos/transmittals/report','PcosController@reportTransmittals');
					Route::get('projects/{project_id}/pcos/report','PcosController@pdfReport');
					Route::get('projects/{project_id}/pcos/shares','PcosController@shares');
					Route::post('projects/{project_id}/pcos/{pcoIds}/unshare','PcosController@unshare');

					Route::resource('projects/{project_id}/pcos', 'PcosController');

					//pco's subcontractors
					Route::resource('projects/{project_id}/pcos/{pco_id}/subcontractors', 'PcoSubcontractorsController');
					//pco's recipient
					Route::resource('projects/{project_id}/pcos/{pco_id}/recipient', 'PcoRecipientController');

					//pco's versions'
					Route::resource('projects/{project_id}/pcos/{pco_id}/{version_type}/{pco_subcontractor_recipient_id}/version', 'PcoVersionsController');
                    Route::post('projects/{project_id}/pcos/{pco_id}/{version_type}/{pco_subcontractor_recipient_id}/version/{versionId}/notes', 'PcoVersionsController@updateTransmittalNotes');


                    //projects companies
					Route::any('subcontractors/address','ProjectCompaniesController@updateAddress');
					Route::post('projects/{id}/companies/{comp_id}/address-book-company/{ab_id}/invite','ProjectCompaniesController@invite');
					Route::get('projects/{id}/companies/{comp_id}/permissions','ProjectPermissionsController@companyPermissions');
					Route::post('projects/{id}/companies/{comp_id}/permissions/update','ProjectPermissionsController@updateCompanyPermissions');
					Route::post('projects/{id}/companies/{ab_id}/share','ProjectCompaniesController@share');
					Route::post('projects/{id}/companies/{ps_id}/unshare','ProjectCompaniesController@unshare');
					Route::get('projects/{id}/companies/{comp_id}/project-company/{proj_comp_id}/office/{office_id?}','ProjectCompaniesController@showOfficeContacts');
					Route::post('projects/{id}/companies/{comp_id}/project-company/{proj_comp_id}/office/{office_id}','ProjectCompaniesController@updateOfficeContacts');
					Route::resource('projects/{id}/companies','ProjectCompaniesController', ['except' => 'create']);

					//project subcontractors
					Route::get('subcontractor/project/{token}/accept','ProjectSubcontractorsController@acceptProject');
					Route::get('subcontractor/project/{token}/reject','ProjectSubcontractorsController@rejectProject');

					//project bids and files
					Route::resource('projects/{proj_id}/bids/{id}/bidders/{supp_id}/files', 'ProposalFilesController', ['except' => 'create']);
					Route::post('projects/{proj_id}/bids/{id}/bidders/invite', 'ProposalSuppliersController@invite');
					Route::get('projects/{proj_id}/bids/{id}/bidders/{bidder_id}/permissions', 'ProposalSuppliersController@getPermissions');
					Route::post('projects/{proj_id}/bids/{id}/bidders/{bidder_id}/permissions', 'ProposalSuppliersController@updatePermissions');
					Route::resource('projects/{proj_id}/bids/{id}/bidders', 'ProposalSuppliersController');
					Route::get('projects/{project_id}/bids/report','ProposalsController@report');
                    Route::get('projects/{projectId}/bids/{bidId}/report','ProposalsController@reportBidders');
					Route::get('projects/{project_id}/bids/filter','ProposalsController@filter');
					Route::resource('projects/{proj_id}/bids', 'ProposalsController');

					//project materials and services
					Route::get('projects/{proj_id}/expediting-report/report', 'MaterialsController@report');
					Route::get('projects/{proj_id}/expediting-report/filter', 'MaterialsController@filter');
					Route::resource('projects/{proj_id}/expediting-report', 'MaterialsController');
				});

				Route::group(['middleware' => 'project_files_read'], function() {
					//project files
					Route::get('projects/{project_id}/project-files/{type}/file/shares','ProjectFilesController@uploadedFileShares');
					//received project files
					//Route::get('projects/{project_id}/project-files/{type}/file/received','ProjectFilesController@received');
					//Route::get('projects/{project_id}/project-files/{type}/file/received/shares','ProjectFilesController@receivedFileShares');
					Route::resource('projects/{project_id}/project-files/{type}/file','ProjectFilesController');

					Route::any('projects/{project_id}/project-files/{type}/file/upload','ProjectFilesController@upload');
					Route::get('projects/{project_id}/project-files/{type}/file/download/{file_name}','ProjectFilesController@download');
					Route::post('projects/{project_id}/file/{file_id}/share-company/{share_comp_id}/receive-company/{receive_comp_id}/unshare','ProjectFilesController@unshare');
				});

				//completed and shared projects listing
				Route::get('projects/completed','ProjectsController@completed');
				Route::get('projects/shared','ProjectsController@shared');

				//projects search
				Route::get('projects/search/created','ProjectsController@search');
				Route::get('projects/search/completed','ProjectsController@search');
				Route::get('projects/search/shared','ProjectsController@search');

                //unshare project
                Route::delete('projects/{id}/unshare/{projectSubcontractorId}', 'ProjectsController@destroyShared');

				//project info for subcontractors
				Route::get('projects/{proj_id}/subcontractor/project-info/edit','ProjectsController@subcontractorProjectInfoEdit');
				Route::post('projects/{proj_id}/subcontractor/project-info/store','ProjectsController@subcontractorProjectInfoStore');
				Route::post('projects/{proj_id}/subcontractor/project-info/update','ProjectsController@subcontractorProjectInfoUpdate');

				//all uploaded files per project and all blog posts per project
				Route::get('projects/{proj_id}/all-uploaded-files', 'ProjectsController@allUploadedFiles');
				Route::get('projects/{proj_id}/all-blog-posts', 'ProjectsController@allBlogPosts');

				//projects
				Route::resource('projects', 'ProjectsController');

				//project contracts and files
				Route::get('projects/{proj_id}/contracts/filter', 'ContractsController@filter');
				Route::resource('projects/{proj_id}/contracts/{id}/files', 'ContractFilesController', ['except'	=> 'create']);
				Route::resource('projects/{proj_id}/contracts/{contr_id}/items', 'ContractItemsController', ['except'	=> 'show']);
				Route::resource('projects/{proj_id}/contracts', 'ContractsController', ['except' => 'show']);

				//Custom categories
				Route::resource('custom-categories', 'CustomCategoriesController');

				Route::resource('projects/{proj_id}/permissions', 'ProjectPermissionsController');
			});

			//blog
			Route::get('projects/{projectId}/type/{fileType}/file/{fileName}', 'BlogController@getFile');

			//Company profile editing
			Route::group(['middleware' => 'company'], function() {
				Route::group(['middleware' => 'only_company_admin'], function() {
					Route::get('company-profile/{companyId}', 'CompaniesController@index');
					Route::post('company-profile/{companyId}', 'CompaniesController@update');
					Route::get('company-profile/{companyId}/add-new-address', 'CompaniesController@addAddress');
					Route::get('company-profile/{companyId}/change-subscription-level', 'CompaniesController@getChangeSubscriptionLevel');
					Route::post('company-profile/{companyId}/change-subscription-level', 'CompaniesController@postChangeSubscriptionLevel');
					Route::get('company-profile/{companyId}/change-subscription-level/credit-card/{subscriptionId}/{email}/{companyName}', 'PaymentController@calculateAndPostToHostedPage');
					Route::post('company-profile/{companyId}/change-subscription-level/points', 'CompaniesController@changeSubscriptionLevelByPoints');
				});
			});

			//User profile editing
			Route::get('/user-profile', 'UsersController@index');
			Route::post('/user-profile', 'UsersController@update');
			Route::get('/change-password', 'UsersController@changePassword');
			Route::post('/change-password', 'UsersController@postPassword');

			//sharing project files
			Route::any('share/all/submittals','ProjectFilesController@shareAllSubmittals');
			Route::any('share/selected/submittals','ProjectFilesController@shareSelectedSubmittals');

			//sharing project rfis
			Route::any('share/all/rfis','ProjectFilesController@shareAllRfis');
			Route::any('share/selected/rfis','ProjectFilesController@shareSelectedRfis');

			//sharing project pcos
			Route::any('share/all/pcos','ProjectFilesController@shareAllPcos');
			Route::any('share/selected/pcos','ProjectFilesController@shareSelectedPcos');

			Route::any('share/all','ProjectFilesController@shareAll');
			Route::any('share/selected','ProjectFilesController@shareSelected');

			//Company users management
			Route::get('manage-users/{user_id}/permissions', 'ManageUsersController@getPermissions');
			Route::post('manage-users/{user_id}/permissions', 'ManageUsersController@postPermissions');
			Route::resource('manage-users', 'ManageUsersController');

            //tasks
            Route::get('projects/{projectId}/tasks', 'TasksController@projectTasks');
            Route::get('my-tasks', 'TasksController@myTasks');
            Route::get('tasks/{id}/notifications','TasksController@send');
            Route::get('tasks/report/single/{id}','TasksController@reportSingle');
            Route::get('tasks/report/{type}','TasksController@report');
            Route::resource('tasks', 'TasksController');
            Route::get('ajax/module-items/{projectId}/{typeId}', 'AjaxController@getAllModuleItems');

			//validate uac
			Route::any('validate-uac', 'AddressBookController@validateUac');

		});

		Route::group(['middleware' => 'only_super_admin'], function() {
			Route::get('subscription-levels', 'SubscriptionLevelsController@index');
			Route::get('subscription-levels/{subscriptionLevelId}/edit', 'SubscriptionLevelsController@edit');
			Route::post('subscription-levels/{subscriptionLevelId}/update', 'SubscriptionLevelsController@update');
			Route::get('transactions', 'TransactionsController@index');
			Route::get('logs', 'ListCompaniesUsers@logs');

			//Super Admin companies and users
			Route::group(['prefix' => 'list-companies'], function() {
				Route::any('save-company-expire-date', 'ListCompaniesUsers@saveExpireDate');
                Route::any('save-company-subscription-type', 'ListCompaniesUsers@saveSubscriptionType');
				Route::any('get-company-expire-date', 'ListCompaniesUsers@getExpireDate');
				Route::post('/{companyId}/reset-points', 'ListCompaniesUsers@resetPoints');
				Route::post('/create-free-account', 'ListCompaniesUsers@store');
				Route::get('/filter', 'ListCompaniesUsers@filter');
				Route::get('/create-free-account', 'ListCompaniesUsers@create');
				Route::get('/{companyId}/view-addresses', 'ListCompaniesUsers@viewAddresses');
				Route::get('/{companyId}/{userId}', 'ListCompaniesUsers@viewUsers');
				Route::get('/{companyId}', 'ListCompaniesUsers@viewCompany');
				Route::get('/', 'ListCompaniesUsers@index');
                Route::delete('/delete/{id}', 'ListCompaniesUsers@destroy');
			});

			//Master format
			Route::get('masterformat/upload', 'MasterFormatController@upload');
			Route::post('masterformat/import', 'MasterFormatController@import');
			Route::resource('masterformat', 'MasterFormatController', ['except' => 'show']);

			Route::get('masterformattype/create', 'MasterFormatController@createMasterFormatType');
			Route::post('masterformattype/create', 'MasterFormatController@storeMasterFormatType');
            Route::get('masterformattype/edit/{id}', 'MasterFormatController@editMasterFormatType');
            Route::post('masterformattype/edit/{id}', 'MasterFormatController@updateMasterFormatType');
            Route::delete('masterformattype/delete/{ids}', 'MasterFormatController@destroyMasterFormatType');

		});

		//Autocomplete routes
		Route::any('autocomplete/{request}', 'AutoCompleteController@implementAutoComplete');

		//get office contacts (autocomplete)
		Route::any('get-office-contacts', 'AutoCompleteController@getOfficeContacts');

		Route::group(['middleware' => 'only_allowed_subcontractors'], function() {
			//download transmittal (notified via email)
			//download transmittal (notified via email)
			Route::get('projects/{projectID}/get-transmittal', 'TransmittalsController@showTransmittal');
			Route::get('projects/{projectID}/get-project-file', 'ProjectFilesController@showProjectFile');
            Route::get('projects/{projectID}/download-batch-file/{companyId}', 'ProjectFilesController@showBatchFile');

			Route::get('projects/{projectID}/shared/project-files/{type}/file/shares', 'SharedProjectsController@projectFileShares');
			Route::get('projects/{projectID}/shared/project-files/{type}/file', 'SharedProjectsController@projectFiles');
			Route::get('projects/{projectID}/shared/expediting-report/filter', 'SharedProjectsController@filterMaterials');
			Route::get('projects/{projectID}/shared/expediting-report/report', 'SharedProjectsController@reportMaterials');

			//received files
			Route::get('projects/{projectID}/received/project-files/{type}/file/shares', 'SharedProjectsController@receivedFileShares');
			Route::get('projects/{projectID}/received/project-files/{type}/file', 'SharedProjectsController@receivedFiles');

			//shared submittals
			Route::get('projects/{projectID}/shared/submittals/shares', 'SharedProjectsController@submittalShares');
			Route::get('projects/{projectID}/shared/submittals/filter', 'SharedProjectsController@filterSubmittals');
			Route::get('projects/{projectID}/shared/submittals/savePdf', 'SharedProjectsController@savePdfSubmittals');
			Route::get('projects/{projectID}/shared/submittals/{id}', 'SharedProjectsController@submittalVersions');
			Route::get('projects/{projectId}/shared/submittals/{submittalId}/version/{versionId}', 'SharedProjectsController@viewSubmittalVersion');

			//shared rfis
			Route::get('projects/{projectID}/shared/rfis/shares', 'SharedProjectsController@rfiShares');
			Route::get('projects/{projectID}/shared/rfis/filter', 'SharedProjectsController@filterRfis');
			Route::get('projects/{projectID}/shared/rfis/report', 'SharedProjectsController@savePdfRfis');
			Route::get('projects/{projectID}/shared/rfis/{id}', 'SharedProjectsController@rfiVersions');
			Route::get('projects/{projectId}/shared/rfis/{rfiId}/version/{versionId}', 'SharedProjectsController@viewRfiVersion');

			//shared pcos
			Route::get('projects/{projectID}/shared/pcos/shares', 'SharedProjectsController@pcoShares');
			Route::get('projects/{projectID}/shared/pcos/filter', 'SharedProjectsController@filterPcos');
			Route::get('projects/{projectID}/shared/pcos/report', 'SharedProjectsController@savePdfPcos');
			Route::get('projects/{projectID}/shared/pcos/{id}', 'SharedProjectsController@pcoDetails');
			Route::get('projects/{projectID}/shared/pcos/{pcosID}/subcontractors/{subcontractorID}', 'SharedProjectsController@pcoSubcontractorVersions');
			Route::get('projects/{projectID}/shared/pcos/{pcosID}/recipient/{subcontractorID}', 'SharedProjectsController@pcoRecipientVersions');
			Route::get('projects/{projectId}/shared/pcos/{pcosId}/{versionType}/{pcoSubcontractorRecipientId}/version/{versionId}', 'SharedProjectsController@viewPcoVersion');

			Route::get('projects/{projectID}/shared/contracts/{id}/files', 'SharedProjectsController@contractFiles');



			Route::get('projects/{projectID}/shared/{module}', 'SharedProjectsController@index');

			//bids
            Route::get('projects/{projectID}/shared/bids/filter', 'SharedProjectsController@filterBids');
            Route::get('projects/{projectID}/shared/bids/{id}', 'SharedProjectsController@showBid');
            //Route::get('projects/{projectID}/shared/bids/{id}/bidders/{supplierId}', 'SharedProjectsController@showBidder');
            Route::put('projects/{projectID}/shared/bids/{id}/bidders/{supplierId}', 'SharedProjectsController@updateBidder');
            Route::post('projects/{projectID}/shared/bids/{id}/bidders/{supplierId}/notifications/{proposalNumber}','SharedProjectsController@sendBidProposal');

            //shared submittals/rfis/pcos notifications
            Route::post('projects/{projectID}/shared/{module}/{id}/version/{versionId}/notifications/{type}','SharedProjectsController@sendProposalNoifications');
            Route::post('projects/{projectID}/shared/{module}/{id}/subcontractors/{subcontractorId}/version/{versionId}/notifications/{type}','SharedProjectsController@sendProposalNoificationsPco');
        });
		Route::get('referral/invite', 'ReferralController@invite');
		Route::post('referral/send', 'ReferralController@send');
		Route::get('referral/accept/{token}', 'ReferralController@accept');
	});
});

