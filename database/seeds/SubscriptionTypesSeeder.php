<?php
use App\Models\Subscription_type;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class SubscriptionTypesSeeder extends Seeder {

    public function run() {

        $levelOne = Subscription_type::where('name','=','Level 1')->first();
        if (is_null($levelOne)) {
            Subscription_type::create([
                'name' => 'Level 1',
                'ref_points' => Config::get('subscription_levels.level1.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.level1.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.level1.upload_limit'),
                'download_limit' => Config::get('subscription_levels.level1.download_limit'),
            ]);
        }

        $levelTwo = Subscription_type::where('name','=','Level 2')->first();
        if (is_null($levelTwo)) {
            Subscription_type::create([
                'name' => 'Level 2',
                'ref_points' => Config::get('subscription_levels.level2.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.level2.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.level2.upload_limit'),
                'download_limit' => Config::get('subscription_levels.level2.download_limit'),
            ]);
        }

        $levelThree = Subscription_type::where('name','=','Level 3')->first();
        if (is_null($levelThree)) {
            Subscription_type::create([
                'name' => 'Level 3',
                'ref_points' => Config::get('subscription_levels.level3.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.level3.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.level3.upload_limit'),
                'download_limit' => Config::get('subscription_levels.level3.download_limit'),
            ]);
        }

        $levelSalesAgent = Subscription_type::where('name','=','Sales Agent')->first();
        if (is_null($levelSalesAgent)) {
            Subscription_type::create([
                'name' => 'Sales Agent',
                'ref_points' => Config::get('subscription_levels.sales_agent.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.sales_agent.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.sales_agent.upload_limit'),
                'download_limit' => Config::get('subscription_levels.sales_agent.download_limit'),
            ]);
        }

        $levelZero = Subscription_type::where('name','=','Level 0')->first();
        if (is_null($levelZero)) {
            Subscription_type::create([
                'name' => 'Level 0',
                'ref_points' => Config::get('subscription_levels.level0.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.level0.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.level0.upload_limit'),
                'download_limit' => Config::get('subscription_levels.level0.download_limit'),
            ]);
        }

        $levelZero = Subscription_type::where('name','=','Test Account')->first();
        if (is_null($levelZero)) {
            Subscription_type::create([
                'name' => 'Test Account',
                'ref_points' => Config::get('subscription_levels.test_account.ref_points'),
                'ref_points_needed' => Config::get('subscription_levels.test_account.ref_points_needed'),
                'upload_limit' => Config::get('subscription_levels.test_account.upload_limit'),
                'download_limit' => Config::get('subscription_levels.test_account.download_limit'),
            ]);
        }
    }
}