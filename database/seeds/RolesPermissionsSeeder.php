<?php
use App\Models\AssignedRoles;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;
use App\Models\User as User;

class RolesPermissionsSeeder extends Seeder {

    public function run() {
        //delete pivot tables connections
        DB::table('role_user')->delete();
        DB::table('permission_role')->delete();

        //create super admin role
        $superadmin_role = Role::firstOrCreate([
            'name' => 'Super Admin',
        ]);

        //create company admin role
        $company_admin_role = Role::firstOrCreate([
            'name' => 'Company Admin',
        ]);

        //create company admin role
        Role::firstOrCreate([
            'name' => 'Company User',
        ]);

        //create project admin role
        Role::firstOrCreate([
            'name' => 'Project Admin',
        ]);

        //assign super admin role to the super admin user
        $super_admin = User::where('title', '=', 'Super Admin')->first();
        $admin_assigned_role = new AssignedRoles;
        $admin_assigned_role->user_id = $super_admin->id;
        $admin_assigned_role->role_id = $superadmin_role->id;
        $admin_assigned_role->save();

        //assign company admin role to the Test Company company admin user
        $company_admin = User::where('title', '=', 'Company Admin')->first();
        $admin_assigned_role2 = new AssignedRoles;
        $admin_assigned_role2->user_id = $company_admin->id;
        $admin_assigned_role2->role_id = $company_admin_role->id;
        $admin_assigned_role2->save();

        //assign company admin role to the SubLevel3 company admin user
        $company_admin2 = User::where('title', '=', 'General Manager')->first();
        $admin_assigned_role3 = new AssignedRoles;
        $admin_assigned_role3->user_id = $company_admin2->id;
        $admin_assigned_role3->role_id = $company_admin_role->id;
        $admin_assigned_role3->save();

        //create permissions
        $manageAllCompanyUsers = Permission::firstOrCreate([
            'name' => 'manage_all_company_users',
            'display_name' => 'Manage All Company Users',
        ]);

        $manageMasterFormat = Permission::firstOrCreate([
            'name' => 'manage_master_format',
            'display_name' => 'Manage Master Format List',
        ]);

        //assign permissions to the roles
        $superadmin_role->perms()->sync(array($manageAllCompanyUsers->id, $manageMasterFormat->id));

        $company_admin_role->perms()->sync(array($manageAllCompanyUsers->id));
    }

}
