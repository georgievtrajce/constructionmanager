<?php
use App\Models\Expediting_report_status;
use Illuminate\Database\Seeder;

class ExpeditingReportStatusSeeder extends Seeder {

    public function run() {

        Expediting_report_status::firstOrCreate(['short_name' => 'Not Procured', 'name'=>'Not Procured']);
        Expediting_report_status::firstOrCreate(['short_name' => 'Procured', 'name'=>'Procured']);
        Expediting_report_status::firstOrCreate(['short_name' => 'Completed', 'name'=>'Completed']);
    }

}