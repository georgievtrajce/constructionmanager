<?php
use App\Models\Rfi_status;
use Illuminate\Database\Seeder;

class RfiStatusesSeeder extends Seeder {

    public function run() {

//        Rfi_status::firstOrCreate(['short_name' => 'APP', 'name'=>'Approved']);
//        Rfi_status::firstOrCreate(['short_name' => 'AAN', 'name'=>'Approved As Noted']);
//        Rfi_status::firstOrCreate(['short_name' => 'AAN/R', 'name'=>'Approved As Noted / Resubmit']);
//        Rfi_status::firstOrCreate(['short_name' => 'R&R', 'name'=>'Revise & Resubmit']);
//        Rfi_status::firstOrCreate(['short_name' => 'REJ/R', 'name'=>'Rejected / Resubmit']);
        Rfi_status::firstOrCreate(['short_name' => 'Open', 'name'=>'Open']);
        Rfi_status::firstOrCreate(['short_name' => 'Closed', 'name'=>'Closed']);
        Rfi_status::firstOrCreate(['short_name' => 'Draft', 'name'=>'Draft']);
    }

}