<?php

use App\Models\Address_book;
use App\Models\Company;
use App\Models\File_type;
use App\Models\Master_Format;
use App\Models\Pco_status;
use App\Models\Pco_version_file;
use App\Models\Rfi_status;
use App\Models\Rfi_version_file;
use App\Models\Submittal_status;
use App\Models\Submittal_version_file;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Laracasts\TestDummy\Factory;

class ProjectsSeeder extends Seeder {

    /** @test **/
    public function run() {

        //projects for SubLevelThree
        for ($i = 0; $i < 12; $i++) {
            //get random master format entry
            $masterFormat = Master_Format::all()->random(1);

            $addressBook = Address_book::all()->random(1);

            $company = Company::where('name','=','SubLevel3')->firstOrFail();
            $user = User::where('title','=','General Manager')->firstOrFail();

            $project = Factory::create('Project', [
                'owner_id' => $addressBook->id,
                'architect_id' => $addressBook->id,
                'comp_id' => $company->id,
            ]);

            DB::statement("UPDATE projects SET upc_code = concat('UPC-',LPAD(id, 6, '0'))");

            //create submittals
            $submittals = Factory::times(15)->create('Submittal',[
                'mf_number' => $masterFormat->number,
                'mf_title' => $masterFormat->title,
                'sub_id' => $addressBook->id,
                'recipient_id' => $addressBook->id,
                'gc_id' => $addressBook->id,
                'proj_id' => $project->id,
                'comp_id' => $company->id,
                'user_id' => $user->id,
            ]);

            foreach ($submittals as $submittal) {
                //get random submittal status
                $submittalStatus = Submittal_status::all()->random(1);

                //create submittal versions
                $submittalVersion = Factory::times(3)->create('Submittal_version',[
                    'submittal_id' => $submittal->id,
                    'status_id' => $submittalStatus->id,
                ]);

                $submittal->last_version = $submittalVersion->last()->id;
                $submittal->save();

                foreach ($submittalVersion as $version) {
                    //get file type
                    $fileType = File_type::where('display_name','=','submittals')->firstOrFail();

                    //add files in database
                    $file1 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'sent_appr_file',
                    ]);

                    Submittal_version_file::create([
                        'submittal_version_id' => $version->id,
                        'file_id' => $file1->id
                    ]);

                    $file2 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'subm_sent_sub_file',
                    ]);

                    Submittal_version_file::create([
                        'submittal_version_id' => $version->id,
                        'file_id' => $file2->id
                    ]);
                }
            }

            //create rfis
            $rfis = Factory::times(15)->create('Rfi',[
                'mf_number' => $masterFormat->number,
                'mf_title' => $masterFormat->title,
                'sub_id' => $addressBook->id,
                'recipient_id' => $addressBook->id,
                'gc_id' => $addressBook->id,
                'proj_id' => $project->id,
                'comp_id' => $company->id,
                'user_id' => $user->id,
            ]);

            foreach ($rfis as $rfi) {
                //get random rfi status
                $rfiStatus = Rfi_status::all()->random(1);

                //create rfi versions
                $rfiVersion = Factory::times(3)->create('Rfi_version',[
                    'rfi_id' => $rfi->id,
                    'status_id' => $rfiStatus->id,
                ]);

                $rfi->last_version = $rfiVersion->last()->id;
                $rfi->save();

                foreach ($rfiVersion as $version) {
                    //get file type
                    $fileType = File_type::where('display_name','=','rfis')->firstOrFail();

                    //add files in database
                    $file1 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'sent_appr_file',
                    ]);

                    Rfi_version_file::create([
                        'rfi_version_id' => $version->id,
                        'file_id' => $file1->id
                    ]);

                    $file2 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'subm_sent_sub_file',
                    ]);

                    Rfi_version_file::create([
                        'rfi_version_id' => $version->id,
                        'file_id' => $file2->id
                    ]);
                }
            }

            //create pcos
            $pcos = Factory::times(15)->create('Pco',[
                'comp_id' => $company->id,
                'user_id' => $user->id,
                'gc_id' => $addressBook->id,
                'proj_id' => $project->id,
            ]);

            foreach ($pcos as $pco) {
                //create subcontractors
                $pcoSubcontractors = Factory::times(3)->create('Pco_subcontractor_recipient',[
                    'pco_id' => $pco->id,
                    'ab_subcontractor_id' => $addressBook->id,
                    'ab_recipient_id' => 0,
                    'mf_number' => $masterFormat->number,
                    'mf_title' => $masterFormat->title,
                ]);

                //create subcontractor versions
                foreach ($pcoSubcontractors as $pcoSubcontractor) {
                    //get random pco status
                    $pcoStatus = Pco_status::all()->random(1);

                    $subcontractorVersion = Factory::times(3)->create('Pco_version',[
                        'subcontractor_id' => $pcoSubcontractor->id,
                        'recipient_id' => 0,
                        'status_id' => $pcoStatus->id,
                    ]);

                    $pcoSubcontractor->last_recipient_version = $subcontractorVersion->last()->id;
                    $pcoSubcontractor->save();

                    foreach ($subcontractorVersion as $version) {
                        //get file type
                        $fileType = File_type::where('display_name','=','pcos')->firstOrFail();

                        //add files in database
                        $file1 = Factory::create('Project_file', [
                            'ft_id' => $fileType->id,
                            'comp_id' => $company->id,
                            'user_id' => $user->id,
                            'proj_id' => $project->id,
                            'version_date_connection' => 'sent_appr_file',
                        ]);

                        Pco_version_file::create([
                            'pco_version_id' => $version->id,
                            'file_id' => $file1->id
                        ]);

                        $file2 = Factory::create('Project_file', [
                            'ft_id' => $fileType->id,
                            'comp_id' => $company->id,
                            'user_id' => $user->id,
                            'proj_id' => $project->id,
                            'version_date_connection' => 'subm_sent_sub_file',
                        ]);

                        Pco_version_file::create([
                            'pco_version_id' => $version->id,
                            'file_id' => $file2->id
                        ]);
                    }
                }

                //create recipient
                $pcoRecipient = Factory::create('Pco_subcontractor_recipient',[
                    'pco_id' => $pco->id,
                    'ab_subcontractor_id' => 0,
                    'ab_recipient_id' => $addressBook->id,
                    'mf_number' => $masterFormat->number,
                    'mf_title' => $masterFormat->title,
                ]);

                //get random pco status
                $pcoStatus = Pco_status::all()->random(1);

                $recipientVersion = Factory::times(3)->create('Pco_version',[
                    'subcontractor_id' => 0,
                    'recipient_id' => $pcoRecipient->id,
                    'status_id' => $pcoStatus->id,
                ]);

                $pcoRecipient->last_recipient_version = $recipientVersion->last()->id;
                $pcoRecipient->save();

                foreach ($recipientVersion as $version) {
                    //get file type
                    $fileType = File_type::where('display_name','=','pcos')->firstOrFail();

                    //add files in database
                    $file1 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'sent_appr_file',
                    ]);

                    Pco_version_file::create([
                        'pco_version_id' => $version->id,
                        'file_id' => $file1->id
                    ]);

                    $file2 = Factory::create('Project_file', [
                        'ft_id' => $fileType->id,
                        'comp_id' => $company->id,
                        'user_id' => $user->id,
                        'proj_id' => $project->id,
                        'version_date_connection' => 'subm_sent_sub_file',
                    ]);

                    Pco_version_file::create([
                        'pco_version_id' => $version->id,
                        'file_id' => $file2->id
                    ]);
                }

                $pco->last_subcontractor = $pcoSubcontractors->last()->id;
                $pco->save();
            }

            //get all file types
            $fileTypes = File_type::all();

            //create project files
            foreach ($fileTypes as $fileType) {
                $projectFiles = Factory::times(20)->create('Project_file',[
                    'ft_id' => $fileType->id,
                    'comp_id' => $company->id,
                    'user_id' => $user->id,
                    'proj_id' => $project->id,
                    'name' => 'Test '.$fileType->display_name,
                    'number' => '163859395',
                ]);
            }

        }



//        //address book for SubLevel3 (subscription level 3)
//        for ($i = 0; $i < 50; $i++) {
//
//            $addressBook = Factory::create('Address_book', ['owner_comp_id' => 2]);
//
//            Factory::times(3)->create('Ab_contact',['ab_id' => $addressBook->id]);
//            Factory::times(3)->create('Address',['ab_id' => $addressBook->id]);
//            Factory::times(3)->create('Address_book_categories', ['ab_id' => $addressBook->id]);
//            Factory::times(3)->create('Mf_address_book', ['ab_id' => $addressBook->id]);
//        }
    }
}
