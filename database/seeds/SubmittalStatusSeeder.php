<?php
use App\Models\Submittal_status;
use Illuminate\Database\Seeder;

class SubmittalStatusSeeder extends Seeder {

    public function run() {

        Submittal_status::firstOrCreate(['short_name' => 'APP', 'name'=>'Approved']);
        Submittal_status::firstOrCreate(['short_name' => 'AAN', 'name'=>'Approved As Noted']);
        Submittal_status::firstOrCreate(['short_name' => 'AAN/R', 'name'=>'Approved As Noted / Resubmit']);
        Submittal_status::firstOrCreate(['short_name' => 'R&R', 'name'=>'Revise & Resubmit']);
        Submittal_status::firstOrCreate(['short_name' => 'REJ/R', 'name'=>'Rejected / Resubmit']);
        Submittal_status::firstOrCreate(['short_name' => 'SFA', 'name'=>'Sent For Approval']);
        Submittal_status::firstOrCreate(['short_name' => 'Draft', 'name'=>'Draft']);
    }
}
