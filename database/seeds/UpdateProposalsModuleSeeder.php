<?php

use App\Models\File_type;
use App\Models\Module;
use Illuminate\Database\Seeder;

class UpdateProposalsModuleSeeder extends Seeder {

    public function run() {

        Module::where('display_name','=','proposals')
            ->where('name','=','Proposals')
            ->update([
                'name' => 'Bids',
                'display_name' => 'bids'
            ]);

        File_type::where('display_name','=','proposals')
            ->where('name','=','Proposals')
            ->update([
                'name' => 'Bids',
                'display_name' => 'bids'
            ]);

    }

}