<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//Model::unguard();

		try {

			$this->call('SubscriptionTypesSeeder');
//			$this->call('UsersSeeder');
//			$this->call('RolesPermissionsSeeder');
//			$this->call('CompaniesAdminsSeeder');
//			$this->call('DefaultCategoriesSeeder');
//			$this->call('AddressBookSeeder');
//			$this->call('FileTypesSeeder');
//			$this->call('ModulesSeeder');
//			$this->call('SubmittalStatusSeeder');
//			$this->call('RfiStatusesSeeder');
//			$this->call('PcoStatusesSeeder');
//			$this->call('MasterFormatSeeder');
//			$this->call('ProjectsSeeder');
//			$this->call('ExpeditingReportStatusSeeder');
//			$this->call('UpdateProposalsModuleSeeder');

		} catch (Exception $e) {
			echo $e->getMessage() . PHP_EOL;
			echo $e->getCode() . PHP_EOL;
			echo $e->getLine() . PHP_EOL;
			echo $e->getFile() . PHP_EOL;

			//var_dump($e);
		}
	}

}
