<?php
use App\Models\Module;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/26/2015
 * Time: 3:24 PM
 */

class ModulesSeeder extends Seeder {

    public function run() {
        Module::firstOrCreate(['name' => 'Projects','display_name' => 'projects']);
        Module::firstOrCreate(['name' => 'Address Book','display_name' => 'address-book']);
        Module::firstOrCreate(['name' => 'Submittals','display_name' => 'submittals']);
        Module::firstOrCreate(['name' => 'Proposals','display_name' => 'proposals']);
        Module::firstOrCreate(['name' => 'Contracts','display_name' => 'contracts']);
        Module::firstOrCreate(['name' => 'Expediting Reports','display_name' => 'expediting-report']);
        Module::firstOrCreate(['name' => 'Companies','display_name' => 'companies']);
        Module::firstOrCreate(['name' => 'Custom Categories','display_name' => 'custom-categories']);
        Module::firstOrCreate(['name' => "RFI's",'display_name' => 'rfis']);
        Module::firstOrCreate(['name' => "PCO's",'display_name' => 'pcos']);
        Module::firstOrCreate(['name' => "Blog",'display_name' => 'blog']);
    }

}