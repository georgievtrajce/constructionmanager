<?php
use App\Models\Default_category;
use Illuminate\Database\Seeder;

class DefaultCategoriesSeeder extends Seeder {

    public function run() {

        Default_category::firstOrCreate([
            'name' => 'Material Suppliers',
        ]);

        Default_category::firstOrCreate([
            'name' => 'Subcontractors',
        ]);

        Default_category::firstOrCreate([
            'name' => 'Owners',
        ]);

        Default_category::firstOrCreate([
            'name' => 'Architects',
        ]);

        Default_category::firstOrCreate([
            'name' => 'Engineers',
        ]);

        Default_category::firstOrCreate([
            'name' => 'Surveyors',
        ]);
    }
}