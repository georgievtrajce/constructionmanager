<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Laracasts\TestDummy\Factory;

class AddressBookSeeder extends Seeder {

    /** @test **/
    public function run() {
        //Factory::$factoriesPath = 'app/tests/factories';

        //address book for Test Company (subscription level 2)
        for ($i = 0; $i < 50; $i++) {

            $addressBook = Factory::create('Address_book', ['owner_comp_id' => 1]);

            Factory::times(3)->create('Ab_contact',['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Address',['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Address_book_categories', ['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Mf_address_book', ['ab_id' => $addressBook->id]);
        }

        //address book for SubLevel3 (subscription level 3)
        for ($i = 0; $i < 50; $i++) {

            $addressBook = Factory::create('Address_book', ['owner_comp_id' => 2]);

            Factory::times(3)->create('Ab_contact',['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Address',['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Address_book_categories', ['ab_id' => $addressBook->id]);
            Factory::times(3)->create('Mf_address_book', ['ab_id' => $addressBook->id]);
        }
    }
}
