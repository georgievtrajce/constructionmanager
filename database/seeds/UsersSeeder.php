<?php
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\User as User;
use App\Models\Company as Company;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Model;
use Laracasts\TestDummy\Factory;
use App\Models\AssignedRoles;

class UsersSeeder extends Seeder {

    public function run()
    {

        DB::table('users')->delete();
        //create super admin
        $superadmin = new User();
        $superadmin->email = 'dragan.atanasov@it-labs.com';
        $superadmin->password = Hash::make('superadmin123');
        $superadmin->name = 'Dragan Atanasov';
        $superadmin->title = 'Super Admin';
        $superadmin->office_phone = '12345';
        $superadmin->cell_phone = '67890';
        $superadmin->fax = '1234567890';
        $superadmin->confirmed = 1;
        $superadmin->save();

        //create company
        $company_one = new Company();
        $company_one->name = 'Test Company';
        $company_one->subs_id = 2;
        $company_one->subscription_payment_date = Carbon::now()->toDateString();
        $company_one->subscription_expire_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
        $company_one->save();

        //create company admin
        $companyadmin_one = new User();
        $companyadmin_one->email = 'companyadmin@it-labs.com';
        $companyadmin_one->password = Hash::make('companyadmin123');
        $companyadmin_one->name = 'Company Admin';
        $companyadmin_one->title = 'Company Admin';
        $companyadmin_one->office_phone = '12345';
        $companyadmin_one->cell_phone = '67890';
        $companyadmin_one->fax = '1234567890';
        $companyadmin_one->comp_id = $company_one->id;
        $companyadmin_one->confirmed = 1;
        $companyadmin_one->save();

        //create account subscription level 3
        //create company
        $company_two = new Company();
        $company_two->name = 'SubLevel3';
        $company_two->subs_id = 3;
        $company_two->subscription_payment_date = Carbon::now()->toDateString();
        $company_two->subscription_expire_date = date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 day"));
        $company_two->save();

        //create company admin
        $companyadmin_two = new User();
        $companyadmin_two->email = 'sublevelthree@it-labs.com';
        $companyadmin_two->password = Hash::make('sublevelthree123');
        $companyadmin_two->name = 'Jane Doe';
        $companyadmin_two->title = 'General Manager';
        $companyadmin_two->office_phone = '12345';
        $companyadmin_two->cell_phone = '67890';
        $companyadmin_two->fax = '1234567890';
        $companyadmin_two->comp_id = $company_two->id;
        $companyadmin_two->confirmed = 1;
        $companyadmin_two->save();

    }
}