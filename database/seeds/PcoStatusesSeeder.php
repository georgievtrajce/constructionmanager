<?php
use App\Models\Pco_status;
use Illuminate\Database\Seeder;

class PcoStatusesSeeder extends Seeder {

    public function run() {

        Pco_status::firstOrCreate(['short_name' => 'APP', 'name'=>'Approved']);
        Pco_status::firstOrCreate(['short_name' => 'AAN', 'name'=>'Approved As Noted']);
        Pco_status::firstOrCreate(['short_name' => 'AAN/R', 'name'=>'Approved As Noted / Resubmit']);
        Pco_status::firstOrCreate(['short_name' => 'R&R', 'name'=>'Revise & Resubmit']);
        Pco_status::firstOrCreate(['short_name' => 'REJ/R', 'name'=>'Rejected / Resubmit']);
        Pco_status::firstOrCreate(['short_name' => 'SFA', 'name'=>'Sent For Approval']);
        Pco_status::firstOrCreate(['short_name' => 'REJ', 'name'=>'Rejected']);
        Pco_status::firstOrCreate(['short_name' => 'Draft', 'name'=>'Draft']);
    }

}