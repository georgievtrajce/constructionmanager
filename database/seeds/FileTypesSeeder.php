<?php
use App\Models\File_type;
use Illuminate\Database\Seeder;

class FileTypesSeeder extends Seeder {

    public function run() {

        File_type::firstOrCreate(['name' => 'Submittals','display_name' => 'submittals']);
        File_type::firstOrCreate(['name' => 'Proposals','display_name' => 'proposals']);
        File_type::firstOrCreate(['name' => 'Contracts','display_name' => 'contracts']);
        File_type::firstOrCreate(['name' => 'Drawings','display_name' => 'drawings']);
        File_type::firstOrCreate(['name' => 'Specifications','display_name' => 'specifications']);
        File_type::firstOrCreate(['name' => 'Addendums','display_name' => 'addendums']);
        File_type::firstOrCreate(['name' => 'RFIs','display_name' => 'rfis']);
        File_type::firstOrCreate(['name' => 'PCOs','display_name' => 'pcos']);
        File_type::firstOrCreate(['name' => 'Correspondence/Emails','display_name' => 'correspondence-emails']);
        File_type::firstOrCreate(['name' => 'Inspection/Test Reports','display_name' => 'inspection-reports']);
        File_type::firstOrCreate(['name' => 'Permits','display_name' => 'permits']);
        File_type::firstOrCreate(['name' => 'Project Photos','display_name' => 'project-photos']);
        File_type::firstOrCreate(['name' => 'Work Schedules','display_name' => 'work-schedules']);
        File_type::firstOrCreate(['name' => 'Punch Lists','display_name' => 'punch-lists']);
        File_type::firstOrCreate(['name' => 'Meeting Minutes','display_name' => 'meeting-minutes']);
        File_type::firstOrCreate(['name' => 'Closeout Documents','display_name' => 'closeout-documents']);
        File_type::firstOrCreate(['name' => 'Billing','display_name' => 'billing']);
        File_type::firstOrCreate(['name' => 'Miscellaneous','display_name' => 'miscellaneous']);
    }
}