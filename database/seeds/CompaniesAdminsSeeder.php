<?php
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Laracasts\TestDummy\Factory;
use App\Models\AssignedRoles;

class CompaniesAdminsSeeder extends Seeder
{

    public function run()
    {
        //create random users with companies and role Company Admin

        for ($i = 0; $i < 50; $i++) {
            $user = Factory::create('factoryUser');

            $admin_assigned_role = new AssignedRoles;
            $admin_assigned_role->user_id = $user->id;
            $admin_assigned_role->role_id = 2;
            $admin_assigned_role->save();

            $data = ['comp_id' => $user->company->id];
            $address = Factory::times(3)->create('companyAddress',$data);
        }

        DB::statement("UPDATE companies SET uac = concat('UAC-',LPAD(id, 6, '0'))");
    }

}