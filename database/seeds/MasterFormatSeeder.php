<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel as Excel;
use App\Models\Master_Format as Master_Format;


class MasterFormatSeeder extends Seeder {

    public function run() {

        Excel::load('database/seeds/MasterFormat_2014_Transition_Matrix.csv', function ($reader) {
            $reader->noHeading()->each(function ($row) {

                $mfNumber = str_replace(" ", "", $row[0]);
                $mfTitle = $row[1];
                Master_Format::firstOrCreate(array('number'=>$mfNumber,'title' =>$mfTitle));
            });
        });
    }
}
