<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectTableOwnerColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("ALTER TABLE projects CHANGE `owner_transmittal_id` `contractor_transmittal_id` INT(11) NOT NULL DEFAULT 0");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("ALTER TABLE projects CHANGE `contractor_transmittal_id` `owner_transmittal_id` INT(1) NOT NULL DEFAULT 0");
	}

}
