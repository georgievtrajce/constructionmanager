<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubmittalsVersionsDropNumberField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->dropColumn('submittal_no');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->string('submittal_no')->after('submittal_id');
		});
	}

}
