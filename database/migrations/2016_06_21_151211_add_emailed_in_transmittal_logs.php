<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailedInTransmittalLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transmittals_logs', function($table)
		{
			$table->tinyInteger('emailed')->default(0)->after('date_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transmittals_logs', function($table)
		{
			$table->dropColumn('emailed');
		});
	}

}
