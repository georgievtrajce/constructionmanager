<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInSubmittialVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('submittals_versions', function($table)
        {
            $table->string('submitted_for')->after('subc_notes')->default(NULL);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('submittals_versions', function($table)
        {
            $table->dropColumn('submitted_for');
        });
	}

}
