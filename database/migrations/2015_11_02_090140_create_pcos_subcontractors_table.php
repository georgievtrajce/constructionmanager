<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcosSubcontractorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pcos_subcontractors_recipients', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('pco_id');
			$table->integer('last_sub_version');
			$table->integer('ab_subcontractor_id');
			$table->integer('ab_recipient_id');
			$table->string('mf_number');
			$table->string('mf_title');
			$table->text('note');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pcos_subcontractors_recipients');
	}

}
