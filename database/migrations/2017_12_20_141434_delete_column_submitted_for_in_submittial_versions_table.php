<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnSubmittedForInSubmittialVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('submittals', function($table)
        {
            $table->dropColumn('submitted_for');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('submittals', function($table)
        {
            $table->string('submitted_for')->after('sent_via')->default(NULL);
        });
	}

}
