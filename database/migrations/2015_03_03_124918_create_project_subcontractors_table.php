<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectSubcontractorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_subcontractors', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('comp_parent_id');
			$table->integer('comp_child_id');
			$table->integer('proj_id');
			$table->integer('address_id');
			$table->string('token');
			$table->tinyInteger('token_active');
			$table->tinyInteger('status');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_subcontractors');
	}

}
