<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ab_contacts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('title')->nullable();
			$table->string('email')->nullable();
			$table->integer('address_id')->nullable();
			$table->string('office_phone')->nullable();
			$table->string('cell_phone')->nullable();
			$table->string('fax')->nullable();
			$table->integer('ab_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ab_contacts');
	}

}
