<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCompanyPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    $companyPermissions = \App\Models\Project_company_permission::whereNotNull('proj_comp_id')
                                                                    ->where('comp_child_id', '=', 0)
                                                                    ->get();

	    foreach ($companyPermissions as $companyPermission) {
            $projectCompany = \App\Models\Project_company::where('id', '=', $companyPermission->proj_comp_id)->first();

            if ($projectCompany) {
                $addressBookCompany = \App\Models\Address_book::where('id', '=', $projectCompany->ab_id)->first();
                if ($addressBookCompany && !empty($addressBookCompany->synced_comp_id)) {
                    $companyPermission->comp_child_id = $addressBookCompany->synced_comp_id;
                    $companyPermission->save();
                }
            }
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //
	}

}
