<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_files', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('task_id');
            $table->integer('comp_id');
			$table->string('file_name', 255);
            $table->string('file_type',100);
			$table->date('expiration_date');
			$table->integer('size');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_files');
	}

}
