<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pcos', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('comp_id');
			$table->integer('user_id');
			$table->integer('last_subcontractor');
			$table->string('name');
			$table->string('subject');
			$table->string('sent_via');
			$table->string('cost');
			$table->integer('proj_id');
			$table->string('reason_for_change');
			$table->text('description_of_change');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pcos');
	}

}
