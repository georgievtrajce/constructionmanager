<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubscriptionTypesLimitations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('subscription_types', function($table)
		{
			$table->string('upload_limit')->after('ref_points');
			$table->string('download_limit')->after('upload_limit');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('subscription_types', function($table)
		{
			$table->dropColumn('upload_limit');
			$table->dropColumn('download_limit');
		});
	}

}
