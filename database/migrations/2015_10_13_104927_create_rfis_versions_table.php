<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfisVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfis_versions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('rfi_id');
			$table->string('rfi_number');
			$table->integer('cycle_no');
			$table->date('sent_appr');
			$table->date('rec_appr');
			$table->date('subm_sent_sub');
			$table->date('rec_sub');
			$table->integer('status_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfis_versions');
	}

}
