<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RenameNotesAddSubcNotesSubmittalsVersions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittals_versions', function($table)
		{
            DB::update(DB::raw("ALTER TABLE `submittals_versions` CHANGE COLUMN `notes` `appr_notes` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `status_id`;"));
            $table->text('subc_notes')->after('appr_notes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittals_versions', function($table)
		{
            $table->dropColumn('subc_notes');
            DB::update(DB::raw("ALTER TABLE `submittals_versions` CHANGE COLUMN `appr_notes` `notes` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `status_id`;"));

        });
	}

}
