<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubcontractorProjectInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('subcontractor_project_info', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->double('price', 14, 4)->nullable();
			$table->tinyInteger('active');
			$table->string('location')->nullable();
			$table->integer('number')->nullable();
			$table->date('start_date')->nullable();
			$table->date('end_date')->nullable();
			$table->integer('owner_id')->nullable();
			$table->integer('architect_id')->nullable();
			$table->integer('proj_id');
			$table->integer('subcontractor_id');
			$table->integer('creator_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('subcontractor_project_info');
	}

}
