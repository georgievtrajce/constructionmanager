<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectPermissionsFileTypeId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_permissions', function($table)
		{
			$table->integer('file_type_id')->after('entity_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_permissions', function($table)
		{
			$table->dropColumn('file_type_id');
		});
	}

}
