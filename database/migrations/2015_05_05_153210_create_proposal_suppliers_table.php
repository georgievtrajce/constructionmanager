<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalSuppliersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposal_suppliers', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('ab_id');
			$table->integer('prop_id');
			$table->integer('ab_cont_id');
			$table->integer('addr_id');
			$table->double('proposal_1');
			$table->double('proposal_2');
			$table->double('proposal_final');
			$table->timestamps();
	});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposal_suppliers');
	}

}
