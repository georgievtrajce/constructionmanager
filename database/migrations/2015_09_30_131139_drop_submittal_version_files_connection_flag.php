<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSubmittalVersionFilesConnectionFlag extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittal_version_files', function($table)
		{
			$table->dropColumn('version_date_connection');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittal_version_files', function($table)
		{
			$table->string('version_date_connection')->after('file_id');
		});
	}

}
