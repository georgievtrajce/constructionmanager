<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalSupplierPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposal_supplier_permissions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bidder_id');
			$table->integer('comp_parent_id');
			$table->integer('comp_child_id');
			$table->tinyInteger('read');
			$table->tinyInteger('write');
			$table->tinyInteger('delete');
			$table->integer('ab_id');
			$table->integer('proj_id');
			$table->integer('entity_id');
			$table->integer('entity_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposal_supplier_permissions');
	}

}
