<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompIdInProjectCompanyContacts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_company_contacts', function($table)
		{
			$table->integer('proj_comp_id')->after('proj_id');
			$table->integer('ab_address_id')->after('ab_cont_id');
			$table->integer('ab_comp_id')->after('ab_address_id');
			$table->dropColumn('custom_field');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_company_contacts', function($table)
		{
			$table->dropColumn('proj_comp_id');
			$table->dropColumn('ab_address_id');
			$table->dropColumn('ab_comp_id');
		});
	}

}
