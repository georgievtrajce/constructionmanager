<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcoVersionFilesDownloadPriority extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pco_version_files', function($table)
		{
			$table->tinyInteger('download_priority')->after('file_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pco_version_files', function($table)
		{
			$table->dropColumn('download_priority');
		});
	}

}
