<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubmittalsVersionsChanges extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->dropColumn('number');
			$table->dropColumn('aws_file_path');
			$table->integer('file_id')->after('submittal_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->string('number')->after('submittal_id');
			$table->string('aws_file_path')->after('status');
			$table->dropColumn('file_id');
		});
	}

}
