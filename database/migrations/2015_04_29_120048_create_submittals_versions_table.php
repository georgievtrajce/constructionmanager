<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmittalsVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submittals_versions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('submittal_id');
			$table->string('number');
			$table->integer('cycle_no');
			$table->date('sent_appr');
			$table->date('rec_appr');
			$table->date('subm_sent_sub');
			$table->date('rec_sub');
			$table->string('status', 45);
			$table->string('aws_file_path');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submittals_versions');
	}

}
