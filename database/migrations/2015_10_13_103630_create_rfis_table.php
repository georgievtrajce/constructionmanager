<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfis', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('last_version');
			$table->integer('comp_id');
			$table->integer('user_id');
			$table->string('name');
			$table->string('subject');
			$table->string('sent_via');
			$table->string('is_change');
			$table->string('mf_number');
			$table->string('mf_title');
			$table->integer('sub_id');
			$table->integer('recipient_id');
			$table->integer('proj_id');
			$table->text('question');
			$table->text('answer');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfis');
	}

}
