<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterMasterFormatTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('master_format', function($table)
        {
            $table->integer('master_format_type_id')->default(0)->after('id');
        });

        $id = DB::table('master_format_types')->insertGetId(
            [
                'year' => 2014,
                'name' => 'Master format - 2014'
            ]
        );

        DB::table('master_format')->update(array('master_format_type_id' => $id));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('master_format', function($table)
        {
            $table->dropColumn('master_format_type_id');
        });
	}
}
