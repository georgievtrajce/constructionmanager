<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsContractorVarbalInstructionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_report_contractor_verbal_instructions', function (Blueprint $table) {
            //step 9
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            $table->integer('selected_comp_id');
            $table->integer('selected_employee_id');
            $table->text('description')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_contractor_verbal_instructions');
	}

}
