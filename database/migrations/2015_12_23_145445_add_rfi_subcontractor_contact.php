<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRfiSubcontractorContact extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfis', function($table)
		{
			$table->integer('sub_contact_id')->after('sub_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfis', function($table)
		{
			$table->dropColumn('sub_contact_id');
		});
	}

}
