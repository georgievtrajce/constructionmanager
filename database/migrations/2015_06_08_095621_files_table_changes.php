<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FilesTableChanges extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('files', function($table)
		{
			$table->dropColumn('ff_id');
			$table->dropColumn('path');
			$table->dropColumn('extension');
			$table->dropColumn('shared');

			$table->integer('comp_id')->after('user_id');
			$table->string('file_name')->after('number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('files', function($table)
		{
			$table->integer('ff_id')->after('user_id');
			$table->string('path')->after('number');
			$table->string('extension', 45)->after('path');
			$table->tinyInteger('shared');

			$table->dropColumn('comp_id');
			$table->dropColumn('file_name');
		});
	}

}
