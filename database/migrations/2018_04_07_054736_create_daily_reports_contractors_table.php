<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsContractorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_report_contractors', function (Blueprint $table) {
            //step 3 tab 1
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            //workers with names tab
            $table->string('worker_office');
            $table->string('worker_name');
            $table->string('title');
            $table->integer('regular_hours');
            $table->integer('overtime_hours');
            $table->text('description')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_contractors');
	}

}
