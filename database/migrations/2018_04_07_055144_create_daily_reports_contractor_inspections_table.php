<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsContractorInspectionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_report_contractor_inspections', function (Blueprint $table) {
            //step 7
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            $table->string('testing_performed');
            $table->string('mf_number');
            $table->string('mf_title');
            $table->integer('selected_comp_id')->default(0);
            $table->integer('selected_employee_id')->default(0);
            $table->string('other_company')->nullable();
            $table->string('other_employee')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_contractor_inspections');
	}

}
