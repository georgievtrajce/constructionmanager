<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcoPermissionTypeInProposalSupplierPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('proposal_supplier_permissions', function($table)
		{
			$table->tinyInteger('pco_permission_type')->after('read');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proposal_supplier_permissions', function($table)
		{
			$table->dropColumn('pco_permission_type');
		});
	}

}
