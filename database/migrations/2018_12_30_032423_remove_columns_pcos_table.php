<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveColumnsPcosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pcos', function($table)
        {
            $table->dropColumn('self_performed');
            $table->dropColumn('sub_id');
            $table->dropColumn('sub_contact_id');
            $table->dropColumn('send_notif_subcontractor');
            $table->dropColumn('send_notif_recipient');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pcos', function($table)
        {
            $table->tinyInteger('self_performed')->default(0);
            $table->integer('sub_id')->default(0);
            $table->integer('sub_contact_id')->default(0);
            $table->tinyInteger('send_notif_subcontractor')->default(0);
            $table->tinyInteger('send_notif_recipient')->default(0);
        });
	}

}
