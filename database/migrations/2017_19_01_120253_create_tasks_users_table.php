<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks_users', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('task_id');
			$table->integer('user_id')->nullable();
            $table->integer('ab_cont_id')->nullable();
            $table->integer('ab_comp_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks_users');
	}

}
