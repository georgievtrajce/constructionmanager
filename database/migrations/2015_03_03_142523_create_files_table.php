<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('files', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ft_id');
			$table->integer('user_id');
			$table->integer('proj_id');
			$table->integer('ff_id');
			$table->string('name');
			$table->string('number', 45);
			$table->string('path');
			$table->string('extension', 45);
			$table->tinyInteger('shared');
			$table->integer('size');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('files');
	}

}
