<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookDistributionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_book_distribution', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ab_id');
            $table->integer('ab_file_id');
            $table->integer('ab_cont_id')->nullable();
            $table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address_book_distribution');
	}

}
