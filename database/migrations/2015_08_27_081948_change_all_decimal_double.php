<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllDecimalDouble extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//proposals
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price DOUBLE(12,4)');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value DOUBLE(12,4)');

		//proposal_suppliers
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_1 DOUBLE(12,4)');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_2 DOUBLE(12,4)');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_final DOUBLE(12,4)');

		//contracts
		DB::statement('ALTER TABLE contracts MODIFY COLUMN value DOUBLE(12, 4)');

		//projects
		DB::statement('ALTER TABLE projects MODIFY COLUMN price DOUBLE(12, 4)');

		//transactions
		DB::statement('ALTER TABLE transactions MODIFY COLUMN amount DOUBLE(12, 4)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//proposals
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price DECIMAL');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value DECIMAL');

		//proposal_suppliers
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_1 DECIMAL');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_2 DECIMAL');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_final DECIMAL');

		//contracts
		DB::statement('ALTER TABLE contracts MODIFY COLUMN value DOUBLE');

		//projects
		DB::statement('ALTER TABLE projects MODIFY COLUMN price DECIMAL');

		//transactions
		DB::statement('ALTER TABLE transactions MODIFY COLUMN amount INT(11)');

	}


}
