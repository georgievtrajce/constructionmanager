<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsToAddressBookFile extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('ab_files', function($table)
		{
			$table->integer('notification')->nullable();
            $table->integer('notification_days')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('ab_files', function($table)
		{
			$table->dropColumn('notification');
            $table->dropColumn('notification_days');
		});
	}

}
