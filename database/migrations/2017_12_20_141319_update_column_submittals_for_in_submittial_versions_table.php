<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnSubmittalsForInSubmittialVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    DB::statement("SET SQL_SAFE_UPDATES = 0");
        DB::statement("UPDATE submittals_versions
                            INNER JOIN submittals ON submittals.id = submittals_versions.submittal_id
                            SET submittals_versions.submitted_for = submittals.submitted_for");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

	}

}
