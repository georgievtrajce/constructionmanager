<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfiPermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfi_permissions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('rfi_id');
			$table->integer('comp_parent_id');
			$table->integer('comp_child_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfi_permissions');
	}

}
