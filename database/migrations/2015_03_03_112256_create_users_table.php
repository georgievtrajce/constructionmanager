<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('name');
			$table->string('title');
			$table->integer('address_id')->nullable();
			$table->string('office_phone', 45);
			$table->string('cell_phone', 45)->nullable();
			$table->string('fax', 45)->nullable();
			$table->integer('comp_id')->nullable();
			$table->integer('type');
			$table->rememberToken()->nullable();
			$table->tinyInteger('active')->default(1);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
