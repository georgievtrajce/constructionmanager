<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAbIdInProjectSubcontractors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->integer('proj_comp_id')->after('comp_child_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->dropColumn('proj_comp_id');
		});
	}

}
