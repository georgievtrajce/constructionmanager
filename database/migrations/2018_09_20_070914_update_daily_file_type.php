<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDailyFileType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    $filePermissions = \App\Models\Project_permission::groupBy('proj_id', 'user_id')->get();
	    foreach ($filePermissions as $permission) {
            $checkForDaily = \App\Models\Project_permission::where('file_type_id', '=', 19)
                                                            ->where('proj_id', '=', $permission->proj_id)
                                                            ->where('user_id', '=', $permission->user_id)
                                                            ->first();
            if (!$checkForDaily) {
                $permissionDaily = new \App\Models\Project_permission();
                $permissionDaily->file_type_id = 19;
                $permissionDaily->entity_id = 19;
                $permissionDaily->entity_type = 2;
                $permissionDaily->show_cost = 0;
                $permissionDaily->proj_id = $permission->proj_id;
                $permissionDaily->user_id = $permission->user_id;
                $permissionDaily->read = 0;
                $permissionDaily->write = 0;
                $permissionDaily->delete = 0;
                $permissionDaily->save();
            }
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        //
	}

}
