<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeProposalTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//proposals
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price DECIMAL');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value DECIMAL');

		//proposal_suppliers
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_1 DECIMAL');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_2 DECIMAL');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_final DECIMAL');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//proposals
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price VARCHAR');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value VARCHAR');

		//proposal_suppliers
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_1 DOUBLE');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_2 DOUBLE');
		DB::statement('ALTER TABLE proposal_suppliers MODIFY COLUMN proposal_final DOUBLE');
	}

}
