<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSubmittalsVersionsStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->dropColumn('status');
			$table->integer('status_id')->after('rec_sub');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittals_versions', function($table)
		{
			$table->dropColumn('status_id');
			$table->string('status', 45)->after('rec_sub');
		});
	}

}
