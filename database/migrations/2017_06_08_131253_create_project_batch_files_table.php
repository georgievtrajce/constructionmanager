<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectBatchFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_batch_files', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('generated')->default(0);
            $table->integer('sent')->default(0);
            $table->integer('user_id')->nullable();
            $table->integer('total_file_size')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_batch_files');
	}

}
