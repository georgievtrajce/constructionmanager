<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShareCompIdFilePermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('file_permissions', function($table)
		{
			$table->integer('share_comp_id')->after('comp_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('file_permissions', function($table)
		{
			$table->dropColumn('share_comp_id');
		});
	}

}
