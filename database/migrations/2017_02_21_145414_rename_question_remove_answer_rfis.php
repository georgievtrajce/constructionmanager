<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RenameQuestionRemoveAnswerRfis extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfis', function($table)
		{
            DB::update(DB::raw("ALTER TABLE `rfis` CHANGE COLUMN `question` `note` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `proj_id`;"));
            //$table->renameColumn('question', 'note');
            $table->dropColumn('answer');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfis', function($table)
		{
            $table->text('answer');
            DB::update(DB::raw("ALTER TABLE `rfis` CHANGE COLUMN `note` `question` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `proj_id`;"));

        });
	}

}
