<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShowCostInProjectPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_permissions', function($table)
		{
			$table->tinyInteger('show_cost')->after('delete');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_permissions', function($table)
		{
			$table->dropColumn('show_cost');
		});
	}

}
