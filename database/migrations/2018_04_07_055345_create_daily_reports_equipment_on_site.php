<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsEquipmentOnSite extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    //step 5
        Schema::create('daily_report_equipment', function (Blueprint $table) {
            //step 3
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            $table->string('equipment');
            $table->integer('quality');
            $table->integer('selected_comp_id');
            $table->integer('hours');
            $table->string('description')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_equipment');
	}

}
