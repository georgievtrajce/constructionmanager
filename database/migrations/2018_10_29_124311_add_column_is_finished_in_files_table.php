<?php

use App\Models\Project_file;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsFinishedInFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('files', function($table)
        {
            $table->integer('is_finished')->after('emailed')->default(0);
        });

        $files = Project_file::where('transmittal', '=', 0)
            ->where('name', '!=', '')
            ->whereNotNull('name')
            ->where('number', '!=', '')
            ->whereNotNull('number')->get();

        $finishedFiles = array();
        foreach ($files as $file) {
            $finishedFiles[] = $file->id;
        }

        if(count($finishedFiles) > 0) {
            Project_file::whereIn('id', $finishedFiles)->update(['is_finished' => 1]);
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('files', function($table)
        {
            $table->dropColumn('is_finished');
        });
	}

}
