<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeInPcosSubcontractorsRecipients extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->tinyInteger('type')->after('pco_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->dropColumn('type');
		});
	}

}
