<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllProjectsUserPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $allProjectModule = \App\Models\Module::create(['name' => 'All projects & modules', 'display_name' => 'all-projects-modules']);

        //delete all permissions
        $addressBookPermissions = \App\Models\User_permission::where('entity_type', '=', 1)->where('entity_id', '=', 2)->get();
        foreach ($addressBookPermissions as $addressBookPermission) {
            \App\Models\User_permission::firstOrCreate([
                'user_id' => $addressBookPermission->user_id,
                'read' => 0,
                'write' => 0,
                'delete' => 0,
                'entity_id' => $allProjectModule->id,
                'entity_type' => 1
            ]);
        }

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $allProjectModule = \App\Models\Module::where('name', '=', 'All projects & modules')->first();

        \App\Models\User_permission::where('entity_type', '=', 1)->where('entity_id', '=', $allProjectModule->id)->delete();
	}
}
