<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsTablePrimeSubcontractor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('projects', function($table)
        {
            $table->integer('prime_subcontractor_id')->nullable();
            $table->integer('prime_subcontractor_transmittal_id')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('projects', function($table)
        {
            $table->dropColumn('prime_subcontractor_id');
            $table->dropColumn('prime_subcontractor_transmittal_id');
        });
	}

}
