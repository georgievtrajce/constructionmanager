<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tasks', function (Blueprint $table) {
			$table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->integer('comp_id');
            $table->integer('added_by');
			$table->string('title', 255);
			$table->date('due_date');
			$table->integer('status')->default(1);
            $table->string('mf_number', 255)->nullable();
            $table->string('mf_title', 255)->nullable();
            $table->string('custom_type', 255)->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('pco_id')->nullable();
            $table->integer('rfi_id')->nullable();
            $table->integer('submittal_id')->nullable();
            $table->integer('contract_id')->nullable();
            $table->integer('bid_id')->nullable();
            $table->integer('material_id')->nullable();
			$table->text('note')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tasks');
	}

}
