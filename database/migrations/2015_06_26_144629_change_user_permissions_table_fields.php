<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUserPermissionsTableFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_permissions', function($table)
		{
			$table->dropColumn('add');
			$table->dropColumn('edit');
			$table->tinyInteger('read')->after('user_id');
			$table->tinyInteger('write')->after('read');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_permissions', function($table)
		{
			$table->dropColumn('read');
			$table->dropColumn('write');
			$table->tinyInteger('add')->after('user_id');
			$table->tinyInteger('edit')->after('add');
		});
	}

}
