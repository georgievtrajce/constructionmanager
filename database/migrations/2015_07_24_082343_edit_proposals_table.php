<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price VARCHAR(255)');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value VARCHAR(255)');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN quantity VARCHAR(255)');

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('ALTER TABLE proposals MODIFY COLUMN unit_price DOUBLE');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN value DOUBLE');
		DB::statement('ALTER TABLE proposals MODIFY COLUMN quantity DOUBLE');
	}

}
