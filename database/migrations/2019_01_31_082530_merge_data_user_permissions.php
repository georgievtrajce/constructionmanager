<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MergeDataUserPermissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $customCategories = \App\Models\Module::find(8);

        if ($customCategories) {
            $addressBook = \App\Models\Module::find(2);
            if ($addressBook) {
                $addressBook->name = 'Address Book & Custom categories';
                $addressBook->save();
            }

            $customCategories->delete();
        }

        //delete all permissions
        \App\Models\User_permission::where('entity_type', '=', 1)->where('entity_id', '=', 8)->delete();
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $addressBook = \App\Models\Module::find(2);
        if ($addressBook) {
            $addressBook->name = 'Address Book';
            $addressBook->save();
        }

        \App\Models\Module::create(['id' => 8, 'name' => 'Custom categories', 'display_name' => 'custom-categories']);

        $userPermissions = \App\Models\User_permission::where('entity_type', '=', 1)->where('entity_id', '=', 2)->get();

        foreach ($userPermissions as $permission) {
            $permissionReplicate = $permission->replicate();
            $permissionReplicate->entity_id = 8;
            $permissionReplicate->save();
        }
	}
}
