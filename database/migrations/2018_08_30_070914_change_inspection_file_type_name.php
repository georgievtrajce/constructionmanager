<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeInspectionFileTypeName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("UPDATE file_types SET name = 'Inspection/Test Reports' WHERE name = 'Inspection Reports'");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("UPDATE file_types SET name = 'Inspection Reports' WHERE name = 'Inspection/Test Reports'");
	}

}
