<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmittalVersionsMultipleFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submittal_version_files', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('submittal_version_id');
			$table->integer('file_id');
			$table->string('version_date_connection');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submittal_version_files');
	}

}
