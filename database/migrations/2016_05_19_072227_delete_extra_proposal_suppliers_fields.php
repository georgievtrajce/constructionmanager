<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteExtraProposalSuppliersFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('proposal_suppliers', function($table)
		{
			$table->dropColumn('invited_comp_id');
			$table->dropColumn('token');
			$table->dropColumn('token_active');
			$table->dropColumn('status');
			$table->dropColumn('once_accepted');
			$table->integer('proj_bidder_id')->after('id')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('proposal_suppliers', function($table)
		{
			$table->integer('invited_comp_id')->after('id')->default(0);
			$table->string('token')->after('proposal_final');
			$table->tinyInteger('token_active')->after('token');
			$table->tinyInteger('status')->after('token_active')->default(0);
			$table->tinyInteger('once_accepted')->after('status')->default(0);
			$table->dropColumn('proj_bidder_id');
		});
	}

}
