<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPermissionSelectedUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('project_permission_selected_users', function (Blueprint $table) {
            $table->integer('proj_id');
            $table->integer('user_id');
        });

        DB::statement("ALTER TABLE project_permission_selected_users ADD PRIMARY KEY(proj_id,user_id)");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('project_permission_selected_users');
	}

}
