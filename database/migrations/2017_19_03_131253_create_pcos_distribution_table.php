<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcosDistributionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pcos_versions_distribution', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('pco_version_id');
            $table->integer('ab_cont_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('type')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pcos_versions_distribution');
	}

}
