<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ab_files', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ab_id');
            $table->integer('comp_id');
			$table->string('name');
			$table->string('number', 255);
			$table->string('file_name', 255);
            $table->string('file_type',100);
			$table->date('expiration_date');
            $table->string('file_orientation', 10);
			$table->integer('size');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ab_files');
	}

}
