<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RfisVersionsDropNumberField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfis_versions', function($table)
		{
			$table->dropColumn('rfi_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfis_versions', function($table)
		{
			$table->string('rfi_number')->after('rfi_id');
		});
	}

}
