<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectSubcontractorsOnceAccepted extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->tinyInteger('once_accepted')->after('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->dropColumn('once_accepted');
		});
	}

}
