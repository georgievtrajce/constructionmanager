<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCostToPcosVersions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_versions', function($table)
		{
			$table->string('cost')->after('notes');
		});

        DB::statement('ALTER TABLE pcos_versions MODIFY COLUMN cost DOUBLE(14,4)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_versions', function($table)
		{
			$table->dropColumn('cost');
		});
	}

}
