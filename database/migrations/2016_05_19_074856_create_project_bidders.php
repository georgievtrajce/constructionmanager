<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectBidders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_bidders', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('comp_parent_id');
			$table->integer('invited_comp_id');
			$table->integer('ab_id');
			$table->integer('proj_id');
			$table->string('token');
			$table->tinyInteger('token_active');
			$table->tinyInteger('status')->default(0);
			$table->tinyInteger('once_accepted')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_bidders');
	}

}
