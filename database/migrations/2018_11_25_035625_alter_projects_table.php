<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('projects', function($table)
        {
            $table->integer('owner_transmittal_id')->default(0);
            $table->integer('architect_transmittal_id')->default(0);
            $table->integer('general_contractor_id')->default(0);
            $table->tinyInteger('make_me_gc')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('projects', function($table)
        {
            $table->dropColumn('owner_transmittal_id');
            $table->dropColumn('architect_transmittal_id');
            $table->dropColumn('general_contractor_id');
            $table->dropColumn('make_me_gc');
        });
	}

}
