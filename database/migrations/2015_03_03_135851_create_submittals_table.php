<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmittalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submittals', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('mf_number');
			$table->string('mf_title');
			$table->integer('sub_id');
			$table->integer('proj_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submittals');
	}

}
