<?php

use Illuminate\Database\Migrations\Migration;

class AddCountersInProjects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('projects', function($table)
		{
            $table->integer('pco_counter')->default(0)->after('proj_admin');
            $table->integer('rfi_counter')->default(0)->after('pco_counter');
            $table->integer('submittals_counter')->default(0)->after('rfi_counter');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('projects', function($table)
		{
			$table->dropColumn('pco_counter');
            $table->dropColumn('rfi_counter');
            $table->dropColumn('submittal_counter');
		});
	}

}
