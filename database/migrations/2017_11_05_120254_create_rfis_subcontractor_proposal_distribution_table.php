<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfisSubcontractorProposalDistributionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rfis_subcontractor_proposal_distribution', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('version_id');
            $table->integer('user_id');
            $table->boolean('subcontractor')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rfis_subcontractor_proposal_distribution');
	}

}
