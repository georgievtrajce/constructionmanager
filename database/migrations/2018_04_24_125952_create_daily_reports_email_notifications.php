<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsEmailNotifications extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_report_email_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            $table->tinyInteger('emailed')->default(0);
            $table->tinyInteger('is_ab_contact')->default(0);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_email_notifications');
	}

}
