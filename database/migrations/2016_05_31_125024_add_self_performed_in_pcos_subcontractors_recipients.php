<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSelfPerformedInPcosSubcontractorsRecipients extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->tinyInteger('self_performed')->after('last_recipient_version')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->dropColumn('self_performed');
		});
	}

}
