<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcosSubcontractorProposalDistributionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pcos_subcontractor_proposal_distribution', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('version_id');
            $table->integer('user_id');
            $table->boolean('subcontractor')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pcos_subcontractor_proposal_distribution');
	}

}
