<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportsFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_report_files', function (Blueprint $table) {
            //step 11
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->integer('daily_report_id');
            $table->integer('file_type_id');
            $table->integer('file_id');
            $table->string('name');
            $table->string('file_name');
            $table->string('file_mime_type');
            $table->tinyInteger('emailed');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        DB::statement("INSERT INTO file_types VALUES(NULL, 'Daily Reports', 'daily-reports', '2018-03-03 09:10:23', '2018-03-03 09:10:23')");
        DB::statement("INSERT INTO file_types VALUES(NULL, 'Daily Reports Images', 'daily-reports-images', '2018-03-03 09:10:23', '2018-03-03 09:10:23')");
        DB::statement("INSERT INTO file_types VALUES(NULL, 'Daily Reports Files', 'daily-reports-files', '2018-03-03 09:10:23', '2018-03-03 09:10:23')");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_report_files');

        DB::statement("DELETE FROM file_types WHERE display_name = 'daily-reports'");
        DB::statement("DELETE FROM file_types WHERE display_name = 'daily-reports-images'");
        DB::statement("DELETE FROM file_types WHERE display_name = 'daily-reports-files'");
	}

}
