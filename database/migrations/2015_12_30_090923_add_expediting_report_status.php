<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExpeditingReportStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('expediting_reports', function($table)
		{
			$table->integer('er_status_id')->after('ab_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('expediting_reports', function($table)
		{
			$table->dropColumn('er_status_id');
		});
	}

}
