<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransmittalsLogs extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transmittals_logs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('comp_id');
			$table->integer('user_id');
			$table->integer('file_id');
			$table->integer('file_type_id');
			$table->integer('proj_id');
			$table->integer('version_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transmittals_logs');
	}

}
