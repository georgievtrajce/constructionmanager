<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsageHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies_usage_history', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
            $table->string('data',255);
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->string('type')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies_usage_history');
	}

}
