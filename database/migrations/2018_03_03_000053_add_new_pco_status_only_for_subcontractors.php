<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPcoStatusOnlyForSubcontractors extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement("INSERT INTO pco_statuses VALUES(NULL, 'R-UR', 'Received - Under Review', '2018-03-03 09:10:23', '2018-03-03 09:10:23')");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement("DELETE FROM pco_statuses WHERE short_name = 'R-UR'");
	}

}
