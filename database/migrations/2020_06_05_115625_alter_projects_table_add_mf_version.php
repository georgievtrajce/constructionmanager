<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterProjectsTableAddMfVersion extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('projects', function($table)
        {
            $table->integer('master_format_type_id')->default(0)->after('prime_subcontractor_transmittal_id');
        });

        $masterFormatType = DB::table('master_format_types')->where('name', '=', 'Master format - 2014')->first();

        DB::table('projects')->update(array('master_format_type_id' => $masterFormatType->id));
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('projects', function($table)
        {
            $table->dropColumn('master_format_type_id');
        });
	}
}
