<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubcontractorProjectNumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->string('sub_proj_number', 45)->after('proj_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('project_subcontractors', function($table)
		{
			$table->dropColumn('sub_proj_number');
		});
	}

}
