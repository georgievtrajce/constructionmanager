<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationFlagsInCompanies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('companies', function($table)
		{
			$table->tinyInteger('storage_notification')->after('limit_year')->default(0);
            $table->tinyInteger('upload_notification')->after('storage_notification')->default(0);
            $table->tinyInteger('download_notification')->after('upload_notification')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('companies', function($table)
		{
			$table->dropColumn('storage_notification');
            $table->dropColumn('upload_notification');
            $table->dropColumn('download_notification');
		});
	}

}
