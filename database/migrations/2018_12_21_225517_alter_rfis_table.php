<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRfisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('rfis', function($table)
        {
            $table->tinyInteger('send_notif_subcontractor')->default(0);
            $table->tinyInteger('send_notif_recipient')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('rfis', function($table)
        {
            $table->dropColumn('send_notif_subcontractor');
            $table->dropColumn('send_notif_recipient');
        });
	}

}
