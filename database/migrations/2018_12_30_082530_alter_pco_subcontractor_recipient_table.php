<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPcoSubcontractorRecipientTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('pcos_subcontractors_recipients', function($table)
        {
            $table->tinyInteger('send_notif')->default(0);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('pcos_subcontractors_recipients', function($table)
        {
            $table->dropColumn('send_notif');
        });
	}

}
