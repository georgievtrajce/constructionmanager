<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpeditingReportsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('expediting_reports', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('proj_id');
			$table->string('mf_number');
			$table->string('mf_title');
			$table->string('name');
			$table->string('position');
			$table->integer('ab_id');
			$table->tinyInteger('subm_needed');
			$table->integer('status_id');
			$table->date('need_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('expediting_reports');
	}

}
