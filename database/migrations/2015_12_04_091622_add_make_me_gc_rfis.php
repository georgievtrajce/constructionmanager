<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMakeMeGcRfis extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfis', function($table)
		{
			$table->tinyInteger('make_me_gc')->after('gc_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfis', function($table)
		{
			$table->dropColumn('make_me_gc');
		});
	}

}
