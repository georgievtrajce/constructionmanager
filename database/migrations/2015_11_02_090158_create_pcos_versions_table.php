<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePcosVersionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pcos_versions', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('subcontractor_id');
			$table->integer('recipient_id');
			$table->string('pco_number');
			$table->integer('cycle_no');
			$table->date('sent_appr');
			$table->date('rec_appr');
			$table->date('subm_sent_sub');
			$table->date('rec_sub');
			$table->integer('status_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pcos_versions');
	}

}
