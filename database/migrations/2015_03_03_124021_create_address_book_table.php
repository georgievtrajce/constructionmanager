<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressBookTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('address_book', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('mf_id');
			$table->integer('owner_comp_id');
			$table->integer('synced_comp_id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('address_book');
	}

}
