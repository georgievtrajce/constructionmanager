<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddItemsInContracts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contracts', function($table)
		{
			$table->string('mf_title')->after('value');
			$table->string('mf_number')->after('mf_title');
			$table->string('name')->after('mf_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contracts', function($table)
		{
			$table->dropColumn('mf_title');
			$table->dropColumn('mf_number');
			$table->dropColumn('name');
		});
	}

}
