<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCompanyRatings extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_company_ratings', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ab_cont_id');
			$table->integer('proj_id');
            $table->integer('price')->nullable();
            $table->integer('quality_of_work')->nullable();
            $table->integer('quality_of_materials')->nullable();
            $table->integer('expertise')->nullable();
            $table->integer('problems_resolution')->nullable();
            $table->integer('safety')->nullable();
            $table->integer('cleanness')->nullable();
            $table->integer('organized_and_professional')->nullable();
            $table->integer('communication_skills')->nullable();
            $table->integer('completed_on_time')->nullable();
            $table->float('project_rating')->nullable();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_company_contacts');
	}
}
