<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcoSubcontractorContact extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->integer('sub_contact_id')->after('ab_subcontractor_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->dropColumn('sub_contact_id');
		});
	}

}
