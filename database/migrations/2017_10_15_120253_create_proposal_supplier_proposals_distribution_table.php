<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalSupplierProposalsDistributionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposal_supplier_proposals_distribution', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('bidder_id');
            $table->integer('user_id');
            $table->integer('proposal_no');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposal_supplier_proposals_distribution');
	}

}
