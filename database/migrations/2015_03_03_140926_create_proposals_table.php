<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('proposals', function (Blueprint $table) {

			$table->increments('id');
			$table->integer('proj_id');
			$table->string('mf_title');
			$table->string('mf_number');
			$table->string('name');
			$table->double('unit_price');
			$table->double('quantity');
			$table->double('value');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('proposals');
	}

}
