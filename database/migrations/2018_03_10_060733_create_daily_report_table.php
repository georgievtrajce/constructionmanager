<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('daily_reports', function (Blueprint $table) {
            //step 1
            $table->increments('id');
            $table->integer('proj_id');
            $table->integer('comp_id');
            $table->integer('user_id');
            $table->string('report_num');
            $table->date('date');

            //step 2
            $table->integer('is_windy')->default(0);
            $table->integer('is_sunny')->default(0);
            $table->integer('is_overcast')->default(0);
            $table->integer('is_cloudy')->default(0);
            $table->integer('is_rain')->default(0);
            $table->integer('is_snow')->default(0);
            $table->integer('is_sleet')->default(0);
            $table->integer('is_hail')->default(0);
            $table->integer('is_lightning')->default(0);
            $table->string('temperature')->default(0);
            $table->integer('is_dry')->default(0);
            $table->integer('is_frozen')->default(0);
            $table->integer('ground_is_snow')->default(0);
            $table->integer('is_mud')->default(0);
            $table->integer('is_water')->default(0);
            $table->text('condition_notes')->nullable();

            //step 10
            $table->text('general_notes')->nullable();

            $table->integer('copied_report_id')->default(0);
            $table->tinyInteger('is_emailed')->default(0);
            $table->tinyInteger('is_uploaded')->default(0);
            $table->timestamps();
        });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('daily_reports');

	}

}
