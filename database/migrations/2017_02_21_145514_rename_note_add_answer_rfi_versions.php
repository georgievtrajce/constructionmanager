<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RenameNoteAddAnswerRfiVersions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('rfis_versions', function($table)
		{
            DB::update(DB::raw("ALTER TABLE `rfis_versions` CHANGE COLUMN `notes` `question` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `status_id`;"));
            $table->text('answer')->after('question');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('rfis_versions', function($table)
		{
            $table->dropColumn('answer');
            DB::update(DB::raw("ALTER TABLE `rfis_versions` CHANGE COLUMN `question` `notes` TEXT NOT NULL COLLATE 'utf8_unicode_ci' AFTER `status_id`;"));

        });
	}

}
