<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcosOfficesIds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->integer('sub_office_id')->after('ab_subcontractor_id');
			$table->integer('recipient_office_id')->after('ab_recipient_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->dropColumn('sub_office_id');
			$table->dropColumn('recipient_office_id');
		});
	}

}
