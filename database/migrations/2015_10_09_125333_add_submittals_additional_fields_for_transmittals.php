<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmittalsAdditionalFieldsForTransmittals extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submittals', function($table)
		{
			$table->integer('recipient_id')->after('sub_id');
			$table->integer('user_id')->after('comp_id');
			$table->string('subject')->after('name');
			$table->string('sent_via')->after('subject');
			$table->string('submitted_for')->after('sent_via');
			$table->string('submittal_type')->after('submitted_for');
			$table->integer('quantity')->after('submittal_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submittals', function($table)
		{
			$table->dropColumn('recipient_id');
			$table->dropColumn('user_id');
			$table->dropColumn('subject');
			$table->dropColumn('sent_via');
			$table->dropColumn('submitted_for');
			$table->dropColumn('submittal_type');
			$table->dropColumn('quantity');
		});
	}

}
