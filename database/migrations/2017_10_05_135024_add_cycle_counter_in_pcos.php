<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCycleCounterInPcos extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->integer('cycle_counter')->after('note')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pcos_subcontractors_recipients', function($table)
		{
			$table->dropColumn('cycle_counter');
		});
	}

}
