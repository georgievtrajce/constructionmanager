@extends('layouts.public-master')

@section('content')

<div class="cm-login" data-type="alternative-page">	
	<form class="cm-login-form" role="form" method="POST" action="{{URL('/auth/login')}}">

		<div class="row">
			<div class="col-sm-12">
				<div class="cm-login-logo">
					<img src="/img/login-logo.png" class="img-responsive" alt="logo">
				</div>
				<div class="cm-login-controls">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						<input type="email" class="form-control" placeholder="{{trans('labels.login.email')}}" name="email" value="{{ old('email') }}">
					</div>

					<div class="form-group">
						<input type="password" class="form-control" placeholder="{{trans('labels.login.password')}}" name="password">
					</div>

					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" name="remember"> {{trans('labels.login.remember_me')}}
							</label>
						</div>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							{{trans('labels.login.login')}}
						</button>
					</div>
					<div class="form-group">
						<a href="{{URL('/password/email')}}">{{trans('labels.login.forgot_your_password')}}</a>
					</div>
					<div class="form-group">
						{{trans('labels.login.dont_have_account')}} <a href="{{URL('auth/register')}}">{{trans('labels.login.sign_up')}}</a>
					</div>
				</div>
				@if (Session::has('flash_notification.message'))
				<div class="alert alert-{{ Session::get('flash_notification.level') }}">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					{{ Session::get('flash_notification.message') }}
				</div>
				@endif
				@if (count($errors) > 0)
				<div class="cm-login-error alert alert-danger">
					<p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
			</div>
		</div>

		
	</form>
</div>

@endsection
