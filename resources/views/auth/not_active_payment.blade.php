@extends('layouts.public-master')

@section('content')
    <div class="cms-page">
        <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default" style="margin-top: 30px;">
                    <div class="panel-heading">{{trans('labels.company_profile.subscription_levels')}}</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                        @endif
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-12" id="inline_content">
                                        <div class="cm-spacer-xs"></div>
                                        <ul class="cm-pricing cf">
                                            <li class="cm-pricing-item four-table {{ (Input::get('level') == 1) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-one') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 1 ? 'featured' : '' }}">
                                                <div class="cm-pricing-heading">
                                                    <div class="cm-pricing-plan">
                                                        Level 1
                                                    </div>
                                                    <div class="cm-pricing-price">
                                                    <span>
                                                        <sup>$</sup>@if($firstSubscription->amount > 0){{number_format((number_format($firstSubscription->amount,2) / 12),0)}}@endif<sub></sub>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="cm-pricing-features">
                                                    <ul>
                                                        <li>
                                                            Monthly Price / Billed Annually
                                                        </li>
                                                        <li>
                                                            All Features Included
                                                        </li>
                                                        <li>
                                                            Unlimited Users
                                                        </li>
                                                        <li>
                                                            Unlimited Projects
                                                        </li>
                                                        <li>
                                                            Data Storage {{CustomHelper::formatByteSizeUnits($firstSubscription->storage_limit)}}
                                                        </li>
                                                    </ul>
                                                </div>
                                                <a href="{{URL('payment/hosted/'.Auth::user()->comp_id.'/'.Config::get('subscription_levels.level-one').'/'.Auth::user()->email.'/'.Auth::user()->company->name)}}" class="">
                                                    <div class="cm-pricing-button">
                                                        <label class="btn btn-primary">
                                                            <span>Choose Plan</span>
                                                        </label>
                                                    </div></a>
                                            </li>
                                            <li class="cm-pricing-item four-table {{ (Input::get('level') == 2) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-two') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 2 ? 'featured' : '' }}">
                                                <div class="cm-pricing-heading">
                                                    <div class="cm-pricing-plan">
                                                        Level 2
                                                    </div>
                                                    <div class="cm-pricing-price">
                                                    <span>
                                                        <sup>$</sup>@if($secondSubscription->amount > 0){{number_format((number_format($secondSubscription->amount,2) / 12),0)}}@endif<sub></sub>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="cm-pricing-features">
                                                    <ul>
                                                        <li>Monthly Price / Billed Annually</li>
                                                        <li>All Features Included</li>
                                                        <li>Unlimited Users</li>
                                                        <li>Unlimited Projects</li>
                                                        <li>Data Storage {{CustomHelper::formatByteSizeUnits($secondSubscription->storage_limit)}}</li>
                                                    </ul>
                                                </div>
                                                <a href="{{URL('payment/hosted/'.Auth::user()->comp_id.'/'.Config::get('subscription_levels.level-two').'/'.Auth::user()->email.'/'.Auth::user()->company->name)}}">
                                                    <div class="cm-pricing-button">
                                                        <label class="btn btn-primary">
                                                            <span>Choose Plan</span>
                                                        </label>
                                                    </div></a>
                                            </li>

                                            <li class="cm-pricing-item four-table {{ (Input::get('level') == 3) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-three') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 3 ? 'featured' : '' }}">
                                                <div class="cm-pricing-heading">
                                                    <div class="cm-pricing-plan">
                                                        Level 3
                                                    </div>
                                                    <div class="cm-pricing-price">
                                                    <span>
                                                        <sup>$</sup>@if($thirdSubscription->amount > 0){{number_format(($thirdSubscription->amount / 12),0)}}@endif<sub></sub>
                                                    </span>
                                                    </div>
                                                </div>
                                                <div class="cm-pricing-features">
                                                    <ul>
                                                        <li>Monthly Price / Billed Annually</li>
                                                        <li>All Features Included</li>
                                                        <li>Unlimited Users</li>
                                                        <li>Unlimited Projects</li>
                                                        <li>Data Storage {{CustomHelper::formatByteSizeUnits($thirdSubscription->storage_limit)}}</li>
                                                    </ul>
                                                </div>
                                                <a href="{{URL('payment/hosted/'.Auth::user()->comp_id.'/'.Config::get('subscription_levels.level-three').'/'.Auth::user()->email.'/'.Auth::user()->company->name)}}">
                                                    <div class="cm-pricing-button">
                                                        <label class="btn btn-primary">
                                                            <span>Choose Plan</span>
                                                        </label>
                                                    </div></a>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="https://pcicompliancemanager.com/safemaker/Safemaker/cardJs?p=bacbaj"
            type="text/javascript"></script>

@endsection