@extends('layouts.public-master')

@section('content')
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="cms-page">
	

	<div class="container container-inset">

		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="row">
			<div class="col-sm-12">

				<form class="form-horizontal" role="form" method="POST" action="{{URL('/auth/register')}}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="referral_token" value="{{ Request::segment(3)}}">
					<input type="hidden" name="sub_token" value="{{ Request::segment(4)}}">
					<input type="hidden" name="projectCompanyId" value="{{ Input::get('projectCompanyId')}}">
					<input type="hidden" name="bidderId" value="{{ Input::get('bidderId')}}">

					<fieldset>
						<legend>Sign In Info</legend>

						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.login.email')}}</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.login.password')}}</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.user_profile.confirm_password')}}</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>Company Info</legend>
						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.company.name')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
							</div>
						</div>
					</fieldset>
					<div class="addresses-main-cont">
						<fieldset class="address-container">
							<legend>{{trans('labels.register.company_address')}}</legend>
							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_name')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_addr_office_title_req" value="{{ old('company_addr_office_title_req') }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.Address')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_addr_street_req" value="{{ old('company_addr_street_req') }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.city')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_addr_town_req" value="{{ old('company_addr_town_req') }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.state')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_addr_state_req" value="{{ old('company_addr_state_req') }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.zip')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_addr_zip_code_req" value="{{ old('company_addr_zip_code_req') }}">
								</div>
							</div>

							<!--<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="button" class="btn btn-primary btn-sm js-add-address">
										 {{--trans('labels.company_profile.add_address')--}}
									</button>
								</div>
							</div>-->
						</fieldset>
						@if(old('company_add_addr'))
						<?php
$company_add_addr = old('company_add_addr');



?>

						@for($i=0; $i<sizeof($company_add_addr); $i++)
						@if($company_add_addr[$i]['company_addr_office_title'] != '' || $company_add_addr[$i]['company_addr_number'] != '' || $company_add_addr[$i]['company_addr_street'] != '' || $company_add_addr[$i]['company_addr_town'] != '' || $company_add_addr[$i]['company_addr_state'] != '' || $company_add_addr[$i]['company_addr_zip_code'] != '')

						<fieldset class="address-container address-additional-container">
							<legend>{{trans('labels.register.company_address')}}</legend>
							<div class="form-group">
								<label class="col-md-4 control-label">{{trans('labels.address_book.office_name')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_add_addr[{{$i}}][company_addr_office_title]" value="{{ $company_add_addr[$i]['company_addr_office_title'] }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.Address')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_add_addr[{{$i}}][company_addr_street]" value="{{ $company_add_addr[$i]['company_addr_street'] }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.city')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_add_addr[{{$i}}][company_addr_town]" value="{{ $company_add_addr[$i]['company_addr_town'] }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.state')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_add_addr[{{$i}}][company_addr_state]" value="{{ $company_add_addr[$i]['company_addr_state'] }}">
								</div>
							</div>

							<div class="form-group">
								<label class="cm-control-required col-md-4 control-label">{{trans('labels.zip')}}</label>
								<div class="col-md-6">
									<input type="text" class="form-control" name="company_add_addr[{{$i}}][company_addr_zip_code]" value="{{ $company_add_addr[$i]['company_addr_zip_code'] }}">
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="button" class="btn btn-danger js-remove-address">
										{{trans('labels.register.remove_address')}}
									</button>
								</div>
							</div>
						</fieldset>

						@endif
						@endfor
						@endif
					</div>
					<fieldset>
						<legend>{{trans('labels.register.contact_person')}}</legend>
						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.name')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="admin_name" value="{{ old('admin_name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.title')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="admin_title" value="{{ old('admin_title') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_phone')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="admin_office_phone" value="{{ old('admin_office_phone') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('labels.address_book.cell_phone')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="admin_cell_phone" value="{{ old('admin_cell_phone') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">{{trans('labels.address_book.fax')}}</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="admin_fax" value="{{ old('admin_fax') }}">
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend>
							{{trans('labels.company_profile.subscription_type')}}
						</legend>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4" id="inline_content">
								<h4>Choose Subscription Level:</h4>
								<div class="cm-spacer-xs"></div>
								@if(!empty(Request::segment(4)))
									<div class="radio cm-radio-inline">
										<label>
											<input type="radio" id="subs0" name="subscription_type" value="5" />
											Level 0 (free)
										</label>
									</div>
                                                                <br>
                                                                <br>
								@endif
								<ul class="cm-pricing cf">
				<li class="cm-pricing-item four-table {{ (Input::get('level') == 1) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-one') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 1 ? 'featured' : '' }}">
					<div class="cm-pricing-heading">
						<div class="cm-pricing-plan">
							Level 1
						</div>
						<div class="cm-pricing-price">
							<span>
								<sup>$</sup>@if($firstSubscription->amount > 0){{number_format((number_format($firstSubscription->amount,2) / 12),0)}}@endif<sub></sub>
							</span>
						</div>
					</div>
					<div class="cm-pricing-features">
						<ul>
							<li>
								Monthly Price / Billed Annually
							</li>
							<li>
								All Features Included
							</li>
							<li>
								Unlimited Users
							</li>
							<li>
								Unlimited Projects
							</li>
							<li>
								Data Storage {{CustomHelper::formatByteSizeUnits($firstSubscription->storage_limit)}}
							</li>
						</ul>
					</div>
					<div class="cm-pricing-button">				
										
						<label class="btn btn-primary">
							<span>Choose Plan</span>
							<input type="radio" id="subs1" name="subscription_type" value="1" {{ (Input::get('level') == 1) ? 'checked' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-one') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 1 ? 'checked' : '' }} />
						</label>
					</div>
				</li>

				<li class="cm-pricing-item four-table {{ (Input::get('level') == 2) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-two') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 2 ? 'featured' : '' }}">
					<div class="cm-pricing-heading">
						<div class="cm-pricing-plan">
							Level 2
						</div>
						<div class="cm-pricing-price">
							<span>
								<sup>$</sup>@if($secondSubscription->amount > 0){{number_format((number_format($secondSubscription->amount,2) / 12),0)}}@endif<sub></sub>
							</span>
						</div>
					</div>
					<div class="cm-pricing-features">
						<ul>
							<li>Monthly Price / Billed Annually</li>
							<li>All Features Included</li>
							<li>Unlimited Users</li>
							<li>Unlimited Projects</li>
							<li>Data Storage {{CustomHelper::formatByteSizeUnits($secondSubscription->storage_limit)}}</li>
						</ul>
					</div>
					<div class="cm-pricing-button">				
										
							<label class="btn btn-primary">
							<span>Choose Plan</span>
										<input type="radio" id="subs2" name="subscription_type" value="2" {{ (Input::get('level') == 2) ? 'checked' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-two') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 2 ? 'checked' : '' }} />
					</labels>
					</div>
				</li>

				<li class="cm-pricing-item four-table {{ (Input::get('level') == 3) ? 'featured' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-three') ? 'featured' : '' }} {{ old('subscription_type') && old('subscription_type') == 3 ? 'featured' : '' }}">
					<div class="cm-pricing-heading">
						<div class="cm-pricing-plan">
							Level 3
						</div>
						<div class="cm-pricing-price">
							<span>
								<sup>$</sup>@if($thirdSubscription->amount > 0){{number_format(($thirdSubscription->amount / 12),0)}}@endif<sub></sub>
							</span>
						</div>
					</div>
					<div class="cm-pricing-features">
						<ul>
							<li>Monthly Price / Billed Annually</li>
						<li>All Features Included</li>
						<li>Unlimited Users</li>
						<li>Unlimited Projects</li>
						<li>Data Storage {{CustomHelper::formatByteSizeUnits($thirdSubscription->storage_limit)}}</li>
						</ul>
					</div>
					<div class="cm-pricing-button">				
										
							<label class="btn btn-primary">
							<span>Choose Plan</span>
						<input type="radio" id="subs3" name="subscription_type" value="3" {{ (Input::get('level') == 3) ? 'checked' : '' }} {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-three') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 3 ? 'checked' : '' }} />
				</label>
					</div>
				</li>

			</ul>

								<div class="cm-spacer-xs"></div>
								<div class="checkbox ">
									<label>
										<input type="checkbox" name="terms_agree" {{ old('terms_agree') ? 'checked' : '' }}> {{trans('labels.terms_agree')}} <a href="{{URL('/terms-of-use')}}">{{trans('labels.terms_and_conditions')}}</a> {{trans('labels.and')}} <a href="{{URL('/privacy-policy')}}">{{trans('labels.privacy_policy')}}</a>
									</label>
								</div>

							</div>
						</div>
					</fieldset>
					@if(env('GOOGLE_RECAPTCHA_KEY'))
						<div class="cm-spacer-xs"></div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
									<div class="g-recaptcha"
										 data-sitekey="{{env('GOOGLE_RECAPTCHA_KEY')}}">
									</div>
							</div>
						</div>
					@endif
					<div class="cm-spacer-xs"></div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-4">
							<button type="submit" class="btn btn-success pull-right">
								{{trans('labels.master_layout.register')}}
							</button>
						</div>
					</div>
				</form>

			</div>
		</div>

	</div>
</div>
<script id="company-address-template" type="text/template">
	<fieldset style="display:none;" class="address-container">
		<legend>Company Address</legend>
		<div class="form-group">
			<label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_name')}}</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="company_add_addr[<%= counter %>][company_addr_office_title]">
			</div>
		</div>
		<div class="form-group">
			<label class="cm-control-required col-md-4 control-label">{{trans('labels.Address')}}</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="company_add_addr[<%= counter %>][company_addr_street]">
			</div>
		</div>
		<div class="form-group">
			<label class="cm-control-required col-md-4 control-label">{{trans('labels.city')}}</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="company_add_addr[<%= counter %>][company_addr_town]">
			</div>
		</div>
		<div class="form-group">
			<label class="cm-control-required col-md-4 control-label">{{trans('labels.state')}}</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="company_add_addr[<%= counter %>][company_addr_state]">
			</div>
		</div>
		<div class="form-group">
			<label class="cm-control-required col-md-4 control-label">{{trans('labels.zip')}}</label>
			<div class="col-md-6">
				<input type="text" class="form-control" name="company_add_addr[<%= counter %>][company_addr_zip_code]">
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6 col-md-offset-4">
				<button type="button" class="btn btn-sm btn-danger js-remove-address">{{trans('labels.register.remove_address')}}</button>
			</div>
		</div>
	</fieldset>
</script>

<script src="https://pcicompliancemanager.com/safemaker/Safemaker/cardJs?p=bacbaj"
type="text/javascript"></script>

<script>
	var column = $('.cm-pricing-item');
	column.click(function() {

		$('input[name="subscription_type"]', this).prop("checked",true);
		column.removeClass('featured');
		$(this).addClass('featured');
	});
</script>
@endsection