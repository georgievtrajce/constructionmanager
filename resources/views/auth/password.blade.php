@extends('layouts.public-master')
@section('content')
<div class="cms-page">
	<div class="container">
	<div class="col-md-6 col-md-offset-3">
		<div class="row">
			<div class="col-md-5">
				<header class="cm-heading">
					{{trans('labels.login.reset_password')}}
				</header>
			</div>
			<div class="col-md-7">
				<div class="cm-btn-group cm-pull-right cf">
				</div>
			</div>
		</div>
		<style type="text/css">
		.cm-heading{
			font-weight: normal;
		font-size: 25px;
		margin: 0 0 1em 0;
		color: #37474f;
		text-shadow: rgba(0, 0, 0, 0.15) 0 0 1px;
		}
		</style>
		<div class="row">
			<div class="col-md-12">
				<div class="panel ">
					<div class="panel-body">
						@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif
						@if (count($errors) > 0)
						<div class="alert alert-danger">
							<p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif
						<form class="form" role="form" method="POST" action="{{URL('/password/email')}}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<label>{{trans('labels.login.email')}}</label>

								<input type="email" class="form-control" name="email" value="{{ old('email') }}">

							</div>
							<div class="form-group">

								<button type="submit" class="btn btn-primary pull-right">
								{{trans('labels.login.send_reset_link')}}
								</button>

							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
@endsection