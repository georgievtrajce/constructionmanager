<form action="https://gateway.helcim.com/hosted/" method="POST" name="frm">
    <!-- Basic Fields -->
    <input type="hidden" name="merchantId" value="{{ $merchantId }}">
    <input type="hidden" name="token" value="{{ $token }}">
    <input type="hidden" name="amount" value="{{ $amount }}">
    <input type="hidden" name="billingName" value="{{ $companyName }}">
    <input type="hidden" name="companyId" value="{{ $companyId }}">
    <input type="hidden" name="companyName" value="{{ $companyName }}">
    <input type="hidden" name="subscriptionId" value="{{ $subscriptionId }}">
    <input type="hidden" name="projectCompanyId" value="{{ $projectCompanyId }}">
    <input type="hidden" name="bidderId" value="{{ $bidderId }}">
    <input type="hidden" name="email" value="{{ $email }}">
    <input type="hidden" name="referralToken" value="{{ $referralToken }}">
    <input type="hidden" name="subToken" value="{{ $subToken }}">
    <input type="hidden" name="paymentType" value="{{ $paymentType }}">
</form>

<script language="JavaScript">
    document.frm.submit();
</script>