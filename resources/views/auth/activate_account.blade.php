@extends('layouts.public-master')

@section('content')
    <div class="cms-page">
        <div class="container">
        <div class="row">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
        </div>
    </div>
    </div>
@endsection
