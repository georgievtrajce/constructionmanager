@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-12">
                        <p>You have successfully registered to the Cloud PM web application.</p>
                        <p>Please look for the Registration Confirmation Email in your Inbox.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection