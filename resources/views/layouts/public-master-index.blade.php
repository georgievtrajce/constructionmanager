<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/additional.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74036441-1', 'auto');
    ga('send', 'pageview');

</script>
<div class="cms-loader">
    <img src="{{URL::asset('images/logo.svg')}}" alt="Loader Logo">
</div>
<div class="cms-page-main home">
    <header class="cms-header" id="home">
        <div class="cms-header-wrap transparent">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <a href="#" class="cms-header-logo">
                            <img src="{{URL::asset('images/logo.png')}}" alt="Logo">
                        </a>
                    </div>
                    <div class="col-xs-10">
                        <a class="fa fa-bars cms-header-navigation-trigger"></a>
                        <ul class="cms-header-navigation">
                            <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#features">Features</a></li>
                            <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#pricing-tables">Pricing</a></li>
                            <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#contact">Contact</a></li>
                            <li class="cms-header-navigation-link"><a href="{{URL('auth/login')}}">Login</a></li>
                            <li class="cms-header-navigation-link"><a href="{{URL('auth/register')}}">Register</a></li>
                            <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/blog/">Blog</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="cms-header-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7">
                        <h2 class="cms-header-content-title">
                            Cost effective cloud-based solution for Construction Project Management and Document Control
                        </h2>
                        <!--<p class="cms-header-content-copy" style="font-size: 16px;">
                            With our web application every member of the Project Team<br />will always have the latest Project Documents & Files
                        </p>-->
                        <a href="{{URL('auth/register')}}" class="btn btn-lg btn-primary">REGISTER NOW</a>
                    </div>
                    <div class="col-md-6 col-sm-5">
                        <a class="cms-header-content-video" data-toggle="modal" data-target="#video-modal">
                            <img src="{{URL::asset('images/video.jpg')}}" class="img-responsive img-thumbnail" alt="video">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

</div>

<footer class="cms-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <ul class="cms-footer-list">
                    <li><a href="https://cloud-pm.com/refund-policy/">Refund policy </a></li>
                    <li><a href="https://cloud-pm.com/privacy-policy/">Privacy policy</a></li>
                    <li><a href="https://cloud-pm.com/terms-of-use/">Terms of use</a></li>
                </ul>
            </div>
            <div class="col-sm-6">
                <span class="cms-footer-copy">© 2015 Construction Master Solutions, LLC</span>
            </div>
        </div>
    </div>
</footer>


<!-- Modal -->
<div class="modal fade" id="video-modal" tabindex="-1" role="dialog" aria-labelledby="videoModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"></h4>
            </div>
            <div class="modal-body">

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/KxufHxA2xYs" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/app.js')}}"></script>
<script>
    $(document).ready(function(){
        transparentHeader();
    })

</script>
</body>
</html>
