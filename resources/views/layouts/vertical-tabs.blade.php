@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">@yield('title')</header>
        </div>
    </div>

    @yield('tabs')
   
        
        <div class="panel cm-panel-alt">
            <div class="panel-body">
                <div class="col-md-2">
                    @yield('vertical-tabs')
                </div>
                <div class="col-md-10">
                    @yield('tabsContent')
                </div>
            </div>
        </div>
</div>
@endsection