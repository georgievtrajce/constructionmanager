@extends('layouts.master')
@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-12">
                    @yield('title')               
            </div>
        </div>

        @yield('tabs')
        @yield('tabsContent')

    </div>
@endsection