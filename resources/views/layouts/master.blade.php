<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="google-site-verification" content="HKS2YAXsZy2TsKyDonJAvCPwbFuz_eD6t4CG8mVHKeY" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>

    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">

    <link href={{URL::asset('css/app.css')}} rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700,400,600" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Scripts -->
    <script>
        var site_url = '{{URL()}}';
        var aws_url = '{{Config::get('constants.s3_base_url').'/'.Config::get('filesystems.disks.s3.bucket').'/'}}';
        var companyId = '{{(!empty(Auth::user()))?\Illuminate\Support\Facades\Auth::user()->comp_id:0}}';
        var cloudFrontUrl = '{{env('AWS_CLOUD_FRONT')}}';
        var tinyImageUrl = '{{URL('resize-image-tiny')}}'
        var identityPool = '{{env('IDENTITY_POOL')}}';
    </script>

</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74036441-1', 'auto');
    ga('send', 'pageview');

</script>
    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid container-inset">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{URL('/')}}">
                    <img class="pull-right" src="{{URL('/images/logo.png')}}" height="40px">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @if (Auth::guest())
                        <li><a href="{{URL('/products-and-pricing')}}">{{trans('labels.master_layout.products_and_pricing')}}</a></li>
                        <li><a href="{{URL('/terms-of-use')}}">{{trans('labels.master_layout.terms_of_use')}}</a></li>
                        <li><a href="{{URL('/refund-policy')}}">{{trans('labels.master_layout.refund_policy')}}</a></li>
                        <li><a href="{{URL('/privacy-policy')}}">{{trans('labels.master_layout.privacy_policy')}}</a></li>
                        <li><a href="{{URL('/contact-us')}}">{{trans('labels.master_layout.contact_us')}}</a></li>
                    @else
                    @if (!Auth::user()->hasRole(Config::get('constants.roles.super_admin')))
                        <li class="@if(Request::segment(1) == 'home') {{'active'}} @endif"><a href="{{URL('/home')}}">{{trans('labels.master_layout.dashboard')}}</a></li>
                        <li><a href="https://help.cloud-pm.com/help">{{trans('labels.master_layout.help')}}</a></li>
                    @endif
                    @endif
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a href="{{URL('auth/login')}}">{{trans('labels.master_layout.login')}}</a></li>
                        <li><a href="{{URL('auth/register')}}">{{trans('labels.master_layout.register')}}</a></li>
                    @else
                        @if(Auth::user()->hasRole(Config::get('constants.roles.super_admin')))
                            <li class="@if(Request::segment(1) == 'subscription-levels'){{'active'}}@endif"><a href="{{URL('subscription-levels')}}">{{trans('labels.company_profile.subscription_levels')}}</a></li>
                            <li class="@if(Request::segment(1) == 'transactions'){{'active'}}@endif"><a href="{{URL('transactions')}}">{{trans('labels.transactions')}}</a></li>
                            <li class="@if(Request::segment(1) == 'masterformat'){{'active'}}@endif"><a href="{{URL('masterformat')}}">{{trans('labels.master_format')}}</a></li>
                            <li class="@if(Request::segment(1) == 'list-companies'){{'active'}}@endif"><a href="{{URL('list-companies')}}">{{trans('labels.master_layout.companies_and_users')}}</a></li>
                        @endif
                        @if(count(Auth::user()->company) && (((Auth::user()->hasRole(['Company Admin', 'Project Admin'])) || (Permissions::can('read', 'address-book')))))
                            <li class="@if(Request::segment(1) == 'address-book') {{'active'}} @endif"><a href="{{URL('address-book')}}">{{trans('labels.address_book_title')}}</a></li>
                        @endif
                        @if(!Auth::user()->hasRole(Config::get('constants.roles.super_admin')))
                            <li class="@if(Request::segment(1) == 'projects') {{'active'}} @endif"><a href="{{URL('projects')}}">{{trans('labels.projects')}}</a></li>
                        @endif
                        @if(!Auth::user()->hasRole(Config::get('constants.roles.super_admin')))
                            <li class="@if(Request::segment(1) == 'tasks') {{'active'}} @endif"><a href="{{URL('tasks')}}">{{trans('labels.tasks.tasks')}}</a></li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle cm-user-dropdown" data-toggle="dropdown" role="button" aria-expanded="false">
                                <span class="cm-user-info">
                                    {{ Auth::user()->name }}
                                    @if(count(Auth::user()->company))
                                        <span class="cm-user-company">{{ Auth::user()->company->name }}</span>
                                    @else
                                        <span class="cm-user-company">{{trans('labels.super_admin')}}</span>
                                    @endif
                                </span>
                         <!--        @if(!count(Auth::user()->company) || is_null(Auth::user()->company->logo))
                                    <img class="cm-user-avatar" src="{{URL('/img/cm-img-user.png')}}" alt="">
                                @else
                                    <img class="cm-user-avatar" src="{{Config::get('constants.s3_base_url').'/'.Config::get('filesystems.disks.s3.bucket').'/'.Auth::user()->company->logo}}" alt="">
                                @endif -->
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                @if(count(Auth::user()->company) && Auth::user()->hasRole('Company Admin'))
                                    <li><a href="{{URL('manage-users')}}">{{trans('labels.master_layout.manage_users')}}</a></li>
                                @endif
<!--                                @if(count(Auth::user()->company) && Auth::user()->hasRole(['Company Admin','Project Admin']))
                                    <li><a href="{{URL('referral/invite')}}">{{trans('labels.master_layout.invite_friend')}}</a></li>
                                @endif-->
                                @if(Auth::user()->hasRole('Company Admin'))
                                    <li><a href="{{URL('company-profile/'.Auth::user()->company->id)}}">{{trans('labels.master_layout.company_profile')}}</a></li>
                                @endif
                                <li><a href="{{URL('/user-profile')}}">{{trans('labels.master_layout.user_profile')}}</a></li>
                                <li><a href="{{URL('auth/logout')}}">{{trans('labels.master_layout.logout')}}</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="cm-page-content">
        @yield('content')
    </div>
    @include('popups.upload_image_s3_popup')

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.2.0/tinymce.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://sdk.amazonaws.com/js/aws-sdk-2.2.10.min.js"></script>
    <script src="{{URL::asset('js/all.js')}}"></script>
    @yield('extra-js')
</body>
</html>
