<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/additional.css')}}">
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400italic,700,700italic,300italic,300' rel='stylesheet' type='text/css'>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="{{Request::segment(2)}}">
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-74036441-1', 'auto');
    ga('send', 'pageview');

</script>
    <div class="cms-page-main">
        <header class="cms-header">
            <div class="cms-header-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-2">
                            <a href="/" class="cms-header-logo">
                                <img src="{{URL::asset('images/logo.png')}}" alt="Logo">
                            </a>
                        </div>
                        <div class="col-xs-10">
                            <a class="fa fa-bars cms-header-navigation-trigger"></a>
                            <ul class="cms-header-navigation">
                                <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#features">Features</a></li>
                                <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#pricing">Pricing</a></li>
                                <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/#contact">Contact</a></li>
                                <li class="cms-header-navigation-link"><a href="{{URL('auth/login')}}">Login</a></li>
                                <li class="cms-header-navigation-link"><a href="{{URL('auth/register')}}">Register</a></li>
                                <li class="cms-header-navigation-link"><a href="https://cloud-pm.com/blog/">Blog</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')

    </div>

    <footer class="cms-footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="cms-footer-list">
                        <li><a href="https://cloud-pm.com/refund-policy/">Refund policy </a></li>
                        <li><a href="https://cloud-pm.com/privacy-policy/">Privacy policy</a></li>
                        <li><a href="https://cloud-pm.com/terms-of-use/">Terms of use</a></li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <span class="cms-footer-copy">© 2015 Construction Master Solutions, LLC</span>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
    <script src="{{URL::asset('js/app.js')}}"></script>
    <script src="{{URL::asset('js/underscore.js')}}"></script>
    <script src="{{URL::asset('js/cm.toggleTemplate.js')}}"></script>
    <script src="{{URL::asset('js/register.js')}}"></script>
</body>
</html>
