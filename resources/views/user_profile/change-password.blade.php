@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.user_profile.change_your_password')}}
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <!--         <div class="row">
                        <div class="form-group">
                            <div class="col-md-3">
                                <a href="{{URL('/user-profile')}}" class="btn cm-btn-secondary btn-sm">{{'<< '.trans('labels.user_profile.back_to_profile')}}</a>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form" class="form" action="{{URL('/change-password')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.user_profile.old_password')}}</label>

                                        <input type="password" class="form-control" name="old_password">

                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.user_profile.new_password')}}</label>

                                        <input type="password" class="form-control" name="password">

                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.user_profile.confirm_password')}}</label>

                                        <input type="password" class="form-control" name="password_confirmation">

                                </div>
                                <div class="form-group">

                                        <button type="submit" class="btn btn-success pull-right">
                                        {{trans('labels.save')}}
                                        </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection