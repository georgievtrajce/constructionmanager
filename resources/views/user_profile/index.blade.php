@extends('layouts.master') @section('content')
<div class="container">
    <!--<div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.user_profile.edit_profile')}}
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>-->
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <form class="form form-horizontal" role="form" action="{{URL('/user-profile')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <h3>{{trans('labels.user_profile.edit_profile')}}</h3>
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.name')}}</label>
                                    <div class="col-md-6">
                                        <input {{(!Auth::user()->hasRole('Company Admin') && !Auth::user()->hasRole('Super
                                        Admin'))?'readonly':''}} type="text" class="form-control" name="name" value="{{Auth::user()->name
                                        }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.email')}}</label>
                                    <div class="col-md-6">
                                        <input {{(!Auth::user()->hasRole('Company Admin') && !Auth::user()->hasRole('Super
                                        Admin'))?'readonly':''}} type="text" class="form-control" name="email" value="{{Auth::user()->email
                                        }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.title')}}</label>
                                    <div class="col-md-6">
                                        <input {{(!Auth::user()->hasRole('Company Admin') && !Auth::user()->hasRole('Super
                                        Admin'))?'readonly':''}} type="text" class="form-control" name="title" value="{{Auth::user()->title
                                        }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_phone')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="office_phone" value="{{Auth::user()->office_phone }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.address_book.cell_phone')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="cell_phone" value="{{Auth::user()->cell_phone }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.address_book.fax')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="fax" value="{{Auth::user()->fax }}">
                                    </div>
                                </div>
                                @if(!Auth::user()->hasRole('Super Admin')) @if(Auth::user()->hasRole('Company Admin') && count(Auth::user()->company->addresses)
                                == 0)
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.user_profile.office_address')}}</label>
                                    <span>{{trans('labels.user_profile.address_warning')}}</span>
                                </div>
                                @elseif(Auth::user()->hasRole('Company User') && count(Auth::user()->company->addresses) == 0)
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.user_profile.office_address')}}</label>
                                    <span>{{trans('labels.user_profile.address_user_warning')}}</span>
                                </div>
                                @else
                                <div class="form-group">
                                    <label class="col-md-4 control-label ">{{trans('labels.user_profile.office_address')}}</label>
                                    <div class="col-md-6">
                                        <select name="user_address_office">
                                                            @foreach(Auth::user()->company->addresses as $companyAddress)
                                                            <option {{ (isset(Auth::user()->address_id) && Auth::user()->address_id == $companyAddress->id) ? 'selected' : '' }} value="{{ $companyAddress->id }}">{{ isset($companyAddress->office_title) ? $companyAddress->office_title : $companyAddress->city.', '.$companyAddress->state }}</option>
                                                            @endforeach
                                                        </select>
                                    </div>
                                </div>
                                @endif @endif
                            </div>
                            <div class="col-md-6 col-md-offset-4 mt20">
                                <button type="submit" class="btn btn-success pull-right">
                                            {{trans('labels.update')}}
                                            </button>
                                <a href="{{URL('/change-password')}}" class="btn cm-btn-secondary">{{trans('labels.user_profile.change_password')}}</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection