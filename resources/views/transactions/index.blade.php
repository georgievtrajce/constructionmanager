@extends('layouts.tabs')
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (sizeof($transactions))
                <div class="row">
                    <div class="col-sm-5">
                        {!! Form::open(['method'=>'GET','url'=>'transactions']) !!}
                        <div class="col-sm-12">
                            <h3>{{trans('labels.sort')}}:</h3>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="form-group">
                                    {!! Form::label('sort', trans('labels.sort_by').':') !!} <br />
                                    {!! Form::select('sort', array('id' => trans('labels.date'), 'company_name' => trans('labels.Company'), 'subscription_type_name' => trans('labels.subscription_level')), Input::get('sort')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('order', trans('labels.order_by').':') !!} <br />
                                {!! Form::select('order', array('asc' => trans('labels.ascending'), 'desc' => trans('labels.descending')), Input::get('order')) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="pull-right">
                    <?php echo $transactions->appends([
                            'sort'=>Input::get('sort'),
                            'order'=>Input::get('order'),
                    ])->render(); ?>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('labels.Company')}}</th>
                            <th>{{trans('labels.address.plural')}}</th>
                            <th>{{trans('labels.subscription_level')}}</th>
                            <th>{{trans('labels.email')}}</th>
                            <th>{{trans('labels.amount')}}</th>
                            <th>{{trans('labels.created_at')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transactions as $transaction)
                            <tr>
                                <td>@if(!is_null($transaction->company)){{$transaction->company->name}}@endif</td>
                                <td>
                                    @if(!is_null($transaction->company))
                                        @if(count($transaction->company->addresses))
                                            @foreach($transaction->company->addresses as $address)
                                                <p>
                                                    <i>{{$address->office_title}}</i><br>
                                                    {{$address->street.', '.$address->city.' '.$address->state}}<br>
                                                    {{$address->zip}}
                                                </p>
                                            @endforeach
                                        @endif
                                    @endif
                                </td>
                                <td>@if(!is_null($transaction->subscription_type)){{$transaction->subscription_type->name}}@endif</td>
                                <td>{{$transaction->email}}</td>
                                <td>{{'$'.$transaction->amount}}</td>
                                <td>{{date("m/d/Y", strtotime($transaction->created_at))}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <p>{{trans('labels.no_records')}}</p>
            @endif
        </div>
    </div>
@endsection