@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading">
                {{trans('labels.masterformat.edit')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    {!! Form::open(['method'=> 'PUT', 'url'=>URL('masterformat/'.$item->id)]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.title')) !!}
                                {!! Form::text('title', $item->title, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('number', trans('labels.number')) !!}
                                {!! Form::text('number', $item->number, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('version', trans('labels.version')) !!}
                                {!! Form::select('master_format_type', $masterFormatTypes, $item->master_format_type_id, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group pull-right">
                                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                            @include('errors.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
