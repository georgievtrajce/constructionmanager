@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading">
                {{trans('labels.masterformat.edit_version')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    {!! Form::open(['method'=> 'POST', 'url'=>URL('masterformattype/edit/'.$item->id)]) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.title')) !!}
                                {!! Form::text('title', $item->name, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('number', trans('labels.year')) !!}
                                <select class="form-control" id="year" name="year">
                                    @for($i = 2010; $i < 2030; $i++)
                                        <option @if($i == $item->year) selected @endif value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                            @include('errors.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
