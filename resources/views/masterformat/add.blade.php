@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading">
                {{trans('labels.masterformat.add_new')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::open(['method'=> 'POST', 'url'=>'masterformat']) !!}
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.title')) !!}
                                {!! Form::text('title', Input::get('title'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('number', trans('labels.number')) !!}
                                {!! Form::text('number', Input::get('number'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('version', trans('labels.version')) !!}
                                {!! Form::select('master_format_type', $masterFormatTypes, Input::get('master_format_type'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group pull-right">
                                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                            @include('errors.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
