@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading">
                {{trans('labels.masterformat.add_new_version')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::open(['method'=> 'POST', 'url'=>'masterformattype/create']) !!}
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.title')) !!}
                                {!! Form::text('title', Input::get('title'), ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('number', trans('labels.year')) !!}
                                <select class="form-control" id="year" name="year">
                                    @for($i=2010;$i<2030;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="form-group pull-right">
                                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                            @include('errors.list')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
