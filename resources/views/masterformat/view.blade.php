@extends('layouts.master')
@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-10">
                        <h3 class="cm-heading">
                        {{trans('labels.masterformat.master_format_list_items')}}</div>
                    </h3>
                    <div class="col-sm-2">
                        <div class="pull-right">{{trans('labels.total_records', ['number' => $masterFormat->total()])}}</div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-body">
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;
                                </button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                        @endif
                        <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                            <ul class="nav nav-tabs cm-tab-inner" role="tablist">
                                <li role="presentation" @if ((!isset($_GET['view']) && !isset($view)) || (isset($_GET['view']) && $_GET['view'] != 'type') || (isset($view) && $view != 'type')) class="active" @endif><a href="{{URL('masterformat')}}">{{trans('labels.master_format')}}</a></li>
                                <li role="presentation" @if ((isset($_GET['view']) && $_GET['view'] == 'type') || (isset($view) && $view == 'type')) class="active" @endif><a href="{{URL('masterformat')}}?view=type">{{trans('labels.master_format_versions')}}</a></li>
                            </ul>
                            <div class="panel panel-default panel-body cm-panel-tabs-body">
                                <div id="myTabContent" class="tab-content">
                                    @if ((isset($_GET['view']) && $_GET['view'] == 'type') || (isset($view) && $view == 'type'))
                                        <div class="row">
                                            <div class="col-sm-11">
                                                <a href="{{URL('masterformattype/create')}}"
                                                   class="btn btn-primary">{{trans('labels.masterformat.insert_new_type')}}</a>
                                            </div>
                                            <div class="col-sm-1">
                                                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                                <div class="form-group">
                                                    <input type="hidden" value="{{URL('masterformattype/delete').'/'}}" id="form-url"/>
                                                    <button disabled id="delete-button"
                                                            class='btn btn-danger mr5' type='submit'
                                                            data-toggle="modal" data-target="#confirmDelete"
                                                            data-title="Delete Record"
                                                            data-message='{{trans('labels.global_delete_modal')}}'>
                                                        {{trans('labels.delete')}}
                                                    </button>
                                                </div>
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th><input type="checkbox" name="select_all" id="companies_select_all"></th>
                                                            <th>{{trans('labels.name')}}</th>
                                                            <th>{{trans('labels.year')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($paginatedMasterFormat as $item)
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" name="comp_id[]"
                                                                           class="company multiple-items-checkbox" value="{{$item->id}}"
                                                                           data-id="{{$item->id}}">
                                                                </td>
                                                                <td><a href="{{URL('masterformattype/edit/'.$item->id)}}">{{$item->name}}</a></td>
                                                                <td>{{$item->year}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <?php echo $paginatedMasterFormat->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="cm-filter">

                                                    <div class="row">
                                                        {!! Form::open(['method'=>'GET','url'=>'masterformat']) !!}
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                {!! Form::label('version', trans('labels.version')) !!}
                                                                {!! Form::select('master_format_type', $masterFormatTypes, Input::get('master_format_type')) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                {!! Form::label('name', trans('labels.name')) !!}
                                                                {!! Form::text('name', Input::get('name'), ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                {!! Form::label('number', trans('labels.number')) !!}
                                                                {!! Form::text('number', Input::get('number'), ['class' => 'form-control']) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                {!! Form::label('sort', trans('labels.sort_by')) !!}
                                                                {!! Form::select('sort', array('id'=>'ID','number'=>'Number','title'=>'Title'),Input::get('sort')) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <div class="form-group">
                                                                {!! Form::label('order', trans('labels.order_by')) !!}
                                                                {!! Form::select('order', array('asc'=>'Ascending','desc'=>'Descending'),Input::get('order')) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-1 ">
                                                            <div class="form-group">
                                                                {!! Form::label('filter', ' &nbsp;') !!}
                                                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary']) !!}
                                                            </div>
                                                        </div>
                                                        {!! Form::close() !!}
                                                        <div class="col-sm-1">
                                                            {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                                            <div class="form-group">
                                                                <input type="hidden" value="{{URL('masterformat').'/'}}" id="form-url"/>
                                                                <button disabled id="delete-button"
                                                                        class='btn btn-danger mr5 cm-btn-fixer' type='submit'
                                                                        data-toggle="modal" data-target="#confirmDelete"
                                                                        data-title="Delete Record"
                                                                        data-message='{{trans('labels.global_delete_modal')}}'>
                                                                    {{trans('labels.delete')}}
                                                                </button>
                                                            </div>
                                                            {!! Form::close()!!}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <a href="{{URL('masterformat/create')}}"
                                                   class="btn btn-primary">{{trans('labels.masterformat.insert_new')}}</a>
                                                <a href="{{URL('masterformat/upload')}}"
                                                   class="btn btn-primary">{{trans('labels.masterformat.import_csv')}}</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-striped">
                                                        <thead>
                                                        <tr>
                                                            <th><input type="checkbox" name="select_all" id="companies_select_all"></th>
                                                            <th>{{trans('labels.number')}}</th>
                                                            <th>{{trans('labels.title')}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach ($masterFormat as $item)
                                                            <tr>
                                                                <td>
                                                                    <input type="checkbox" name="comp_id[]"
                                                                           class="company multiple-items-checkbox" value="{{$item->id}}"
                                                                           data-id="{{$item->id}}">
                                                                </td>
                                                                <td><a href="{{URL('masterformat/'.$item->id.'/edit')}}">{{$item->number}}</a></td>
                                                                <td>{{$item->title}}</td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="pull-right">
                                                    <?php echo $masterFormat->appends([
                                                        'name' => Input::get('name'),
                                                        'number' => Input::get('number'),
                                                        'sort' => Input::get('sort'),
                                                        'order' => Input::get('order'),
                                                        'master_format_type' => Input::get('master_format_type')
                                                    ])->render(); ?>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('popups.delete_record_popup')
    @include('popups.alert_popup')
@endsection
