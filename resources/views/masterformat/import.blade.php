@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading">
                {{trans('labels.masterformat.import_csv')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="panel">
                <div class="panel-body">
                    <form class="form-inline" action="{{URL('/masterformat/import')}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('version', trans('labels.version')) !!}
                                    {!! Form::select('master_format_type', $masterFormatTypes, Input::get('master_format_type'), ['class' => 'form-control']) !!}
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input name="_token" type="hidden" value="{!! csrf_token() !!}"/>
                                    <input type="file" name="master_format" accept=".csv">
                                </div>
                                <br />
                                <br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group pull-left">
                                    <input type="submit" value="{{trans('labels.submit')}}" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @include('errors.list')
        </div>
    </div>
</div>
@endsection
