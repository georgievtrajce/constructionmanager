@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="content">
            <h1>{{trans('labels.master_layout.contact_us')}}</h1>
            <p>
                Construction Master Solutions, LLC<br><br>

                {{trans('labels.Address')}}: 1501 Hamburg Turnpike, Suite #417, Wayne, NJ 07470<br><br>

                {{trans('labels.phone')}}: 973-873-6426<br><br>

                {{trans('labels.email')}}: <a href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>
            </p>
        </div>
    </div>
@stop
