@extends('layouts.master')

@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cm-heading">{{trans('labels.Dashboard')}}
                    <small class="cm-heading-sub">{{trans('labels.statistics_and_reports')}}</small>
                </h1>
            </div>
        </div>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div class="row">
            @if (sizeof($projects))
                <div class="col-md-4">
                    <div data-equal-height class="cm-box cm-box-brand">
                        <div class="cm-box-heading">
                            <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-globe"></i></span>
                            <h3 class="cm-box-heading-title">{{trans('labels.latest_projects')}}</h3>
                        </div>

                        <ul class="cm-box-list">
                            @foreach ($projects as $project)
                                <li class="cm-box-list-item">
                                    <h6 class="cm-box-list-item-date">{{Carbon::parse($project->created_at)->format('m/d/Y')}}</h6>
                                    <a class="cm-box-list-item-title" href="{{URL('/projects/'.$project->id)}}"><strong>{{$project->name}}</strong></a>
                                </li>
                            @endforeach
                        </ul>
                        <a class="cm-box-more" href="{{URL('/projects/')}}">{{trans('labels.view_more')}}</a>
                    </div>
                </div>
            @endif
            @if(Auth::user()->hasRole(['Company Admin', 'Project Admin']) || Permissions::can('read', 'address-book'))
                @if (sizeof($addressBookEntries))
                    <div class="col-md-4">
                        <div data-equal-height class="cm-box cm-box-positive">
                            <div class="cm-box-heading">
                                <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-folder-open"></i></span>
                                <h3 class="cm-box-heading-title">{{trans('labels.latest_ab_entries')}}</h3>
                            </div>
                            <ul class="cm-box-list">
                                @foreach ($addressBookEntries as $addressBookEntry)
                                    <li class="cm-box-list-item">
                                        <h6 class="cm-box-list-item-date">{{Carbon::parse($addressBookEntry->created_at)->format('m/d/Y')}}</h6>
                                        <a class="cm-box-list-item-title" href="{{(Permissions::can('read', 'address-book') && !Permissions::can('write', 'address-book'))?URL('/address-book/'.$addressBookEntry->id.'/view'):URL('/address-book/'.$addressBookEntry->id)}}"><strong>{{$addressBookEntry->name}}</strong></a>
                                    </li>
                                @endforeach
                            </ul>
                            <a class="cm-box-more" href="{{URL('/address-book/')}}">{{trans('labels.view_more')}}</a>
                        </div>
                    </div>
                @endif
            @endif
            @if(Auth::user()->hasRole(['Company Admin', 'Project Admin']))
                @if (sizeof($users))
                    <div class="col-md-4">
                        <div data-equal-height class="cm-box cm-box-special">
                            <div class="cm-box-heading">
                                <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-user"></i></span>
                                <h3 class="cm-box-heading-title">{{trans('labels.latest_users')}}</h3>
                            </div>
                            <ul class="cm-box-list">
                                @foreach ($users as $user)
                                    <li class="cm-box-list-item">
                                        <h6 class="cm-box-list-item-date">{{Carbon::parse($user->created_at)->format('m/d/Y')}}</h6>
                                        <a lass="cm-box-list-item-title" href="{{URL('/manage-users/'.$user->id.'/edit')}}"><strong>{{$user->name}}</strong></a>
                                    </li>
                                @endforeach
                            </ul>
                            <a class="cm-box-more" href="{{URL('/manage-users/')}}">{{trans('labels.view_more')}}</a>
                        </div>
                    </div>
                @endif
            @endif
            @if (sizeof($files))
                <div class="col-md-4">
                    <div data-equal-height class="cm-box cm-box-negative">
                        <div class="cm-box-heading">
                            <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-file"></i></span>
                            <h3 class="cm-box-heading-title">{{trans('labels.latest_files')}}</h3>
                        </div>
                        <ul class="cm-box-list">
                            @foreach ($files as $file)
                                <li class="cm-box-list-item">
                                    <h6 class="cm-box-list-item-date">{{Carbon::parse($file->created_at)->format('m/d/Y')}}</h6>
                                    <a class="download cm-box-list-item-title" href="javascript:;"><strong>{{$file->file_name}}</strong></a>
                                    <?php
                                    if ($file->display_name == Config::get('constants.contracts')) {
                                        $contractPath = !empty($file->contract_file)?'contract_'.$file->contract_file->contr_id.'/':'';
                                        $filePathPartial = $file->display_name.'/'.$contractPath.$file->file_name;
                                    } else if ($file->display_name == Config::get('constants.bids.plural')) {
                                        $filePathPartial = Config::get('constants.proposals').'/'.$file->file_name;
                                    } else {
                                        $filePathPartial = $file->display_name.'/'.$file->file_name;
                                    }
                                    ?>
                                    <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$file->proj_id.'/'.$filePathPartial}}">
                                    <p class="cm-box-list-item-date">
                                        @if(!is_null($file->project)) {{$file->project->name}} @endif
                                    </p>
                                    <p class="cm-box-list-item-date">
                                        <i>
                                            @if(!is_null($file->project)) {{$file->ft_name}} @endif
                                        </i>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                        <a class="cm-box-more" href="{{URL('home/all-uploaded-files')}}">{{trans('labels.view_more')}}</a>
                    </div>
                </div>
            @endif
            @if (sizeof($blogPosts))
                <div class="col-md-4">
                    <div data-equal-height class="cm-box cm-box-info">
                     <div class="cm-box-heading">
                            <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-comment"></i></span>
                            <h3 class="cm-box-heading-title">{{trans('labels.latest_blog_posts')}}</h3>
                        </div>
                        <ul class="cm-box-list">
                            @foreach ($blogPosts as $blogPost)
                                <li class="cm-box-list-item">
                                    <h6 class="cm-box-list-item-date">{{Carbon::parse($blogPost->created_at)->format('m/d/Y')}}</h6>
                                    <a class="cm-box-list-item-title" href="{{URL('/projects/'.$blogPost->proj_id.'/blog/'.$blogPost->id)}}"><strong>{{ $blogPost->title }}</strong></a>
                                    <p class="cm-box-list-item-date">
                                        @if(!is_null($blogPost->project)) {{$blogPost->project->name}} @endif
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                        <a class="cm-box-more" href="{{URL('home/all-blog-posts')}}">{{trans('labels.view_more')}}</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
