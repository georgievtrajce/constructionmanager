@extends('layouts.master')

@section('content')
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="cm-heading">
                            {{trans('labels.all_uploaded_files')}}
                            <small class="cm-heading-sub">{{trans('labels.total_records', ['number'=> $files->total()])}}</small>
                        </h1>
                    </div>
                </div>
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        @if (sizeof($files))
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>{{trans('labels.file.name')}}</th>
                                    <th>{{trans('labels.project.name')}}</th>
                                    <th>{{trans('labels.files.file_type')}}</th>
                                    <th>{{trans('labels.files.upload_date')}}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($files as $file)
                                    <tr>
                                        <td>{{$file->file_name}}</td>
                                        <td>@if(!is_null($file->project)){{$file->project->name}}@endif</td>
                                        <td>{{$file->ft_name}}</td>
                                        <td>{{Carbon::parse($file->created_at)->format('m/d/Y')}}</td>
                                        <td>
                                            <a class="btn btn-sm btn-primary download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                            <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$file->proj_id.'/'.$file->display_name.'/'.$file->file_name}}">
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div>
                                <p class="text-center">{{trans('labels.no_records')}}</p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <?php echo $files->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
