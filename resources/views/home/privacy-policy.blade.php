@extends('layouts.public-master')

@section('content')
    <div class="cms-page">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <small><i>Last Updated: December 16, 2015</i></small>

                    <h2>Privacy Notice</h2>

                    <p>Construction Master Solutions, LLC (“CMS”) respects your privacy and is committed to

                        taking reasonable steps towards protecting your privacy online and managing your

                        information responsibly.  This Privacy Notice is intended to notify you of our online

                        privacy practices, the type of information that may be collected and stored, how the

                        information is used, with whom the information may be shared, what choices are

                        available to you regarding the collection, use and distribution of the information, what

                        kind of security procedures are in place to protect the loss, misuse or alteration of

                        information under our control, and who to contact with questions or concerns.  <b><i>Please

                                note that we are privately owned and operated and are not affiliated with any state

                                or federal agency.</i></b><br><br>


                        This Privacy Notice applies to the website located at

                        <a href="https://www.cloud-pm.com">www.cloud-pm.com</a>  (the <b>“Website”</b>) and to any phone, email or postal

                        mail contacts with you.   This Privacy Notice does not apply to other websites to which

                        we may link, or to other companies or persons who might be listed as third-party

                        contacts or suppliers on this Website, or to whom we may refer you if you call or write

                        us.<br><br>


                        <b>By using this Website and/or by contacting us and providing any personal information,

                            you consent to the collection and use of information by us in accordance with this

                            Privacy Notice.  If we decide to change our Privacy Notice, we will post those changes

                            on this page.</b>
                    </p>

                    <h3><ins>Why We Collect Information:</ins></h3>

                    <p>We collect information, including personal information in order to provide our products

                        and services. In addition, your information may be used to communicate to you about

                        information, events, and offers which you might be interested in.  We may correlate

                        your personal information obtained from you with information from other sources.  For

                        example, we may cross-check personal information that you provide us against

                        information that might be otherwise available to us in order to prevent or detect fraud.
                    </p>

                    <h3><ins>How We Collect Information:</ins></h3>

                    <p>We collect information in three primary ways:<br><br>


                        1. <b>You Give Us Information:</b> We collect information from you when you

                        submit it to us through our Website or by otherwise contacting us.<br><br>



                        2. <b>We Collect Information Automatically:</b> We automatically collect certain

                        types of information when you visit our Website and/or use our products and services.

                        For example, we automatically collect various types of technical information when you

                        use our Website as otherwise described in this Privacy Notice.<br><br>


                        3. <b>We Collect Information from Other Sources:</b> We may obtain information

                        about you from outside sources.  For example, we may obtain commercially available

                        information about you from third parties, such as credit information or to prevent fraud,

                        or purchase e-mail lists from third parties for advertising and marketing purposes. We

                        may also receive information from third-parties who provide services for us through

                        web-beacons and other technologies as otherwise discussed in this Privacy Notice.
                    </p>

                    <h3><ins>What Information We Collect:</ins></h3>

                    <p>The following generally describes the type of information we may collect about you.

                        Those who wish to communicate with us but do not wish to provide personally

                        identifiable information online may contact us through the address or phone numbers

                        provided on our Website.  However, you should note that if you contact us by phone or

                        via postal mail, you may still be submitting personally identifiable information to us, as

                        set forth herein.
                    </p>

                    <h4>Account Creation and Information Storage:</h4>

                    <p>When you subscribe from the Website, we, or a third-party providing services on our

                        behalf, will collect the information necessary to complete your order, such as your

                        name, company information, e-mail address, payment information and billing address.

                        You may also provide us with additional information as you use the Website, such as the

                        documents and plans that you upload, addresses you store in the Website address book,

                        or the communications that you make through the Website. If you create an account,

                        we will store this information and other information about your use of the Website, in

                        order to make future experiences faster and more convenient. We will not share your

                        uploaded documents or stored addresses except as necessary to provide services to you

                        through the Website or as directed by you.
                    </p>

                    <h4>General Browsing:</h4>

                    <p>We do not automatically collect personally identifiable information unless you

                        voluntarily input or submit this information, such as during a registration or ordering

                        process, when you submit questions or comments, or otherwise interact with us online.

                        Should you voluntarily provide us with personally identifiable information, you consent

                        to our use of it in accordance with this Privacy Notice.<br><br>


                        Even if you do not submit personal information through the Website, we gather

                        navigational information about where visitors go on the Website and information about

                        the technical efficiencies of our Website and services (i.e., time to connect to the

                        Website, time to download pages, etc.). This information allows us to see which areas of

                        our Website are most visited and helps us better understand the user experience. In this

                        process of gathering information, we may collect personally identifiable and non-

                        personally identifiable information (for example, domain type, browser type and

                        version, service provider and IP address, referring/exit pages, operating system,

                        date/time stamp, and click-stream data). We may also create and use electronic records

                        to compile statistics about how our visitors collectively interact with our Website.
                    </p>

                    <h4>Use of Cookies:</h4>

                    <p>Cookies are bits of electronic information that a website may transfer to a visitor’s

                        computer to identify specific information about that visitor’s visits to the website, by

                        assigning a unique identification to your computer. We may use cookies in a variety of

                        ways, including to tell us whether you have previously visited the Website. We may also

                        use cookies set by third-parties with whom we have entered into agreements which

                        may enable us, for example, to obtain analytics information about the use of our

                        Website.  Those third-party cookies may enable us to obtain aggregate demographic

                        information and user statistics about you and your preferences from these third-party

                        sources as well as our information we have about you.  You can set your browser not to

                        accept cookies or to notify you when you are sent a cookie, giving you the opportunity

                        to decide whether or not to accept it.  You may also use commonly available tools in

                        your browser to remove cookies which may have been placed onto your computer.

                        Shared local objects (commonly referred to as “FLASH cookies”) are cookies that are

                        placed on your computer that are not removed through normal browser management

                        tools. We will not use FLASH cookies in a manner inconsistent with the purposes for

                        which they were designed unless you are specifically notified that we intend to use

                        them for a specific service.
                    </p>

                    <h4>Use of Web Beacons, Clear-GIFs, Pixel Tags and JavaScript:</h4>

                    <p>We also may use web beacons (also known as action pixels, pixel tags, or clear gifs)

                        and/or JavaScript plug-ins, placed on our Website and in our emails to you. These web

                        beacons and plug-ins are small graphic images (typically that you cannot see) or code on

                        a website or in an email message which are used for such things as recording web pages

                        and advertisements clicked-on by a user, or for tracking the performance of email

                        marketing campaigns. These devices help us analyze our customers' online behavior and

                        measure the effectiveness of our Website and our marketing. We may also work with

                        third-party service providers that help us track, collect, and analyze this information.

                        Third-party entities with whom we have agreements may place these devices on the

                        Website and/or in e-mails to use information obtained from them such as pages viewed,

                        items purchased, e-mails opened and items upon which you may click in e-mails.
                    </p>

                    <h4>Server Logs and Widgets:</h4>

                    <p>A web server log is a record of activity created by a computer that delivers certain web

                        pages to your browser.  Certain activities that you perform on our Website may record

                        information in server logs, such as if you enter a search term into a search box located

                        on the Website.  The server log may record the search term(s), or the link you clicked on

                        to bring you to our Website.  The server log may also record information about your

                        browser, such as you IP address and the cookies set on your browser.<br><br>


                        A widget is generally an application that can be embedded in a webpage.  Widgets can

                        provide real-time information to the webpage.  Widgets are often provided by third-

                        parties and we may provide widgets on our website.   Widgets may enable the third-

                        parties that provide them to collect date about users visiting the Website. If we allow

                        widgets on our Website, data will only be collected in accordance with this Privacy

                        Notice.
                    </p>

                    <h4>Search Queries on Our Website:</h4>

                    <p>We may provide you with options to search for information on our Website.  If you

                        enter information in a search query box, we may store that information and we may

                        aggregate that information with other information we may have about the browser

                        and/or IP address from which the search query originated.
                    </p>

                    <h3><ins>How We Use the Information We Collect:</ins></h3>

                    <p>We may use the information we collect internally in our business for various business

                        purposes, such as to: (i) provide the Website and our products/services; (ii) analyze

                        trends and conduct research about improving our products and services; (iii) provide

                        support and respond to questions from customers and Website visitors; (iv) improve our

                        Website, products and/or services; (v) learn about customers’ needs; (vi) contact

                        consumers for research, informational, and marketing purposes; (vii) track traffic

                        patterns and Website usage; (viii) enable online ordering and provide customer service

                        with respect to such orders; (ix) correlate information with other commercially available

                        information to identify demographics and preferences to assist us in our marketing

                        efforts; (x) provide specific relevant marketing, promotional, or other information to

                        you; (xi) address information security and/or privacy practices, control, network

                        functioning, engineering, and troubleshooting issues; (xii) investigate claims and/or legal

                        actions, violations of law or agreements, and compliance with relevant applicable laws

                        and legal process; (xiii) comply with law, or based on our good faith belief that it is

                        necessary to conform or comply with the law, or otherwise to disclose information to

                        prevent fraud to reduce credit risks, to cooperate with police and other governmental

                        authorities, or to protect the rights, property or safety of visitors to the Website or the

                        public; and/or (xiv) process or engage in a sale of all or part of our business, or if we go

                        through a reorganization or merger.<br><br>


                        Generally, using all of the above mentioned, and similar type tools, we may collect,

                        aggregate, and use information from or about you such as data about the type of

                        browser and operating system used, which web pages you view, the time and duration

                        or your visits to our Website, the search queries you may use on this Website, whether

                        you clicked on any items or links on the Website, whether you have clicked on any links

                        in any emails sent from us, or third-parties on our behalf, whether you have chosen to

                        opt-out of certain services or information sharing, and whether you have viewed or

                        ordered certain products or services.<br><br>


                        We may also share personally identifiable information and non-personally identifiable

                        information externally with our affiliates, licensees and joint venture partners, as well as

                        with other third-party service providers who help us provide operational services for the

                        Website and our business, which might include, but is not necessarily limited to:

                        business entities that provide e-mail address management and communication contact

                        services, network equipment and application management providers and hosting

                        entities,  credit and debit card payment gateways and processors and the issuing and

                        acquiring banks involved in the funds settlement procedures necessary to charge your

                        cards or financial accounts, entities which obtain information such as demographic

                        information which might be aggregated with information we have about you, judicial,

                        administrative and/or legal or financial accounting providers in the event that

                        information must be reviewed or released in response to civil and/or criminal

                        investigations, claims, lawsuits, or if we are subject to judicial or administrative process

                        (such as a subpoena) to release your information or to prosecute or defend legal

                        actions, as we believe is necessary or appropriate to prevent physical harm or financial

                        loss or in connection with an investigation of suspected or actual illegal activity, and

                        with other service providers which may be involved in the other types of services and

                        activities otherwise discussed in this Privacy Notice.<br><br>


                        We may also use your personally identifiable information to contact you in ways other

                        than e-mail or regular mail, such as via telephone contact, facsimile or SMS, in

                        accordance with your communication preferences.
                    </p>

                    <h3><ins>Business Transfers</ins></h3>

                    <p>If CMS, or substantially all of its assets, were acquired, or in the unlikely event that CMS

                        goes out of business or enters bankruptcy, user information would be one of the assets

                        that is transferred or acquired by a third-party. You acknowledge that such transfers

                        may occur, and that any acquirer of CMS may continue to use your personal information

                        only in accordance with this Privacy Notice.
                    </p>

                    <h3><ins>Your Choices about the Information We Collect</ins></h3>

                    <p>If you do not consent to the way in which we may use your personal information, please

                        do not submit any personal information to us.  If you do not wish to receive emails

                        about special offers and other promotions from us, please contact us using the below

                        information.  If you do not wish to receive other marketing materials from us and/or if

                        you do not want us to share your personal information with other entities as stated in

                        this Privacy Notice, please provide us with your exact name and address and advise us

                        that you wish to opt-out for information sharing or receiving information from us or

                        both as the case may be.  Please direct your opt-out request to:<br><br>


                        Construction Master Solutions, LLC<br>

                        1501 Hamburg Turnpike Suite #418<br>

                        Wayne, NJ 07470<br>

                        or by e-mail at <a href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>
                    </p>

                    <h3><ins>Privacy of Children Who Visit the Website</ins></h3>

                    <p>We recognize the importance of children's safety and privacy. The Website is not

                        designed to attract children, and is not intended for use by any children under the age

                        of 18. Moreover, in accordance with the Children’s Online Privacy Protection Act

                        (<b>“COPPA”</b>), CMS does not request, or knowingly collect, any personally identifiable

                        information from children under the age of 13. If we become aware of any mistaken or

                        inadvertent collection of information from a child under the age of 13 through their

                        misuse of our Website, we will take all necessary measures to promptly delete such

                        information from our records. We ask all parents and guardians to assist us in our

                        efforts to comply with COPPA by prohibiting children under the age of 13 from providing

                        CMS with any of their personal information or using the Website without parental

                        oversight and assistance.
                    </p>

                    <h3><ins>Your California Privacy Rights</ins></h3>

                    <p>We respect your privacy. Therefore we never share or sell your information with any

                        third parties for their marketing purpose. CMS will share information only with third-

                        party service providers who help us provide operational services for the Website and

                        our business, as noted <br><br>


                        We collect various types of personal information about you during the course of your

                        relationship with us as a customer. Under California law, if you are a resident of

                        California, you may make a written request to us about how we have shared your

                        information with third parties for their direct marketing purposes. In response to your

                        written request, we are allowed to provide you with a notice describing the cost-free

                        means to opt-out of our sharing your information with third parties with whom we do

                        not share the same brand name, if the third-party will use such information for its direct

                        marketing purposes.<br><br>


                        If you would like to exercise your rights under California law, please send your written

                        request to the e-mail address or postal address below. Please include your postal

                        address in your request. Within thirty (30) days of receiving your written request, we

                        will provide you with a Third-Party Direct Marketing Opt-Out Form so you may request

                        that your personal information not be disclosed to third parties for their direct

                        marketing purposes:<br><br>


                        Construction Master Solutions, LLC<br>

                        1501 Hamburg Turnpike Suite #418<br>

                        Wayne, NJ 07470<br>

                        or by e-mail at <a href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>
                    </p>

                    <h3><ins>Visitors to the Website Outside of the United States</ins></h3>

                    <p>If you are visiting the Website from a location outside of the United States, your

                        connection will generally be through and to servers located in the United States.

                        Information you receive from the Website will generally be created on servers located in

                        the United States, and information you provide will generally be maintained on web

                        servers and systems located within the United States. The data protection laws in the

                        United States and other countries might not be as those in your country.  By using this

                        Website and/or submitting information to us, you specifically consent to the transfer of

                        your information to the United States to the facilities and servers we use, and to those

                        with whom we may share your information.
                    </p>

                    <h3><ins>Links</ins></h3>

                    <p>For your convenience, the Website may contain links to other websites.  We are not

                        responsible for the privacy practices, advertising, products, or the content of such other

                        websites. None of the links should be deemed to imply that we endorse or have any

                        affiliation with the links.
                    </p>

                    <h3><ins>Security</ins></h3>

                    <p>We believe in providing a safe and secure experience for all of our online visitors. To

                        that end, we have implemented security measures to protect the information collected

                        from you. We maintain reasonable physical and electronic safeguards designed to limit

                        unauthorized access to your personally identifiable information, and to protect you

                        against the criminal misuse of that information.<br><br>


                        We use data encryption technology when transferring and receiving your sensitive

                        personal information, such as credit card or financial account information. For example,

                        in your transactions submitted by you through the Website, a solid key icon or a locked

                        padlock icon at the lower portion of your web browser window confirms the Website is

                        secured through Secure Sockets Layer (SSL). Pages requesting personal information

                        should always have one of these icons. You can also confirm your data is being

                        encrypted by looking at the URL line of your browser (the place where you type website

                        addresses). When accessing a secure server, the site address will change from “http” to

                        “https”.<br><br>


                        While we use the foregoing security measures to protect your information, please note

                        that no data transmitted over the Internet or stored and utilized for business purposes

                        can be guaranteed to be completely secure.  No security measures are perfect or

                        impenetrable.  We cannot guarantee that only authorized persons will view your

                        information. We cannot ensure that information you share on the Website will not

                        become publicly available. You can reduce these risks by using common sense security

                        practices such as choosing a strong password, using different passwords for different

                        services, and using up to date antivirus software.
                    </p>

                    <h3><ins>Questions / Changes in Notice</ins></h3>

                    <p>If you have questions or concerns with respect to our Privacy Notice, you may contact us

                        at <a href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>.  We may elect to change or amend our Privacy

                        Notice; in such event, we will post the notice changes in our Privacy Notice on the

                        Website. If you are concerned about how your personal information is used, please visit

                        our Website often for this and other important announcements and updates.
                    </p>

                </div>
            </div>
        </div>
    </div>
@stop
