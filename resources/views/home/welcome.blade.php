@extends('layouts.public-master-index')

@section('content')
	<div class="cms-page">
		<div class="cms-panel">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="cms-page-title text-center">What is Cloud PM?</h3>
					</div>
				</div>
				<div class="row" style="margin-top: 20px;">
					<div class="col-sm-12">
						<p class="lead">
							Cloud PM is a SaaS (Software as a Service) solution designed to help the General

							Contractors, Subcontractors and Material Suppliers to successfully manage and run their projects, while

							keeping the operating cost and the overhead as lowest as possible.<br><br>

							Unlike the other software and web applications we are not charging per the number of Users or Projects

							that you have. You can add unlimited number of Company Users, Subcontractors, Material Suppliers,

							Owners, Architects and open unlimited number of Projects without any additional costs.<br><br>

							Every Project Manager has a different way of doing things. At Cloud PM all the

							modules and forms are standardized and every Project is organized the same way. This will make the life

							much easier for everybody. No matter which Project do you open, the information or the files you are

							looking for will be at the same location at each one of them. All the Submittals, RFI’s, PCO’s, Bids, Logs

							will be in the same standardized format no matter who is working on them.<br><br>

							Since Cloud PM is a cloud based application, the Project Documents & Files are

							accessible to all the Project Team Members the moment they are uploaded in the modules. There will

							be no excuses that your Subcontractor has never received your email or that your Superintendent has

							never received the latest plans. All the Drawings, Specifications, Submittals, RFI's will always be available

							to everybody and from everywhere. Only the person who has the right User Permissions can view or

							modify the Project Documents & Files.
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="cms-panel cms-panel-base" id="features">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="cms-page-title text-center">Features</h3>
					</div>
				</div>
				<!--<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h5 class="cms-page-subtitle text-center">With our numerous and easy to use features Cloud PM is the most cost effective and most valuable application in the current market</h5>
					</div>
				</div>-->
				<div class="row">
					<div class="col-sm-3">
						<div class="cms-iconbox" data-equal-height>
							<div class="cms-iconbox-icon"><span class="fa fa-book"></span></div>
							<h5 class="cms-iconbox-title text-center">Powerful Address Book</h5>
							<p class="cms-iconbox-content text-center">Company categorization, multiple Offices and Contacts for each Company</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="cms-iconbox" data-equal-height>
							<div class="cms-iconbox-icon"><span class="fa fa-list-alt"></span></div>
							<h5 class="cms-iconbox-title text-center">Projects and Team</h5>
							<p class="cms-iconbox-content text-center">Unlimited number of Projects and Team Members</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="cms-iconbox" data-equal-height>
							<div class="cms-iconbox-icon"><span class="fa fa-folder-open "></span></div>
							<h5 class="cms-iconbox-title text-center">File management</h5>
							<p class="cms-iconbox-content text-center">Upload and share multiple files, organize them by Name, Code, Specification Numbers and Titles etc</p>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="cms-iconbox" data-equal-height>
							<div class="cms-iconbox-icon"><span class="fa fa-user"></span></div>
							<h5 class="cms-iconbox-title text-center">User Permissions</h5>
							<p class="cms-iconbox-content text-center">Read, write, delete rights for the Company Employees and the other Team Members</p>
						</div>
					</div>
				</div>
				<div class="cms-divider cms-divider--s"></div>
				<div class="row">
					<div class="col-sm-1"></div>
					<div class="col-sm-10">
						<div class="row">
							<div class="col-sm-4">
								<div class="cms-iconbox" data-equal-height>
									<div class="cms-iconbox-icon"><span class="fa fa-bar-chart-o"></span></div>
									<h5 class="cms-iconbox-title text-center">Submittal, RFI, PCO, Proposal</h5>
									<p class="cms-iconbox-content text-center">Modules for organizing files, creating versions, creating Transmittals and Logs, status tracking etc</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="cms-iconbox" data-equal-height>
									<div class="cms-iconbox-icon"><span class="fa fa-file-text"></span></div>
									<h5 class="cms-iconbox-title text-center">Blog module</h5>
									<p class="cms-iconbox-content text-center">Posting project updates, announcements etc</p>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="cms-iconbox" data-equal-height>
									<div class="cms-iconbox-icon"><span class="fa fa-dollar"></span></div>
									<h5 class="cms-iconbox-title text-center">Fixed pricing</h5>
									<p class="cms-iconbox-content text-center">Data Storage & Data Transfer based pricing</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-1"></div>
				</div>
				<div class="cms-divider cms-divider--m"></div>
				<div class="row">
					<div class="col-sm-4 col-sm-offset-4">
						<a href="https://cloud-pm.com/#features" class="btn btn-primary btn-lg btn-block">SEE FULL FEATURES LIST</a>
					</div>
				</div>
			</div>
		</div>
		<div class="cms-panel" id="pricing">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="cms-page-title text-center">Products & Pricing</h3>
						<h5 class="cms-page-subtitle text-center">We offer 3 subscription levels</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="cms-price-box">
							<h4 class="cms-price-box-title">Level 1</h4>
							<div class="cms-price-box-content">
								<span class="cms-price-box-price">@if($firstSubscription->amount > 0){{'$'.number_format((number_format($firstSubscription->amount,2) / 12),2)}}@else{{trans('labels.free')}}@endif</span>
								<ul class="cms-price-box-list">
									<li class="cms-price-box-list-item">Monthly Price/Billed Annually</li>
									<li class="cms-price-box-list-item">All Features Included</li>
									<li class="cms-price-box-list-item">Unlimited Users</li>
									<li class="cms-price-box-list-item">Unlimited Projects</li>
									<li class="cms-price-box-list-item">
										@if($firstSubscription->upload_limit > 0)
											{{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($firstSubscription->upload_limit)])}}
										@else
											{{trans('labels.master_layout.privacy_products11')}}
										@endif
									</li>
									<li class="cms-price-box-list-item">
										@if($firstSubscription->download_limit > 0)
											{{trans('labels.master_layout.download_of_files', ['number' => CustomHelper::formatByteSizeUnits($firstSubscription->download_limit)])}}
										@else
											{{trans('labels.master_layout.no_download')}}
										@endif
									</li>
								</ul>
								<a href="{{URL('/auth/register?level=1')}}" class="cms-price-box-btn btn">CHOOSE PLAN</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="cms-price-box active">
							<h4 class="cms-price-box-title">Level 2</h4>
							<div class="cms-price-box-content">
								<span class="cms-price-box-price">@if($secondSubscription->amount > 0){{'$'.number_format((number_format($secondSubscription->amount,2) / 12),2)}}@else{{trans('labels.free')}}@endif</span>
								<ul class="cms-price-box-list">
									<li class="cms-price-box-list-item">Monthly Price/Billed Annually</li>
									<li class="cms-price-box-list-item">All Features Included</li>
									<li class="cms-price-box-list-item">Unlimited Users</li>
									<li class="cms-price-box-list-item">Unlimited Projects</li>
									<li class="cms-price-box-list-item">
										@if($secondSubscription->upload_limit > 0)
											{{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($secondSubscription->upload_limit)])}}
										@else
											{{trans('labels.master_layout.privacy_products11')}}
										@endif
									</li>
									<li class="cms-price-box-list-item">
										@if($secondSubscription->download_limit > 0)
											{{trans('labels.master_layout.download_of_files', ['number' => CustomHelper::formatByteSizeUnits($secondSubscription->download_limit)])}}
										@else
											{{trans('labels.master_layout.no_download')}}
										@endif
									</li>
								</ul>
								<a href="{{URL('/auth/register?level=2')}}" class="cms-price-box-btn btn">CHOOSE PLAN</a>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="cms-price-box">
							<h4 class="cms-price-box-title">Level 3</h4>
							<div class="cms-price-box-content">
								<span class="cms-price-box-price">@if($thirdSubscription->amount > 0){{'$'.number_format(($thirdSubscription->amount / 12),2)}}@else{{trans('labels.free')}}@endif</span>
								<ul class="cms-price-box-list">
									<li class="cms-price-box-list-item">Monthly Price/Billed Annually</li>
									<li class="cms-price-box-list-item">All Features Included</li>
									<li class="cms-price-box-list-item">Unlimited Users</li>
									<li class="cms-price-box-list-item">Unlimited Projects</li>
									<li class="cms-price-box-list-item">
										@if($thirdSubscription->upload_limit > 0)
											{{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($thirdSubscription->upload_limit)])}}
										@else
											{{trans('labels.master_layout.privacy_products11')}}
										@endif
									</li>
									<li class="cms-price-box-list-item">
										@if($thirdSubscription->download_limit > 0)
											{{trans('labels.master_layout.download_of_files', ['number' => CustomHelper::formatByteSizeUnits($thirdSubscription->download_limit)])}}
										@else
											{{trans('labels.master_layout.no_download')}}
										@endif
									</li>
								</ul>
								<a href="{{URL('/auth/register?level=3')}}" class="cms-price-box-btn btn">CHOOSE PLAN</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<p class="lead">For a free on-line presentation please send your requests to <a class="text-color-text" href="mailto:sales@cloud-pm.com">sales@cloud-pm.com</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="cms-panel cms-panel-secondary" id="contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3 class="cms-page-title text-center">Contact us</h3>
						<h5 class="cms-page-subtitle text-center">Construction Master Solutions, LLC</h5>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="panel">
							<div class="panel-body">
								<div class="row">
									<div class="col-sm-4">
										<h4>OUR ADDRESS</h4>
										<p class="lead">
											1501 Hamburg Turnpike,<br />
											Suite #418, Wayne, NJ 07470
										</p>
									</div>
									<div class="col-sm-4">
										<h4>EMAIL US</h4>
										<p class="lead">
											<a class="text-color-text" href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>
											<a class="text-color-text" href="mailto:support@cloud-pm.com">support@cloud-pm.com</a>
										</p>
									</div>
									<div class="col-sm-4">
										<h4>CALL US</h4>
										<p class="lead">
											973-873-6426
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="cms-page-flyer">
			<a href="http://www.buildingsny.com/?appname=100008&moduleid=0&campaignid=61910056" class="cms-page-flyer-img" target="_blank"><img src="{{URL::asset('images/flyer.gif')}}" alt="Flyer"></a>

			<a class="cms-page-flyer-trigger"><img src="{{URL::asset('images/trigger.png')}}" alt="Flyer Trigger"></a>
		</div>
	</div>
@stop
