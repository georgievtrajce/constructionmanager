@extends('layouts.public-master')

@section('content')
    <style>
        ol {
            padding-left: 20px;
        }

        ol li {
            list-style-position: outside;
        }
    </style>
    <div class="cms-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="cms-page-title text-center">Features</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h5 class="cms-page-subtitle text-center"></h5>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-building"></span></div>
                        <h5 class="cms-iconbox-title">Company Profile</h5>
                        <ol>
                            <li>Create Company Profile (visible only to the Admin).</li>
                            <li>Create multiple offices.</li>
                            <li>Add / Invite unlimited number of Company Employees / Users and add them to one of the created offices.</li>
                            <li>Assign User Permissions for the Address Book (read, write, delete).</li>
                            <li>Invite other Companies to subscribe to the App and earn Referral Points to get a free subscription.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-book"></span></div>
                        <h5 class="cms-iconbox-title">Address Book</h5>
                        <ol>
                            <li>Create Address Book Entries.</li>
                            <li>Import Entries from MS Outlook and Google Address Book.</li>
                            <li>Organize the Entries under Categories (Subcontractors, Material Suppliers, Architects, Engineers, Surveyor’s, create a Custom Category etc.).</li>
                            <li>Assign Master Format Numbers & Titles (Specification Numbers & Titles) to the companies.</li>
                            <li>Search the address book by a MF Number or Title.</li>
                            <li>Add a Description of Services to each company. </li>
                            <li>Add multiple offices and multiple contacts for each company.</li>
                            <li>Save/print each company info.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-list-alt"></span></div>
                        <h5 class="cms-iconbox-title">Projects</h5>
                        <ol>
                            <li>Create unlimited number of Projects.</li>
                            <li>Add Project Info (Project Address, Project Value, Project Start and End Dates, Owner & Architect)</li>
                            <li>Upload Project Files into predefined folders (Drawings, Specifications, Daily Reports, etc.).</li>
                            <li>Organize the uploaded files by assigning File Names and File Numbers/Codes.</li>
                            <li>Assign Project User Permissions for each of your Company Employees. Read, Write & Delete permissions for each of the Project Folders & Files (Drawings, Submittals, PCO’s, Proposals, Contracts etc.).</li>
                            <li>Invite companies from your Address Book to join the Project (Subcontractors, Material Suppliers, Architects, Owners etc.).</li>
                            <li>Assign Project Permissions for each one of your Subcontractors. Read Only permissions for each one of the Project Folders & Files (Drawings, Submittals, PCO’s, Proposals, Contracts etc.).</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="cms-divider cms-divider--s"></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-files-o"></span></div>
                        <h5 class="cms-iconbox-title">Submittal Module</h5>
                        <ol>
                            <li>Create and organize Submittals, assign Submittal Names & Numbers.</li>
                            <li>Assign Master Format Numbers & Titles for each Submittal.</li>
                            <li>Assign Supplier/Subcontractor for the Submittal.</li>
                            <li>Create Submittal versions/cycles.</li>
                            <li>Enter Sent / Received Dates for each Cycle.</li>
                            <li>Assign a Status of the Submittal (R&R, APP etc.).</li>
                            <li>Upload files for each of the Cycles and Sent/Received Dates.</li>
                            <li>Create Submittal Transmittals (PDF File).</li>
                            <li>Log of all created Transmittals. Create a PDF File/Print the Transmittal Log.</li>
                            <li>Sort/Filter the Submittal List by MF Number or Title, Submittal Name, Supplier/Subcontractor, Submittal Status.</li>
                            <li>Create a PDF File/Print the Submittal List (the complete list or the Sorted/Filtered list).</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-info-circle"></span></div>
                        <h5 class="cms-iconbox-title">RFI Module</h5>
                        <ol>
                            <li>Create and organize RFI’s, assign RFI Names & Numbers.</li>
                            <li>Assign Master Format Numbers & Titles for each RFI.</li>
                            <li>Assign Supplier/Subcontractor for the RFI.</li>
                            <li>Create RFI versions/cycles.</li>
                            <li>Enter Sent / Received Dates for each Cycle.</li>
                            <li>Assign a Status of the RFI (Open, Closed).</li>
                            <li>Upload files for each of the Cycles and Sent/Received Dates.</li>
                            <li>Create RFI Transmittals (PDF File).</li>
                            <li>Log of all created Transmittals. Create a PDF File/Print the Transmittal Log.</li>
                            <li>Sort/Filter the RFI List by MF Number or Title, RFI Name, Supplier/Subcontractor, RFI Status.</li>
                            <li>Create a PDF File/Print the RFI List (the complete list or the Sorted/Filtered list).</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-industry"></span></div>
                        <h5 class="cms-iconbox-title">PCO module</h5>
                        <ol>
                            <li>Create and organize PCO’s, assign PCO Names & Numbers.</li>
                            <li>Assign Master Format Numbers & Titles for each PCO.</li>
                            <li>Assign multiple Suppliers/Subcontractors for each PCO.</li>
                            <li>Create PCO versions/cycles.</li>
                            <li>Enter Sent / Received Dates for each Cycle.</li>
                            <li>Assign a Status of the PCO (R&R, APP etc.).</li>
                            <li>Upload files for each of the Cycles and Sent/Received Dates.</li>
                            <li>Create PCO Transmittals (PDF File).</li>
                            <li>Log of all created Transmittals. Create a PDF File/Print the Transmittal Log.</li>
                            <li>Sort/Filter the PCO List by MF Number or Title, PCO Name, Supplier/Subcontractor, PCO Status.</li>
                            <li>Create a PDF File/Print the PCO List (the complete list or the Sorted/Filtered list).</li>
                        </ol>
                    </div>
                </div>

            </div>
            <div class="cms-divider cms-divider--s"></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-folder"></span></div>
                        <h5 class="cms-iconbox-title">Proposals Module</h5>
                        <ol>
                            <li>Create and organize the proposals and the proposal files.</li>
                            <li>Add MF Number & Title</li>
                            <li>Add Material/Service Name, Estimated Quantity, Estimated Value.</li>
                            <li>Add multiple proposals from different Companies for each entry.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-briefcase"></span></div>
                        <h5 class="cms-iconbox-title">Contracts Module</h5>
                        <ol>
                            <li>Create a list of all the contracts for the Project.</li>
                            <li>Upload the contracts.</li>
                            <li>Add Contract Title, Number/Code, Price, MF Number & Title</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-bar-chart"></span></div>
                        <h5 class="cms-iconbox-title">Expediting Report Module</h5>
                        <ol>
                            <li>Create a list of all the Materials/Services needed for the project.</li>
                            <li>Add MF Number & Title, Material/Service Name, Position (location) where the Material/ Service is needed.</li>
                            <li>Submittals Needed (Yes, No) and Submittal Status (APP, R&R, etc.) Fields.</li>
                            <li>Sort/Filter by Material/Service Name, Subcontractor, MF Number & Title, Submittal Status.</li>
                        </ol>
                    </div>
                </div>
            </div>
            <div class="cms-divider cms-divider--s"></div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="cms-iconbox" data-equal-height>
                        <div class="cms-iconbox-icon"><span class="fa fa-bar-chart"></span></div>
                        <h5 class="cms-iconbox-title">Blog Module</h5>
                        <ol>
                            <li>Post project updates.</li>
                            <li>Post announcements, warnings.</li>
                            <li>Post photos, videos etc.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop