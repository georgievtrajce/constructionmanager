@extends('layouts.master')

@section('content')
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="cm-heading">
                            {{trans('labels.all_blog_posts')}}
                            <small class="cm-heading-sub">{{trans('labels.total_records', ['number'=> $blogPosts->total()])}}</small>
                        </h1>
                    </div>
                </div>
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        @if (sizeof($blogPosts))
                        <div class="table-responsive">
                            <table class="table table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>{{trans('labels.blog.title')}}</th>
                                    <th>{{trans('labels.project.name')}}</th>
                                    <th>{{trans('labels.blog.published')}}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($blogPosts as $post)
                                    <tr>
                                        <td>{{$post->title}}</td>
                                        <td>@if(!is_null($post->project)){{$post->project->name}}@endif</td>
                                        <td>{{Carbon::parse($post->created_at)->format('m/d/Y')}}</td>
                                        <td>
                                            @if(!is_null($post->project))
                                                <a class="btn btn-sm btn-primary" href="{{URL('/projects/'.$post->project->id.'/blog/'.$post->id)}}">{{trans('labels.readMore')}}</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <div>
                                <p class="text-center">{{trans('labels.no_records')}}</p>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <?php echo $blogPosts->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
