@extends('layouts.public-master')

@section('content')
    <div class="cms-page">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="cms-page-title">Refund Policy</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <h5 class="cms-page-subtitle text-center"></h5>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <p>
                        In the first 30 days after you register, if you decide that our web application is not for you, we will gladly

                        refund your money.<br><br>


                        Please email us at <a href="mailto:billing@cloud-pm.com">billing@cloud-pm.com</a> with your request and we will send you a

                        short questionnaire regarding your experience with our application. Please write in the email subject

                        <b>Request for Refund: Name of your Company</b><br><br>


                        Please note that since we are having actual costs for data storage & transfer, you will be charged for one

                        month usage and a processing fee equal to 8% of your initial payment.<br><br>


                        The payment will be refunded after we receive the completed questionnaire back from you. You have 10

                        business days to return the questionnaire by email to us. If we don’t receive the questionnaire in 10

                        business days your request will be denied and no refund will be issued.<br><br>

                        We are offering a 1 month free testing account to all of our clients. If the client tests the web application

                        and later subscribes to the same, no refund will be issued.
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop
