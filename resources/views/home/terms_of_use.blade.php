@extends('layouts.public-master')

@section('content')
    <div class="cms-page">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">

                    <small><i>Last Updated: December 16, 2015</i></small>
                    <h1>Terms and Conditions of Use</h1>
                    <p>
                        Welcome. By accessing or using the Construction Master Solutions, LLC (<b>“CMS”</b>) website

                        located at <a href="https://www.cloud-pm.com">www.cloud-pm.com</a>  (the <b>“Website”</b>), you indicate that you

                        have read and understand this Terms and Conditions of Use Agreement (the <b>“Terms of

                            Use”</b>), which incorporates by reference our Privacy Notice, located at

                        <a href="https://www.cloud-pm.com/privacy-policy">https://www.cloud-pm.com/privacy-policy</a>  and agree to be bound by it

                        in its entirety.  If you do not understand or have questions about the Terms of Use,

                        immediately stop all use of the Website and contact

                        info@cloud-pm.com.  If you do not accept this Terms of Use, you may

                        not use the Website.  Your continued use of the Website constitutes your agreement to

                        the most current version of this Terms of Use.  If at any time you do not agree to this

                        Terms of Use, you must cease your use of this Website. <b><i>Please note that we are

                                privately owned and operated and are not affiliated with any state or federal agency.</i></b>

                        <br>
                        <br>

                        The Terms of Use apply only to the Website and do not apply to any other websites that

                        are linked to the Website.  For access to the terms and conditions or privacy policies of

                        linked websites, please refer to the policies of such websites.  It is your obligation to

                        review this Terms of Use before using the Website.  Any changes to the Terms of Use

                        will be effective immediately upon our posting them to the Website, unless otherwise

                        stated. We reserve the right to change the contents of the Website at any time, with or

                        without notice. <br><br>
                    </p>

                    <h2>I. Definitions</h2>

                    <p>For the purposes of this Terms of Use, the following definitions apply:<br><br>


                        <b>“Affiliates”</b> refers to companies and divisions under the ownership of CMS or that own

                        CMS.<br><br>

                        <b>“Content”</b> includes all Text, Graphics, Design and Programming used on the Website.<br><br>


                        <b>“Graphics”</b> includes all logos, buttons, and other graphical elements on the Website,

                        including the color combinations and the page layout of the Website, with the exception

                        of trademarks and intellectual property belonging to third-parties and displayed with

                        permission.<br><br>


                        <b>“Programming”</b> includes, but is not limited to, both client-side code (including but not

                        limited to HTML, JavaScript, etc.) and server-side code (including but not limited to

                        Active Server Pages,  VBScript, databases, etc.) used on the Website.<br><br>


                        <b>“Text”</b> includes all text on every page of the Website, whether editorial, navigational, or

                        instructional.<br><br>


                        <b>“You”</b> or <b>“your”</b> (whether or not capitalized) refers to the person accessing the Website

                        and agreeing to this Terms of Use.<br><br>

                    </p>
                    <h2>II. Use Of This Website</h2>

                    <p>The Website serves as a repository for construction documents and files, allowing users

                        to quickly and easily communicate and share documents for various projects. A limited

                        license is granted to you by us to view, download, and use the Website solely as

                        permitted in this Terms of Use.  The Website may only be used in a manner consistent

                        with this purpose, and only within the context of the products, services and information

                        set forth on the Website.  Except as provided herein, you are not permitted to download

                        (other than page caching) or change any portion of the Website, unless you have our

                        express written consent.
                    </p>

                    <h3>Account Registration</h3>

                    <p>Certain functions of the Website require registration. You may only create and use an

                        account for your business use. If you register or provide information to us in any

                        manner, you agree to provide only true, accurate, current and complete information.

                        You are responsible for maintaining the security of your account, and you are fully

                        responsible for all activities and actions that occur in connection with the account.<br><br>



                        If we issue a username and/or password to you, you agree to protect such information

                        by, among other things, keeping your password and other information relating to your

                        account confidential. If, notwithstanding the foregoing obligation, you allow another

                        party to use your account, you will be responsible for all use of the Website and all

                        other activities performed by the party using your account. You must immediately notify

                        us of any unauthorized uses of your account or any other breaches of security. You

                        agree that we will not be liable for any acts or omissions by you, including but not

                        limited to any damages of any kind incurred as a result of such acts or omissions.  If you

                        delete your account, we may still need to retain certain information for record keeping,

                        administrative, legal and technical purposes, and there may also be residual information

                        that will remain within our databases and other records, which will not be removed or

                        changed. By registering for an account, you grant us permission to send transactional

                        and marketing emails, information about the functionalities of the web application,

                        offers, promotions and other marketing material to you via email.  You may unsubscribe

                        from marketing content at any time.<br><br>



                        You understand that in order to create an account, you will be required to explicitly

                        agree to abide by the Terms of Use and Privacy Notice. It is your responsibility to

                        ascertain and obey all applicable local, state, federal and international laws, as

                        applicable, regarding the use of any services via this Website.
                    </p>

                    <h3>Product Information and Specifications</h3>

                    <p>All features, content, specifications and prices described or depicted on this Website are

                        subject to change at any time without notice. All prices and denominations are listed,

                        and concomitant transactions are conducted, in United States Dollars. We attempt to

                        ensure that information on the Website is complete, accurate and current. Despite our

                        efforts, the information on this Website may occasionally be inaccurate, incomplete or

                        out of date, and we make no representation as to the completeness, accuracy or

                        currentness of any information on the Website.
                    </p>

                    <h2>III. Restrictions on Use of the Website</h2>

                    <p>You may not collect or use any portion of the content of this Website in any derivative

                        way, or download, or copy information or other matter for use of any other party.  You

                        may not gather information and data on the Website from mining, robots or other

                        extraction tools.  The information displayed on the Website may not be used for any

                        purpose except in connection with your direct use of the Website as permitted by this

                        Terms of Use, and may not be excerpted, summarized, duplicated or otherwise removed

                        from the Website except with our explicit, written permission.  In addition, you

                        represent, warrant and agree that you will not use (or plan, encourage or help others to

                        use) the Website for any purpose or in any manner that is prohibited by this Terms of

                        Use or by applicable law.  You also may not interfere with the proper operation of the

                        Website including, but not limited to, by attacking, hacking into, or otherwise

                        attempting to penetrate any non-publicly accessible elements of the Website or its

                        servers or network, through the use of bots, Trojan Horses, viruses, DNS attacks, or

                        other technology which is designed or intended to interfere with the proper operation

                        of the Website or the use of the Website by any users.  You agree that you will not

                        circumvent or attempt to circumvent any security or access control technology

                        implemented on the Website, or the servers and network associated with the Website.

                        Any unauthorized use terminates the permission or license granted by CMS, in addition

                        to all rights at law or in equity. In addition, you specifically agree not to use this Website

                        to do any of the following:
                    </p>
                    <ul>
                        <li>Email or transmit content that is harmful, threatening, abusive, harassing,

                            tortious, defamatory, obscene, libelous, invasive of another’s privacy, or

                            otherwise unlawful;</li>

                        <li>Advocate illegal activity or an intention to commit an illegal act;</li>

                        <li>Impersonate or misrepresent your connection to any other entity or person

                            or otherwise manipulate identifiers to disguise the origin of the content;</li>

                        <li>Email or transmit content that infringes on the intellectual property  or the

                            rights of any entity or person;</li>

                        <li>Intentionally or unintentionally violate any applicable local, state, national

                            or international law;</li>

                        <li>Email or transmit material that includes viruses, worms or any other

                            computer code, files or programs designed to interrupt, destroy or limit the

                            functionality of any computer software or hardware or telecommunications;<br>

                            and/or</li>

                        <li>Disrupt the normal flow of communications or affect the ability of others to

                            engage in activities via this Website.</li>
                    </ul>

                    <h2>IV. Termination Of Access</h2>

                    <p>Use of this Website is a privilege. Users who violate this Terms of Use may be denied

                        access to the Website, and we reserve the right to suspend your use of the Website for

                        any reason at any time, in our sole discretion. The Website and its contents are not

                        intended for the use of children under the age of 13. Children under the age of 13 may

                        not use, or submit any information to, the Website. Individuals under the age of 18 may

                        only access the Website under the supervision of a parent or legal guardian who is at

                        least 18 years of age, and who agrees to be bound by, and responsible for, action taken

                        on the Website.
                    </p>

                    <h3>V. Information You Provide to CMS via this Website</h3>

                    <p>If you choose to provide personal information via this Website, the information will be

                        used only for certain purposes, as described in our Privacy Notice. Additionally, we may

                        collect or share certain information based on your usage of the Website, as described in

                        our Privacy Notice.<br><br>


                        In order to facilitate communications between you and CMS, this Website offers you the

                        ability to contact us. Although we strive to protect and secure our online

                        communications, and use the security measures detailed in our Privacy Notice to

                        protect your information, please note that no data transmitted over the Internet can be

                        guaranteed to be completely secure and no security measures are perfect or

                        impenetrable. CMS shall have no liability whatsoever for any unaccepted or

                        unprocessed email instructions or requests, or for any loss or damage arising from or in

                        connection with any unauthorized use by third-parties of any information that you send

                        by email or the Website.  If you would like to transmit sensitive information to us,

                        please contact us to arrange a more secure means of communication.
                    </p>

                    <h3>Limitations on Information Submitted</h3>

                    <p>We do not seek to receive any confidential or proprietary information or trade secrets

                        of third-parties through the Website, and we seek only the minimum necessary

                        information to provide our services and products.
                    </p>

                    <h3>Ownership of User-Furnished Items</h3>

                    <p>You retain all rights, title, and interest in the User-Furnished Items, along with all

                        Intellectual Property Rights associated with the User-Furnished Items, and no title to or

                        ownership of any of the foregoing is transferred to CMS, except as expressly set forth in

                        this Agreement. CMS hereby assigns you all right, title and interest, and associated

                        Intellectual Property Rights that it may have or acquire to any User-Furnished Items.

                        <b>“User-Furnished Items”</b> means the non-public or proprietary information and data that:

                        (a) is owned or controlled by you, as the case may be; and (b) is furnished by you to CMS

                        for use in connection with the Website. You hereby grant to CMS a limited, worldwide,

                        transferable, and non-exclusive license to use the User-Furnished Items as may be

                        reasonably necessary for CMS to provide the services and the Website. <b>“Intellectual

                            Property Rights”</b> means all rights under the patent, copyright and trademark laws of the

                        United States or other applicable jurisdiction.
                    </p>

                    <h2>VI. Monitoring Of Information</h2>

                    <p>CMS does not assume any obligation to review or monitor the content or other

                        information submitted to the Website by third parties.  You assume the risk of verifying

                        the accuracy of such information posted through independent investigation.  CMS does

                        not ordinarily review the content and information that you upload to the Website.

                        Notwithstanding the foregoing, we may in our discretion review information submitted

                        to the Website for any purpose whatsoever and we reserve the right, in our sole

                        discretion, to remove, edit or reject any information submitted to the Website for any

                        reason whatsoever.<br><br>


                        CMS reserves the right to cooperate with any law enforcement authorities or court

                        order requesting or directing CMS to disclose the identity of anyone sending any email

                        messages, or publishing or otherwise making available any materials that are believed to

                        violate the Terms of Use or law. You agree to waive and indemnify and hold CMS

                        harmless from and against any and all claims whatsoever resulting from or in connection

                        with any action by CMS regarding any investigations either by CMS or law enforcement

                        authorities.
                    </p>

                    <h2>VII. Intellectual Property Rights</h2>

                    <p>CMS owns any and all Intellectual Property Rights relating to the CMS brand, trade

                        name, trade dress, and other content including:  copyright, trademark, service mark,

                        trade name, trade dress, proprietary logo, insignia, business identifier, and/or other Text

                        and Graphics that has or provides the “look and feel” of the CMS brand image, as well as

                        all of the Content, including the Text, Graphics, Programming, photographs, video and

                        audio contained herein (the <b>“CMS Intellectual Property”</b>). Your use of the Website does

                        not grant you any rights or licenses relating to the CMS Intellectual Property, including

                        but not limited to any copyrights, trademark rights, patent rights, database rights, moral

                        rights, sui generis rights and other intellectual property and proprietary rights therein,

                        except as expressly provided for in these Terms of Use.  None of the CMS Intellectual

                        Property may be used, reproduced, published, transmitted, distributed, displayed,

                        performed, exhibited, modified, used to create derivative works, sold, re-sold or used in

                        any sale, or exploited for in any way, in whole or in part, except as provided for herein

                        and unless you obtain our prior written consent. You may not reproduce, modify,

                        display, sell, or distribute the CMS Intellectual Property, or use it in any other way for

                        public or commercial purpose.  The foregoing limitations include, but are not limited to,

                        copying or adapting the HTML code used to generate web pages on the Website, as well

                        as any other Graphics or Programming. All other Content, including product names,

                        names of services, trademarks, service marks and other intellectual property is the

                        property of their respective owners, as indicated, and may only be used as permitted.
                    </p>

                    <h2>VIII. Procedure for Making and Responding To Claims of Copyright Infringement</h2>

                    <p>We will respond to claims of copyright infringement, and will promptly process and

                        investigate notices of alleged infringement by third-parties and will take appropriate

                        actions under the Digital Millennium Copyright Act (<b>“DMCA”</b>), Title 17, United States

                        Code, Section 512(c)(2), where applicable. In keeping with the DMCA, notifications of

                        claimed copyright infringement by third-parties should be sent to Website's designated

                        agent noted below (the <b>“Designated Agent”</b>).  If you believe that your copyrighted work

                        has been infringed under U.S. copyright law and is accessible on the Website, please

                        notify us by contacting the Designated Agent.<br><br>


                        In order to give effective notification of a claim of copyright infringement by a third-

                        party under the DMCA, you must send a written communication to the Designated

                        Agent that includes substantially the following: (1) a physical or electronic signature of a

                        person authorized to act on behalf of the owner of an exclusive right that is allegedly

                        infringed; (2) identification of the copyrighted work, or, if multiple copyrighted works

                        are covered by a single notification, a representative list of such works on the Website,

                        that are claimed to have been infringed; (3) identification of the material that is claimed

                        to be infringing or to be the subject of infringing activity and that is to be removed or

                        access to which is to be disabled, and  information reasonably sufficient to permit us to

                        locate the material; (4) information reasonably sufficient to permit us to contact the

                        complaining party, such as an address, telephone number, and, if available, an e-mail

                        address at which the complaining party may be contacted; (5) a statement that the

                        complaining party has a good faith belief that neither the copyright owner, nor its agent

                        nor the law has authorized the use of the material in the manner complained of; and (6)

                        a statement that the information in the notification is accurate, and under penalty of

                        perjury, that the complaining party is authorized to act on behalf of the owner of an

                        exclusive right that is allegedly infringed.<br><br>



                        The Designated Agent for notice of claims of copyright infringement can be reached at:<br><br>


                        Construction Master Solutions, LLC<br>

                        1501 Hamburg Turnpike Suite #418<br>

                        Wayne, NJ 07470<br>

                        or by e-mail at <a href="mailto:info@cloud-pm.com">info@cloud-pm.com</a>.<br><br>



                        If a valid notification of alleged copyright infringement is received, we will remove or

                        disable access to the material identified in the notice as being infringing or as being the

                        subject of infringing activity, and take reasonable steps to notify the alleged infringer

                        that it has removed or disabled access to this material. Please note that, under the

                        DMCA, a claimant who makes a misrepresentation concerning copyright infringement

                        may be liable for any damages, including costs and attorneys' fees, incurred by the

                        alleged infringer who is injured by such misrepresentation as a result of reliance upon

                        such misrepresentation in removing or disabling access to the material or activity

                        claimed to be infringing or in replacing the removed material or ceasing to disable

                        access to it.
                    </p>

                    <h2>IX. Disclaimer of Warranties &amp; Limitations of Liability</h2>

                    <p><b>Your consent and agreement to the following disclaimers and limitations is a material

                            inducement for us to permit you to access the Website.  Your use of the Website, and

                            the obligations and liabilities of us in respect of your use of the Website, is expressly

                            limited as follows:</b>
                    </p>

                    <h3>DISCLAIMER OF WARRANTIES</h3>

                    <p>THE WEBSITE AND ITS CONTENT, INCLUDING ALL PRODUCTS/SERVICES SOLD, IS

                        PROVIDED “AS IS” AND WITHOUT ANY WARRANTY WHATSOEVER. WE DISCLAIM ANY

                        AND ALL EXPRESS AND IMPLIED WARRANTIES WHATSOEVER TO THE MAXIMUM EXTENT

                        PERMITTED BY LAW, INCLUDING WITHOUT LIMITATION, THE WARRANTIES OF

                        MERCHANTABILITY, TITLE, NON-INFRINGEMENT OF THIRD-PARTIES RIGHTS, AND

                        FITNESS FOR PARTICULAR PURPOSE. YOUR USE OF THIS WEBSITE, AND/OR RELIANCE ON

                        ANY OF ITS CONTENT IS AT YOUR OWN RISK.

                        CMS DOES NOT WARRANT THAT THE FUNCTIONS OR CONTENT CONTAINED IN THIS

                        WEBSITE WILL BE UNINTERRUPTED, ACCURATE OR ERROR-FREE.  YOU AND NOT CMS

                        ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR, OR CORRECTION IN THE

                        EVENT THAT THERE IS ANY LOSS OR DAMAGE ARISING FROM OR IN CONNECTION WITH

                        THE USE OF THIS WEBSITE OR ITS CONTENT. CMS DOES NOT WARRANT OR MAKE ANY

                        REPRESENTATION WHATSOEVER REGARDING THE USE, OR THE RESULT OF USE, OF THE

                        CONTENT OF THIS WEBSITE RELATED TO ACCURACY, RELIABILITY OR OTHERWISE.  THE

                        CONTENT OF THIS WEBSITE MAY INCLUDE ERRORS (INCLUDING, WITHOUT LIMITATION,

                        TECHNICAL OR TYPOGRAPHICAL ERRORS), AND CMS MAY MAKE CHANGES OR

                        IMPROVEMENTS TO THIS WEBSITE AT ANY TIME WITH OR WITHOUT NOTICE.

                        NEITHER CMS NOR ANY OTHER PERSON OR ENTITY ASSOCIATED WITH THE DESIGN OR

                        MAINTENANCE OF THIS WEBSITE SHALL BE HELD LIABLE OR RESPONSIBLE IN ANY WAY

                        FOR ANY DAMAGE, LOSS, INJURY, OR MALFUNCTION ASSOCIATED WITH YOUR USE OF

                        THIS WEBSITE.
                    </p>

                    <h3>LIMITATION OF LIABILITY</h3>

                    <p>IN NO EVENT SHALL CMS AND/OR ITS SUBSIDIARIES, AFFILIATES, RELATED COMPANIES,

                        SUPPLIERS, ADVERTISERS, SPONSORS, THIRD-PARTY SERVICE PROVIDERS, AND THEIR

                        RESPECTIVE EMPLOYEES, OFFICERS, DIRECTORS, AND AGENTS BE LIABLE FOR ANY

                        INCIDENTAL, CONSEQUENTIAL, PUNITIVE, INDIRECT OR SPECIAL DAMAGES, (INCLUDING

                        LOST PROFITS AND DAMAGES) WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR

                        ANY OTHER LEGAL THEORY, AND WHETHER OR NOT CMS IS ADVISED OF THE

                        POSSIBILITY OF SUCH DAMAGES.  BECAUSE SOME STATES OR JURISDICTIONS DO NOT

                        ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR

                        INCIDENTAL DAMAGES, THE LIMITATIONS SET FORTH IN THIS PARAGRAPH MAY NOT

                        APPLY TO YOU. IF THE FOREGOING LIMITATIONS ARE HELD INAPPLICABLE OR

                        UNENFORCEABLE FOR ANY REASON, THEN THE MAXIMUM LIABILITY OF CMS TO YOU

                        FOR ANY TYPE OF DAMAGES SHALL BE LIMITED TO THE TOTAL DAMAGES FOR WHICH

                        CMS SHALL BE LIABLE SHALL BE LIMITED TO THE COST OF THE SUBSCRIPTIONS(S)

                        PURCHASED, OR ONE HUNDRED UNITED STATES DOLLARS ($100), WHICHEVER IS

                        GREATER.
                    </p>

                    <h3>Release and Indemnification</h3>

                    <p>You, on behalf of your successors, assigns, heirs, and personal representatives hereby

                        irrevocably and fully release CMS, and its subsidiaries, Affiliates and each of their

                        officers, directors, employees, assigns, agents and representatives from and against any

                        and all suits, claims, actions, causes of action, arbitration, liabilities, obligations,

                        damages, losses, penalties or fines known or unknown, arising out of or in connection

                        with (1) your use of this Website; (2) the use of any information accessed by you from

                        this Website; and (3) the use of products sold or used from the Website.

                        You agree to indemnify and hold CMS and its subsidiaries, Affiliates, officers, agents,

                        employees, partners, and licensors harmless from any and all liability, claims, causes of

                        actions, damages, costs and expenses, including but not limited to, attorneys’ fees and

                        costs of suit, made by any third-party due to or arising out of (1) your use of this

                        Website, (2) information that you submit, transmit, or otherwise make available via this

                        Website or otherwise, including but not limited to the User-Furnished Items, or (3) your

                        breach of this Terms of Use. This indemnification obligation shall survive any

                        termination or expiration of this Terms of Use for any reason.
                    </p>

                    <h3>Exclusions</h3>

                    <p>SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR

                        THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL

                        DAMAGES.  THEREFORE SOME OF THE EXCLUSIONS AND/OR LIMITATIONS STATED

                        ABOVE MAY NOT APPLY TO YOU.
                    </p>

                    <h3>Third-Party Websites</h3>

                    <p>The Website may contain links to other websites for your convenience and information.

                        Such links may be to advertisers, content providers or other companies who may use

                        our logo and/or style as a result of a co-branding agreement. These websites may be

                        operated by companies that are not affiliated with CMS, and may have different privacy

                        policies and terms of use. CMS does not control the content that appears on these

                        websites or their privacy practices. Notwithstanding the presentation of, or links to, any

                        third-party information or website on the Website, no such presentation shall be

                        considered an endorsement, guarantee, representation or warranty, either express or

                        implied, by us on behalf of any third-party.  We shall have no liability or responsibility

                        whatsoever for the content, subject matter or substance of any information accessed or

                        obtained from third-party websites accessed from or via the Website.  Access to third-

                        party websites from the Website is done at your own risk.  We do not warranty any

                        opportunities found through the Website or through third-parties that advertise on the

                        Website or are purchased in reliance on information obtained from the Website.
                    </p>

                    <h3>Viruses and Transmission of Sensitive Information</h3>

                    <p>CMS cannot and does not guarantee or warrant that the materials contained on this

                        Website will be free of viruses, worms or other code or related hazards that may have

                        destructive properties (collectively “viruses”). It is your responsibility to ensure that you

                        have sufficient procedures, firewalls, checkpoints, and safeguards within your computer

                        system to satisfy your particular requirements to protect against viruses. CMS does not

                        assume any responsibility or risk for your use of the Internet, nor does CMS assume any

                        responsibility for any products or services of, or hyperlinks to, third-parties.
                    </p>

                    <h3>Miscellaneous</h3>

                    <p>We do not represent that materials on the Website are appropriate or available for use

                        in your location. Persons who choose to access the Website do so on their own initiative

                        and at their own risk, and are responsible for compliance with local laws, if and to the

                        extent local laws are applicable.  You agree that regardless of any statute or law to the

                        contrary, any claim or cause of action arising out of or related to use of the Website

                        must be filed within one (1) year after such claim or cause of action arose or be forever

                        barred. If any provision of the Terms of Use or Privacy Notice is found to be

                        unenforceable for any reason, than such provision shall be deemed severable from the

                        Terms of Use or the Privacy Notice, as the case.
                    </p>

                    <h2>X. General Information</h2>

                    <p><ins>Entire Agreement.</ins> These Terms of Use, together with the Privacy Notice constitute the

                        entire agreement between you and CMS governing your use of this Website,

                        superseding any prior agreements between you and CMS with respect to this Website.

                        You also may be subject to additional terms and conditions that may apply when you

                        use or purchase certain additional services of CMS.<br><br>


                        <ins>Waiver and Severability.</ins> The failure of CMS to enforce any right of the provisions in the

                        Terms of Use shall not constitute a waiver of such right or provision.  If any provision of

                        the Terms of Use is found by a court of competent jurisdiction to be invalid, illegal, or

                        unenforceable, it shall not affect any other provision of the Terms of Use, and the Terms

                        of Use shall be construed without regard to the invalid, illegal, or unenforceable

                        provision.<br><br>


                        <ins>Choice of Law; Jurisdiction; Venue.</ins> Your use of this Website and any dispute arising out

                        of or in connection with this Website shall be governed by the laws of the State of New

                        Jersey without giving effect to any conflict of laws provisions. By accessing this Website,

                        you agree that any action or proceeding arising out of or in connection with this

                        Website shall be brought solely in a court of competent jurisdiction sitting in the State

                        of New Jersey, and you agree to submit to the personal and exclusive jurisdiction of the

                        courts located therein. You hereby waive any defense of an inconvenient forum to the

                        maintenance of any action or proceeding in other courts and any objection to venue

                        with respect to such proceeding.
                    </p>

                </div>
            </div>
        </div>
    </div>
@stop
