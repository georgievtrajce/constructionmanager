@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="content">
            <h1>{{trans('labels.master_layout.products_and_pricing')}}</h1>

            <h3>{{trans('labels.master_layout.privacy_products1')}}</h3>
            <table>
                <thead>
                <tr>
                    <th style="background:#3498db;color:white;"><h4>{{trans('labels.master_layout.level1')}}<span class="pull-right" style="font-style: italic;">@if($firstSubscription->amount > 0){{'$'.number_format($firstSubscription->amount,2)}}@else{{trans('labels.free')}}@endif</span></h4></th>
                    <th style="background:#217dbb;color:white;"><h4>{{trans('labels.master_layout.level2')}}<span class="pull-right" style="font-style: italic;">@if($secondSubscription->amount > 0){{'$'.number_format($secondSubscription->amount,2)}}@else{{trans('labels.free')}}@endif</span></h4></th>
                    <th style="background:#3498db;color:white;"><h4>{{trans('labels.master_layout.level3')}}<span class="pull-right" style="font-style: italic;">@if($thirdSubscription->amount > 0){{'$'.number_format($thirdSubscription->amount,2)}}@else{{trans('labels.free')}}@endif</span></h4></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products2')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products3')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products4')}}</td>
                </tr>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products5')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products6')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products7')}}</td>
                </tr>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products8')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products9')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products10')}}</td>
                </tr>
                <tr>
                    <td>
                        @if($firstSubscription->upload_limit > 0)
                            {{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($firstSubscription->upload_limit)])}}
                        @else
                            {{trans('labels.master_layout.privacy_products11')}}
                        @endif
                    </td>
                    <td>
                        @if($secondSubscription->upload_limit > 0)
                            {{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($secondSubscription->upload_limit)])}}
                        @else
                            {{trans('labels.master_layout.privacy_products11')}}
                        @endif
                    </td>
                    <td>
                        @if($thirdSubscription->upload_limit > 0)
                            {{trans('labels.master_layout.privacy_products12', ['number' => CustomHelper::formatByteSizeUnits($thirdSubscription->upload_limit)])}}
                        @else
                            {{trans('labels.master_layout.privacy_products11')}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products14')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products15')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products16')}}</td>
                </tr>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products17')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products18')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products19')}}</td>
                </tr>
                <tr>
                    <td>{{trans('labels.master_layout.privacy_products20')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products21')}}</td>
                    <td>{{trans('labels.master_layout.privacy_products22')}}</td>
                </tr>
                <tr>
                    <td><a href="{{URL('/auth/register?subscription_level='.Config::get('subscription_levels.level-one'))}}" class="btn btn-primary">Select</a></td>
                    <td><a href="{{URL('/auth/register?subscription_level='.Config::get('subscription_levels.level-two'))}}" class="btn btn-primary">Select</a></td>
                    <td><a href="{{URL('/auth/register?subscription_level='.Config::get('subscription_levels.level-three'))}}" class="btn btn-primary">Select</a></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@stop
