@extends('layouts.master') @section('content')
<div class="row">
    <div class="col-sm-12">
        @include('projects.partials.tabs', array('activeTab' => 'project-files'))
    </div>
</div>
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-6">
            <header class="cm-heading">
                {{trans('labels.daily_report')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item">
                        <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">
                            {{trans('labels.Project').': '.$project->name}}
                        </a>
                    </li>
                    <li class="cm-trail-item">
                        <a href="{{URL('projects/'.$project->id.'/project-files/daily-reports/file')}}" class="cm-trail-link">
                            {{trans('labels.Project').' Daily Reports'}}
                        </a>
                    </li>
                    <li class="cm-trail-item active">
                        Create Daily Report
                    </li>
                </ul>
            </header>
        </div>
        <div class="col-md-6">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="pull-right">
                    <div class="cm-switch">
                        <span>{{trans('labels.address_book.save_with_docs')}}</span>
                        <input type="checkbox" id="addDocuments" class="cm-switch-input" name="addDocuments" />
                        <label for="addDocuments" class="cm-switch-knob"></label>
                    </div>
                    <a target="_blank" class="btn btn-success " id="generateDailyReport" style="cursor:pointer;">
                        {{trans('labels.files.download')}}
                        <div id="generate_daily_report_wait" class="pull-left" style="display: none;">
                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="panel cm-panel-alt">
        <div class="panel-body">
            <div class="col-md-2">
                <div class="form-group visible-xs visible-sm mb20">
                    <select class="form-control" id="daily-reports-select">
                        <option value="general-info" data-target="#general-info">
                            {{trans('labels.tasks.general-info')}}
                        </option>
                        <option value="general-info" data-target="#weather-info">
                            {{trans('labels.weather_ground_conditions')}}
                        </option>
                        <option value="prime-contractor" data-target="#prime-contractor">
                            {{trans('labels.contractor_labor')}}
                        </option>
                        <option value="subcontractor-contractor" data-target="#subcontractor-contractor">
                            {{trans('labels.subcontractor_labor')}}
                        </option>
                        <option value="equipment-on-site" data-target="#equipment-on-site">
                            {{trans('labels.equipment_on_site')}}
                        </option>
                        <option value="construction-materials" data-target="#construction-materials">
                            {{trans('labels.construction_materials')}}
                        </option>
                        <option value="testing-inspections" data-target="#testing-inspections">
                            {{trans('labels.testing_inspections_performed')}}
                        </option>
                        <option value="safety-observations" data-target="#safety-observations">
                            {{trans('labels.safety_observations')}}
                        </option>
                        <option value="verbal-instruction" data-target="#verbal-instruction">
                            {{trans('labels.verbal_instruction')}}
                        </option>
                        <option value="general-notes" data-target="#general-notes">
                            {{trans('labels.general_notes')}}
                        </option>
                        <option value="gallery" data-target="#gallery">
                            {{trans('labels.gallery')}}
                        </option>
                        <option value="attachments" data-target="#attachments">
                            {{trans('labels.attachments')}}
                        </option>
                    </select>
                </div>

                <ul id="daily-reports-tab" class="cm-tab cm-tab-vertical-alt hidden-xs hidden-sm" role="tablist">
                    <li class="cm-tab-item @if(Input::get('step') == false || Input::get('step') == 1) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#general-info" {{(Input::get('step') == false || Input::get('step') == 1) ? 'aria-expanded="true"' : 'aria-expanded="false"' }} >
                            {{trans('labels.tasks.general-info')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 2) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#weather-info" {{(Input::get('step') == 2) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.weather_ground_conditions')}}
                        </a>
                    </li>

                    <li class="cm-tab-item @if(Input::get('step') == 3) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#prime-contractor" {{(Input::get('step') == 3) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.contractor_labor')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 4) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#subcontractor-contractor" {{(Input::get('step') == 4) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.subcontractor_labor')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 5) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#equipment-on-site" {{(Input::get('step') == 5) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.equipment_on_site')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 6) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#construction-materials" {{(Input::get('step') == 6) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.construction_materials')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 7) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#testing-inspections" {{(Input::get('step') == 7) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.testing_inspections_performed')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 8) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#safety-observations" {{(Input::get('step') == 8) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.safety_observations')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 9) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#verbal-instruction" {{(Input::get('step') == 9) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.verbal_instruction')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 10) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#general-notes" {{(Input::get('step') == 10) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.general_notes')}}
                        </a>
                    </li>
                    <li class="cm-tab-item @if(Input::get('step') == 11) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#gallery" {{(Input::get('step') == 11) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                    {{trans('labels.gallery')}}
                    <li class="cm-tab-item @if(Input::get('step') == 12) active @endif">
                        <a class="cm-tab-link" data-toggle="tab" href="#attachments" {{(Input::get('step') == 11) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                            {{trans('labels.attachments')}}
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="tab-content">

                    <!-- errors -->
                    <div id="alert-danger-ajax"></div>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div id="alert-success-ajax"></div>
                    @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                    @endif

                    <!-- GENERAL INFO -->
                    <div id="general-info" class="tab-pane fade  @if(Input::get('step') == false || Input::get('step') == 1) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'general-info', 'class' => 'cf', 'role' => 'form']) !!}
                            <input type="hidden" value="1" name="current_step">

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6" style="padding-left:0">
                                                <div class="form-group">
                                                    <label for="report_num">Report #:</label>
                                                    <input type="text" id="report_num" class="form-control" name="report_num" readonly value="{{ $daily_report->report_num }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <a href="#" id="custom_report_num" class="btn btn-primary mt25 cm-heading-btn" style="width:100%;">Enter custom report number</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="date">Date:</label>
                                                <input type="text" id="report_date" class="form-control" name="date" value="{{ Carbon::parse($daily_report->date)->format('m/d/Y') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="day_of_week">Day of week:</label>
                                                <input type="text" id="day_of_week" class="form-control" name="day_of_week"  readonly value="{{$daily_report->day_of_week}}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <input class="btn btn-success pull-right" type="submit" value="Save">
                                        </div>
                                        <hr>
                                        <div class="col-md-12">
                                            <p class="mt20" style="font-weight:bold; font-size:18px">Email Report to:</p>
                                        </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                                    <div class="cms-list-box">
                                                        <ul class="cms-list-group">
                                                            <li class="cms-list-group-item cf">
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" name="select_all_companies" class="select_all_companies select_all_company_users" id="select_all_companies">
                                                                        <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                        Select All Companies
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <?php $allCompanyIds[] = '0'.$project->id; ?>
                                                            <li class="cms-list-group-item cf">
                                                                <div class="radio">
                                                                    <label>
                                                                        <input type="radio" checked class="companyCheckboxOut" name="companyRadioDistributionOut" data-type="user" data-id="{{$my_company->company->id}}" id="mycompany{{$my_company->company->id}}">
                                                                        <span class="radio-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                        {{$my_company->company->name}}
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            @foreach($companies as $id => $name)
                                                                <li data-id="{{$projects_companies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-out">
                                                                    <div class="radio">
                                                                        <label>
                                                                            <input type="radio" class="companyCheckboxDistributionOut" name="companyRadioDistributionOut" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                                                            <span class="radio-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                            {{$name}}
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                                <?php $allCompanyIds[] = $id; ?>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                                    <div class="cms-list-box">
                                                        <ul class="cms-list-group" id="usersListOut">
                                                            <?php $my_company_users = []; $myCompanyUserId = 0; $myCompanyUserCount = 0; ?>
                                                                <li class="cms-list-group-item cf">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_0{{$project->id}}">
                                                                            <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                            Select All Users
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                                <li class="cms-list-group-item cf" id="0{{$project->id}}_{{$my_company->company->currentUser->id}}">
                                                                    <div class="checkbox">
                                                                        <label>
                                                                            <input {{(!empty(old('employees_users_daily_report')) && in_array($my_company->company->currentUser->id, old('employees_users_daily_report')))?'checked':''}}
                                                                                   type="checkbox" name="employees_users_daily_report[]" value="{{$my_company->company->currentUser->id}}" class="users_0{{$project->id}} report-checkboxes">
                                                                            <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                            {{$my_company->company->currentUser->name}}
                                                                        </label>
                                                                    </div>
                                                                </li>
                                                                @if(count($my_company->projectUserPermissions) > 0)
                                                                    <?php $my_company_users[$my_company->company->currentUser->id] = [$my_company->company->id => $my_company->company->currentUser->name]; ?>
                                                                    @foreach($my_company->projectUserPermissions as $projectPermission)
                                                                        <li class="cms-list-group-item cf" id="0{{$project->id}}_{{$projectPermission->user->id}}">
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input {{(!empty(old('employees_users_daily_report')) && in_array($projectPermission->user->id, old('employees_users_daily_report')))?'checked':''}}
                                                                                           type="checkbox" name="employees_users_daily_report[]" value="{{$projectPermission->user->id}}" class="users_0{{$project->id}} report-checkboxes">
                                                                                    <span class="checkbox-material">
                                                                                <span class="check"></span>
                                                                            </span>
                                                                                    {{$projectPermission->user->name}}
                                                                                </label>
                                                                            </div>
                                                                        </li>
                                                                        <?php $my_company_users[$projectPermission->user->id] = [$my_company->company->id => $projectPermission->user->name]; ?>
                                                                        @if($myCompanyUserCount == 0)
                                                                            <?php $myCompanyUserId =  $projectPermission->user->id; $myCompanyUserCount++; ?>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                        </ul>
                                                        <?php $other_users = []; ?>
                                                        @foreach($users as $id => $value)
                                                            <ul class="cms-list-group contactsListDistributionOut hide" id="contactsListDistributionOut-{{$id}}">
                                                                @if(count($value) > 0)
                                                                    <li class="cms-list-group-item cf">
                                                                        <div class="checkbox">
                                                                            <label>
                                                                                <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_{{$id}}">
                                                                                <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                                Select All Users
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                    @foreach($value as $project_contact)
                                                                        @if($project_contact->contact)
                                                                            <li data-id="{{$projects_companies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-out" id="{{$id}}_{{$project_contact->contact->id}}">
                                                                                <div class="checkbox">
                                                                                    <label>
                                                                                        <input {{ (!empty(old('employees_contacts_daily_report')) && in_array($project_contact->contact->id, old('employees_contacts_daily_report')))?'checked':''}}
                                                                                               type="checkbox" name="employees_contacts_daily_report[]" value="{{$project_contact->contact->id}}" class="users_{{$id}} report-checkboxes">
                                                                                        <span class="checkbox-material">
                                                                                            <span class="check"></span>
                                                                                        </span>
                                                                                        {{$project_contact->contact->name}}
                                                                                    </label>
                                                                                </div>
                                                                            </li>
                                                                            <?php $other_users[$project_contact->contact->id] = [$id => $project_contact->contact->name]; ?>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            </ul>
                                                        @endforeach
                                                    </div>
                                                </div>
                                                @if((isset($daily_report->generatedFile) && !empty($daily_report->generatedFile)))
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a class="btn btn-sm btn-primary pull-right ml5 sendDailyReportEmail" title="Email Report"
                                                               data-path="{{'company_'.$daily_report->comp_id.'/project_'.$daily_report->proj_id.'/daily_report/'.$daily_report->id.'/report/'.$daily_report->generatedFile->file_name}}"
                                                               data-id="{{$daily_report->id}}"
                                                               data-file-id="{{$daily_report->generatedFile->file_id}}" data-emailed="{{$daily_report->generatedFile->emailed}}"
                                                               href="javascript:;">
                                                                <span class="glyphicon glyphicon-send mr5" aria-hidden="true"></span>
                                                                <span>Email Report</span>
                                                                <div class="pull-left email_wait" style="display: none;">
                                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </a>
                                                            <input type="hidden" class="daily-report-file" id="{{$daily_report->generatedFile->file_id}}" value="{{'company_'.$daily_report->comp_id.'/project_'.$daily_report->proj_id.'/daily_report/'.$daily_report->id.'/report/'.$daily_report->generatedFile->file_name}}">
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            @if(count($daily_report->emailedUsers) > 0 || count($daily_report->emailedContracts) > 0)
                                                <div class="col-md-12">
                                                    <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                                    @if(count($daily_report->emailedUsers) > 0)
                                                        @foreach($daily_report->emailedUsers as $emailedUser)
                                                            <?php $sendDate = Carbon\Carbon::parse($emailedUser->created_at); ?>
                                                            @if(!empty($emailedUser->user))
                                                                    {{$emailedUser->user->name}}{{!empty($emailedUser->user->company)?' - '.$emailedUser->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                            @endif
                                                        @endforeach
                                                    @endif

                                                    @if(count($daily_report->emailedUsers) > 0)
                                                        @foreach($daily_report->emailedContracts as $emailedContract)
                                                            <?php $sendDate = Carbon\Carbon::parse($emailedContract->created_at); ?>
                                                                @if(!empty($emailedContract->contract))
                                                                    {{$emailedContract->contract->name}}{{!empty($emailedContract->contract->addressBook)?' - '.$emailedContract->contract->addressBook->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                                @endif
                                                        @endforeach
                                                    @endif
                                                    <br /><br />
                                                </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- WEATHER INFO -->
                    <div id="weather-info" class="tab-pane fade @if(Input::get('step') == 2) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'weather-info', 'class' => 'cf', 'role' => 'form']) !!}
                            <input type="hidden" value="2" name="current_step">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">

                                            <h4 class="mb0 mt0">Weather Conditions</h4>
                                            <p class="text-muted">Check all that apply</p>

                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_windy" @if($daily_report->is_windy) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Windy
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_sunny" @if($daily_report->is_sunny) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Sunny
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_overcast" @if($daily_report->is_overcast) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Overcast
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_cloudy" @if($daily_report->is_cloudy) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Cloudy
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_rain" @if($daily_report->is_rain) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Rain
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 col-sm-6 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_snow" @if($daily_report->is_snow) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Snow
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_sleet" @if($daily_report->is_sleet) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Sleet
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_hail" @if($daily_report->is_hail) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Hail
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_lightning" @if($daily_report->is_lightning) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Lightning
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group mt10">
                                                <label for="temperature">Temp.:</label>
                                                <input type="text" class="form-control" name="temperature" value="{{(!empty($daily_report->temperature))? $daily_report->temperature : old('temperature') }}">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <h4 class="mb0 mt0">Ground Conditions</h4>
                                            <p class="text-muted">Check all that apply</p>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_dry" @if($daily_report->is_dry) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Dry
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_frozen" @if($daily_report->is_frozen) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Frozen
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="ground_is_snow" @if($daily_report->ground_is_snow) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Snow
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_mud" @if($daily_report->is_mud) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Mud
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="is_water" @if($daily_report->is_water) {{'checked'}} @endif>
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                Water
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <input class="btn btn-success pull-left mt10" type="submit" value="Save">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea class="form-control span12 textEditorSmallVersion" rows="5" name="condition_notes" id="condition_notes">
                                                    {{ (!empty($daily_report->condition_notes)) ? $daily_report->condition_notes : old('condition_notes') }}
                                                </textarea>
                                            <span id="weather-notes_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- PRIME CONTRACTOR LABOR -->
                    <div id="prime-contractor" class="tab-pane fade @if(Input::get('step') == 3) in active @endif">


                        <ul class="nav nav-tabs cm-inner-tabs">
                            <li class="@if(Input::get('tab') == false || Input::get('tab') == 1) active @endif">
                                <a data-toggle="tab" href="#workers-with-name" {{(Input::get('tab') == false || Input::get('tab') == 1) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                                    Workers With Names</a>
                            </li>
                            <li class="@if(Input::get('tab') == 2) active @endif">
                                <a data-toggle="tab" href="#workers-per-trade" {{(Input::get('tab') == 2) ? 'aria-expanded="true"' : 'aria-expanded="false"' }}>
                                    Workers per Trade</a>
                            </li>
                        </ul>

                        <div class="tab-content mt20">
                            <div id="workers-with-name" class="tab-pane fade @if(Input::get('tab') == false || Input::get('tab') == 1) in active @endif">
                                {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'workers-with-name', 'class' => 'cf', 'role' => 'form']) !!}
                                <input type="hidden" value="3" name="current_step">
                                <input type="hidden" value="workers-with-name" name="contractor_tab">
                                <input type="hidden" value="0" name="update" id="contractor1_update_hidden_btn">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- <h4 class="mb20 mt0">Prime Contractor Labor</h4> -->
                                            <div class="row mb20">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="worker_office" class="control-label">Worker Office:</label>
                                                        <div class="status-cont">
                                                            <select name="worker_office" id="contractor1_worker_office" onchange="changeWorkerName(false, false)">
                                                                @foreach($worker_offices as $office)
                                                                    <option {{(old("worker_office") == $office['id'] ? 'selected' : '')}} value="{{$office['id']}}">{{$office['name']}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                   </div>
                                                 </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="worker_name" class="control-label">Worker Name:</label>
                                                        <div class="status-cont">
                                                            <select name="worker_name" id="contractor1_worker_name" onchange="changeWorkerTitle(false)">
                                                                @foreach($worker_users as $officeId => $users)
                                                                    @foreach($users as $key => $value)
                                                                        @if(isset($worker_offices[0]))
                                                                            @if($worker_offices[0]['id'] == $officeId)
                                                                                <option {{(old("worker_name") == $key ? 'selected' : '')}} data-office-id="{{$officeId}}" value="{{$key}}">{{$value}}</option>
                                                                            @endif
                                                                        @endif
                                                                    @endforeach
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="title">Title / Trade:</label>
                                                        <?php $countUsers = 0; ?>
                                                        @foreach($worker_titles as $officeId => $titles)
                                                            @foreach($titles as $key => $value)
                                                                @if(isset($worker_offices[0]))
                                                                    @if($worker_offices[0]['id'] == $officeId)
                                                                        @if($countUsers < 1)
                                                                            <input type="text" class="form-control" name="title" id="contractor1_title" data-office-id="{{$officeId}}" data-user-id="{{$key}}" value="{{$value}}">
                                                                            <?php $countUsers++; ?>
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="regular_hours">Regular Hours:</label>
                                                                <input type="text" class="form-control" name="regular_hours" id="contractor1_regular_hours" value="{{old("regular_hours")}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="overtime_hours">Overtime Hours:</label>
                                                                <input type="text" class="form-control" name="overtime_hours" id="contractor1_overtime_hours" value="{{old("overtime_hours")}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="contractor1_save" class="col-md-12">
                                                    <input id="contractor1_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                                </div>
                                                <div id="contractor1_update" class="col-md-12" style="display: none">
                                                    <input id="contractor1_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                                    <input id="contractor1_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="description">Description of Work</label>
                                                    <textarea class="form-control span12 textEditorSmall" rows="5" name="description" id="contractor1_description">{{old("description")}}</textarea>
                                                    <span id="description-of-work_character_count"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <hr class="mt0">
                                        @if(count($daily_report->contractors) == 0)
                                            <p class="text-center">No Info</p>
                                        @else
                                            <input type="hidden" value="" name="contractor1_ids" id="contractor1_ids">
                                            <a disabled id="contractor1_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="3" data-tab="1" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered cm-table-compact">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Worker Office</th>
                                                            <th>Worker Name</th>
                                                            <th>Title / Trade</th>
                                                            <th>Description of Work</th>
                                                            <th class="text-center">Regular Hours </th>
                                                            <th class="text-center">Overtime Hours</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
                                                    @foreach($daily_report->contractors as $contractor)
                                                        <tr>
                                                            <td class="text-center">
                                                                <input type="checkbox" name="contractor1_checkboxes" class="contractor1_checkboxes" data-id="{{$contractor->id}}" data-step="3" data-tab="1" onchange="enableDeleteButton(this)">
                                                            </td>
                                                            <td>
                                                                <a class="daily_table_edit" style="cursor:pointer"
                                                                   data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$contractor->id}}" data-step="3" data-tab="1">
                                                                    {{--{{$contractor->worker_office}}--}}
                                                                    @foreach($worker_offices as $office)
                                                                        @if($office['id'] == $contractor->worker_office)
                                                                            {{$office['name']}}
                                                                         @endif
                                                                    @endforeach
                                                                </a>
                                                            </td>
                                                                {{--<td>$contractor->worker_name</td>--}}
                                                            @foreach($worker_users as $officeId => $users)
                                                                @foreach($users as $key => $name)
                                                                    @if($key == $contractor->worker_name)
                                                                        <td>{{$name}}</td>
                                                                    @endif
                                                                @endforeach
                                                            @endforeach
                                                            <td>{{$contractor->title}}</td>
                                                            <td>{{strip_tags($contractor->description)}}</td>
                                                            <td class="text-center">{{$contractor->regular_hours}}</td>
                                                            <td class="text-center">{{$contractor->overtime_hours}}</td>
                                                        </tr>
                                                        <?php $totalHours += ($contractor->regular_hours + $contractor->overtime_hours);
                                                                $regularHours += $contractor->regular_hours;
                                                                $overtimeHours += $contractor->overtime_hours; ?>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td td-right"></td>
                                                            <td class="text-right text-bold">
                                                                <strong>TOTAL</strong>
                                                            </td>
                                                            <td class="text-center">
                                                                <strong>{{$regularHours}}</strong>
                                                            </td>
                                                            <td class="text-center">
                                                                <strong>{{$overtimeHours}}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td td-right"></td>
                                                            <td class="text-right text-bold">
                                                                <strong>TOTAL HOURS WORKED</strong>
                                                            </td>
                                                            <td colspan="2" class="text-center">
                                                                <strong>{{$totalHours}}</strong>
                                                            </td>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                            </div>
                                        @endif
                                        </div>
                                    </div>
                               {!! Form::close() !!}
                            </div>
                            <div id="workers-per-trade" class="tab-pane fade @if(Input::get('tab') == 2) in active @endif">
                                {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'workers-per-trade', 'class' => 'cf', 'role' => 'form']) !!}
                                <input type="hidden" value="3" name="current_step">
                                <input type="hidden" value="workers-per-trade" name="contractor_tab">
                                <input type="hidden" value="0" name="update" id="contractor2_update_hidden_btn">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <!-- <h4 class="mb20 mt0">Prime Contractor Labor</h4> -->
                                            <div class="row mb20">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="trade">Trade:</label>
                                                        <input type="text" class="form-control" name="trade" id="contractor2_trade" value="{{old("trade")}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="trade_workers">Workers #:</label>
                                                        <input type="text" class="form-control" name="workers" id="contractor2_workers" value="{{old("workers")}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="trade_regular_hours">Regular Hours:</label>
                                                                <input type="text" class="form-control" name="regular_hours" id="contractor2_regular_hours" value="{{old("regular_hours")}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="trade_overtime_hours">Overtime Hours:</label>
                                                                <input type="text" class="form-control" name="overtime_hours" id="contractor2_overtime_hours" value="{{old("overtime_hours")}}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="contractor2_save" class="col-md-12">
                                                    <input id="contractor2_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                                </div>
                                                <div id="contractor2_update" class="col-md-12" style="display: none">
                                                    <input id="contractor2_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                                    <input id="contractor2_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="trade_description">Description of Work</label>
                                                    <textarea class="form-control span12 textEditorSmall" rows="5" name="description" id="contractor2_description">{{old("description")}}</textarea>
                                                    <span id="description-of-work2_character_count"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <hr class="mt0">
                                        @if(count($daily_report->contractorTrades) == 0)
                                            <p class="text-center">No Info</p>
                                        @else
                                            <input type="hidden" value="" name="contractor2_ids" id="contractor2_ids">
                                            <a disabled id="contractor2_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="3" data-tab="2" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered cm-table-compact">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>Trade</th>
                                                            <th>Workers</th>
                                                            <th>Description of Work</th>
                                                            <th class="text-center">Regular Hours </th>
                                                            <th class="text-center">Overtime Hours</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
                                                    @foreach($daily_report->contractorTrades as $contractor)
                                                        <tr>
                                                            <td class="text-center">
                                                                <input type="checkbox" name="contractor2_checkboxes" class="contractor2_checkboxes" data-id="{{$contractor->id}}" data-step="3" data-tab="2" onchange="enableDeleteButton(this)">
                                                            </td>
                                                            <td><a class="daily_table_edit" style="cursor:pointer"
                                                                   data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$contractor->id}}" data-step="3" data-tab="2">{{$contractor->trade}}</a></td>
                                                            <td>{{$contractor->workers}}</td>
                                                            <td>{{strip_tags($contractor->description)}}</td>
                                                            <td class="text-center">{{$contractor->workers * $contractor->regular_hours}}</td>
                                                            <td class="text-center">{{$contractor->workers * $contractor->overtime_hours}}</td>
                                                        </tr>
                                                        <?php $totalHours += (($contractor->workers * $contractor->regular_hours) + ($contractor->workers * $contractor->overtime_hours));
                                                        $regularHours += ($contractor->workers * $contractor->regular_hours);
                                                        $overtimeHours += ($contractor->workers * $contractor->overtime_hours); ?>
                                                    @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td td-right"></td>
                                                            <td class="text-right text-bold">
                                                                <strong>TOTAL</strong>
                                                            </td>
                                                            <td class="text-center">
                                                                <strong>{{$regularHours}}</strong>
                                                            </td>

                                                            <td class="text-center">
                                                                <strong>{{$overtimeHours}}</strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="no-td"></td>
                                                            <td class="no-td"></td>
                                                            <td class="no-td td-right"></td>
                                                            <td class="text-right text-bold">
                                                                <strong>TOTAL HOURS WORKED</strong>
                                                            </td>
                                                            <td colspan="2" class="text-center">
                                                                <strong>{{$totalHours}}</strong>
                                                            </td>
                                                        </tr>

                                                    </tfoot>
                                                </table>
                                            </div>
                                        @endif
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                    <!-- SUBCONTRACTOR CONTRACTOR LABOR -->
                    <div id="subcontractor-contractor" class="tab-pane fade @if(Input::get('step') == 4) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'subcontractor-contractor', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="4" name="current_step">
                        <input type="hidden" value="0" name="update" id="subcontractor_update_hidden_btn">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">Prime Contractor Labor</h4> -->
                                    <div class="row mb20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Company:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="subcontractor_selected_comp_id">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="trade">Trade:</label>
                                                <input type="text" class="form-control" name="trade" id="subcontractor_trade" value="{{old('trade') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="workers">Workers:</label>
                                                <input type="text" class="form-control" name="workers" id="subcontractor_workers" value="{{old('workers') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="regular_hours">Regular Hours:</label>
                                                        <input type="text" class="form-control" name="regular_hours" id="subcontractor_regular_hours" value="{{old('regular_hours') }}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="overtime_hours">Overtime Hours:</label>
                                                        <input type="text" class="form-control" name="overtime_hours" id="subcontractor_overtime_hours" value="{{old('overtime_hours') }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="subcontractor_save" class="col-md-12">
                                            <input id="subcontractor_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                        </div>
                                        <div id="subcontractor_update" class="col-md-12" style="display: none">
                                            <input id="subcontractor_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                            <input id="subcontractor_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description">Description of Work</label>
                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="description" id="subcontractor_description">{{old('description') }}</textarea>
                                            <span id="description-of-work3_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <hr class="mt0">
                                @if(count($daily_report->subcontractors) == 0)
                                    <p class="text-center">No Info</p>
                                @else
                                    <input type="hidden" value="" name="subcontractor_ids" id="subcontractor_ids">
                                    <a disabled id="subcontractor_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="4" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Company</th>
                                                    <th>Trade</th>
                                                    <th class="text-center">Workers</th>
                                                    <th>Description of Work</th>
                                                    <th class="text-center">Regular Hours </th>
                                                    <th class="text-center">Overtime Hours</th>
                                                    <th class="text-center">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
                                            @foreach($daily_report->subcontractors as $subcontractor)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="subcontractor_checkboxes" class="subcontractor_checkboxes" data-id="{{$subcontractor->id}}" data-step="4" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    @if(isset($project_companies[$subcontractor->selected_comp_id]) && !empty($project_companies[$subcontractor->selected_comp_id]))
                                                        <td>{{$project_companies[$subcontractor->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$subcontractor->selected_comp_id}}</td>
                                                    @endif
                                                    <td><a class="daily_table_edit" style="cursor:pointer"
                                                           data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$subcontractor->id}}" data-step="4">{{$subcontractor->trade}}</a></td>
                                                    <td class="text-center">{{strip_tags($subcontractor->workers)}}</td>
                                                    <td>{{strip_tags($subcontractor->description)}}</td>
                                                    <td class="text-center">{{$subcontractor->workers * $subcontractor->regular_hours}}</td>
                                                    <td class="text-center">{{$subcontractor->workers * $subcontractor->overtime_hours}}</td>
                                                    <td class="text-center">{{($subcontractor->workers * $subcontractor->regular_hours) + ($subcontractor->workers * $subcontractor->overtime_hours)}}</td>
                                                </tr>
                                                <?php $totalHours += ($subcontractor->workers * $subcontractor->regular_hours) + ($subcontractor->workers * $subcontractor->overtime_hours);
                                                $regularHours += ($subcontractor->workers * $subcontractor->regular_hours);
                                                $overtimeHours += ($subcontractor->workers * $subcontractor->overtime_hours); ?>
                                            @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td class="no-td"></td>
                                                    <td class="no-td"></td>
                                                    <td class="no-td"></td>
                                                    <td class="no-td td-right"></td>
                                                    <td class="text-right text-bold">
                                                        <strong>TOTAL</strong>
                                                    </td>
                                                    <td class="text-center">
                                                        <strong>{{$regularHours}}</strong>
                                                    </td>

                                                    <td class="text-center">
                                                        <strong>{{$overtimeHours}}</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="no-td"></td>
                                                    <td class="no-td"></td>
                                                    <td class="no-td"></td>
                                                    <td class="no-td td-right"></td>
                                                    <td class="text-right text-bold">
                                                        <strong>TOTAL HOURS WORKED</strong>
                                                    </td>
                                                    <td colspan="2" class="text-center">
                                                        <strong>{{$totalHours}}</strong>
                                                    </td>
                                                </tr>

                                            </tfoot>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                       {!! Form::close() !!}
                    </div>

                    <!-- EQUIPMENT ON SITE -->
                    <div id="equipment-on-site" class="tab-pane fade @if(Input::get('step') == 5) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'equipment-on-site', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="5" name="current_step">
                        <input type="hidden" value="0" name="update" id="equipment_update_hidden_btn">
                            <div class="row">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">Prime Contractor Labor</h4> -->
                                    <div class="row mb20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="equipment">Equipment:</label>
                                                <input type="text" class="form-control" name="equipment" id="equipment_equipment" value="{{old('equipment')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quality">Quantity:</label>
                                                <input type="text" class="form-control" name="quality" id="equipment_quantity" value="{{old('quality')}}">
                                            </div>
                                        </div>
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="owner">Owner:</label>--}}
                                                {{--<input type="text" class="form-control" name="owner" id="equipment_owner">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Owner:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="equipment_selected_comp_id">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="hours">Hours Worked:</label>
                                                <input type="text" class="form-control" name="hours" id="equipment_hours" value="{{old('hours')}}">
                                            </div>
                                        </div>

                                        <div id="equipment_save" class="col-md-12">
                                            <input id="equipment_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                        </div>
                                        <div id="equipment_update" class="col-md-12" style="display: none">
                                            <input id="equipment_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                            <input id="equipment_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description">Description of Work</label>
                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="description" id="equipment_description">{{old('description')}}</textarea>
                                            <span id="description-of-work4_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <hr class="mt0">
                                @if(count($daily_report->equipments) == 0)
                                    <p class="text-center">No Info</p>
                                @else
                                    <input type="hidden" value="" name="equipment_ids" id="equipment_ids">
                                    <a disabled id="equipment_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="5" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Equipment</th>
                                                    <th>Quantity</th>
                                                    <th>Owner</th>
                                                    <th>Description of Work</th>
                                                    <th class="text-center">Hours Worked</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($daily_report->equipments as $equipment)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="equipment_checkboxes" class="equipment_checkboxes" data-id="{{$equipment->id}}" data-step="5" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    <td><a class="daily_table_edit" style="cursor:pointer"
                                                           data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$equipment->id}}" data-step="5">{{$equipment->equipment}}</a></td>
                                                    <td class="text-center">{{$equipment->quality}}</td>
                                                    @if(isset($project_companies[$equipment->selected_comp_id]) && !empty($project_companies[$equipment->selected_comp_id]))
                                                        <td>{{$project_companies[$equipment->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$equipment->selected_comp_id}}</td>
                                                    @endif
                                                    {{--<td>{{$equipment->owner}}</td>--}}
                                                    <td>{{strip_tags($equipment->description)}}</td>
                                                    <td class="text-center">{{$equipment->quality * $equipment->hours}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- CONSTRUCTION MATERIALS DELIVERED TO SITE -->
                    <div id="construction-materials" class="tab-pane fade @if(Input::get('step') == 6) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'construction-materials', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="6" name="current_step">
                        <input type="hidden" value="0" name="update" id="materials_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">Construction Materails Delivered To Site</h4> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="material_delivered">Materials Delivered:</label>
                                                <input type="text" class="form-control" name="material_delivered" id="materials_delivered" value="{{old('material_delivered')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="quantity">Quantity:</label>
                                                <input type="text" class="form-control" name="quantity" id="materials_quantity" value="{{old('quantity')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Owner:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="materials_selected_comp_id">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="materials_save">
                                        <input id="materials_save_btn" class="btn btn-success pull-right" type="submit" value="Save">
                                    </div>
                                    <div id="materials_update" style="display: none">
                                        <input id="materials_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                        <input id="materials_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="notes">Description of work</label>
                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="notes" id="materials_notes">{{old('notes')}}</textarea>
                                            <span id="description-of-work5_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                @if(count($daily_report->materials) == 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr class="mt0">
                                            <p class="text-center">No Info</p>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" value="" name="materials_ids" id="materials_ids">
                                    <a disabled id="materials_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="6" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Materials Delivered</th>
                                                    <th>Owner</th>
                                                    <th class="text-center">Description of work</th>
                                                    <th class="text-center">Quantity</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($daily_report->materials as $material)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="materials_checkboxes" class="materials_checkboxes" data-id="{{$material->id}}" data-step="6" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    <td> <a class="daily_table_edit" style="cursor:pointer"
                                                            data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$material->id}}" data-step="6">{{$material->material_delivered}}</a></td>
                                                    @if(isset($project_companies[$material->selected_comp_id]) && !empty($project_companies[$material->selected_comp_id]))
                                                        <td>{{$project_companies[$material->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$material->selected_comp_id}}</td>
                                                    @endif
                                                    <td>{{strip_tags($material->notes)}}</td>
                                                    <td class="text-center">{{$material->quantity}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- TESTING / INSPECTIONS PERFORMED -->
                    <div id="testing-inspections" class="tab-pane fade @if(Input::get('step') == 7) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'testing-inspections', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="7" name="current_step">
                        <input type="hidden" value="0" name="update" id="inspections_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">Testing / Inspections Performed</h4> -->
                                    <div class="row">
                                        <div class="col-md-12" style="padding-left:0">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="testing_performed">Testing / Inspections Performed:</label>
                                                    <input type="text" class="form-control" name="testing_performed" id="inspections_performed" value="{{old('testing_performed')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="spec_or_dwg">Spec. Section:</label>
                                                {{--<input type="text" class="form-control" name="spec_or_dwg" id="inspections_spec_or_dwg">--}}
                                                <input type="text" id="inspections_spec_or_dwg" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="javascript:;" id="custom_mf_button" class="btn btn-primary mt25 cm-heading-btn" style="width:100%;">Enter custom number and title</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mf_number">MF Number:</label>
                                                <input type="text" class="form-control mf-number-auto" id="inspections_mf_number" name="mf_number" readonly value="{{old("mf_number")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="mf_title">MF Title:</label>
                                                <input type="text" class="form-control mf-title-auto" id="inspections_mf_title" name="mf_title" readonly value="{{old("mf_title")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Company:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="inspections_selected_comp_id" onchange="changeEmployeeName('inspections', null, null)">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                            <option {{ (old("selected_comp_id") === '0' ? 'selected' : '')}} class="other" value="0">Other company</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="inspector">Inspector:</label>--}}
                                                {{--<input type="text" class="form-control" name="inspector" id="inspections_inspector">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Inspector:</label>
                                                <div class="status-cont">
                                                    <select name="selected_employee_id" id="inspections_selected_employee_id">
                                                        @if(old("selected_employee_id") == null)
                                                            @foreach($my_company_users as $userId => $value)
                                                                @foreach($value as $companyId => $userName)
                                                                    <option value="{{$userId}}">{{$userName}}</option>
                                                                @endforeach
                                                            @endforeach
                                                                    <option class="other" value="0">Other user</option>
                                                        @else
                                                            @foreach($my_company_users as $userId => $value)
                                                                @foreach($value as $companyId => $userName)
                                                                    @if(old("selected_comp_id") == $companyId)
                                                                        <option {{ (old("selected_employee_id") == $userId ? 'selected' : '')}} value="{{$userId}}">{{$userName}}</option>
                                                                    @endif;
                                                                @endforeach
                                                            @endforeach

                                                            @foreach($other_users as $userId => $value)
                                                                @foreach($value as $companyId => $userName)
                                                                    @if(old("selected_comp_id") == $companyId)
                                                                        <option {{ (old("selected_employee_id") == $userId ? 'selected' : '')}} value="{{$userId}}">{{$userName}}</option>
                                                                    @endif;
                                                                @endforeach
                                                            @endforeach
                                                            <option {{ (old("selected_employee_id") === '0' ? 'selected' : '')}} class="other" value="0">Other user</option>
                                                        @endif;
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group other_company" style="{{ ((old('selected_comp_id') === '0') ? '' : 'display:none') }}">
                                                <label for="other_company">Other Company:</label>
                                                <input type="text" class="form-control" id="inspections_other_company" name="other_company" value="{{old("other_company")}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group other_employee" style="{{ ((old('selected_employee_id') === '0') ? '' : 'display:none') }}">
                                                <label for="other_employee">Other Inspector:</label>
                                                <input type="text" class="form-control" id="inspections_other_employee" name="other_employee" value="{{old("other_employee")}}">
                                            </div>
                                        </div>

                                        <div id="inspections_save" class="col-md-12">
                                            <input id="inspections_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                        </div>
                                        <div id="inspections_update" class="col-md-12" style="display: none">
                                            <input id="inspections_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                            <input id="inspections_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea class="form-control span12 textEditorSmallVersion" rows="5" name="notes" id="inspections_notes">{{old("notes")}}</textarea>
                                            <span id="description-of-work6_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                @if(count($daily_report->inspections) == 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr class="mt0">
                                            <p class="text-center">No Info</p>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" value="" name="inspections_ids" id="inspections_ids">
                                    <a disabled id="inspections_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="7" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Testing / Inspections Performed</th>
                                                    <th>Spec. Section</th>
                                                    <th>Company</th>
                                                    <th>Inspector</th>
                                                    <th>Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($daily_report->inspections as $inspection)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="inspections_checkboxes" class="inspections_checkboxes" data-id="{{$inspection->id}}" data-step="7" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    <td><a class="daily_table_edit" style="cursor:pointer"
                                                           data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$inspection->id}}" data-step="7">{{$inspection->testing_performed}}</a></td>
                                                    <td class="text-center">{{$inspection->mf_number}} - {{$inspection->mf_title}}</td>
                                                    @if(isset($project_companies[$inspection->selected_comp_id]) && !empty($project_companies[$inspection->selected_comp_id]))
                                                        <td>{{$project_companies[$inspection->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$inspection->other_company}}</td>
                                                    @endif
                                                    @if(isset($my_company_users[$inspection->selected_employee_id]) && !empty($my_company_users[$inspection->selected_employee_id]))
                                                        @foreach($my_company_users[$inspection->selected_employee_id] as $employeeName)
                                                            <td>{{$employeeName}}</td>
                                                        @endforeach
                                                    @elseif(isset($other_users[$inspection->selected_employee_id]) && !empty($other_users[$inspection->selected_employee_id]))
                                                        @foreach($other_users[$inspection->selected_employee_id] as $employeeName)
                                                            <td>{{$employeeName}}</td>
                                                        @endforeach
                                                    @else
                                                        <td>{{$inspection->other_employee}}</td>
                                                    @endif
                                                    <td>{{strip_tags($inspection->notes)}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- SAFETY OBSERVATIONS -->
                    <div id="safety-observations" class="tab-pane fade @if(Input::get('step') == 8) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'safety-observations', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="8" name="current_step">
                        <input type="hidden" value="0" name="update" id="observations_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">Safety Observations</h4> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="safety_deficiencies_observed">Safety Deficiencies Observed:</label>
                                                <input type="text" class="form-control" name="safety_deficiencies_observed" id="observations_safety" value="{{old('safety_deficiencies_observed')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="selected_comp_id" class="control-label">Company:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="observations_selected_comp_id">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="observations_save">
                                        <input id="observations_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                    </div>
                                    <div id="observations_update" style="display: none">
                                        <input id="observations_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                        <input id="observations_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="notes">Notes</label>
                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="notes" id="observations_notes">{{old('notes')}}</textarea>
                                            <span id="description-of-work7_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                @if(count($daily_report->observations) == 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr class="mt0">
                                            <p class="text-center">No Info</p>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" value="" name="observations_ids" id="observations_ids">
                                    <a disabled id="observations_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="8" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Safety Deficiencies Observed</th>
                                                    <th>Company</th>
                                                    <th>Notes</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($daily_report->observations as $observation)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="observations_checkboxes" class="observations_checkboxes" data-id="{{$observation->id}}" data-step="8" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    <td><a class="daily_table_edit" style="cursor:pointer"
                                                           data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$observation->id}}" data-step="8">{{$observation->safety_deficiencies_observed}}</a></td>
                                                    @if(isset($project_companies[$observation->selected_comp_id]) && !empty($project_companies[$observation->selected_comp_id]))
                                                        <td>{{$project_companies[$observation->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$observation->selected_comp_id}}</td>
                                                    @endif
                                                    <td>{{strip_tags($observation->notes)}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- LIST OF VERBAL INSTRUCTION GIVEN BY OWNER, ARCHITECT, OR ENGINEER OF RECORD -->
                    <div id="verbal-instruction" class="tab-pane fade @if(Input::get('step') == 9) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'verbal-instruction', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="9" name="current_step">
                        <input type="hidden" value="0" name="update" id="instructions_update_hidden_btn">
                        <div class="row mb20">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">List of Verbal Instruction Given by Owner, Architect, or Engineer of Record</h4> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="selected_comp_id" class="control-label">Company:</label>
                                                <div class="status-cont">
                                                    <select name="selected_comp_id" id="instructions_selected_comp_id" onchange="changeEmployeeName('instructions', null, null)">
                                                        @foreach($project_companies as $key => $value)
                                                            <option {{ (old("selected_comp_id") == $key ? 'selected' : '')}} value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="name">Name:</label>--}}
                                                {{--<input type="text" class="form-control" name="name" id="instructions_name">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company" class="control-label">Name:</label>
                                                <div class="status-cont">
                                                    <select name="selected_employee_id" id="instructions_selected_employee_id">
                                                        @foreach($my_company_users as $userId => $value)
                                                            @foreach($value as $key => $userName)
                                                                <option {{ (old("selected_employee_id") == $userId ? 'selected' : '')}} value="{{$userId}}">{{$userName}}</option>
                                                            @endforeach
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="instructions_save">
                                        <input id="instructions_save_btn" class="btn btn-success pull-left" type="submit" value="Save">
                                    </div>
                                    <div id="instructions_update" style="display: none">
                                        <input id="instructions_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                        <input id="instructions_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="description">Instructions / Verbal Approvals</label>
                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="description" id="instructions_description">{{old('description')}}</textarea>
                                            <span id="description-of-work7_character_count"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                @if(count($daily_report->instructions) == 0)
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr class="mt0">
                                            <p class="text-center">No Info</p>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" value="" name="instructions_ids" id="instructions_ids">
                                    <a disabled id="instructions_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="9" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Company</th>
                                                    <th>Name</th>
                                                    <th>Instructions / Verbal Approvals</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($daily_report->instructions as $instruction)
                                                <tr>
                                                    <td class="text-center">
                                                        <input type="checkbox" name="instructions_checkboxes" class="instructions_checkboxes" data-id="{{$instruction->id}}" data-step="9" onchange="enableDeleteButton(this)">
                                                    </td>
                                                    @if(isset($project_companies[$instruction->selected_comp_id]) && !empty($project_companies[$instruction->selected_comp_id]))
                                                        <td>{{$project_companies[$instruction->selected_comp_id]}}</td>
                                                    @else
                                                        <td>{{$instruction->selected_comp_id}}</td>
                                                    @endif
                                                    @if(isset($my_company_users[$instruction->selected_employee_id]) && !empty($my_company_users[$instruction->selected_employee_id]))
                                                        @foreach($my_company_users[$instruction->selected_employee_id] as $employeeName)
                                                            <td><a class="daily_table_edit" style="cursor:pointer"
                                                                   data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$instruction->id}}" data-step="9">{{$employeeName}}</a></td>
                                                        @endforeach
                                                    @elseif(isset($other_users[$instruction->selected_employee_id]) && !empty($other_users[$instruction->selected_employee_id]))
                                                        @foreach($other_users[$instruction->selected_employee_id] as $employeeName)
                                                            <td><a class="daily_table_edit" style="cursor:pointer"
                                                                   data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$instruction->id}}" data-step="9">{{$employeeName}}</a></td>
                                                        @endforeach
                                                    @else
                                                        <td><a class="daily_table_edit" style="cursor:pointer"
                                                               data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$instruction->id}}" data-step="9">{{$instruction->selected_employee_id}}</a></td>
                                                    @endif
                                                    <td>{{strip_tags($instruction->description)}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- GENERAL NOTES -->
                    <div id="general-notes" class="tab-pane fade  @if(Input::get('step') == 10) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'general-notes', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="10" name="current_step">
                        <input type="hidden" value="0" name="update" id="general_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="general_notes">General Notes</label>
                                        <textarea class="form-control span12 textEditor" rows="5" name="general_notes" id="general_notes">
                                            {{ (!empty($daily_report->general_notes)) ? $daily_report->general_notes : old('general_notes') }}
                                        </textarea>
                                        <span id="general_notes_character_count"></span>
                                    </div>

                                    <input class="btn btn-success pull-right" type="submit" value="Save">
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- GALLERY -->
                    <div id="gallery" class="tab-pane fade  @if(Input::get('step') == 11) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'gallery', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="11" name="current_step">
                        <input type="hidden" value="0" name="update" id="gallery_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Notes:</label>
                                                <input type="text" class="form-control" name="name" value="{{old('name')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>{{trans('labels.images')}}:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly title="{{trans('labels.image')}}" placeholder="{{trans('labels.image')}}">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary" style="padding: 10px 18px !important;">
                                                            <input type="file" style="display: none;" name="image_file" id="image_file" multiple="">

                                                            <input type="hidden" name="image_file_id" id="image_file_id" value="">
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <input class="btn btn-info pull-left cm-btn-fixer" type="submit" value="Add New">
                                        </div>
                                    </div>
                                    <div class="row file_main_message_container">
                                        <div id="file_message_container"
                                             class="col-md-12 image_file_message_container message_container"
                                             style="color: #49A078;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            @if(!isset($daily_report->countImages[0]))
                                <div class="col-md-12">
                                    <hr class="mt0">
                                    <p class="text-center">No Info</p>
                                </div>
                            @else
                                    <input type="hidden" value="" name="gallery_ids" id="gallery_ids">
                                    <div class="col-md-12">
                                        <a disabled id="gallery_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="11" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    </div>
                                @foreach($daily_report->files as $file)
                                    @if($file->file_type_id == Config::get('constants.daily_report_image_file_type_id'))
                                        <div class="col-md-3">
                                            <div class="cm-img-placeholder cm-popover">
                                                <img src="{{env('AWS_CLOUD_FRONT').'/company_' . $file->comp_id. '/project_' . $file->proj_id . '/daily_report/' . $file->daily_report_id . '/image/' . $file->file_name}}" alt="{{$file->file_name}}" style="height:140px; width:auto">
                                                <div class="col-md-12" style="padding-left:0;">
                                                    <input type="checkbox" name="gallery_checkboxes" class="gallery_checkboxes" data-id="{{$file->id}}" data-step="11" onchange="enableDeleteButton(this)">
                                                    <span>{{$file->name}}</span>
                                                </div>
                                                <p></p>
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                            </div>
                        {!! Form::close() !!}
                    </div>

                    <!-- ATTACHMENTS -->
                    <div id="attachments" class="tab-pane fade in @if(Input::get('step') == 12) in active @endif">
                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id), 'id' => 'attachments', 'class' => 'cf', 'role' => 'form']) !!}
                        <input type="hidden" value="12" name="current_step">
                        <input type="hidden" value="0" name="update" id="attachments_update_hidden_btn">
                            <div class="row mb20">
                                <div class="col-md-6">
                                    <!-- <h4 class="mb20 mt0">List of Verbal Instruction Given by Owner, Architect, or Engineer of Record</h4> -->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">Name:</label>
                                                <input type="text" class="form-control" name="name" id="attachments_name" value="{{old('name')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="description">Description:</label>
                                                <input type="text" class="form-control" name="description" id="attachments_description" value="{{old('description')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>{{trans('labels.pdf')}}:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly title="{{trans('labels.pdf')}}" placeholder="{{trans('labels.pdf')}}" id="attachments_pdf">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary" style="padding: 10px 18px !important;">
                                                            <input type="file" style="display: none;" name="pdf_file" id="pdf_file" multiple>

                                                            <input type="hidden" name="pdf_file_id" id="pdf_file_id" value="">
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="attachments_save" class="col-md-4">
                                            {{--<button type="button" id="pdf_file_upload" class="btn btn-info pull-left cm-btn-fixer">--}}
                                                {{--<div id="pdf_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">--}}
                                                    {{--<img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">--}}
                                                {{--</div>--}}
                                                {{--{{trans('labels.upload')}}--}}
                                            {{--</button>--}}
                                            <input class="btn btn-info pull-left cm-btn-fixer" type="submit" value="Add New">
                                        </div>
                                        <div id="attachments_update" class="col-md-4" style="display: none">
                                            <input id="attachments_update_btn" class="btn btn-success pull-right mr5" type="submit" value="Update">
                                            <input id="attachments_delete_btn" class="btn btn-danger pull-right mr5" onclick="cancelRow(this)" type="button" value="Cancel">
                                        </div>
                                    </div>
                                    <div class="row file_main_message_container">
                                        <div id="file_message_container"
                                             class="col-md-12 pdf_file_message_container message_container"
                                             style="color: #49A078;"></div>
                                    </div>
                                </div>
                                {{--<div class="col-md-6">--}}
                                    {{--<input class="btn btn-info pull-right mr5" type="submit" value="Add New">--}}
                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    @if(!isset($daily_report->countPdfs[0]))
                                    <div class="row">
                                        <div class="col-md-12">
                                            <hr class="mt0">
                                            <p class="text-center">No Info</p>
                                        </div>
                                    </div>
                                @else
                                    <input type="hidden" value="" name="attachments_ids" id="attachments_ids">
                                    <a disabled id="attachments_delete_all" class="btn btn-xs btn-danger pull-right" onclick="deleteMultipleRows(this)" data-step="12" data-daily-report-id="{{$daily_report->id}}">Delete</a>
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Name</th>
                                                    <th>Description</th>
                                                    <th class="text-center">Download</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($daily_report->files as $file)
                                                    @if($file->file_type_id == Config::get('constants.daily_report_pdf_file_type_id'))
                                                        <tr>
                                                            <td class="text-center w50">
                                                                <input type="checkbox" name="attachments_checkboxes" class="attachments_checkboxes" data-id="{{$file->id}}" data-step="12" onchange="enableDeleteButton(this)">
                                                            </td>
                                                            <td><a class="daily_table_edit" style="cursor:pointer"
                                                                   data-daily-report-id="{{$daily_report->id}}" data-row-id="{{$file->id}}" data-step="12">{{$file->name}}</a></td>
                                                            <td>{{$file->description}}</td>
                                                            <td class="text-center">
                                                                <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$file->file_id}}" value = "{{'company_' . $file->comp_id. '/project_' . $file->proj_id . '/daily_report/' . $file->daily_report_id . '/pdf/' . $file->file_name}}">
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <input id="projectId" type="hidden" value="{{$project->id}}">
                    <input id="dailyReportId" type="hidden" value="{{$daily_report->id}}">
                    <input id="myCompanyId" type="hidden" value="{{$my_company->company->id}}">
                    <input id="myFirstCompanyUserId" type="hidden" value="{{$myCompanyUserId}}">
                    <input id="allCompanyIds" type="hidden" value="{{implode(',',$allCompanyIds)}}">
                </div>
            </div>
        </div>
    </div>
</div>
@include('address_book.partials.successful-upload')
@include('address_book.partials.error-upload')
<!-- popup -->
<div class="product-image-overlay">
    <span class="product-image-overlay-close">x</span>
    <img src="" />
    <h5 class="product-image-description"></h5>
</div>
@include('popups.alert_popup')
@include('popups.approve_popup')
@include('popups.please-wait')
@endsection
<script type="text/javascript">
    var allWorkerUsers = '{!! json_encode($worker_users) !!}';
    var allWorkerTitles = '{!! json_encode($worker_titles) !!}';
    var myCompanyUsers = '{!! json_encode($my_company_users) !!}';
    var otherUsers = '{!! json_encode($other_users) !!}';
</script>