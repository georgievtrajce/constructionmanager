@extends('layouts.master') @section('content')
<div class="row">
    <div class="col-sm-12">
        @include('projects.partials.tabs', array('activeTab' => 'project-files'))
    </div>
</div>
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-6">
            <header class="cm-heading">
                {{trans('labels.daily_report')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item">
                        <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">
                            {{trans('labels.Project').': '.$project->name}}
                        </a>
                    </li>
                    <li class="cm-trail-item active">
                        Create Daily Report
                    </li>
                </ul>
            </header>
        </div>
    </div>

    <div class="panel cm-panel-alt">
        <div class="panel-body">
            <div class="col-md-2">
                <div class="form-group visible-xs visible-sm mb20">
                    <select class="form-control" id="daily-reports-select">
                        <option value="general-info" data-target="#general-info">
                            {{trans('labels.tasks.general-info')}}
                        </option>
                        <option value="general-info" data-target="#weather-info">
                            {{trans('labels.weather_ground_conditions')}}
                        </option>
                        <option value="prime-contractor" data-target="#prime-contractor">
                            {{trans('labels.contractor_labor')}}
                        </option>
                        <option value="subcontractor-contractor" data-target="#subcontractor-contractor">
                            {{trans('labels.subcontractor_labor')}}
                        </option>
                        <option value="equipment-on-site" data-target="#equipment-on-site">
                            {{trans('labels.equipment_on_site')}}
                        </option>
                        <option value="construction-materials" data-target="#construction-materials">
                            {{trans('labels.construction_materials')}}
                        </option>
                        <option value="testing-inspections" data-target="#testing-inspections">
                            {{trans('labels.testing_inspections_performed')}}
                        </option>
                        <option value="safety-observations" data-target="#safety-observations">
                            {{trans('labels.safety_observations')}}
                        </option>
                        <option value="verbal-instruction" data-target="#verbal-instruction">
                            {{trans('labels.verbal_instruction')}}
                        </option>
                        <option value="general-notes" data-target="#general-notes">
                            {{trans('labels.general_notes')}}
                        </option>
                        <option value="gallery" data-target="#gallery">
                            {{trans('labels.gallery')}}
                        </option>
                        <option value="attachments" data-target="#attachments">
                            {{trans('labels.attachments')}}
                        </option>
                    </select>
                </div>

                <ul id="daily-reports-tab" class="cm-tab cm-tab-vertical-alt hidden-xs hidden-sm" role="tablist">
                    <li class="cm-tab-item active">
                        <a class="cm-tab-link" data-toggle="tab" href="#general-info" aria-expanded="true">
                            {{trans('labels.tasks.general-info')}}
                        </a>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.weather_ground_conditions')}}
                        </span>
                    </li>

                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.contractor_labor')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.subcontractor_labor')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.equipment_on_site')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.construction_materials')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.testing_inspections_performed')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.safety_observations')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.verbal_instruction')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.general_notes')}}
                        </span>
                    </li>
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                    {{trans('labels.gallery')}}
                    <li class="cm-tab-item">
                        <span class="cm-tab-link">
                            {{trans('labels.attachments')}}
                        </span>
                    </li>
                </ul>
            </div>
            <div class="col-md-10">
                <div class="tab-content">
                    <!-- GENERAL INFO -->
                    <div id="general-info" class="tab-pane fade  @if(Input::get('step') == false || Input::get('step') == 1) in active @endif">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                        @endif
                        @if(isset($daily_report->id) && !empty($daily_report->id))
                            {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/daily-report/'.$daily_report->id.'/update-file'), 'id' => 'update-file', 'class' => 'cf', 'role' => 'form']) !!}
                                <input type="hidden" value="14" name="current_step">
                        @else
                            <form id="general-info" role="form" action="{{URL('/projects/'.$project->id.'/daily-report/store-file')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" value="13" name="current_step">
                        @endif
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-6" style="padding-left:0">
                                            <div class="form-group">
                                                <label for="report_num">Report #:</label>
                                                <input type="text" class="form-control" id="report_num" name="report_num" readonly  value="{{ (isset($daily_report->report_num)) ? $daily_report->report_num : ((!empty(old('report_num')))? old('report_num'): $report_num) }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <a href="#" id="custom_report_num" class="btn btn-primary mt25 cm-heading-btn" style="width:100%;">Enter custom report number</a>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="date">Date:</label>
                                                <input type="text" id="report_date" class="form-control" name="date"  value="{{ (isset($daily_report->date)) ? $daily_report->date : old('date') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="" for="day_of_week">Day of week:</label>
                                                <input type="text" id="day_of_week" class="form-control" name="day_of_week"  readonly value="{{ (isset($daily_report->date)) ? $daily_report->day_of_week : old('day_of_week')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>{{trans('labels.pdf')}}:</label>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly title="{{trans('labels.pdf')}}" placeholder="{{trans('labels.pdf')}}" value="{{ (isset($daily_report->generatedFile->file_name)) ? $daily_report->generatedFile->file_name : old('report_file') }}">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            <input type="file" style="display: none;" name="report_file" id="report_file" multiple>

                                                            <input type="hidden" name="report_file_id" id="report_file_id" value="{{(isset($daily_report->generatedFile->id)) ? $daily_report->generatedFile->id : '' }}">
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($daily_report->id) && !empty($daily_report->id))
                                                <input class="btn btn-primary pull-right cm-btn-fixer" type="submit" value="Update">
                                            @else
                                                <input class="btn btn-success pull-right cm-btn-fixer" type="submit" value="Save">
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @if(isset($daily_report->id) && !empty($daily_report->id))
                            {!! Form::close() !!}
                        @else
                            </form>
                        @endif
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- popup -->
<div class="product-image-overlay">
    <span class="product-image-overlay-close">x</span>
    <img src="" />
    <h5 class="product-image-description"></h5>
</div>
@endsection