<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<style>
    hr{
        display: block;
        height: 1px;
        border: 0;
        border-top: 1px solid #000;
        margin: 0;
        padding: 0;
    }
    .daily-padding {
        padding: 5px 5px !important;
        text-align: left !important;
    }
    .page-break {
        page-break-after: always;
    }
    img {
        width: auto;
        height: auto;
        max-width: 700px;
        max-height: 400px;
    }
</style>
<?php $my_company_users = []; ?>
    @if(count($my_company->projectUserPermissions) > 0)
        <?php $my_company_users[$my_company->company->currentUser->id] = [$my_company->company->id => $my_company->company->currentUser->name]; ?>
        @foreach($my_company->projectUserPermissions as $projectPermission)
            <?php $my_company_users[$projectPermission->user->id] = [$my_company->company->id => $projectPermission->user->name]; ?>
        @endforeach
    @endif

<?php $other_users = []; ?>
@foreach($users as $id => $value)
    @foreach($value as $project_contact)
        <?php $other_users[$project_contact->contact->id] = [$id => $project_contact->contact->name]; ?>
    @endforeach
@endforeach

<table width="100%" style="margin: 0px; padding: 0px;">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black;">
            <table width="100%">
                <tr>
                    <td style="width: 260px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 250px; vertical-align: bottom;">
                        Day of week: <br>
                        {{Config::get('constants.carbon_days.'.Carbon::parse($daily_report->date)->dayOfWeek)}}
                    </td>
                    <td style="float: right; width: 180px; vertical-align: bottom;">
                        <h2 style="float: left; margin: 0px; text-align: right;">{{'Daily Report'}}</h2>
                        <p style="float: left; margin: 0px; text-align: right;">{{'No: '.$daily_report->report_num}}</p>
                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.Carbon::parse($daily_report->date)->format("m/d/Y")}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-bottom: 3px solid black; width: 100%; margin: 0px; padding: 0px;">
            <table width="100%" style="margin: 0px; padding: 0px;">
                <tr>
                    <td style="vertical-align: top;">
                        <b>{{'Project: '}}</b>
                    </td>
                    <td style="vertical-align: top; width: 250px;">
                        {{$project->name}}
                    </td>
                    <td style="vertical-align: top; width: 150px; text-align: right;">
                        <b>{{'Project Number: '}}</b>
                    </td>
                    <td style="vertical-align: top; width: 200px;">
                        {{$project->number}}
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">
                        <b>{{'Address: '}}</b>
                    </td>
                    <td style="vertical-align: top; width: 250px;">
                        {{$daily_report->user->address->street}}<br>
                        {{$daily_report->user->address->city.', '.$daily_report->user->address->state.' '.$daily_report->user->address->zip}}
                    </td>
                    <td style="vertical-align: top; width: 150px; text-align: right;">
                        <b>{{'Created by: '}}</b>
                    </td>
                    <td style="vertical-align: top; width: 200px;">
                        {{$daily_report->user->name}}<br>
                        {{$daily_report->user->title}}<br>
                        {{$daily_report->user->email}}<br>
                        {{$daily_report->user->cell_phone}}
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- WEATHER INFO -->
<div class="row" style="font-size: 11px;">
    <h3>Weather Conditions</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Windy</th>
            <th class="daily-padding">Sunny</th>
            <th class="daily-padding">Overcast</th>
            <th class="daily-padding">Cloudy</th>
            <th class="daily-padding">Rain</th>
            <th class="daily-padding">Snow</th>
            <th class="daily-padding">Sleet</th>
            <th class="daily-padding">Hail</th>
            <th class="daily-padding">Lighting</th>
            <th class="daily-padding">Temperature</th>
        </tr>
        </thead>
        <tr>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_windy) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_sunny) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_overcast) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_cloudy) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_rain) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_snow) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_sleet) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_hail) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_lightning) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding">{{$daily_report->temperature}}</td>
        </tr>
    </table>
    <h3>Ground Conditions</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Dry</th>
            <th class="daily-padding">Frozen</th>
            <th class="daily-padding">Snow</th>
            <th class="daily-padding">Mud</th>
            <th class="daily-padding">Water</th>
            <th class="daily-padding">Notes</th>
        </tr>
        </thead>
        <tr>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_dry) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_frozen) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->ground_is_snow) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_mud) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding"><div class="checkbox"><label><input type="checkbox" name="is_windy" @if($daily_report->is_water) {{'checked'}} @endif><span class="checkbox-material"><span class="check"></span></span></label></div></td>
            <td class="daily-padding">{{strip_tags($daily_report->condition_notes)}}</td>
        </tr>
        <tr>
            <td colspan="11"><hr></td>
        </tr>
    </table>
</div>
<!-- PRIME CONTRACTOR LABOR -->
@if(count($daily_report->contractors) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Contractor Labor - Workers With Names</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Worker Office</th>
            <th class="daily-padding">Worker Name</th>
            <th class="daily-padding">Title / Trade</th>
            <th class="daily-padding">Description of Work</th>
            <th class="daily-padding">Regular Hours</th>
            <th class="daily-padding">Overtime Hours</th>
        </tr>
        </thead>
        <tbody>
        <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
        @foreach ($daily_report->contractors as $contractor)
            <?php $totalHours += ($contractor->regular_hours + $contractor->overtime_hours);
            $regularHours += $contractor->regular_hours;
            $overtimeHours += $contractor->overtime_hours; ?>
            <tr>
                <td class="daily-padding">{{$worker_offices[$contractor->worker_office]}}</td>
                <td class="daily-padding">{{$worker_users[$contractor->worker_name]}}</td>
                <td class="daily-padding">{{$contractor->title}}</td>
                <td class="daily-padding">{{strip_tags($contractor->description)}}</td>
                <td class="daily-padding">{{$contractor->regular_hours}}</td>
                <td class="daily-padding">{{$contractor->overtime_hours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
            <tr>
                <td colspan="3"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL</strong>
                </td>
                <td class="daily-padding">{{$regularHours}}</td>
                <td class="daily-padding">{{$overtimeHours}}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL HOURS WORKED</strong>
                </td>
                <td colspan="2" class="daily-padding text-center">{{$totalHours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        </tbody>
    </table>
</div>
@endif
@if(count($daily_report->contractorTrades) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Contractor Labor - Workers With Names</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Trade</th>
            <th class="daily-padding">Workers</th>
            <th class="daily-padding">Description of Work</th>
            <th class="daily-padding">Regular Hours</th>
            <th class="daily-padding">Overtime Hours</th>
        </tr>
        </thead>
        <tbody>
        <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
        @foreach ($daily_report->contractorTrades as $contractor)
            <?php $totalHours += (($contractor->workers * $contractor->regular_hours) + ($contractor->workers * $contractor->overtime_hours));
            $regularHours += ($contractor->workers * $contractor->regular_hours);
            $overtimeHours += ($contractor->workers * $contractor->overtime_hours); ?>
            <tr>
                <td class="daily-padding">{{$contractor->trade}}</td>
                <td class="daily-padding">{{$contractor->workers}}</td>
                <td class="daily-padding">{{strip_tags($contractor->description)}}</td>
                <td class="daily-padding">{{$contractor->regular_hours}}</td>
                <td class="daily-padding">{{$contractor->overtime_hours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
            <tr>
                <td colspan="2"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL</strong>
                </td>
                <td class="daily-padding">{{$regularHours}}</td>
                <td class="daily-padding">{{$overtimeHours}}</td>
            </tr>
            <tr>
                <td colspan="2"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL HOURS WORKED</strong>
                </td>
                <td class="daily-padding text-center">{{$totalHours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        </tbody>
    </table>
</div>
@endif
<!-- SUBCONTRACTOR CONTRACTOR LABOR -->
@if(count($daily_report->subcontractors) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Subcontractor Labor</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Company</th>
            <th class="daily-padding">Trade</th>
            <th class="daily-padding">Description of Work</th>
            <th class="daily-padding">Regular Hours</th>
            <th class="daily-padding">Overtime Hours</th>
            <th class="daily-padding">Total</th>
        </tr>
        </thead>
        <tbody>
        <?php $totalHours = 0; $regularHours = 0; $overtimeHours = 0;  ?>
        @foreach ($daily_report->subcontractors as $subcontractor)
            <?php $totalHours += ($subcontractor->workers * $subcontractor->regular_hours) + ($subcontractor->workers * $subcontractor->overtime_hours);
            $regularHours += ($subcontractor->workers * $subcontractor->regular_hours);
            $overtimeHours += ($subcontractor->workers * $subcontractor->overtime_hours); ?>
            <tr>
                @if(isset($project_companies[$subcontractor->selected_comp_id]) && !empty($project_companies[$subcontractor->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$subcontractor->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$subcontractor->selected_comp_id}}</td>
                @endif
                <td class="daily-padding">{{$subcontractor->trade}}</td>
                <td class="daily-padding">{{$subcontractor->regular_hours}}</td>
                <td class="daily-padding">{{strip_tags($subcontractor->description)}}</td>
                <td class="daily-padding">{{$subcontractor->overtime_hours}}</td>
                <td class="daily-padding">{{$subcontractor->regular_hours + $subcontractor->overtime_hours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
            <tr>
                <td colspan="3"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL</strong>
                </td>
                <td class="daily-padding">{{$regularHours}}</td>
                <td class="daily-padding">{{$overtimeHours}}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td class="text-right text-bold">
                    <strong>TOTAL HOURS WORKED</strong>
                </td>
                <td colspan="2" class="daily-padding text-center">{{$totalHours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        </tbody>
    </table>
</div>
@endif
<!-- EQUIPMENT ON SITE -->
@if(count($daily_report->equipments) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Equipment on Site</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Equipment</th>
            <th class="daily-padding">Quantity</th>
            <th class="daily-padding">Owner</th>
            <th class="daily-padding">Description of Work</th>
            <th class="daily-padding">Hours Worked</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($daily_report->equipments as $equipment)
            <tr>
                <td class="daily-padding">{{$equipment->equipment}}</td>
                <td class="daily-padding">{{$equipment->quality}}</td>
                @if(isset($project_companies[$equipment->selected_comp_id]) && !empty($project_companies[$equipment->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$equipment->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$equipment->selected_comp_id}}</td>
                @endif
                <td class="daily-padding">{{strip_tags($equipment->description)}}</td>
                <td class="daily-padding">{{$equipment->hours}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<!-- CONSTRUCTION MATERIALS DELIVERED TO SITE -->
@if(count($daily_report->materials) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Construction Materials Delivered</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Materials Delivered</th>
            <th class="daily-padding">Quantity</th>
            <th class="daily-padding">Owner</th>
            <th class="daily-padding">Notes</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($daily_report->materials as $material)
            <tr>
                <td class="daily-padding">{{$material->material_delivered}}</td>
                <td class="daily-padding">{{$material->quantity}}</td>
                @if(isset($project_companies[$material->selected_comp_id]) && !empty($project_companies[$material->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$material->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$material->selected_comp_id}}</td>
                @endif
                <td class="daily-padding">{{strip_tags($material->notes)}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<!-- TESTING / INSPECTIONS PERFORMED -->
@if(count($daily_report->inspections) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Testing / Inspections Performed</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Testing / Inspections Performed</th>
            <th class="daily-padding">Spec. Section</th>
            <th class="daily-padding">Company</th>
            <th class="daily-padding">Inspector</th>
            <th class="daily-padding">Notes</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($daily_report->inspections as $inspection)
            <tr>
                <td class="daily-padding">{{$inspection->testing_performed}}</td>
                <td class="daily-padding">{{$inspection->mf_number}} - {{$inspection->mf_title}}</td>
                @if(isset($project_companies[$inspection->selected_comp_id]) && !empty($project_companies[$inspection->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$inspection->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$inspection->other_company}}</td>
                @endif
                @if(isset($my_company_users[$inspection->selected_employee_id]) && !empty($my_company_users[$inspection->selected_employee_id]))
                    @foreach($my_company_users[$inspection->selected_employee_id] as $employeeName)
                        <td class="daily-padding">{{$employeeName}}</td>
                    @endforeach
                @elseif(isset($other_users[$inspection->selected_employee_id]) && !empty($other_users[$inspection->selected_employee_id]))
                    @foreach($other_users[$inspection->selected_employee_id] as $employeeName)
                        <td class="daily-padding">{{$employeeName}}</td>
                    @endforeach
                @else
                    <td class="daily-padding">{{$inspection->other_employee}}</td>
                @endif
                <td class="daily-padding">{{strip_tags($inspection->notes)}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<!-- SAFETY OBSERVATIONS -->
@if(count($daily_report->observations) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Safety Observation</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Safety Deficiencies Observed</th>
            <th class="daily-padding">Company</th>
            <th class="daily-padding">Notes</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($daily_report->observations as $observation)
            <tr>
                <td class="daily-padding">{{$observation->safety_deficiencies_observed}}</td>
                @if(isset($project_companies[$observation->selected_comp_id]) && !empty($project_companies[$observation->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$observation->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$observation->selected_comp_id}}</td>
                @endif
                <td class="daily-padding">{{strip_tags($observation->notes)}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<!-- LIST OF VERBAL INSTRUCTION GIVEN BY OWNER, ARCHITECT, OR ENGINEER OF RECORD -->
@if(count($daily_report->instructions) > 0)
<div class="row" style="font-size: 11px;">
    <h3>Verbal Instruction</h3>
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">Company</th>
            <th class="daily-padding">Name</th>
            <th class="daily-padding">Instructions / Verbal Approvals</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($daily_report->instructions as $instruction)
            <tr>
                @if(isset($project_companies[$instruction->selected_comp_id]) && !empty($project_companies[$instruction->selected_comp_id]))
                    <td class="daily-padding">{{$project_companies[$instruction->selected_comp_id]}}</td>
                @else
                    <td class="daily-padding">{{$instruction->selected_comp_id}}</td>
                @endif
                @if(isset($my_company_users[$instruction->selected_employee_id]) && !empty($my_company_users[$instruction->selected_employee_id]))
                    @foreach($my_company_users[$instruction->selected_employee_id] as $employeeName)
                        <td class="daily-padding">{{$employeeName}}</td>
                    @endforeach
                @elseif(isset($other_users[$instruction->selected_employee_id]) && !empty($other_users[$instruction->selected_employee_id]))
                    @foreach($other_users[$instruction->selected_employee_id] as $employeeName)
                        <td class="daily-padding">{{$employeeName}}</td>
                    @endforeach
                @endif
                <td class="daily-padding">{{strip_tags($instruction->description)}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endif
<!-- GENERAL NOTES -->
@if(!empty($daily_report->general_notes))
<div class="row" style="font-size: 11px;">
    <table style="width: 100%;">
        <thead>
        <tr>
            <th class="daily-padding">General Notes</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td class="daily-padding">{{strip_tags($daily_report->general_notes)}}</td>
            </tr>
            <tr>
                <td colspan="11"><hr></td>
            </tr>
        </tbody>
    </table>
</div>
@endif
@if($addDocuments == "true")
    <!-- GALLERY -->
    @if(isset($daily_report->countImages[0]) && $daily_report->countImages[0]->count_images > 0)
    <div class="page-break"></div>
    <div class="row" style="font-size: 11px;">
        <h3>Gallery</h3>
        <table style="width: 100%;">
            <tbody>
            @foreach ($daily_report->files as $image)
                @if($image->file_type_id == Config::get('constants.daily_report_image_file_type_id'))
                    <tr>
                        <td class="daily-padding">
                            <img src="{{env('AWS_CLOUD_FRONT').'/company_' . $image->comp_id. '/project_' . $image->proj_id . '/daily_report/' . $image->daily_report_id . '/image/' . $image->file_name}}" alt="">
                            <p class="mt10">{{$image->name}}</p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="11"><hr></td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    </div>
    @endif
    {{--<!-- ATTACHMENTS -->--}}
    {{--@if(isset($daily_report->countPdfs[0]) && $daily_report->countPdfs[0]->count_pdfs > 0)--}}
    {{--<div class="page-break"></div>--}}
    {{--<div class="row" style="font-size: 11px;">--}}
        {{--<h3>Attachments</h3>--}}
        {{--<table style="width: 100%;">--}}
            {{--<thead>--}}
            {{--<tr>--}}
                {{--<th class="daily-padding">Name</th>--}}
                {{--<th class="daily-padding">Description</th>--}}
                {{--<th class="daily-padding">Download</th>--}}
            {{--</tr>--}}
            {{--</thead>--}}
            {{--<tbody>--}}
            {{--@foreach ($daily_report->files as $attachment)--}}
                {{--@if($attachment->file_type_id == Config::get('constants.daily_report_pdf_file_type_id'))--}}
                {{--<tr>--}}
                    {{--<td class="daily-padding">{{$attachment->name}}</td>--}}
                    {{--<td class="daily-padding">{{strip_tags($attachment->description)}}</td>--}}
                    {{--<td class="daily-padding">--}}
                        {{--<a target="_blank" class="btn btn-xs btn-success download" href="{{env('AWS_CLOUD_FRONT').'/company_' . $attachment->comp_id. '/project_' . $attachment->proj_id . '/daily_report/' . $attachment->daily_report_id . '/pdf/' . $attachment->file_name}}">--}}
                            {{--{{env('AWS_CLOUD_FRONT').'/company_' . $attachment->comp_id. '/project_' . $attachment->proj_id . '/daily_report/' . $attachment->daily_report_id . '/pdf/' . $attachment->file_name}}--}}
                        {{--</a>--}}
                    {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                    {{--<td colspan="11"><hr></td>--}}
                {{--</tr>--}}
                {{--@endif--}}
            {{--@endforeach--}}
            {{--</tbody>--}}
        {{--</table>--}}
    {{--</div>--}}
    {{--@endif--}}
@endif
</body>
</html>