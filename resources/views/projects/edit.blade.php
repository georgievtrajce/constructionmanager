@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.project.edit')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                                                 class="cm-trail-link">{{trans('labels.Project')}}
                            : {{$project->name}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/edit')}}"
                                                        class="cm-trail-link">{{trans('labels.project.edit')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'projects'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            {!! Form::model($project, ['method'=> 'PUT', 'url'=>URL('projects/'.$project->id)]) !!}
            <div>
                <div class="row box-cont-1">
                    <div class="col-md-6">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    <h4>{{trans('labels.general')}}</h4>
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('name', trans('labels.project.name').':') !!}
                                                        {!! Form::text('name', $project->name, ['class' => 'form-control']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('number', trans('labels.project.number').':') !!}
                                                        {!! Form::text('number', $project->number, ['class' => 'form-control']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cm-spacer-xs"></div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('upc_code', trans('labels.project.upc_code').':') !!}
                                                        {!! Form::text('upc_code', $project->upc_code, ['class' => 'form-control', 'disabled'=>'true']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::label('active', trans('labels.project.status').':') !!}
                                                        {!! Form::select('active', [1=>'Active',0=>'Completed'] ,Input::get('active'), ['class' =>
                                                        'form-control']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cm-spacer-xs"></div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        {!! Form::label('street', trans('labels.Address').':') !!}
                                                        @if(!is_null($project->address))
                                                            {!! Form::text('street', $project->address->street, ['class' => 'form-control']) !!}
                                                        @else
                                                            {!! Form::text('street', Input::get('street'), ['class' => 'form-control']) !!}
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('city', trans('labels.city').':') !!}
                                                        @if(!is_null($project->address))
                                                            {!! Form::text('city', $project->address->city, ['class' => 'form-control']) !!}
                                                        @else
                                                            {!! Form::text('city', Input::get('city'), ['class' => 'form-control']) !!}
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('state', trans('labels.state').':') !!}
                                                        @if(!is_null($project->address))
                                                            {!! Form::text('state', $project->address->state, ['class' => 'form-control']) !!}
                                                        @else
                                                            {!! Form::text('state', '', ['class' => 'form-control']) !!}
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('zip', trans('labels.zip').':') !!}
                                                        @if(!is_null($project->address))
                                                            {!! Form::text('zip', $project->address->zip, ['class' => 'form-control']) !!}
                                                        @else
                                                            {!! Form::text('zip', '', ['class' => 'form-control']) !!}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cm-spacer-xs"></div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('start_date', trans('labels.project.start_date').':') !!}
                                                        {!! Form::text('start_date', ($project->start_date != 0) ?
                                                        Carbon::parse($project->start_date)->format('m/d/Y') : '', ['class' =>
                                                        'datepicker','data-date-format' =>
                                                        'mm/dd/yyyy']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('end_date', trans('labels.project.end_date').':') !!}
                                                        {!! Form::text('end_date', ($project->end_date != 0) ?
                                                        Carbon::parse($project->end_date)->format('m/d/Y') :
                                                        '', ['class' => 'datepicker','data-date-format' => 'mm/dd/yyyy']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        {!! Form::label('price', trans('labels.project.value').':') !!}
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            {!! Form::text('price', number_format($project->price,2), ['class' => 'form-control']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4>{{trans('labels.specification_version')}}</h4>
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::select('master_format_type', $masterFormatTypes, $project->master_format_type_id, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    <h4>{{trans('labels.project-stakeholders')}}</h4>
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('owner', trans('labels.project.owner').' ('.trans('labels.import_from_ab').'):')
                                                !!}
                                                @if(isset($project->owner->name))
                                                    {!! Form::text('owner', $project->owner->name, ['class' => 'form-control', 'id'=>'owner_ab']) !!}
                                                    <input type="hidden" id="owner_id" name="owner_id"
                                                           value="{{$project->owner->id}}"/>
                                                @else
                                                    {!! Form::text('owner', Input::get('owner_ab'), ['class' => 'form-control', 'id'=>'owner_ab']) !!}
                                                    <input type="hidden" id="owner_id" name="owner_id"
                                                           value="{{Input::old('owner_id')}}"/>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" name="defaultRecipient" data-type="default"
                                                       id="default_recipient_owner"
                                                       @if($project->owner_transmittal_id || (empty($project->owner_transmittal_id) && empty($project->architect_transmittal_id) && empty($project->contractor_transmittal_id) && empty($project->prime_subcontractor_transmittal_id))) checked="checked" @endif>
                                                {{trans('labels.transmittals.default_recipient')}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="select_contact_owner">
                                                <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                                                <select class="col-md-12" name="owner_transmittal_id"
                                                        id="owner_transmittal_id">
                                                    <option value="0"></option>
                                                    @if(count($ownerTransmittals))
                                                        @foreach($ownerTransmittals as $transmittal)
                                                            <option value="{{$transmittal->id}}" @if($project->owner_transmittal_id == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">No available contacts for this company</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cm-spacer-xs"></div>
                                    <hr class="mt5 mb5">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('architect', trans('labels.project.architect').'
                                                ('.trans('labels.import_from_ab').'):') !!}
                                                @if(isset($project->architect->name))
                                                    {!! Form::text('architect', $project->architect->name, ['class' =>
                                                    'form-control','id'=>'architect_ab']) !!}
                                                    <input type="hidden" id="architect_id" name="architect_id"
                                                           value="{{$project->architect->id}}"/>
                                                @else
                                                    {!! Form::text('architect', Input::get('architect_ab'), ['class' => 'form-control',
                                                    'id'=>'architect_ab']) !!}
                                                    <input type="hidden" id="architect_id" name="architect_id"
                                                           value="{{Input::old('architect_id')}}"/>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" name="defaultRecipient" data-type="default"
                                                       id="default_recipient_architect"
                                                       @if($project->architect_transmittal_id) checked="checked" @endif>
                                                {{trans('labels.transmittals.default_recipient')}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="select_contact_architect">
                                                <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                                                <select class="col-md-12" name="architect_transmittal_id"
                                                        id="architect_transmittal_id">
                                                    <option value="0"></option>
                                                    @if(count($architectTransmittals))
                                                        @foreach($architectTransmittals as $transmittal)
                                                            <option value="{{$transmittal->id}}" @if($project->architect_transmittal_id == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">No available contacts for this company</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cm-spacer-xs"></div>
                                    <hr class="mt5 mb5">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>
                                                    {{trans('labels.submittals.general_contractor').' ('.trans('labels.import_from_ab').'):'}}
                                                </label>
                                                <div class="row">
                                                    <div class="col-md-12 general_contractor_container"
                                                         @if($project->make_me_gc) style="display: none;" @endif>
                                                        @if(isset($project->generalContractor->name))
                                                            <input type="text" class="form-control address-book-gc-auto"
                                                                   name="general_contractor"
                                                                   value="{{$project->generalContractor->name}}">
                                                            <input type="hidden" class="address-book-gc-id"
                                                                   name="general_contractor_id"
                                                                   value="{{$project->generalContractor->id}}">
                                                        @else
                                                            <input type="text" class="form-control address-book-gc-auto"
                                                                   name="general_contractor" value="">
                                                            <input type="hidden" class="address-book-gc-id"
                                                                   name="general_contractor_id"
                                                                   value="{{Input::old('general_contractor_id')}}">
                                                        @endif
                                                    </div>
                                                    <div class="col-md-12 mt10">
                                                        <input type="checkbox" name="make_me_gc" id="make_me_gc"
                                                               @if($project->make_me_gc) checked="checked" @endif>
                                                        {{trans('labels.submittals.general_contractor_me')}}
                                                        <input type="hidden" name="make_me_gc_id" id="make_me_gc_id"
                                                               value="{{Auth::user()->comp_id}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" name="defaultRecipient" data-type="default"
                                                       id="default_recipient_contractor"
                                                       @if($project->contractor_transmittal_id) checked="checked" @endif>
                                                {{trans('labels.transmittals.default_recipient')}}
                                            </div>
                                        </div>
                                        <div class="col-md-6 general_contractor_container"
                                             @if($project->make_me_gc) style="display: none;" @endif>
                                            <div id="select_contact_contractor" @if(!isset($project->generalContractor->name)) @endif>
                                                <div class="form-group">
                                                    <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                                                    <select class="col-md-12" name="contractor_transmittal_id"
                                                            id="contractor_transmittal_id">
                                                        <option value="0"></option>
                                                        @if(count($contractorTransmittals))
                                                            @foreach($contractorTransmittals as $transmittal)
                                                                <option value="{{$transmittal->id}}" @if($project->contractor_transmittal_id == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="">No available contacts for this company
                                                            </option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cm-spacer-xs"></div>
                                    <hr class="mt5 mb5">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {!! Form::label('primeSubcontractor', trans('labels.project.prime_subcontractor').'
                                                ('.trans('labels.import_from_ab').'):') !!}
                                                @if($project->primeSubcontractor != null)
                                                    {!! Form::text('prime_subcontractor', $project->primeSubcontractor->name, ['class' =>
                                                    'form-control','id'=>'prime_subcontractor_ab']) !!}
                                                    <input type="hidden" id="prime_subcontractor_id"
                                                           name="prime_subcontractor_id"
                                                           value="{{$project->primeSubcontractor->id}}"/>
                                                @else

                                                    {!! Form::text('prime_subcontractor', Input::get('prime_subcontractor_ab'), ['class' => 'form-control',
                                                    'id'=>'prime_subcontractor_ab']) !!}
                                                    <input type="hidden" id="prime_subcontractor_id"
                                                           name="prime_subcontractor_id"
                                                           value="{{Input::old('prime_subcontractor_id')}}"/>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" name="defaultRecipient" data-type="default"
                                                       id="default_recipient_prime_subcontractor"
                                                       @if($project->prime_subcontractor_transmittal_id) checked="checked" @endif>
                                                {{trans('labels.transmittals.default_recipient')}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="select_contact_prime_subcontractor">
                                                <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                                                <select class="col-md-12" name="prime_subcontractor_transmittal_id"
                                                        id="prime_subcontractor_transmittal_id">
                                                    <option value="0"></option>
                                                    @if(count($primeSubcontractorTransmittals))
                                                        @foreach($primeSubcontractorTransmittals as $transmittal)
                                                            <option value="{{$transmittal->id}}" @if($project->prime_subcontractor_transmittal_id == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                                        @endforeach
                                                    @else
                                                        <option value="">No available contacts for this company</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cm-spacer-xs"></div>
                                    <hr class="mt5 mb5">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <div class="form-group pull-right">
                                                    {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
                                                </div>
                                            </div>
                                            <input type="hidden" id="transmittal_flag" name="transmittal_flag"
                                                   value=""/>
                                            <input type="hidden" id="project_id" name="project_id"
                                                   value="{{$project->id}}"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            @include('errors.list')
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
        </div>
    </div>
@endsection