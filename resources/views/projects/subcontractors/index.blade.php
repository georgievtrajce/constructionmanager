@extends('layouts.master')

@section('content')
    <div class="container-fluid container-inset">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-12">
                        Name: {{$project->name}}
                        <ul class="nav nav-pills" role="tablist">
                            <li role="presentation"><a href="/projects/{{$project->id}}">Project Info</a></li>
                            <li role="presentation"><a href="/projects/{{$project->id}}/companies">Companies</a></li>
                            <li role="presentation" class="active"><a href="/projects/{{$project->id}}/subcontractors">Subcontractors</a></li>
                            <li role="presentation"><a href="/projects/{{$project->id}}/proposals">Proposals</a></li>
                            <li role="presentation"><a href="/projects/{{$project->id}}/submittals">Submittals</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif

                {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/subcontractors']) !!}
                <div class="row">
                    <h4>Add Subcontractor</h4>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('company', 'Company:') !!}
                            {!! Form::text('proj_sub', Input::old('proj_sub'), ['class' => 'form-control', 'id'=>'proj_sub']) !!}
                        </div>
                        <input type="hidden" id="company_id" name="comp_id" value="{{Request::old('comp_id')}}"/>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            {!! Form::label('address', 'Address:') !!}
                            <select name="addresses" id="addresses">
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <input type="hidden" id="proj_id" name="proj_id" value="{{$project->id}}"/>
                        <div class="form-group">
                            {!! Form::label('save', '') !!}
                            {!! Form::submit('Save',['class' => 'form-control btn btn-sm btn-primary']) !!}
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::close() !!}
            @include('errors.list')

            {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/subcontractors']) !!}
            <div class="row">
                <h4>Sort Subcontractors</h4>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('sort', 'Sort By:') !!} </br>
                        {!! Form::select('sort', array('proj_sub_id'=>'ID','name'=>'Name','office_title'=>'Location'), Input::get('sort')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('order', 'Order By:') !!} 
                        {!! Form::select('order', array('asc'=>'Ascending','desc'=>'Descending'),Input::get('order')) !!}
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        {!! Form::label('filter', ' &nbsp;') !!}
                        {!! Form::submit('Filter',['class' => 'form-control btn btn-sm btn-primary']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            @include('projects.subcontractors.table', ['subcontractors'=>$subcontractors,'childsubcontractors'=>$childsubcontractors])
        </div>
    </div>
    </div>
@endsection