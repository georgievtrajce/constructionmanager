<table class="table table-hover table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Company Name</th>
        <th>Location</th>
        <th>Contact Person</th>
        <th>Title</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Status</th>
    </tr>
    </thead>
    <tbody>
    @for($i=0;$i<sizeof($subcontractors);$i++)
        <tr>
            <td>{{$subcontractors[$i]->proj_sub_id}}</td>
            <td>{{$subcontractors[$i]->name}}</td>
            <td>
                <select class="sub_addr" name="sub_addr" id="sub_addr_{{$subcontractors[$i]->proj_sub_id}}">
                    @for($j=0;$j<sizeof($subcontractors[$i]->addresses);$j++)
                        <option @if($subcontractors[$i]->addresses[$j]->id == $subcontractors[$i]->address_id)
                                selected="selected"
                                @endif
                            value={{$subcontractors[$i]->addresses[$j]->id}}>{{$subcontractors[$i]->addresses[$j]->office_title}}</option>
                    @endfor
                </select></td>
            <td>{{$subcontractors[$i]->users[0]->name}}</td>
            <td>{{$subcontractors[$i]->users[0]->title}}</td>
            <td>{{$subcontractors[$i]->users[0]->office_phone}}</td>
            <td>{{$subcontractors[$i]->users[0]->email}}</td>
            <td>
                @if ($subcontractors[$i]->status == 1)
                Pending
                @elseif($subcontractors[$i]->status == 2)
                Active
                @endif
            </td>
            <td>
                <?php
                    //format child subcontractors link
                $link[$i] = '';
                for($j=0;$j<sizeof($childsubcontractors[$subcontractors[$i]->comp_child_id]);$j++){
                    $link[$i] .= "<a href ='".URL::to('companies/'.$childsubcontractors[$subcontractors[$i]->comp_child_id][$j]->child_company_single->id)."'>".$childsubcontractors[$subcontractors[$i]->comp_child_id][$j]->child_company_single->name."</a>, ";
                }
                if (empty($link[$i])){
                   $link[$i] = 'No Subcontractors';
                }
                ?>
                <button type="button" class="btn btn-primary" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-content="{{$link[$i]}}">
                    View Subcontractors
                </button>
            </td>
            <td>
                {!! Form::open(['method'=>'DELETE', 'url'=>(Request::path().'/'.$subcontractors[$i]->id)]) !!}
                <input type="hidden" name="proj_sub_id" value="{{$subcontractors[$i]->proj_sub_id}}"/>
                {!! Form::submit('Delete',['class'=>'btn btn-danger','onclick' => 'return confirm("Are you sure that you want to delete this record?")']) !!}
                {!! Form::close()!!}
            </td>
        </tr>
    @endfor
    </tbody>
</table>