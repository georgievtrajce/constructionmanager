<div class="form-group visible-xs visible-sm">
    <select class="form-control" id="sel1">
         @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'drawings')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/drawings/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'drawings') {{'selected'}} @endif>{{trans('labels.files.drawings').' ('.CustomHelper::countProjectFiles('drawings', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'specifications')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/specifications/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'specifications') {{'selected'}} @endif>{{trans('labels.files.specifications').' ('.CustomHelper::countProjectFiles('specifications', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'addendums')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/addendums/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'addendums') {{'selected'}} @endif>{{trans('labels.files.addendums').' ('.CustomHelper::countProjectFiles('addendums', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'correspondence-emails')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/correspondence-emails/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'correspondence-emails') {{'selected'}} @endif>{{trans('labels.files.correspondence-emails').' ('.CustomHelper::countProjectFiles('correspondence-emails', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

         @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'daily-reports')))
             <option data-href="{{URL('/projects/'.$project->id.'/project-files/daily-reports/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'daily-reports') {{'selected'}} @endif>{{trans('labels.files.daily-reports').' ('.CustomHelper::countDailyReportsFiles($project->id, Auth::user()->comp_id).')'}}</option>
         @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'inspection-reports')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/inspection-reports/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'inspection-reports') {{'selected'}} @endif>{{trans('labels.files.inspection-reports').' ('.CustomHelper::countProjectFiles('inspection-reports', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'permits')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/permits/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'permits') {{'selected'}} @endif>{{trans('labels.files.permits').' ('.CustomHelper::countProjectFiles('permits', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'project-photos')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/project-photos/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'project-photos') {{'selected'}} @endif>{{trans('labels.files.project-photos').' ('.CustomHelper::countProjectFiles('project-photos', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'work-schedules')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/work-schedules/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'work-schedules') {{'selected'}} @endif>{{trans('labels.files.work-schedules').' ('.CustomHelper::countProjectFiles('work-schedules', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'punch-lists')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/punch-lists/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'punch-lists') {{'selected'}} @endif>{{trans('labels.files.punch-lists').' ('.CustomHelper::countProjectFiles('punch-lists', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'meeting-minutes')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/meeting-minutes/file')}}" class="cm-tab-item @if(Request::segment(4) == 'meeting-minutes') {{'selected'}} @endif">{{trans('labels.files.meeting-minutes').' ('.CustomHelper::countProjectFiles('meeting-minutes', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'closeout-documents')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/closeout-documents/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'closeout-documents') {{'selected'}} @endif>{{trans('labels.files.closeout-documents').' ('.CustomHelper::countProjectFiles('closeout-documents', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'billing')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/billing/file')}}" @if(Request::segment(4) == 'billing') {{'selected'}} @endif class="cm-tab-item">{{trans('labels.files.billing').' ('.CustomHelper::countProjectFiles('billing', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif

        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'miscellaneous')))
        <option data-href="{{URL('/projects/'.$project->id.'/project-files/miscellaneous/file')}}" class="cm-tab-item" @if(Request::segment(4) == 'miscellaneous') {{'selected'}} @endif>{{trans('labels.files.miscellaneous').' ('.CustomHelper::countProjectFiles('miscellaneous', $project->id, Auth::user()->comp_id).')'}}</option>
        @endif
 
    </select>
</div>


<ul class="cm-tab cm-tab-vertical-alt hidden-xs hidden-sm" role="tablist">
    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'drawings')))
    <li class="cm-tab-item @if(Request::segment(4) == 'drawings') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/drawings/file')}}">{{trans('labels.files.drawings').' ('.CustomHelper::countProjectFiles('drawings', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'specifications')))
    <li class="cm-tab-item @if(Request::segment(4) == 'specifications') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/specifications/file')}}">{{trans('labels.files.specifications').' ('.CustomHelper::countProjectFiles('specifications', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'addendums')))
    <li class="cm-tab-item @if(Request::segment(4) == 'addendums') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/addendums/file')}}">{{trans('labels.files.addendums').' ('.CustomHelper::countProjectFiles('addendums', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'correspondence-emails')))
    <li class="cm-tab-item @if(Request::segment(4) == 'correspondence-emails') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/correspondence-emails/file')}}">{{trans('labels.files.correspondence-emails').' ('.CustomHelper::countProjectFiles('correspondence-emails', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'daily-reports')))
        <li class="cm-tab-item @if(Request::segment(4) == 'daily-reports') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/daily-reports/file')}}">{{trans('labels.files.daily-reports').' ('.CustomHelper::countDailyReportsFiles($project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'inspection-reports')))
    <li class="cm-tab-item @if(Request::segment(4) == 'inspection-reports') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/inspection-reports/file')}}">{{trans('labels.files.inspection-reports').' ('.CustomHelper::countProjectFiles('inspection-reports', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'permits')))
    <li class="cm-tab-item @if(Request::segment(4) == 'permits') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/permits/file')}}">{{trans('labels.files.permits').' ('.CustomHelper::countProjectFiles('permits', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'project-photos')))
    <li class="cm-tab-item @if(Request::segment(4) == 'project-photos') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/project-photos/file')}}">{{trans('labels.files.project-photos').' ('.CustomHelper::countProjectFiles('project-photos', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'work-schedules')))
    <li class="cm-tab-item @if(Request::segment(4) == 'work-schedules') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/work-schedules/file')}}">{{trans('labels.files.work-schedules').' ('.CustomHelper::countProjectFiles('work-schedules', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'punch-lists')))
    <li class="cm-tab-item @if(Request::segment(4) == 'punch-lists') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/punch-lists/file')}}">{{trans('labels.files.punch-lists').' ('.CustomHelper::countProjectFiles('punch-lists', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'meeting-minutes')))
    <li class="cm-tab-item @if(Request::segment(4) == 'meeting-minutes') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/meeting-minutes/file')}}">{{trans('labels.files.meeting-minutes').' ('.CustomHelper::countProjectFiles('meeting-minutes', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'closeout-documents')))
    <li class="cm-tab-item @if(Request::segment(4) == 'closeout-documents') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/closeout-documents/file')}}">{{trans('labels.files.closeout-documents').' ('.CustomHelper::countProjectFiles('closeout-documents', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'contracts')))
    <!--<li class="cm-tab-item @if(Request::segment(4) == 'contracts') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/contracts/file')}}">{{trans('labels.files.contracts').' ('.CustomHelper::countProjectFiles('contracts', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
    <li class="cm-tab-item @if(Request::segment(4) == 'bids') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/bids/file')}}">{{trans('labels.files.proposals').' ('.CustomHelper::countProjectFiles('bids', $project->id, Auth::user()->comp_id).')'}}</a></li>-->
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'billing')))
    <li class="cm-tab-item @if(Request::segment(4) == 'billing') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/billing/file')}}">{{trans('labels.files.billing').' ('.CustomHelper::countProjectFiles('billing', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif

    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'miscellaneous')))
    <li class="cm-tab-item @if(Request::segment(4) == 'miscellaneous') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/project-files/miscellaneous/file')}}">{{trans('labels.files.miscellaneous').' ('.CustomHelper::countProjectFiles('miscellaneous', $project->id, Auth::user()->comp_id).')'}}</a></li>
    @endif
</ul>