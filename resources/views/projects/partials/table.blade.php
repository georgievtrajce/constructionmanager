<table class="table table-hover table-bordered cm-table-compact">
    <thead>
        <tr>
         <th></th>
            <th>
                <?php
                if (Input::get('sort') == 'name' && Input::get('order') == 'desc') {
                    $url = Request::url().'?sort=name&order=asc';
                } else {
                    $url = Request::url().'?sort=name&order=desc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                ?>
                {{trans('labels.project.name')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'number' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=number&order=desc';
                } else {
                    $url = Request::url().'?sort=number&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                ?>
                {{trans('labels.project.number')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                {{trans('labels.project.location')}}
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'start_date' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=start_date&order=desc';
                } else {
                    $url = Request::url().'?sort=start_date&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                ?>
                {{trans('labels.project.start_date')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'end_date' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=end_date&order=desc';
                } else {
                    $url = Request::url().'?sort=end_date&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                ?>
                {{trans('labels.project.end_date')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'price' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=price&order=desc';
                } else {
                    $url = Request::url().'?sort=price&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                ?>
                {{trans('labels.project.value')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                {{trans('labels.project.creator')}}
            </th>
            <th>
                {{trans('labels.role')}}
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($projects as $project)
        <tr>
            <td class="text-center">
                @if ((Auth::user()->comp_id == $project->comp_id) && ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('delete', 'projects')) || (Auth::user()->id == $project->proj_admin)))
                    <input type="checkbox" name="select_all" class="multiple-items-checkbox-projects" data-id="{{$project->id}}" data-subid="">
                @elseif (count($project->projectSubcontractorsSelf) > 0 && count($project->projectBiddersSelf) > 0 && Auth::user()->hasRole('Company Admin'))
                    <input type="checkbox" name="select_all" class="multiple-items-checkbox-projects" data-id="{{$project->id}}" data-subid="{{$project->projectSubcontractorsSelf[0]->id}}" data-bidid="{{$project->projectBiddersSelf[0]->id}}">
                @elseif (count($project->projectSubcontractorsSelf) && Auth::user()->hasRole('Company Admin'))
                    <input type="checkbox" name="select_all" class="multiple-items-checkbox-projects" data-id="{{$project->id}}" data-subid="{{$project->projectSubcontractorsSelf[0]->id}}" data-bidid="0">
                @elseif (count($project->projectBiddersSelf) > 0 && Auth::user()->hasRole('Company Admin'))
                    <input type="checkbox" name="select_all" class="multiple-items-checkbox-projects" data-id="{{$project->id}}" data-subid="0" data-bidid="{{$project->projectBiddersSelf[0]->id}}">
                @endif
            </td>
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && !is_null($project->subcontractorProjectInfo) && !empty($project->subcontractorProjectInfo->name))
                    <a href="{{URL('projects/'.$project->id)}}" >{{$project->subcontractorProjectInfo->name}}</a>
                @else
                    <a href="{{URL('projects/'.$project->id)}}" >{{$project->name}}</a>
                @endif
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && !is_null($project->subcontractorProjectInfo) && !empty($project->subcontractorProjectInfo->number))
                    {{$project->subcontractorProjectInfo->number}}
                @else
                    {{$project->number}}
                @endif
            </td>
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && !is_null($project->subcontractorAddress))
                    {{$project->subcontractorAddress->city}}
                    @if (!empty($project->subcontractorAddress->state))
                        ,
                        {{$project->subcontractorAddress->state}}
                    @endif
                @else
                    @if (!is_null($project->address))
                        @if (!empty($project->address->city))
                            {{$project->address->city}}
                        @endif
                        @if (!empty($project->address->state))
                            ,
                            {{$project->address->state}}
                        @endif
                    @endif
                @endif
            </td>
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && (!is_null($project->subcontractorProjectInfo) && ($project->subcontractorProjectInfo->start_date != 0)))
                    {{date("m/d/Y", strtotime($project->subcontractorProjectInfo->start_date))}}
                @else
                    @if(!empty($project->start_date) && $project->start_date != 0)
                        {{date("m/d/Y", strtotime($project->start_date))}}
                    @endif
                @endif
            </td>
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && (!is_null($project->subcontractorProjectInfo) && ($project->subcontractorProjectInfo->end_date != 0)))
                    {{date("m/d/Y", strtotime($project->subcontractorProjectInfo->end_date))}}
                @else
                    @if(!empty($project->end_date) && $project->end_date != 0)
                        {{date("m/d/Y", strtotime($project->end_date))}}
                    @endif
                @endif
            </td>
            <td>
                @if(Auth::user()->comp_id != $project->comp_id && !is_null($project->subcontractorProjectInfo))
                    {{'$'.number_format($project->subcontractorProjectInfo->price,2)}}
                @else
                    {{'$'.number_format($project->price,2)}}
                @endif
            </td>
            <td>@if (!is_null($project->company)) {{$project->company->name}} @endif</td>
            <td>{{CustomHelper::getCompanyRoleForProject($project)}}</td>

        </tr>
        @endforeach
    </tbody>
</table>