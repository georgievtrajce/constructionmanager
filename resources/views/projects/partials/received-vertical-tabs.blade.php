<ul class="cm-tab cm-tab-vertical-alt" role="tablist">
    <li class="cm-tab-item @if(Request::segment(5) == 'drawings') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                          href="{{URL('/projects/'.$project->id.'/received/project-files/drawings/file')}}">{{trans('labels.files.drawings').' ('.CustomHelper::countReceivedProjectFiles('drawings', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'specifications') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                href="{{URL('/projects/'.$project->id.'/received/project-files/specifications/file')}}">{{trans('labels.files.specifications').' ('.CustomHelper::countReceivedProjectFiles('specifications', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'addendums') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                           href="{{URL('/projects/'.$project->id.'/received/project-files/addendums/file')}}">{{trans('labels.files.addendums').' ('.CustomHelper::countReceivedProjectFiles('addendums', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'submittals') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/submittals/file')}}">{{trans('labels.files.submittals').' ('.CustomHelper::countReceivedProjectFiles('submittals', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a></li>
    <li class="cm-tab-item @if(Request::segment(5) == 'rfis') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/rfis/file')}}">{{trans('labels.files.rfis').' ('.CustomHelper::countReceivedProjectFiles('rfis', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a></li>
    <li class="cm-tab-item @if(Request::segment(5) == 'pcos') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/pcos/file')}}">{{trans('labels.files.pcos').' ('.CustomHelper::countReceivedProjectFiles('pcos', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a></li>
    <li class="cm-tab-item @if(Request::segment(5) == 'correspondence-emails') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/correspondence-emails/file')}}">{{trans('labels.files.correspondence-emails').' ('.CustomHelper::countReceivedProjectFiles('correspondence-emails', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'inspection-reports') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                    href="{{URL('/projects/'.$project->id.'/received/project-files/inspection-reports/file')}}">{{trans('labels.files.inspection-reports').' ('.CustomHelper::countReceivedProjectFiles('inspection-reports', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'permits') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                         href="{{URL('/projects/'.$project->id.'/received/project-files/permits/file')}}">{{trans('labels.files.permits').' ('.CustomHelper::countReceivedProjectFiles('permits', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'project-photos') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                href="{{URL('/projects/'.$project->id.'/received/project-files/project-photos/file')}}">{{trans('labels.files.project-photos').' ('.CustomHelper::countReceivedProjectFiles('project-photos', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'work-schedules') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                href="{{URL('/projects/'.$project->id.'/received/project-files/work-schedules/file')}}">{{trans('labels.files.work-schedules').' ('.CustomHelper::countReceivedProjectFiles('work-schedules', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'punch-lists') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                             href="{{URL('/projects/'.$project->id.'/received/project-files/punch-lists/file')}}">{{trans('labels.files.punch-lists').' ('.CustomHelper::countReceivedProjectFiles('punch-lists', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'meeting-minutes') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                 href="{{URL('/projects/'.$project->id.'/received/project-files/meeting-minutes/file')}}">{{trans('labels.files.meeting-minutes').' ('.CustomHelper::countReceivedProjectFiles('meeting-minutes', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'closeout-documents') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                    href="{{URL('/projects/'.$project->id.'/received/project-files/closeout-documents/file')}}">{{trans('labels.files.closeout-documents').' ('.CustomHelper::countReceivedProjectFiles('closeout-documents', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    {{--<li class="cm-tab-item @if(Request::segment(5) == 'contracts') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/contracts/file')}}">{{trans('labels.files.contracts').' ('.CustomHelper::countReceivedProjectFiles('contracts', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a></li>--}}
    {{--<li class="cm-tab-item @if(Request::segment(5) == 'proposals') {{'active'}} @endif"><a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/received/project-files/proposals/file')}}">{{trans('labels.files.proposals').' ('.CustomHelper::countReceivedProjectFiles('proposals', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a></li>--}}
    <li class="cm-tab-item @if(Request::segment(5) == 'billing') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                     href="{{URL('/projects/'.$project->id.'/received/project-files/billing/file')}}">{{trans('labels.files.billing').' ('.CustomHelper::countReceivedProjectFiles('billing', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'miscellaneous') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                           href="{{URL('/projects/'.$project->id.'/received/project-files/miscellaneous/file')}}">{{trans('labels.files.miscellaneous').' ('.CustomHelper::countReceivedProjectFiles('miscellaneous', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
</li>
</ul>