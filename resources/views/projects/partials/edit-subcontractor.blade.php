<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('name', trans('labels.project.name').':') !!}
            {!! Form::text('name', $project->subcontractorProjectInfo->name, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('number', trans('labels.project.number').':') !!}
            {!! Form::text('number', $project->subcontractorProjectInfo->number, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('upc_code', trans('labels.project.upc_code').':') !!}
            {!! Form::text('upc_code', $project->upc_code, ['class' => 'form-control', 'disabled'=>'true']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('active', trans('labels.project.status').':') !!}
            {!! Form::select('active', [1=>'Active',0=>'Completed'] ,$project->subcontractorProjectInfo->active, ['class' => 'form-control']) !!}
        </div>
    </div>

</div>


<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('street', trans('labels.Address').':') !!}
            @if(!is_null($project->subcontractorAddress))
                {!! Form::text('street', $project->subcontractorAddress->street, ['class' => 'form-control']) !!}
            @else
                {!! Form::text('street', Input::get('street'), ['class' => 'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('city', trans('labels.city').':') !!}
            @if(!is_null($project->subcontractorAddress))
                {!! Form::text('city', $project->subcontractorAddress->city, ['class' => 'form-control']) !!}
            @else
                {!! Form::text('city', Input::get('city'), ['class' => 'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('zip', trans('labels.zip').':') !!}
            @if(!is_null($project->subcontractorAddress))
                {!! Form::text('zip', $project->subcontractorAddress->zip, ['class' => 'form-control']) !!}
            @else
                {!! Form::text('zip', '', ['class' => 'form-control']) !!}
            @endif
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('state', trans('labels.state').':') !!}
            @if(!is_null($project->subcontractorAddress))
                {!! Form::text('state', $project->subcontractorAddress->state, ['class' => 'form-control']) !!}
            @else
                {!! Form::text('state', '', ['class' => 'form-control']) !!}
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('start_date', trans('labels.project.start_date').':') !!}
            {!! Form::text('start_date', ($project->subcontractorProjectInfo->start_date != 0) ? Carbon::parse($project->subcontractorProjectInfo->start_date)->format('m/d/Y') : '', ['class' => 'datepicker','data-date-format' => 'mm/dd/yyyy']) !!}
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('end_date', trans('labels.project.end_date').':') !!}
            {!! Form::text('end_date', ($project->subcontractorProjectInfo->end_date != 0) ? Carbon::parse($project->subcontractorProjectInfo->end_date)->format('m/d/Y') : '', ['class' => 'datepicker','data-date-format' => 'mm/dd/yyyy']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('price', trans('labels.project.value').':') !!}
            <div class="input-group">
                <span class="input-group-addon">$</span>
                {!! Form::text('price', number_format($project->subcontractorProjectInfo->price,2), ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('owner', trans('labels.project.owner').' ('.trans('labels.import_from_ab').'):') !!}
            @if(!is_null($project->subcontractorProjectInfo) && !is_null($project->subcontractorProjectInfo->owner) && !empty($project->subcontractorProjectInfo->owner->name))
                {!! Form::text('owner', $project->subcontractorProjectInfo->owner->name, ['class' => 'form-control', 'id'=>'owner_ab']) !!}
                <input type="hidden" id="owner_id" name="owner_id" value="{{$project->subcontractorProjectInfo->owner->id}}"/>
            @else
                {!! Form::text('owner', '', ['class' => 'form-control', 'id'=>'owner_ab']) !!}
                <input type="hidden" id="owner_id" name="owner_id" value=""/>
            @endif
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            {!! Form::label('architect', trans('labels.project.architect').' ('.trans('labels.import_from_ab').'):') !!}
            @if(!is_null($project->subcontractorProjectInfo) && !is_null($project->subcontractorProjectInfo->architect) && !empty($project->subcontractorProjectInfo->architect->name))
                {!! Form::text('architect', $project->subcontractorProjectInfo->architect->name, ['class' => 'form-control','id'=>'architect_ab']) !!}
                <input type="hidden" id="architect_id" name="architect_id" value="{{$project->subcontractorProjectInfo->architect->id}}"/>
            @else
                {!! Form::text('architect', '', ['class' => 'form-control', 'id'=>'architect_ab']) !!}
                <input type="hidden" id="architect_id" name="architect_id" value="{{Input::old('architect_id')}}"/>
            @endif
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="contacts">{{trans('labels.contact.plural')}}:</label><br />
            <a href="{{URL('/address-book/create')}}" class="btn btn-primary">{{trans('labels.contact.add_address_book_entry')}}</a>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group pull-right">
            {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>