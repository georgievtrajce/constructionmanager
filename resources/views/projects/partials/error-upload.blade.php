<script class="error-upload" type="text/template">
    <style>
        .bg-danger {
            background-color: #f2dede;
        }

        .error_message_cont {
            float: left;
            font-size: 12px;
            color: #D64933;
        }

        .error_upload_message{
            float: left;
            margin: 5px 0px;
            padding: 5px 5px 5px 10px;
        }
    </style>
    <div class="bg-danger error_message_cont">
        <span class="error_upload_message">{{trans('labels.upload_file_template.error_upload')}}</span>
    </div>
</script>