<script class="upload-limitation" type="text/template">
    <style>
        .bg-warning {
            background-color: #fcf8e3;
        }

        .limitation_message_cont {
            float: left;
            font-size: 12px;
            color: #D64933;
        }

        .limitation_upload_message{
            float: left;
            margin: 5px 0px;
            padding: 5px 5px 5px 10px;
        }
    </style>
    <div class="bg-warning limitation_message_cont">
        <span class="limitation_upload_message">
            {{trans('labels.upload_file_template.limitation_upload_part1')}}
                <%= type %>
            {{trans('labels.upload_file_template.limitation_upload_part2')}}
        </span>
    </div>
</script>