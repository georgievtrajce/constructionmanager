<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('name', trans('labels.project.name').':') !!}
            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('number', trans('labels.project.number').':') !!}
            <input type="text" class="form-control" name="number" value="{{ old('number') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('active', trans('labels.project.status').':') !!}
            {!! Form::select('active', [1=>'Active',0=>'Completed'] ,Input::get('active'), ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">

    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('street', trans('labels.Address').':') !!}
            <input type="text" class="form-control" name="street" value="{{ old('street') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('city', trans('labels.city').':') !!}
            <input type="text" class="form-control" name="city" value="{{ old('city') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('zip', trans('labels.zip').':') !!}
            <input type="text" class="form-control" name="zip" value="{{ old('zip') }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('state', trans('labels.state').':') !!}
            <input type="text" class="form-control" name="state" value="{{ old('state') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('start_date', trans('labels.project.start_date').':') !!}
            {!! Form::text('start_date', (old('start_date') != 0) ? Carbon::parse(old('start_date'))->format('m/d/Y') : '', ['class' => 'datepicker','data-date-format' => 'yyyy-mm-dd']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('end_date', trans('labels.project.end_date').':') !!}
            {!! Form::text('end_date', (old('end_date') != 0) ? Carbon::parse(old('end_date'))->format('m/d/Y') : '', ['class' => 'datepicker','data-date-format' => 'yyyy-mm-dd']) !!}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('price', trans('labels.project.value').':') !!}
            <div class = "input-group">
                <span class = "input-group-addon">$</span>
                <input type="text" class="form-control" name="price" value="{{ old('price') }}">
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('owner', trans('labels.project.owner').' ('.trans('labels.import_from_ab').'):') !!}
            <input type="text" class="form-control" id="owner_ab" name="owner" value="{{ old('owner') }}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('architect', trans('labels.project.architect').' ('.trans('labels.import_from_ab').'):') !!}
            <input type="text" class="form-control" id="architect_ab" name="architect" value="{{ old('architect') }}">
        </div>
    </div>

</div>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group pull-right">
            {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>
<input type="hidden" id="owner_id" name="owner_id" value="{{old('owner_id')}}"/>
<input type="hidden" id="architect_id" name="architect_id" value="{{old('architect_id')}}"/>
