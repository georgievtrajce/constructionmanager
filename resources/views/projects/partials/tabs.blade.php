<div id="dev-sidenav" class="cms-sidenav">

    <a href="#" class="sidenav-toggle">
        <div class="icon"></div>
    </a>
    @if (CompanyPermissions::hasSubcontractors($project->id) && (Permissions::unshared($project->id)))
    <ul class="cms-sidenav-menu cf">
        @if (Auth::user()->company->subs_id != Config('subscription_levels.level-zero'))
        <li class="cms-sidenav-menu-item">
            <a class="cms-sidenav-btn" data-toggle="tab" href="#project-files">{{trans('labels.project_files')}}</a>
        </li>
        @endif
        @if (Auth::user()->comp_id != $project->comp_id)
        <li class="cms-sidenav-menu-item">
            <a class="cms-sidenav-btn" data-toggle="tab" href="#master-files">{{trans('labels.shared.master_files')}}</a>
        </li>
        @endif
    </ul>
    @endif
    <div id="dev-sidenav" class="cms-sidenav-inner">
        <div class="tab-content">
            @if (Auth::user()->company->subs_id != Config('subscription_levels.level-zero'))
            <div id="project-files" class="tab-pane fade {{(strpos($_SERVER['REQUEST_URI'], "/shared/") !== false)?'':'active in'}}">
                <ul class="cms-sidenav-inner-list" role="tablist">
                    <li role="presentation" class="@if($activeTab == 'projects'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/')}}">{{trans('labels.project.info')}}</a></li>
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'bids')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'bids'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/bids')}}">{{trans('labels.files.bids')}}</a></li>
                    @endif
                    @if (((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'companies')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)) && (Permissions::unshared($project->id)))
                    <li role="presentation" class="@if($activeTab == 'companies'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/companies'.'/')}}">{{trans('labels.company.plural')}}</a></li>
                    @endif
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'contracts')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'contracts'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/contracts')}}">{{trans('labels.files.contracts')}}</a></li>
                    @endif
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'submittals')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'submittals'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/submittals'.'/')}}"> {{trans('labels.files.submittals')}}</a></li>
                    @endif
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'rfis')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'rfis'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/rfis'.'/')}}"> {{trans('labels.files.rfis')}}</a></li>
                    @endif
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'pcos')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'pcos'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/pcos'.'/')}}"> {{trans('labels.files.pcos')}}</a></li>
                    @endif
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'expediting-report')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'expediting-report'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/expediting-report'.'/')}}">{{trans('labels.materials_and_services')}}</a></li>
                    @endif
                    @if (((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'blog')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)) && (Permissions::unshared($project->id)))
                    <li role="presentation" class="@if($activeTab == 'blog'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/blog')}}">{{trans('labels.Blog')}}</a></li>
                    @endif
                    @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'permissions'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/permissions')}}">{{trans('labels.manage_users.user_permissions')}}</a></li>
                    @endif
                    <li role="presentation" class="@if($activeTab == 'project-files'){{'active'}}@endif">
                        <a id="project-files" href="{{URL('/projects/'.$project->id.'/project-files/drawings/file')}}">{{trans('labels.project_files')}}</a>
                    </li>
                    @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('read', 'tasks')) || (Auth::user()->hasRole('Project Admin')
                    && $project->proj_admin == Auth::user()->id))
                    <li role="presentation" class="@if($activeTab == 'project-tasks'){{'active'}}@endif">
                        <a id="project-files" href="{{URL('/projects/'.$project->id.'/tasks')}}">{{trans('labels.tasks.project-tasks')}}</a>
                    </li>
                    @endif 
                </ul>
            </div>
            @endif
            <div id="master-files" class="tab-pane fade {{(strpos($_SERVER['REQUEST_URI'], "/shared/") !== false || (Auth::user()->company->subs_id == Config('subscription_levels.level-zero')))?'active in':''}}">
                <ul class="" role="tablist">
                    <li role="presentation" class="@if($activeTab == 'info'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/info')}}">{{trans('labels.shared.master_project_info')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_bids'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/bids')}}">{{trans('labels.shared.master_bids')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_contracts'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/contracts')}}">{{trans('labels.shared.master_contracts')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_submittals'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/submittals')}}">{{trans('labels.shared.master_submittals')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_rfis'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/rfis')}}">{{trans('labels.shared.master_rfis')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_pcos'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/pcos')}}">{{trans('labels.shared.master_pcos')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_materials_and_services'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/expediting-report')}}">{{trans('labels.shared.master_materials_and_services')}}</a></li>
                    <li role="presentation" class="@if($activeTab == 'shared_project_files'){{'active'}}@endif"><a href="{{URL('projects/'.$project->id.'/shared/project-files/drawings/file')}}">{{trans('labels.shared.master_project_files')}}</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>