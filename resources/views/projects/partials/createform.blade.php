@include('popups.please-wait')
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('name', trans('labels.project.name').':') !!}
                    {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('number', trans('labels.project.number').':') !!}
                    {!! Form::text('number', Input::old('number'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('street', trans('labels.Address').':') !!}
                    {!! Form::text('street', Input::old('street'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('city', trans('labels.city').':') !!}
                    {!! Form::text('city', Input::old('city'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('state', trans('labels.state').':') !!}
                    {!! Form::text('state', Input::old('state'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('zip', trans('labels.zip').':') !!}
                    {!! Form::text('zip', Input::old('zip'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('start_date', trans('labels.project.start_date').':') !!}
                    {!! Form::text('start_date', Input::old('start_date'), ['class' => 'datepicker','data-date-format' => 'yyyy-mm-dd']) !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('end_date', trans('labels.project.end_date').':') !!}
                    {!! Form::text('end_date', Input::old('end_date'), ['class' => 'datepicker','data-date-format' => 'yyyy-mm-dd']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('price', trans('labels.project.value').':') !!}
                    <div class = "input-group">
                        <span class = "input-group-addon">$</span>
                        {!! Form::text('price', Input::old('price'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('owner', trans('labels.project.owner').' ('.trans('labels.import_from_ab').'):') !!}
                    {!! Form::text('owner', Input::old('owner'), ['class' => 'form-control', 'id'=>'owner_ab']) !!}
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="radio" name="defaultRecipient" data-type="default" id="default_recipient_owner" @if(Input::old('transmittal_flag') == "" || Input::old('transmittal_flag') === "3") checked @endif>
                        {{trans('labels.transmittals.default_recipient')}}
                    </div>
                    <div id="select_contact_owner" >
                        <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                        <select name="owner_transmittal_id" id="owner_transmittal_id">
                            <option value="0"></option>
                            @if(Input::old('owner_id'))
                                @if(count($ownerTransmittals))
                                    @foreach($ownerTransmittals as $transmittal)
                                        <option value="{{$transmittal->id}}" @if(Input::old('owner_transmittal_id') == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                    @endforeach
                                @endif
                            @endif
                            <option value="">No available contacts for this company</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('architect', trans('labels.project.architect').' ('.trans('labels.import_from_ab').'):') !!}
                    {!! Form::text('architect', Input::old('architect'), ['class' => 'form-control','id'=>'architect_ab']) !!}
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="radio" name="defaultRecipient" data-type="default" id="default_recipient_architect" @if(Input::old('transmittal_flag') === "1") checked @endif>
                        {{trans('labels.transmittals.default_recipient')}}
                    </div>
                    <div id="select_contact_architect" >
                        <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                        <select name="architect_transmittal_id" id="architect_transmittal_id">
                                <option value="0"></option>
                                @if(Input::old('architect_id'))
                                    @if(count($architectTransmittals))
                                        @foreach($architectTransmittals as $transmittal)
                                            <option value="{{$transmittal->id}}" @if(Input::old('architect_transmittal_id') == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                        @endforeach
                                    @endif
                                @endif
                                <option value="">No available contacts for this company</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>
                        {{trans('labels.submittals.general_contractor').' ('.trans('labels.import_from_ab').'):'}}
                    </label>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="checkbox" name="make_me_gc" id="make_me_gc" @if(Input::old('make_me_gc')) checked="checked" @endif>
                            {{trans('labels.submittals.general_contractor_me')}}
                            <input type="hidden" name="make_me_gc_id" id="make_me_gc_id" value="{{Auth::user()->comp_id}}">
                        </div>
                        <div class="col-md-12 general_contractor_container" @if(Input::old('make_me_gc')) style="display: none;" @endif>
                            {{trans('labels.submittals.import_from_address_book')}}
                            <input type="
                            text" class="form-control address-book-gc-auto"
                                   name="general_contractor" value="{{Input::old('general_contractor')}}">
                            <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{Input::old('general_contractor_id')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 general_contractor_container" @if(Input::old('make_me_gc')) style="display: none;" @endif>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="radio" name="defaultRecipient" data-type="default" id="default_recipient_contractor" @if(Input::old('transmittal_flag') === "0") checked @endif>
                        {{trans('labels.transmittals.default_recipient')}}
                    </div>
                    <div id="select_contact_contractor">
                        <div class="form-group">
                            <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                            <select name="contractor_transmittal_id" id="contractor_transmittal_id">
                                    <option value="0"></option>
                                    @if(Input::old('general_contractor_id') || Input::old('make_me_gc'))
                                        @if(count($contractorTransmittals))
                                            @foreach($contractorTransmittals as $transmittal)
                                                <option value="{{$transmittal->id}}" @if(Input::old('contractor_transmittal_id') == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                            @endforeach
                                        @endif
                                    @endif
                                    <option value="">No available contacts for this company</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('primeSubcontractor', trans('labels.project.prime_subcontractor').' ('.trans('labels.import_from_ab').'):') !!}
                    {!! Form::text('prime_subcontractor', Input::old('prime_subcontractor'), ['class' => 'form-control','id'=>'prime_subcontractor_ab']) !!}
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="radio" name="defaultRecipient" data-type="default" id="default_recipient_prime_subcontractor" @if(Input::old('transmittal_flag') === "2") checked @endif >
                        {{trans('labels.transmittals.default_recipient')}}
                    </div>
                    <div id="select_contact_prime_subcontractor" >
                        <label>{{trans('labels.transmittals.select_contact'). ':'}}</label>
                        <select name="prime_subcontractor_transmittal_id" id="prime_subcontractor_transmittal_id">
                            <option value="0"></option>
                            @if(Input::old('prime_subcontractor_id'))
                                @if(count($primeSubcontractorTransmittals))
                                    @foreach($primeSubcontractorTransmittals as $transmittal)
                                        <option value="{{$transmittal->id}}" @if(Input::old('prime_subcontractor_transmittal_id') == $transmittal->id) {{'selected'}} @endif>{{$transmittal->name}}</option>
                                    @endforeach
                                @endif
                            @endif
                            <option value="">No available contacts for this company</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <!--empty for now -->
    </div>
    <div class="col-md-12">
        <div class="form-group pull-right">
            {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success']) !!}
        </div>
    </div>
</div>
<input type="hidden" id="owner_id" name="owner_id" value="{{Request::old('owner_id')}}"/>
<input type="hidden" id="architect_id" name="architect_id" value="{{Request::old('architect_id')}}"/>
<input type="hidden" id="prime_subcontractor_id" name="prime_subcontractor_id" value="{{Request::old('prime_subcontractor_id')}}"/>
<input type="hidden" id="transmittal_flag" name="transmittal_flag" value=""/>
<input type="hidden" id="project_id" name="project_id" value="0"/>