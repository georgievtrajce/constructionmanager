<ul class="cm-tab cm-tab-vertical-alt" role="tablist">
    <li class="cm-tab-item @if(Request::segment(5) == 'drawings') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                          href="{{URL('/projects/'.$project->id.'/shared/project-files/drawings/file')}}">{{trans('labels.files.drawings').' ('.CustomHelper::countSharedProjectFiles('drawings', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'specifications') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                                href="{{URL('/projects/'.$project->id.'/shared/project-files/specifications/file')}}">{{trans('labels.files.specifications').' ('.CustomHelper::countSharedProjectFiles('specifications', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'addendums') {{'active'}} @endif"><a class="cm-tab-link"
                                                                                           href="{{URL('/projects/'.$project->id.'/shared/project-files/addendums/file')}}">{{trans('labels.files.addendums').' ('.CustomHelper::countSharedProjectFiles('addendums', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'correspondence-emails') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/correspondence-emails/file')}}">{{trans('labels.files.correspondence-emails').' ('.CustomHelper::countSharedProjectFiles('correspondence-emails', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
   {{-- <li class="cm-tab-item @if(Request::segment(5) == 'daily-reports') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/daily-reports/file')}}">{{trans('labels.files.daily-reports').' ('.CustomHelper::countSharedProjectFiles('daily-reports', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>--}}
    <li class="cm-tab-item @if(Request::segment(5) == 'inspection-reports') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/inspection-reports/file')}}">{{trans('labels.files.inspection-reports').' ('.CustomHelper::countSharedProjectFiles('inspection-reports', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'permits') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/permits/file')}}">{{trans('labels.files.permits').' ('.CustomHelper::countSharedProjectFiles('permits', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'project-photos') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/project-photos/file')}}">{{trans('labels.files.project-photos').' ('.CustomHelper::countSharedProjectFiles('project-photos', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'work-schedules') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/work-schedules/file')}}">{{trans('labels.files.work-schedules').' ('.CustomHelper::countSharedProjectFiles('work-schedules', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'punch-lists') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/punch-lists/file')}}">{{trans('labels.files.punch-lists').' ('.CustomHelper::countSharedProjectFiles('punch-lists', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'meeting-minutes') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/meeting-minutes/file')}}">{{trans('labels.files.meeting-minutes').' ('.CustomHelper::countSharedProjectFiles('meeting-minutes', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'closeout-documents') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/closeout-documents/file')}}">{{trans('labels.files.closeout-documents').' ('.CustomHelper::countSharedProjectFiles('closeout-documents', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'billing') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/billing/file')}}">{{trans('labels.files.billing').' ('.CustomHelper::countSharedProjectFiles('billing', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
    <li class="cm-tab-item @if(Request::segment(5) == 'miscellaneous') {{'active'}} @endif">
        <a class="cm-tab-link" href="{{URL('/projects/'.$project->id.'/shared/project-files/miscellaneous/file')}}">{{trans('labels.files.miscellaneous').' ('.CustomHelper::countSharedProjectFiles('miscellaneous', $project->id, $project->comp_id, Auth::user()->comp_id).')'}}</a>
    </li>
</ul>