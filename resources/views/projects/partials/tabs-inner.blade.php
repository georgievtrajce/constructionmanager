<ul class="nav nav-tabs cm-inner-tabs">
    <li @if($activeTab == 'info') class="active" @endif><a data-toggle="pill" href="#info">Info</a></li>
    <li @if($activeTab == 'files') class="active" @endif><a data-toggle="pill" href="#files">Files</a></li>
    <li @if($activeTab == 'tasks') class="active" @endif><a data-toggle="pill" href="#tasks">Tasks</a></li>
</ul>