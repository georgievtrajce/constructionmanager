<script class="successful-upload-multi" type="text/template">
    <style>
        .bg-success {
            background-color: #dff0d8;
        }

        .inst_delete_file {
            padding: 5px;
            margin: 5px 5px 5px 10px;
        }

        .successful_message_cont {
            float: left;
            font-size: 12px;
            color: #49A078;
        }

        .successful_upload_message{
            float: left;
            margin: 5px 0px;
            padding: 5px 5px 5px 10px;
        }

        .file_container{
            float: left;
            margin: 5px 0px;
            padding: 5px 0px;
        }
    </style>
    <div class="bg-success successful_message_cont container<%= file_id %>">
        <span class="successful_upload_message">{{trans('labels.upload_file_template.successful_upload').':'}}</span>
        <span class="file_container"><%= file_name %></span>
        <div class="pull-left delete-wait-cont" style="width: 50px;">
            &nbsp;
            <div class="pull-left delete-wait" style="display: none; margin-left: 15px; margin-top: 3px;">
                <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="30px">
            </div>
        </div>
        <button type="button"
           data-toggle="modal"
           data-target="#confirmDelete"
           data-title="Delete Record"
           data-message='{{trans('labels.global_delete_modal')}}'
         data-file-id-holder="<%= file_id_holder %>" data-file-path="<%= file_path %>" data-file-id="<%= file_id %>" data-type="<%= type %>" class="btn btn-sm btn-danger inst_delete_file_multi pull-left">X</button>
    </div>
</script>