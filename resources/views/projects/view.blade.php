@extends('layouts.tabs')
@section('title')
<div class="row">
<div class="col-md-5">
 <header class="cm-heading">
    {{trans('labels.Project')}} {{trans('labels.info')}}
    <ul class="cm-trail">
        <li class="cm-trail-item active">
            <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a>
        </li>
    </ul>
</header>
</div>
<div class="col-md-7">
<div class="cm-btn-group cm-pull-right cf">
    @if ($project->comp_id == Auth::user()->comp_id && Auth::user()->hasRole('Company Admin'))
        <a href="{{URL('projects/'.$project->id.'/edit')}}" class="btn btn-success pull-right">{{trans('labels.project.edit')}}</a>
    @elseif(Auth::user()->hasRole('Company User'))
    @elseif(Permissions::can('write', 'projects'))
    <a href="{{URL('projects/'.$project->id.'/subcontractor/project-info/edit')}}" class="btn btn-success pull-right">{{trans('labels.project.edit')}}</a>
    @endif

    <a target="_blank" href="{{URL('projects/'.$project->id.'/merge/report')}}" class="btn btn-info pull-right cm-heading-btn">{{trans('labels.project.go-to-meeting')}}</a>
    @if (intval($project->comp_id) == intval(Auth::user()->comp_id))
        @if ((empty($projectDownloadRequest) || ($projectDownloadRequest->generated == 1 && $projectDownloadRequest->sent == 1)) && $usedStorageByProject > 0 && (($batchDownloadPermission && $batchDownloadPermission->read == 1) || ($project->comp_id == Auth::user()->comp_id && Auth::user()->hasRole('Company Admin'))))
            <a href="#" class="btn btn-primary pull-right cm-heading-btn btn-batch-download" data-id="{{$project->id}}">
                {{trans('labels.project.download-all-files')}}<div class="ripple-wrapper"></div>
            </a>
        @endif
    @endif
</div>
</div>
</div>
@endsection
@section('tabs')
@include('projects.partials.tabs', array('activeTab' => 'projects'))
@endsection
@section('tabsContent')
<div class="panel">
    <div class="panel-body">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div>
            <div class="row box-cont-1">
                <div class="col-md-6">
                    <div class="box">
                        <div class="card">
                            <div class="card-body">
                                <h4>{{trans('labels.general')}}</h4>
                                <div class="cm-spacer-xs"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.name')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo) && !empty($project->subcontractorProjectInfo->name))
                                                    {{$project->subcontractorProjectInfo->name}}
                                                    @else
                                                    @if(!empty($project->name))
                                                    {{$project->name}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if(!empty($project->name))
                                                    {{$project->name}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.Address')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!empty($project->subcontractorAddress))
                                                    <a target="_blank" href="http://maps.google.com/?q={{(!empty($project->subcontractorAddress->street)?$project->subcontractorAddress->street:'').
                                                        (!empty($project->subcontractorAddress->city)?', ':'').
                                                        (!empty($project->subcontractorAddress->city)?$project->subcontractorAddress->city:'').
                                                        (!empty($project->subcontractorAddress->state)?', ':'').
                                                        (!empty($project->subcontractorAddress->state)?$project->subcontractorAddress->state:'')}}">
                                                        {{(!empty($project->subcontractorAddress->street)?$project->subcontractorAddress->street:'').
                                                        (!empty($project->subcontractorAddress->city)?', ':'').
                                                        (!empty($project->subcontractorAddress->city)?$project->subcontractorAddress->city:'').
                                                        (!empty($project->subcontractorAddress->state)?', ':'').
                                                        (!empty($project->subcontractorAddress->state)?$project->subcontractorAddress->state:'').
                                                        (!empty($project->subcontractorAddress->zip)?', ':'').
                                                        (!empty($project->subcontractorAddress->zip)?$project->subcontractorAddress->zip:'')}}
                                                    </a>
                                                    @else
                                                    @if(!empty($project->address))
                                                    <a target="_blank" href="http://maps.google.com/?q={{(!empty($project->address->street)?$project->address->street:'').
                                                        (!empty($project->address->city)?', ':'').
                                                        (!empty($project->address->city)?$project->address->city:'').
                                                        (!empty($project->address->state)?', ':'').
                                                        (!empty($project->address->state)?$project->address->state:'')}}">
                                                        {{(!empty($project->address->street)?$project->address->street:'').
                                                        (!empty($project->address->city)?', ':'').
                                                        (!empty($project->address->city)?$project->address->city:'').
                                                        (!empty($project->address->state)?', ':'').
                                                        (!empty($project->address->state)?$project->address->state:'').
                                                        (!empty($project->address->zip)?', ':'').
                                                        (!empty($project->address->zip)?$project->address->zip:'')}}
                                                    </a>
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if(!is_null($project->address) && !empty($project->address->street))
                                                    <a target="_blank" href="http://maps.google.com/?q={{(!empty($project->address->street)?$project->address->street:'').
                                                        (!empty($project->address->city)?', ':'').
                                                        (!empty($project->address->city)?$project->address->city:'').
                                                        (!empty($project->address->state)?', ':'').
                                                        (!empty($project->address->state)?$project->address->state:'')}}">
                                                        {{(!empty($project->address->street)?$project->address->street:'').
                                                        (!empty($project->address->city)?', ':'').
                                                        (!empty($project->address->city)?$project->address->city:'').
                                                        (!empty($project->address->state)?', ':'').
                                                        (!empty($project->address->state)?$project->address->state:'').
                                                        (!empty($project->address->zip)?', ':'').
                                                        (!empty($project->address->zip)?$project->address->zip:'')}}
                                                    </a>
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.number')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo) && !empty($project->subcontractorProjectInfo->number))
                                                    {{$project->subcontractorProjectInfo->number}}
                                                    @else
                                                    @if(!empty($project->number))
                                                    {{$project->number}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if(!empty($project->number))
                                                    {{$project->number}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.upc_code')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    {{$project->upc_code}}
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.status')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo))
                                                    <?php echo ($project->subcontractorProjectInfo->active == Config::get('constants.status.active')) ? trans('labels.active') : trans('labels.completed'); ?>
                                                    @else
                                                    <?php echo ($project->active == Config::get('constants.status.active')) ? trans('labels.active') : trans('labels.completed'); ?>
                                                    @endif
                                                    @else
                                                    <?php echo ($project->active == Config::get('constants.status.active')) ? trans('labels.active') : trans('labels.completed'); ?>
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.value')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo))
                                                    {{'$'.number_format($project->subcontractorProjectInfo->price,2)}}
                                                    @endif
                                                    @else
                                                    {{'$'.number_format($project->price,2)}}
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.owner')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo) && isset($project->subcontractorProjectInfo->owner))
                                                    <a href="{{URL('address-book/'.$project->subcontractorProjectInfo->owner->id.'/view')}}">{{$project->subcontractorProjectInfo->owner->name}}</a>
                                                    @else
                                                    @if (isset($project->owner))
                                                    <a href="{{URL('address-book/'.$project->owner->id.'/view')}}">{{$project->owner->name}}</a>
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if (isset($project->owner))
                                                    <a href="{{URL('address-book/'.$project->owner->id.'/view')}}">{{$project->owner->name}}</a>
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.architect')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                        @if(!is_null($project->subcontractorProjectInfo) && isset($project->subcontractorProjectInfo->architect))
                                                            <a href="{{URL('address-book/'.$project->subcontractorProjectInfo->architect->id.'/view')}}">{{$project->subcontractorProjectInfo->architect->name}}</a>
                                                        @else
                                                            @if (isset($project->architect))
                                                                <a href="{{URL('address-book/'.$project->architect->id.'/view')}}">{{$project->architect->name}}</a>
                                                            @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if (isset($project->architect))
                                                            <a href="{{URL('address-book/'.$project->architect->id.'/view')}}">{{$project->architect->name}}</a>
                                                        @else
                                                            <i>{{trans('labels.unknown')}}</i>
                                                        @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.submittals.general_contractor')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if($project->make_me_gc)
                                                        <i>{{Auth::user()->company->name}}</i>
                                                    @else
                                                        @if (isset($project->generalContractor->id))
                                                            <a href="{{URL('address-book/'.$project->generalContractor->id.'/view')}}">{{$project->generalContractor->name}}</a>
                                                        @else
                                                            <i>{{trans('labels.unknown')}}</i>
                                                        @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.prime_subcontractor')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                        @if(!is_null($project->subcontractorProjectInfo) && isset($project->subcontractorProjectInfo->primeSubcontractor))
                                                            <a href="{{URL('address-book/'.$project->subcontractorProjectInfo->primeSubcontractor->id.'/view')}}">{{$project->subcontractorProjectInfo->prime_subcontractor->name}}</a>
                                                        @else
                                                            @if (isset($project->primeSubcontractor))
                                                                <a href="{{URL('address-book/'.$project->primeSubcontractor->id.'/view')}}">{{$project->primeSubcontractor->name}}</a>
                                                            @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                            @endif
                                                        @endif
                                                    @else
                                                        @if (isset($project->primeSubcontractor))
                                                            <a href="{{URL('address-book/'.$project->primeSubcontractor->id.'/view')}}">{{$project->primeSubcontractor->name}}</a>
                                                        @else
                                                            <i>{{trans('labels.unknown')}}</i>
                                                        @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>

                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.start_date')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo) && ($project->subcontractorProjectInfo->start_date != 0))
                                                    {{date("m/d/Y", strtotime($project->subcontractorProjectInfo->start_date))}}
                                                    @else
                                                    @if(!empty($project->start_date) && $project->start_date != 0)
                                                    {{date("m/d/Y", strtotime($project->start_date))}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if(!empty($project->start_date) && $project->start_date != 0)
                                                    {{date("m/d/Y", strtotime($project->start_date))}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.project.end_date')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if(Auth::user()->comp_id != $project->comp_id)
                                                    @if(!is_null($project->subcontractorProjectInfo) && ($project->subcontractorProjectInfo->end_date != 0))
                                                    {{date("m/d/Y", strtotime($project->subcontractorProjectInfo->end_date))}}
                                                    @else
                                                    @if(!empty($project->end_date) && $project->end_date != 0)
                                                    {{date("m/d/Y", strtotime($project->end_date))}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                    @else
                                                    @if(!empty($project->end_date) && $project->end_date != 0)
                                                    {{date("m/d/Y", strtotime($project->end_date))}}
                                                    @else
                                                    <i>{{trans('labels.unknown')}}</i>
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    @if(!empty($city))
                    <div class="box">
                        <div class="card">
                            <div class="card-body">
                                <h4>{{trans('labels.project.weather')}}</h4>
                                <div class="cm-spacer-xs"></div>
                                <iframe id="forecast_embed" type="text/html" frameborder="0" height="245" width="100%" src="https://forecast.io/embed/#lat={{$city->lat}}&lon={{$city->lng}}&name={{$city->city.", ".$city->state}}&color=#00aaff&font=Georgia&units=us"> </iframe>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="row box-cont-2">
                <div class="col-md-6">
                    @if (sizeof($files))
                    <div class="box box-min-height">
                        <div class="card">
                            <div class="card-body">
                                <div class="cm-box cm-box-negative">
                                    <div class="cm-box-heading">
                                        <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-file"></i></span>
                                        <h3 class="cm-box-heading-title">{{trans('labels.latest_files')}}</h3>
                                    </div>
                                    <ul class="cm-box-list">
                                        @foreach ($files as $file)
                                        @if($file->transmittal != Config('constants.transmittal_flags.transmittal'))
                                        <li class="cm-box-list-item">
                                            <h6 class="cm-box-list-item-date">{{Carbon::parse($file->created_at)->format('m/d/Y')}}</h6>
                                            <a class="download cm-box-list-item-title" href="javascript:;"><strong>{{$file->file_name}}</strong></a>
                                            <?php
                                            if ($file->display_name == Config::get('constants.contracts')) {
                                            $contractPath = !empty($file->contract_file)?'contract_'.$file->contract_file->contr_id.'/':'';
                                            $filePathPartial = $file->display_name.'/'.$contractPath.$file->file_name;
                                            } else if ($file->display_name == Config::get('constants.bids.plural')) {
                                            $filePathPartial = Config::get('constants.proposals').'/'.$file->file_name;
                                            } else {
                                            $filePathPartial = $file->display_name.'/'.$file->file_name;
                                            }
                                            ?>
                                            <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$file->proj_id.'/'.$filePathPartial}}">
                                            <p class="cm-box-list-item-date">{{$file->project->name or ''}}</p>
                                            <p class="cm-box-list-item-date"><i>{{$file->ft_name or ''}}</i></p>
                                        </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                    <a class="cm-box-more" href="{{URL('/projects/'.$file->proj_id.'/all-uploaded-files')}}">{{trans('labels.view_more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-md-6">
                    <div class="box box-min-height">
                        <div class="card">
                            <div class="card-body">
                                <h4>{{trans('labels.project.project_statistics')}}</h4>
                                <div class="cm-spacer-xs"></div>
                                @if ($submittalsStatistics['Open']+$submittalsStatistics['Closed']+$submittalsStatistics['Draft'] > 0)
                                    <div id="chart_div_submittals" style="width: 100%; min-height: 100%"></div>
                                @endif
                                @if ($rfisStatistics['Open']+$rfisStatistics['Closed']+$rfisStatistics['Draft'] > 0)
                                    <div id="chart_div_rfis" style="width: 100%; min-height: 100%"></div>
                                @endif
                                @if ($pcoStatistics['Open']+$pcoStatistics['Closed']+$pcoStatistics['Draft'] > 0)
                                    <div id="chart_div" style="width: 100%; min-height: 100%"></div>
                                @endif
                                <span class="progress-bar-title">{{trans('labels.project.storage_statistics')}}: </span>
                                <div class="meter animate blue">
                                    <span style="width: {{($percentageSum<1)?1:$percentageSum}}%"><span></span></span>
                                    <label>{{$sizeSum}}&nbsp;used&nbsp;({{$percentageSum}}%)</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    @if (sizeof($blogPosts))
                    <div class="box">
                        <div class="card">
                            <div class="card-body">
                                <div class="cm-box cm-box-info">
                                    <div class="cm-box-heading">
                                        <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-comment"></i></span>
                                        <h3 class="cm-box-heading-title">{{trans('labels.latest_blog_posts')}}</h3>
                                    </div>
                                    <ul class="cm-box-list">
                                        @foreach ($blogPosts as $blogPost)
                                        <li class="cm-box-list-item">
                                            <h6 class="cm-box-list-item-date">{{Carbon::parse($blogPost->created_at)->format('m/d/Y')}}</h6>
                                            <a class="cm-box-list-item-title" href="{{URL('/projects/'.$blogPost->proj_id.'/blog/'.$blogPost->id)}}"><strong>{{ $blogPost->title }}</strong></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                    <a class="cm-box-more" href="{{URL('projects/'.$project->id.'/all-blog-posts')}}">{{trans('labels.view_more')}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
<script id="sub_proj_number_template" type="text/template">
@if(count($project->projectSubcontractors))
<input type="text" class="form-control sub_proj_number" name="sub_proj_number">
<a class="btn btn-sm btn-success sub_project_number_save" href="javascript:;">
    {{trans('labels.save')}}
</a>
<a class="btn btn-sm btn-danger pull-right sub_project_number_cancel" href="javascript:;">
    {{trans('labels.cancel')}}
</a>
@endif
</script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}"></script>
<style>
#map_canvas {
height: 300px;
width: 100%;
}
</style>
<script type="text/javascript" src="{{URL::asset('js/loader.js')}}"></script>
<script type="text/javascript">
var locationAddress = '{{(!empty($address))?implode(',', $address):''}}';

var dataDbSubmittals = [['Type', 'Submittals']];
@foreach($submittalsStatistics as $key=>$item)
    dataDbSubmittals.push(['{{$key}}', {{$item}}]);
@endforeach

var dataDbRFIs = [['Type', 'RFI\'s']];
@foreach($rfisStatistics as $key=>$item)
    dataDbRFIs.push(['{{$key}}', {{$item}}]);
@endforeach

var dataDbPCOs = [['Type', 'PCO\'s']];
@foreach($pcoStatistics as $key=>$item)
    dataDbPCOs.push(['{{$key}}', {{$item}}]);
@endforeach
</script>
<script src="{{URL::asset('js/google-charts.js')}}"></script>
@include('popups.approve_popup')
@endsection