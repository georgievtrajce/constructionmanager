@extends('layouts.master')

@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <h1 class="cm-heading">
                {{trans('labels.general')}}
                <small class="cm-heading-sub">{{trans('labels.total_records', ['number'=> $projects->count()])}}</small>
            </h1>
        </div>
        @if ((Auth::user()->hasRole(['Company Admin', 'Project Admin'])) || (Permissions::can('write', 'projects')))
        <div class="col-md-7">
            <div class="cm-pull-right">
                    <div class="cm-btn-group cf">
                        <a href="{{URL('projects/create')}}" class="btn btn-success pull-right">{{trans('labels.project.create')}}</a>
                    </div>
            </div>
        </div>
        @endif
    </div>

    @if(sizeof($projects))
            <div class="cm-filter">
                <div class="row">

                    <div class="col-md-12">
                        {!! Form::open(['method'=>'GET','url'=>'projects/search/'.$searchUrl]) !!}
                        <div class="row">
                            <div class="col-sm-3 col-md-3 col-lg-2">
                                <div class="form-group">
                                    {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                    {!! Form::select('search_by',['name' => trans('labels.project.name'), 'number'=>trans('labels.project.number'), 'location'=>trans('labels.project.location')], Input::get('search_by')) !!}
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group">
                                    {!! Form::label('search', trans('labels.search').':') !!}
                                    {!! Form::text('search',Input::get('search'),['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-5">
                                <div class="form-group">
                                    {!! Form::hidden('sort',Input::get('sort','id')) !!}
                                    {!! Form::hidden('order',Input::get('order','desc')) !!}
                                    {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                    <!-- {!! Form::label(trans('labels.filter'), ' &nbsp;') !!} -->
                                </div>
                            </div>

                        {!! Form::close() !!}

                        <div class="col-xs-6 col-sm-3 col-md-3 col-lg-2">

                        {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                            <input type="hidden" value="projects/" id="form-url" />
                            <a href="#" disabled id="delete-button" class='btn btn-xs btn-danger pull-right cm-btn-fixer delete-button-multi' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                {{trans('labels.delete')}}
                            </a>
                        {!! Form::close()!!}

                        </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

    <div class="panel">
        <div class="panel-body">


            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                <ul class="nav nav-tabs cm-tab-inner" role="tablist">
                    <li role="presentation" @if (Request::segment(2)== '' || Request::segment(3)== 'created') class="active" @endif><a href="{{URL('projects')}}">{{trans('labels.project.active')}}</a></li>
                    <li role="presentation" @if (Request::segment(2)== 'completed' || Request::segment(3)== 'completed') class="active" @endif><a href="{{URL('projects/completed')}}">{{trans('labels.project.completed')}}</a></li>
                </ul>
                <div class="panel panel-default panel-body cm-panel-tabs-body">
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="created">
                            <div class="table-responsive">  
                                @if(count($projects))
                                @include('projects.partials.table', ['projects' => $projects])
                                @else
                                <p><span class="col-sm-12 text-center">{{trans('labels.no_projects')}}</span></p>
                                @endif
                           </div>
                       </div>
                    </div>
                    <div class="pull-right">
                        <?php echo $projects->appends([
                                'id'=>Input::get('id'),
                                'name'=>Input::get('company_name'),
                                'number'=>Input::get('number'),
                                'upc_code'=>Input::get('upc_code'),
                                'price'=>Input::get('price'),
                                'start_date'=>Input::get('start_date'),
                                'end_date'=>Input::get('end_date'),
                                'sort'=>Input::get('sort'),
                                'order'=>Input::get('order'),
                        ])->render(); ?>
                    </div>
                </div>
           </div>


           </div>
       </div>

   </div>
@include('popups.delete_record_popup')
   @endsection
