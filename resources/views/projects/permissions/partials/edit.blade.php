@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.Project')}} {{trans('labels.manage_users.user_permissions')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('/projects/'.$project->id.'/permissions')}}" class="cm-trail-link">{{trans('labels.project_permission.plural')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/permissions/'.$user->id.'/edit')}}" class="cm-trail-link">{{trans('labels.project_permission.plural')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'permissions'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cm-filter cm-filter__alt cf">
                                <p>{{trans('labels.name').': '.$user->name}}</p>
                                <p>{{trans('labels.manage_users.user').': '.$user->email}}</p>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="row">
                                @if (Session::has('flash_notification.message'))
                                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('flash_notification.message') }}
                                </div>
                                @endif


                                @if(count($permissions))
                                {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project->id.'/permissions/'.$user->id, 'role'=> 'form']) !!}
                                <div class="col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="select_all" id="select_all_permissions"> {{trans('labels.select_all')}}
                                        </label>
                                    </div>
                                    <!--  <h3>{{trans('labels.manage_users.modules')}}</h3> -->
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th style="width: 20%;">{{trans('labels.manage_users.module')}}</th>
                                                    <th style="width: 20%;">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="select_all" id="select_read_permissions">
                                                                {{trans('labels.manage_users.read')}}
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th style="width: 20%;">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="select_all" id="select_write_permissions">
                                                                {{trans('labels.manage_users.write')}}
                                                            </label>
                                                        </div>
                                                    </th>
                                                    <th style="width: 20%;">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="select_all" id="select_delete_permissions">
                                                                {{trans('labels.manage_users.delete')}}
                                                            </label>
                                                        </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    {{Config::get('constants.modules.download_batch_files')}}
                                                </td>
                                                <td>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input <?php echo ($batchPermissions && $batchPermissions->read == 1) ? 'checked' : ''; ?> type="checkbox" class=" permission mandatory-permissions read read-permission" name="batch_read[1]">
                                                        </label>
                                                    </div>
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                                <?php $i=0; ?>
                                                @foreach($permissions as $permission)
                                                    @if($permission->entity_type == 1)
                                                        <?php
                                                        $mainModuleReadClass = '';
                                                        $projectEventClass = $permission->module->name == 'Projects' ? 'project-event ' : '';
                                                        if($permission->module->name == 'Projects' || $permission->module->name == 'Address Book' || $permission->module->name == 'Custom Categories')
                                                        {
                                                            $readPermissionsClass = '';
                                                            $eventPermissionsClass = '';
                                                            $mainModuleEventClass = 'main-module-event ';
                                                        } else {
                                                            $readPermissionsClass = 'read-permission ';
                                                            $eventPermissionsClass = 'event-permissions ';
                                                            $mainModuleEventClass = '';
                                                        }
                                                        ?>
                                                <tr>
                                                    <td>
                                                        @if($permission->module->name == 'Projects')
                                                            {{Config::get('constants.modules.project_info')}}
                                                        @else
                                                            {{$permission->module->name}}
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class=" permission mandatory-permissions read <?php echo ($permission->module->name == 'Address Book') ? 'address-book-read ' : ''; echo ($permission->module->name == 'Projects') ? 'projects-module ' : ''; echo $readPermissionsClass; echo $mainModuleReadClass; ?>" name="module_read[{{$i}}]">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        @if($permission->module->name != 'Projects')
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" class="permission write <?php echo $projectEventClass; echo $eventPermissionsClass; echo ($permission->module->name == 'Address Book') ? 'address-book-event ' : ''; echo $mainModuleEventClass; ?>" name="module_write[{{$i}}]">
                                                            </label>
                                                        </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($permission->module->name != 'Projects')
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->delete == 1) ? 'checked' : ''; ?> type="checkbox" class="permission delete <?php echo $projectEventClass; echo $eventPermissionsClass; echo ($permission->module->name == 'Address Book') ? 'address-book-event ' : ''; echo $mainModuleEventClass; ?>" name="module_delete[{{$i}}]">
                                                            </label>
                                                        </div>
                                                        @endif
                                                    </td>
                                                    <input type="hidden" name="module_id[{{$i}}]" value="{{$permission->module->id}}">
                                                </tr>
                                                    <?php $i++; ?>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <!-- <h3>{{trans('labels.project_files')}}</h3> -->
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th style="width: 20%;">{{trans('labels.project_files')}}</th>
                                                    <th style="width: 20%; padding-left: 20px; font-weight: normal;">{{trans('labels.manage_users.read')}}</th>
                                                    <th style="width: 20%; padding-left: 20px; font-weight: normal;">{{trans('labels.manage_users.write')}}</th>
                                                    <th style="width: 20%; padding-left: 20px; font-weight: normal;">{{trans('labels.manage_users.delete')}}</th>
                                                    <!--<th style="width: 20%; padding-left: 20px; font-weight: normal;">&nbsp;</th>-->
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=0; ?>
                                                @foreach($permissions as $permission)
                                                @if($permission->entity_type == 2 && !in_array($permission->fileType->display_name, ['daily-reports-images', 'daily-reports-files']))
                                                <tr>
                                                    <td>{{$permission->fileType->name}}</td>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="mandatory-permissions permission read-permission read" name="project_file_read[{{$i}}]">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" class="event-permissions permission write" name="project_file_write[{{$i}}]">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input <?php echo ($permission->delete == 1) ? 'checked' : ''; ?> type="checkbox" class="event-permissions permission delete" name="project_file_delete[{{$i}}]">
                                                            </label>
                                                        </div>
                                                    </td>
                                                    <!--<td>&nbsp;</td>-->
                                                    <input type="hidden" name="project_file_id[{{$i}}]" value="{{$permission->fileType->id}}">
                                                </tr>
                                                <?php $i++; ?>
                                                @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success pull-right">
                                            {{trans('labels.save')}}
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                @else
                                    <div class="col-md-12">
                                        <p class="text-center">{{trans('labels.manage_users.permissions_warning')}}</p>
                                    </div>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
