



      <style>
          .radio label span.check {
              left: 10px!important;
          }
      </style>
      <div class="col-md-4 mb15">
          <h4>{{trans('labels.office')}}</h4>
          <select id="permissions_user_address_office" name="permissions_user_address_office" >
              <?php
                $firstOffice = false;
                if (\Input::get('officeId')) {
                    $firstOffice = \Input::get('officeId');
                }
              ?>
              @foreach($addresses as $companyAddress)
                  <?php if(!$firstOffice) {
                          $firstOffice = $companyAddress->id;
                        }
                  ?>
                  <option {{($firstOffice != $companyAddress->id)?'':'selected'}} value="{{ $companyAddress->id }}">{{ isset($companyAddress->office_title) ? $companyAddress->office_title : $companyAddress->city.', '.$companyAddress->state }}</option>
              @endforeach
          </select>
      </div>
      @if(count($offices))
          <div class="col-md-12 ml15 mb5">
              <div class="row">
                  {{trans('labels.select_all_employees')}}
              </div>
          </div>
          @foreach($offices as $officeId=>$users)
          <div class="col-md-12">
              <div class="table-responsive">
                <table id="container-{{$officeId}}" class="table table-hover table-bordered cm-table-compact permissions-table {{($firstOffice != $officeId)?'hide':''}}">
                    <thead>
                    <tr>
                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                            <th class="text-center">
                                <input type="checkbox" name="select_all" class="offices_select_all" id="offices_select_all_{{$officeId}}">
                            </th>
                        @endif
                        <th>{{trans('labels.name')}}</th>
                        <th>{{trans('labels.email')}}</th>
                        <th>{{trans('labels.title_trade')}}</th>
                        @if(Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                            <th>{{trans('labels.project_admin')}}</th>
                        @endif
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $projectAdminFlag = 0;
                    if (Auth::user()->id == $project->proj_admin) {
                        $projectAdminFlag = 1;
                    }
                    ?>
                    @for($i=0; $i<sizeof($users); $i++)
                        @if(!$users[$i]->hasRole('Company Admin'))
                            <tr>
                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                                    <td class="text-center">
                                        <input type="checkbox" name="user_ids[]" value="{{$users[$i]->id}}" data-id="{{$users[$i]->id}}" @if($users[$i]->is_selected) {{'checked'}} @endif class="office_file_{{$officeId}} multiple-items-checkbox">
                                    </td>
                                @endif
                                <td>@if (isset($users[$i]->name)) {{$users[$i]->name}} @endif</td>
                                <td>@if (isset($users[$i]->email)) {{$users[$i]->email}} @endif</td>
                                <td>@if (isset($users[$i]->title)) {{$users[$i]->title}} @endif</td>
                                @if($users[$i]->is_selected)
                                    @if(Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                                        <td>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" @if($project->proj_admin == $users[$i]->id){{'checked'}}@endif name="project_admin" class="project_admin"
                                                            data-checked="{{($project->proj_admin == $users[$i]->id)?'true':'false'}}" data-project-id="{{$project->id}}" data-project-admin="{{$projectAdminFlag}}" value="{{$users[$i]->id}}">
                                                    <div class="project_admin_wait" style="display: none;">
                                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="20px">
                                                    </div>
                                                </label>
                                            </div>
                                        </td>
                                    @endif
                                @else
                                    <td></td>
                                @endif
                                <td>
                                    @if($users[$i]->is_selected)
                                        @if($project->proj_admin != $users[$i]->id)
                                            <a href="{{URL('/projects/'.$project->id.'/permissions/'.$users[$i]->id.'/edit')}}">{{trans('labels.project_permission.manage')}}</a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endfor
                    </tbody>
                </table>

              </div>
          </div>
              <?php $first = false; ?>
          @endforeach

      @else
          <p class="text-center">{{trans('labels.no_records')}}</p>
      @endif

