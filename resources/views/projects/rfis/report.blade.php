<!DOCTYPE html>
<html lang="en">
<head>
    <title>CMS Construction Master</title>
</head>
<body>
<style>
    hr{
        display: block;
        height: 1px;
        border: 0;
        border-top: 1px solid #000;
        margin: 0;
        padding: 0;
    }
</style>
    <div class="cm-page-content">
        <div class="container">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <table width="100%" class="table table-hover table-striped cm-table-compact">
                            <tr>
                                <td style="width: 100%; border-bottom: 3px solid black;">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 400px; vertical-align: bottom;">
                                                <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                                @if(count(Auth::user()->company->addresses))
                                                    {{Auth::user()->company->addresses[0]->street}}<br>
                                                    {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                                @endif
                                            </td>
                                            <td style="width: 450px; vertical-align: bottom;">
                                                {{Auth::user()->email}}<br>
                                                {{Auth::user()->office_phone}}
                                            </td>
                                            <td style="float: right; vertical-align: bottom;">
                                                <h2 style="float: left; margin: 0px; text-align: right;">{{"RFI's"}}</h2>
                                                <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; border-bottom: 3px solid black;">
                                    <table width="100%">
                                        <tr>
                                            <td style="vertical-align: top; width: 465px;">
                                                <b>{{'Project: '}}</b>
                                                {{$project->name}}
                                            </td>
                                            <td style="vertical-align: top; width: 390px; text-align: right;">
                                                <b>{{'Project Number: '}}</b>
                                                {{$project->number}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h3>{{trans('labels.rfis.rfis_filtered_by')}}</h3>
                            <h4>
                                @if(!empty($mf_title))
                                    {{$mf_title}}<br>
                                @endif
                                @if(!empty($sub_title))
                                    {{$sub_title}}<br>
                                @endif
                                @if(!empty($status_title))
                                    {{$status_title}}
                                @endif
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row" style="font-size: 11px;">
                        <style>
                            .rfis-padding {
                                padding: 5px 5px!important;
                                text-align: left!important;
                            }
                        </style>
                        @if(count($rfis) != 0)
                            <table style="width: 100%;" class="table table-hover table-striped cm-table-compact">
                                <thead>
                                <tr>
                                    <th class="rfis-padding">{{trans('labels.rfis.version_no')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.cycle_no')}}</th>
                                    <th class="rfis-padding">{{trans('labels.master_format_number_and_title')}}</th>
                                    <th class="rfis-padding">{{trans('labels.rfis.name')}}</th>
                                    <th class="rfis-padding">{{trans('labels.supplier_subcontractor')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.sent_for_approval')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.received_from_approval')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                    <th class="rfis-padding">{{trans('labels.status')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rfis as $item)
                                        <tr>
                                            <td class="rfis-padding">{{$item->number}}</td>
                                            <td class="rfis-padding">{{$item->version_cycle_no}}</td>
                                            <td class="rfis-padding">
                                                @if (isset($item->mf_number) && isset($item->mf_title))
                                                    {{$item->mf_number.' - '.$item->mf_title}}
                                                @endif
                                            </td>
                                            <td class="rfis-padding">{{$item->name}}</td>
                                            <td class="rfis-padding">
                                                {{($item->self_performed ? Auth::user()->company->name : $item->subcontractor_name)}}
                                            </td>
                                            <td class="rfis-padding">
                                                @if($item->version_rec_sub != 0)
                                                    {{date("m/d/Y", strtotime($item->version_rec_sub))}}
                                                    @if($item->version_sent_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_sent_appr))) - strtotime(date("m/d/Y", strtotime($item->version_rec_sub))); ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_sub))); ?>
                                                    @endif
                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td class="rfis-padding">
                                                @if($item->version_sent_appr != 0)
                                                    {{date("m/d/Y", strtotime($item->version_sent_appr))}}
                                                    @if($item->version_rec_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_rec_appr))) - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                                    @endif
                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td class="rfis-padding">
                                                @if($item->version_rec_appr != 0)
                                                    {{date("m/d/Y", strtotime($item->version_rec_appr))}}
                                                    @if($item->version_subm_sent_sub != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                                    @endif
                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td class="rfis-padding">
                                                @if($item->version_subm_sent_sub != 0)
                                                    {{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}
                                                    @if(!in_array($item->version_status_short_name, ['Closed']))
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub))); ?>
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="rfis-padding">{{$item->version_status_short_name}}</td>
                                        </tr>
                                        @if(!empty($printWithNotes))
                                            <tr>
                                                <td colspan="10" style="background: #ddd; padding: 5px;">
                                                    @if(!empty($item->last_version_report->question))
                                                        <strong>Question:</strong> {!! $item->last_version_report->question !!}
                                                    @endif
                                                    @if(!empty($item->last_version_report->question))
                                                        <br />
                                                        <strong>Answer:</strong> {!! $item->last_version_report->answer !!}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td colspan="10"><hr></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">{{trans('labels.no_records')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>