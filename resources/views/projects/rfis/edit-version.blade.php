@extends('layouts.tabs')
@section('title')
  <div class="row">
    <div class="col-md-12">
      <header class="cm-heading">
        {{trans('labels.rfis.edit_rfi_version')}}
        <small class="cm-heading-suffix">{{trans('labels.rfi').': '.$rfiVersion->rfi->name}}</small>
        <ul class="cm-trail">
          <li class="cm-trail-item">
            <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a>
          </li>
          <li class="cm-trail-item">
            <a href="{{URL('projects/'.$project->id.'/rfis')}}" class="cm-trail-link">{{trans('labels.rfis.project_rfis')}}</a>
          </li>
          <li class="cm-trail-item">
            <a href="{{URL('projects/'.$project->id.'/rfis/'.$rfiVersion->rfi->id.'/edit')}}" class="cm-trail-link">{{trans('labels.rfis.edit_rfi')}}
            </a>
          </li>
          <li class="cm-trail-item active">
            <a href="{{URL('projects/'.$project->id.'/rfis/'.$rfiVersion->rfi->id.'/version/'.$rfiVersion->id.'/edit')}}" class="cm-trail-link">{{trans('labels.rfis.edit_rfi_version')}}
            </a>
          </li>
        </ul>
      </header>
    </div>
  </div>
@endsection
@section('tabs')
  @include('projects.partials.tabs', array('activeTab' => 'rfis'))
@endsection
@section('tabsContent')
  <div class="panel">
    <div class="panel-body">
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
            }}
        </div>
      @endif
      <div>
        {!! Form::open(['files'=>true, 'name' => 'versionForm', 'id' => 'rfis', 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/rfis/'.$rfiVersion->rfi->id.'/version/'.$rfiVersion->id)]) !!}
        <div class="row box-cont-1">
          <div class="col-md-4">
            <div class="box">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="cm-control-required">{{trans('labels.rfis.name')}}</label>
                            <input type="text" readonly class="form-control " name="name" value="{{$rfiVersion->rfi->name}}">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="cm-control-required">{{trans('labels.rfis.version_number')}}</label>
                            <input type="text" readonly class="form-control " name="number" value="{{$rfiVersion->rfi->number}}">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="col-md-6 pl0">
                            <div class="form-group">
                              <label class="cm-control-required">{{trans('labels.submittals.cycle_no')}}</label>
                              <input type="text" class="form-control " name="cycle_no" value="{{ $rfiVersion->cycle_no }}">
                            </div>
                          </div>
                          <div class="col-md-6 pl0">
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="form-group">
                            <label class="cm-control-required">{{trans('labels.status')}}</label>
                            <div class="status-cont ">
                              <select name="status">
                                @foreach($statuses as $key => $value)
                                  <option {{ (isset($rfiVersion->status) && $rfiVersion->status->id == $key) ? 'selected' : '' }} value="{{$key}}">{{$value}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <input type="hidden" id="type" name="type" value="{{Config::get('constants.rfis')}}">
                          <input type="hidden" id="module" name="type" value="{{Request::segment(3)}}">
                          <input type="hidden" id="databaseType" name="databaseType" value="{{Config::get('constants.transmittal_type.rfi')}}">
                          <input type="hidden" id="versionId" name="versionId" value="{{$rfiVersion->id}}">
                          <input type="hidden" id="projectId" name="projectId" value="{{$project->id}}">
                          <button type="submit" id="submittal-file-submit" class="btn btn-success pull-left">{{trans('labels.update')}}</button>
                          <div class="col-md-4">
                            <div class="please_wait text-right"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="box">
              <div class="card">
                <div class="card-body">
                  <h4>RFI files</h4>
                  <div class="cm-spacer-xs"></div>
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="">
                        <label>
                          {{trans('labels.submittals.received_from_subcontractor')}}
                        </label>
                        <div class="row">
                          <div class="col-md-4 ">
                              <div class="input-group">
                                <input type="text" id="rec-sub" class="form-control " name="rec_sub" value="{{ $rec_sub }}">
                                <span class="input-group-addon">
                                @if($rfiVersion->rec_sub != 0)
                                    @if($rfiVersion->sent_appr != 0)
                                      <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfiVersion->sent_appr))) - strtotime(date("m/d/Y", strtotime($rfiVersion->rec_sub))); ?>
                                    @else
                                      <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfiVersion->rec_sub))); ?>
                                    @endif
                                    <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>
                                 @endif
                                 </span>
                              </div>
                          </div>
                          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                          || (Permissions::can('write', 'rfis')))
                            <div class="col-md-4 ">
                              <!-- // upload input -->
                                <div class="input-group">
                                  <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          <input type="file" style="display: none;" name="rec_sub_file" id="rec_sub_file" multiple>
                                          <span class="glyphicon glyphicon-upload"></span>
                                      </span>
                                  </label>
                                    @if(count($rfiVersion->file))
                                      <?php $i = 0; ?>
                                      @foreach($rfiVersion->file as $recSubFile)
                                        @if($recSubFile->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                          <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id"
                                                 value="{{$recSubFile->id}}">
                                          <?php $i = 1; ?>
                                        @endif
                                      @endforeach
                                      @if($i == 0)
                                        <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                      @endif
                                    @else
                                      <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-1">
                              <div id="rec_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                     width="17px">
                              </div>
                            </div>
                            <div class="col-md-3">
                            @if(count($rfiVersion->file))
                              @foreach($rfiVersion->file as $recSubFile)
                                @if($recSubFile->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                    <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                    @if(isset($rfiVersion['email_transmittal_files']['sentAppr3Path']) && $rfiVersion['email_transmittal_files']['sentAppr3Path'] == Config::get('constants.submittal_version_files.rec_sub_file'))
                                      <input type="hidden" class="s3FilePath sentAppr3Path" id="{{$recSubFile->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$recSubFile->file_name}}">
                                    @else
                                      <input type="hidden" class="s3FilePath" id="{{$recSubFile->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$recSubFile->file_name}}">
                                    @endif
                                  @endif
                                @endforeach
                              @endif

                              @endif

                              @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                              || (Permissions::can('delete', 'rfis'))) @if(count($rfiVersion->file)) @foreach($rfiVersion->file as
                    $recSubFile) @if($recSubFile->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                   data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                  <div class="pull-left delete-wait"
                                       style="display: none;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt=""
                                         width="17px">
                                  </div>
                                </a>
                                <input type="hidden" data-type="rfis" class="s3FilePath" id="{{$recSubFile->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$recSubFile->file_name}}">                    @endif @endforeach @endif @endif
                            </div>
                        </div>
                      </div>
                      <div class="">
                        <label>{{trans('labels.submittals.sent_for_approval')}}</label>
                        <div class="row">
                          <div class="col-md-4">
                              <div class="input-group">
                                <input type="text" id="sent-appr" class="form-control " name="sent_appr" value="{{ $sent_appr }}">
                                <span class="input-group-addon">
                                  @if($rfiVersion->sent_appr != 0)
                                    @if($rfiVersion->rec_appr != 0)
                                      <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfiVersion->rec_appr))) - strtotime(date("m/d/Y", strtotime($rfiVersion->sent_appr))); ?>
                                    @else
                                      <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfiVersion->sent_appr))); ?>
                                    @endif
                                    <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>
                                  @endif
                                  </span>
                              </div>
                          </div>
                          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                          || (Permissions::can('write', 'rfis')))
                            <div class="col-md-4">
                              <!-- // upload input -->
                                <div class="input-group">
                                  <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          <input type="file" style="display: none;" name="sent_appr_file" id="sent_appr_file" multiple>
                                          <span class="glyphicon glyphicon-upload"></span>
                                      </span>
                                  </label>
                                    @if(count($rfiVersion->file))
                                      <?php $i = 0; ?>
                                      @foreach($rfiVersion->file as $file)
                                        @if($file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                          <input type="hidden" name="sent_appr_file_id"
                                                 id="sent_appr_file_id" value="{{$file->id}}">
                                          <?php $i = 1; ?>
                                        @endif
                                      @endforeach
                                      @if($i == 0)
                                        <input type="hidden" name="sent_appr_file_id"
                                               id="sent_appr_file_id">
                                      @endif
                                    @else
                                      <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-1">
                              <div id="sent_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                     width="17px">
                              </div>
                            </div>
                            <div class="col-md-3">
                            @if(count($rfiVersion->file))
                              @foreach($rfiVersion->file as $file)
                                @if($file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                    <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                    @if(isset($rfiVersion['email_transmittal_files']['sentAppr3Path']) && $rfiVersion['email_transmittal_files']['sentAppr3Path'] == Config::get('constants.submittal_version_files.sent_appr_file'))
                                      <input type="hidden" class="s3FilePath sentAppr3Path" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @else
                                      <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @endif
                                  @endif
                                @endforeach
                              @endif

                              @endif

                              @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                              || (Permissions::can('delete', 'rfis'))) @if(count($rfiVersion->file)) @foreach($rfiVersion->file as
                    $file) @if($file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                   data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                  <div class="pull-left delete-wait"
                                       style="display: none;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt=""
                                         width="17px">
                                  </div>
                                </a>
                                <input type="hidden" data-type="rfis" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">                    @endif @endforeach @endif @endif
                            </div>
                        </div>
                      </div>
                      <div class="">
                        <label>{{trans('labels.submittals.received_from_approval')}}</label>
                        <div class="row">
                          <div class="col-md-4">
                              <div class="input-group">
                                <input type="text" id="rec-appr" class="form-control " name="rec_appr" value="{{ $rec_appr }}">
                                <span class="input-group-addon">
                                @if($rfiVersion->rec_appr != 0)
                                    @if($rfiVersion->subm_sent_sub != 0)
                                      <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfiVersion->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($rfiVersion->rec_appr))); ?>
                                    @else
                                      <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfiVersion->rec_appr))); ?>
                                    @endif
                                    <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>
                                  @endif
                                </span>
                              </div>
                          </div>
                          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                          || (Permissions::can('write', 'rfis')))
                            <div class="col-md-4">
                              <!-- // upload input -->
                                <div class="input-group">
                                  <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          <input type="file" style="display: none;" name="rec_appr_file" id="rec_appr_file" multiple>
                                          <span class="glyphicon glyphicon-upload"></span>
                                      </span>
                                  </label>
                                    @if(count($rfiVersion->file))
                                      <?php $i = 0; ?>
                                      @foreach($rfiVersion->file as $file)
                                        @if($file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                          <input type="hidden" name="rec_appr_file_id"
                                                 id="rec_appr_file_id" value="{{$file->id}}">
                                          <?php $i = 1; ?>
                                        @endif
                                      @endforeach
                                      @if($i == 0)
                                        <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id">
                                      @endif
                                    @else
                                      <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-1">
                              <div id="rec_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                     width="17px">
                              </div>
                            </div>
                            <div class="col-md-3">
                            @if(count($rfiVersion->file))
                              @foreach($rfiVersion->file as $file)
                                @if($file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                  <!-- {{trans('labels.file.current')}} -->
                                    <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                    @if(isset($rfiVersion['email_transmittal_files']['submSent3Path']) && $rfiVersion['email_transmittal_files']['submSent3Path'] == Config::get('constants.submittal_version_files.rec_appr_file'))
                                      <input type="hidden" class="s3FilePath submSent3Path" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @else
                                      <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @endif
                                  @endif
                                @endforeach
                              @endif
                              @endif
                              @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                              || (Permissions::can('delete', 'rfis'))) @foreach($rfiVersion->file as $file)
                                @if($file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                <a href="javascript:;" class="btn btn-sm btn-danger delete-file pull-left ml5" data-toggle="modal" data-target="#confirmDelete"
                                   data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                  <div class="pull-left delete-wait"
                                       style="display: none;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt=""
                                         width="17px">
                                  </div>
                                </a>
                                <input type="hidden" data-type="rfis" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">                    @endif @endforeach @endif
                            </div>
                        </div>
                      </div>
                      <div class="">
                        <label>{{trans('labels.submittals.sent_to_subcontractor')}}</label>
                        <div class="row">
                          <div class="col-md-4">

                              <div class="input-group">
                                <input type="text" id="subm-sent-sub" class="form-control " name="subm_sent_sub" value="{{ $subm_sent_sub }}">
                                <span class="input-group-addon">
                                  @if($rfiVersion->subm_sent_sub != 0)
                                    @if(!in_array($rfiVersion->status->short_name, ['Closed']))
                                      <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfiVersion->subm_sent_sub))); ?>
                                      <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>
                                    @endif
                                  @endif
                                </span>
                              </div>
                          </div>
                          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                          || (Permissions::can('write', 'rfis')))
                            <div class="col-md-4">
                              <!-- // upload input -->
                                <div class="input-group">
                                  <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                  <label class="input-group-btn">
                                      <span class="btn btn-primary">
                                          <input type="file" style="display: none;" name="subm_sent_sub_file" id="subm_sent_sub_file" multiple>
                                          <span class="glyphicon glyphicon-upload"></span>
                                      </span>
                                  </label>
                                    @if(count($rfiVersion->file))
                                      <?php $i = 0; ?>
                                      @foreach($rfiVersion->file as $file)
                                        @if($file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                          <input type="hidden" name="subm_sent_sub_file_id"
                                                 id="subm_sent_sub_file_id" value="{{$file->id}}">
                                          <?php $i = 1; ?>
                                        @endif
                                      @endforeach
                                      @if($i == 0)
                                        <input type="hidden" name="subm_sent_sub_file_id"
                                               id="subm_sent_sub_file_id">
                                      @endif
                                    @else
                                      <input type="hidden" name="subm_sent_sub_file_id"
                                             id="subm_sent_sub_file_id">
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-1">
                              <div id="subm_sent_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                     width="17px">
                              </div>
                            </div>
                            <div class="col-md-3">
                            @if(count($rfiVersion->file))
                              @foreach($rfiVersion->file as $file)
                                @if($file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                  <!-- {{trans('labels.file.current')}} -->
                                    <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                    @if(isset($rfiVersion['email_transmittal_files']['submSent3Path']) && $rfiVersion['email_transmittal_files']['submSent3Path'] == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                      <input type="hidden" class="s3FilePath submSent3Path" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @else
                                      <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                    @endif
                                  @endif
                                @endforeach
                              @endif

                              @endif

                              @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                              || (Permissions::can('delete', 'rfis'))) @if(count($rfiVersion->file)) @foreach($rfiVersion->file as $file)
                                @if($file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                   data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                  <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                  <div class="pull-left delete-wait"
                                       style="display: none;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt=""
                                         width="17px">
                                  </div>
                                </a>
                                <input type="hidden" data-type="rfis" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">                    @endif @endforeach @endif @endif
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {!! Form::close()!!}
        <div class="row box-cont-2">
          <div class="col-md-12">
            <div class="box">
              <div class="card">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row">
                        <form method="POST" id="notesForm" action="{{url('projects/'.$project->id.'/rfis/'.$rfiVersion->rfi_id.'/version/'.$rfiVersion->id.'/notes')}}">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                          <div class="col-md-12">
                            <label class="control-label">
                              Transmittal Notes
                              <small class="cm-heading-btn">{{trans('labels.char-rows-15')}}</small>
                            </label>
                            <ul class="nav nav-tabs cm-inner-tabs">
                              <li class="{{(isset($_GET['tab']) && $_GET['tab'] == 'subm_sent_sub')?'':'active'}}">
                                <a data-toggle="tab" href="#trans">
                                  {{trans('labels.rfis.question')}}
                                </a>
                              </li>
                              <li class="{{(isset($_GET['tab']) && $_GET['tab'] == 'subm_sent_sub')?'in active':''}}">
                                <a data-toggle="tab" href="#submit">
                                  {{trans('labels.rfis.answer')}}
                                </a>
                              </li>
                            </ul>
                            <div class="tab-content mt20">
                              <!--TAB 1 =========================================-->
                              <div id="trans" class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == 'subm_sent_sub')?'':'in active'}}">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <textarea class="form-control span12 textEditorSmallVersion" rows="5" name="version_question" id="version_question">{!! $rfiVersion->question !!}</textarea>
                                    <span id="version_question_character_count"></span>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12 cf">
                                      @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                      || (Permissions::can('write', 'rfis')))
                                        <a class="btn btn-sm btn-info pull-left" id="generateRecTransmittal" href="javascript:;">
                                          <span class="glyphicon glyphicon-duplicate mr5" aria-hidden="true"></span>
                                          <span>Create Transmittal</span>
                                          <div id="rec_transmittal_wait" style="display: none;">
                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                                 width="17px">
                                          </div>
                                        </a>
                                        <input type="hidden" id="recTransmittalDate" name="recTransmittalDate" value="{{Config::get('constants.transmittal_date_type.sent_appr')}}">
                                        @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                          <a class="btn btn-sm cm-btn-secondary download ml5 pull-left" href="javascript:;">
                                            <i class="glyphicon glyphicon-download mr5" aria-hidden="true"></i>
                                            {{trans('labels.files.download')}}
                                          </a>
                                          <input type="hidden" class="s3FilePath" id="{{$recTransmittal->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$recTransmittal->file->file_name}}">
                                        @endif
                                      @endif
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                              <div class="cms-list-box">
                                                <ul class="cms-list-group">
                                                  <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" name="select_all_companies" class="select_all_companies select_all_company_users" id="select_all_companies">
                                                        <span class="checkbox-material">
                                                          <span class="check"></span>
                                                        </span>
                                                        Select All Companies
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <?php $allCompanyIds[] = '0'.$project->id; ?>
                                                  <li class="cms-list-group-item cf">
                                                    <div class="radio">
                                                      <label>
                                                        <input type="radio" checked class="companyCheckboxIn" name="companyRadioDistributionIn" data-type="user" id="mycompany">
                                                        <span class="radio-material">
                                                <span class="check"></span>
                                              </span>
                                                        {{$myCompany->name}}
                                                      </label>
                                                    </div>
                                                  </li>
                                                  @foreach($companies as $id => $name)
                                                    <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                      <div class="radio">
                                                        <label>
                                                          <input type="radio" class="companyCheckboxDistributionIn" name="companyRadioDistributionIn" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                                          <span class="radio-material">
                                                <span class="check"></span>
                                              </span>
                                                          {{$name}}
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <?php $allCompanyIds[] = $id; ?>
                                                  @endforeach
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                              <div class="cms-list-box">
                                                <ul class="cms-list-group" id="usersListIn">
                                                  <?php $my_company_users = []; $myCompanyUserId = 0; $myCompanyUserCount = 0; ?>
                                                  @if(count($myCompany->users) > 0)
                                                    <li class="cms-list-group-item cf">
                                                      <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_0{{$project->id}}">
                                                          <span class="checkbox-material">
                                                  <span class="check"></span>
                                                </span>
                                                          Select All Users
                                                        </label>
                                                      </div>
                                                    </li>
                                                    @foreach($myCompany->users as $user)
                                                      @if(in_array($user->id, $projectUsers) || $user->hasRole(Config::get('constants.roles.company_admin')))
                                                        <li class="cms-list-group-item cf" id="0{{$project->id}}_{{$user->id}}">
                                                          <div class="checkbox">
                                                            <label>
                                                              <input {{ (!empty(old('employees_users_in')) && in_array($user->id, old('employees_users_in')))?'checked':''}}
                                                                     type="checkbox" class="users_0{{$project->id}}" name="employees_users_in[]" value="{{$user->id}}">
                                                              <span class="checkbox-material">
                                                          <span class="check"></span>
                                                        </span>
                                                              {{$user->name}}
                                                            </label>
                                                          </div>
                                                        </li>
                                                      @endif
                                                    @endforeach
                                                  @endif
                                                </ul>
                                                <?php $other_users = []; ?>
                                                @foreach($users as $id => $value)
                                                  <ul class="cms-list-group contactsListDistributionIn hide" id="contactsListDistributionIn-{{$id}}">
                                                    @if(count($value) > 0)
                                                      <li class="cms-list-group-item cf">
                                                        <div class="checkbox">
                                                          <label>
                                                            <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_{{$id}}">
                                                            <span class="checkbox-material">
                                                                                          <span class="check"></span>
                                                                                      </span>
                                                            Select All Users
                                                          </label>
                                                        </div>
                                                      </li>
                                                      @foreach($value as $project_contact)
                                                        @if (!empty($project_contact->contact))
                                                          <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                            <div class="checkbox">
                                                              <label>
                                                                <input {{ (!empty(old('employees_contacts_in')) && in_array($project_contact->contact->id, old('employees_contacts_in')))?'checked':''}}
                                                                       type="checkbox" name="employees_contacts_in[]" class="users_{{$id}}" value="{{$project_contact->contact->id}}">
                                                                <span class="checkbox-material">
                                                      <span class="check"></span>
                                                    </span>
                                                                {{$project_contact->contact->name}}
                                                              </label>
                                                            </div>
                                                          </li>
                                                          <?php $other_users[$project_contact->contact->id] = [$id => $project_contact->contact->name]; ?>
                                                        @endif
                                                      @endforeach
                                                    @endif
                                                  </ul>
                                                @endforeach
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <input id="allCompanyIds" type="hidden" value="{{implode(',',$allCompanyIds)}}">
                                    </div>
                                  @endif
                                  <div class="row">
                                    <div class="col-md-12 cf">
                                      @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                      || (Permissions::can('write', 'rfis')))
                                        @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                          <a class="btn btn-sm btn-primary pull-left ml5 sendTransmittalEmail" title="{{trans('labels.transmittals.send_email')}}"
                                             data-transmittal-path="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$recTransmittal->file->file_name}}"
                                             data-transmittal-id="{{$recTransmittal->id}}" data-file-uploaded-selector="sentAppr3Path" data-file-id="{{$recTransmittal->file->id}}"
                                             data-emailed="{{$recTransmittal->emailed}}" data-sub-rec="{{Config::get('constants.ab_company_type.recipient')}}"
                                             href="javascript:;">
                                            <span class="glyphicon glyphicon-send mr5" aria-hidden="true"></span>
                                            <span>Email Transmittal</span>
                                            <div class="pull-left email_wait" style="display: none;">
                                              <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                                   width="17px">
                                            </div>
                                          </a>
                                        @endif
                                      @endif
                                    </div>
                                    @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                      <div class="col-md-12 cf">
                                        <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                        @if(!empty($rfiVersion->rfiVersionApprUsers))
                                          @foreach($rfiVersion->rfiVersionApprUsers as $rfiVersionUser)
                                            <?php $sendDate = Carbon\Carbon::parse($rfiVersionUser->created_at); ?>
                                            @if(!empty($rfiVersionUser->users))
                                              @foreach($rfiVersionUser->users as $user)
                                                {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                              @endforeach
                                            @endif
                                            @if(!empty($rfiVersionUser->abUsers))
                                              @foreach($rfiVersionUser->abUsers as $user)
                                                {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}} ({{$sendDate->format('m/d/Y')}}) <br />
                                              @endforeach
                                            @endif
                                          @endforeach
                                        @endif
                                      </div>
                                    @endif
                                  </div>
                                </div>
                              </div>
                              <!--TAB 2 =========================================-->
                              <div id="submit" class="tab-pane fade {{(isset($_GET['tab']) && $_GET['tab'] == 'subm_sent_sub')?'in active':''}}">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <textarea class="form-control span12 textEditorSmallVersion" rows="5" name="version_answer" id="version_answer">{!! $rfiVersion->answer !!}</textarea>
                                    <span id="version_answer_character_count"></span>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                      || (Permissions::can('write', 'rfis')))
                                        <a class="btn btn-sm btn-info pull-left" id="generateSubTransmittal" href="javascript:;">
                                          <span class="glyphicon glyphicon-duplicate mr5" aria-hidden="true"></span>
                                          <span>Create Transmittal</span>
                                          <div id="sub_transmittal_wait" class="pull-left" style="display: none;">
                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                                 width="17px">
                                          </div>
                                        </a>
                                        <input type="hidden" id="subTransmittalDate" name="subTransmittalDate" value="{{Config::get('constants.transmittal_date_type.subm_sent_sub')}}">
                                        @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                          <a class="btn btn-sm cm-btn-secondary download ml5 pull-left" href="javascript:;">
                                            <i class="glyphicon glyphicon-download mr5" aria-hidden="true"></i>
                                            {{trans('labels.files.download')}}
                                          </a>
                                          <input type="hidden" class="s3FilePath" id="{{$subTransmittal->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$subTransmittal->file->file_name}}">
                                        @endif
                                      @endif
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                  @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                              <div class="cms-list-box">
                                                <ul class="cms-list-group">
                                                  <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" name="select_all_subcontractor_companies" class="select_all_subcontractor_companies select_all_company_users" id="select_all_subcontractor_companies">
                                                        <span class="checkbox-material">
                                                <span class="check"></span>
                                              </span>
                                                        Select All Companies
                                                      </label>
                                                    </div>
                                                  </li>
                                                  <?php $allSubcontractorCompanyIds[] = '0'.$project->id; ?>
                                                  <li class="cms-list-group-item cf">
                                                    <div class="radio">
                                                      <label>
                                                        <input type="radio" checked class="companyCheckboxOut" name="companyRadioDistributionOut" data-type="user" id="mycompany2">
                                                        <span class="radio-material">
                                                <span class="check"></span>
                                              </span>
                                                        {{$myCompany->name}}
                                                      </label>
                                                    </div>
                                                  </li>
                                                  @foreach($companies as $id => $name)
                                                    <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-out">
                                                      <div class="radio">
                                                        <label>
                                                          <input type="radio" class="companyCheckboxDistributionOut" name="companyRadioDistributionOut" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                                          <span class="radio-material">
                                                <span class="check"></span>
                                              </span>
                                                          {{$name}}
                                                        </label>
                                                      </div>
                                                    </li>
                                                    <?php $allSubcontractorCompanyIds[] = $id; ?>
                                                  @endforeach
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                              <div class="cms-list-box">
                                                <ul class="cms-list-group" id="usersListOut">
                                                  <?php $my_company_users = []; $myCompanyUserId = 0; $myCompanyUserCount = 0; ?>
                                                  @if(count($myCompany->users) > 0)
                                                    <li class="cms-list-group-item cf">
                                                      <div class="checkbox">
                                                        <label>
                                                          <input type="checkbox" name="select_all_subcontractor_users" class="select_all_subcontractor_users" id="select_all_subcontractor_users_0{{$project->id}}">
                                                          <span class="checkbox-material">
                                                  <span class="check"></span>
                                                </span>
                                                          Select All Users
                                                        </label>
                                                      </div>
                                                    </li>
                                                    @foreach($myCompany->users as $user)
                                                      @if(in_array($user->id, $projectUsers) || $user->hasRole(Config::get('constants.roles.company_admin')))
                                                        <li class="cms-list-group-item cf" id="0{{$project->id}}_{{$user->id}}">
                                                          <div class="checkbox">
                                                            <label>
                                                              <input {{ (!empty(old('employees_users_out')) && in_array($user->id, old('employees_users_out')))?'checked':''}}
                                                                     type="checkbox" name="employees_users_out[]" class="subcontractor_users_0{{$project->id}}" value="{{$user->id}}">
                                                              <span class="checkbox-material">
                                                          <span class="check"></span>
                                                        </span>
                                                              {{$user->name}}
                                                            </label>
                                                          </div>
                                                        </li>
                                                        <?php $my_company_users[$user->id] = [$myCompany->id => $user->name]; ?>
                                                        @if($myCompanyUserCount == 0)
                                                          <?php $myCompanyUserId =  $user->id; $myCompanyUserCount++; ?>
                                                        @endif
                                                      @endif
                                                    @endforeach
                                                  @endif
                                                </ul>
                                                <?php $otherSubcontractorUsers = []; ?>
                                                @foreach($users as $id => $value)
                                                  <ul class="cms-list-group contactsListDistributionOut hide" id="contactsListDistributionOut-{{$id}}">
                                                    @if(count($value) > 0)
                                                      <li class="cms-list-group-item cf">
                                                        <div class="checkbox">
                                                          <label>
                                                            <input type="checkbox" name="select_all_subcontractor_users" class="select_all_subcontractor_users" id="select_all_subcontractor_users_{{$id}}">
                                                            <span class="checkbox-material">
                                                  <span class="check"></span>
                                                </span>
                                                            Select All Users
                                                          </label>
                                                        </div>
                                                      </li>
                                                      @foreach($value as $project_contact)
                                                        @if (!empty($project_contact->contact))
                                                          <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-out" id="{{$id}}_{{$project_contact->contact->id}}">
                                                            <div class="checkbox">
                                                              <label>
                                                                <input {{(!empty(old('employees_contacts_out')) && in_array($project_contact->contact->id, old('employees_contacts_out')))?'checked':''}}
                                                                       type="checkbox" name="employees_contacts_out[]" class="subcontractor_users_{{$id}}" value="{{$project_contact->contact->id}}">
                                                                <span class="checkbox-material">
                                                      <span class="check"></span>
                                                    </span>
                                                                {{$project_contact->contact->name}}
                                                              </label>
                                                            </div>
                                                          </li>
                                                          <?php $otherSubcontractorUsers[$project_contact->contact->id] = [$id => $project_contact->contact->name]; ?>
                                                        @endif
                                                      @endforeach
                                                    @endif
                                                  </ul>
                                                @endforeach
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <input id="allSubcontractorCompanyIds" type="hidden" value="{{implode(',',$allSubcontractorCompanyIds)}}">
                                    </div>
                                  @endif
                                  <div class="row">
                                  <div class="col-md-12">
                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                    || (Permissions::can('write', 'rfis')))
                                      @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                        <a class="btn btn-sm btn-primary pull-left ml5 sendTransmittalEmail" title="{{trans('labels.transmittals.send_email')}}"
                                           data-transmittal-path="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$subTransmittal->file->file_name}}"
                                           data-transmittal-id="{{$subTransmittal->id}}" data-file-uploaded-selector="submSent3Path" data-file-id="{{$subTransmittal->file->id}}"
                                           data-emailed="{{$subTransmittal->emailed}}" data-sub-rec="{{Config::get('constants.ab_company_type.subcontractor')}}"
                                           href="javascript:;">
                                          <span class="glyphicon glyphicon-send mr5" aria-hidden="true"></span>
                                          <span>Email Transmittal</span>
                                          <div class="pull-left email_wait" style="display: none;">
                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                                 width="17px">
                                          </div>
                                        </a>
                                      @endif
                                    @endif
                                  </div>
                                  @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                    <div class="col-md-12">
                                      <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                      @if(!empty($rfiVersion->rfiVersionSubcUsers))
                                        @foreach($rfiVersion->rfiVersionSubcUsers as $rfiVersionUser)
                                          <?php $sendDate = Carbon\Carbon::parse($rfiVersionUser->created_at); ?>
                                          @if(!empty($rfiVersionUser->users))
                                            @foreach($rfiVersionUser->users as $user)
                                              {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                            @endforeach
                                          @endif
                                          @if(!empty($rfiVersionUser->abUsers))
                                            @foreach($rfiVersionUser->abUsers as $user)
                                              {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}} ({{$sendDate->format('m/d/Y')}}) <br />
                                            @endforeach
                                          @endif
                                        @endforeach
                                      @endif
                                    </div>
                                  @endif
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('projects.partials.successful-upload')
  @include('projects.partials.error-upload')
  @include('projects.partials.upload-limitation')
  @include('popups.alert_popup')
  @include('popups.delete_record_popup')
  @include('popups.approve_popup')
@endsection
