@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-7">
            <header class="cm-heading">
                Edit RFI
                <small class="cm-heading-suffix">{{trans('labels.rfi').': '.$rfi->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/rfis" class="cm-trail-link">{{trans('labels.rfis.project_rfis')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/rfis/{{$rfi->id}}/edit" class="cm-trail-link">{{trans('labels.rfis.edit_rfi')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-5">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
                    <a class="btn btn-success cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'9'.'&module_item='.$rfi->id.'&title='.$rfi->name.'&mf_number='.$rfi->mf_number.'&mf_title='.$rfi->mf_title.'&mf_number_title='.((!empty($rfi->mf_number) && !empty($rfi->mf_title))?($rfi->mf_number.' - '.$rfi->mf_title):'')}}">{{trans('labels.tasks.create')}}</a>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'rfis'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            <div style="margin-bottom: 60px;">
                <div class="col-md-12">
                    @include('projects.partials.tabs-inner', array('activeTab' =>  Input::get('activeInnerTab', 'files')))
                </div>
            </div>
            <div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                        }}
                    </div>
                @endif
                <div class="tab-content">
                    <div id="info" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'info') in active @endif">
                        <div id="transmittal_info_cont" class="mt20">
                            {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/rfis/'.$rfi->id), 'id' => 'rfis', 'role' => 'form']) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <div class="row box-cont-1">
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>RFI Info</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required cm-control-required">{{trans('labels.rfis.name')}}</label>
                                                                        <input type="text" class="form-control" name="name" value="{{$rfi->name}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required cm-control-required">{{trans('labels.rfis.version_no')}}</label>
                                                                        <input type="text" readonly class="form-control" id="item-number" name="number" value="{{$rfi->number}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" id="custom-number" class="btn btn-primary pull-right">
                                                                        {{trans('labels.rfis.enter_version_number')}}
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                                                        <input type="text" class="cm-control-required form-control mf-number-title-auto"
                                                                               name="master_format_search"
                                                                               value="{{ old('master_format_search') }}">
                                                                        <input type="hidden" class="master-format-id" name="master_format_id"
                                                                               value="{{old('master_format_id')}}">
                                                                        <input type="hidden" id="project_id" value="{{$project->id}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group mf-cont">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right">
                                                                            {{trans('labels.submittals.enter_version_number_custom')}}
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required control-label">{{trans('labels.spec_number')}}</label>
                                                                        <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{$rfi->mf_number}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required control-label">{{trans('labels.spec_title')}}</label>
                                                                        <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{$rfi->mf_title}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <?php
                                                        $subcontractor_notification = false; $subcontractorId = null;
                                                        $recipient_notification = false; $recipientId = null;
                                                        ?>
                                                        <div class="col-md-7">
                                                            <div class="form-group" id="subcontractor_container" @if($rfi->self_performed) {{'style=display:none;'}} @endif>
                                                                <label>{{trans('labels.submittals.import_supplier_from_companies')}}</label>
                                                                @if(!is_null($rfi->subcontractor) && $rfi->sub_id != 0)
                                                                    <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="{{$rfi->subcontractor->name}}">
                                                                    <input type="hidden" class="address-book-id" name="sub_id" value="{{$rfi->sub_id}}">
                                                                @else
                                                                    <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="">
                                                                    <input type="hidden" class="address-book-id" name="sub_id" value="">
                                                                @endif
                                                            </div>
                                                            <div class="form-group" id="notif_subcontractor" @if(!$subcontractor_notification) style="display: none;" @endif>
                                                                <input type="hidden" id="notif_subcontractor_id" name="notif_subcontractor_id" value="{{$subcontractorId}}">
                                                            </div>

                                                            <div class="form-group">
                                                                <input type="checkbox" name="self_performed" id="self_performed" @if($rfi->self_performed) {{'checked'}} @endif> {{trans('labels.submittals.self_performed')}}
                                                            </div>
                                                            <hr>
                                                        </div>
                                                        <div class="col-md-5">
                                                            @if(!is_null($rfi->subcontractor) && $rfi->sub_id != 0)
                                                                <div class="form-group subcontractor_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                    <div class="row">
                                                                        <div class="col-sm-12">
                                                                            <label>Select Contact:</label>
                                                                            <select name="sub_contact" id="sub_contact">
                                                                                <option value=""></option>
                                                                                @if(count($projectSubcontractorContacts))
                                                                                    @foreach($projectSubcontractorContacts as $contact)
                                                                                        <?php $subcontractor_notification = true; if($rfi->sub_contact_id == $contact->id) $subcontractorId = $contact->id; ?>
                                                                                        <option value="{{$contact->id}}" @if($rfi->sub_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                    @endforeach
                                                                                @else
                                                                                    <option value="">No available contacts for this company</option>
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            @else
                                                                <div class="form-group subcontractor_contacts_container col-sm-12" style="margin-bottom: 10px;"></div>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="form-group">
                                                                <label>
                                                                    {{trans('labels.submittals.import_recipient_from_companies')}}
                                                                </label>
                                                                <?php
                                                                $recipientValue = '';
                                                                $recipientIdValue = '';
                                                                if (!is_null($rfi->recipient) && $rfi->recipient_id != 0 && count($projectRecipientContacts) > 0) {
                                                                    $recipientValue = $rfi->recipient->name;
                                                                    $recipientIdValue = $rfi->recipient_id;
                                                                } else if(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0) {
                                                                    $recipientValue = $project->architect->name;
                                                                    $recipientIdValue = $project->architect->id;
                                                                } else if(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0) {
                                                                    $recipientValue = $project->primeSubcontractor->name;
                                                                    $recipientIdValue = $project->primeSubcontractor->id;
                                                                } else if(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0) {
                                                                    $recipientValue = $project->owner->name;
                                                                    $recipientIdValue = $project->owner->id;
                                                                } else if(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0) {
                                                                    $recipientValue = $project->generalContractor->name;
                                                                    $recipientIdValue = $project->generalContractor->id;
                                                                }
                                                                ?>
                                                                <input type="text" class="form-control address-book-recipient-auto" name="recipient" value="{{$recipientValue}}">
                                                                <input type="hidden" class="address-book-recipient-id" name="recipient_id" value="{{$recipientIdValue}}">
                                                            </div>
                                                            <hr>
                                                        </div>
                                                        <div class="col-md-5">
                                                            @if(!is_null($rfi->recipient) && $rfi->recipient_id != 0 && count($projectRecipientContacts) > 0)
                                                                @if(!is_null($rfi->recipient))
                                                                    <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Select Contact:</label>
                                                                                <select name="recipient_contact" id="recipient_contact">
                                                                                    <option value=""></option>
                                                                                    @if(count($projectRecipientContacts))
                                                                                        @foreach($projectRecipientContacts as $contact)
                                                                                            <?php $recipient_notification = true; if($rfi->recipient_contact_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                            <option value="{{$contact->id}}" @if($rfi->recipient_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                        @endforeach
                                                                                    @else
                                                                                        <option value="">No available contacts for this company</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @elseif(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                                @if(isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                                    <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Select Contact:</label>
                                                                                <select name="recipient_contact" id="recipient_contact">
                                                                                    <option value=""></option>
                                                                                    @if(count($architectProjectTransmittals))
                                                                                        @foreach($architectProjectTransmittals as $contact)
                                                                                            <?php $recipient_notification = true; if($project->architect_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                            <option value="{{$contact->id}}" @if($project->architect_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                        @endforeach
                                                                                    @else
                                                                                        <option value="">No available contacts for this company</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @elseif(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                                @if(isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                                    <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Select Contact:</label>
                                                                                <select name="recipient_contact" id="recipient_contact">
                                                                                    <option value=""></option>
                                                                                    @if(count($primeSubcontractorProjectTransmittals))
                                                                                        @foreach($primeSubcontractorProjectTransmittals as $contact)
                                                                                            <?php $recipient_notification = true; if($project->prime_subcontractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                            <option value="{{$contact->id}}" @if($project->prime_subcontractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                        @endforeach
                                                                                    @else
                                                                                        <option value="">No available contacts for this company</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @elseif(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                                @if(isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                                    <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Select Contact:</label>
                                                                                <select name="recipient_contact" id="recipient_contact">
                                                                                    <option value=""></option>
                                                                                    @if(count($ownerProjectTransmittals))
                                                                                        @foreach($ownerProjectTransmittals as $contact)
                                                                                            <?php $recipient_notification = true; if($project->owner_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                            <option value="{{$contact->id}}" @if($project->owner_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                        @endforeach
                                                                                    @else
                                                                                        <option value="">No available contacts for this company</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @elseif(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                                @if(isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                                    <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Select Contact:</label>
                                                                                <select name="recipient_contact" id="recipient_contact">
                                                                                    <option value=""></option>
                                                                                    @if(count($contractorProjectTransmittals))
                                                                                        @foreach($contractorProjectTransmittals as $contact)
                                                                                            <?php $recipient_notification = true; if($project->contractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                            <option value="{{$contact->id}}" @if($project->contractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                        @endforeach
                                                                                    @else
                                                                                        <option value="">No available contacts for this company</option>
                                                                                    @endif
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            @else
                                                                <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;"></div>
                                                            @endif
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label class="mb5">
                                                                    {{trans('labels.rfis.import_general_contractor')}}
                                                                </label>
                                                                <div class="row">
                                                                    <div class="col-md-12 general_contractor_container">
                                                                        @if($project->make_me_gc && isset($project->company->name))
                                                                            <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->company->name}}" readonly>
                                                                            <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->company->id}}">
                                                                        @elseif(!is_null($rfi->general_contractor) && $rfi->gc_id != 0)
                                                                            <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$rfi->general_contractor->name}}" <?php (!empty($project->general_contractor_id))?'readonly':'' ?>>
                                                                            <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$rfi->gc_id}}">
                                                                        @elseif(isset($project->generalContractor->name))
                                                                            <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->generalContractor->name}}">
                                                                            <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->generalContractor->id}}">
                                                                        @else
                                                                            <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="">
                                                                            <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="">
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Transmittal Info</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.subject')}}</label>
                                                                        <input type="text" class="form-control" name="subject" value="{{ $rfi->subject }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.sent_via')}}</label>
                                                                        <select class="custom-options" name="sent_via_dropdown" id="sent_via_dropdown" data-id="sent">
                                                                            @foreach($sentVia as $item)
                                                                                <option <?php echo (!empty($rfi->sent_via) && $rfi->sent_via == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                            <option {{ (!empty($rfi->sent_via) && !in_array($rfi->sent_via, $sentVia))?'selected':'' }} value="0">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" class="custom-text-sent form-control {{ (!empty($rfi->sent_via) && !in_array($rfi->sent_via, $sentVia))?'':'hide' }}" name="sent_via" id="sent_via" value="{{ (!in_array($rfi->sent_via, $sentVia))?$rfi->sent_via:'' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.rfis.is_change')}}</label>
                                                                        <select class="custom-options" name="is_change" id="is_change">
                                                                            @foreach($rfiChange as $item)
                                                                                <option <?php echo (!empty($rfi->is_change) && $rfi->is_change == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control">{{trans('labels.rfis.internal_note')}}</label>
                                                                        <textarea class="form-control span12 textEditorSmall" rows="4" name="note">{!! $rfi->note !!}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-success pull-right mt20">
                                                                        {{trans('labels.update')}}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="cm-spacer-xs"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                    <div id="files" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'files') in active @endif">
                        <div class="col-md-12 mt20">
                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'rfis')))
                                <a href="{{URL('projects/'.$project->id.'/rfis/'.$rfi->id.'/version/create')}}" class="btn btn-primary">{{trans('labels.submittals.add_new_version')}}</a>
                            @endif
                            <div class="pull-right">
                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'rfis')))
                                    @if(count($rfi->rfis_versions) > 1) {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                    <input type="hidden" value="{{URL('projects/'.$project->id.'/rfis/'.$rfi->id.'/version').'/'}}" id="form-url" />
                                    <button data-limit="1" disabled id="delete-button" class='btn btn-danger pull-right mr5' type='submit' data-toggle="modal"
                                            data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                        {{trans('labels.delete')}}
                                    </button>
                                    {!! Form::close()!!}
                                    @endif
                                @endif
                            </div>
                            @if($rfi->rfis_versions->count() != 0)
                                <div class="cm-spacer-xs"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered cm-table-compact">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>{{trans('labels.rfis.version_no')}}</th>
                                            <th>{{trans('labels.rfis.name')}}</th>
                                            <th>{{trans('labels.submittals.cycle_no')}}</th>
                                            <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                            <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                            <th>{{trans('labels.status')}}</th>
                                            <th>{{trans('labels.files.download')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $rfisVersions = $rfi->rfis_versions;




                                        ?>
                                        @for ($i=0; $i < count($rfisVersions); $i++)
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="multiple-items-checkbox" data-id="{{$rfisVersions[$i]->id}}">
                                                </td>
                                                <td>{{$rfi->number}}</td>
                                                <td>
                                                    <a href="{{URL('projects/'.$project->id.'/rfis/'.$rfi->id.'/version/'.$rfisVersions[$i]->id.'/edit')}}" class="">{{$rfi->name}}</a>
                                                </td>
                                                <td>{{$rfisVersions[$i]->cycle_no}}</td>
                                                <td>
                                                    @if($rfisVersions[$i]->rec_sub != 0) {{date("m/d/Y", strtotime($rfisVersions[$i]->rec_sub))}} @if($rfisVersions[$i]->sent_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->sent_appr))) - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_sub)));




                                                        ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_sub)));




                                                        ?> @endif ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($rfisVersions[$i]->sent_appr != 0)
                                                        {{date("m/d/Y", strtotime($rfisVersions[$i]->sent_appr))}}
                                                        @if($rfisVersions[$i]->rec_appr != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_appr))) - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->sent_appr)));




                                                            ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->sent_appr)));




                                                            ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($rfisVersions[$i]->rec_appr != 0) {{date("m/d/Y", strtotime($rfisVersions[$i]->rec_appr))}}
                                                    @if(isset($rfisVersions[$i+1]) && $rfisVersions[$i]->rec_sub == 0 && $rfisVersions[$i+1]->sent_appr != 0 && $rfisVersions[$i+1]->rec_sub == 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfisVersions[$i+1]->sent_appr))) - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_appr)));




                                                        ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @else
                                                        @if($rfisVersions[$i]->subm_sent_sub != 0 && !empty($rfisVersions[$i]->subm_sent_sub))
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_appr)));




                                                            ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->rec_appr)));




                                                            ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($rfisVersions[$i]->subm_sent_sub != 0) {{date("m/d/Y", strtotime($rfisVersions[$i]->subm_sent_sub))}}
                                                    @if(isset($rfisVersions[$i+1]) && $rfisVersions[$i+1]->rec_sub != 0 && !empty($rfisVersions[$i+1]->rec_sub))
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($rfisVersions[$i+1]->rec_sub))) - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->subm_sent_sub)));




                                                        ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @else
                                                        @if(!in_array($rfisVersions[$i]->status->short_name, ['Closed']))
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($rfisVersions[$i]->subm_sent_sub)));




                                                            ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                    @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!is_null($rfisVersions[$i]->status)) {{$rfisVersions[$i]->status->short_name}} @endif
                                                </td>
                                                <td>
                                                    @if(count($rfisVersions[$i]->transmittalSubmSentFile) > 0 || count($rfisVersions[$i]->transmittalSentFile) > 0)
                                                        @if(count($rfisVersions[$i]->transmittalSubmSentFile) > 0 &&
                                                           !empty($rfisVersions[$i]->subm_sent_sub) &&
                                                           $rfisVersions[$i]->subm_sent_sub != '0000-00-00')
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$rfisVersions[$i]->transmittalSubmSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$rfisVersions[$i]->transmittalSubmSentFile[0]->file_name}}">
                                                            </p>
                                                        @elseif(count($rfisVersions[$i]->transmittalSentFile) > 0 &&
                                                            !empty($rfisVersions[$i]->sent_appr) &&
                                                            $rfisVersions[$i]->sent_appr != '0000-00-00' &&
                                                           (empty($rfisVersions[$i]->subm_sent_sub) ||
                                                           $rfisVersions[$i]->subm_sent_sub == '0000-00-00') &&
                                                           (empty($rfisVersions[$i]->rec_appr) ||
                                                           $rfisVersions[$i]->rec_appr == '0000-00-00'))
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$rfisVersions[$i]->transmittalSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$rfisVersions[$i]->transmittalSentFile[0]->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endif
                                                    @if($rfisVersions[$i]->download_file)
                                                        @if(!is_null($rfisVersions[$i]->file) && count($rfisVersions[$i]->file))
                                                            @foreach($rfisVersions[$i]->file as $file)
                                                                @if($file->version_date_connection == $rfisVersions[$i]->file_status)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </td>
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p class="text-center">{{trans('labels.no_records')}}</p>
                            @endif
                        </div>
                    </div>
                    <div id="tasks" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'tasks') in active @endif">
                        <div class="col-md-12">
                            @if(((Auth::user()->hasRole('Company Admin')) || Auth::user()->hasRole('Project Admin') || $userCanDelete))
                                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                <input type="hidden" value="{{url('tasks')}}/" id="form-url" />
                                <a href="#" disabled id="delete-button" class='btn btn-danger pull-right cm-btn-fixer delete-button-multi' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                    {{trans('labels.delete')}}
                                </a>
                                {!! Form::close()!!}
                            @endif
                            @if (Session::has('flash_notification.message'))
                                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('flash_notification.message') }}
                                </div>
                            @endif
                            @include("tasks.partials.table")
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('popups.delete_record_popup')
@endsection