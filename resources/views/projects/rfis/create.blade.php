@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.rfis.create')}}
                <small class="cm-heading-sub">{{trans('labels.Project').': '.$project->name}}</small>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'rfis'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                }}
                </div>
            @endif
            <form id="rfis" role="form" action="{{URL('/projects/'.$project->id.'/rfis')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <div class="row box-cont-1">
                        <div class="col-md-6">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>RFI Info</h4>
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required control-label">{{trans('labels.rfis.name')}}</label>
                                                            <input type="text" class="form-control" name="name" id="item-name" value="{{ old('name') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <label class="cm-control-required  control-label">{{trans('labels.rfis.version_no')}}</label>
                                                        <input type="text" readonly class="form-control" id="item-number" name="number" value="{{ (!empty(old('number')))?old('number'):$project->generated_number }}">
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label class="control-label">&nbsp;</label>
                                                        <a href="javascript:;" id="custom-number" class="btn btn-primary pull-right">
                                                            {{trans('labels.rfis.enter_version_number')}}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label class="cm-control-required  control-label">{{trans('labels.submittals.cycle_no')}}</label>
                                                            <input type="text" readonly class="form-control date-number" id="cycle_no" name="cycle_no" value="{{ !empty(old('cycle_no'))?old('cycle_no'):1 }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="control-label">&nbsp;</label>
                                                            <input type="hidden" id="useCustom" name="useCustom" value="false" >
                                                            <input type="hidden" id="currentCycleNumber" name="currentCycleNumber" value="1" >
                                                            <a href="#" id="customNumber" class="btn btn-primary pull-right">Custom</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                                            <input type="text" class="cm-control-required form-control mf-number-title-auto"
                                                                   name="master_format_search"
                                                                   value="{{ old('master_format_search') }}">
                                                            <input type="hidden" class="master-format-id" name="master_format_id"
                                                                   value="{{old('master_format_id')}}">
                                                            <input type="hidden" id="project_id" value="{{$project->id}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group mf-cont">
                                                            <label class="control-label">&nbsp;</label>
                                                            <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right">
                                                                {{trans('labels.submittals.enter_version_number_custom')}}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="cm-control-required control-label">{{trans('labels.spec_number')}}</label>
                                                            <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{old('master_format_number')}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <label class="cm-control-required control-label">{{trans('labels.spec_title')}}</label>
                                                            <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{old('master_format_title') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <div class="form-group" id="subcontractor_container">
                                                            <label class="control-label">{{trans('labels.submittals.import_supplier_from_companies')}}</label>
                                                            <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="{{ old('supplier_subcontractor') }}">
                                                            <input type="hidden" class="address-book-id" name="sub_id" value="{{old('sub_id')}}">
                                                            <input type="hidden" id="ab_sub_name" name="ab_sub_name" value="{{old('ab_sub_name')}}">
                                                        </div>
                                                        <div class="form-group" id="notif_subcontractor" @if(!old('notif_subcontractor_id')) style="display: none;" @endif>
                                                            <input type="hidden" id="notif_subcontractor_id" name="notif_subcontractor_id" value="">
                                                        </div>

                                                        <div class="form-group">
                                                            <input type="checkbox" name="self_performed" id="self_performed"> {{trans('labels.submittals.self_performed')}}
                                                        </div>

                                                        <hr>
                                                    </div>
                                                    <div class="col-md-5">
                                                        @if(old('sub_contact') != '')
                                                            <div class="subcontractor_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label>Select contact:</label>
                                                                        <select name="sub_contact" id="sub_contact">
                                                                            <option value="{{old('sub_contact')}}">{{old('ab_sub_name')}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="subcontractor_contacts_container col-sm-12" style="margin-bottom: 10px;"></div>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-7">
                                                        <?php $recipient_notification = false; $recipientId = null; ?>
                                                        <div class="form-group">
                                                            <label class="control-label">
                                                                {{trans('labels.submittals.import_recipient_from_companies')}}
                                                            </label>
                                                            <?php
                                                            $recipientValue = old('recipient');
                                                            $recipientIdValue = old('recipient_id');
                                                            if (isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0) {
                                                                $recipientValue = $project->architect->name;
                                                                $recipientIdValue = $project->architect->id;
                                                            } else if(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0) {
                                                                $recipientValue = $project->primeSubcontractor->name;
                                                                $recipientIdValue = $project->primeSubcontractor->id;
                                                            } else if(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0) {
                                                                $recipientValue = $project->owner->name;
                                                                $recipientIdValue = $project->owner->id;
                                                            } else if(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0) {
                                                                $recipientValue = $project->generalContractor->name;
                                                                $recipientIdValue = $project->generalContractor->id;
                                                            }
                                                            ?>
                                                            <input type="text" class="form-control address-book-recipient-auto" name="recipient" value="{{$recipientValue}}">
                                                            <input type="hidden" class="address-book-recipient-id" name="recipient_id" value="{{$recipientIdValue}}">
                                                        </div>
                                                        <hr>
                                                    </div>
                                                    <div class="col-md-5">
                                                        @if(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label>Select contact:</label>
                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                            <option value=""></option>
                                                                            @if(count($architectProjectTransmittals) > 0)
                                                                                @foreach($architectProjectTransmittals as $contact)
                                                                                    <?php $recipient_notification = true; if($project->architect_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                    <option value="{{$contact->id}}" @if($project->architect_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                @endforeach
                                                                            @else
                                                                                <option value="">No available contacts for this company</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label>Select contact:</label>
                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                            <option value=""></option>
                                                                            @if(count($primeSubcontractorProjectTransmittals) > 0)
                                                                                @foreach($primeSubcontractorProjectTransmittals as $contact)
                                                                                    <?php $recipient_notification = true; if($project->prime_subcontractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                    <option value="{{$contact->id}}" @if($project->prime_subcontractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                @endforeach
                                                                            @else
                                                                                <option value="">No available contacts for this company</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label>Select contact:</label>
                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                            <option value=""></option>
                                                                            @if(count($ownerProjectTransmittals) > 0)
                                                                                @foreach($ownerProjectTransmittals as $contact)
                                                                                    <?php $recipient_notification = true; if($project->owner_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                    <option value="{{$contact->id}}" @if($project->owner_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                @endforeach
                                                                            @else
                                                                                <option value="">No available contacts for this company</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @elseif(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <label>Select contact:</label>
                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                            <option value=""></option>
                                                                            @if(count($contractorProjectTransmittals) > 0)
                                                                                @foreach($contractorProjectTransmittals as $contact)
                                                                                    <?php $recipient_notification = true; if($project->contractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                    <option value="{{$contact->id}}" @if($project->contractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                @endforeach
                                                                            @else
                                                                                <option value="">No available contacts for this company</option>
                                                                            @endif
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;"></div>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label ">
                                                                {{trans('labels.rfis.import_general_contractor')}}
                                                            </label>
                                                            <div class="general_contractor_container">
                                                                @if($project->make_me_gc && isset($project->company->name))
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->company->name}}" readonly>
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->company->id}}">
                                                                @elseif(isset($project->generalContractor->name))
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->generalContractor->name}}" readonly>
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->generalContractor->id}}">
                                                                @else
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{Input::old('general_contractor')}}">
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{Input::old('general_contractor_id')}}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>Transmittal Info</h4>
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class=" control-label">{{trans('labels.submittals.subject')}}</label>
                                                            <input type="text" class="form-control" name="subject" id="transmittal-subject" value="{{ old('subject') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class=" control-label">{{trans('labels.submittals.sent_via')}}</label>
                                                            <select class="custom-options" name="sent_via_dropdown" id="sent_via_dropdown" data-id="sent">
                                                                @foreach($sentVia as $item)
                                                                    <option <?php echo (!empty(old('sent_via')) && old('sent_via') == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                @endforeach
                                                                <option {{ (!empty(old('sent_via')) && !in_array(old('sent_via'), $sentVia))?'selected':'' }} value="0">Custom</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="custom-text-sent form-control {{ (!empty(old('sent_via')) && !in_array(old('sent_via'), $sentVia))?'':'hide' }}" name="sent_via" id="sent_via" value="{{ (!in_array(old('sent_via'), $sentVia))?old('sent_via'):'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class=" control-label">{{trans('labels.rfis.is_change')}}</label>
                                                            <select class="custom-options" name="is_change" id="is_change">
                                                                @foreach($rfiChange as $item)
                                                                    <option <?php echo (!empty(old('is_change')) && old('is_change') == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control control-label">{{trans('labels.rfis.internal_note')}}</label>
                                                            <textarea class="form-control span12 textEditorSmall" rows="5" name="note">{!! old('note') !!}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cm-spacer-xs"></div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <input type="hidden" id="type" name="type" value="{{Config::get('constants.rfis')}}">
                                                        <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                            {{trans('labels.save')}}
                                                        </button>
                                                        <div class="pull-right">
                                                            <div class="please_wait text-right"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                                <div class="cm-spacer-xs"></div>
                                                <div class="cm-spacer-xs"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
@endsection