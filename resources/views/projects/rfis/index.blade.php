@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.rfis.project_rfis')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                    class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a>
                </li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/rfis')}}"
                class="cm-trail-link">{{trans('labels.rfis.project_rfis')}}</a></li>
            </ul>
        </header>
    </div>
    <div class="col-md-7">
        <div class="cm-btn-group cm-pull-right cf">
            <div class="cm-btn-group cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'rfis')))
                <a href="{{URL('projects/'.$project->id.'/rfis/create')}}"
                class="btn btn-success pull-right">{{trans('labels.rfis.create')}}</a>
                @endif
                @if(count($rfis) > 0)
                <a href="{{URL('projects/'.$project->id.'/rfis/transmittals')}}"
                class="btn btn-primary pull-right cm-heading-btn">{{trans('labels.transmittals_log')}}</a>
                @endif

                <div class="pull-left">

                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'rfis')))
                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                        <input type="hidden" value="{{URL('projects/'.$project->id.'/rfis').'/'}}" id="form-url" />
                        <button disabled id="delete-button" class='btn btn-danger pull-right mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                            {{trans('labels.delete')}}
                        </button>
                    {!! Form::close()!!}
                @endif
                </div><!-- /.pull-left -->
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('projects.partials.tabs', array('activeTab' => 'rfis'))
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel">
            <div class="panel-body">
                @if(count($rfis) > 0)
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-10">
                        @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif
                        <div class="cm-filter cm-filter__alt cf">
                            <div class="row">
                                <div class="col-sm-12">
                                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/rfis/filter']) !!}
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div id="chart_div_rfis_details" style="width: 100%; min-height: 100%;margin-top: -15px;"></div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                                {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                     'supplier_subcontractor' => trans('labels.supplier_subcontractor'),
                                                                                     'name' => trans('labels.rfis.name'),
                                                                                     ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                            </div>
                                            <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                                                {!! Form::label('search', trans('labels.search').':') !!}
                                                {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                                {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                                <input type="hidden" id="project_id" value="{{$project->id}}">
                                            </div>
                                            <div class="form-group search-field {{(Input::get('drop_search_by') == 'supplier_subcontractor')?'show':'hide'}}" id="supplier_subcontractor">
                                                {!! Form::label('search-name', trans('labels.search').':') !!}
                                                {!! Form::text('supplier_subcontractor',Input::get('supplier_subcontractor'),['class' => 'cm-control-required form-control address-book-auto search-field-input']) !!}
                                                {!! Form::hidden('sub_id', Input::get('sub_id'), ['class' => 'address-book-id']) !!}
                                            </div>
                                            <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                                {!! Form::label('search-name', trans('labels.search').':') !!}
                                                {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('status', trans('labels.status')) !!}
                                                {!! Form::select('status',$statuses,Input::get('status')) !!}
                                                {!! Form::hidden('sort',Input::get('sort','number')) !!}
                                                {!! Form::hidden('order',Input::get('order','asc')) !!}
                                            </div>
                                            <div class="form-group">
                                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">

                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-2">
                        <div class="col-md-10 mt10">
                            {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/rfis/report','target'=>'_blank', 'class'=>'pull-right']) !!}
                            {!! Form::hidden('sort',Input::get('sort','number')) !!}
                            {!! Form::hidden('order',Input::get('order','asc')) !!}
                            {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                            {!! Form::hidden('report_sub_id',Input::get('sub_id')) !!}
                            {!! Form::hidden('report_sub_name',Input::get('supplier_subcontractor')) !!}
                            {!! Form::hidden('report_status',$status) !!}
                            {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-info pull-right']) !!}
                            <br>
                            <label class="pull-right">
                                <input type="checkbox" name="print_with_notes"> Print with Notes
                            </label>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                <thead>
                                    <tr>
                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'rfis')))
                                        <th>
                                            <input type="checkbox" name="select_all" id="rfis_select_all">
                                        </th>
                                        @endif
                                        <th>
                                            <?php
                                            if (Input::get('sort') == 'number' && Input::get('order') == 'desc') {
                                                $url = Request::url().'?sort=number&order=asc';
                                            } else {
                                                $url = Request::url().'?sort=number&order=desc';
                                            }
                                            $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                            $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                            $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                                            $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                            $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                            $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                            ?>
                                            {{trans('labels.rfis.version_no')}}
                                            <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.cycle_no')}}
                                        </th>
                                        <th>
                                            <?php
                                            if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                                                $url = Request::url().'?sort=mf_number&order=desc';
                                            } else {
                                                $url = Request::url().'?sort=mf_number&order=asc';
                                            }
                                            $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                            $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                            $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                                            $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                            $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                            $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                            ?>
                                            {{trans('labels.master_format_number_and_title')}}
                                            <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                        </th>
                                        <th>
                                            <?php
                                            if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                                                $url = Request::url().'?sort=name&order=desc';
                                            } else {
                                                $url = Request::url().'?sort=name&order=asc';
                                            }
                                            $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                            $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                            $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                                            $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                            $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                            $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                            ?>
                                            {{trans('labels.rfis.name')}}
                                            <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                        </th>
                                        <th>
                                            <?php
                                            if (Input::get('sort') == 'subcontractor_name' && Input::get('order') == 'asc') {
                                                $url = Request::url().'?sort=subcontractor_name&order=desc';
                                            } else {
                                                $url = Request::url().'?sort=subcontractor_name&order=asc';
                                            }
                                            $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                            $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                            $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                                            $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                            $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                            $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                            ?>
                                            {{trans('labels.supplier_subcontractor')}}
                                            <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.received_from_subcontractor')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.sent_for_approval')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.received_from_approval')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.sent_to_subcontractor')}}
                                        </th>
                                        <th>
                                            {{trans('labels.status')}}
                                        </th>
                                        <th>
                                            {{trans('labels.files.download')}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rfis as $item)
                                    <tr>
                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'rfis')))
                                        <td>
                                            <input type="checkbox" name="rfi_id[]" value="{{$item->id}}" data-id="{{$item->id}}" class="rfi multiple-items-checkbox">
                                        </td>
                                        @endif
                                        <td>{{$item->number}}</td>
                                        <td>{{$item->version_cycle_no}}</td>
                                        <td>
                                            @if (isset($item->mf_number) && isset($item->mf_title))
                                            {{$item->mf_number.' - '.$item->mf_title}}
                                            @endif
                                        </td>
                                        <td>
                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'rfis')))
                                            <a href="{{URL('projects/'.$project->id.'/rfis/'.$item->id.'/edit')}}"
                                            class="">{{$item->name}}</a>
                                            @endif
                                        </td>
                                        <td>
                                            {{($item->self_performed ? Auth::user()->company->name : $item->subcontractor_name)}}
                                        </td>
                                        <td>
                                            @if($item->version_rec_sub != 0)
                                            {{date("m/d/Y", strtotime($item->version_rec_sub))}}
                                            @if($item->version_sent_appr != 0)
                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_sent_appr))) - strtotime(date("m/d/Y", strtotime($item->version_rec_sub))); ?>
                                            @else
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_sub))); ?>
                                            @endif
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->version_sent_appr != 0)
                                            {{date("m/d/Y", strtotime($item->version_sent_appr))}}
                                            @if($item->version_rec_appr != 0)
                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_rec_appr))) - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                            @else
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                            @endif
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->version_rec_appr != 0)
                                            {{date("m/d/Y", strtotime($item->version_rec_appr))}}
                                            @if($item->version_subm_sent_sub != 0)
                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                            @else
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                            @endif
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->version_subm_sent_sub != 0)
                                            {{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}
                                            @if(!in_array($item->version_status_short_name, ['Closed']))
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub))); ?>
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                            @endif
                                        </td>
                                        <td>{{$item->version_status_short_name}}</td>
                                        <td>
                                            @if(isset($item->rfis_versions) && count($item->rfis_versions) > 0)
                                                <?php (count($item->rfis_versions)) > 0 ? $lastRevision = count($item->rfis_versions) - 1 : $lastRevision = 0 ?>
                                                    @if(count($item->rfis_versions[$lastRevision]->transmittalSubmSentFile) > 0 || count($item->rfis_versions[$lastRevision]->transmittalSentFile) > 0)
                                                        @if(count($item->rfis_versions[$lastRevision]->transmittalSubmSentFile) > 0 &&
                                                           !empty($item->rfis_versions[$lastRevision]->subm_sent_sub) &&
                                                           $item->rfis_versions[$lastRevision]->subm_sent_sub != '0000-00-00')
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$item->rfis_versions[$lastRevision]->transmittalSubmSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$item->rfis_versions[$lastRevision]->transmittalSubmSentFile[0]->file_name}}">
                                                            </p>
                                                        @elseif(count($item->rfis_versions[$lastRevision]->transmittalSentFile) > 0 &&
                                                            !empty($item->rfis_versions[$lastRevision]->sent_appr) &&
                                                            $item->rfis_versions[$lastRevision]->sent_appr != '0000-00-00' &&
                                                           (empty($item->rfis_versions[$lastRevision]->subm_sent_sub) ||
                                                           $item->rfis_versions[$lastRevision]->subm_sent_sub == '0000-00-00') &&
                                                           (empty($item->rfis_versions[$lastRevision]->rec_appr) ||
                                                           $item->rfis_versions[$lastRevision]->rec_appr == '0000-00-00'))
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$item->rfis_versions[$lastRevision]->transmittalSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/transmittals/'.$item->rfis_versions[$lastRevision]->transmittalSentFile[0]->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endif
                                                @if($item->rfis_versions[$lastRevision]->download_file == true)
                                                    @if(!is_null($item->rfis_versions[$lastRevision]->file) && count($item->rfis_versions[$lastRevision]->file))
                                                        @foreach($item->rfis_versions[$lastRevision]->file as $file)
                                                            @if($file->version_date_connection == $item->rfis_versions[$lastRevision]->file_status)
                                                            {{--@if(!is_null($item->file_name))--}}
                                                                <p class="transmittal-paragraph">
                                                                    <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                    {{--<input type="hidden" class="s3FilePath" id="{{$item->file_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$item->file_name}}">--}}
                                                                    <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                                                </p>
                                                            {{--@endif--}}
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @endif
                                        </td>


                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pull-right">
                            <?php echo $rfis->appends([
                            'sort' => Input::get('sort'),
                            'order' => Input::get('order'),
                            'master_format_search' => Input::get('master_format_search'),
                            'master_format_id' => Input::get('master_format_id'),
                            'supplier_subcontractor' => Input::get('supplier_subcontractor'),
                            'sub_id' => Input::get('sub_id'),
                            'status' => Input::get('status'),
                            ])->render(); ?>
                        </div>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">{{trans('labels.no_records')}}</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{URL::asset('js/loader.js')}}"></script>
<script type="text/javascript">
    var dataDbRFIs = [['Type', 'RFI\'s']];
    @foreach($rfisStatistics as $key=>$item)
        dataDbRFIs.push(['{{str_replace('&','and',$key)}}', {{$item}}]);
    @endforeach
</script>
<script src="{{URL::asset('js/google-charts.js')}}"></script>
@include('popups.delete_record_popup')
@include('popups.alert_popup')
@endsection