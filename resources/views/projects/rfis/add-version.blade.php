@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.rfis.add_new_rfi_version')}} <small class="cm-heading-suffix">{{trans('labels.rfi').': '.$rfi->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/rfis')}}" class="cm-trail-link">{{trans('labels.rfis.project_rfis')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/rfis/'.$rfi->id.'/version/create')}}" class="cm-trail-link">{{trans('labels.rfis.add_new_rfi_version')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'rfis'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            <form id="rfis" name="versionForm" role="form" action="{{URL('/projects/'.$project->id.'/rfis/'.$rfi->id.'/version')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <div class="row box-cont-1">
                        <div class="col-md-4">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required">{{trans('labels.rfis.name')}}</label>
                                                            <input type="text" readonly class="form-control date-number" name="name" value="{{$rfi->name}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required">{{trans('labels.rfis.version_number')}}</label>
                                                            <input type="text" readonly class="form-control date-number" name="number" value="{{$rfi->number}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6 pl0">
                                                            <div class="form-group">
                                                                <label class="cm-control-required ">{{trans('labels.rfis.cycle_no')}}</label>
                                                                <input type="text" readonly class="form-control date-number" id="cycle_no" name="cycle_no" value="{{ !empty(old('cycle_no'))?old('cycle_no'):($rfi->cycle_counter+1) }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 pl0">
                                                            <div class="form-group">
                                                                <input type="hidden" id="useCustom" name="useCustom" value="false" >
                                                                <input type="hidden" id="currentCycleNumber" name="currentCycleNumber" value="{{($rfi->cycle_counter+1)}}" >
                                                                <a href="#" id="customNumber" class="btn btn-primary mt25 cm-heading-btn">Custom</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="cm-control-required control-label">{{trans('labels.status')}}</label>
                                                        <div class="form-group">
                                                            <select name="status" class="date-number">
                                                                @foreach($statuses as $key => $value)
                                                                    <option <?php echo (!empty($lastVersionStatus) && $lastVersionStatus == $key) ? 'selected' : ''; ?> value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">&nbsp;</label>
                                                        <input type="hidden" id="type" name="type" value="{{Config::get('constants.rfis')}}">
                                                        <button type="submit" id="submittal-file-submit" class="btn btn-success pull-left">
                                                            {{trans('labels.update')}}
                                                        </button>
                                                        <div class="please_wait text-right"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>RFI files</h4>
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <label class="control-label">{{trans('labels.submittals.received_from_subcontractor')}}</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" id="rec-sub" class="form-control date-number" name="rec_sub" value="{{old('rec_sub') }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                <label class="input-group-btn">
                                                                <span class="btn btn-primary">
                                                                    <input type="file" style="display: none;" name="rec_sub_file" id="rec_sub_file" multiple>
                                                                        <span class="glyphicon glyphicon-upload"></span>
                                                                    <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id" value="{{old('rec_sub_file_id') }}">
                                                                </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="rec_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <label class="control-label">{{trans('labels.submittals.sent_for_approval')}}</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                                <input type="text" id="sent-appr" class="form-control date-number" name="sent_appr" value="{{old('sent_appr')}}">
                                                        </div>
                                                        <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                    <span class="btn btn-primary">
                                                                        <input type="file" style="display: none;" name="sent_appr_file" id="sent_appr_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                        <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id" value="{{old('sent_appr_file_id') }}">
                                                                    </span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="sent_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <label class="control-label">{{trans('labels.submittals.received_from_approval')}}</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                                <input type="text" id="rec-appr" class="form-control date-number" name="rec_appr" value="{{old('rec_appr') }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                    <span class="btn btn-primary">
                                                                    <input type="file" style="display: none;" name="rec_appr_file" id="rec_appr_file" multiple>
                                                                        <span class="glyphicon glyphicon-upload"></span>
                                                                        <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id" value="{{old('rec_appr_file_id') }}">
                                                                    </span>
                                                                    </label>
                                                                </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div id="rec_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <label class="control-label">{{trans('labels.submittals.sent_to_subcontractor')}}</label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" id="subm-sent-sub" class="form-control date-number" name="subm_sent_sub" value="{{old('subm_sent_sub') }}">
                                                        </div>
                                                        <div class="col-md-4">
                                                            <!-- // upload input -->
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                <label class="input-group-btn">
                                                                <span class="btn btn-primary">
                                                                    <input type="file" style="display: none;" name="subm_sent_sub_file" id="subm_sent_sub_file" multiple>
                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                    <input type="hidden" name="subm_sent_sub_file_id" id="subm_sent_sub_file_id" value="{{old('subm_sent_sub_file_id') }}">
                                                                </span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div id="subm_sent_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
@endsection
