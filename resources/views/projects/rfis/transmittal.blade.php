<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cloud PM</title>
</head>
<body>
    <table width="100%">
        <tr>
            <td style="width:100%; border-bottom: 3px solid black;">
                <table width="100%">
                    <tr>
                        <td style="width: 260px; vertical-align: bottom;">
                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                            @if(count(Auth::user()->company->addresses))
                                {{Auth::user()->company->addresses[0]->street}}<br>
                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                            @endif
                        </td>
                        <td style="width: 250px; vertical-align: bottom;">
                            {{Auth::user()->email}}<br>
                            {{Auth::user()->office_phone}}
                        </td>
                        <td style="float: right; width: 180px; vertical-align: bottom; margin: 0px;">
                            <h2 style="float: left; width: 180px; margin: 0px; text-align: right;">{{'RFI'}}</h2>
                            <p style="float: left; width: 180px; margin: 0px; text-align: right;">{{'No: '.$version->rfi->number}}</p>
                            <p style="float: left; width: 180px; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 3px solid black; width: 100%;">
                <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'Project: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            {{$version->rfi->project->name}}
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'Project Number: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{$version->rfi->project->number}}
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'To: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            @if($dateType == Config::get('constants.transmittal_date_type.sent_appr'))
                                @if(!is_null($version->rfi->recipient))
                                    @if(!is_null($version->rfi->recipient_contact))
                                        {{$version->rfi->recipient_contact->name}}<br>
                                        {{$version->rfi->recipient_contact->title}}<br>
                                    @endif
                                    {{$version->rfi->recipient->name}}<br>
                                    @if(!is_null($version->rfi->recipient_contact))
                                        @if(!is_null($version->rfi->recipient_contact->office))
                                            {{$version->rfi->recipient_contact->office->street}}<br>
                                            {{$version->rfi->recipient_contact->office->city.', '.$version->rfi->recipient_contact->office->state.' '.$version->rfi->recipient_contact->office->zip}}<br>
                                        @endif
                                        {{$version->rfi->recipient_contact->office_phone}}<br>
                                        {{$version->rfi->recipient_contact->email}}<br>
                                    @endif
                                @else
                                    {{'Unknown'}}
                                @endif
                            @else
                                @if(!is_null($version->rfi->subcontractor))
                                    @if(!is_null($version->rfi->subcontractor_contact))
                                        {{$version->rfi->subcontractor_contact->name}}<br>
                                        {{$version->rfi->subcontractor_contact->title}}<br>
                                    @endif
                                    {{$version->rfi->subcontractor->name}}<br>
                                    @if(!is_null($version->rfi->subcontractor_contact))
                                        @if(!is_null($version->rfi->subcontractor_contact->office))
                                            {{$version->rfi->subcontractor_contact->office->street}}<br>
                                            {{$version->rfi->subcontractor_contact->office->city.', '.$version->rfi->subcontractor_contact->office->state.' '.$version->rfi->subcontractor_contact->office->zip}}<br>
                                        @endif
                                        {{$version->rfi->subcontractor_contact->office_phone}}<br>
                                        {{$version->rfi->subcontractor_contact->email}}<br>
                                    @endif
                                @else
                                    {{'Unknown'}}
                                @endif
                            @endif
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'From: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{Auth::user()->name}}<br>
                            {{Auth::user()->title}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 345px; vertical-align: top;">
                            <p>
                                <b>{{'Subject: '}}</b>
                                {{$version->rfi->subject}}
                            </p>
                            <p>
                                <b>{{'The RFI is change: '}}</b>
                                {{$version->rfi->is_change}}
                            </p>
                        </td>
                        <td style="width: 345px; vertical-align: top;">
                            <p style="text-align: right;">
                                <b>{{'Sent Via: '}}</b>
                                {{$version->rfi->sent_via}}
                            </p>
                            <p style="text-align: right;">
                                <b>{{'Status: '}}</b>
                                @if(!is_null($version->status))
                                    {{$version->status->name}}
                                @endif
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 180px;"><b>{{'RFI No: '}}</b></td>
                        <td>{{$version->rfi->number}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Cycle No: '}}</b></td>
                        <td>{{$version->cycle_no}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'MF Number & Title: '}}</b></td>
                        <td>{{$version->rfi->mf_number.' - '.$version->rfi->mf_title}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Subcontractor/Supplier: '}}</b></td>
                        <td>
                            @if ($version->rfi->self_performed)
                                {{Auth::user()->company->name}}
                            @else
                                @if(!is_null($version->rfi->subcontractor))
                                    {{$version->rfi->subcontractor->name}}
                                @else
                                    {{'Unknown'}}
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'General Contractor: '}}</b></td>
                        <td>
                            @if($version->rfi->project->make_me_gc == 1 && $version->rfi->project->company)
                                {{$version->rfi->project->company->name}}
                            @elseif($version->rfi->make_me_gc === 0 && $version->rfi->project->make_me_gc === 0)
                                @if(!is_null($version->rfi->general_contractor))
                                    {{$version->rfi->general_contractor->name}}
                                @else
                                    {{'Unknown'}}
                                @endif
                            @else
                                {{Auth::user()->company->name}}
                            @endif
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table>
                    <tr>
                        <td style="width: 180px;vertical-align: top;"><br/><b>{{'Question: '}}</b></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="margin-left:5px;">
        {!! $version->question !!}
    </div>
    @if (!empty($dateType) && $dateType != 'sent_appr')
        <div style="width: 100%; border-top: 1px solid black;height: 5px;margin-top:5px;"></div>
        <b style="margin-left:5px;display:inline-block;">{{'Answer: '}}</b>
        <div style="margin-left:5px;">
            {!! $version->answer !!}
        </div>
    @endif
</body>
</html>