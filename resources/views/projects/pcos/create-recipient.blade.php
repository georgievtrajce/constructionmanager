@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.pcos.add_recipient')}} <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/edit')}}" class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/recipient/create')}}" class="cm-trail-link">{{trans('labels.pcos.add_recipient')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
                <form id="rfis" class="form" role="form" action="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/recipient')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="row box-cont-1">
                        <div class="col-md-12">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="cm-control-required">{{trans('labels.pcos.name')}}</label>
                                                    <input type="text" class="form-control" name="name" readonly value="{{ $pco->name }}">
                                                </div>
                                                <div class="col-md-7 pl0">
                                                    <?php $recipient_notification = false; $recipientId = null; ?>
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {{trans('labels.submittals.import_recipient_from_companies')}}
                                                        </label>
                                                        <?php
                                                        $recipientValue = old('recipient');
                                                        $recipientIdValue = old('recipient_id');
                                                        if (isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0) {
                                                            $recipientValue = $project->architect->name;
                                                            $recipientIdValue = $project->architect->id;
                                                        } else if(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0) {
                                                            $recipientValue = $project->primeSubcontractor->name;
                                                            $recipientIdValue = $project->primeSubcontractor->id;
                                                        } else if(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0) {
                                                            $recipientValue = $project->owner->name;
                                                            $recipientIdValue = $project->owner->id;
                                                        } else if(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0) {
                                                            $recipientValue = $project->generalContractor->name;
                                                            $recipientIdValue = $project->generalContractor->id;
                                                        }
                                                        ?>
                                                        <input type="text" class="form-control address-book-recipient-auto" name="recipient" value="{{$recipientValue}}">
                                                        <input type="hidden" class="address-book-recipient-id" name="recipient_id" value="{{$recipientIdValue}}">
                                                    </div>
                                                    <hr>
                                                </div>
                                                <div class="col-md-5 pl0">
                                                    @if(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($architectProjectTransmittals) > 0)
                                                                            @foreach($architectProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->architect_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->architect_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @elseif(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($primeSubcontractorProjectTransmittals) > 0)
                                                                            @foreach($primeSubcontractorProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->prime_subcontractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->prime_subcontractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @elseif(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($ownerProjectTransmittals) > 0)
                                                                            @foreach($ownerProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->owner_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->owner_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @elseif(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($contractorProjectTransmittals) > 0)
                                                                            @foreach($contractorProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->contractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->contractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;"></div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label >{{trans('labels.pcos.internal_notes')}}</label>
                                                        <textarea class="form-control span12 textEditorSmall" rows="5" name="note">{!! old('note') !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                        {{trans('labels.save')}}
                                                    </button>
                                                </div>
                                            </div>
                                            <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
@endsection