@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.pcos.edit_subcontractor')}}
                <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pcoSubcontractor->pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco->id.'/edit')}}" class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco->id.'/subcontractors/'.$pcoSubcontractor->id.'/edit')}}"
                                                        class="cm-trail-link">{{trans('labels.pcos.edit_subcontractor')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>                            {{ Session::get('flash_notification.message') }}
                </div>
            @endif
                <div class="row box-cont-1">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    <h4>Change Info</h4>
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco->id.'/subcontractors/'.$pcoSubcontractor->id),
                                        'id' => 'pco_subcontractor', 'role' => 'form']) !!}
                                            <div class="col-md-6">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required">{{trans('labels.pcos.change_name')}}</label>
                                                            <input type="text" class="form-control" name="name" value="{{$pcoSubcontractor->pco_name}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label>{{trans('labels.pcos.master_format_search')}}</label>
                                                            <input type="text" class="cm-control-required form-control mf-number-title-auto mb0" name="master_format_search" value="{{ old('master_format_search') }}">
                                                            <input type="hidden" class="master-format-id mb0" name="master_format_id" value="{{old('master_format_id')}}">
                                                            <input type="hidden" id="project_id" value="{{$project->id}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <div class="mf-cont">
                                                                <label class="control-label">&nbsp;</label>
                                                                <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">
                                                                    {{trans('labels.submittals.enter_custom_number_and_title')}}
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label class="cm-control-required">{{trans('labels.master_format_number_and_title')}}</label>
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{$pcoSubcontractor->mf_number}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{$pcoSubcontractor->mf_title}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                    $subcontractorId = null;
                                                ?>
                                                <div class="row">
                                                    <div class="col-md-7">
                                                        <div class="form-group" id="subcontractor_container" @if($pcoSubcontractor->self_performed) {{'style=display:none;'}} @endif>
                                                            <label class="cm-control-required">
                                                                {{trans('labels.submittals.import_supplier_from_companies')}}
                                                            </label>
                                                            <input type="text" class="form-control address-book-auto" id="supplier_subcontractor" name="supplier_subcontractor" value="@if(!is_null($pcoSubcontractor->ab_subcontractor)){{$pcoSubcontractor->ab_subcontractor->name}}@endif">
                                                            <input type="hidden" class="address-book-id" id="sub_id" name="sub_id" value="@if(!is_null($pcoSubcontractor->ab_subcontractor)){{$pcoSubcontractor->ab_subcontractor_id}}@endif">
                                                        </div>
                                                        <input type="hidden" id="notif_subcontractor_id" name="notif_subcontractor_id" value="{{$subcontractorId}}">
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="subcontractor_contacts_container" @if($pcoSubcontractor->self_performed) {{'style=display:none;'}} @endif>
                                                            <?php $subcontractor_notification = false; $subcontractorId = null; ?>
                                                            <label>Select Contact:</label>
                                                            <select name="sub_contact" id="sub_contact">
                                                                <option value=""></option>
                                                                @if(!is_null($pcoSubcontractor->ab_subcontractor) && count($projectSubcontractorContacts))
                                                                    @foreach($projectSubcontractorContacts as $contact)
                                                                        <?php $subcontractor_notification = true; if($pcoSubcontractor->sub_contact_id == $contact->id) $subcontractorId = $contact->id; ?>
                                                                        <option value="{{$contact->id}}" @if($pcoSubcontractor->sub_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                    @endforeach
                                                                @endif
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 mt10">
                                                    <div class="form-group">
                                                        <input type="checkbox" name="self_performed" id="self_performed"> {{trans('labels.submittals.self_performed')}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="cm-control">{{trans('labels.pcos.internal_notes')}}</label>
                                                    <textarea class="form-control span12 textEditorSmallSubcontractors" rows="5" name="note">{!! $pcoSubcontractor->note !!}</textarea>
                                                </div>
                                                <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                    {{trans('labels.update')}}
                                                </button>
                                            </div>
                                            <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="pull-left">
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                || (Permissions::can('write', 'pcos')))
                                                    <a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco_id.'/subcontractors/'.$pcoSubcontractor->id.'/version/create')}}"
                                                       class="btn btn-primary">{{trans('labels.submittals.add_new_version')}}</a>                                @endif
                                            </div>
                                            <div class="pull-right">
                                                @if(count($pcoSubcontractor->subcontractor_versions) > 1) {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent',
                                'url'=>URL(''), 'id' => 'delete-form']) !!}
                                                <input type="hidden" value="{{URL('projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco_id.'/subcontractors/'.$pcoSubcontractor->id.'/version').'/'}}"
                                                       id="form-url" />
                                                <button data-limit="1" disabled id="delete-button" class='btn btn-xs btn-danger pull-right' type='submit' data-toggle="modal"
                                                        data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                                    {{trans('labels.delete')}}
                                                </button> {!! Form::close()!!} @endif
                                            </div>
                                            @if($pcoSubcontractor->subcontractor_versions->count() != 0)
                                                <div class="table-responsive">
                                                    <table class="table table-hover table-bordered cm-table-compact">
                                                        <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>
                                                                {{trans('labels.pcos.change_name')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.submittals.cycle_no')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.submittals.received_from_subcontractor')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.submittals.sent_to_subcontractor')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.pcos.cost')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.status')}}
                                                            </th>
                                                            <th>
                                                                {{trans('labels.files.download')}}
                                                            </th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php $subcontractorVersions = $pcoSubcontractor->subcontractor_versions; ?> @for ($i=0; $i
                                        < count($subcontractorVersions); $i++) <tr>
                                                            <td class="text-center">
                                                                <input type="checkbox" class="multiple-items-checkbox" data-id="{{$subcontractorVersions[$i]->id}}">
                                                            </td>
                                                            {{--
                                                            <td>@if(!is_null($pcoSubcontractor->pco->number)){{$pcoSubcontractor->pco->number}}@endif</td>--}}
                                                            <td>
                                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                                || (Permissions::can('read', 'pcos')))
                                                                    <a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoSubcontractor->pco_id.'/subcontractors/'.$pcoSubcontractor->id.'/version/'.$subcontractorVersions[$i]->id.'/edit')}}"
                                                                       class=""> {{$pcoSubcontractor->pco_name}}</a>                                                @endif
                                                            </td>
                                                            <td>{{$subcontractorVersions[$i]->cycle_no}}</td>
                                                            <td>
                                                                @if($subcontractorVersions[$i]->rec_sub != 0) {{date("m/d/Y", strtotime($subcontractorVersions[$i]->rec_sub))}}
                                                                @if($subcontractorVersions[$i]->subm_sent_sub != 0)
                                                                    <?php $dateDiff = strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i]->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i]->rec_sub))); ?>
                                                                @else
                                                                    <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i]->rec_sub))); ?>
                                                                @endif
                                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                @endif
                                                            </td>
                                                            <td>
                                                                @if(!is_null($subcontractorVersions[$i]->subm_sent_sub) && $subcontractorVersions[$i]->subm_sent_sub != 0)
                                                                    {{date("m/d/Y", strtotime($subcontractorVersions[$i]->subm_sent_sub))}}
                                                                    @if(!empty($subcontractorVersions[$i+1]) && $subcontractorVersions[$i+1]->rec_sub != 0)
                                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i+1]->rec_sub))) - strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i]->subm_sent_sub))); ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                    @else
                                                                        @if(!in_array($subcontractorVersions[$i]->status->short_name, ['APP', 'AAN']))
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($subcontractorVersions[$i]->subm_sent_sub))); ?>
                                                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                        @endif
                                                                    @endif
                                                                @endif
                                                            </td>
                                                            <td>
                                                                {{'$'.number_format($subcontractorVersions[$i]->cost, 2)}}
                                                            </td>
                                                            <td>{{$subcontractorVersions[$i]->status->short_name}}</td>
                                                            <td>
                                                                @if($subcontractorVersions[$i]->transmittal_file)
                                                                    @if(!is_null($subcontractorVersions[$i]->transmittalSubmSentFile) && count($subcontractorVersions[$i]->transmittalSubmSentFile) > 0)
                                                                        <p class="transmittal-paragraph">
                                                                            <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                            <input type="hidden" class="s3FilePath" id="{{$subcontractorVersions[$i]->transmittalSubmSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$subcontractorVersions[$i]->transmittalSubmSentFile[0]->file_name}}">
                                                                        </p>
                                                                    @endif
                                                                @endif
                                                                @if($subcontractorVersions[$i]->download_file)
                                                                    @if(!is_null($subcontractorVersions[$i]->file) && count($subcontractorVersions[$i]->file) > 0)
                                                                        @foreach($subcontractorVersions[$i]->file as $file)
                                                                            @if($file->version_date_connection == $subcontractorVersions[$i]->file_status)
                                                                                <p class="transmittal-paragraph">
                                                                                    <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                    <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                                </p>
                                                                            @endif
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            </td>
                                                        </tr>
                                                        @endfor
                                                        </tbody>
                                                    </table>

                                                </div>
                                            @else
                                                <hr>
                                                <p class="text-center">{{trans('labels.no_records')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    @include('popups.delete_record_popup')
@endsection