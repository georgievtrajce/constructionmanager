@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.pcos.edit_recipient')}}
                <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pcoRecipient->pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                                                 class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}"
                                                 class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoRecipient->pco->id.'/edit')}}"
                                                 class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item active"><a
                                href="{{URL('projects/'.$project->id.'/pcos/'.$pcoRecipient->pco->id.'/recipient/'.$pcoRecipient->id.'/edit')}}"
                                class="cm-trail-link">{{trans('labels.pcos.edit_recipient')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            <div class="row box-cont-1">
                <div class="col-md-12">
                    <div class="box">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/pcos/'.$pcoRecipient->pco->id.'/recipient/'.$pcoRecipient->id), 'class' => 'form', 'id' => 'pco_recipient', 'role' => 'form']) !!}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{trans('labels.pcos.name')}}</label>
                                                <input type="text" class="form-control" name="name" readonly
                                                       value="{{$pcoRecipient->pco->name}}">
                                            </div>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <?php $recipient_notification = false; $recipientId = null; ?>
                                            <div class="col-md-7 pl0">
                                                <div class="form-group">
                                                    <label>
                                                        {{trans('labels.submittals.import_recipient_from_companies')}}
                                                    </label>
                                                    <?php
                                                    $recipientValue = '';
                                                    $recipientIdValue = '';
                                                    if (!is_null($pcoRecipient->ab_recipient) && $pcoRecipient->ab_recipient_id != 0 && count($projectRecipientContacts) > 0) {
                                                        $recipientValue = $pcoRecipient->ab_recipient->name;
                                                        $recipientIdValue = $pcoRecipient->ab_recipient_id;
                                                    } else if(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0) {
                                                        $recipientValue = $project->architect->name;
                                                        $recipientIdValue = $project->architect->id;
                                                    } else if(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0) {
                                                        $recipientValue = $project->primeSubcontractor->name;
                                                        $recipientIdValue = $project->primeSubcontractor->id;
                                                    } else if(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0) {
                                                        $recipientValue = $project->owner->name;
                                                        $recipientIdValue = $project->owner->id;
                                                    } else if(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0) {
                                                        $recipientValue = $project->generalContractor->name;
                                                        $recipientIdValue = $project->generalContractor->id;
                                                    }
                                                    ?>
                                                    <input type="text" class="form-control address-book-recipient-auto" name="recipient" value="{{$recipientValue}}">
                                                    <input type="hidden" class="address-book-recipient-id" name="recipient_id" value="{{$recipientIdValue}}">
                                                </div>
                                            </div>
                                            <div class="col-md-5 pl0">
                                                @if(!is_null($pcoRecipient->ab_recipient) && $pcoRecipient->ab_recipient_id != 0 && count($projectRecipientContacts) > 0)
                                                    @if(!is_null($pcoRecipient->ab_recipient))
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select Contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($projectRecipientContacts))
                                                                            @foreach($projectRecipientContacts as $contact)
                                                                                <?php $recipient_notification = true; if($pcoRecipient->recipient_contact_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($pcoRecipient->recipient_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @elseif(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                    @if(isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select Contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($architectProjectTransmittals))
                                                                            @foreach($architectProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->architect_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->architect_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @elseif(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                    @if(isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select Contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($primeSubcontractorProjectTransmittals))
                                                                            @foreach($primeSubcontractorProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->prime_subcontractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->prime_subcontractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @elseif(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                    @if(isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select Contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($ownerProjectTransmittals))
                                                                            @foreach($ownerProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->owner_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->owner_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @elseif(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                    @if(isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                        <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <label>Select Contact:</label>
                                                                    <select name="recipient_contact" id="recipient_contact">
                                                                        <option value=""></option>
                                                                        @if(count($contractorProjectTransmittals))
                                                                            @foreach($contractorProjectTransmittals as $contact)
                                                                                <?php $recipient_notification = true; if($project->contractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                <option value="{{$contact->id}}" @if($project->contractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                            @endforeach
                                                                        @else
                                                                            <option value="">No available contacts for this company</option>
                                                                        @endif
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @else
                                                    <div class="recipient_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;"></div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="cm-control">{{trans('labels.pcos.internal_notes')}}</label>
                                                <textarea class="form-control span12 textEditorSmall" rows="5"
                                                          name="note">{!! $pcoRecipient->note !!}</textarea>
                                            </div>
                                            <button type="submit" class="btn btn-success pull-right">
                                                {{trans('labels.update')}}
                                            </button>
                                        </div>
                                        <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                        <a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoRecipient->pco_id.'/recipient/'.$pcoRecipient->id.'/version/create')}}"
                                           class="btn btn-primary">{{trans('labels.submittals.add_new_version')}}</a>
                                    @endif
                                    </div>
                                    <div class="col-md-6">
                                    @if($pcoRecipient->recipient_versions->count() != 0)
                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'pcos')))
                                            @if(count($pcoRecipient->recipient_versions) > 1)
                                                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                                <input type="hidden" value="{{URL('projects/'.$project->id.'/pcos/'.$pcoRecipient->pco_id.'/recipient/'.$pcoRecipient->id.'/version').'/'}}" id="form-url" />
                                                <button data-limit="1" disabled id="delete-button" class='btn btn-danger pull-right' type='button' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                                    {{trans('labels.delete')}}
                                                </button>
                                                {!! Form::close()!!}
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered cm-table-compact">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>{{trans('labels.pcos.version_no')}}</th>
                                                    <th>{{trans('labels.pcos.name')}}</th>
                                                    <th>{{trans('labels.submittals.cycle_no')}}</th>
                                                    <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                                    <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                                    <th>{{trans('labels.pcos.cost')}}</th>
                                                    <th>{{trans('labels.status')}}</th>
                                                    <th>{{trans('labels.files.download')}}</th>

                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php $recipientVersions = $pcoRecipient->recipient_versions; ?>
                                                @for ($i=0; $i < count($recipientVersions); $i++)
                                                    <tr>
                                                        <td class="text-center">
                                                            <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$recipientVersions[$i]->id}}">
                                                        </td>
                                                        <td>@if(!is_null($pcoRecipient->pco->number)){{$pcoRecipient->pco->number}}@endif</td>
                                                        <td>
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'pcos')))
                                                                <a href="{{URL('projects/'.$project->id.'/pcos/'.$pcoRecipient->pco_id.'/recipient/'.$pcoRecipient->id.'/version/'.$recipientVersions[$i]->id.'/edit')}}"
                                                                   class="">{{$pcoRecipient->pco->name}}</a>
                                                            @endif
                                                        </td>
                                                        <td>{{$recipientVersions[$i]->cycle_no}}</td>
                                                        <td>
                                                            @if($recipientVersions[$i]->sent_appr != 0)
                                                                {{date("m/d/Y", strtotime($recipientVersions[$i]->sent_appr))}}
                                                                @if($recipientVersions[$i]->rec_appr != 0)
                                                                    <?php $dateDiff = strtotime(date("m/d/Y", strtotime($recipientVersions[$i]->rec_appr))) - strtotime(date("m/d/Y", strtotime($recipientVersions[$i]->sent_appr))); ?>
                                                                @else
                                                                    <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($recipientVersions[$i]->sent_appr))); ?>
                                                                @endif
                                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(!is_null($recipientVersions[$i]->rec_appr) && $recipientVersions[$i]->rec_appr != 0)
                                                                {{date("m/d/Y", strtotime($recipientVersions[$i]->rec_appr))}}
                                                                @if(!empty($recipientVersions[$i+1]) && $recipientVersions[$i+1]->sent_appr != 0)
                                                                    <?php $dateDiff = strtotime(date("m/d/Y", strtotime($recipientVersions[$i+1]->sent_appr))) - strtotime(date("m/d/Y", strtotime($recipientVersions[$i]->rec_appr))); ?>
                                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                @else
                                                                    @if(!in_array($recipientVersions[$i]->status->short_name, ['APP', 'AAN']))
                                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($recipientVersions[$i]->rec_appr))); ?>
                                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{'$'.number_format($recipientVersions[$i]->cost, 2)}}
                                                        </td>
                                                        <td>{{$recipientVersions[$i]->status->short_name}}</td>
                                                        <td>
                                                            @if($recipientVersions[$i]->transmittal_file)
                                                                @if(!is_null($recipientVersions[$i]->transmittalSentFile) && count($recipientVersions[$i]->transmittalSentFile) > 0)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$recipientVersions[$i]->transmittalSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$recipientVersions[$i]->transmittalSentFile[0]->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endif
                                                            @if($recipientVersions[$i]->download_file)
                                                                @if(!is_null($recipientVersions[$i]->file) && count($recipientVersions[$i]->file) > 0)
                                                                    @foreach($recipientVersions[$i]->file as $file)
                                                                        @if($file->version_date_connection == $recipientVersions[$i]->file_status)
                                                                            <p class="transmittal-paragraph">
                                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                            </p>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </td>


                                                    </tr>
                                                @endfor
                                                </tbody>
                                            </table>
                                        </div>
                                    @else
                                        <p class="text-center">{{trans('labels.no_records')}}</p>
                                    @endif
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('popups.delete_record_popup')
@endsection