@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-7">
            <header class="cm-heading">
                Edit PCO
                <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                                                 class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}"
                                                 class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/edit')}}"
                                                        class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-5">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
                    <a class="btn btn-success btn-sm cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'10'.'&module_item='.$pco->id.'&title='.$pco->name.'&mf_number='.$pco->mf_number.'&mf_title='.$pco->mf_title.'&mf_number_title='.$pco->mf_number.' - '.$pco->mf_title}}">{{trans('labels.tasks.create')}}</a>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            <div style="margin-bottom: 60px;">
                <div class="col-md-12">
                    @include('projects.partials.tabs-inner', array('activeTab' =>  Input::get('activeInnerTab', 'files')))
                </div>
            </div>
            <div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert"
                                aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <div class="tab-content">
                    <div id="info" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'info') in active @endif">
                        <div id="transmittal_info_cont" class="mt20">
                            {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/pcos/'.$pco->id), 'id' => 'pcos', 'role' => 'form']) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <div class="row box-cont-1">
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>{{trans('labels.pco_info')}}</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required">{{trans('labels.pcos.name')}}</label>
                                                                        <input type="text" class="form-control" name="name" value="{{ $pco->name }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required">{{trans('labels.pcos.number')}}</label>
                                                                        <input type="text" readonly class="form-control" id="item-number" name="number" value="{{ $pco->number }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a href="javascript:;" id="custom-number" class="btn btn-primary pull-right">
                                                                            {{trans('labels.pcos.enter_version_number')}}
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="mb5">
                                                                            {{trans('labels.pcos.import_general_contractor')}}
                                                                        </label>
                                                                        <div class="row">
                                                                            <div class="col-md-12 general_contractor_container">
                                                                                @if($project->make_me_gc && isset($project->company->name))
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->company->name}}" readonly>
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->company->id}}">
                                                                                @elseif(!is_null($pco->general_contractor) && $pco->gc_id != 0)
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$pco->general_contractor->name}}" <?php (!empty($project->general_contractor_id))?'readonly':'' ?>>
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$pco->gc_id}}">
                                                                                @elseif(isset($project->generalContractor->name))
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->generalContractor->name}}">
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->generalContractor->id}}">
                                                                                @else
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="">
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="">
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>{{trans('labels.transmittal_info')}}</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.subject')}}</label>
                                                                        <input type="text" class="form-control" name="subject" value="{{ $pco->subject }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.sent_via')}}</label>
                                                                        <select class="custom-options" name="sent_via_dropdown" id="sent_via_dropdown" data-id="sent">
                                                                            @foreach($sentVia as $item)
                                                                                <option <?php echo (!empty($pco->sent_via) && $pco->sent_via == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                            <option {{ (!empty($pco->sent_via) && !in_array($pco->sent_via, $sentVia))?'selected':'' }} value="0">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" class="custom-text-sent form-control {{ (!empty($pco->sent_via) && !in_array($pco->sent_via, $sentVia))?'':'hide' }}" name="sent_via" id="sent_via" value="{{ (!in_array($pco->sent_via, $sentVia))?$pco->sent_via:'' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.pcos.reason_for_change')}}</label>
                                                                        <select class="custom-options" name="reason_for_change_dropdown" id="reason_for_change_dropdown" data-id="reason">
                                                                            @foreach($reasonsForChange as $item)
                                                                                <option <?php echo (!empty($pco->reason_for_change) && $pco->reason_for_change == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                            <option {{ (!empty($pco->reason_for_change) && !in_array($pco->reason_for_change, $reasonsForChange))?'selected':'' }} value="0">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" class="custom-text-reason form-control {{ (!empty($pco->reason_for_change) && !in_array($pco->reason_for_change, $reasonsForChange))?'':'hide' }}" name="reason_for_change" id="reason_for_change" value="{{ (!in_array($pco->reason_for_change, $reasonsForChange))?$pco->reason_for_change:'' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control">{{trans('labels.pcos.internal_notes')}}</label>
                                                                        <textarea class="form-control span12 textEditorSmall" rows="5"
                                                                                  name="description_of_change">{!! $pco->description_of_change !!}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                                        {{trans('labels.update')}}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="cm-spacer-xs"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                    <div id="files" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'files') in active @endif">
                        <div class="row box-cont-2">
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="card">
                                        <div class="card-body">
                                                @if(is_null($pco->recipient))
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <a href="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/recipient/create')}}"
                                                               class="btn btn-sm btn-primary">{{trans('labels.pcos.add_pco_recipient')}}</a>
                                                        </div>
                                                    </div>
                                                @endif
                                                @if(!is_null($pco->recipient))
                                                    <h4 class="mb20 pull-left">{{trans('labels.pcos.recipient')}}</h4>
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-bordered cm-table-compact">
                                                            <tbody>
                                                            <tr>
                                                                <th>{{trans('labels.pcos.recipient')}}</th>
                                                                <th>{{trans('labels.pcos.name')}}</th>
                                                                <th>{{trans('labels.submittals.cycle_no')}}</th>
                                                                <th>
                                                                    {{trans('labels.submittals.received_from_approval')}}
                                                                </th>
                                                                <th>
                                                                    {{trans('labels.submittals.sent_for_approval')}}
                                                                </th>

                                                                <th>
                                                                    {{trans('labels.pcos.cost')}}
                                                                </th>
                                                                <th>
                                                                    {{trans('labels.status')}}
                                                                </th>
                                                                <th>
                                                                    {{trans('labels.files.download')}}
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'pcos')))
                                                                        <a href="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/recipient/'.$pco->recipient->id.'/edit')}}"
                                                                           class="">{{$pco->recipient->ab_recipient->name}}</a>
                                                                    @elseif (isset($pco->recipient))
                                                                        {{$pco->recipient->ab_recipient->name}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    {{$pco->name}}
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && !is_null($pco->recipient->latest_recipient_version))
                                                                        {{$pco->recipient->latest_recipient_version->cycle_no}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && !is_null($pco->recipient->latest_recipient_version) && $pco->recipient->latest_recipient_version->sent_appr != 0)
                                                                        {{date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->sent_appr))}}
                                                                        @if($pco->recipient->latest_recipient_version->rec_appr != 0)
                                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->rec_appr))) - strtotime(date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->sent_appr))); ?>
                                                                        @else
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->sent_appr))); ?>
                                                                        @endif
                                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && !is_null($pco->recipient->latest_recipient_version) && !is_null($pco->recipient->latest_recipient_version->rec_appr) && $pco->recipient->latest_recipient_version->rec_appr != 0)
                                                                        {{date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->rec_appr))}}
                                                                        @if(!in_array($pco->recipient->latest_recipient_version->status->short_name, ['APP', 'AAN']))
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pco->recipient->latest_recipient_version->rec_appr))); ?>
                                                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                        @endif

                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && !is_null($pco->recipient->latest_recipient_version))
                                                                        {{'$'.number_format($pco->recipient->latest_recipient_version->cost, 2)}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && !is_null($pco->recipient->latest_recipient_version))
                                                                        {{$pco->recipient->latest_recipient_version->status->short_name}}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if(isset($pco->recipient) && isset($pco->recipient->recipient_versions) && count($pco->recipient->recipient_versions) > 0)
                                                                        <?php (count($pco->recipient->recipient_versions)) > 0 ? $lastRecipientVersion = count($pco->recipient->recipient_versions) - 1 : $lastRecipientVersion = 0 ?>
                                                                        @if($pco->recipient->recipient_versions[$lastRecipientVersion]->transmittal_file == true)
                                                                            @if(isset($pco->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile) && count($pco->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile) > 0)
                                                                                <p class="transmittal-paragraph">
                                                                                    <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                                    <input type="hidden" class="s3FilePath" id="{{$pco->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile[0]->id}}"
                                                                                           value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$pco->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile[0]->file_name}}">
                                                                                </p>
                                                                            @endif
                                                                        @endif
                                                                        @if($pco->recipient->recipient_versions[$lastRecipientVersion]->download_file == true)
                                                                            @if(isset($pco->recipient->recipient_versions[$lastRecipientVersion]->file) && count($pco->recipient->recipient_versions[$lastRecipientVersion]->file) > 0)
                                                                                @foreach($pco->recipient->recipient_versions[$lastRecipientVersion]->file as $file)
                                                                                    @if($file->version_date_connection == $pco->recipient->recipient_versions[$lastRecipientVersion]->file_status)
                                                                                        <p class="transmittal-paragraph">
                                                                                            <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                            <input type="hidden" class="s3FilePath" id="{{$file->id}}"
                                                                                                   value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                                        </p>
                                                                                    @endif
                                                                                @endforeach
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            <tbody>
                                                        </table>
                                                    </div>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="box">
                                    <div class="card">
                                        <div class="card-body">
                                                <div class="mb20">
                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <a href="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/subcontractors/create')}}"
                                                                   class="btn btn-sm btn-primary">{{trans('labels.pcos.add_pco_subcontractor')}}</a>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    @if(count($pco->subcontractors))
                                                        <h4 class="mb20 pull-left">{{trans('labels.pcos.subcontractors_change_items')}}</h4>
                                                        <div class="pull-right">
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'pcos')))
                                                                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                                                <input type="hidden" value="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/subcontractors').'/'}}" id="form-url" />
                                                                <button disabled id="delete-button" class='btn btn-xs btn-danger pull-right' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                                                    {{trans('labels.delete')}}
                                                                </button>
                                                                {!! Form::close()!!}
                                                            @endif
                                                        </div>

                                                        <div class="table-responsive">
                                                            <table class="table table-hover table-bordered cm-table-compact">
                                                                <tbody>
                                                                <tr>
                                                                    <th></th>
                                                                    <th>{{trans('labels.pcos.subcontractor')}}</th>
                                                                    <th>{{trans('labels.pcos.change_name')}}</th>
                                                                    <th>{{trans('labels.submittals.cycle_no')}}</th>
                                                                    <th>
                                                                        {{trans('labels.mf_number_and_title')}}
                                                                    </th>
                                                                    <th>
                                                                        {{trans('labels.submittals.received_from_subcontractor')}}
                                                                    </th>
                                                                    <th>
                                                                        {{trans('labels.submittals.sent_to_subcontractor')}}
                                                                    </th>

                                                                    <th>
                                                                        {{trans('labels.pcos.cost')}}
                                                                    </th>
                                                                    <th>
                                                                        {{trans('labels.status')}}
                                                                    </th>
                                                                    <th>
                                                                        {{trans('labels.files.download')}}
                                                                    </th>
                                                                </tr>
                                                                @foreach($pco->subcontractors as $subcontractor)
                                                                    <tr>
                                                                        <td class="text-center">
                                                                            <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$subcontractor->id}}">
                                                                        </td>
                                                                        <td>
                                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'pcos')))
                                                                                <a href="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/subcontractors/'.$subcontractor->id.'/edit')}}"
                                                                                   class="">
                                                                                    @if($subcontractor->self_performed)
                                                                                        {{Auth::user()->company->name}}
                                                                                    @else
                                                                                        {{!is_null($subcontractor->ab_subcontractor) ? $subcontractor->ab_subcontractor->name : ''}}
                                                                                    @endif
                                                                                </a>
                                                                            @else
                                                                                @if($subcontractor->self_performed)
                                                                                    {{Auth::user()->company->name}}
                                                                                @else
                                                                                    {{!is_null($subcontractor->ab_subcontractor) ? $subcontractor->ab_subcontractor->name : ''}}
                                                                                @endif
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            {{$subcontractor->pco_name or ''}}
                                                                        </td>
                                                                        <td>
                                                                            @if (isset($subcontractor->latest_subcontractor_version))
                                                                                {{$subcontractor->latest_subcontractor_version->cycle_no}}
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            {{$subcontractor->mf_number.' - '.$subcontractor->mf_title}}
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($subcontractor->latest_subcontractor_version) && !is_null($subcontractor->latest_subcontractor_version->rec_sub) && $subcontractor->latest_subcontractor_version->rec_sub != 0)
                                                                                {{date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->rec_sub))}}
                                                                                @if(isset($subcontractor->latest_subcontractor_version->subm_sent_sub) && $subcontractor->latest_subcontractor_version->subm_sent_sub != 0)
                                                                                    <?php $dateDiff = strtotime(date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->rec_sub))); ?>
                                                                                @else
                                                                                    <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->rec_sub))); ?>
                                                                                @endif
                                                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if(!is_null($subcontractor->latest_subcontractor_version) && !is_null($subcontractor->latest_subcontractor_version->subm_sent_sub) && $subcontractor->latest_subcontractor_version->subm_sent_sub != 0)
                                                                                {{date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->subm_sent_sub))}}
                                                                                @if(!in_array($subcontractor->latest_subcontractor_version->status->short_name, ['APP', 'AAN']))
                                                                                    <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($subcontractor->latest_subcontractor_version->subm_sent_sub))); ?>
                                                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                                                @endif
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if (isset($subcontractor->latest_subcontractor_version))
                                                                                {{'$'.number_format($subcontractor->latest_subcontractor_version->cost, 2)}}
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if (isset($subcontractor->latest_subcontractor_version))
                                                                                {{$subcontractor->latest_subcontractor_version->status->short_name}}
                                                                            @endif
                                                                        </td>
                                                                        <td>
                                                                            @if(isset($subcontractor->subcontractor_versions) && count($subcontractor->subcontractor_versions) > 0)
                                                                                <?php (count($subcontractor->subcontractor_versions)) > 0 ? $lastSubcontractorVersion = count($subcontractor->subcontractor_versions) - 1 : $lastSubcontractorVersion = 0 ?>
                                                                                @if($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittal_file == true)
                                                                                    @if(isset($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile) && count($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile) > 0)
                                                                                        <p class="transmittal-paragraph">
                                                                                            <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                                            <input type="hidden" class="s3FilePath" id="{{$subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile[0]->id}}"
                                                                                                   value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile[0]->file_name}}">
                                                                                        </p>
                                                                                    @endif
                                                                                @endif
                                                                                @if($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->download_file == true)
                                                                                    @if(isset($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file) && count($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file) > 0)
                                                                                        @foreach($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file as $file)
                                                                                            @if($file->version_date_connection == $subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file_status)
                                                                                                <p class="transmittal-paragraph">
                                                                                                    <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                                    <input type="hidden" class="s3FilePath" id="{{$file->id}}"
                                                                                                           value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                                                </p>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                                <tbody>
                                                            </table>
                                                        </div>
                                                    @endif
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div id="tasks" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'tasks') in active @endif">
                    <div class="col-md-12">
                        @if(((Auth::user()->hasRole('Company Admin')) || Auth::user()->hasRole('Project Admin') || $userCanDelete))
                            {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                            <input type="hidden" value="{{url('tasks')}}/" id="form-url" />
                            <a href="#" disabled id="delete-button" class='btn btn-danger pull-right cm-btn-fixer delete-button-multi' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                {{trans('labels.delete')}}
                            </a>
                            {!! Form::close()!!}
                        @endif
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                        @endif
                        @include("tasks.partials.table")
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('popups.delete_record_popup')
@endsection