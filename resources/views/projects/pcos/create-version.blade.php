@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.pcos.add_new_pco_version')}} <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/edit')}}" class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/edit')}}"
                                                 class="cm-trail-link">@if($versionType == Config::get('constants.pco_version_type.subcontractors')) {{trans('labels.pcos.edit_subcontractor')}} @else {{trans('labels.pcos.edit_recipient')}} @endif</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/create')}}"
                                                        class="cm-trail-link">{{trans('labels.pcos.add_new_pco_version')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                }}
                </div>
            @endif
            <form id="pcos" name="versionForm" role="form" action="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version')}}"
                  accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <div class="row box-cont-1">
                        <div class="col-md-4">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                                <label class="cm-control-required control-label">{{trans('labels.pcos.change_name')}}</label>
                                                            @else
                                                                <label class="cm-control-required control-label">{{trans('labels.pcos.name')}}</label>
                                                            @endif
                                                            <input type="text" readonly class="form-control" name="name" value="{{ $pco->name }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-6 pl0">
                                                            <div class="form-group">
                                                                <label class="cm-control-required ">{{trans('labels.submittals.cycle_no')}}</label>
                                                                <input type="text" readonly class="form-control date-number" id="cycle_no" name="cycle_no" value="{{ !empty(old('cycle_no'))?old('cycle_no'):($pcoSubcontractorRecipient->cycle_counter+1) }}">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 pl0">
                                                            <div class="form-group">
                                                                <input type="hidden" id="useCustom" name="useCustom" value="false" >
                                                                <input type="hidden" id="currentCycleNumber" name="currentCycleNumber" value="{{($pcoSubcontractorRecipient->cycle_counter+1)}}" >
                                                                <a href="#" id="customNumber" class="btn btn-primary mt25 cm-heading-btn">Custom</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>{{trans('labels.pcos.cost')}}</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">$</div>
                                                                <input type="text" class="form-control" name="cost" value="{{ old('cost') }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required control-label">{{trans('labels.status')}}</label>
                                                            <select name="status">
                                                                @foreach($statuses as $key => $value)
                                                                    <option <?php echo (!empty($lastVersionStatus) && $lastVersionStatus == $key) ? 'selected' : ''; ?> value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label class="control-label">&nbsp;</label>
                                                        <input type="hidden" id="type" name="type" value="{{Config::get('constants.pcos')}}">
                                                        <button type="submit" id="submittal-file-submit" class="btn btn-success pull-left">
                                                            {{trans('labels.save')}}
                                                        </button>
                                                        <div class="please_wait text-right"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                            <h4>Change files</h4>
                                        @else
                                            <h4>PCO files</h4>
                                        @endif
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                    <div class="">
                                                        <label class="control-label">{{trans('labels.submittals.received_from_subcontractor')}}</label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="rec-sub" class="form-control" name="rec_sub" value="{{old('rec_sub') }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                        <label class="input-group-btn">
                                                                            <span class="btn btn-primary">
                                                                                <input type="file" style="display: none;" name="rec_sub_file" id="rec_sub_file" multiple>
                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id" value="{{old('rec_sub_file_id') }}">
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="rec_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                                @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                    <div class="">
                                                        <label class="control-label">{{trans('labels.submittals.sent_for_approval')}}</label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="sent-appr" class="form-control" name="sent_appr" value="{{old('sent_appr')}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                        <label class="input-group-btn">
                                                                            <span class="btn btn-primary">
                                                                                <input type="file" style="display: none;" name="sent_appr_file" id="sent_appr_file" multiple>
                                                                                    <span class="glyphicon glyphicon-upload"></span>
                                                                                <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id" value="{{old('sent_appr_file_id') }}">
                                                                            </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="sent_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                    <div class="">
                                                        <label class="control-label">{{trans('labels.submittals.received_from_approval')}}</label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="rec-appr" class="form-control" name="rec_appr" value="{{old('rec_appr') }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                        <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="rec_appr_file" id="rec_appr_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                            <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id" value="{{old('rec_appr_file_id') }}">
                                                                        </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="rec_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                    <div class="">
                                                        <label class="control-label">{{trans('labels.submittals.sent_to_subcontractor')}}</label>
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <input type="text" id="subm-sent-sub" class="form-control" name="subm_sent_sub" value="{{old('subm_sent_sub') }}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                        <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="subm_sent_sub_file" id="subm_sent_sub_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                            <input type="hidden" name="subm_sent_sub_file_id" id="subm_sent_sub_file_id" value="{{old('subm_sent_sub_file_id') }}">
                                                                        </span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="subm_sent_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
@endsection