<!DOCTYPE html>
<html lang="en">
<head>
    <title>Cloud PM</title>
</head>
<body>
<style>
    hr{
        display: block;
        height: 1px;
        border: 0;
        border-top: 1px solid #000;
        margin: 0;
        padding: 0;
    }
</style>
    <div class="cm-page-content">
        <div class="container">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <table width="100%">
                            <tr>
                                <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 400px; vertical-align: bottom;">
                                                <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                                @if(count(Auth::user()->company->addresses))
                                                    {{Auth::user()->company->addresses[0]->street}}<br>
                                                    {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                                @endif
                                            </td>
                                            <td style="width: 450px; margin-left: 40px; vertical-align: bottom;">
                                                {{Auth::user()->email}}<br>
                                                {{Auth::user()->office_phone}}
                                            </td>
                                            <td style="float: right; vertical-align: bottom;">
                                                <h2 style="float: left; margin: 0px; text-align: right;">{{"PCO's"}}</h2>
                                                <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 100%; border-bottom: 3px solid black;">
                                    <table width="100%">
                                        <tr>
                                            <td style="vertical-align: top; width: 465px;">
                                                <b>{{'Project: '}}</b>
                                                {{$project->name}}
                                            </td>
                                            <td style="vertical-align: top; width: 390px; text-align: right;">
                                                <b>{{'Project Number: '}}</b>
                                                {{$project->number}}
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <h3>{{trans('labels.pcos.pcos_filtered_by')}}</h3>
                            <h4>
                                @if(!empty($mf_title))
                                    {{$mf_title}}<br>
                                @endif
                                @if(!empty($sub_title))
                                    {{$sub_title}}<br>
                                @endif
                                @if(!empty($status_title))
                                    {{$status_title}}
                                @endif
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row" style="font-size: 11px;">
                        <style>
                            .rfis-padding {
                                padding: 5px 5px!important;
                                text-align: left!important;
                                vertical-align: top;
                            }
                        </style>
                        @if(count($pcos) != 0)
                            <table style="width: 100%;" class="table table-hover table-striped cm-table-compact">
                                <thead>
                                <tr>
                                    <th class="rfis-padding">{{trans('labels.pcos.version_no')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.cycle_no')}}</th>
                                    <th class="rfis-padding">{{trans('labels.pcos.name')}}</th>
                                    <th class="rfis-padding">
                                        {{trans('labels.mf_number_and_title').' - '.trans('labels.pcos.subcontractors')}}
                                    </th>
                                    <th class="rfis-padding">{{trans('labels.pcos.recipient')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.sent_for_approval')}}</th>
                                    <th class="rfis-padding">{{trans('labels.submittals.received_from_approval')}}</th>
                                    <th class="rfis-padding">{{trans('labels.status')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pcos as $item)
                                        <tr>
                                            <td class="rfis-padding">{{(!is_null($item->number)) ? $item->number : ''}}</td>
                                            <td class="rfis-padding">{{(!is_null($item->version_cycle_no)) ? $item->version_cycle_no : ''}}</td>
                                            <td class="rfis-padding">{{$item->name}}</td>
                                            <td class="rfis-padding">
                                                @if(count($item->subcontractors))
                                                    @foreach($item->subcontractors as $subcontractor)
                                                        <p style="padding: 0px 0px 5px 0px; margin: 0px;">{{$subcontractor->mf_number.' '.$subcontractor->mf_title.' - '.($subcontractor->self_performed ? (!is_null($item->company) ? $item->company->name : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name)}}</p>
                                                    @endforeach
                                                @endif
                                            </td>
                                            <td class="rfis-padding">{{(!is_null($item->recipient)) ? $item->recipient->ab_recipient->name : ''}}</td>
                                            <td class="rfis-padding">
                                                @if($item->version_sent_appr != 0)
                                                    {{date("m/d/Y", strtotime($item->version_sent_appr))}}
                                                    @if($item->version_rec_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_rec_appr))) - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                                    @endif
                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td class="rfis-padding">
                                                @if(!is_null($item->version_rec_appr))
                                                    @if($item->version_rec_appr != 0)
                                                        {{date("m/d/Y", strtotime($item->version_rec_appr))}}
                                                    @endif
                                                    @if($item->version_rec_appr != 0 && !in_array($item->version_status_short_name, ['APP', 'AAN']))
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                @endif
                                            </td>
                                            <td class="rfis-padding">{{(!is_null($item->version_status_short_name)) ? $item->version_status_short_name : ''}}</td>
                                        </tr>
                                        @if(!empty($printWithNotes))
                                            <tr>
                                                <td colspan="8" style="background: #ddd; padding: 5px;">
                                                    @if(!empty($item->recipient) && !empty($item->recipient->latest_recipient_version) && !empty($item->recipient->latest_recipient_version->notes))
                                                        {!! $item->recipient->latest_recipient_version->notes !!}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                        <tr>
                                            <td colspan="8"><hr></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <p class="text-center">{{trans('labels.no_records')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>