<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cloud PM</title>
</head>
<body>
    <table width="100%" class="table table-hover table-striped cm-table-compact" id="unshared-files">
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table width="100%">
                    <tr>
                        <td style="width: 260px; vertical-align: bottom;">
                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                            @if(count(Auth::user()->company->addresses))
                                {{Auth::user()->company->addresses[0]->street}}<br>
                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                            @endif
                        </td>
                        <td style="width: 250px; margin-left: 40px; vertical-align: bottom;">
                            {{Auth::user()->email}}<br>
                            {{Auth::user()->office_phone}}
                        </td>
                        <td style="float: right; width: 180px; vertical-align: bottom; margin: 0px;">
                            <h2 style="float: left; width: 180px; margin: 0px; text-align: right;">{{'PCO'}}</h2>
                            <p style="float: left; width: 180px; margin: 0px; text-align: right;">{{'No: '.$version->subcontractor->pco->number}}</p>
                            <p style="float: left; width: 180px; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 3px solid black; width: 100%;">
                <table width="100%">
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'Project: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            {{$version->subcontractor->pco->project->name}}
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'Project Number: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{$version->subcontractor->pco->project->number}}
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'To: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            @if(!is_null($version->subcontractor->subcontractor_contact))
                                {{$version->subcontractor->subcontractor_contact->name}}<br>
                                {{$version->subcontractor->subcontractor_contact->title}}
                            @endif
                            <br />
                            {{($version->subcontractor->self_performed ? Auth::user()->company->name : $version->subcontractor->ab_subcontractor->name)}}<br>
                            @if(!is_null($version->subcontractor->subcontractor_contact))
                                @if(!is_null($version->subcontractor->subcontractor_contact->office))
                                    {{$version->subcontractor->subcontractor_contact->office->street}}<br>
                                    {{$version->subcontractor->subcontractor_contact->office->city.', '.$version->subcontractor->subcontractor_contact->office->state.' '.$version->subcontractor->subcontractor_contact->office->zip}}<br>
                                @endif
                                {{$version->subcontractor->subcontractor_contact->office_phone}}<br>
                                {{$version->subcontractor->subcontractor_contact->email}}<br>
                            @endif
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'From: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{Auth::user()->name}}<br>
                            {{Auth::user()->title}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 345px; vertical-align: top;">
                            <p>
                                <b>{{'Subject: '}}</b>
                                {{$version->subcontractor->pco->subject}}
                            </p>
                            <p>
                                <b>{{'Cost: '}}</b>
                                {{'$'.number_format(($version->subcontractor->latest_subcontractor_version != null)?$version->subcontractor->latest_subcontractor_version->cost:0, 2)}}
                            </p>
                        </td>
                        <td style="width: 345px; vertical-align: top;">
                            <p style="text-align: right;">
                                <b>{{'Sent Via: '}}</b>
                                {{$version->subcontractor->pco->sent_via}}
                            </p>
                            <p style="text-align: right;">
                                <b>{{'Status: '}}</b>
                                @if(!is_null($version->status))
                                    {{$version->status->name}}
                                @endif
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 180px;"><b>{{'PCO No: '}}</b></td>
                        <td>{{$version->subcontractor->pco->number}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Cycle No: '}}</b></td>
                        <td>{{$version->cycle_no}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'MF Number & Title: '}}</b></td>
                        <td>{{$version->subcontractor->mf_number." - ".$version->subcontractor->mf_title}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Subcontractor/Supplier: '}}</b></td>
                        <td>{{$version->subcontractor->self_performed ? Auth::user()->company->name : $version->subcontractor->ab_subcontractor->name}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'General Contractor: '}}</b></td>
                        <td>
                            @if($version->subcontractor->pco->project->make_me_gc == 1 && $version->subcontractor->pco->project->company)
                                {{$version->subcontractor->pco->project->company->name}}
                            @elseif($version->subcontractor->pco->make_me_gc === 0 && $version->subcontractor->pco->project->make_me_gc === 0)
                                @if(!is_null($version->subcontractor->pco->general_contractor))
                                    {{$version->subcontractor->pco->general_contractor->name}}
                                @endif
                            @else
                                {{Auth::user()->company->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 180px;">
                            <b>{{'Reason For Change: '}}</b>
                        </td>
                        <td>{{$version->subcontractor->pco->reason_for_change}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%;">
                <table>
                    <tr>
                        <td style="width: 180px;vertical-align: top;">
                            <br/>
                            <b>
                                {{trans('labels.pcos.notes')}}:
                            </b>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <div style="margin-left:5px;">
        {!! $version->notes !!}
    </div>
</body>
</html>