@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.pcos.edit_pco_version')}}
                <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/edit')}}" class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/edit')}}"
                                                 class="cm-trail-link">@if($versionType == Config::get('constants.pco_version_type.subcontractors')) {{trans('labels.pcos.edit_subcontractor')}} @else {{trans('labels.pcos.edit_recipient')}} @endif</a>
                    </li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$pcoVersion->id.'/edit')}}"
                                                        class="cm-trail-link">{{trans('labels.pcos.edit_pco_version')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                        }}
                </div>
            @endif
            <div>
                {!! Form::open(['files'=>true, 'name' => 'versionForm', 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$pcoVersion->id),
                            'id' => 'pco_subcontractor', 'role' => 'form']) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row box-cont-1">
                    <div class="col-md-4">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                            <label class="cm-control-required control-label">{{trans('labels.pcos.change_name')}}</label>
                                                        @else
                                                            <label class="cm-control-required control-label">{{trans('labels.pcos.name')}}</label>
                                                        @endif
                                                        @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                            <input type="text" readonly class="form-control " name="name" value="{{ $pcoVersion->subcontractor->pco_name or '' }}">
                                                        @else
                                                            <input type="text" readonly class="form-control " name="name" value="{{ $pco->name }}">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="col-md-6 pl0">
                                                        <div class="form-group">
                                                            <label class="cm-control-required">{{trans('labels.submittals.cycle_no')}}</label>
                                                            <input type="text" class="form-control " name="cycle_no" value="{{ $pcoVersion->cycle_no }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 pl0">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>{{trans('labels.pcos.cost')}}</label>
                                                        <div class="">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">$</span>
                                                                <input type="text" class="form-control" name="cost" value="{{ number_format($pcoVersion->cost, 2) }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="cm-control-required">{{trans('labels.status')}}</label>
                                                        <div class="status-cont ">
                                                            <select name="status">
                                                                @foreach($statuses as $id => $name)
                                                                    <option {{ (isset($pcoVersion->status) && $pcoVersion->status->id == $id) ? 'selected' : '' }} value="{{$id}}">{{$name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label class="control-label">&nbsp;</label>
                                                    <input type="hidden" id="type" name="type" value="{{Config::get('constants.pcos')}}">
                                                    <input type="hidden" id="module" name="type" value="{{Request::segment(3)}}">
                                                    <input type="hidden" id="databaseType" name="databaseType" value="{{Config::get('constants.transmittal_type.pco')}}">
                                                    <input type="hidden" id="versionId" name="versionId" value="{{$pcoVersion->id}}">
                                                    <input type="hidden" id="projectId" name="projectId" value="{{$project->id}}">
                                                    <button type="submit" id="submittal-file-submit" class="btn btn-success pull-left">
                                                        {{trans('labels.update')}}
                                                    </button>
                                                    <div class="col-md-4">
                                                        <div class="please_wait text-right"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                        <h4>Change files</h4>
                                    @else
                                        <h4>PCO files</h4>
                                    @endif
                                    <div class="cm-spacer-xs"></div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                <div class="">
                                                    <label>{{trans('labels.submittals.received_from_subcontractor')}}</label>
                                                    <div class="row ">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="text" id="rec-sub" class="form-control" name="rec_sub" value="{{ $rec_sub }}">
                                                                <span class="input-group-addon">@if(isset($pcoVersion->rec_sub) && $pcoVersion->rec_sub != 0)
                                                                        @if(isset($pcoVersion->subm_sent_sub) && $pcoVersion->subm_sent_sub != 0)
                                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_sub))); ?>
                                                                        @else
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_sub))); ?>
                                                                        @endif
                                                                        <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>                                    @endif
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="rec_sub_file" id="rec_sub_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                                @if(count($pcoVersion->files))
                                                                    <?php $i = 0; ?>
                                                                    @foreach($pcoVersion->files as $recSubFile)
                                                                        @if($recSubFile->file->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                                                            <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id" value="{{$recSubFile->file->id}}">
                                                                            <?php $i = 1; ?>
                                                                        @endif
                                                                    @endforeach
                                                                    @if($i == 0)
                                                                        <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                                                    @endif
                                                                @else
                                                                    <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                                                @endif
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div id="rec_sub_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3">
                                                            @if(count($pcoVersion->files))
                                                                @foreach($pcoVersion->files as $file)
                                                                    @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                                                        <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                                                        @if(isset($pcoVersion['email_transmittal_files']['sentAppr3Path']) && $pcoVersion['email_transmittal_files']['sentAppr3Path'] == Config::get('constants.submittal_version_files.rec_sub_file'))
                                                                            <input type="hidden" class="s3FilePath sentAppr3Path" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @else
                                                                            <input type="hidden" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                            || (Permissions::can('delete', 'pcos')))
                                                                @if(count($pcoVersion->files))
                                                                    @foreach($pcoVersion->files as $file)
                                                                        @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.rec_sub_file'))
                                                                            <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                                                               data-title="Delete Record" data-message="{{trans('labels.global_delete_modal')}}">
                                                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                                <div class="pull-left delete-wait" style="display: none; margin-left: 15px;">
                                                                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                </div>
                                                                            </a>
                                                                            <input type="hidden" data-type="pcos" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                <div class="">
                                                    <label>{{trans('labels.submittals.sent_for_approval')}}</label>
                                                    <div class="row ">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="text" id="sent-appr" class="" name="sent_appr" value="{{ $sent_appr }}">
                                                                <span class="input-group-addon">
                                                                @if($pcoVersion->sent_appr != 0)
                                                                        @if($pcoVersion->rec_appr != 0)
                                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))) - strtotime(date("m/d/Y", strtotime($pcoVersion->sent_appr))); ?>
                                                                        @else
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->sent_appr))); ?>
                                                                        @endif
                                                                        <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>                                    @endif
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                        || (Permissions::can('write', 'pcos')))
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="sent_appr_file" id="sent_appr_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                                @if(count($pcoVersion->files))
                                                                    <?php $i = 0; ?> @foreach($pcoVersion->files as $file) @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                                                        <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id" value="{{$file->file->id}}">
                                                                        <?php $i = 1; ?> @endif @endforeach @if($i == 0)
                                                                        <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id"> @endif @else
                                                                    <input type="hidden" name="sent_appr_file_id" id="sent_appr_file_id"> @endif
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div id="sent_appr_file_wait" class="pull-left ml5" style="display: none; margin-left: 10px;">
                                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3">
                                                        @if(count($pcoVersion->files))
                                                            @foreach($pcoVersion->files as $file)
                                                                @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                                                    <!-- {{trans('labels.file.current')}} -->
                                                                        <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                                                        @if(isset($pcoVersion['email_transmittal_files']['submSent3Path']) && $pcoVersion['email_transmittal_files']['submSent3Path'] == Config::get('constants.submittal_version_files.sent_appr_file'))
                                                                            <input type="hidden" class="s3FilePath submSent3Path" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @else
                                                                            <input type="hidden" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                            || (Permissions::can('delete', 'pcos'))) @if(count($pcoVersion->files)) @foreach($pcoVersion->files as $file) @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.sent_appr_file'))
                                                                <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                                                   data-title="Delete Record" data-message="{{trans('labels.global_delete_modal')}}">
                                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                    <div class="pull-left delete-wait" style="display: none;">
                                                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                    </div>
                                                                </a>
                                                                <input type="hidden" data-type="pcos" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                            @endif @endforeach @endif @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                <div class="">
                                                    <label>{{trans('labels.submittals.received_from_approval')}}</label>
                                                    <div class="row ">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="text" id="rec-appr" class="form-control " name="rec_appr" value="{{ $rec_appr }}">
                                                                <span class="input-group-addon">
                                                                    @if($pcoVersion->rec_appr != 0)
                                                                        @if($pcoVersion->subm_sent_sub != 0)
                                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))); ?>
                                                                        @else
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))); ?>
                                                                        @endif
                                                                        <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>                                    @endif
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                        || (Permissions::can('write', 'pcos')))
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="rec_appr_file" id="rec_appr_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                                @if(count($pcoVersion->files))
                                                                    <?php $i = 0; ?> @foreach($pcoVersion->files as $file) @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                                                        <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id" value="{{$file->file->id}}">
                                                                        <?php $i = 1; ?> @endif @endforeach @if($i == 0)
                                                                        <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id"> @endif @else
                                                                    <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id"> @endif
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div id="rec_appr_file_wait" class="pull-left ml5"
                                                                     style="display: none; margin-left: 10px;">
                                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                                                         width="17px">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3">
                                                        @if(count($pcoVersion->files))
                                                            @foreach($pcoVersion->files as $file)
                                                                @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                                                    <!-- {{trans('labels.file.current')}} -->
                                                                        <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                                                        @if(isset($pcoVersion['email_transmittal_files']['submSent3Path']) && $pcoVersion['email_transmittal_files']['submSent3Path'] == Config::get('constants.submittal_version_files.rec_appr_file'))
                                                                            <input type="hidden" class="s3FilePath submSent3Path" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @else
                                                                            <input type="hidden" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                            || (Permissions::can('delete', 'pcos')))
                                                            @if(count($pcoVersion->files))
                                                                @foreach($pcoVersion->files as $file)
                                                                @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.rec_appr_file'))
                                                                <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                                                   data-title="Delete Record" data-message="{{trans('labels.global_delete_modal')}}">
                                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                    <div class="pull-left delete-wait" style="display: none;">
                                                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                    </div>
                                                                </a>
                                                                <input type="hidden" data-type="pcos" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                @endif
                                                                @endforeach
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                <div class="">
                                                    <label>{{trans('labels.submittals.sent_to_subcontractor')}}</label>
                                                    <div class="row ">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="text" id="subm-sent-sub" name="subm_sent_sub" class="" value="{{ $subm_sent_sub }}">
                                                                <span class="input-group-addon">
                                                                    @if(!empty($subm_sent_sub))
                                                                        @if(!in_array($pcoVersion->status->short_name, ['APP', 'AAN']))
                                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))); ?>
                                                                            <span>({{floor($dateDiff / (60 * 60 * 24))}})</span>
                                                                        @endif
                                                                    @endif
                                                                </span>
                                                            </div>
                                                        </div>
                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                        || (Permissions::can('write', 'pcos')))
                                                            <div class="col-md-4">
                                                                <!-- // upload input -->
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" readonly title="Attach file" placeholder="Attach file">
                                                                    <label class="input-group-btn">
                                                                        <span class="btn btn-primary">
                                                                            <input type="file" style="display: none;" name="subm_sent_sub_file" id="subm_sent_sub_file" multiple>
                                                                            <span class="glyphicon glyphicon-upload"></span>
                                                                        </span>
                                                                    </label>
                                                                </div>
                                                                @if(count($pcoVersion->files))
                                                                    <?php $i = 0; ?> @foreach($pcoVersion->files as $file) @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                                                        <input type="hidden" name="subm_sent_sub_file_id" id="subm_sent_sub_file_id" value="{{$file->file->id}}">
                                                                        <?php $i = 1; ?> @endif @endforeach @if($i == 0)
                                                                        <input type="hidden" name="subm_sent_sub_file_id" id="subm_sent_sub_file_id"> @endif @else
                                                                    <input type="hidden" name="subm_sent_sub_file_id" id="subm_sent_sub_file_id"> @endif
                                                            </div>
                                                            <div class="col-md-1">
                                                                <div id="subm_sent_sub_file_wait" class="pull-left ml5" style="display: none;">
                                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                            </div>
                                                        @endif
                                                        <div class="col-md-3">
                                                            @if(count($pcoVersion->files))
                                                                @foreach($pcoVersion->files as $file)
                                                                    @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                                                        <a class="download pull-left btn btn-primary btn-download btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                                                                        @if(isset($pcoVersion['email_transmittal_files']['sentAppr3Path']) && $pcoVersion['email_transmittal_files']['sentAppr3Path'] == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                                                            <input type="hidden" class="s3FilePath sentAppr3Path" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @else
                                                                            <input type="hidden" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endif
                                                                @endforeach
                                                            @endif
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                                                            || (Permissions::can('delete', 'pcos')))
                                                                @if(count($pcoVersion->files))
                                                                    @foreach($pcoVersion->files as $file)
                                                                        @if($file->file->version_date_connection == Config::get('constants.submittal_version_files.subm_sent_sub_file'))
                                                                            <a class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" data-toggle="modal" data-target="#confirmDelete"
                                                                               data-title="Delete Record" data-message="{{trans('labels.global_delete_modal')}}">
                                                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                                                <div class="pull-left delete-wait" style="display: none; margin-left: 15px;">
                                                                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                </div>
                                                                            </a>
                                                                            <input type="hidden" data-type="pcos" class="s3FilePath" id="{{$file->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$file->file->file_name}}">
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close()!!}
                <div class="row box-cont-2">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="row">
                                                <form method="POST" id="notesForm" action="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$pcoVersion->id.'/notes')}}">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <div class="col-md-12">
                                                        <label class="control-label">
                                                            @if($versionType != 'subcontractors')
                                                                {{trans('labels.pcos.description_of_change')}}
                                                            @else
                                                                {{trans('labels.pcos.transmittal_notes')}}
                                                            @endif
                                                            <small class="cm-heading-btn">{{trans('labels.char-rows-15')}}</small>
                                                        </label>
                                                        @if($versionType != 'subcontractors')
                                                            <ul class="nav nav-tabs cm-inner-tabs">
                                                                <li class="active">
                                                                    <a>
                                                                        {{trans('labels.submittals.appr_note')}}
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        @else
                                                            <ul class="nav nav-tabs cm-inner-tabs">
                                                                <li class="active">
                                                                    <a>
                                                                        {{trans('labels.submittals.subc_note')}}
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        @endif

                                                        <div class="tab-content mt20">
                                                            <!--TAB 1 =========================================-->
                                                            <div id="trans" class="tab-pane fade in active">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <textarea class="form-control span12 textEditorSmallVersion" rows="5" name="version_note" id="version_note">{!! $pcoVersion->notes !!}</textarea>
                                                                        <span id="version_note_character_count"></span>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            @if($versionType != 'subcontractors')
                                                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                                                    <a class="btn btn-sm btn-info pull-left" id="generateRecTransmittal" href="javascript:;">
                                                                                        <span class="glyphicon glyphicon-duplicate mr5" aria-hidden="true"></span>
                                                                                        <span>Create Transmittal</span>
                                                                                        <div id="rec_transmittal_wait" class="pull-left" style="display: none;">
                                                                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                        </div>
                                                                                    </a>
                                                                                    <input type="hidden" id="recTransmittalDate" name="recTransmittalDate" value="{{Config::get('constants.transmittal_date_type.sent_appr')}}">
                                                                                    @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                                                                        <a class="btn btn-sm cm-btn-secondary download ml5 pull-left" href="javascript:;">
                                                                                            <i class="glyphicon glyphicon-download mr5" aria-hidden="true"></i>
                                                                                            {{trans('labels.files.download')}}
                                                                                        </a>
                                                                                        <input type="hidden" class="s3FilePath" id="{{$recTransmittal->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$recTransmittal->file->file_name}}">

                                                                                    @endif
                                                                                @endif
                                                                            @else
                                                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                                                    <a class="btn btn-info pull-left" id="generateSubTransmittal" href="javascript:;">
                                                                                        <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
                                                                                        <span>Create Transmittal</span>
                                                                                        <div id="sub_transmittal_wait" class="pull-left" style="display: none;">
                                                                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                        </div>
                                                                                    </a>
                                                                                    <input type="hidden" id="subTransmittalDate" name="subTransmittalDate" value="{{Config::get('constants.transmittal_date_type.subm_sent_sub')}}">
                                                                                    @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                                                                        <a class="btn btn-sm cm-btn-secondary download ml5 pull-left" href="javascript:;">
                                                                                            <i class="glyphicon glyphicon-download mr5" aria-hidden="true"></i>
                                                                                            {{trans('labels.files.download')}}
                                                                                        </a>
                                                                                        <input type="hidden" class="s3FilePath" id="{{$subTransmittal->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$subTransmittal->file->file_name}}">

                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            @if(($versionType != 'subcontractors' && !is_null($recTransmittal) && !is_null($recTransmittal->file)) ||
                                                                                ($versionType == 'subcontractors' && !is_null($subTransmittal) && !is_null($subTransmittal->file)))
                                                                                <div class="row">
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                                                                            <div class="cms-list-box">
                                                                                                <ul class="cms-list-group">
                                                                                                    <li class="cms-list-group-item cf">
                                                                                                        <div class="checkbox">
                                                                                                            <label>
                                                                                                                <input type="checkbox" name="select_all_companies" class="select_all_companies select_all_company_users" id="select_all_companies">
                                                                                                                <span class="checkbox-material">
                                                                                                                    <span class="check"></span>
                                                                                                                </span>
                                                                                                                Select All Companies
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                    <?php $allCompanyIds[] = '0'.$project->id; ?>
                                                                                                    <li class="cms-list-group-item cf">
                                                                                                        <div class="radio">
                                                                                                            <label>
                                                                                                                <input type="radio" checked class="companyCheckboxIn" name="companyRadioDistributionIn" data-type="user" id="mycompany">
                                                                                                                <span class="radio-material">
                                                                                                                    <span class="check"></span>
                                                                                                                </span>
                                                                                                                {{$myCompany->name}}
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </li>
                                                                                                    @foreach($companies as $id => $name)
                                                                                                        <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                                                                            <div class="radio">
                                                                                                                <label>
                                                                                                                    <input type="radio" class="companyCheckboxDistributionIn" name="companyRadioDistributionIn" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                                                                                                    <span class="radio-material">
                                                                                                                        <span class="check"></span>
                                                                                                                    </span>
                                                                                                                    {{$name}}
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                        <?php $allCompanyIds[] = $id; ?>
                                                                                                    @endforeach
                                                                                                </ul>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                                                                            <div class="cms-list-box">
                                                                                                <ul class="cms-list-group" id="usersListIn">
                                                                                                    <?php $my_company_users = []; $myCompanyUserId = 0; $myCompanyUserCount = 0; ?>
                                                                                                    @if(count($myCompany->users) > 0)
                                                                                                        <li class="cms-list-group-item cf">
                                                                                                            <div class="checkbox">
                                                                                                                <label>
                                                                                                                    <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_0{{$project->id}}">
                                                                                                                    <span class="checkbox-material">
                                                                                                                        <span class="check"></span>
                                                                                                                    </span>
                                                                                                                    Select All Users
                                                                                                                </label>
                                                                                                            </div>
                                                                                                        </li>
                                                                                                        @foreach($myCompany->users as $user)
                                                                                                            @if(in_array($user->id, $projectUsers) || $user->hasRole(Config::get('constants.roles.company_admin')))
                                                                                                                <li class="cms-list-group-item cf" id="0{{$project->id}}_{{$user->id}}">
                                                                                                                    <div class="checkbox">
                                                                                                                        <label>
                                                                                                                            <input {{(!empty(old('employees_users_in')) && in_array($user->id, old('employees_users_in')))?'checked':''}}
                                                                                                                                   type="checkbox" name="employees_users_in[]" class="users_0{{$project->id}}" value="{{$user->id}}">
                                                                                                                            <span class="checkbox-material">
                                                                                                                                <span class="check"></span>
                                                                                                                            </span>
                                                                                                                            {{$user->name}}
                                                                                                                        </label>
                                                                                                                    </div>
                                                                                                                </li>
                                                                                                                <?php $my_company_users[$user->id] = [$myCompany->id => $user->name]; ?>
                                                                                                                @if($myCompanyUserCount == 0)
                                                                                                                    <?php $myCompanyUserId =  $user->id; $myCompanyUserCount++; ?>
                                                                                                                @endif
                                                                                                            @endif
                                                                                                        @endforeach
                                                                                                    @endif
                                                                                                </ul>
                                                                                                <?php $other_users = []; ?>
                                                                                                @foreach($users as $id => $value)
                                                                                                    <ul class="cms-list-group contactsListDistributionIn hide" id="contactsListDistributionIn-{{$id}}">
                                                                                                        @if(count($value) > 0)
                                                                                                            <li class="cms-list-group-item cf">
                                                                                                                <div class="checkbox">
                                                                                                                    <label>
                                                                                                                        <input type="checkbox" name="select_all_users" class="select_all_users" id="select_all_users_{{$id}}">
                                                                                                                        <span class="checkbox-material">
                                                                                                                            <span class="check"></span>
                                                                                                                        </span>
                                                                                                                        Select All Users
                                                                                                                    </label>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                            @foreach($value as $project_contact)
                                                                                                                @if (!empty($project_contact->contact))
                                                                                                                    <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in" id="{{$id}}_{{$project_contact->contact->id}}">
                                                                                                                        <div class="checkbox">
                                                                                                                            <label>
                                                                                                                                <input {{(!empty(old('employees_contacts_in')) && in_array($project_contact->contact->id, old('employees_contacts_in')))?'checked':''}}
                                                                                                                                       type="checkbox" name="employees_contacts_in[]" class="users_{{$id}}" value="{{$project_contact->contact->id}}">
                                                                                                                                <span class="checkbox-material">
                                                                                                                                    <span class="check"></span>
                                                                                                                                </span>
                                                                                                                                {{$project_contact->contact->name}}
                                                                                                                            </label>
                                                                                                                        </div>
                                                                                                                    </li>
                                                                                                                    <?php $other_users[$project_contact->contact->id] = [$id => $project_contact->contact->name]; ?>
                                                                                                                @endif
                                                                                                            @endforeach
                                                                                                        @endif
                                                                                                    </ul>
                                                                                                @endforeach
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <input id="allCompanyIds" type="hidden" value="{{implode(',',$allCompanyIds)}}">
                                                                                </div>
                                                                            @endif
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    @if($versionType != 'subcontractors')
                                                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                                                           @if(!is_null($recTransmittal) && !is_null($recTransmittal->file))
                                                                                                <a class="btn btn-sm btn-primary pull-left ml5 sendTransmittalEmail" title="{{trans('labels.transmittals.send_email')}}" data-transmittal-path="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$recTransmittal->file->file_name}}"
                                                                                                   data-transmittal-id="{{$recTransmittal->id}}" data-file-id="{{$recTransmittal->file->id}}"
                                                                                                   data-file-uploaded-selector="submSent3Path" data-emailed="{{$recTransmittal->emailed}}"
                                                                                                   data-sub-rec="{{Config::get('constants.ab_company_type.recipient')}}" data-sub-rec-id="{{Request::segment(6)}}"
                                                                                                   href="javascript:;">
                                                                                                    <span class="glyphicon glyphicon-send mr5" aria-hidden="true"></span>
                                                                                                    <span>Email Transmittal</span>
                                                                                                    <div class="pull-left email_wait" style="display: none;">
                                                                                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                                    </div>
                                                                                                </a>
                                                                                            @endif
                                                                                        @endif
                                                                                    @else
                                                                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'pcos')))
                                                                                            @if(!is_null($subTransmittal) && !is_null($subTransmittal->file))
                                                                                                <a class="btn btn-primary pull-left ml5 sendTransmittalEmail" title="{{trans('labels.transmittals.send_email')}}" data-transmittal-path="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/transmittals/'.$subTransmittal->file->file_name}}"
                                                                                                   data-transmittal-id="{{$subTransmittal->id}}" data-file-id="{{$subTransmittal->file->id}}"
                                                                                                   data-file-uploaded-selector="sentAppr3Path" data-emailed="{{$subTransmittal->emailed}}"
                                                                                                   data-sub-rec="{{Config::get('constants.ab_company_type.subcontractor')}}" data-sub-rec-id="{{Request::segment(6)}}"
                                                                                                   href="javascript:;">
                                                                                                    <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                                                                                    <span>Email Transmittal</span>
                                                                                                    <div class="pull-left email_wait" style="display: none;">
                                                                                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                                                    </div>
                                                                                                </a>
                                                                                            @endif
                                                                                        @endif
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @if(($versionType != 'subcontractors' && !is_null($recTransmittal) && !is_null($recTransmittal->file)) ||
                                                                        ($versionType == 'subcontractors' && !is_null($subTransmittal) && !is_null($subTransmittal->file)))
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                                                            </div>
                                                                            <div class="col-md-12">
                                                                                @if(!empty($pcoVersion->pcoVersionApprUsers))
                                                                                    @foreach($pcoVersion->pcoVersionApprUsers as $pcoVersionUser)
                                                                                        <?php $sendDate = Carbon\Carbon::parse($pcoVersionUser->created_at); ?>
                                                                                        @if(!empty($pcoVersionUser->users))
                                                                                            @foreach($pcoVersionUser->users as $user)
                                                                                                {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                                                            @endforeach
                                                                                        @endif
                                                                                        @if(!empty($pcoVersionUser->abUsers))
                                                                                            @foreach($pcoVersionUser->abUsers as $user)
                                                                                                {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                                                            @endforeach
                                                                                        @endif
                                                                                    @endforeach
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
@endsection