@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.pcos.add_subcontractor')}} <small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/edit')}}" class="cm-trail-link">{{trans('labels.pcos.edit')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/pcos/'.$pco->id.'/subcontractors/create')}}" class="cm-trail-link">{{trans('labels.pcos.add_subcontractor')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                        }}
                </div>
            @endif
            <form id="rfis" role="form" action="{{URL('/projects/'.$project->id.'/pcos/'.$pco->id.'/subcontractors')}}" accept-charset="UTF-8"
                  method="POST" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="row box-cont-1">
                      <div class="col-md-12">
                          <div class="box">
                              <div class="card">
                                  <div class="card-body">
                                      <h4>Change Info</h4>
                                      <div class="cm-spacer-xs"></div>
                                      <div class="row">
                                          <div class="col-md-6">
                                              <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                              <div class="row">
                                                  <div class="col-md-12">
                                                      <div class="form-group">
                                                          <label class="cm-control-required">{{trans('labels.pcos.change_name')}}</label>
                                                          <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-10">
                                                      <div class="form-group">
                                                          <label>{{trans('labels.pcos.master_format_search')}}</label>
                                                          <input type="text" class="cm-control-required form-control mf-number-title-auto" name="master_format_search" value="{{ old('master_format_search') }}">
                                                          <input type="hidden" class="master-format-id" name="master_format_id" value="{{old('master_format_id')}}">
                                                          <input type="hidden" id="project_id" value="{{$project->id}}">
                                                      </div>
                                                  </div>
                                                  <div class="col-md-2">
                                                      <div class="form-group">
                                                          <div class="mf-cont">
                                                              <label class="control-label">&nbsp;</label>
                                                              <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">
                                                                  {{trans('labels.submittals.enter_custom_number_and_title')}}
                                                              </a>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                              <label class="cm-control-required">
                                                  {{trans('labels.master_format_number_and_title')}}
                                              </label>
                                              <div class="row">
                                                  <div class="col-md-7">
                                                      <div class="form-group">
                                                          <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{old('master_format_number') }}">
                                                      </div>
                                                  </div>
                                                  <div class="col-md-5">
                                                      <div class="form-group">
                                                          <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{ old('master_format_title') }}">
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="row">
                                                  <div class="col-md-7">
                                                      <div class="form-group" id="subcontractor_container">
                                                          <label class="control-label">{{trans('labels.submittals.import_supplier_from_companies')}}</label>
                                                          <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="{{ old('supplier_subcontractor') }}">
                                                          <input type="hidden" class="address-book-id" name="sub_id" value="{{old('sub_id')}}">
                                                          <input type="hidden" id="ab_sub_name" name="ab_sub_name" value="{{old('ab_sub_name')}}">
                                                      </div>
                                                      <div class="form-group" id="notif_subcontractor" @if(!old('notif_subcontractor_id')) style="display: none;" @endif>
                                                          <input type="hidden" id="notif_subcontractor_id" name="notif_subcontractor_id" value="">
                                                      </div>
                                                  </div>
                                                  <div class="col-md-5">
                                                      @if(old('sub_contact') != '')
                                                          <div class="subcontractor_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;">
                                                              <div class="row">
                                                                  <label>Select contact:</label>
                                                                  <select name="sub_contact" id="sub_contact">
                                                                      <option value="{{old('sub_contact')}}">{{old('ab_sub_name')}}</option>
                                                                  </select>
                                                              </div>
                                                          </div>
                                                      @else
                                                          <div class="subcontractor_contacts_container col-sm-12 pl0" style="margin-bottom: 10px;"></div>
                                                      @endif
                                                  </div>
                                              </div>
                                              <div class="col-md-12">
                                                  <div class="form-group">
                                                      <input type="checkbox" name="self_performed" id="self_performed"> {{trans('labels.submittals.self_performed')}}
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">{{trans('labels.pcos.internal_notes')}}</label>
                                                  <textarea class="form-control span12 textEditorSmallSubcontractors" rows="5" name="note">{!! old('note') !!}</textarea>
                                              </div>
                                              <div class="form-group">
                                                  <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                      {{trans('labels.save')}}
                                                  </button>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
            </form>
        </div>
    </div>
@endsection