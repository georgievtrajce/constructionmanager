@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.pcos.create')}}
                <small class="cm-heading-sub">{{trans('labels.Project').': '.$project->name}}</small>
            </header>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'pcos'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            <form id="rfis" role="form" action="{{URL('/projects/'.$project->id.'/pcos')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <div class="row box-cont-1">
                        <div class="col-md-6">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>{{trans('labels.pco_info')}}</h4>
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control-required">{{trans('labels.pcos.name')}}</label>
                                                            <input type="text" class="form-control" id="item-name" name="name" value="{{ old('name') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <label class="cm-control-required  control-label">{{trans('labels.pcos.number')}}</label>
                                                            <input type="text" readonly class="form-control" id="item-number" name="number" value="{{ (!empty(old('number')))?old('number'):$project->generated_number }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="control-label">&nbsp;</label>
                                                            <a href="javascript:;" id="custom-number" class="btn btn-primary pull-right">
                                                                {{trans('labels.pcos.enter_version_number')}}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="control-label ">
                                                                {{trans('labels.pcos.import_general_contractor')}}
                                                            </label>
                                                            <div class="general_contractor_container">
                                                                @if($project->make_me_gc && isset($project->company->name))
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->company->name}}" readonly>
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->company->id}}">
                                                                @elseif(isset($project->generalContractor->name))
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->generalContractor->name}}" readonly>
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->generalContractor->id}}">
                                                                @else
                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{old('general_contractor')}}">
                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{old('general_contractor_id')}}">
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="box">
                                <div class="card">
                                    <div class="card-body">
                                        <h4>Transmittal Info</h4>
                                        <div class="cm-spacer-xs"></div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>{{trans('labels.submittals.subject')}}</label>
                                                            <input type="text" class="form-control" id="transmittal-subject" name="subject" value="{{ old('subject') }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{trans('labels.submittals.sent_via')}}</label>
                                                            <select class="custom-options" name="sent_via_dropdown" id="sent_via_dropdown" data-id="sent">
                                                                @foreach($sentVia as $item)
                                                                    <option <?php echo (!empty(old('sent_via')) && old('sent_via') == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                @endforeach
                                                                <option {{ (!empty(old('sent_via')) && !in_array(old('sent_via'), $sentVia))?'selected':'' }} value="0">Custom</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="custom-text-sent form-control {{ (!empty(old('sent_via')) && !in_array(old('sent_via'), $sentVia))?'':'hide' }}" name="sent_via" id="sent_via" value="{{ (!in_array(old('sent_via'), $sentVia))?old('sent_via'):'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>{{trans('labels.pcos.reason_for_change')}}</label>
                                                            <select class="custom-options" name="reason_for_change_dropdown" id="reason_for_change_dropdown" data-id="reason">
                                                                @foreach($reasonsForChange as $item)
                                                                    <option <?php echo (!empty(old('reason_for_change')) && old('reason_for_change') == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                @endforeach
                                                                <option {{ (!empty(old('reason_for_change')) && !in_array(old('reason_for_change'), $reasonsForChange))?'selected':'' }} value="0">Custom</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <input type="text" class="custom-text-reason form-control {{ (!empty(old('reason_for_change')) && !in_array(old('reason_for_change'), $reasonsForChange))?'':'hide' }}" name="reason_for_change" id="reason_for_change" value="{{ (!in_array(old('reason_for_change'), $reasonsForChange))?old('reason_for_change'):'' }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label class="cm-control">{{trans('labels.pcos.internal_notes')}}</label>
                                                            <textarea class="form-control span12 textEditorSmall" rows="4" name="description_of_change">{!! old('description_of_change') !!}</textarea>
                                                            <br/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="cm-spacer-xs"></div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <input type="hidden" id="type" name="type" value="{{Config::get('constants.submittals')}}">
                                                        <button type="submit" id="submittal-file-submit" class="btn btn-success pull-right">
                                                            {{trans('labels.save')}}
                                                        </button>
                                                        <p class="please_wait text-right"></p>
                                                        <div id="results"></div>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                                <div class="cm-spacer-xs"></div>
                                                <div class="cm-spacer-xs"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection