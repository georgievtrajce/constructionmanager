<p>A new {{$type}} was uploaded for the project @if (isset($project->name)) {{$project->name}}@endif.
    You can see the file <a class="more" href="{{URL('/projects/'.$project->id.'/project-files/'.$type.'/file')}}">here</a>.
</p>