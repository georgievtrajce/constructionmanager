@extends('layouts.tabs')
@section('title')
{{trans('labels.Blog')}}
<a href="{{URL('/projects/'.$project->id.'/blog/create')}}" class="btn btn-success pull-right">{{trans('labels.blog.create')}}</a>
<ul class="cm-trail">
    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
    class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/blog')}}" class="cm-trail-link">{{trans('labels.Blog')}}</a></li>
</ul>
@endsection
@section('tabs')
@include('projects.partials.tabs', array('activeTab' => 'blog'))
@endsection
@section('tabsContent')
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-6">
                {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/blog']) !!}
                <div class="form-group">
                    {!! Form::label('title', trans('labels.blog.title').':') !!}
                    {!! Form::text('title', Input::old('title'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-12">
                <textarea id='blogContent' name="blogContent" class="form-control textEditor" value="{{Input::old('blogContent')}}"></textarea>
                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success cm-btn-fixer pull-right']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('errors.list')
</div>
@endsection