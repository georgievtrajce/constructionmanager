@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.Blog')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/blog')}}" class="cm-trail-link">{{trans('labels.Blog')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/blog/'.$blogPost->id.'/edit')}}" class="cm-trail-link">{{trans('labels.blog.edit')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'blog'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif
                        {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project->id.'/blog/'.$blogPost->id]) !!}
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::label('title', trans('labels.blog.title').':') !!}
                                {!! Form::text('title', $blogPost->title, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! Form::text('blogContent', $blogPost->content, ['class' => 'form-control textEditor', 'id'=>'blogContent']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save',['class' => 'btn btn-success pull-right']) !!}
                        </div>
                        {!! Form::close() !!}
                        @include('errors.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection