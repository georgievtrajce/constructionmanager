@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <header class="cm-heading">
        {{trans('labels.Blog')}}

    <ul class="cm-trail">
        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
        <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/blog')}}" class="cm-trail-link">{{trans('labels.Blog')}}</a></li>
    </ul>
      </header>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        <div class="cm-btn-group cf">
    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'blog')))
        <a href="{{URL('/projects/'.$project->id.'/blog/create')}}" class="btn btn-success pull-right">{{trans('labels.blog.create')}}</a>
    @endif
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'blog'))
    </div>
  </div>

    <div class="panel">
        <div class="panel-body">
            <div class="row">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                @include('projects.blog.partials.posts')
            </div>
        </div>
    </div>
@endsection
@include('popups.delete_record_popup')