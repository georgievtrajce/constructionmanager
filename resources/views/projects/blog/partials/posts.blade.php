@if (!sizeof($blogPosts))
<div class="col-md-12 text-center">
  <span class="">{{trans('labels.no_blog_posts')}}</span>
</div>
@else
@foreach ($blogPosts as $post)
<div class="cf">
<div class="col-md-6 col-md-offset-3">
  <div class="cm-blog" data-equal-height>
    <div class="cf">
      <p class="cm-blog-heading mb0 pull-left">
        <span>{{trans('labels.by')}}
          <span class="cm-blog-emphasis">
            @if(!is_null($post->author))
            {{$post->author->name}}
            @else
            @if (isset($author) && isset($author->name)){{$author->name}}@endif
            @endif
          </span>
        </span>
      </p>
      <p class="cm-blog-heading mb0 pull-right">{{trans('labels.blog.created_at')}}: <span class="cm-blog-emphasis"> @if (isset($post->created_at)) {{Carbon::parse($post->created_at)->format('m/d/Y h:i:s')}} @endif</span>
    </p>
    </div>
      <hr>
    <h3 class="cm-blog-title">@if (isset($post->title)){{$post->title}}@endif</h3>
    <p>@if(isset($post->content)){!!  $post->content !!}@endif</p>
    <div class="cm-spacer-normal"></div>
    <div class="cm-blog-footer">
      <hr>
      @if ($post->readMore)
      <a href="{{URL('/projects/'.$post->proj_id.'/blog/'.$post->id)}}" class="btn btn-sm btn-primary pull-right">{{trans('labels.readMore')}}</a>
      @endif
      <div class="cm-btn-group cf">
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'rfis')))
        {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL('/projects/'.$project->id.'/blog/'.$post->id)]) !!}
        {{--{!! Form::submit(trans('labels.delete'),['class'=>'btn btn-sm btn-danger pull-left cm-heading-btn', 'onclick' => 'return confirm("'.trans('labels.blog.delete_modal').'")']) !!}--}}
        <button class='btn btn-sm btn-danger pull-left' style="margin-right: 15px;" type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
        {{trans('labels.delete')}}
        </button>
        {!! Form::close()!!}
        <a href="{{URL('/projects/'.$project->id.'/blog/'.$post->id.'/edit')}}" class="btn btn-sm btn-default pull-left">{{trans('labels.blog.edit')}}</a>
        @endif
      </div>
    </div>
  </div>
</div>
</div>

@endforeach
@endif
<div class="col-md-12">
  <div class="pull-right">
    <?php echo $blogPosts->appends([
    'id'=>Input::get('id'),
    ])->render(); ?>
  </div>
</div>

<!-- popup -->
<div class="product-image-overlay">
  <span class="product-image-overlay-close">x</span>
  <img src="" />
</div>