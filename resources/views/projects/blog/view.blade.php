@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <header class="cm-heading">
        {{trans('labels.Blog')}}
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/blog')}}" class="cm-trail-link">{{trans('labels.Blog')}}</a></li>
        </ul>
      </header>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        <div class="cm-btn-group cf">
          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'blog')))
          <a href="{{URL('/projects/'.$project->id.'/blog/create')}}" class="btn btn-success pull-right">{{trans('labels.blog.create')}}</a>
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'blog'))
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <div class="cm-blog">
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                <div>
                <p class="cm-blog-heading">
                    {{trans('labels.by')}}
                    <span class="cm-blog-emphasis">
                      @if(!is_null($blogPost->author))
                      {{$blogPost->author->name}}
                      @else
                      @if (isset($author) && isset($author->name)){{$author->name}}@endif
                      @endif
                    </span>
                  </p>
                  <p class="cm-blog-heading">{{trans('labels.blog.created_at')}}: <span class="cm-blog-emphasis">@if (isset($blogPost->created_at)) {{$blogPost->created_at}} @endif </span></p>
                  <h2 class="cm-blog-title">@if (isset($blogPost->title)){{$blogPost->title}}@endif</h2>
                  <p>@if(isset($blogPost->content)){!! $blogPost->content !!}@endif</p>

                  <div class="cm-spacer-normal"></div>
                  <div class="cm-btn-group cf">
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'rfis')))
                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL('/projects/'.$project->id.'/blog/'.$blogPost->id)]) !!}
                    {{--{!! Form::submit(trans('labels.delete'),['class'=>'btn btn-sm btn-danger pull-left cm-heading-btn', 'onclick' => 'return confirm("'.trans('labels.blog.delete_modal').'")']) !!}--}}
                    <button class='btn btn-sm btn-danger pull-left' style="margin-right: 15px;" type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                    {{trans('labels.delete')}}
                    </button>
                    {!! Form::close()!!}
                    <a href="{{URL('/projects/'.$project->id.'/blog/'.$blogPost->id.'/edit')}}" class="btn btn-sm btn-default pull-left">{{trans('labels.blog.edit')}}</a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @include('popups.delete_record_popup')
  @endsection