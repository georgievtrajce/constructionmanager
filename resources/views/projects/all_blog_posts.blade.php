@extends('layouts.tabs')
@section('title')
    {{trans('labels.all_blog_posts')}}
    @if ($project->comp_id == Auth::user()->comp_id && Auth::user()->hasRole('Company Admin'))
        <a href="{{URL('projects/'.$project->id.'/edit')}}" class="btn btn-primary pull-right">{{trans('labels.project.edit')}}</a>
    @elseif(Auth::user()->hasRole('Company User'))

    @elseif(Permissions::can('write', 'projects'))
        <a href="{{URL('projects/'.$project->id.'/subcontractor/project-info/edit')}}" class="btn btn-primary pull-right">{{trans('labels.project.edit')}}</a>
    @endif
    <ul class="cm-trail">
        <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
    </ul>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'projects'))
@endsection
@section('tabsContent')
<div class="panel">
    <div class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cm-heading">
                    <small class="cm-heading-sub">{{trans('labels.total_records', ['number'=> $blogPosts->total()])}}</small>
                </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="btn btn-info btn-sm" href="{{URL('projects/'.$project->id)}}">
                    <span class="glyphicon glyphicon-step-backward"></span>
                    {{trans('labels.back')}}
                </a>
            </div>
        </div>
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                @if (sizeof($blogPosts))
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>{{trans('labels.blog.title')}}</th>
                            <th>{{trans('labels.project.name')}}</th>
                            <th>{{trans('labels.blog.published')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($blogPosts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{{$project->name}}</td>
                                <td>{{Carbon::parse($post->created_at)->format('m/d/Y')}}</td>
                                <td>
                                    @if(!is_null($post->project))
                                        <a class="btn btn-sm btn-primary" href="{{URL('/projects/'.$post->project->id.'/blog/'.$post->id)}}">{{trans('labels.readMore')}}</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
                @else
                    <div>
                        <p class="text-center">{{trans('labels.no_records')}}</p>
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-12">
                    <div class="pull-right">
                        <?php echo $blogPosts->render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
