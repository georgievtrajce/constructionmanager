<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cloud PM</title>
</head>
<body>
    <table width="100%" style="margin: 0px; padding: 0px;">
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table width="100%">
                    <tr>
                        <td style="width: 260px; vertical-align: bottom;">
                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                            @if(count(Auth::user()->company->addresses))
                                {{Auth::user()->company->addresses[0]->street}}<br>
                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                            @endif
                        </td>
                        <td style="width: 250px; vertical-align: bottom;">
                            {{Auth::user()->email}}<br>
                            {{Auth::user()->office_phone}}
                        </td>
                        <td style="float: right; width: 180px; vertical-align: bottom;">
                            <h2 style="float: left; margin: 0px; text-align: right;">{{'Submittal'}}</h2>
                            <p style="float: left; margin: 0px; text-align: right;">{{'No: '.$version->submittal->number}}</p>
                            <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="border-bottom: 3px solid black; width: 100%; margin: 0px; padding: 0px;">
                <table width="100%" style="margin: 0px; padding: 0px;">
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'Project: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            {{$version->submittal->project->name}}
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'Project Number: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{$version->submittal->project->number}}
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <b>{{'To: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 250px;">
                            @if($dateType == Config::get('constants.transmittal_date_type.sent_appr'))
                                @if(!is_null($version->submittal->recipient))
                                    @if(!is_null($version->submittal->recipient_contact))
                                        {{$version->submittal->recipient_contact->name}}<br>
                                        {{$version->submittal->recipient_contact->title}}<br>
                                    @endif
                                    {{$version->submittal->recipient->name}}<br>
                                    @if(!is_null($version->submittal->recipient_contact))
                                        @if(!is_null($version->submittal->recipient_contact->office))
                                            {{$version->submittal->recipient_contact->office->street}}<br>
                                            {{$version->submittal->recipient_contact->office->city.', '.$version->submittal->recipient_contact->office->state.' '.$version->submittal->recipient_contact->office->zip}}<br>
                                        @endif
                                        {{$version->submittal->recipient_contact->office_phone}}<br>
                                        {{$version->submittal->recipient_contact->email}}<br>
                                    @endif
                                @else
                                    {{'Unknown'}}
                                @endif
                            @else
                                @if(!is_null($version->submittal->subcontractor))
                                    @if(!is_null($version->submittal->subcontractor_contact))
                                        {{$version->submittal->subcontractor_contact->name}}<br>
                                        {{$version->submittal->subcontractor_contact->title}}<br>
                                    @endif
                                    {{$version->submittal->subcontractor->name}}<br>
                                    @if(!is_null($version->submittal->subcontractor_contact))
                                        @if(!is_null($version->submittal->subcontractor_contact->office))
                                            {{$version->submittal->subcontractor_contact->office->street}}<br>
                                            {{$version->submittal->subcontractor_contact->office->city.', '.$version->submittal->subcontractor_contact->office->state.' '.$version->submittal->subcontractor_contact->office->zip}}<br>
                                        @endif
                                        {{$version->submittal->subcontractor_contact->office_phone}}<br>
                                        {{$version->submittal->subcontractor_contact->email}}<br>
                                    @endif
                                @else
                                    {{'Unknown'}}
                                @endif
                            @endif
                        </td>
                        <td style="vertical-align: top; width: 150px; text-align: right;">
                            <b>{{'From: '}}</b>
                        </td>
                        <td style="vertical-align: top; width: 200px;">
                            {{Auth::user()->name}}<br>
                            {{Auth::user()->title}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 345px;">
                            <p>
                                <b>{{'Subject: '}}</b>
                                {{$version->submittal->subject}}
                            </p>
                            <p>
                                <b>{{'Submitted For: '}}</b>
                                {{$version->submitted_for}}
                            </p>
                        </td>
                        <td style="width: 345px;">
                            <p style="text-align: right;">
                                <b>{{'Sent Via: '}}</b>
                                {{$version->submittal->sent_via}}
                            </p>
                            <p style="text-align: right;">
                                <b>{{'Status: '}}</b>
                                @if(!is_null($version->status))
                                    {{$version->status->name}}
                                @endif
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table>
                    <tr>
                        <td style="width: 180px;"><b>{{'Submittal No: '}}</b></td>
                        <td>{{$version->submittal->number}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Cycle No: '}}</b></td>
                        <td>{{$version->cycle_no}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'MF Number & Title: '}}</b></td>
                        <td>{{$version->submittal->mf_number.' - '.$version->submittal->mf_title}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Subcontractor/Supplier: '}}</b></td>
                        <td>
                            @if ($version->submittal->self_performed)
                                {{Auth::user()->company->name}}
                            @else
                                @if(!is_null($version->submittal->subcontractor))
                                    {{$version->submittal->subcontractor->name}}
                                @else
                                    {{'Unknown'}}
                                @endif
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'General Contractor: '}}</b></td>
                        <td>
                            @if($version->submittal->project->make_me_gc == 1 && $version->submittal->project->company)
                                {{$version->submittal->project->company->name}}
                            @elseif($version->submittal->make_me_gc === 0 && $version->submittal->project->make_me_gc === 0)
                                @if(!is_null($version->submittal->general_contractor))
                                    {{$version->submittal->general_contractor->name}}
                                @else
                                    {{'Unknown'}}
                                @endif
                            @else
                                {{Auth::user()->company->name}}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Submittal Type: '}}</b></td>
                        <td>{{$version->submittal->submittal_type}}</td>
                    </tr>
                    <tr>
                        <td style="width: 180px;"><b>{{'Quantity: '}}</b></td>
                        <td>{{$version->submittal->quantity}}</td>
                    </tr>
                </table>
            </td>
        </tr>
        @if (!empty($dateType) && $dateType == 'sent_appr')
        <tr>
            <td style="width: 100%;">
                <table>
                    <tr>
                        <td style="width: 180px;"><br/><b>{{trans('labels.submittals.notes')}}:</b></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        @endif
        @if (!empty($dateType) && $dateType == 'subm_sent_sub')
        <tr>
            <td style="width: 100%;">
                <table>
                    <tr>
                        <td style="width: 180px;"><br/><b>{{trans('labels.submittals.notes')}}:</b></td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        @endif
    </table>
    <div style="margin-left:5px;">
        @if (!empty($dateType) && $dateType == 'sent_appr')
            {!! $version->appr_notes !!}
        @elseif (!empty($dateType) && $dateType == 'subm_sent_sub')
            {!! $version->subc_notes !!}
        @endif
    </div>
</body>
</html>