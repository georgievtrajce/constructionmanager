@extends('layouts.tabs')
@section('title')
    <div class="row">
        <div class="col-md-7">
            <header class="cm-heading">
                {{trans('labels.submittals.edit_submittal')}}
                <small class="cm-heading-suffix">{{trans('labels.submittal_global').': '.$submittal->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/submittals" class="cm-trail-link">{{trans('labels.submittals.project_submittals')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/submittals/{{$submittal->id}}/edit" class="cm-trail-link">{{trans('labels.submittals.edit_submittal')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-5">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
                || (Permissions::can('write', 'tasks')))
                    <a class="btn btn-success cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'3'.'&module_item='.$submittal->id.'&title='.$submittal->name.'&mf_number='.$submittal->mf_number.'&mf_title='.$submittal->mf_title.'&mf_number_title='.$submittal->mf_number.' - '.$submittal->mf_title}}">{{trans('labels.tasks.create')}}</a>                @endif
            </div>
        </div>
    </div>
@endsection
@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'submittals'))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            <div style="margin-bottom: 60px;">
                <div class="col-md-12">
                    @include('projects.partials.tabs-inner', array('activeTab' =>  Input::get('activeInnerTab', 'files')))
                </div>
            </div>
            <div>
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                <div class="tab-content">
                    <div id="info" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'info') in active @endif">
                        <div id="transmittal_info_cont" class="mt20">
                            {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/submittals/'.$submittal->id), 'id' => 'submittals', 'class' => 'cf', 'role' => 'form']) !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div>
                                <div class="row box-cont-1">
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Submittal Info</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required cm-control-required">{{trans('labels.submittals.name')}}</label>
                                                                        <input type="text" class="form-control" name="name" value="{{$submittal->name}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required  control-label">{{trans('labels.submittals.version_no')}}</label>
                                                                        <input type="text" readonly class="form-control" id="item-number" name="number" value="{{$submittal->number}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a href="javascript:;" id="custom-number" class="btn btn-primary pull-right">
                                                                            {{trans('labels.submittals.enter_version_number_custom')}}
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                                                        <input type="text" class="cm-control-required form-control mf-number-title-auto"
                                                                               name="master_format_search"
                                                                               value="{{ old('master_format_search') }}">
                                                                        <input type="hidden" class="master-format-id" name="master_format_id"
                                                                               value="{{old('master_format_id')}}">
                                                                        <input type="hidden" id="project_id" value="{{$project->id}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <div class="form-group mf-cont">
                                                                        <label class="control-label">&nbsp;</label>
                                                                        <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right">
                                                                            {{trans('labels.submittals.enter_version_number_custom')}}
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required control-label">{{trans('labels.spec_number')}}</label>
                                                                        <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{$submittal->mf_number}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <div class="form-group">
                                                                        <label class="cm-control-required control-label">{{trans('labels.spec_title')}}</label>
                                                                        <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{$submittal->mf_title}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <?php
                                                                    $subcontractor_notification = false; $subcontractorId = null;
                                                                    $recipient_notification = false; $recipientId = null;
                                                                ?>
                                                                <div class="col-md-7">
                                                                    <div class="form-group" id="subcontractor_container" @if($submittal->self_performed) {{'style=display:none;'}} @endif>
                                                                        <label>{{trans('labels.submittals.import_supplier_from_companies')}}</label>
                                                                        @if(!is_null($submittal->subcontractor) && $submittal->sub_id != 0)
                                                                            <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="{{$submittal->subcontractor->name}}">
                                                                            <input type="hidden" class="address-book-id" name="sub_id" value="{{$submittal->sub_id}}">
                                                                        @else
                                                                            <input type="text" class="form-control address-book-auto" name="supplier_subcontractor" value="">
                                                                            <input type="hidden" class="address-book-id" name="sub_id" value="">
                                                                        @endif
                                                                    </div>
                                                                    <div class="form-group" id="notif_subcontractor" @if(!$subcontractor_notification) style="display: none;" @endif>
                                                                        <input type="hidden" id="notif_subcontractor_id" name="notif_subcontractor_id" value="{{$subcontractorId}}">
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <input type="checkbox" name="self_performed" id="self_performed" @if($submittal->self_performed) {{'checked'}} @endif> {{trans('labels.submittals.self_performed')}}
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    @if(!is_null($submittal->subcontractor) && $submittal->sub_id != 0)
                                                                        <div class="form-group subcontractor_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <label>Select Contact:</label>
                                                                                    <select name="sub_contact" id="sub_contact">
                                                                                        <option value=""></option>
                                                                                        @if(count($projectSubcontractorContacts))
                                                                                            @foreach($projectSubcontractorContacts as $contact)
                                                                                                <?php $subcontractor_notification = true; if($submittal->sub_contact_id == $contact->id) $subcontractorId = $contact->id; ?>
                                                                                                <option value="{{$contact->id}}" @if($submittal->sub_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                            @endforeach
                                                                                        @else
                                                                                            <option value="">No available contacts for this company</option>
                                                                                        @endif
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @else
                                                                        <div class="form-group subcontractor_contacts_container col-sm-12 mt15" style="margin-bottom: 10px;"></div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <div class="form-group">
                                                                        <label>
                                                                            {{trans('labels.submittals.import_recipient_from_companies')}}
                                                                        </label>
                                                                        <?php
                                                                            $recipientValue = '';
                                                                            $recipientIdValue = '';
                                                                            if (!is_null($submittal->recipient) && $submittal->recipient_id != 0 && count($projectRecipientContacts) > 0) {
                                                                                $recipientValue = $submittal->recipient->name;
                                                                                $recipientIdValue = $submittal->recipient_id;
                                                                            } else if(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0) {
                                                                                $recipientValue = $project->architect->name;
                                                                                $recipientIdValue = $project->architect->id;
                                                                            } else if(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0) {
                                                                                $recipientValue = $project->primeSubcontractor->name;
                                                                                $recipientIdValue = $project->primeSubcontractor->id;
                                                                            } else if(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0) {
                                                                                $recipientValue = $project->owner->name;
                                                                                $recipientIdValue = $project->owner->id;
                                                                            } else if(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0) {
                                                                                $recipientValue = $project->generalContractor->name;
                                                                                $recipientIdValue = $project->generalContractor->id;
                                                                            }
                                                                        ?>
                                                                            <input type="text" class="form-control address-book-recipient-auto" name="recipient" value="{{$recipientValue}}">
                                                                            <input type="hidden" class="address-book-recipient-id" name="recipient_id" value="{{$recipientIdValue}}">
                                                                    </div>
                                                                    <hr>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    @if(!is_null($submittal->recipient) && $submittal->recipient_id != 0 && count($projectRecipientContacts) > 0)
                                                                        @if(!is_null($submittal->recipient))
                                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Select Contact:</label>
                                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                                            <option value=""></option>
                                                                                            @if(count($projectRecipientContacts))
                                                                                                @foreach($projectRecipientContacts as $contact)
                                                                                                    <?php $recipient_notification = true; if($submittal->recipient_contact_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                                    <option value="{{$contact->id}}" @if($submittal->recipient_contact_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                                @endforeach
                                                                                            @else
                                                                                                <option value="">No available contacts for this company</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @elseif(isset($project->architect->name) && isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                                        @if(isset($project->architectTransmittal->name) && count($architectProjectTransmittals) > 0)
                                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Select Contact:</label>
                                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                                            <option value=""></option>
                                                                                            @if(count($architectProjectTransmittals))
                                                                                                @foreach($architectProjectTransmittals as $contact)
                                                                                                    <?php $recipient_notification = true; if($project->architect_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                                    <option value="{{$contact->id}}" @if($project->architect_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                                @endforeach
                                                                                            @else
                                                                                                <option value="">No available contacts for this company</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @elseif(isset($project->primeSubcontractor->name) && isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                                        @if(isset($project->primeSubcontractorTransmittal->name) && count($primeSubcontractorProjectTransmittals) > 0)
                                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Select Contact:</label>
                                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                                            <option value=""></option>
                                                                                            @if(count($primeSubcontractorProjectTransmittals))
                                                                                                @foreach($primeSubcontractorProjectTransmittals as $contact)
                                                                                                    <?php $recipient_notification = true; if($project->prime_subcontractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                                    <option value="{{$contact->id}}" @if($project->prime_subcontractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                                @endforeach
                                                                                            @else
                                                                                                <option value="">No available contacts for this company</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @elseif(isset($project->owner->name) && isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                                        @if(isset($project->ownerTransmittal->name) && count($ownerProjectTransmittals) > 0)
                                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Select Contact:</label>
                                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                                            <option value=""></option>
                                                                                            @if(count($ownerProjectTransmittals))
                                                                                                @foreach($ownerProjectTransmittals as $contact)
                                                                                                    <?php $recipient_notification = true; if($project->owner_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                                    <option value="{{$contact->id}}" @if($project->owner_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                                @endforeach
                                                                                            @else
                                                                                                <option value="">No available contacts for this company</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @elseif(isset($project->generalContractor->name) && isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                                        @if(isset($project->contractorTransmittal->name) && count($contractorProjectTransmittals) > 0)
                                                                            <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;">
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <label>Select Contact:</label>
                                                                                        <select name="recipient_contact" id="recipient_contact">
                                                                                            <option value=""></option>
                                                                                            @if(count($contractorProjectTransmittals))
                                                                                                @foreach($contractorProjectTransmittals as $contact)
                                                                                                    <?php $recipient_notification = true; if($project->contractor_transmittal_id == $contact->id) $recipientId = $contact->id; ?>
                                                                                                    <option value="{{$contact->id}}" @if($project->contractor_transmittal_id == $contact->id) {{'selected'}} @endif>{{$contact->name}}</option>
                                                                                                @endforeach
                                                                                            @else
                                                                                                <option value="">No available contacts for this company</option>
                                                                                            @endif
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endif
                                                                    @else
                                                                        <div class="recipient_contacts_container col-sm-12" style="margin-bottom: 10px;"></div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="mb5">
                                                                            {{trans('labels.submittals.import_general_contractor')}}
                                                                        </label>
                                                                        <div class="row">
                                                                            <div class="col-md-12 general_contractor_container">
                                                                                @if($project->make_me_gc && isset($project->company->name))
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->company->name}}" readonly>
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->company->id}}">
                                                                                @elseif(!is_null($submittal->general_contractor) && $submittal->gc_id != 0)
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$submittal->general_contractor->name}}" <?php (!empty($project->general_contractor_id))?'readonly':'' ?>>
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$submittal->gc_id}}">
                                                                                @elseif(isset($project->generalContractor->name))
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="{{$project->generalContractor->name}}">
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="{{$project->generalContractor->id}}">
                                                                                @else
                                                                                    <input type="text" class="form-control address-book-gc-auto" name="general_contractor" value="">
                                                                                    <input type="hidden" class="address-book-gc-id" name="general_contractor_id" value="">
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="box">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4>Transmittal Info</h4>
                                                    <div class="cm-spacer-xs"></div>
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.subject')}}</label>
                                                                        <input type="text" class="form-control" name="subject" value="{{ $submittal->subject }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.sent_via')}}</label>
                                                                        <select class="custom-options" name="sent_via_dropdown" id="sent_via_dropdown" data-id="sent">
                                                                            @foreach($sentVia as $item)
                                                                                <option <?php echo (!empty($submittal->sent_via) && $submittal->sent_via == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                            <option {{ (!empty($submittal->sent_via) && !in_array($submittal->sent_via, $sentVia))?'selected':'' }} value="0">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" class="custom-text-sent form-control {{ (!empty($submittal->sent_via) && !in_array($submittal->sent_via, $sentVia))?'':'hide' }}" name="sent_via" id="sent_via" value="{{ (!in_array($submittal->sent_via, $sentVia))?$submittal->sent_via:'' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.submittal_type')}}</label>
                                                                        <select class="custom-options" name="submittal_type_dropdown" id="submittal_type_dropdown" data-id="submittal">
                                                                            @foreach($submittalType as $item)
                                                                                <option <?php echo (!empty($submittal->submittal_type) && $submittal->submittal_type == $item) ? 'selected' : ''; ?> value="{{$item}}">{{$item}}</option>
                                                                            @endforeach
                                                                            <option {{ (!empty($submittal->submittal_type) && !in_array($submittal->submittal_type, $submittalType))?'selected':'' }} value="0">Custom</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" class="custom-text-submittal form-control {{ (!empty($submittal->submittal_type) && !in_array($submittal->submittal_type, $submittalType))?'':'hide' }}" name="submittal_type" value="{{ (!in_array($submittal->submittal_type, $submittalType))?$submittal->submittal_type:'' }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group">
                                                                        <label>{{trans('labels.submittals.quantity')}}</label>
                                                                        <input type="text" class="form-control" name="quantity" value="{{ $submittal->quantity }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="cm-control control-label">{{trans('labels.submittals.internal_note')}}</label>
                                                                        <textarea class="form-control span12 textEditorSmall" rows="5" name="note">{!! $submittal->note  !!}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <button type="submit" class="btn btn-success pull-right mt20">
                                                                        {{trans('labels.update')}}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" class="project_name" name="project_name" value="{{$project->name}}">
                                                            <div class="cm-spacer-xs"></div>
                                                            <div class="cm-spacer-xs"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close()!!}
                        </div>
                    </div>
                    <div id="files" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'files') in active @endif">
                        <div class="col-md-12 mt20">
                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'submittals')))
                                <a href="{{URL('projects/'.$project->id.'/submittals/'.$submittal->id.'/version/create')}}" class="btn btn-primary">{{trans('labels.submittals.add_new_version')}}</a>
                            @endif
                            <div class="pull-right">
                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'submittals')))
                                    @if(count($submittal->submittals_versions) > 1)
                                        {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                        <input type="hidden" value="{{URL('projects/'.$project->id.'/submittals/'.$submittal->id.'/version').'/'}}" id="form-url" />
                                        <button data-limit="1" disabled id="delete-button" class='btn btn-xs btn-danger pull-right ' type='submit' data-toggle="modal"
                                                data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                            {{trans('labels.delete')}}
                                        </button>
                                        {!! Form::close()!!}
                                    @endif
                                @endif
                            </div>
                            @if($submittal->submittals_versions->count() != 0)
                                <div class="cm-spacer-xs"></div>
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered cm-table-compact">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>{{trans('labels.submittals.version_no')}}</th>
                                            <th>{{trans('labels.submittals.name')}}</th>
                                            <th>{{trans('labels.submittals.cycle_no')}}</th>
                                            <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                            <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                            <th>{{trans('labels.status')}}</th>
                                            <th>{{trans('labels.files.download')}}</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php $submittalVersions = $submittal->submittals_versions;

                                        ?>
                                        @for ($i=0; $i < count($submittalVersions); $i++)
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" class="multiple-items-checkbox" data-id="{{$submittalVersions[$i]->id}}">
                                                </td>
                                                <td>{{$submittal->number}}</td>
                                                <td>
                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'submittals')))
                                                        <a href="{{URL('projects/'.$project->id.'/submittals/'.$submittal->id.'/version/'.$submittalVersions[$i]->id.'/edit')}}"
                                                           class="">{{$submittal->name}}</a>
                                                    @else
                                                        {{$submittal->name}}
                                                    @endif
                                                </td>
                                                <td>{{$submittalVersions[$i]->cycle_no}}</td>
                                                <td>
                                                    @if($submittalVersions[$i]->rec_sub != 0)
                                                        {{date("m/d/Y", strtotime($submittalVersions[$i]->rec_sub))}}
                                                        @if($submittalVersions[$i]->sent_appr != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->sent_appr))) - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_sub)));

                                                            ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_sub)));

                                                            ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($submittalVersions[$i]->sent_appr != 0)
                                                        {{date("m/d/Y", strtotime($submittalVersions[$i]->sent_appr))}}
                                                        @if($submittalVersions[$i]->rec_appr != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_appr))) - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->sent_appr)));

                                                            ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->sent_appr)));

                                                            ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($submittalVersions[$i]->rec_appr != 0)
                                                        {{date("m/d/Y", strtotime($submittalVersions[$i]->rec_appr))}}
                                                        @if($submittalVersions[$i]->rec_sub == 0 && !empty($submittalVersions[$i+1]) && $submittalVersions[$i+1]->sent_appr != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($submittalVersions[$i+1]->sent_appr))) - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_appr)));

                                                            ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @else
                                                            @if($submittalVersions[$i]->subm_sent_sub != 0)
                                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_appr)));

                                                                ?>
                                                            @else
                                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->rec_appr)));

                                                                ?>
                                                            @endif
                                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!is_null($submittalVersions[$i]->subm_sent_sub) && $submittalVersions[$i]->subm_sent_sub != 0)
                                                        {{date("m/d/Y", strtotime($submittalVersions[$i]->subm_sent_sub))}}
                                                        @if(!empty($submittalVersions[$i+1]) && $submittalVersions[$i+1]->rec_sub != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($submittalVersions[$i+1]->rec_sub))) - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->subm_sent_sub)));

                                                            ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @else
                                                            @if(!in_array($submittalVersions[$i]->status->short_name, ['APP','AAN']))
                                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($submittalVersions[$i]->subm_sent_sub)));

                                                                ?> ({{floor($dateDiff / (60 * 60 * 24))}})
                                                            @endif
                                                        @endif
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(!is_null($submittalVersions[$i]->status))
                                                        {{$submittalVersions[$i]->status->short_name}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if(count($submittalVersions[$i]->transmittalSubmSentFile) > 0 || count($submittalVersions[$i]->transmittalSentFile) > 0)
                                                        @if(count($submittalVersions[$i]->transmittalSubmSentFile) > 0 &&
                                                           !empty($submittalVersions[$i]->subm_sent_sub) &&
                                                           $submittalVersions[$i]->subm_sent_sub != '0000-00-00')
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$submittalVersions[$i]->transmittalSubmSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/submittals/transmittals/'.$submittalVersions[$i]->transmittalSubmSentFile[0]->file_name}}">
                                                            </p>
                                                        @elseif(count($submittalVersions[$i]->transmittalSentFile) > 0 &&
                                                            !empty($submittalVersions[$i]->sent_appr) &&
                                                            $submittalVersions[$i]->sent_appr != '0000-00-00' &&
                                                           (empty($submittalVersions[$i]->subm_sent_sub) ||
                                                           $submittalVersions[$i]->subm_sent_sub == '0000-00-00') &&
                                                           (empty($submittalVersions[$i]->rec_appr) ||
                                                           $submittalVersions[$i]->rec_appr == '0000-00-00'))
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$submittalVersions[$i]->transmittalSentFile[0]->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/submittals/transmittals/'.$submittalVersions[$i]->transmittalSentFile[0]->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endif
                                                    @if($submittalVersions[$i]->download_file)
                                                        @if(!is_null($submittalVersions[$i]->file) && count($submittalVersions[$i]->file) > 0)
                                                            @foreach($submittalVersions[$i]->file as $file)
                                                                @if($file->version_date_connection == $submittalVersions[$i]->file_status)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/submittals/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </td>

                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p class="text-center">{{trans('labels.no_records')}}</p>
                            @endif
                        </div>
                    </div>
                    <div id="tasks" class="tab-pane fade @if(Input::get('activeInnerTab', 'files') == 'tasks') in active @endif">
                        <div class="col-md-12">
                            @if(((Auth::user()->hasRole('Company Admin')) || Auth::user()->hasRole('Project Admin') || $userCanDelete))
                                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                <input type="hidden" value="{{url('tasks')}}/" id="form-url" />
                                <a href="#" disabled id="delete-button" class='btn btn-danger pull-right cm-btn-fixer delete-button-multi' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                    {{trans('labels.delete')}}
                                </a>
                                {!! Form::close()!!}
                            @endif
                            @if (Session::has('flash_notification.message'))
                                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('flash_notification.message') }}
                                </div>
                            @endif
                            @include("tasks.partials.table")
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('popups.delete_record_popup')
@endsection


