<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Cloud PM</title>
</head>
<body>

    <table width="100%">
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table width="100%">
                    <tr>
                        <td style="width: 380px; vertical-align: bottom;">
                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                            @if(count(Auth::user()->company->addresses))
                                {{Auth::user()->company->addresses[0]->street}}<br>
                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                            @endif
                        </td>
                        <td style="width: 280px; vertical-align: bottom;">
                            {{Auth::user()->email}}<br>
                            {{Auth::user()->office_phone}}
                        </td>
                        <td style="float: right; vertical-align: bottom;">
                            <h2 style="float: left; margin: 0px; text-align: right;">{{'Transmittal Log'}}</h2>
                            <h2 style="float: left; margin: 0px; text-align: right;">{{"Submittals"}}</h2>
                            <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; border-bottom: 3px solid black;">
                <table width="100%">
                    <tr>
                        <td style="vertical-align: top; width: 465px;">
                            <b>{{'Project: '}}</b>
                            {{$project->name}}
                        </td>
                        <td style="vertical-align: top; width: 390px; text-align: right;">
                            <b>{{'Project Number: '}}</b>
                            {{$project->number}}
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%; font-size: 11px;">
                @if(count($transmittals))
                    <table width="100%">
                        <thead>
                            <tr>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.no')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.submittals.cycle_no')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.subject')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.mf_number')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.mf_title')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.sent_to')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.transmittals.date_sent')}}</th>
                                <th style="text-align: left; padding: 5px;">{{trans('labels.status')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($transmittals as $item)
                            <tr>
                                <td style="padding: 5px; vertical-align: top;">{{$item->number}}</td>
                                <td style="padding: 5px; vertical-align: top;">{{$item->version_cycle_no}}</td>
                                <td style="padding: 5px; vertical-align: top;">{{$item->subject}}</td>
                                <td style="padding: 5px; vertical-align: top;">{{$item->mf_number}}</td>
                                <td style="padding: 5px; vertical-align: top;">{{$item->mf_title}}</td>
                                <td style="padding: 5px; vertical-align: top;">{{$item->sent_to}}</td>
                                <td style="padding: 5px; vertical-align: top;">
                                    @if($item->date_sent > 0)
                                        {{date("m/d/Y", strtotime($item->date_sent))}}
                                    @endif
                                </td>
                                <td style="padding: 10px; vertical-align: top;">{{$item->version_status}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    {{trans('labels.no_records')}}
                @endif
            </td>
        </tr>
    </table>
</body>
</html>