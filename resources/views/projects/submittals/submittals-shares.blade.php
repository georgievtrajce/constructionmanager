@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.submittals.project_submittals')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/submittals/')}}" class="cm-trail-link">{{trans('labels.submittals.project_submittals')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'submittals')))
                <a href="{{URL('projects/'.$project->id.'/submittals/create')}}" class="btn btn-success pull-right">{{trans('labels.submittals.create')}}</a>
                @endif
                <div class="pull-left">
                {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>URL(''), 'id' => 'unshare-form']) !!}
                    <input type="hidden" value="{{URL('projects/'.$project->id.'/submittals').'/'}}" id="form-url" />
                    <button disabled id="unshare-button" class='btn cm-btn-secondary pull-right mr5' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.project.unshare_modal')}}'>
                        {{trans('labels.unshare')}}
                    </button>
                {!! Form::close()!!}
            </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'submittals'))
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <ul class="nav nav-pills">
                        <li class="active dropdown">
                            <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                {{trans('labels.select')}}
                                <span class="caret"></span>
                            </a>
                            <ul role="group" class="dropdown-menu">
                                <li>
                                    <a href="{{URL('projects/'.$project->id.'/submittals/')}}">{{trans('labels.submittals.created')}}
                                    </a>
                                </li>
                                <li>
                                    <a href="{{URL('projects/'.$project->id.'/submittals/shares')}}">{{trans('labels.submittals.shares')}}
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <?php echo $sharedSubmittals->render();?>
                    </div>
                </div>
            </div>
            <div class="cm-spacer-normal"></div>
            <div class="container-fluid">
                <div class="row">
                    <!-- <h3>{{trans('labels.submittals.submittals_shares')}}</h3> -->
                    @if(count($sharedSubmittals) > 0)
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>{{trans('labels.submittals.version_no')}}</th>
                                    <th>{{trans('labels.submittals.cycle_no')}}</th>
                                    <th>{{trans('labels.master_format_number_and_title')}}</th>
                                    <th>{{trans('labels.submittals.name')}}</th>
                                    <th>{{trans('labels.supplier_subcontractor')}}</th>
                                    <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                    <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                    <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                    <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                    <th>{{trans('labels.status')}}</th>
                                    <th>{{trans('labels.submittals.shared_with')}}</th>
                                    <th>{{trans('labels.File')}}</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sharedSubmittals as $item)
                                <tr>
                                    <td class="text-center">
                                        <input type="checkbox" class="multiple-items-checkbox-unshare"
                                            data-id="{{$item->id}}"
                                            data-comp-parent-id="{{$item->comp_parent_id}}"
                                            data-comp-child-id="{{$item->comp_child_id}}" >
                                    </td>
                                    <td>{{$item->number}}</td>
                                    <td>{{$item->version_cycle_no}}</td>
                                    <td>
                                        @if (isset($item->mf_number) && isset($item->mf_title))
                                        {{$item->mf_number.' - '.$item->mf_title}}
                                        @endif
                                    </td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        {{($item->self_performed ? Auth::user()->company->name : $item->subcontractor_name)}}
                                    </td>
                                    <td>@if($item->version_rec_sub != 0){{date("m/d/Y", strtotime($item->version_rec_sub))}}@endif</td>
                                    <td>@if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif</td>
                                    <td>@if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif</td>
                                    <td>@if($item->version_subm_sent_sub != 0){{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}@endif</td>
                                    <td>{{$item->version_status_short_name}}</td>
                                    <td>{{$item->company_name}}</td>
                                    <td>
                                        @if(!is_null($item->file_name))
                                        <a class="btn btn-sm btn-primary download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                        <input type="hidden" class="s3FilePath" id="{{$item->file_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/submittals/'.$item->file_name}}">
                                        @endif
                                    </td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <p class="text-center">{{trans('labels.files.no_shared_files')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('popups.unshare_project_popup')
    @endsection