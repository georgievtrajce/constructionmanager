@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                Transmittal Log
                <small class="cm-heading-suffix">{{trans('labels.Project').': '.$project->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/submittals')}}" class="cm-trail-link">{{trans('labels.submittals.project_submittals')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">

                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'submittals')))
                    {!! Form::open(['method'=>'POST', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                    <input type="hidden" value="{{URL('projects/'.$project->id.'/submittals/transmittals').'/'}}" id="form-url" />
                    <button disabled id="delete-button" class='btn btn-danger pull-right cm-btn-fixer' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                        {{trans('labels.delete')}}
                    </button>
                    {!! Form::close()!!}
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'submittals'))
        </div>
    </div>



    <div class="panel">
        <div class="panel-body">

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif

            @if($transmittals->count() != 0)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="cm-filter cm-filter__alt cf cf">
                            <div class="row">
                                <div class="col-sm-7">
                                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/submittals/transmittals/filter']) !!}
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                {!! Form::label('master_format_search',trans('labels.search_by')) !!}
                                                {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto']); !!}
                                                {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id']); !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('sent_to', trans('labels.search')) !!}
                                                {!! Form::text('sent_to',Input::get('sent_to'),['class' => 'form-control address-book-auto']); !!}
                                                {!! Form::hidden('company_id',Input::get('company_id'),['class' => 'address-book-id']); !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('status', trans('labels.status')) !!}
                                                {!! Form::select('status',$statuses,Input::get('status')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="pull-left">
                            {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/submittals/transmittals/report','target'=>'_blank']) !!}

                            {!! Form::hidden('report_sort',Input::get('sort')) !!}
                            {!! Form::hidden('report_order',Input::get('order')) !!}
                            {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                            {!! Form::hidden('report_comp_id',Input::get('company_id')) !!}
                            {!! Form::hidden('report_sent_to',Input::get('sent_to')) !!}
                            {!! Form::hidden('report_status',$status) !!}

                            {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-sm pull-right btn-info']) !!}

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <?php echo $transmittals->render(); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact">
                                <thead>
                                <tr>
                                <th class="text-center">
                                    {{--<input type="checkbox" name="select_all" id="transmittal_select_all">--}}
                                </th>
                                    <th>
                                        <?php
                                        if (Input::get('sort') == 'number' && Input::get('order') == 'desc') {
                                            $url = Request::url().'?sort=number&order=asc';
                                        } else {
                                            $url = Request::url().'?sort=number&order=desc';
                                        }
                                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                        $url .= !empty(Input::get('sent_to'))?'&sent_to='.Input::get('sent_to'):'';
                                        $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                        ?>
                                        {{trans('labels.transmittals.no')}}
                                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>
                                        {{trans('labels.submittals.cycle_no')}}
                                    </th>
                                    <th>
                                        {{trans('labels.transmittals.subject')}}
                                    </th>
                                    <th>
                                        <?php
                                        if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                                            $url = Request::url().'?sort=mf_number&order=desc';
                                        } else {
                                            $url = Request::url().'?sort=mf_number&order=asc';
                                        }
                                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                        $url .= !empty(Input::get('sent_to'))?'&sent_to='.Input::get('sent_to'):'';
                                        $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                        ?>
                                        {{trans('labels.mf_number_and_title')}}
                                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>

                                    <th>
                                        {{trans('labels.transmittals.sent_to')}}
                                        {{--{{trans('labels.supplier_subcontractor')}}--}}
                                    </th>
                                    <th>
                                        <?php
                                        if (Input::get('sort') == 'date_sent' && Input::get('order') == 'asc') {
                                            $url = Request::url().'?sort=date_sent&order=desc';
                                        } else {
                                            $url = Request::url().'?sort=date_sent&order=asc';
                                        }
                                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                        $url .= !empty(Input::get('sent_to'))?'&sent_to='.Input::get('sent_to'):'';
                                        $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                        ?>
                                        {{trans('labels.transmittals.date_sent')}}
                                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>
                                        <?php
                                        if (Input::get('sort') == 'version_status' && Input::get('order') == 'asc') {
                                            $url = Request::url().'?sort=version_status&order=desc';
                                        } else {
                                            $url = Request::url().'?sort=version_status&order=asc';
                                        }
                                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                        $url .= !empty(Input::get('sent_to'))?'&sent_to='.Input::get('sent_to'):'';
                                        $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                        ?>
                                        {{trans('labels.status')}}
                                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($transmittals as $item)
                                    <tr>
                                    <td class="text-center">
                                        <input type="checkbox" class="transmittal multiple-items-checkbox-docs" data-id="{{$item->id}}">
                                    </td>
                                    <td>{{$item->number}}</td>
                                    <td>{{$item->version_cycle_no}}</td>
                                    <td>{{$item->subject}}</td>

                                    <td>{{$item->mf_number}} - {{$item->mf_title}}</td>
                                    <td>{{$item->sent_to}}</td>
                                    <td>
                                        @if($item->date_sent > 0)
                                            {{date("m/d/Y", strtotime($item->date_sent))}}
                                        @endif
                                    </td>
                                    <td>{{$item->version_status}}</td>
                                    <td>
                                        @if(!is_null($item->file))
                                            <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                            <input type="hidden" class="s3FilePath" id="{{$item->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/submittals/transmittals/'.$item->file->file_name}}">
                                        @endif
                                    </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <?php echo $transmittals->appends([
                                    'sort'=>Input::get('sort'),
                                    'order'=>Input::get('order'),
                                    'master_format_search'=>Input::get('master_format_search'),
                                    'master_format_id'=>Input::get('master_format_id'),
                                    'sent_to'=>Input::get('sent_to'),
                                    'company_id'=>Input::get('company_id'),
                                    'status'=>Input::get('status'),
                            ])->render(); ?>
                        </div>
                    </div>
                </div>
            @else
                <p class="text-center">{{trans('labels.no_records')}}</p>
            @endif
        </div>
    </div>
      </div>
    @include('popups.delete_record_popup')
@endsection
