@extends('layouts.master')
@section('content')
<style>
.radio label span.check {
left: 10px!important;
}
</style>
<div class="container">
        <div class="row">
            <div class="col-sm-12">
                @include('projects.partials.tabs', array('activeTab' => 'companies'))
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1 class="cm-heading">
                {{trans('labels.company.permissions')}} <small class="cm-heading-suffix">{{trans('labels.Company')}}: {{(is_null($company->ab_name)) ? 'Unknown' : $company->ab_name}}</small>
                    <ul class="cm-trail">
                        <li class="cm-trail-item">
                            <a href="{{URL('projects/'.$project->id)}}"
                                                     class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a>
                        </li>
                        <li class="cm-trail-item">
                            <a href="{{URL('projects/'.$project->id.'/companies')}}"
                                                     class="cm-trail-link">{{trans('labels.edit')}} {{trans('labels.Project')}} {{trans('labels.company.plural')}}</a>
                        </li>
                        <li class="cm-trail-item active"><a href="{{Request::url()}}" class="cm-trail-link">{{trans('labels.company.permissions')}}</a></li>
                    </ul>
                </h1>
            </div>
            </div>
           <div class="panel">
                <div class="panel-body">
                    <div class="row">
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                <div class="col-md-12">
                    <div class="container-fluid">
                        <div class="row">
                            {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/companies/'.$company->id.'/permissions/update', 'role'=> 'form']) !!}
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="select_all" id="select_all_permissions"> {{trans('labels.select_all')}}
                                    </label>
                                </div>
                                <!--  <h3>{{trans('labels.manage_users.modules')}}</h3> -->
                            @if(count($permissions))
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                        <tr>
                                            <th style="width: 19%;">{{trans('labels.manage_users.module')}}</th>
                                            <th style="width: 30%;">{{trans('labels.manage_users.read')}}</th>
                                            <th style="width: 20%;">{{trans('labels.manage_users.write')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($permissions as $permission)
                                            @if ($permission->entity_type == 1 && $permission->module->name != Config::get('constants.modules.projects') && $permission->module->name != Config::get('constants.modules.blog'))
                                                <?php
                                                    $mainModuleReadClass = '';
                                                    $projectEventClass = $permission->module->name == 'Projects' ? 'project-event ' : '';
                                                    if($permission->module->name == 'Projects' || $permission->module->name == 'Address Book' || $permission->module->name == 'Custom Categories')
                                                    {
                                                        $readPermissionsClass = '';
                                                        $eventPermissionsClass = '';
                                                        $mainModuleEventClass = 'main-module-event ';
                                                    } else {
                                                        $readPermissionsClass = 'read-permission ';
                                                        $eventPermissionsClass = 'event-permissions ';
                                                        $mainModuleEventClass = '';
                                                    }
                                                ?>
                                        <tr>
                                            <td>{{$permission->module->name}}</td>
                                            <td>
                                                <div class="checkbox pull-left">
                                                    <label>
                                                        <input <?php if($permission->module->display_name == Config::get('constants.pcos') || $permission->module->display_name == Config::get('constants.rfis') || $permission->module->display_name == Config::get('constants.submittals')) { ?> data-id="{{$permission->module->display_name}}" id="parentCheckbox-{{$permission->module->display_name}}" <?php } ?> <?php if($permission->module->display_name == Config::get('constants.pcos')){echo 'id="pco_permission"';} ?> <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="parentCheckbox permission mandatory-permissions <?php echo ($permission->module->name == 'Address Book') ? 'address-book-read ' : ''; echo ($permission->module->name == 'Projects') ? 'projects-module ' : ''; echo $readPermissionsClass; echo $mainModuleReadClass; ?>" name="module_read[]" value="{{$permission->module->id}}">
                                                    </label>
                                                </div>
                                                @if($permission->module->display_name == Config::get('constants.pcos'))
                                                    <div class="radio pull-left">
                                                        <label class="radio-inline">
                                                            <input class="pco_permission_type childRadio" data-id="{{$permission->module->display_name}}"  <?php echo ($permission->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) ? 'checked' : ''; ?> type="radio" name="pco_permission_type" value="{{Config::get('constants.pco_permission_type.all_pcos')}}">{{trans('labels.pcos.all_pcos')}}
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input id="own_pcos" class="pco_permission_type childRadio" data-id="{{$permission->module->display_name}}" <?php echo ($permission->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) ? 'checked' : ''; ?> type="radio" name="pco_permission_type" value="{{Config::get('constants.pco_permission_type.his_pcos')}}">{{trans('labels.pcos.own_pcos')}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </td>
                                            <td>
                                                @if($permission->module->display_name == Config::get('constants.pcos') ||
                                                    $permission->module->display_name == Config::get('constants.rfis') ||
                                                    $permission->module->display_name == Config::get('constants.submittals'))
                                                    <div class="checkbox pull-left">
                                                        <?php $extraPcoClass=''; ?>
                                                        @if($permission->module->display_name == Config::get('constants.pcos'))
                                                             <?php $extraPcoClass='childRadioPco'; ?>
                                                        @endif
                                                        <label>
                                                            <input  <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" data-id="{{$permission->module->display_name}}" id="childCheckbox-{{$permission->module->display_name}}" class="{{$extraPcoClass}} childCheckbox permission mandatory-permissions <?php  echo $readPermissionsClass; echo $mainModuleReadClass; ?>" name="module_write[]" value="{{$permission->module->id}}">
                                                            Own {{$permission->module->name}}
                                                        </label>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <p>{{trans('labels.manage_users.permissions_warning')}}</p>
                            @endif
                        </div>
                        <div class="row">
                            <!-- <h3>{{trans('labels.project_files')}}</h3> -->
                            @if (count($permissions))
                            <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact">
                                <thead>
                                    <tr>
                                        <th style="width: 300px;">{{trans('labels.project_files')}}</th>
                                        <th>{{trans('labels.manage_users.read')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($permissions as $permission)
                                    @if ($permission->entity_type == 2 && !in_array($permission->fileType->display_name, ['daily-reports-images', 'daily-reports-files']))
                                    <tr>
                                        <td>{{$permission->fileType->name}}</td>
                                        <td>
                                            <div class="checkbox">
                                                <label>
                                                    <input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="mandatory-permissions permission read-permission" name="project_file_read[]" value="{{$permission->fileType->id}}">
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>

                            </div>
                            @else
                            <p>{{trans('labels.manage_users.permissions_warning')}}</p>
                            @endif
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right">
                                    {{trans('labels.save')}}
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
                </div>
            </div>
</div>

@endsection