<div class="table-responsive">
<table class="table table-hover table-bordered cm-table-compact">
    <thead>
    <tr>
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
        <th></th>
        @endif
        <th>
            <?php
                if (Input::get('sort') == 'name' && Input::get('order') == 'desc') {
                    $url = Request::url().'?sort=name&order=asc';
                } else {
                    $url = Request::url().'?sort=name&order=desc';
                }
            ?>
            {{trans('labels.company.name')}} <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
        </th>
        <th>
            <?php
                if (Input::get('sort') == 'id' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=id&order=desc';
                } else {
                    $url = Request::url().'?sort=id&order=asc';
                }
            ?>
            {{trans('labels.category.plural')}} <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
        </th>
        <th>
            {{trans('labels.project.office')}}
        </th>
        <th>
            {{trans('labels.master_format')}} {{trans('labels.number')}} {{trans('labels.and')}} {{trans('labels.title')}} {{--<a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>--}}
        </th>
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
        <th></th>
        @endif
    </tr>
    </thead>
    <tbody>
    @for($i=0; $i<sizeof($companies); $i++)
        <tr>
            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
            <td class="text-center">
                <input type="checkbox" name="select_all" class="multiple-items-checkbox-companies" data-id="{{$companies[$i]->pc_id}}" data-extid="{{$companies[$i]->ps_id or ''}}">
            </td>
            @endif
            <td>
             @if (count($companies[$i]->contacts))
                @if($companies[$i]->pc_addr_id !== 0)
                    <a href="{{URL(Request::path().'/'.$companies[$i]->id.'/project-company/'.$companies[$i]->pc_id.'/office/'.$companies[$i]->pc_addr_id)}}" class="">
                        @if (isset($companies[$i]->name))
                            {{$companies[$i]->name}}
                        @endif
                    </a>
                @else
                    <a href="{{URL(Request::path().'/'.$companies[$i]->id.'/project-company/'.$companies[$i]->pc_id.'/office')}}" class="">
                        @if (isset($companies[$i]->name))
                            {{$companies[$i]->name}}
                        @endif
                    </a>
                @endif
            @else
                @if (isset($companies[$i]->name))
                    {{$companies[$i]->name}}
                @endif
            @endif
            </td>
            <td>
                @foreach($companies[$i]->default_categories as $default_category)
                    {{$default_category->name.', '}}
                @endforeach
                @foreach($companies[$i]->custom_categories as $custom_category)
                    {{$custom_category->name.', '}}
                @endforeach
            </td>
            <td>
                {{$companies[$i]->office_title or ''}}
            </td>
            <td>
                @if(count($companies[$i]->master_format_items))
                    @foreach($companies[$i]->master_format_items as $masterFormatItems)
                        <p>{{ $masterFormatItems->number.' - '.$masterFormatItems->title }}</p>
                    @endforeach
                @endif
            </td>
            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
            <td>
                <?php $shared = false; ?>
                @if ($companies[$i]->synced_comp_id)
                    @if ($companies[$i]->ps_id)
                        @if ($companies[$i]->ps_status == Config::get('constants.projects.shared_status.pending'))
                            <b>Pending...</b>
                        @else
                            <?php $shared = true; ?>
                            {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>(Request::path().'/'.$companies[$i]->ps_id.'/unshare')]) !!}
                            <input type="hidden" name="comp_id" value="{{$companies[$i]->synced_comp_id}}"/>
                            {{--{!! Form::submit(trans('labels.unshare'),['class'=>'btn cm-btn-secondary btn-sm', 'onclick' => 'return confirm("'.trans('labels.project.unshare_modal').'")']) !!}--}}
                            <button class='btn cm-btn-secondary btn-sm' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.project.unshare_modal')}}'>
                                {{trans('labels.unshare')}}
                            </button>
                            {!! Form::close()!!}
                        @endif
                    @else
                        {!! Form::open(['method'=>'POST', 'class'=>'share-form-prevent', 'url'=>(Request::path().'/'.$companies[$i]->id.'/share')]) !!}
                        <input type="hidden" name="project_company_id" value="{{$companies[$i]->pc_id}}"/>
                        <input type="hidden" name="company_name" value="{{$companies[$i]->name}}"/>
                        <input type="hidden" name="addr_id" value="{{$companies[$i]->addr_id}}"/>
                        <input type="hidden" name="comp_id" value="{{$companies[$i]->synced_comp_id}}"/>
                        {{--{!! Form::submit(trans('labels.share'),['class'=>'btn btn-success btn-sm', 'onclick' =>'return confirm("'.trans('labels.project.share_modal').'")']) !!}--}}
                        <button class='btn btn-success btn-sm' type='submit' data-toggle="modal" data-target="#confirmShare" data-title="Share Project" data-message='{{trans('labels.project.share_modal')}}'>
                            {{trans('labels.share')}}
                        </button>
                        {!! Form::close()!!}
                    @endif
                @else
                    @if (empty($companies[$i]->proj_comp_id))
                        {!! Form::open(['method'=>'POST','url'=>URL('projects/'.$project->id.'/companies/'.$companies[$i]->pc_id.'/address-book-company/'.$companies[$i]->id.'/invite')]) !!}
                        {!! Form::submit(trans('labels.invite'),['class'=>'btn btn-info btn-sm invite-company']) !!}
                        {!! Form::close()!!}
                    @else
                        @if($companies[$i]->ps_status == Config::get('constants.projects.shared_status.pending') && $companies[$i]->ps_token_active == Config::get('constants.projects.status.active'))
                            <b>{{trans('labels.companies.invitation_pending')}}</b>
                        @endif
                    @endif
                @endif

            </td>
            @endif
        </tr>
    @endfor
    </tbody>
</table>
</div>