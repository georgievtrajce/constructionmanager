@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <header class="cm-heading">
        {{trans('labels.Project')}} {{trans('labels.company.plural')}}
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/companies')}}" class="cm-trail-link">{{trans('labels.edit')}} {{trans('labels.Project')}} {{trans('labels.company.plural')}}</a></li>
        </ul>
      </header>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'companies')))
          {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
          <input type="hidden" value="{{URL(Request::path()).'/'}}" id="form-url" />
          <button disabled id="delete-button" class='btn btn-danger pull-right' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
            {{trans('labels.delete')}}
          </button>
          {!! Form::close()!!}
        @endif
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      @include('projects.partials.tabs', array('activeTab' => 'companies'))
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel">
        <div class="panel-body">
          @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
          </div>
          @endif
          @include('errors.list')
          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
            <div class="cm-filter cm-filter__alt cf">
            <div class="row">
              <div class="col-sm-7">
                <div class="row">
                  <div class="col-sm-12">
                    <h4>{{trans('labels.add')}} {{trans('labels.company.plural')}}:</h4>
                  </div>
                </div>
                <div class="row">
                 {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/companies']) !!}
                  <div class="col-sm-3">
                    <div class="form-group">
                      {!! Form::label('company', trans('labels.Company').':') !!}
                      {!! Form::text('proj_ab', Input::get('proj_ab'), ['class' => 'form-control', 'id'=>'proj_ab']) !!}
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      {!! Form::label('prop_addr', trans('labels.project.office').':') !!}
                      <select name="comp_addr" id="comp_addr">
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('category', trans('labels.category.plural').':') !!}
                      {!! Form::text('category', Input::get('proj_ab_cat'), ['class' => 'form-control', 'id'=>'proj_ab_cat', 'disabled'=>'true']) !!}
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success cm-btn-fixer']) !!}
                    </div>
                  </div>
                  <input type="hidden" id="ab_id" name="ab_id" value=""/>
                  <input type="hidden" id="proj_id" name="proj_id" value="{{$project->id}}"/>
                  {!! Form::close() !!}

                </div>
              </div>
            </div>
          </div>
          @endif
          @if (sizeof ($companies))
          <div class="pull-right">
            <?php echo $companies->appends([
            'id'=>Input::get('id'),
            'name'=>Input::get('name'),
            'mf_id'=>Input::get('mf_id'),
            'sort'=>Input::get('sort'),
            'order'=>Input::get('order'),
            ])->render(); ?>
          </div>
          @include('projects.companies.table')
        </div>
        @else
        <div class="row">
          <div class="col-md-12">
            <p class="text-center">{{trans('labels.no_records')}}</p>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  @include('popups.delete_record_popup')
  @include('popups.unshare_project_popup')
  @include('popups.share_project_popup')
</div>
@endsection