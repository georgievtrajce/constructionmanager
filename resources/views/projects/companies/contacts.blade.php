@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.add')}} {{trans('labels.Project')}} {{trans('labels.contact.plural')}}
                <small class="cm-heading-suffix">{{trans('labels.Company')}}: {{$company->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                    class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/companies')}}"
                    class="cm-trail-link">{{trans('labels.edit')}} {{trans('labels.Project')}} {{trans('labels.company.plural')}}</a>
                </li>
                <li class="cm-trail-item active"><a href="{{Request::url()}}"
                class="cm-trail-link">    {{trans('labels.add')}} {{trans('labels.Project')}} {{trans('labels.contact.plural')}}</a>
            </li>
        </ul>
    </header>
</div>
<div class="col-md-7">
    <div class="cm-btn-group cm-pull-right cf">
    </div>
</div>
</div>
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'companies'))
        </div>
    </div>
<div class="panel">
<div class="panel-body">
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::label('prop_addr', trans('labels.project.office').':') !!}
                    <select class="sub_addr" name="sub_addr" id="sub_addr_{{$company->proj_comp_id}}" data-project-id = "{{$project->id}}">
                        <option value="" @if(count($company->addresses) == 0 || empty($company->pc_addr_id)) selected="selected" @endif ></option>
                        @for($j=0; $j<sizeof($company->addresses);$j++)
                            <option @if($company->addresses[$j]->id == $company->pc_addr_id) selected="selected" @endif value={{$company->addresses[$j]->id}}>{{$company->addresses[$j]->office_title}}</option>
                        @endfor
                    </select>
                    <br>
                </div>
            </div>
            {!! Form::open(['method'=> 'POST', 'url'=>URL('/projects/'.$project->id.'/companies/'.$company->id.'/project-company/'.$projectCompanyId.'/office/'.$company->addr_id), 'class'=> 'form-horizontal', 'role'=> 'form']) !!}
            @foreach($abContacts as $contact)
            <div class="checkbox">
                <label class="pull-left" style="margin-top: 0px;">
                    <input type="checkbox" value="{{ $contact->id  }}" name="company_contacts[]" {{ in_array($contact->id, $contactIDs) ? 'checked' : '' }}>
                    {{ $contact->name }}
                </label>
                <a href="javascript:;" style="margin-left: 15px; padding: 5px;" data-toggle="collapse" data-target="#contact_data_container_{{$contact->id}}">
                    <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                </a>
            </div>
            <div class="cm-form-group__info">
                <div class="panel panel-default collapse" style="margin-top: 15px;" id="contact_data_container_{{$contact->id}}">
                    <div class="panel-body cf">
                        <table class="cm-table-no-top-border">
                            <tbody>
                                <tr>
                                    <td>
                                        <span>Title:</span>
                                    </td>
                                    <td>
                                        <span>{{ $contact->title }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>{{ trans('labels.address_book.email_address') }}:</span>
                                    </td>
                                    <td>
                                        <a href="mailto:{{ trans('labels.address_book.email_address').': '.$contact->email }}">{{$contact->email }}</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>{{ trans('labels.address_book.office_phone')}}:</span>
                                    </td>
                                    <td>
                                        <span>{{ $contact->office_phone }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>{{ trans('labels.address_book.cell_phone')}}:</span>
                                    </td>
                                    <td>
                                        <span>{{ $contact->cell_phone }}</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span>{{ trans('labels.address_book.fax')}}:</span>
                                    </td>
                                    <td>
                                        <span>{{ $contact->fax }}</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            @endforeach
            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
            <div class="clearfix"></div>
                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success cm-btn-fixer']) !!}
            @endif
        </div>
        <div class="col-sm-6">
            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'companies')))
                <a class="btn btn-success cm-btn-fixer" href={{URL('projects/'.$project->id.'/companies/'.$projectCompanyId.'/permissions')}}>Permissions</a>
            @endif
        </div>
    </div>
    {!! Form::close() !!}
</div>
</div>
</div>
@endsection