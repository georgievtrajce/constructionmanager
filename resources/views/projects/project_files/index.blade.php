@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.project_files')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item">
                        <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">
                            {{trans('labels.Project').': '.$project->name}}
                        </a>
                    </li>
                    <li class="cm-trail-item active">
                        <a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">
                            {{trans('labels.Project').' '.$typeName}}
                        </a>
                    </li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                @if($type != 'daily-reports')
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', $typeDisplay)))
                        <a href="{{URL($createLink)}}" class="btn btn-success pull-right">{{trans('labels.create_new')}}</a>
                    @endif
                @else
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'daily-reports')))
                        <a href="{{URL('/projects/'.$project->id.'/daily-report/upload-file')}}" class="btn btn-primary pull-right">{{trans('labels.daily-report.upload_new')}}</a>
                        <a href="{{URL('/projects/'.$project->id.'/daily-report/create')}}" class="btn btn-success mr5 pull-right">{{trans('labels.create_new')}}</a>
                    @endif
                @endif
                @if(($typeDisplay == 'submittals') || ($typeDisplay == 'proposals') || ($typeDisplay == 'contracts'))
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', $typeDisplay)))
                        <a href="{{URL('projects/'.$project->id.'/'.$typeDisplay)}}"
                           class="btn btn-sm btn-danger">{{trans('labels.files.delete_from').' '.$typeDisplay}}</a>
                    @endif
                @else
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', $typeDisplay)))
                        <div class="pull-left">
                            {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                            <input type="hidden" value="{{URL('projects/'.$project->id.'/project-files/'.$typeDisplay.'/file').'/'}}" id="form-url" />
                            <button disabled id="delete-button" class='btn btn-danger pull-right mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                {{trans('labels.delete')}}
                            </button>
                            {!! Form::close()!!}
                        </div>
                    @endif
                @endif
            </div>
            @if($type == 'daily-reports')
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'daily-reports')))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="cm-btn-group cm-pull-right cf">
                                <a href="{{URL('/projects/'.$project->id.'/daily-report/copy-last-report')}}" class="btn btn-info pull-right">Copy last report</a>
                            </div>
                        </div>
                    </div>
                @endif
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-files'))
        </div>
    </div>
    <div class="panel cm-panel-alt">
        <div class="panel-body">
            <div class="col-md-2">
                @include('projects.partials.vertical-tabs', array('activeTab' => 'drawings'))
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif

                        <div class="">
                        <div class="row">
                            <div class="col-md-12">
                            @if($type != 'daily-reports')
                                @if(count($projectFiles) > 0)
                                    <div class="table-responsive">
                                    <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                        <thead>
                                            <tr>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)  || (Permissions::can('delete', $typeDisplay)))
                                                <th class="text-center">
                                                    <input type="checkbox" name="select_all" id="project_files_select_all">
                                                </th>
                                                @endif
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'files.name' && Input::get('order') == 'asc') {
                                                        $url = Request::url().'?sort=files.name&order=desc';
                                                    } else {
                                                        $url = Request::url().'?sort=files.name&order=asc';
                                                    }
                                                    ?>
                                                    {{trans('labels.files.file_name')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'files.number' && Input::get('order') == 'desc') {
                                                        $url = Request::url().'?sort=files.number&order=asc';
                                                    } else {
                                                        $url = Request::url().'?sort=files.number&order=desc';
                                                    }
                                                    ?>
                                                    {{trans('labels.files.file_number_code')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                @if($type == 'project-photos')
                                                <th>{{trans('labels.file.image_preview')}}
                                                </th>
                                                @endif
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                                                        $url = Request::url().'?sort=mf_number&order=desc';
                                                    } else {
                                                        $url = Request::url().'?sort=mf_number&order=asc';
                                                    }
                                                    ?>
                                                    {{trans('labels.master_format_number_and_title')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'files.created_at' && Input::get('order') == 'asc') {
                                                        $url = Request::url().'?sort=files.created_at&order=desc';
                                                    } else {
                                                        $url = Request::url().'?sort=files.created_at&order=asc';
                                                    }
                                                    ?>
                                                    {{trans('labels.files.upload_date')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th>
                                                    {{trans('labels.files.file_type')}}
                                                </th>
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'files.size' && Input::get('order') == 'asc') {
                                                        $url = Request::url().'?sort=files.size&order=desc';
                                                    } else {
                                                        $url = Request::url().'?sort=files.size&order=asc';
                                                    }
                                                    ?>
                                                    {{trans('labels.files.file_size')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($projectFiles as $item)
                                            <tr>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', $typeDisplay)))
                                                <td class="text-center">
                                                    <input type="checkbox" name="file_id[]" value="{{$item->id}}" data-id="{{$item->id}}" class="project_file multiple-items-checkbox">
                                                </td>
                                                @endif
                                                <td>
                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', $typeDisplay)))
                                                    <a href="{{URL($item->edit_link)}}"
                                                        class=""> {{$item->name}}
                                                    </a>
                                                    @else
                                                        {{$item->name}}
                                                    @endif
                                                </td>
                                                <td>{{$item->number}}</td>
                                                @if($type == 'project-photos')
                                                <td>
                                                    <img width="100px" src="{{env('AWS_CLOUD_FRONT').'/company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$item->file_name}}" />
                                                </td>
                                                @endif
                                                <td>
                                                    @if(!empty($item->mf_number) || !empty($item->mf_title))
                                                    {{$item->mf_number.' - '.$item->mf_title}}
                                                    @endif
                                                </td>
                                                <td>{{Carbon::parse($item->created_at)->format('m/d/Y h:i:s')}}</td>
                                                <td>
                                                    <?php
                                                    if ($item->file_name != '') {
                                                    $fileExtension = explode('.', $item->file_name);
                                                    echo $fileExtension[count($fileExtension)-1];
                                                    }
                                                    ?>
                                                </td>
                                                <td>{{CustomHelper::formatByteSizeUnits($item->size)}}</td>
                                                <td>
                                                    <a class="download"
                                                    href="javascript:;">{{trans('labels.files.download')}}</a>
                                                    <input type="hidden" class="s3FilePath" id="{{$item->id}}"
                                                    value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$item->file_name}}">
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                @else
                                    <p class="text-center">{{trans('labels.no_records')}}</p>
                                @endif
                            @else
                                    @if(count($dailyReports) > 0)
                                        <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                            <thead>
                                            <tr>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)  || (Permissions::can('delete', $typeDisplay)))
                                                    <th class="text-center">
                                                        <input type="checkbox" name="select_all" id="project_files_select_all">
                                                    </th>
                                                @endif
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'daily_reports.report_num' && Input::get('order') == 'desc') {
                                                        $url = Request::url().'?sort=daily_reports.report_num&order=asc';
                                                    } else {
                                                        $url = Request::url().'?sort=daily_reports.report_num&order=desc';
                                                    }
                                                    ?>
                                                    {{trans('labels.daily-report.report')}} #
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th>
                                                    <?php
                                                    if (Input::get('sort') == 'daily_reports.date' && Input::get('order') == 'asc') {
                                                        $url = Request::url().'?sort=daily_reports.date&order=desc';
                                                    } else {
                                                        $url = Request::url().'?sort=daily_reports.date&order=asc';
                                                    }
                                                    ?>
                                                    {{trans('labels.daily-report.date')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                </th>
                                                <th>
                                                    {{trans('labels.daily-report.day_of_week')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.daily-report.creator')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.daily-report.report')}}
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($dailyReports as $dailyReport)
                                                <tr>
                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', $typeDisplay)))
                                                        <td class="text-center">
                                                            <input type="checkbox" name="file_id[]" value="{{$dailyReport->id}}" data-id="{{$dailyReport->id}}" class="project_file multiple-items-checkbox">
                                                        </td>
                                                    @endif
                                                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', $typeDisplay)))
                                                        <td>
                                                            @if($dailyReport->is_uploaded)
                                                                <a href="{{URL('/projects/'.$project->id.'/daily-report/'.$dailyReport->id.'/edit-file')}}" > {{$dailyReport->report_num}}</a>
                                                            @else
                                                                <a href="{{URL('/projects/'.$project->id.'/daily-report/'.$dailyReport->id.'/edit')}}" > {{$dailyReport->report_num}}</a>
                                                            @endif
                                                        </td>
                                                    @else
                                                        <td>{{$dailyReport->report_num}}</td>
                                                    @endif
                                                    <td>{{Carbon::parse($dailyReport->date)->format('m/d/Y')}}</td>
                                                    <td>{{Config::get('constants.carbon_days.'.Carbon::parse($dailyReport->date)->dayOfWeek)}}</td>
                                                    <td>{{$dailyReport->user->name}}</td>
                                                    <td>
                                                        @if(isset($dailyReport->generatedFile) && !empty($dailyReport->generatedFile))
                                                            <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                            <input type="hidden" class="s3FilePath" id="{{$dailyReport->generatedFile->file_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/daily_report/'. $dailyReport->id .'/report/'. $dailyReport->generatedFile->file_name}}">
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>

                                    @else
                                        <p class="text-center">{{trans('labels.no_records')}}</p>
                                    @endif
                                @endif
                            </div>
                        </div>
                            </div>
                        </div>
                </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    @if($type != 'daily-reports')
                                        <?php echo $projectFiles->
                                        appends([
                                        'sort' => Input::get('sort'),
                                        'order' => Input::get('order'),
                                        ])->render();
                                        ?>
                                    @else
                                        <?php echo $dailyReports->
                                        appends([
                                            'sort' => Input::get('sort'),
                                            'order' => Input::get('order'),
                                        ])->render();
                                        ?>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('popups.delete_record_popup')
        @include('popups.alert_popup')
        @endsection