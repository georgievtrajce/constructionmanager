@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.files.upload_new_file')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$typeName}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file/create')}}" class="cm-trail-link">{{trans('labels.files.upload_new_file')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-files'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <form role="form" onsubmit="setFormSubmitting()" class="create" id="form" action="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <h4>{{trans('labels.file.add')}}</h4>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.file.name')}}</label>
                                    <input type="text" id="fileName" class="form-control" name="file_name" value="{{ old('file_name') }}">
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.file.number')}}</label>
                                    <input type="text" id="fileNumber" class="form-control" name="file_number" value="{{ old('file_number') }}">
                                </div>
                                <div class="">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label for="contract_file">{{trans('labels.File')}}:</label>
                                                <input type="file" name="project_file" id="file" multiple>
                                                <input type="hidden" name="file_id" id="file_id" value="{{old('file_id') }}">
                                            </div>
                                            <div class="col-md-4">
                                                <div id="file_wait" style="display: none;">
                                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="40px">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <button type="button" id="file-upload-create" class="btn btn-info btn-sm pull-right cm-btn-fixer">
                                                {{trans('labels.upload')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row file_main_message_container">
                                        <div id="file_message_container"
                                            class="col-md-12 file_message_container message_container"
                                        style="color: #49A078;"></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" class="cm-control-required form-control mf-number-title-auto" name="master_format_search" value="{{ old('master_format_search') }}">
                                            <input type="hidden" class="master-format-id" name="master_format_id" value="{{old('master_format_id')}}">
                                            <input type="hidden" id="project_id" value="{{$project->id}}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                        <div class="mf-cont cf">
                                            <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">
                                                {{trans('labels.submittals.enter_custom_number_and_title')}}
                                            </a>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <label class="cm-control-requiredcontrol-label">{{trans('labels.master_format_number_and_title')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{old('master_format_number') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{ old('master_format_title') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="hidden" id="type" name="dtype" value="{{Config::get('constants.project-files')}}">
                                            <input type="hidden" id="file_type" name="type" value="project-file">
                                            <input type="hidden" id="project_file_type" name="project_file_type" value="{{$typeDisplay}}">
                                            <button id="submit_file" type="submit" class="btn btn-sm btn-success">
                                            {{trans('labels.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('projects.partials.successful-upload-multi')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    <script id="cms-dev-alert" type="text/template">
    <div class="alert cms-dev-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <span class="cms-dev-alert-content"></span>
    </div>
    </script>
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
    @endsection