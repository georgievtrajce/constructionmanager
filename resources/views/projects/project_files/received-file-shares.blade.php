@extends('layouts.master')
@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-12">
                <header class="cm-heading">
                    {{trans('labels.files.files_shared_with_you')}}

                    <ul class="cm-trail">
                        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                        <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$typeName}}</a></li>
                    </ul>
                </header>
            </div>
        </div>

        @include('projects.partials.tabs', array('activeTab' => 'project-files'))


        <div class="panel cm-panel-alt">
            <div class="panel-body">
                <div class="col-md-2">
                    @include('projects.partials.vertical-tabs', array('activeTab' => 'drawings'))
                </div>
                <div class="col-md-10">
                    @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                    @endif


                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav nav-pills">
                                <li class="active dropdown">
                                    <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                        {{trans('labels.select')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul role="group" class="dropdown-menu">
                                        <li>
                                            <a href="{{URL('projects/'.$project->
                                        id.'/project-files/'.$typeDisplay.'/file')}}">{{trans('labels.submittals.created')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{URL('projects/'.$project->
                                        id.'/project-files/'.$typeDisplay.'/file/shares')}}">{{trans('labels.submittals.shares')}}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                        {{trans('labels.files.received_files')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul role="group" class="dropdown-menu">
                                        <li>
                                            <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file/received')}}">{{trans('labels.files_global')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file/received/shares')}}">{{trans('labels.files.shares')}}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <?php echo $sharedProjectFiles->appends([
                                    'sort'=>Input::get('sort'),
                                    'order'=>Input::get('order'),
                                ])->render();
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="cm-spacer-normal"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <h3>{{trans('labels.files.received_and_shared_files')}}</h3>
                            @if(count($sharedProjectFiles) > 0)
                                <div class="table-responsive">
                                    <table class="table table-hover table-striped cm-table-compact">
                                        <thead>
                                        <tr>
                                            <th>{{trans('labels.file.name')}}</th>
                                            <th>{{trans('labels.files.file_number_code')}}</th>
                                            <th>{{trans('labels.files.upload_date')}}</th>
                                            <th>{{trans('labels.files.file_type')}}</th>
                                            <th>{{trans('labels.files.file_size')}}</th>
                                            <th>{{trans('labels.files.shared_with')}}</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($sharedProjectFiles as $item)
                                            <tr>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->number}}</td>
                                                <td>{{$item->created_at}}</td>
                                                <td>
                                                    <?php
                                                    if(isset($item->file_name)) {
                                                        $fileExtension = explode('.',$item->file_name);
                                                        echo $fileExtension[1];
                                                    }
                                                    ?>
                                                </td>
                                                <td>{{$item->size.' bytes'}}</td>
                                                <td>{{$item->company_name}}</td>
                                                <td>
                                                    <a target="_blank" href="{{URL('projects/'.$project->id.'/project-files/'.$typeDisplay.'/file/download/'.$item->file_name)}}">{{$item->file_name}}</a>
                                                </td>
                                                <td>
                                                    {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>URL('projects/'.$project->id.'/file/'.$item->id.'/share-company/'.$item->share_comp_id.'/receive-company/'.$item->receive_comp_id.'/unshare')]) !!}
                                                    {{--{!! Form::submit(trans('labels.unshare'),['class'=>'btn btn-xs btn-warning','style'=>'height:22px','onclick' => 'return confirm("'.trans('labels.files.unshare_popup').'")']) !!}--}}
                                                    <button class='btn cm-btn-secondary btn-sm' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.files.unshare_popup')}}'>
                                                        {{trans('labels.unshare')}}
                                                    </button>
                                                    {!! Form::close()!!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @else
                                <p class="text-center">{{trans('labels.files.no_shared_files')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('popups.unshare_project_popup')
@endsection