@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.project_files')}}
                <a href="{{URL($createLink)}}" class="btn btn-success pull-right">{{trans('labels.create_new')}}</a>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$typeName}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-files'))
        </div>
    </div>
    <div class="panel cm-panel-alt">
        <div class="panel-body">
            <div class="col-md-2">
                @include('projects.partials.vertical-tabs', array('activeTab' => 'drawings'))
            </div>
            <div class="col-md-10">
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                    </div>
                    <div class="col-md-6">
                        <div class="pull-right">
                            <?php echo $sharedProjectFiles->render();
                            ?>
                        </div>
                    </div>
                </div>
                <!--              <div class="cm-spacer-normal"></div> -->
                <div class="">
                    <div class="row">
                    <div class="col-md-12">
                        <h4 class="pull-left">{{trans('labels.files.uploaded_shared_files')}}</h4>
                        @if(count($sharedProjectFiles) > 0)
                        <ul class="nav nav-pills pull-right">
                            <li class="active dropdown">
                                <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                    {{trans('labels.view')}}
                                    <span class="caret"></span>
                                </a>
                                <ul role="group" class="dropdown-menu">
                                    <li>
                                        <a href="{{URL('projects/'.$project->
                                            id.'/project-files/'.$typeDisplay.'/file')}}">{{trans('labels.all_files')}}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{URL('projects/'.$project->
                                            id.'/project-files/'.$typeDisplay.'/file/shares')}}">{{trans('labels.sent_files')}}
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered cm-table-compact" >
                            <thead>
                                <tr>
                                <th></th>
                                    <th>{{trans('labels.file.name')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>{{trans('labels.files.file_number_code')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    @if($type == 'project-photos')
                                    <th>{{trans('labels.file.image_preview')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    @endif
                                    <th>{{trans('labels.master_format_number_and_title')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>{{trans('labels.files.upload_date')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>{{trans('labels.files.file_type')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>{{trans('labels.files.file_size')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th>{{trans('labels.files.shared_with')}}
                                        <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                    </th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sharedProjectFiles as $item)
                                <tr>
                                <td class="text-center"><input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id=""></td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->number}}</td>
                                    @if($type == 'project-photos')
                                    <td>
                                        <img width="200px" src="{{env('AWS_CLOUD_FRONT').'/company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$item->file_name}}" />
                                    </td>
                                    @endif
                                    <td>
                                        @if(!empty($item->mf_number) || !empty($item->mf_title))
                                        {{$item->mf_number.' - '.$item->mf_title}}
                                        @endif
                                    </td>
                                    <td>{{$item->created_at}}</td>
                                    <td>
                                        <?php
                                        if(isset($item->file_name)) {
                                        $fileExtension = explode('.',$item->file_name);
                                        echo $fileExtension[1];
                                        }
                                        ?>
                                    </td>
                                    <td>{{CustomHelper::formatByteSizeUnits($item->size)}}</td>
                                    <td>{{$item->company_name}}</td>
                                    <td>
                                        <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                        <input type="hidden" class="s3FilePath" id="{{$item->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$item->file_name}}">
                                    </td>
                                    <td>
                                        {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>URL('projects/'.$project->id.'/file/'.$item->id.'/share-company/'.$item->share_comp_id.'/receive-company/'.$item->receive_comp_id.'/unshare')]) !!}
                                        {{--{!! Form::submit(trans('labels.unshare'),['class'=>'btn cm-btn-secondary btn-sm', 'onclick' => 'return confirm("'.trans('labels.files.unshare_popup').'")']) !!}--}}
                                        <button class='btn cm-btn-secondary btn-sm' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.files.unshare_popup')}}'>
                                        {{trans('labels.unshare')}}
                                        </button>
                                        {!! Form::close()!!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        @else
                        <p class="text-center">{{trans('labels.files.no_shared_files')}}</p>
                        @endif
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('popups.unshare_project_popup')
@endsection