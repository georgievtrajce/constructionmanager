@extends('layouts.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.files.edit_project_file')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$typeName}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file/'.$projectFile->id.'/edit')}}"
                            class="cm-trail-link">{{trans('labels.file.edit')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-files'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 cf">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>                                {{ Session::get('flash_notification.message') }}
                            </div>
                            @endif {!! Form::open(['files'=>true, 'id'=>'form', 'onsubmit'=>'setFormSubmitting()', 'method'=> 'PUT', 'url'=>URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file/'.$projectFile->id)])
                            !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <h4>{{trans('labels.file.add')}}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="cm-control-required">{{trans('labels.file.name')}}</label>
                                        <input type="text" class="form-control" id="fileName" name="file_name" value="{{ $projectFile->name }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="cm-control-required">{{trans('labels.file.number')}}</label>
                                        <input type="text" class="form-control" id="fileNumber" name="file_number" value="{{ $projectFile->number }}">
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="contract_file">{{trans('labels.File')}}:</label>
                                            <input type="file" name="project_file" id="file">
                                            <input type="hidden" name="file_id" id="file_id" value="{{ $projectFile->id }}">
                                        </div>
                                        <div class="col-md-4">
                                            <div id="file_wait" style="display: none;">
                                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="40px">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="button" id="file-upload" class="btn btn-info btn-sm pull-right cm-btn-fixer">
                                        {{trans('labels.upload')}}
                                        </button>
                                        </div>
                                    </div>
                                    <div class="row file_main_message_container">
                                        <div id="file_message_container" class="col-md-12 file_message_container message_container" style="color: #49A078;"></div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control cm-control-required mf-number-title-auto" name="master_format_search">
                                        <input type="hidden" class="master-format-id" name="master_format_id">
                                        <input type="hidden" id="project_id" value="{{$project->id}}">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                    <div class="mf-cont cf">
                                        <a href="javascript:;" id="custom-mf" class="btn btn-primary mb0 pull-right">
                                        {{trans('labels.submittals.enter_custom_number_and_title')}}
                                    </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="cm-control-requiredcontrol-label">{{trans('labels.master_format_number_and_title')}}</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control mf-number-auto" name="master_format_number" readonly value="{{$projectFile->mf_number}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control mf-title-auto" name="master_format_title" readonly value="{{$projectFile->mf_title}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="cm-spacer-xs"></div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="pull-left">
                                            <input type="hidden" id="type" name="dtype" value="{{Config::get('constants.project-files')}}">
                                            <input type="hidden" id="file_type" name="type" value="project-file">
                                            <input type="hidden" id="project_file_type" name="project_file_type" value="{{$typeDisplay}}">
                                            <button id="submit_file" type="submit" class="btn btn-sm btn-success">
                                    {{trans('labels.save')}}
                                    </button>
                                            <div class="please_wait"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <p>{{trans('labels.file.current').': '}}</p>
                                        <a class="download" href="javascript:;">{{$projectFile->file_name}}</a>
                                        <input type="hidden" class="s3FilePath" id="{{$projectFile->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$projectFile->file_name}}">
                                        
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-6">
                            <h4>{{trans('labels.file.send')}}</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                    <input type="checkbox" name="select_all" id="select_all" value="1">
                                    <span class="checkbox-material">
                                        <span class="check"></span>
                                    </span>
                                    Select All Contacts
                                </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                        <div class="cms-list-box">
                                            <ul class="cms-list-group">
                                                <li class="cms-list-group-item cf">
                                                    <div class="radio">
                                                        <label>
                                                <input type="radio" checked class="companyCheckboxIn" name="companyRadioDistributionIn" data-type="user" id="mycompany">
                                                <span class="radio-material">
                                                    <span class="check"></span>
                                                </span>
                                                {{$myCompany->name}}
                                            </label>
                                                    </div>
                                                </li>
                                                @foreach($companies as $id => $name)
                                                <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                    <div class="radio">
                                                        <label>
                                                    <input type="radio" class="companyCheckboxDistributionIn" name="companyRadioDistributionIn" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                                    <span class="radio-material">
                                                        <span class="check"></span>
                                                    </span>
                                                    {{$name}}
                                                </label>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                        <div class="cms-list-box">
                                            <ul class="cms-list-group" id="usersListIn">
                                                @foreach($myCompany->users as $user)
                                                <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                        <label>
                                                    <input class="contact_item" type="checkbox" name="employees_users_in[]" value="{{$user->id}}">
                                                    <span class="checkbox-material">
                                                        <span class="check"></span>
                                                    </span>
                                                    {{$user->name}}
                                                </label>
                                                    </div>
                                                </li>
                                                @endforeach
                                            </ul>
                                            @foreach($users as $id => $value)
                                            <ul class="cms-list-group contactsListDistributionIn hide" id="contactsListDistributionIn-{{$id}}">
                                                @foreach($value as $project_contact)
                                                    @if(!empty($project_contact->contact))
                                                        <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" class="contact_item" name="employees_contacts_in[]" value="{{$project_contact->contact->id}}">
                                                                    <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                    {{$project_contact->contact->name}}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="cm-spacer-xs"></div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <a class="btn btn-sm pull-left btn-primary sendNotificationEmail" title="{{trans('labels.project-file.send_email')}}" data-file-path="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$projectFile->file_name}}"
                                               data-file-id="{{$projectFile->id}}" data-emailed="{{$projectFile->emailed}}"
                                               href="javascript:;">
                                                Send Notification
                                            </a>
                                            <div class="pull-left" style="margin: 0px 7px; width: 50px;">
                                                &nbsp;
                                                <div class="pull-left email_wait" style="display: none;">
                                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="25px">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                </div>
                                <div class="col-md-12">
                                    @if(!empty($projectFile->projectFileUsers))
                                        @foreach($projectFile->projectFileUsers as $projectFileUser)
                                        <?php $sendDate = Carbon\Carbon::parse($projectFileUser->created_at); ?>
                                            @if(!empty($projectFileUser->users))
                                                @foreach($projectFileUser->users as $user)
                                                    {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                @endforeach
                                            @endif
                                            @if(!empty($projectFileUser->abUsers))
                                                @foreach($projectFileUser->abUsers as $user)
                                                    {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                    <br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    @include('projects.partials.successful-upload') @include('projects.partials.error-upload') @include('projects.partials.upload-limitation')
    @include('popups.alert_popup') @include('popups.delete_record_popup') @include('popups.approve_popup') @endsection