@extends('layouts.master')
@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-12">
                <header class="cm-heading">
                    {{trans('labels.project_files')}}

                    <ul class="cm-trail">
                        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                        <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/project-files/'.$typeDisplay.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$typeName}}</a></li>
                    </ul>
                </header>
            </div>
        </div>
        @include('projects.partials.tabs', array('activeTab' => 'project-files'))
        <div class="panel cm-panel-alt">
            <div class="panel-body">
                <div class="col-md-2">
                    @include('projects.partials.vertical-tabs', array('activeTab' => 'drawings'))
                </div>
                <div class="col-md-10">
                    <div class="row">

                        <div class="col-sm-9">
                            @if (Session::has('flash_notification.message'))
                                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('flash_notification.message') }}
                                </div>
                            @endif


                            <div class="row">
                                <div class="col-md-5">
                                    <ul class="nav nav-pills">
                                        <li class="active dropdown">
                                            <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                                {{trans('labels.select')}}
                                                <span class="caret"></span>
                                            </a>
                                            <ul role="group" class="dropdown-menu">
                                                <li>
                                                    <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file')}}">{{trans('labels.submittals.created')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file/shares')}}">{{trans('labels.submittals.shares')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                                {{trans('labels.files.received_files')}}
                                                <span class="caret"></span>
                                            </a>
                                            <ul role="group" class="dropdown-menu">
                                                <li>
                                                    <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file/received')}}">{{trans('labels.files_global')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{URL('projects/'.$project->
                                    id.'/project-files/'.$typeDisplay.'/file/received/shares')}}">{{trans('labels.files.shares')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-7">
                                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/project-files/'.$typeDisplay.'/file']) !!}
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                {!! Form::label('sort', trans('labels.sort_by')) !!}
                                                <br />
                                                {!! Form::select('sort',['files.id'=>'ID','files.name'=>trans('labels.file.name'),'files.number'=>trans('labels.files.file_number_code'),'files.created_at'=>trans('labels.files.upload_date'),'files.size'=>trans('labels.files.file_size')],Input::get('sort')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                {!! Form::label('order', trans('labels.order_by')) !!}
                                                <br />
                                                {!! Form::select('order', array('asc'=>trans('labels.ascending'),'desc'=>trans('labels.descending')),Input::get('order')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('sort', ' &nbsp;') !!}
                                                {!! Form::submit(trans('labels.sort'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>

                            <div class="pull-right">
                                <?php echo $projectFiles->appends([
                                    'sort'=>Input::get('sort'),
                                    'order'=>Input::get('order'),
                                ])->render();
                                ?>
                            </div>


                            <div class="container-fluid">
                                <div class="row">
                                    <h3>{{trans('labels.files.all_files_shared_with_you')}}</h3>
                                    @if(count($projectFiles) > 0)
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="select_all" id="project_files_select_all"> {{trans('labels.select_all')}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>{{trans('labels.file.name')}}</th>
                                                <th>{{trans('labels.files.file_number_code')}}</th>
                                                <th>{{trans('labels.files.upload_date')}}</th>
                                                <th>{{trans('labels.files.file_type')}}</th>
                                                <th>{{trans('labels.files.file_size')}}</th>
                                                <th>{{trans('labels.files.share_company')}}</th>
                                                <th>{{trans('labels.Shared')}}</th>
                                                <th>{{trans('labels.files.download')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($projectFiles as $item)
                                                <tr>
                                                    <td>
                                                        <input type="checkbox" name="file_id[]" value="{{$item->id}}" class="project_file">
                                                    </td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->number}}</td>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>
                                                        <?php
                                                        if(isset($item->file_name)) {
                                                            $fileExtension = explode('.',$item->file_name);
                                                            echo $fileExtension[1];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>{{$item->size.' bytes'}}</td>
                                                    <td>{{$item->share_company->first()->name}}</td>
                                                    <td>
                                                        <?php
                                                        $j = 0;
                                                        foreach($item->shares as $share) {
                                                            if($share->share_comp_id == Auth::user()->comp_id) {
                                                                $j++;
                                                            }
                                                        }
                                                        if($j > 0) {
                                                            echo 'Yes';
                                                        } else {
                                                            echo 'No';
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <a target="_blank" href="{{URL('projects/'.$project->id.'/project-files/'.$typeDisplay.'/file/download/'.$item->file_name)}}">{{$item->file_name}}</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                         </div>
                                    @else
                                        <p class="text-center">{{trans('labels.no_records')}}</p>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-3">
                            <div class="panel cm-panel-beta">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.files.share_with')}}</label>
                                        <select name="share_with" id="share_with">
                                            <option value="all">{{trans('labels.files.all_subcontractors')}}</option>
                                            <option value="selected">{{trans('labels.files.selected_companies')}}</option>
                                        </select>
                                    </div>

                                    <div id="selected_input_cont">
                                        <div id="search_project_subcontractor">
                                            <label>{{trans('labels.files.select_company')}}</label>
                                            <input type="text" class="form-control project-subcontractor-auto" name="project_subcontractor">
                                            <input type="hidden" class="project-subcontractor-id" name="sub_id">
                                            <input type="hidden" id="project-id" name="proj_id" value="{{$project->id}}"></div>
                                    </div>

                                    <div  id="selected_subcontractors_cont">
                                        <p>{{trans('labels.files.selected_companies').':'}}</p>
                                        <div id="selected_subcontractors"></div>
                                    </div>
                                    <div >
                                        <a href="javascript:;" id="share_project_file" class="btn btn-sm btn-primary cm-btn-fixer pull-right">{{trans('labels.share')}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
