@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.tasks.project-tasks')}}

                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/tasks')}}" class="cm-trail-link">{{trans('labels.tasks.project-tasks')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <div class="cm-btn-group cf">
                    <a target="_blank" href="{{URL('tasks').'/report/project'.'?'.$_SERVER['QUERY_STRING'].(!empty($_SERVER['QUERY_STRING'])?'projectId='.$project->id:'&projectId='.$project->id)}}" class="btn btn-success pull-right">{{trans('labels.address_book.save_to_pdf')}}</a>
                    @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
                        <a href="{{URL('tasks/create').'?project='.$project->id}}" class="btn btn-success pull-right cm-heading-btn">{{trans('labels.tasks.create')}}</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-tasks'))
        </div>
    </div>

    @include("tasks.partials.filter")

    <div class="panel">
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            @include("tasks.partials.table")
        </div>
    </div>
</div>
<script type="text/javascript" src="{{URL::asset('js/loader.js')}}"></script>
<script type="text/javascript">
    var dataDbTasks = [['Type', 'Submittals']];
    @foreach($tasksStatistics as $key=>$item)
    dataDbTasks.push(['{{$key}}', {{$item}}]);
    @endforeach
</script>
<script src="{{URL::asset('js/google-charts.js')}}"></script>
@include('popups.delete_record_popup')
@endsection