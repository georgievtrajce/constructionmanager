@extends('layouts.tabs')

@section('title')
    {{trans('labels.project.edit')}}
    <ul class="cm-trail">
        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
        <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/subcontractor/project-info/edit')}}" class="cm-trail-link">{{trans('labels.project.edit')}}</a></li>
    </ul>
@endsection

@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            @if(is_null($project->subcontractorProjectInfo))
                {!! Form::model($project, ['method'=> 'POST', 'url'=>URL('projects/'.$project->id.'/subcontractor/project-info/store')]) !!}
                    @include('projects.partials.create-subcontractor', $project)
                {!! Form::close() !!}
            @else
                {!! Form::model($project, ['method'=> 'POST', 'url'=>URL('projects/'.$project->id.'/subcontractor/project-info/update')]) !!}
                    @include('projects.partials.edit-subcontractor', $project)
                {!! Form::close() !!}
            @endif
            @include('errors.list')
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
        </div>
    </div>
@endsection