<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS Construction Master</title>
</head>
<body>
<table width="100%">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="width: 400px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 450px; vertical-align: bottom;">
                        {{Auth::user()->email}}<br>
                        {{Auth::user()->office_phone}}
                    </td>
                    <td style="float: right; vertical-align: bottom;">
                        <h2 style="float: left; margin: 0px; text-align: right;">{{"Submittals, RFI's, PCO's"}}</h2>

                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="vertical-align: top; width: 465px;">
                        <b>{{'Project: '}}</b>
                        {{$project->name}}
                    </td>
                    <td style="vertical-align: top; width: 390px; text-align: right;">
                        <b>{{'Project Number: '}}</b>
                        {{$project->number}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="row">
    <h3>{{trans('labels.submittals.submittals_filtered_by')}}</h3>
    <h4>
        @if(!empty($status_title))
            {{$status_title}}
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .submittals-padding {
            padding: 5px 5px !important;
            text-align: left !important;
        }
    </style>
    @if(count($submittals) != 0)
        <table style="width: 100%;">
            <thead>
            <tr>
                <th class="submittals-padding">{{trans('labels.submittals.version_no')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.cycle_no')}}</th>
                <th class="submittals-padding">{{trans('labels.master_format_number_and_title')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.name')}}</th>
                <th class="submittals-padding">{{trans('labels.supplier_subcontractor')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.received_from_subcontractor')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.sent_for_approval')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.received_from_approval')}}</th>
                <th class="submittals-padding">{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                <th class="submittals-padding">{{trans('labels.status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($submittals as $item)
                <tr>
                    <td class="submittals-padding">{{$item->number}}</td>
                    <td class="submittals-padding">{{$item->version_cycle_no}}</td>
                    <td class="submittals-padding">
                        @if (isset($item->mf_number) && isset($item->mf_title))
                            {{$item->mf_number.' - '.$item->mf_title}}
                        @endif
                    </td>
                    <td class="submittals-padding">{{$item->name}}</td>
                    <td class="submittals-padding">
                        {{($item->self_performed ? Auth::user()->company->name : $item->subcontractor_name)}}
                    </td>
                    <td class="submittals-padding">@if($item->version_rec_sub != 0){{date("m/d/Y", strtotime($item->version_rec_sub))}}@endif</td>
                    <td class="submittals-padding">@if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif</td>
                    <td class="submittals-padding">@if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif</td>
                    <td class="submittals-padding">@if($item->version_subm_sent_sub != 0){{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}@endif</td>
                    <td class="submittals-padding">{{$item->version_status_short_name}}</td>
                </tr>
                @if(!empty($printWithNotes))
                    <tr>
                        <td colspan="11" style="background: #ddd; padding: 5px;">{!! $item->note !!}</td>
                    </tr>
                @endif
                <tr>
                    <td colspan="11"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
<div class="row">
    <h3>{{trans('labels.rfis.rfis_filtered_by')}}</h3>
    <h4>
        @if(!empty($status_title))
            {{$status_title}}
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .rfis-padding {
            padding: 5px 5px!important;
            text-align: left!important;
        }
    </style>
    @if(count($rfis) != 0)
        <table style="width: 100%;" class="table table-hover table-striped cm-table-compact">
            <thead>
            <tr>
                <th class="rfis-padding">{{trans('labels.rfis.version_no')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.cycle_no')}}</th>
                <th class="rfis-padding">{{trans('labels.address_book.master_format_number')}}</th>
                <th class="rfis-padding">{{trans('labels.address_book.master_format_title')}}</th>
                <th class="rfis-padding">{{trans('labels.rfis.name')}}</th>
                <th class="rfis-padding">{{trans('labels.supplier_subcontractor')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.received_from_subcontractor')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.sent_for_approval')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.received_from_approval')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                <th class="rfis-padding">{{trans('labels.status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($rfis as $item)
                <tr>
                    <td class="rfis-padding">{{$item->number}}</td>
                    <td class="rfis-padding">{{$item->version_cycle_no}}</td>
                    <td class="rfis-padding">{{$item->mf_number}}</td>
                    <td class="rfis-padding">{{$item->mf_title}}</td>
                    <td class="rfis-padding">{{$item->name}}</td>
                    <td class="rfis-padding">
                        {{($item->self_performed ? Auth::user()->company->name : $item->subcontractor_name)}}
                    </td>
                    <td class="rfis-padding">@if($item->version_rec_sub != 0){{date("m/d/Y", strtotime($item->version_rec_sub))}}@endif</td>
                    <td class="rfis-padding" >@if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif</td>
                    <td class="rfis-padding">@if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif</td>
                    <td class="rfis-padding">@if($item->version_subm_sent_sub != 0){{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}@endif</td>
                    <td class="rfis-padding">{{$item->version_status_short_name}}</td>
                </tr>
                @if(!empty($printWithNotes))
                    <tr>
                        <td colspan="11" style="background: #ddd; padding: 5px;">{{$item->question}}</td>
                    </tr>
                @endif
                <tr>
                    <td colspan="11"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
<div class="row">
    <h3>{{trans('labels.pcos.pcos_filtered_by')}}</h3>
    <h4>
        @if(!empty($status_title))
            {{$status_title}}
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .rfis-padding {
            padding: 5px 5px!important;
            text-align: left!important;
            vertical-align: top;
        }
    </style>
    @if(count($pcos) != 0)
        <table style="width: 100%;" class="table table-hover table-striped cm-table-compact">
            <thead>
            <tr>
                <th class="rfis-padding">{{trans('labels.pcos.version_no')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.cycle_no')}}</th>
                <th class="rfis-padding">{{trans('labels.pcos.name')}}</th>
                <th class="rfis-padding">
                    {{trans('labels.mf_number_and_title').' - '.trans('labels.pcos.subcontractors')}}
                </th>
                <th class="rfis-padding">{{trans('labels.pcos.recipient')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.sent_for_approval')}}</th>
                <th class="rfis-padding">{{trans('labels.submittals.received_from_approval')}}</th>
                <th class="rfis-padding">{{trans('labels.status')}}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pcos as $item)
                <tr>
                    <td class="rfis-padding">{{(!is_null($item->number)) ? $item->number : ''}}</td>
                    <td class="rfis-padding">{{(!is_null($item->version_cycle_no)) ? $item->version_cycle_no : ''}}</td>
                    <td class="rfis-padding">{{$item->name}}</td>
                    <td class="rfis-padding">
                        @if(count($item->subcontractors))
                            @foreach($item->subcontractors as $subcontractor)
                                <p style="padding: 0px 0px 5px 0px; margin: 0px;">
                                    {{$subcontractor->mf_number.' '.$subcontractor->mf_title.' - '.($subcontractor->self_performed ? (!is_null($item->company) ? $item->company->name : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name)}}
                                </p>
                            @endforeach
                        @endif
                    </td>
                    <td class="rfis-padding">{{(!is_null($item->recipient)) ? $item->recipient->ab_recipient->name : ''}}</td>
                    <td class="rfis-padding">
                        @if(!is_null($item->version_sent_appr))
                            @if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif
                        @endif
                    </td>
                    <td class="rfis-padding">
                        @if(!is_null($item->version_rec_appr))
                            @if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif
                        @endif
                    </td>
                    <td class="rfis-padding">{{(!is_null($item->version_status_short_name)) ? $item->version_status_short_name : ''}}</td>
                </tr>
                @if(!empty($printWithNotes))
                    <tr>
                        <td colspan="8" style="background: #ddd; padding: 5px;">{!! $item->description_of_change !!}</td>
                    </tr>
                @endif
                <tr>
                    <td colspan="8"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
</body>
</html>