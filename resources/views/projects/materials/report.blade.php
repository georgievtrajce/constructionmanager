<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<table width="100%">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black;">
            <table width="100%">
                <tr>
                    <td style="width: 400px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 400px; margin-left: 40px; vertical-align: bottom;">
                        {{Auth::user()->email}}<br>
                        {{Auth::user()->office_phone}}
                    </td>
                    <td style="float: right; vertical-align: bottom;">
                        <h2 style="float: left; margin: 0px; text-align: right;">{{"Expediting Report"}}</h2>

                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black;">
            <table width="100%">
                <tr>
                    <td style="vertical-align: top; width: 465px;">
                        <b>{{'Project: '}}</b>
                        {{$project->name}}
                    </td>
                    <td style="vertical-align: top; width: 390px; text-align: right;">
                        <b>{{'Project Number: '}}</b>
                        {{$project->number}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="row">
    <div class="col-md-8">
        <h3>{{trans('labels.materials_and_services')}}  {{trans('labels.filter_by')}} </h3>
        <h4>{{$title}}</h4>
    </div>
</div>
<div class="panel-body">
    <div class="row" style="font-size: 11px;">
        <style>
            .materials-padding {
                padding: 5px 5px !important;
                text-align: left !important;
            }
        </style>
        @if(count($materials) != 0)
            <table style="width: 100%;">
                <thead>
                <tr>
                    <th class="materials-padding">{{trans('labels.master_format')}} {{trans('labels.masterformat.number')}}</th>
                    <th class="materials-padding">{{trans('labels.master_format')}} {{trans('labels.masterformat.title')}}</th>
                    <th class="materials-padding">{{trans('labels.materials_services.name')}}</th>
                    <th class="materials-padding">{{trans('labels.Subcontractor')}}</th>
                    <th class="materials-padding">{{trans('labels.materials_services.position')}}</th>
                    <th class="materials-padding">{{trans('labels.submittal.status')}}</th>
                    <th class="materials-padding">{{trans('labels.submittal.needed')}}</th>
                    <th class="materials-padding">{{trans('labels.materials_services.need_by')}}</th>
                    <th class="materials-padding">{{trans('labels.status')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($materials as $item)
                    <tr>
                        <td class="materials-padding">{{$item->mf_number}}</td>
                        <td class="materials-padding">{{$item->mf_title}}</td>
                        <td class="materials-padding">{{$item->name}}</td>
                        <td class="materials-padding">{{$item->ab_name}}</td>
                        <td class="materials-padding">{{$item->position}}</td>
                        <td class="materials-padding">{{$item->status_name}}</td>
                        <td class="materials-padding">{{($item->subm_needed == 1) ? trans('labels.yes') : trans('labels.no')}}</td>
                        <td class="materials-padding">{{Carbon::parse($item->need_by)->format('m/d/Y')}}</td>
                        <td class="materials-padding">{{$item->er_status_name}}</td>
                    </tr>
                    <tr class="cm-row-separator">
                        <td colspan="8"></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p class="text-center">{{trans('labels.no_records')}}</p>
        @endif
    </div>
</div>
</body>
</html>