@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.create_materials_and_services')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'expediting-report'))
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/expediting-report']) !!}
            @include('projects.materials.partials.createform')
            {!! Form::close() !!}
            @include('errors.list')
        </div>
    </div>
    @endsection