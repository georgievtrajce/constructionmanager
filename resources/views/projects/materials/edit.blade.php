@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
              {{trans('labels.edit_materials_and_services')}}
              <small class="cm-heading-suffix">{{trans('labels.global_material_service').': '.$material->name}}</small>
              <ul class="cm-trail">
                  <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                  <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/expediting-report')}}" class="cm-trail-link">{{trans('labels.materials_and_services')}}</a></li>
                  <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/expediting-report/'.$material->id.'/edit')}}" class="cm-trail-link">{{trans('labels.edit_materials_and_services')}}</a></li>
              </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
                    <a class="btn btn-success cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'6'.'&module_item='.$material->id.'&title='.$material->name.'&mf_number='.$material->mf_number.'&mf_title='.$material->mf_title.'&mf_number_title='.$material->mf_number.' - '.$material->mf_title}}">{{trans('labels.tasks.create')}}</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'expediting-report'))
        </div>
    </div>
<div class="panel">
    <div class="panel-body">
        {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project->id.'/expediting-report/'.$material->id]) !!}
        @include('projects.materials.partials.updateform')
        {!! Form::close() !!}
        @include('errors.list')
    </div>
</div>
@endsection
