@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <header class="cm-heading">
        {{trans('labels.materials_and_services')}}
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/expediting-report')}}" class="cm-trail-link">{{trans('labels.materials_and_services')}}</a></li>
        </ul>
      </header>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        <div class="cm-btn-group cf">
          @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'expediting-report')))
          <a href="{{URL('/projects/'.$project->id.'/expediting-report/create')}}" class="btn btn-success pull-right">{{trans('labels.materials_services.create')}}</a>
          @endif

<div class="pull-right mr5">
                  {!! Form::open(['method'=>'GET','url'=>'/projects/'.$project->id.'/expediting-report/report', 'target'=>'_blank']) !!}
              <div class="form-group">
                {!! Form::hidden('master_format_search',Input::get('master_format_search')) !!}
                {!! Form::hidden('sub_id',Input::get('sub_id')) !!}
                {!! Form::hidden('supplier_subcontractor',Input::get('supplier_subcontractor')) !!}
                {!! Form::hidden('status', Input::get('status')) !!}
                {!! Form::hidden('erStatus', Input::get('er_status')) !!}
                {!! Form::hidden('sort', Input::get('sort','id')) !!}
                {!! Form::hidden('order', Input::get('order','asc')) !!}
                <input class="btn btn-info" type="submit" value="{{trans('labels.save_pdf')}}">
              </div>
              {!! Form::close() !!}
     </div>
          <div class="pull-right">
                @if (Auth::user()->comp_id == $project->comp_id && (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('delete', 'expediting-report')))
                  {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                    <input type="hidden" value="{{URL(Request::path()).'/'}}" id="form-url" />
                    <button disabled id="delete-button" class='btn btn-danger pull-right mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                      {{trans('labels.delete')}}
                    </button>
                  {!! Form::close()!!}
                @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'expediting-report'))
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="panel">
        <div class="panel-body">
          @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
          </div>
          @endif
          @if (sizeof($materials) > 0)
          <div class="cm-filter cm-filter__alt cf">
            <div class="row">
              <!--             <div class="col-sm-4">
                <div class="row">
                  {!! Form::open(['method'=>'GET','url'=>'/projects/'.$project->id.'/expediting-report']) !!}
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('sort', trans('labels.sort_by').':') !!}
                      {!! Form::select('sort',[
                      'id'=>'',
                      'ab_name' => trans('labels.Subcontractor'),
                      'name'=>trans('labels.materials_services.name'),
                      'mf_number' => trans('labels.transmittals.mf_number'),
                      'mf_title' => trans('labels.transmittals.mf_title'),
                      'subm_status'=> trans('labels.submittal.status'),
                      'need_by'=> trans('labels.materials_services.need_by')], Input::get('sort')) !!}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label('order', trans('labels.order_by').':') !!}
                      {!! Form::select('order', array('asc' => trans('labels.ascending'),'desc' => trans('labels.descending')), Input::get('order')) !!}
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      {!! Form::label(trans('labels.sort'), ' &nbsp;') !!}
                      {!! Form::submit(trans('labels.sort'), ['class' => 'btn btn-primary']) !!}
                    </div>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div> -->
              <div class="col-sm-12">
                {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/expediting-report/filter']) !!}
                <div class="row">
                  <div class="col-sm-3">
                    <div class="form-group">
                      {!! Form::label('search_by', trans('labels.search_by').':') !!}
                      {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                           'supplier_subcontractor' => trans('labels.Subcontractor'),
                                                           'name' => trans('labels.materials_services.name'),
                                                           'location' => trans('labels.materials_services.position'),
                                                           ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                      {!! Form::label('search', trans('labels.search').':') !!}
                      {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                      {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                      <input type="hidden" id="project_id" value="{{$project->id}}">
                    </div>
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'supplier_subcontractor')?'show':'hide'}}" id="supplier_subcontractor">
                      {!! Form::label('search-name', trans('labels.search').':') !!}
                      {!! Form::text('supplier_subcontractor',Input::get('supplier_subcontractor'),['class' => 'cm-control-required form-control address-book-auto search-field-input']) !!}
                      {!! Form::hidden('sub_id', Input::get('sub_id'), ['class' => 'address-book-id']) !!}
                    </div>
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                      {!! Form::label('search-name', trans('labels.search').':') !!}
                      {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                    </div>
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'location')?'show':'hide'}}" id="location">
                      {!! Form::label('search-name', trans('labels.search').':') !!}
                      {!! Form::text('location',Input::get('location'),['class' => 'cm-control-required form-control search-field-input']) !!}
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      {!! Form::label('status', trans('labels.submittal.status')) !!}
                      <select name="status" class="form-control">
                        <option value="">{{trans('labels.select_status')}}</option>
                        @foreach ($statuses as $status)
                        @if($status->name != Config::get('constants.submittal_statuses.expediting_report_exclude.rejected'))
                        <option value="{{$status->id}}" @if(Input::get('status') == $status->id) selected @endif>{{$status->name}}</option>
                        @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      {!! Form::label('status', trans('labels.status')) !!}
                      <select name="er_status" class="form-control">
                        <option value="">{{trans('labels.select_status')}}</option>
                        @foreach ($materialStatuses as $status)
                        <option value="{{$status->id}}" @if(Input::get('er_status') == $status->id) selected @endif>{{$status->name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      {!! Form::label('filter', ' &nbsp;') !!}
                      {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary']) !!}
                    </div>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
          @endif
          <div class="row">
            <div class="col-md-12">
              <div class="pull-right">
                <?php echo $materials->appends([
                'id' => Input::get('id'),
                'ab_name' => Input::get('ab_name'),
                'name' => Input::get('name'),
                'mf_number' => Input::get('mf_number'),
                'mf_title' => Input::get('mf_title'),
                'status' => Input::get('status'),
                'sort' => Input::get('sort'),
                'order' => Input::get('order'),
                ])->render(); ?>
              </div>
            </div>
          </div>
          @if(count($materials) > 0)
          <div class="row">
            <div class="col-sm-12">

            </div>
          </div>
          @endif
          @include('projects.materials.partials.table', $materials)
        </div>
      </div>
    </div>
  </div>
  @include('popups.delete_record_popup')
  @endsection