<div class="row">
  <div class="col-md-6">
    <div class="row">
      <div class="col-md-12">
        <div class="">
          <div class="row">
            <div class="col-md-10">
              <div class="form-group">
                <label>{{trans('labels.masterformat.search')}}</label>
                <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                <input type="hidden" class="master-format-id" name="master_format_id" value="">
                <input type="hidden" id="project_id" value="{{$project->id}}">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group cf">
                <div class="mf-cont">
                  <label class="control-label">&nbsp;</label>
                  <a href="javascript:;" id="custom-mf" class="btn btn-primary mb0 pull-right">
                    {{trans('labels.submittals.enter_custom_number_and_title')}}
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-5">
              <div class="form-group">
                <label class="cm-control-required control-label">{{trans('labels.spec_number')}}</label>
                <input type="text" readonly class="mf-number-auto cm-control-required" name="mf_number" value="{{$material->mf_number}}">
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label class="cm-control-required control-label">{{trans('labels.spec_title')}}</label>
                <input type="text" readonly class="mf-title-auto" name="mf_title" value="{{$material->mf_title}}">
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('name', trans('labels.materials_services.name').':', ['class' => 'cm-control-required']) !!} {!! Form::text('name',
          $material->name, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('mas_sub', trans('labels.Subcontractor').':') !!} {!! Form::text('mas_sub', $material->ab_name, ['class'
          => 'form-control']) !!}
          <input type="hidden" name="mas_sub_id" id="mas_sub_id" value="{{$material->ab_id}}" />
        </div>

        <div class="form-group">
          {!! Form::label('position', trans('labels.materials_services.position').':', ['class' => 'cm-control-required']) !!} {!!
          Form::text('position', $material->position, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
          {!! Form::label('status_id', trans('labels.materials_services.status').':') !!}
          <select name="er_status_id" class="form-control">
            <option value=""></option>
            @foreach($materialStatuses as $status)
            <option {{ (($status->id) && $status->id == $material->er_status_id) ? 'selected' : '' }} value="{{$status->id}}">{{$status->name}}</option>
            @endforeach
          </select>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('subm_needed', trans('labels.submittal.needed').':', ['class' => '']) !!}
              <select name="subm_needed" class="form-control">
                <option value="">Select</option>
                <option {{ (isset($material->subm_needed) && $material->subm_needed == '1') ? 'selected' : '' }} value="1">{{trans('labels.yes')}}</option>
                <option {{ (isset($material->subm_needed) && $material->subm_needed == '0') ? 'selected' : '' }} value="0">{{trans('labels.no')}}</option>
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              {!! Form::label('status_id', trans('labels.submittal.status').':', ['class' => ' cm-control-required']) !!}
              <select name="status_id" class="form-control">
                <option value=""></option>
                @foreach($statuses as $status)
                @if($status->name != Config::get('constants.submittal_statuses.expediting_report_exclude.rejected'))
                <option {{ (($status->id) && $status->id == $material->status_id) ? 'selected' : '' }} value="{{$status->id}}">{{$status->name}}</option>
                @endif
                @endforeach
              </select>
            </div>
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('need_by', trans('labels.materials_services.need_by').':', ['class' => ' cm-control-required']) !!} {!! Form::text('need_by',
          Carbon::parse($material->need_by)->format('m/d/Y'), ['class' => 'form-control']) !!}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group cf">
          {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success pull-right']) !!}
        </div>
      </div>
    </div>
  </div>
</div>