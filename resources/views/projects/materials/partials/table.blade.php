<div class="table-responsive">
    <table class="table table-hover table-bordered cm-table-compact">
        @if (sizeof($materials)>0)
        <thead>
            <tr>
                <th></th>
                <th>
                    <?php
                    if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=mf_number&order=desc';
                    } else {
                        $url = Request::url().'?sort=mf_number&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('location'))?'&location='.Input::get('location'):'';
                    $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                    $url .= !empty(Input::get('er_status'))?'&er_status='.Input::get('er_status'):'';
                    $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                    ?>
                    {{trans('labels.master_format_number_and_title')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=name&order=desc';
                    } else {
                        $url = Request::url().'?sort=name&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('location'))?'&location='.Input::get('location'):'';
                    $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                    $url .= !empty(Input::get('er_status'))?'&er_status='.Input::get('er_status'):'';
                    $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                    ?>
                    {{trans('labels.materials_services.name')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'ab_name' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=ab_name&order=desc';
                    } else {
                        $url = Request::url().'?sort=ab_name&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('location'))?'&location='.Input::get('location'):'';
                    $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                    $url .= !empty(Input::get('er_status'))?'&er_status='.Input::get('er_status'):'';
                    $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                    ?>
                    {{trans('labels.Subcontractor')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    {{trans('labels.materials_services.position')}}
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'subm_status' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=subm_status&order=desc';
                    } else {
                        $url = Request::url().'?sort=subm_status&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('location'))?'&location='.Input::get('location'):'';
                    $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                    $url .= !empty(Input::get('er_status'))?'&er_status='.Input::get('er_status'):'';
                    $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                    ?>
                    {{trans('labels.submittal.status')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    {{trans('labels.submittal.needed')}}
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'need_by' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=need_by&order=desc';
                    } else {
                        $url = Request::url().'?sort=need_by&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('location'))?'&location='.Input::get('location'):'';
                    $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                    $url .= !empty(Input::get('er_status'))?'&er_status='.Input::get('er_status'):'';
                    $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                    ?>
                    {{trans('labels.materials_services.need_by')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    {{trans('labels.status')}}
                </th>
            </tr>
        </thead>
        <tbody>
            @for($i=0; $i<sizeof($materials); $i++)
            <tr>
                <td class="text-center">
                    <input type="checkbox" class="multiple-items-checkbox" data-id="{{$materials[$i]->id}}">
                </td>
                <td>
                    @if (isset($materials[$i]->mf_number) && isset($materials[$i]->mf_title))
                    {{$materials[$i]->mf_number.' - '.$materials[$i]->mf_title}}
                    @endif
                </td>
                <td>
                    @if (Auth::user()->comp_id == $project->comp_id && (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('read', 'expediting-report')))
                    @if (isset($materials[$i]->id))
                    <a class="" href="{{URL('projects/'.$materials[$i]->proj_id.'/expediting-report/'.$materials[$i]->id.'/edit')}}">
                        @if (isset($materials[$i]->name)) {{$materials[$i]->name}} @endif
                    </a>
                    @endif
                    @endif
                </td>
                <td>@if (isset($materials[$i]->ab_name)) {{$materials[$i]->ab_name}} @endif</td>
                <td>@if (isset($materials[$i]->position)) {{$materials[$i]->position}} @endif</td>
                <td>@if (isset($materials[$i]->status_name)) {{$materials[$i]->status_name}} @endif</td>
                <td>@if (isset($materials[$i]->subm_needed)) {{($materials[$i]->subm_needed == 1) ? trans('labels.yes') : trans('labels.no')}} @endif</td>
                <td>@if (isset($materials[$i]->need_by)) {{date("m/d/Y", strtotime($materials[$i]->need_by))}} @endif</td>
                <td>@if (!is_null($materials[$i]->status)) {{$materials[$i]->status->short_name}} @endif</td>
            </tr>
            @endfor
        </tbody>
        @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
        @endif
    </table>
</div>