<div class="table-responsive">
        <table class="table table-hover table-bordered cm-table-compact">
            <thead>
                <tr>
                     <th class="text-center">

                     </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'mf_number' && Input::get('order') == 'desc') {
                            $url = Request::url().'?sort=mf_number&order=asc';
                        } else {
                            $url = Request::url().'?sort=mf_number&order=desc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.master_format_number_and_title')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=name&order=desc';
                        } else {
                            $url = Request::url().'?sort=name&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.proposal.name')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'quantity' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=quantity&order=desc';
                        } else {
                            $url = Request::url().'?sort=quantity&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.proposal.quantity')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'unit_price' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=unit_price&order=desc';
                        } else {
                            $url = Request::url().'?sort=unit_price&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.proposal.unit_price')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'value' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=value&order=desc';
                        } else {
                            $url = Request::url().'?sort=value&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.proposal.value')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        {{trans('labels.bid.due_date')}}
                    </th>
                    <th>
                        {{trans('labels.status')}}
                    </th>
                    <th>
                        {{trans('labels.bid.show_bidders')}}
                    </th>
                </tr>
            </thead>
            <tbody>
            @for($i=0; $i<sizeof($proposals); $i++)
                <tr>
                     <td class="text-center">
                         <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$proposals[$i]->id}}">
                     </td>
                    <td>
                        @if (isset($proposals[$i]->mf_number) && isset($proposals[$i]->mf_title))
                            {{$proposals[$i]->mf_number.' - '.$proposals[$i]->mf_title}}
                        @endif
                    </td>
                    <td>

                       @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                            <a class="" href="/projects/{{$proposals[$i]->proj_id}}/bids/{{$proposals[$i]->id}}/edit">@if (isset($proposals[$i]->name)) {{$proposals[$i]->name}} @endif</a>
                        @endif

                    </td>
                    <td>@if (isset($proposals[$i]->quantity)) {{$proposals[$i]->quantity}} @endif</td>
                    <td>@if (isset($proposals[$i]->unit_price)) {{'$'.number_format($proposals[$i]->unit_price, 2)}} @endif</td>
                    <td>@if (isset($proposals[$i]->value)) {{'$'.number_format($proposals[$i]->value, 2)}} @endif</td>
                    <td>@if($proposals[$i]->proposals_needed_by != 0){{date("m/d/Y", strtotime($proposals[$i]->proposals_needed_by))}}@endif</td>
                    <td>@if (isset($proposals[$i]->status)) {{ucfirst($proposals[$i]->status)}} @endif</td>
                    <td align="middle">
                        @if(count($proposals[$i]->suppliers))
                            <a class="btn btn-primary btn-sm show_suppliers glyphicon glyphicon-arrow-down" href="javascript:;"></a>
                        @endif
                    </td>
                </tr>
                @if(sizeof($proposals[$i]->suppliers))
                <tr style="display: none;">
                    <td colspan="12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">{{trans('labels.bid.bidders')}}</h4>
                            </div>
                            <div class="panel-body">

                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'bids')))
                                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form-'.$proposals[$i]->id]) !!}
{{--                                    {!! Form::hidden('ab_id',$proposals[$i]->suppliers[$j]->address_book->id) !!}--}}
                                    <input type="hidden" value="{{URL(Request::path().'/'.$proposals[$i]->id.'/bidders').'/'}}" id="form-url-{{$proposals[$i]->id}}" />
                                    <button disabled id="delete-button-{{$proposals[$i]->id}}"  class='btn btn-xs btn-danger pull-left mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                        {{trans('labels.delete')}}
                                    </button>
                                    {!! Form::close()!!}
                                @endif
                                <div class="cm-spacer-normal cf"></div>

                                    <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                    <tr>
                                        <th class="text-center">
                                        </th>
                                        <th>{{trans('labels.bid.company')}}</th>
                                        <th>{{trans('labels.location')}}</th>
                                        <th>{{trans('labels.contact.name')}}</th>
                                        <th>{{trans('labels.proposal.proposal_1')}}</th>
                                        <th>{{trans('labels.proposal.proposal_2')}}</th>
                                        <th>{{trans('labels.proposal.proposal_final')}}</th>
                                        <th>{{trans('labels.status')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($j= 0; $j<sizeof($proposals[$i]->suppliers); $j++)
                                        <tr>
                                        <td class="text-center">
                                            <input type="checkbox" class="multiple-items-checkbox-bidder" data-bid_id="{{$proposals[$i]->id}}" data-id="{{isset($proposals[$i]->suppliers[$j])?$proposals[$i]->suppliers[$j]->id:''}}" data-ab_id="{{$proposals[$i]->suppliers[$j]->address_book->id}}">
                                        </td>
                                            <td>
                                                 @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                                                    <a class="" href="/projects/{{$proposals[$i]->proj_id}}/bids/{{$proposals[$i]->id}}/bidders/{{$proposals[$i]->suppliers[$j]->id}}/edit">{{$proposals[$i]->suppliers[$j]->ab_name}}</a>
                                                @endif

                                            </td>
                                            <td>
                                                @if (isset($proposals[$i]->suppliers[$j]->office_title))
                                                    {{$proposals[$i]->suppliers[$j]->office_title}}
                                                @endif
                                            </td>
                                            <td>
                                                @if (isset($proposals[$i]->suppliers[$j]))
                                                    {{$proposals[$i]->suppliers[$j]->ab_cont_name}}
                                                @endif
                                            </td>
                                            {{-- @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('show_cost', 'bids'))) --}}
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_1)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_1,2)}} @endif</td>
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_2)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_2,2)}} @endif</td>
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_final)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_final,2)}} @endif</td>
                                            {{-- @endif --}}
                                            <td>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                                                    @if($proposals[$i]->status == Config::get('constants.bids.bid_item_status.open'))
                                                        @if(!is_null($proposals[$i]->suppliers[$j]->project_bidder) && $proposals[$i]->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.pending'))
                                                            <b>{{trans('labels.companies.invitation_pending')}}</b>
                                                        @elseif(!is_null($proposals[$i]->suppliers[$j]->project_bidder) && $proposals[$i]->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.invited'))
                                                            <b>{{trans('labels.companies.invited')}}</b>
                                                        @elseif(!is_null($proposals[$i]->suppliers[$j]->project_bidder) && $proposals[$i]->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.accepted'))
                                                            <b>{{trans('labels.bid.accepted')}}</b>
                                                        @elseif(!is_null($proposals[$i]->suppliers[$j]->project_bidder) && $proposals[$i]->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.rejected'))
                                                            <b>{{trans('labels.bid.rejected')}}</b>
                                                        @else
                                                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'bids')))
                                                                {!! Form::open(['method'=>'POST','url'=>URL('projects/'.$project->id.'/bids/'.$proposals[$i]->id.'/bidders/invite')]) !!}
                                                                {!! Form::hidden('bid_id',$proposals[$i]->id) !!}
                                                                {!! Form::hidden('bidder_id',$proposals[$i]->suppliers[$j]->id) !!}
                                                                {!! Form::hidden('ab_id',$proposals[$i]->suppliers[$j]->address_book->id) !!}
                                                                {!! Form::submit(trans('labels.invite'),['class'=>'btn btn-info btn-sm invite-company']) !!}
                                                                {!! Form::close()!!}
                                                            @endif
                                                        @endif
                                                    @else
                                                        <b>{{trans('labels.proposal.closed')}}</b>
                                                    @endif
                                                @endif
                                            </td>



                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
                @endif
            @endfor
            </tbody>
        </table>
</div>