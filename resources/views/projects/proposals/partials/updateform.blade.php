<style>
    #ui-datepicker-div {
        z-index: 10000!important;
    }
</style>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class=" control-label">{{trans('labels.masterformat.search')}}</label>
                    <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                    <input type="hidden" class="master-format-id" name="master_format_id" value="">
                    <input type="hidden" id="project_id" value="{{$project->id}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group cf">
                    <div class="mf-cont">
                        <label class="control-label">&nbsp;</label>
                        <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">
                            {{trans('labels.submittals.enter_custom_number_and_title')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>{{trans('labels.masterformat.selected')}}</label>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input readonly type="text" class="form-control mf-number-auto" name="master_format_number" value="{{$proposal->mf_number}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input readonly type="text" class="form-control mf-title-auto" name="master_format_title" value="{{$proposal->mf_title}}">
                </div>
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('name', trans('labels.proposal.name').':', ['class' => 'control-label']) !!} {!! Form::text('name', $proposal->name,
            ['class' => 'form-control']) !!}
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('quantity', trans('labels.proposal.quantity').':', ['class' => 'control-label']) !!} {!! Form::text('quantity',
                    $proposal->quantity, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('unit_price', trans('labels.proposal.unit_price').':', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        <span class="input-group-addon">$</span> {!! Form::text('unit_price', number_format($proposal->unit_price,2),
                        ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('value', trans('labels.proposal.value').':', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        <span class="input-group-addon">$</span> {!! Form::text('value', number_format($proposal->value,
                        2), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                {!! Form::label('status', trans('labels.status').':') !!}
                <div class="form-group">
                    <div class="status-cont">
                        <select name="status" id="bid_item_status">
                        <option value="open" @if($proposal->status == Config::get('constants.proposal_statuses.open')) {{'selected'}} @endif>{{trans('labels.proposal.open')}}</option>
                        <option value="closed" @if($proposal->status == Config::get('constants.proposal_statuses.closed')) {{'selected'}} @endif>{{trans('labels.proposal.closed')}}</option>
                    </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                {!! Form::label('status', trans('labels.bid.proposals_needed_by').':') !!}
                <div class="form-group">
                    <div class="form-group">
                        <input type="text" id="need_by" class="form-control" name="proposals_needed_by" value="@if($proposal->proposals_needed_by != 0){{date("
                            m/d/Y ", strtotime($proposal->proposals_needed_by))}}@endif">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('scopeOfWorkContent', trans('labels.proposal.scope_of_work').':') !!}
            <textarea id='scopeOfWorkContent' name="scopeOfWorkContent" class="form-control textEditor">{!! $proposal->scope_of_work !!}</textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="pull-right">
            {!! Form::submit(trans('labels.save'), ['class' => 'btn btn-success']) !!}
        </div>
    </div>
@include('popups.close_unshare_project_popup')