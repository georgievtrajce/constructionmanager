@if(count($proposals))
    <div class="table-responsive">
        <table class="table table-hover cm-table-compact">
            <thead>
            <tr>
                <th>{{trans('labels.master_format_number_and_title')}}</th>
                <th>{{trans('labels.proposal.name')}}</th>
                <th>{{trans('labels.proposal.quantity')}}</th>
                <th>{{trans('labels.proposal.unit_price')}}</th>
                <th>{{trans('labels.proposal.value')}}</th>
                <th>{{trans('labels.status')}}</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @for($i=0; $i<sizeof($proposals); $i++)
                <tr>
                    <td>
                        @if (isset($proposals[$i]->mf_number) && isset($proposals[$i]->mf_title))
                            {{$proposals[$i]->mf_number.' - '.$proposals[$i]->mf_title}}
                        @endif
                    </td>
                    <td>@if (isset($proposals[$i]->name)) {{$proposals[$i]->name}} @endif</td>
                    <td>@if (isset($proposals[$i]->quantity)) {{$proposals[$i]->quantity}} @endif</td>
                    <td>@if (isset($proposals[$i]->unit_price)) {{'$'.number_format($proposals[$i]->unit_price, 2)}} @endif</td>
                    <td>@if (isset($proposals[$i]->value)) {{'$'.number_format($proposals[$i]->value, 2)}} @endif</td>
                    <td>@if (isset($proposals[$i]->status)) {{ucfirst($proposals[$i]->status)}} @endif</td>
                    <td>
                        @if(count($proposals[$i]->suppliers))
                            <a class="btn btn-info btn-sm show_suppliers" href="javascript:;">{{trans('labels.bid.show_bidders')}}</a>
                        @endif
                    </td>
                </tr>
                @if(sizeof($proposals[$i]->suppliers))
                    <tr style="display: none;">
                        <td colspan="10">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">{{trans('labels.bid.bidders')}}</h4>
                                </div>
                                <div class="panel-body">

                                    <table class="table table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{trans('labels.bid.company')}}</th>
                                            <th>{{trans('labels.location')}}</th>
                                            <th>{{trans('labels.contact.name')}}</th>
                                            <th>{{trans('labels.proposal.proposal_1')}}</th>
                                            <th>{{trans('labels.proposal.proposal_2')}}</th>
                                            <th>{{trans('labels.proposal.proposal_final')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @for($j= 0; $j<sizeof($proposals[$i]->suppliers); $j++)
                                            <tr>
                                                <td>
                                                    {{$proposals[$i]->suppliers[$j]->ab_name}}
                                                </td>
                                                <td>
                                                    @if (isset($proposals[$i]->suppliers[$j]->office_title))
                                                        {{$proposals[$i]->suppliers[$j]->office_title}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (isset($proposals[$i]->suppliers[$j]))
                                                        {{$proposals[$i]->suppliers[$j]->ab_cont_name}}
                                                    @endif
                                                </td>
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_1)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_1,2)}} @endif</td>
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_2)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_2,2)}} @endif</td>
                                                <td>@if (isset($proposals[$i]->suppliers[$j]->proposal_final)){{'$'.number_format($proposals[$i]->suppliers[$j]->proposal_final,2)}} @endif</td>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Permissions::can('write', 'proposals')))
                                                    <td><a class="btn cm-btn-secondary btn-sm" href="{{URL('/projects/'.$proposals[$i]->proj_id.'/shared/bids/'.$proposals[$i]->id.'/bidders/'.$proposals[$i]->suppliers[$j]->id)}}">{{trans('labels.bid.view_bidder')}}</a></td>
                                                @endif
                                            </tr>
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endif
            @endfor
            </tbody>
        </table>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="pull-right">
                <?php echo $proposals->appends([
                        'sort'=>Input::get('sort'),
                        'order'=>Input::get('order'),
                        'master_format_search'=>Input::get('master_format_search'),
                        'master_format_id'=>Input::get('master_format_id'),
                        'status'=>Input::get('status'),
                        'mf_number'=>Input::get('mf_number'),
                        'mf_title'=>Input::get('mf_title'),
                        'name'=>Input::get('name'),
                ])->render(); ?>
            </div>
        </div>
    </div>
@endif