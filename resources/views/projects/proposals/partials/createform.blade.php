<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label>{{trans('labels.masterformat.search')}}</label>
                    <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off"
                           value="{{Input::old('master_format_search')}}">
                    <input type="hidden" class="master-format-id" name="master_format_id" value="{{Input::old('master_format_id')}}">
                    <input type="hidden" id="project_id" value="{{$project->id}}">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group cf">
                    <div class="mf-cont">
                        <label class="control-label">&nbsp;</label>
                        <a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">
                            {{trans('labels.submittals.enter_custom_number_and_title')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label>{{trans('labels.masterformat.selected')}}</label>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input readonly type="text" class="form-control mf-number-auto" name="master_format_number" value="{{Input::old('master_format_number')}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <input readonly type="text" class="form-control mf-title-auto" name="master_format_title" value="{{Input::old('master_format_title')}}">
                </div>
            </div>
        </div>
        <style>
            #ui-datepicker-div {
                z-index: 10000!important;
            }
        </style>
        <div class="form-group">
            {!! Form::label('name', trans('labels.proposal.name').':') !!} {!! Form::text('name', Input::old('name')) !!}
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::label('quantity', trans('labels.proposal.quantity').':') !!} {!! Form::text('quantity', Input::old('quantity'))
                    !!}
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('unit_price', trans('labels.proposal.unit_price').':') !!}
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="unit_price" value="{{Input::old('unit_price')}}">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('value', trans('labels.proposal.value').':') !!}
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input type="text" name="value" value="{{Input::old('value')}}">
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                {!! Form::label('status', trans('labels.status').':') !!}
                <div class="form-group">
                <div class="status-cont">
                    <select name="status">
                        <option value="open">{{trans('labels.proposal.open')}}</option>
                        <option value="closed">{{trans('labels.proposal.closed')}}</option>
                    </select>
                </div>
                </div>
            </div>
            <div class="col-md-4">
                {!! Form::label('status', trans('labels.bid.proposals_needed_by').':') !!}
                <div class="form-group">
                    <input type="text" id="need_by" class="form-control" name="proposals_needed_by" value="{{old('proposals_needed_by') }}">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group cf">
            {!! Form::label('scopeOfWorkContent', trans('labels.proposal.scope_of_work').':') !!}
            <textarea id='scopeOfWorkContent' name="scopeOfWorkContent" class="form-control textEditor">{!! Input::old('scopeOfWorkContent') !!}</textarea>
        </div>
    </div>
    <div class="col-md-12">
        {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success pull-right']) !!}
    </div>
</div>