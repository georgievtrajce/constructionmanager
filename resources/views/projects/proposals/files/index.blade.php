@extends('layouts.tabs')

@section('title')
{{trans('labels.proposal.plural')}}
<ul class="cm-trail">
    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
     class="cm-trail-link">{{trans('labels.Proposal')}}: {{$project->name}}</a></li>
     <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/proposals" class="cm-trail-link">{{trans('labels.proposal.plural')}}</a></li>
 </ul>
 @endsection

 @section('tabs')
 @include('projects.partials.tabs', array('activeTab' => 'bids'))
 @endsection

 @section('tabsContent')
 <div class="panel">
    <div class="panel-body">
        @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
        </div>
        @endif

        <div class="row">
            {!! Form::open(['method'=> 'POST', 'url'=>Request::path(), 'files' => true]) !!}
            <div class="col-md-5">
                <h4>{{trans('labels.file.add')}}</h4>
                <div class="form-group">
                    <label for="name">{{trans('labels.name')}}:</label>
                    <input id="name" name="name" type="text" value="{{Input::old('name')}}">
                </div>
                <div class="form-group">
                    <label for="number">{{trans('labels.number')}}:</label>
                    <input id="number" name="number" type="text" value="{{Input::old('number')}}">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="proposal_file">{{trans('labels.File')}}:</label>
                            <input type="file" name="proposal_file" id="file">
                            <input type="hidden" name="file_id" id="file_id" value="{{old('file_id') }}">
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div id="file_wait" style="display: none;">
                            <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="40px">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <button type="button" id="file-upload" class="btn btn-info btn-sm cm-btn-fixer pull-right">
                                {{trans('labels.upload')}}
                            </button>
                        </div>
                    </div>

                </div>
                <div class="cm-spacer-xs"></div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="hidden" id="file_type" name="type" value="bid">
                            <input type="hidden" id="project_file_type" name="project_file_type" value="bids">
                            <button class="btn btn-success db" type="submit">{{trans('labels.save')}}</button>
                        </div>
                    </div>
                </div>

            </div>
            <input type="hidden" id="supp_id" name="supp_id" value="">
            <input type="hidden" id="supplier_id" name="supplier_id" value="{{$supplierId}}">
            <input type="hidden" id="proj_id" name="proj_id" value="{{$project->id}}">
            {!! Form::close() !!}
        </div>
    </div>
    @if (sizeof($files))
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>{{trans('labels.name')}}</th>
                <th>{{trans('labels.number')}}</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @for($i=0; $i<sizeof($files); $i++)
            <tr>
                <td>
                    <a class="download" href="javascript:;">{{$files[$i]->name}}</a><br/>
                    <input type="hidden" class="s3FilePath" id="{{$files[$i]->f_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/supplier_'.$supplierId.'/'.$files[$i]->file_name}}">
                </td>
                <td>{{$files[$i]->number}}</td>
                <td><a href="{{URL('projects/'.$project->id.'/proposals/'.$id.'/suppliers/'.$supplierId.'/files/'.$files[$i]->file_id.'/edit')}}" class="btn btn-primary btn-sm">{{trans('labels.file.edit')}}</a></td>
                <td>
                    {!! Form::open(['method'=>'DELETE', 'url'=>(Request::path().'/'.$files[$i]->id)]) !!}
                    <input type="hidden" name="file_name" value="{{$files[$i]->file_name}}"/>
                    {!! Form::submit(trans('labels.delete'),['class'=>'btn btn-danger btn-sm', 'onclick' => 'return confirm("'.trans('labels.file.delete_modal').'")']) !!}
                    {!! Form::close()!!}
                </td>

            </tr>
            @endfor
        </tbody>
    </table>
    @else
    <div>
        <p class="text-center">{{trans('labels.no_files')}}</p>
    </div>
    @endif
</div>
@include('errors.list')
@endsection