@extends('layouts.tabs')

@section('title')
{{trans('labels.file.edit')}}
<ul class="cm-trail">

    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
     class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
     <li class="cm-trail-item"><a href="/projects/{{$project->id}}/proposals" class="cm-trail-link">{{trans('labels.proposal.files')}}</a></li>
     <li class="cm-trail-item active"><a
        href="/projects/{{$project->id}}/proposals/{{$supplier->prop_id}}/suppliers/{{$supplier->id}}/files/"
        class="cm-trail-link">{{trans('labels.file.edit')}}</a></li>
    </ul>
    @endsection

    @section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'bids', 'projectId' => $project->id))
    @endsection
    @section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project->id.'/proposals/'.$supplier->prop_id.'/suppliers/'.$supplier->id.'/files/'.$file->file_id, 'files' => true])!!}
                <div class="col-md-5">
                    <h4>{{trans('labels.file.add')}}</h4>
                    <div class="form-group">
                        {!! Form::label('name', trans('labels.name').':') !!}
                        {!! Form::text('name', $file->name, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('number', trans('labels.number').':') !!}
                        {!! Form::text('number', $file->number, ['class' => 'form-control']) !!}
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="proposal_file">{{trans('labels.File')}}:</label>
                                <input type="file" name="proposal_file" id="file">
                                <input type="hidden" name="file_id" id="file_id" value="{{$file->f_id}}">
                                <p>
                                    {{trans('labels.file.current')}}:
                                    <a class="download" href="javascript:;">{{$file->file_name}}</a>
                                    <input type="hidden" class="s3FilePath" id="{{$file->f_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/supplier_'.$supplier->id.'/'.$file->file_name}}">
                                </p>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <div id="file_wait" style="display: none;">
                                <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="40px">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <button type="button" id="file-upload" class="btn btn-success btn-sm">
                                    {{trans('labels.upload')}}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="hidden" id="file_type" name="type" value="bid">
                                <input type="hidden" id="supplier_id" name="supplier_id" value="{{$supplier->id}}">
                                <input type="hidden" id="project_file_type" name="project_file_type" value="bids">
                                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-sm btn-success db']) !!}
                            </div>
                        </div>
                    </div>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
        @include('errors.list')
    </div>
    @endsection