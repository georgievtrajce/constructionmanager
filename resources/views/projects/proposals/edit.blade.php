@extends('layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-7">
      <header class="cm-heading">
        {{trans('labels.bid.edit_bid_item')}}<small class="cm-heading-suffix">{{trans('labels.Bid')}}: {{$proposal->name}}</small>
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/bids')}}" class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
          <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/bids/'.$proposal->id.'/edit')}}" class="cm-trail-link">{{trans('labels.bid.edit_bid_item')}}</a></li>
        </ul>
      </header>
    </div>
    <div class="col-md-5">
      <div class="cm-btn-group cm-pull-right cf">
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
            <a class="btn btn-success cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'4'.'&module_item='.$proposal->id.'&title='.$proposal->name.'&mf_number='.$proposal->mf_number.'&mf_title='.$proposal->mf_title.'&mf_number_title='.$proposal->mf_number.' - '.$proposal->mf_title}}">{{trans('labels.tasks.create')}}</a>
        @endif
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'bids'))
    </div>
  </div>
  <div class="panel">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12">
          @include('projects.proposals.partials.tabs-inner')
        </div>
      </div>
      @include('errors.list')
      <div class="row mt10">
        <div class="tab-content">
          <div id="info" class="tab-pane fade">
            {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$proposal->proj_id.'/bids/'.$proposal->id, 'role'=> 'form']) !!}
            @include('projects.proposals.partials.updateform')
            {!! Form::close() !!}
          </div>
          <div id="bidders" class="tab-pane fade in active">

              <div class="col-md-12">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'bids')))
                  <a class="btn btn-xs btn-success mr5 pull-left" href="/projects/{{$project->id}}/bids/{{$proposal->id}}/bidders/create">{{trans('labels.bid.add_bidder')}}</a>
                @endif
                @if(sizeof($proposal->suppliers))
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'bids')))
                  {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form-'.$proposal->id]) !!}
                  {{--                                    {!! Form::hidden('ab_id',$proposal->suppliers[$j]->address_book->id) !!}--}}
                  <input type="hidden" value="{{URL('projects/'.$proposal->proj_id.'/bids/'.$proposal->id.'/bidders').'/'}}" id="form-url-{{$proposal->id}}" />
                  <button disabled id="delete-button-{{$proposal->id}}"  class='btn btn-xs btn-danger pull-left mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                    {{trans('labels.delete')}}
                  </button>
                  {!! Form::close()!!}
                @endif
                <div class="pull-right">
                  @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/bids/'.$proposal->id.'/report','target'=>'_blank']) !!}
                    {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                    {!! Form::hidden('report_status',Input::get('status')) !!}
                    {!! Form::hidden('report_sort',Input::get('sort','proposals.id')) !!}
                    {!! Form::hidden('report_order',Input::get('order','asc')) !!}
                    {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-xs btn-info mr5 pull-right']) !!}
                    {!! Form::close() !!}
                  @endif
                </div>
                <div class="cm-spacer-normal cf"></div>
                  <div class="table-responsive">
                    <table class="table table-hover table-bordered cm-table-compact">
                      <thead>
                      <tr>
                        <th class="text-center">
                        </th>
                        <th>{{trans('labels.bid.company')}}</th>
                        <th>{{trans('labels.location')}}</th>
                        <th>{{trans('labels.contact.name')}}</th>
                        <th>{{trans('labels.proposal.proposal_1')}}</th>
                        <th>{{trans('labels.proposal.proposal_2')}}</th>
                        <th>{{trans('labels.proposal.proposal_final')}}</th>
                        <th>{{trans('labels.status')}}</th>
                      </tr>
                      </thead>
                      <tbody>
                      @for($j= 0; $j<sizeof($proposal->suppliers); $j++)
                        <tr>
                          <td class="text-center">
                            <input type="checkbox" class="multiple-items-checkbox-bidder" data-bid_id="{{$proposal->id}}" data-id="{{isset($proposal->suppliers[$j])?$proposal->suppliers[$j]->id:''}}" data-ab_id="{{$proposal->suppliers[$j]->address_book->id}}">
                          </td>
                          <td>
                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                              <a class="" href="/projects/{{$proposal->proj_id}}/bids/{{$proposal->id}}/bidders/{{$proposal->suppliers[$j]->id}}/edit">{{$proposal->suppliers[$j]->ab_name}}</a>
                            @endif

                          </td>
                          <td>
                            @if (isset($proposal->suppliers[$j]->office_title))
                              {{$proposal->suppliers[$j]->office_title}}
                            @endif
                          </td>
                          <td>
                            @if (isset($proposal->suppliers[$j]))
                              {{$proposal->suppliers[$j]->ab_cont_name}}
                            @endif
                          </td>
                          {{-- @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('show_cost', 'bids'))) --}}
                          <td>@if (isset($proposal->suppliers[$j]->proposal_1)){{'$'.number_format($proposal->suppliers[$j]->proposal_1,2)}} @endif</td>
                          <td>@if (isset($proposal->suppliers[$j]->proposal_2)){{'$'.number_format($proposal->suppliers[$j]->proposal_2,2)}} @endif</td>
                          <td>@if (isset($proposal->suppliers[$j]->proposal_final)){{'$'.number_format($proposal->suppliers[$j]->proposal_final,2)}} @endif</td>
                          {{-- @endif --}}
                          <td>
                            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                              @if($proposal->status == Config::get('constants.bids.bid_item_status.open'))
                                @if(!is_null($proposal->suppliers[$j]->project_bidder) && $proposal->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.pending'))
                                  <b>{{trans('labels.companies.invitation_pending')}}</b>
                                @elseif(!is_null($proposal->suppliers[$j]->project_bidder) && $proposal->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.invited'))
                                  <b>{{trans('labels.companies.invited')}}</b>
                                @elseif(!is_null($proposal->suppliers[$j]->project_bidder) && $proposal->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.accepted'))
                                  <b>{{trans('labels.bid.accepted')}}</b>
                                @elseif(!is_null($proposal->suppliers[$j]->project_bidder) && $proposal->suppliers[$j]->project_bidder->status == Config::get('constants.bids.status.rejected'))
                                  <b>{{trans('labels.bid.rejected')}}</b>
                                @else
                                  @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'bids')))
                                    {!! Form::open(['method'=>'POST','url'=>URL('projects/'.$project->id.'/bids/'.$proposal->id.'/bidders/invite')]) !!}
                                    {!! Form::hidden('bid_id',$proposal->id) !!}
                                    {!! Form::hidden('bidder_id',$proposal->suppliers[$j]->id) !!}
                                    {!! Form::hidden('ab_id',$proposal->suppliers[$j]->address_book->id) !!}
                                    {!! Form::submit(trans('labels.invite'),['class'=>'btn btn-info btn-sm invite-company']) !!}
                                    {!! Form::close()!!}
                                  @endif
                                @endif
                              @else
                                <b>{{trans('labels.proposal.closed')}}</b>
                              @endif
                            @endif
                          </td>
                        </tr>
                      @endfor
                      </tbody>
                    </table>
                  </div>
                  @endif
              </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('popups.delete_record_popup')
@endsection