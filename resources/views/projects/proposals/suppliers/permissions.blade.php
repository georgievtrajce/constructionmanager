@extends('layouts.master')
@section('content')
<style>
.radio label span.check {
left: 10px!important;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.bid.bidder_permissions')}}
                <small class="cm-heading-suffix">{{trans('labels.proposal.company').': '.(is_null($bidder->address_book) ? 'Unknown' : $bidder->address_book->name)}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/bids')}}" class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/bids/'.$bidder->prop_id.'/edit')}}" class="cm-trail-link">{{trans('labels.bid.edit_bid_item')}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project['id']}}/bids/{{$bidder->prop_id}}/bidders/{{$bidder->id}}/edit" class="cm-trail-link">{{trans('labels.bid.edit_bidder')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{Request::url()}}" class="cm-trail-link">{{trans('labels.bid.bidder_permissions')}}:</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'bids', 'projectId' => $project->id))
        </div>
        <div class="col-sm-12">
            <div class="row">
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                <div class="col-md-12">
                    <div class="panel">
                        {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/bids/'.$bidder->prop_id.'/bidders/'.$bidder->id.'/permissions', 'role'=> 'form']) !!}
                        <div class="panel-body">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="select_all" id="select_all_permissions"> {{trans('labels.select_all')}}
                                </label>
                            </div>
                            <!--  <h3>{{trans('labels.manage_users.modules')}}</h3> -->
                            @if(count($bidderPermissions))
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                        <tr>
                                            <th style="width: 25%">{{trans('labels.manage_users.module')}}</th>
                                            <th style="width: 25%">{{trans('labels.manage_users.read')}}</th>
                                            <th>{{trans('labels.manage_users.write')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php //dd($bidderPermissions); ?>
                                        @foreach ($bidderPermissions as $permission)
                                        @if ($permission->entity_type == 1 &&
                                             $permission->module_name != Config::get('constants.modules.address-book') &&
                                             $permission->module_name != Config::get('constants.modules.all-projects') &&
                                             $permission->module_name != Config::get('constants.modules.projects') &&
                                             $permission->module_name != Config::get('constants.modules.blog') &&
                                             $permission->module_name != Config::get('constants.modules.submittals') &&
                                             $permission->module_name != Config::get('constants.modules.contracts') &&
                                             $permission->module_name != Config::get('constants.modules.expediting-reports') &&
                                             $permission->module_name != Config::get('constants.modules.companies-module') &&
                                             $permission->module_name != Config::get('constants.modules.pcos') &&
                                             $permission->module_name != Config::get('constants.modules.tasks')
                                             )
                                        <?php
                                        $mainModuleReadClass = '';
                                        $projectEventClass = $permission->module_name == 'Projects' ? 'project-event ' : '';
                                        if($permission->module_name == 'Projects' || $permission->module_name == 'Address Book' || $permission->module_name == 'Custom Categories')
                                        {
                                            $readPermissionsClass = '';
                                            $eventPermissionsClass = '';
                                            $mainModuleEventClass = 'main-module-event ';
                                        } else {
                                            $readPermissionsClass = 'read-permission ';
                                            $eventPermissionsClass = 'event-permissions ';
                                            $mainModuleEventClass = '';
                                        }
                                        ?>
                                        <tr>
                                            <td>{{$permission->module_name}}</td>
                                            <td>
                                                <div class="checkbox pull-left">
                                                    <label>
                                                        <input <?php if($permission->module_name == Config::get('constants.modules.bids')) { ?> data-id="{{$permission->module_display_name}}" id="parentCheckbox-{{$permission->module_display_name}}" <?php } ?> <?php if($permission->module_display_name == Config::get('constants.pcos')){echo 'id="pco_permission"';} ?> <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="parentCheckbox permission mandatory-permissions <?php echo ($permission->module_name == 'Address Book') ? 'address-book-read ' : ''; echo ($permission->module_name == 'Projects') ? 'projects-module ' : ''; echo $readPermissionsClass; echo $mainModuleReadClass; ?>" name="module_read[]" value="{{$permission->module_id}}">
                                                        @if ($permission->module_name == Config::get('constants.modules.bids'))
                                                            Own bids only
                                                        @endif
                                                    </label>
                                                </div>
                                                @if($permission->module_display_name == Config::get('constants.pcos'))
                                                <div class="radio pull-left">
                                                    <label class="radio-inline"><input class="pco_permission_type" <?php echo ($permission->read == 0) ? 'disabled' : ''; ?> <?php echo ($permission->pco_permission_type == Config::get('constants.pco_permission_type.all_pcos')) ? 'checked' : ''; ?> type="radio" name="pco_permission_type" value="{{Config::get('constants.pco_permission_type.all_pcos')}}">{{trans('labels.pcos.all_pcos')}}</label>
                                                    <label class="radio-inline"><input class="pco_permission_type" <?php echo ($permission->read == 0) ? 'disabled' : ''; ?> <?php echo ($permission->pco_permission_type == Config::get('constants.pco_permission_type.his_pcos')) ? 'checked' : ''; ?> type="radio" name="pco_permission_type" value="{{Config::get('constants.pco_permission_type.his_pcos')}}">{{trans('labels.pcos.own_pcos')}}</label>
                                                </div>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($permission->module_name == Config::get('constants.modules.bids'))
                                                    <div class="checkbox pull-left">
                                                        <label>
                                                            <input <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" data-id="{{$permission->module_display_name}}" id="childCheckbox-{{$permission->module_display_name}}" class="childCheckbox permission mandatory-permissions <?php echo $readPermissionsClass;?>" name="module_write[]" value="{{$permission->module_id}}">
                                                            Own bids only
                                                        </label>
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <p>{{trans('labels.manage_users.permissions_warning')}}</p>
                            @endif
                            <!-- <h3>{{trans('labels.project_files')}}</h3> -->
                            @if (count($bidderPermissions))
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                        <tr>
                                            <th style="width: 25%">{{trans('labels.project_files')}}</th>
                                            <th style="width: 25%">{{trans('labels.manage_users.read')}}</th>
                                            <th>{{trans('labels.manage_users.write')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($bidderPermissions as $permission)
                                        @if ($permission->entity_type == 2 &&
                                             $permission->file_type_name != Config::get('constants.modules.daily-reports') &&
                                             $permission->file_type_name != Config::get('constants.modules.daily-reports-images') &&
                                             $permission->file_type_name != Config::get('constants.modules.daily-reports-files') &&
                                             $permission->file_type_name != Config::get('constants.modules.correspondence-emails') &&
                                             $permission->file_type_name != Config::get('constants.modules.permits') &&
                                             $permission->file_type_name != Config::get('constants.modules.work-schedules') &&
                                             $permission->file_type_name != Config::get('constants.modules.punch-lists') &&
                                             $permission->file_type_name != Config::get('constants.modules.meeting-minutes') &&
                                             $permission->file_type_name != Config::get('constants.modules.closeout-documents') &&
                                             $permission->file_type_name != Config::get('constants.modules.billing')
                                        )
                                        <tr>
                                            <td>{{$permission->file_type_name}}</td>
                                            <td>
                                                <div class="checkbox">
                                                    <label>
                                                        <input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="mandatory-permissions permission read-permission" name="project_file_read[]" value="{{$permission->file_type_id}}">
                                                    </label>
                                                </div>
                                            </td>
                                            <td></td>
                                        </tr>
                                        @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <p>{{trans('labels.manage_users.permissions_warning')}}</p>
                            @endif
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right">
                                    {{trans('labels.save')}}
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection