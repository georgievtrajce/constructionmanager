@extends('layouts.tabs')

@section('title')
    {{trans('labels.supplier.create')}}: <small class="cm-heading-suffix">{{trans('labels.Proposal')}}: {{$proposal->name}}</small>
    <ul class="cm-trail">
        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
        <li class="cm-trail-item"><a href="/projects/{{$project->id}}/bids" class="cm-trail-link">{{trans('labels.bid.plural')}}:</a></li>
        <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/bids/{{$proposal->id}}/bidders/create" class="cm-trail-link">{{trans('labels.bid.create_bidder')}}:</a></li>
    </ul>
@endsection

@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'bids'))
@endsection

@section('tabsContent')
    <div class="panel panel-info">
        <div class="panel-body">
            {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$proposal->proj_id.'/bids/'.$proposal->id.'/bidders']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('ab_id', trans('labels.bid.company').':') !!}
                        {!! Form::text('ab_id', Input::get('ab_id'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('location', trans('labels.location').':') !!}
                        {!! Form::text('addr_id', Input::get('addr_id'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('proposal_1', trans('labels.proposal.proposal_1').':') !!}
                        {!! Form::text('proposal_1', Input::get('proposal_1'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('proposal_2', trans('labels.proposal.proposal_2').':') !!}
                        {!! Form::text('proposal_2', Input::get('proposal_2'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('proposal_final', trans('labels.proposal.proposal_final').':') !!}
                        {!! Form::text('proposal_final', Input::get('proposal_final'), ['class' => 'form-control']) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('ab_cont_id', trans('labels.proposal.contact').':') !!}
                        {!! Form::text('ab_cont_id', Input::get('ab_cont_id'), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group pull-right">
                        {!! Form::submit(trans('labels.save')),['class' => 'form-control btn btn-sm btn-primary']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            @include('errors.list')
        </div>
    </div>
@endsection