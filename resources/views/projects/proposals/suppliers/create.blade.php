@extends('layouts.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.bid.create_bidder')}}
                <small class="cm-heading-suffix">{{trans('labels.Bid')}}: {{$proposal->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/bids" class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/bids/{{$proposal->id}}/bidders/create" class="cm-trail-link">{{trans('labels.bid.create_bidder')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
   <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'bids'))
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            @include('errors.list')
            {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$proposal->proj_id.'/bids/'.$proposal->id.'/bidders', 'role'=> 'form']) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('prop_ab', trans('labels.bid.company').' ('.trans('labels.import_from_ab').'):', ['class' => 'control-label']) !!}
                                <input class="form-control ui-autocomplete-input" name="prop_ab" type="text" id="prop_ab" />
                            </div>
                            <input type="hidden" name="prop_ab_id" id="prop_ab_id" value="{{Input::old('prop_ab_id')}}" />
                            <a href="{{URL('/address-book/create')}}" class="btn btn-primary">{{trans('labels.address_book.add_new_book_item')}}
                        <div class="ripple-wrapper"></div>
                    </a>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('sub_office', trans('labels.location').':', ['class' => 'control-label']) !!}
                                <select name="prop_addr" id="prop_addr">
                            <option value="">{{trans('labels.select_location')}}</option>
                        </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('cont_id', trans('labels.proposal.contact').':', ['class' => 'control-label']) !!}
                                <select name="cont_id" id="cont_id">
                            <option value="">{{trans('labels.select_contact')}}</option>
                        </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    {!! Form::label('proposal_1', trans('labels.proposal.proposal_1').':', ['class' => 'control-label']) !!}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span> {!! Form::text('proposal_1', Input::old('proposal_1'),
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">

                            <!-- upload select input -->
                            <div class="input-group">
                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                      <input type="file" style="display: none;" name="proposal_one_file" id="proposal_one_file" multiple>
                                       <input type="hidden" name="proposal_one_file_id" id="proposal_one_file_id" value="{{old('proposal_one_file_id') }}">
                                      <span class="glyphicon glyphicon-upload"></span>
                                    </span>
                                  </label>
                            </div>

                        </div>
                        <div class="col-sm-4">
                            <button type="button" id="proposal_one_upload" class="btn cm-btn-secondary pull-left">
                                <div id="proposal_one_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                </div>
                                {{trans('labels.upload')}}
                                </button>

                        </div>
                    </div>
                    <div class="row proposal_one_file_main_message_container">
                        <div id="proposal_one_file_message_container" class="col-md-8 col-md-offset-4 proposal_one_file_message_container message_container"
                            style="color: #49A078;"></div>
                    </div>

                    {!! Form::label('proposal_2', trans('labels.proposal.proposal_2').':', ['class' => 'control-label']) !!}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span> {!! Form::text('proposal_2', Input::old('proposal_2'),
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">



                            <!-- upload select input -->
                            <div class="input-group">
                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                      <input type="file" style="display: none;" name="proposal_two_file" id="proposal_two_file" multiple>
                                       <input type="hidden" name="proposal_two_file_id" id="proposal_two_file_id" value="{{old('proposal_two_file_id') }}">
                                      <span class="glyphicon glyphicon-upload"></span>
                                    </span>
                                  </label>
                            </div>



                        </div>
                        <div class="col-sm-4">
                            <button type="button" id="proposal_two_upload" class="btn  cm-btn-secondary pull-left">
                                <div id="proposal_two_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                </div>
                                {{trans('labels.upload')}}
                                </button>

                        </div>
                    </div>
                    <div class="row proposal_two_file_main_message_container">
                        <div id="proposal_two_file_message_container" class="col-md-8 col-md-offset-4 proposal_two_file_message_container message_container"
                            style="color: #49A078;"></div>
                    </div>


                    {!! Form::label('proposal_final', trans('labels.proposal.proposal_final').':', ['class' => 'control-label']) !!}
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span> {!! Form::text('proposal_final', Input::old('proposal_final'),
                                    ['class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">




                            <!-- upload select input -->
                            <div class="input-group">
                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                      <input type="file" style="display: none;" name="proposal_final_file" id="proposal_final_file" multiple>
                                       <input type="hidden" name="proposal_final_file_id" id="proposal_final_file_id" value="{{old('proposal_final_file_id') }}">
                                      <span class="glyphicon glyphicon-upload"></span>
                                    </span>
                                  </label>
                            </div>


                        </div>
                        <div class="col-sm-4">
                            <button type="button" id="proposal_final_upload" class="btn  cm-btn-secondary pull-left">
                                    <div id="proposal_final_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                    {{trans('labels.upload')}}
                                </button>
                        </div>
                    </div>
                    <div class="row proposal_final_file_main_message_container">
                        <div id="proposal_final_file_message_container" class="col-md-8 col-md-offset-4 proposal_final_file_message_container message_container"
                            style="color: #49A078;"></div>
                    </div>

                </div>
                <div class="col-sm-12">
                    <input type="hidden" id="type" name="type" value="{{Config::get('constants.bids.plural')}}"> {!! Form::submit(trans('labels.save'),['class'
                    => 'btn btn-success pull-right']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @include('projects.partials.successful-upload') @include('projects.partials.error-upload') @include('projects.partials.upload-limitation')
    @include('popups.alert_popup') @include('popups.delete_record_popup') @include('popups.approve_popup') @endsection