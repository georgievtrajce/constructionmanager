<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('prop_ab', trans('labels.proposal.supplier').':') !!}
            {!! Form::text('prop_ab', Input::get('prop_ab'), ['class' => 'form-control']) !!}
            <input type="hidden" name="prop_ab_id" id="prop_ab_id" value="{{Input::old('prop_ab_id')}}"/>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('prop_addr', trans('labels.location').':') !!}
            <select name="prop_addr" id="prop_addr">
                <option>{{trans('labels.select_location')}}</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('proposal_1', trans('labels.proposal.proposal_1').':') !!}
            {!! Form::text('proposal_1', Input::get('proposal_1'), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('proposal_2', trans('labels.proposal.proposal_2').':') !!}
            {!! Form::text('proposal_2', Input::get('proposal_2'), ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('proposal_final', trans('labels.proposal.proposal_final').':') !!}
            {!! Form::text('proposal_final', Input::get('proposal_final'), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::label('prop_ab_cont', trans('labels.proposal.supplier').':') !!}
            <select name="prop_ab_cont" id="prop_ab_cont">
                <option>{{trans('labels.select_contact')}}</option>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <div class="form-group pull-right">
            {!! Form::submit(trans('labels.save'),['class' => 'form-control btn btn-sm btn-primary']) !!}
        </div>
    </div>
</div>