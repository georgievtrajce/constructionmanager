@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <header class="cm-heading">
                {{trans('labels.bid.edit_bidder')}}
                <small class="cm-heading-suffix">{{trans('labels.Bid').': '.$proposal->name}}</small>
                <small class="cm-heading-suffix">{{trans('labels.proposal.company').': '.(is_null($supplier->address_book) ? 'Unknown' : $supplier->address_book->name)}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project['id'])}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project['id']}}/bids" class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/bids/'.$supplier->prop_id.'/edit')}}" class="cm-trail-link">{{trans('labels.bid.edit_bid_item')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project['id']}}/bids/{{$supplier->prop_id}}/bidders/{{$supplier->id}}" class="cm-trail-link">{{trans('labels.bid.edit_bidder')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-3">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'bids')))
                <a class="btn btn-success btn-sm cm-btn-fixer" href={{URL('projects/'.$project->id.'/bids/'.$supplier->prop_id.'/bidders/'.$supplier->id.'/permissions')}}>Permissions</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'bids', 'projectId' => $project['id']))
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project['id'].'/bids/'.$supplier->prop_id.'/bidders/'.$supplier->id]) !!}
            <div class="row">
                <div class="col-sm-4">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('prop_ab', trans('labels.bid.bidder').' ('.trans('labels.import_from_ab').'):') !!}
                                {!! Form::text('prop_ab', $supplier->address_book->name, ['class' => 'form-control']) !!}
                                <input type="hidden" name="prop_ab_id" id="prop_ab_id" value="{{$supplier->ab_id}}"/>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('prop_addr', trans('labels.location').':') !!}
                                <?php $officeId = 0; ?>
                                <select name="addr_id" id="addr_id">
                                    <option value="">{{trans('labels.select_location')}}</option>
                                    @for($i=0; $i<sizeof($address_book->addresses); $i++)
                                    @if (($supplier->address) && ($supplier->address->id == $address_book->addresses[$i]->id))
                                    <?php $officeId = $supplier->address->id; ?>
                                    <option selected="selected" value="{{$address_book->addresses[$i]->id}}">{{$address_book->addresses[$i]->office_title}}</option>
                                    @else
                                    <option value="{{$address_book->addresses[$i]->id}}">{{$address_book->addresses[$i]->office_title}}</option>
                                    @endif
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                {!! Form::label('prop_ab_cont', trans('labels.proposal.contact').':') !!}
                                <select name="ab_cont_id" id="ab_cont_id">
                                    <option value="">{{trans('labels.select_contact')}}</option>
                                    @for($i=0; $i<sizeof($address_book->contacts); $i++)
                                    @if($officeId === $address_book->contacts[$i]->address_id)
                                    <option @if(($supplier->address_book_contact) && ($supplier->address_book_contact->id == $address_book->contacts[$i]->id)) selected="selected" @endif value="{{$address_book->contacts[$i]->id}}">{{$address_book->contacts[$i]->name}}</option>
                                    @endif
                                    @endfor
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('show_cost', 'bids')) --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        {!! Form::label('proposal_1', trans('labels.proposal.proposal_1').':', ['class' =>
                        'control-label']) !!}
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    {!! Form::text('proposal_1', number_format($supplier->proposal_1, 2), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                            <div class="col-sm-4">

                                <!-- upload select input -->
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                    <label class="input-group-btn">
                                        <span class="btn btn-primary">
                                            <input type="file" style="display: none;" name="proposal_one_file" id="proposal_one_file" multiple>
                                            <span class="glyphicon glyphicon-upload"></span>
                                        </span>
                                    </label>
                                </div>





                                <?php
                                $existingFileId = null;
                                if (count($supplier->files)) {
                                foreach($supplier->files as $proposalFile) {
                                if(!is_null($proposalFile)) {
                                if($proposalFile->item_no == Config::get('constants.proposal_files.first_proposal')) {
                                $existingFileId = $proposalFile->file_id;
                                }
                                }
                                }
                                }
                                if (!is_null($existingFileId)) {
                                ?>
                                <input type="hidden" name="proposal_one_file_id" id="proposal_one_file_id" value="{{$existingFileId}}">
                                <?php
                                } else {
                                ?>
                                <input type="hidden" name="proposal_one_file_id" id="proposal_one_file_id">
                                <?php
                                }
                                ?>
                            </div>
                            @endif
                            <div class="col-sm-4">
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                <button type="button" id="proposal_one_upload" class="btn btn-sm cm-btn-secondary pull-left mb0">
                                <div id="proposal_one_file_wait" class="pull-right ml5"
                                    style="display: none; margin-left: 10px;">
                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                </div>
                                {{trans('labels.upload')}}
                                </button>



                                @endif
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.first_proposal'))
                                <a class="download pull-left mb0" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                @endif
                                @endif
                                @endforeach
                                @endif
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('delete', 'bids'))
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.first_proposal'))
                                <a target="_blank" class="btn btn-sm btn-danger delete-file pull-left mb0 ml5" href="javascript:;" style="margin-left: 15px;"
                                    data-toggle="modal"
                                    data-target="#confirmDelete"
                                    data-title="Delete Record"
                                    data-message="{{trans('labels.global_delete_modal')}}">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    <div class="pull-left delete-wait" style="display: none;">
                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                    <!-- {{trans('labels.delete')}} -->
                                </a>
                                <input type="hidden" class="s3FilePath" data-type="proposals" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">

                                @endif
                                @endif
                                @endforeach
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="row proposal_one_file_main_message_container">
                            <div id="proposal_one_file_message_container" class="col-md-8 col-md-offset-4 proposal_one_file_message_container message_container" style="color: #49A078;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('proposal_2', trans('labels.proposal.proposal_2').':', ['class' =>
                        'control-label']) !!}
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    {!! Form::text('proposal_2', number_format($supplier->proposal_2, 2), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                            <div class="col-sm-4">
                                <!-- upload select input -->
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                    <label class="input-group-btn">
                                        <span class="btn btn-primary">
                                            <input type="file" style="display: none;" name="proposal_two_file" id="proposal_two_file" multiple>
                                            <span class="glyphicon glyphicon-upload"></span>
                                        </span>
                                    </label>
                                </div>



                                <?php
                                $existingFileId = null;
                                if(count($supplier->files)) {
                                foreach($supplier->files as $proposalFile) {
                                if(!is_null($proposalFile)) {
                                if($proposalFile->item_no == Config::get('constants.proposal_files.second_proposal')) {
                                $existingFileId = $proposalFile->file_id;
                                }
                                }
                                }
                                }
                                if(!is_null($existingFileId)) {
                                ?>
                                <input type="hidden" name="proposal_two_file_id" id="proposal_two_file_id" value="{{$existingFileId}}">
                                <?php
                                } else {
                                ?>
                                <input type="hidden" name="proposal_two_file_id" id="proposal_two_file_id">
                                <?php
                                }
                                ?>
                            </div>
                            @endif
                            <div class="col-sm-4">
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                <button type="button" id="proposal_two_upload" class="btn btn-sm cm-btn-secondary pull-left mb0">
                                <div id="proposal_two_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                {{trans('labels.upload')}}
                                </button>

                                @endif
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.second_proposal'))
                                <a class="download pull-left" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                @endif
                                @endif
                                @endforeach
                                @endif
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('delete', 'bids'))
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.second_proposal'))
                                <a target="_blank" class="btn btn-sm btn-danger delete-file pull-left" href="javascript:;" style="margin-left: 15px;"
                                    data-toggle="modal"
                                    data-target="#confirmDelete"
                                    data-title="Delete Record"
                                    data-message="{{trans('labels.global_delete_modal')}}">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    <div class="pull-left delete-wait" style="display: none;">
                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                    <!-- {{trans('labels.delete')}} -->
                                </a>
                                <input type="hidden" data-type="proposals" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">

                                @endif
                                @endif
                                @endforeach
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="row proposal_two_file_main_message_container">
                            <div id="proposal_two_file_message_container" class="col-md-8 col-md-offset-4 proposal_two_file_message_container message_container" style="color: #49A078;"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('proposal_final', trans('labels.proposal.proposal_final').':', ['class' =>
                        'control-label']) !!}
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    {!! Form::text('proposal_final', number_format($supplier->proposal_final, 2), ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                            <div class="col-sm-4">

                            <!-- upload select input -->
                            <div class="input-group">
                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                <label class="input-group-btn">
                                    <span class="btn btn-primary">
                                        <input type="file" style="display: none;" name="proposal_final_file" id="proposal_final_file" multiple>
                                        <span class="glyphicon glyphicon-upload"></span>
                                    </span>
                                </label>
                            </div>

                                <?php
                                $existingFileId = null;
                                if(count($supplier->files)) {
                                foreach($supplier->files as $proposalFile) {
                                if(!is_null($proposalFile)) {
                                if($proposalFile->item_no == Config::get('constants.proposal_files.final_proposal')) {
                                $existingFileId = $proposalFile->file_id;
                                }
                                }
                                }
                                }
                                ?>
                                <?php
                                if(!is_null($existingFileId)) {
                                ?>
                                <input type="hidden" name="proposal_final_file_id" id="proposal_final_file_id" value="{{$existingFileId}}">
                                <?php
                                } else {
                                ?>
                                <input type="hidden" name="proposal_final_file_id" id="proposal_final_file_id">
                                <?php
                                }
                                ?>
                            </div>
                            @endif
                            <div class="col-sm-4">
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                <button type="button" id="proposal_final_upload" class="btn btn-sm cm-btn-secondary pull-left">
                                <div id="proposal_final_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                </div>
                                {{trans('labels.upload')}}
                                </button>

                                @endif
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.final_proposal'))
                                <a class="download pull-left" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                @endif
                                @endif
                                @endforeach
                                @endif
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('delete', 'bids'))
                                @if(count($supplier->files))
                                @foreach($supplier->files as $proposalFile)
                                @if(!is_null($proposalFile))
                                @if($proposalFile->item_no == Config::get('constants.proposal_files.final_proposal'))
                                <a target="_blank" class="btn btn-sm btn-danger delete-file pull-left ml5" href="javascript:;" style="margin-left: 15px;"
                                    data-toggle="modal"
                                    data-target="#confirmDelete"
                                    data-title="Delete Record"
                                    data-message="{{trans('labels.global_delete_modal')}}">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    <div class="pull-left delete-wait" style="display: none;">
                                        <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                    <!-- {{trans('labels.delete')}} -->
                                </a>
                                <input type="hidden" data-type="proposals" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">

                                @endif
                                @endif
                                @endforeach
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="row proposal_final_file_main_message_container">
                            <div id="proposal_final_file_message_container" class="col-md-8 col-md-offset-4 proposal_final_file_message_container message_container" style="color: #49A078;"></div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- @endif --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group pull-right">
                        <input type="hidden" id="type" name="type" value="{{Config::get('constants.bids.plural')}}">
                        {!! Form::submit('Save',['class' => 'btn btn-sm btn-success']) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            @include('errors.list')
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
    @endsection