<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<table width="100%">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="width: 400px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 450px; vertical-align: bottom;">
                        {{Auth::user()->email}}<br>
                        {{Auth::user()->office_phone}}
                    </td>
                    <td style="float: right; vertical-align: bottom;">
                        <h2 style="float: left; margin: 0px; text-align: right;">{{'Bids'}}</h2>

                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="vertical-align: top; width: 465px;">
                        <b>{{'Project: '}}</b>
                        {{$project->name}}
                    </td>
                    <td style="vertical-align: top; width: 390px; text-align: right;">
                        <b>{{'Project Number: '}}</b>
                        {{$project->number}}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="row">
    <h4 style="margin-top: 30px">{{trans('labels.bid.bids_filtered_by')}}</h4>
    <h4>
        @if(!empty($mf_title))
            {{$mf_title}}<br>
        @endif
        @if(!empty($status_title))
            {{$status_title}}
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .bids-padding {
            padding: 5px 5px !important;
            text-align: left !important;
        }

        h4 {
            font-size: 14px;
            padding: 0px 5px 5px 5px;
            margin: 0px;
        }
    </style>
    @if(count($bids) != 0)
        <table style="width: 100%; padding: 0px; margin: 0px;border-collapse:collapse; table-layout: fixed;">
            <thead style="border-bottom: 1px solid #ddd;">
                <tr>
                    <th class="bids-padding">{{trans('labels.master_format_number_and_title')}}</th>
                    <th class="bids-padding">{{trans('labels.proposal.name')}}</th>
                    <th class="bids-padding">{{trans('labels.proposal.quantity')}}</th>
                    <th class="bids-padding">{{trans('labels.proposal.unit_price')}}</th>
                    <th class="bids-padding">{{trans('labels.proposal.value')}}</th>
                    <th class="bids-padding">{{trans('labels.status')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($bids as $item)
                <tr style="background-color: #ddd;">
                    <td class="bids-padding">
                        @if (isset($item->mf_number) && isset($item->mf_title))
                            {{$item->mf_number.' - '.$item->mf_title}}
                        @endif
                    </td>
                    <td class="bids-padding">@if (isset($item->name)) {{$item->name}} @endif</td>
                    <td class="bids-padding">@if (isset($item->quantity)) {{$item->quantity}} @endif</td>
                    <td class="bids-padding">@if (isset($item->unit_price)) {{'$'.number_format($item->unit_price, 2)}} @endif</td>
                    <td class="bids-padding">@if (isset($item->value)) {{'$'.number_format($item->value, 2)}} @endif</td>
                    <td class="bids-padding">@if (isset($item->status)) {{ucfirst($item->status)}} @endif</td>
                </tr>
                @if(sizeof($item->suppliers))
                    <tr>
                        <td colspan="12" class="bids-padding" style="border-bottom: 1px solid #ddd;">
                            <h4>{{trans('labels.bid.bidders')}}</h4>
                            <table style="width: 100%;  table-layout: fixed;">
                                <thead style="border-bottom: 1px solid #ddd;">
                                    <tr>
                                        <th class="bids-padding">{{trans('labels.bid.company')}}</th>
                                        <th class="bids-padding">{{trans('labels.location')}}</th>
                                        <th class="bids-padding">{{trans('labels.contact.name')}}</th>
                                        <th class="bids-padding">{{trans('labels.proposal.proposal_1')}}</th>
                                        <th class="bids-padding">{{trans('labels.proposal.proposal_2')}}</th>
                                        <th class="bids-padding">{{trans('labels.proposal.proposal_final')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($item->suppliers as $supplier)
                                    <tr>
                                        <td class="bids-padding">{{$supplier->ab_name}}</td>
                                        <td class="bids-padding">
                                            @if (isset($supplier->office_title))
                                                {{$supplier->office_title}}
                                            @endif
                                        </td>
                                        <td class="bids-padding">
                                            @if (isset($supplier))
                                                {{$supplier->ab_cont_name}}
                                            @endif
                                        </td>
                                        <td class="bids-padding">@if (isset($supplier->proposal_1)){{'$'.number_format($supplier->proposal_1,2)}} @endif</td>
                                        <td class="bids-padding">@if (isset($supplier->proposal_2)){{'$'.number_format($supplier->proposal_2,2)}} @endif</td>
                                        <td class="bids-padding">@if (isset($supplier->proposal_final)){{'$'.number_format($supplier->proposal_final,2)}} @endif</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                @endif
                <tr class="cm-row-separator">
                    <td colspan="11"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
</body>
</html>