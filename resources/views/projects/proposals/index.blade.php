@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.Project')}} {{trans('labels.bid.plural')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                    class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a>
                </li>
                <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/bids')}}"
                class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
            </ul>
        </header>
    </div>
    <div class="col-md-7">
        <div class="cm-btn-group cm-pull-right cf">
            @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'bids')))
            <a href="{{URL('/projects/'.$project->id.'/bids/create')}}" class="btn btn-success pull-right ">{{trans('labels.bid.create')}}</a>
            @endif
            <div class="pull-right">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/bids/report','target'=>'_blank']) !!}
                {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                {!! Form::hidden('report_status',Input::get('status')) !!}
                {!! Form::hidden('report_sort',Input::get('sort','proposals.id')) !!}
                {!! Form::hidden('report_order',Input::get('order','asc')) !!}
                {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-info mr5 pull-right']) !!}
                {!! Form::close() !!}
                @endif
            </div>
            <div class="pull-right">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('delete', 'bids')))
                {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                <input type="hidden" value="{{URL(Request::path()).'/'}}" id="form-url" />
                <button disabled id="delete-button"  class='btn btn-danger pull-right  mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                {{trans('labels.delete')}}
                </button>
                {!! Form::close()!!}
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('projects.partials.tabs', array('activeTab' => 'bids'))
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                @include('errors.list')
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                @if (sizeof($proposals))
                <div class="cm-filter cm-filter__alt cf">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-10">
                                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/bids/filter']) !!}
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                                            {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                            'name' => trans('labels.proposal.name'),
                                                                                            'company' => trans('labels.bid.company')], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title" >
                                                            {!! Form::label('search', trans('labels.search').':') !!}
                                                            {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                                            {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                                            <input type="hidden" id="project_id" value="{{$project->id}}">
                                                        </div>
                                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                                            {!! Form::label('search-name', trans('labels.search').':') !!}
                                                            {!! Form::text('bid_name',Input::get('bid_name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                        </div>
                                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'company')?'show':'hide'}}" id="company">
                                                            {!! Form::label('search-name', trans('labels.search').':') !!}
                                                            {!! Form::text('bid_company',Input::get('bid_company'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::label('status', trans('labels.status')) !!}
                                                {!! Form::select('status',['All' => 'All', 'open' => trans('labels.proposal.open'), 'closed' => trans('labels.proposal.closed')],Input::get('status')) !!}
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('projects.proposals.partials.table', $proposals)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <?php echo $proposals->appends([
                            'sort' => Input::get('sort'),
                            'order' => Input::get('order'),
                            'master_format_search' => Input::get('master_format_search'),
                            'master_format_id' => Input::get('master_format_id'),
                            'status' => Input::get('status'),
                            'mf_number' => Input::get('mf_number'),
                            'mf_title' => Input::get('mf_title'),
                            'name' => Input::get('name'),
                            ])->render(); ?>
                        </div>
                    </div>
                </div>
                @else
                <div>
                    <p class="text-center">{{trans('labels.no_records')}}</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@include('popups.delete_record_popup')
@endsection