@extends('layouts.master')
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <header class="cm-heading">
        {{trans('labels.bid.create')}}
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/bids')}}" class="cm-trail-link">{{trans('labels.bid.plural')}}</a></li>
          <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/bids/create')}}" class="cm-trail-link">{{trans('labels.bid.create')}}</a></li>
        </ul>
      </header>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'bids'))
    </div>
  </div>
  <div class="panel">
    <div class="panel-body">
      @include('errors.list')
      {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/bids', 'role'=> 'form']) !!}
      @include('projects.proposals.partials.createform')
      {!! Form::close() !!}
    </div>
  </div>
  @endsection