@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <header class="cm-heading">
        {{trans('labels.contract.plural')}}
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
          <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/proposals" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
        </ul>
      </header>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'contracts')))
        <a href="{{URL('/projects/'.$project->id.'/contracts/create')}}" class="btn btn-success pull-right">{{trans('labels.contract.create')}}</a>
        @endif
        <div class="pull-right">
          @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('delete', 'contracts'))
          {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent',  'url'=>URL(''), 'id' => 'delete-form']) !!}
          <input type="hidden" value="{{URL(Request::path()).'/'}}" id="form-url" />
          {{--{!! Form::submit(trans('labels.delete'),['class'=>'btn btn-danger btn-sm', 'onclick' => 'return confirm("'.trans('labels.contract.delete_modal').'")']) !!}--}}
          <button disabled id="delete-button"  class='btn btn-danger pull-right mr5' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
          {{trans('labels.delete')}}
          </button>
          {!! Form::close()!!}
          @endif
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @include('projects.partials.tabs', array('activeTab' => 'contracts'))
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-body">
          @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
          </div>
          @endif
          @if (sizeof($contracts))
          <div class="cm-filter cm-filter__alt cf">
            <div class="row">
              <div class="col-sm-6">
                {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/contracts/filter']) !!}
                <div class="row">
                    <div class="col-sm-5">
                      <div class="form-group">
                        {!! Form::label('search_by', trans('labels.search_by').':') !!}
                        {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                        'name' => trans('labels.contract.name'),
                                                        'company' => trans('labels.Company')], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                      </div>
                    </div>
                    <div class="col-sm-5">
                      <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                        {!! Form::label('search', trans('labels.search').':') !!}
                        {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                        {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                        <input type="hidden" id="project_id" value="{{$project->id}}">
                      </div>
                      <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                        {!! Form::label('search-name', trans('labels.search').':') !!}
                        {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                      </div>
                      <div class="form-group search-field {{(Input::get('drop_search_by') == 'company')?'show':'hide'}}" id="company">
                        {!! Form::label('search-name', trans('labels.search').':') !!}
                        {!! Form::text('company',Input::get('company'),['class' => 'cm-control-required form-control search-field-input address-book-auto']) !!}
                        {!! Form::hidden('company_id',Input::get('company_id'),['class' => 'address-book-id search-field-input']) !!}
                      </div>
                    </div>
                  <div class="col-sm-2">
                    <div class="form-group">
                      {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                    </div>
                  </div>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
          @include('projects.contracts.partials.table', $contracts)
          <div class="row">
            <div class="col-sm-12">
              <div class="pull-right">
                <?php echo $contracts->appends([
                'id'=>Input::get('id'),
                'company_name'=>Input::get('company_name'),
                'value'=>Input::get('value'),
                'mf_number'=>Input::get('mf_number'),
                'mf_title'=>Input::get('mf_title'),
                'name'=>Input::get('name'),
                'sort'=>Input::get('sort'),
                'order'=>Input::get('order'),
                ])->render(); ?>
              </div>
            </div>
          </div>
          @else
          <div>
            <p class="text-center">{{trans('labels.no_records')}}</p>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
  @include('popups.delete_record_popup')
  @endsection