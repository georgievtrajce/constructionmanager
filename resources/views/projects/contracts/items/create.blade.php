@extends('layouts.tabs')

@section('title')
{{trans('labels.contract.add_item')}}<small class="cm-heading-suffix">{{trans('labels.Contract')}}: {{$contract->id}}</small>
<ul class="cm-trail">
    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/proposals" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/proposals/{{$contract->id}}/suppliers/create" class="cm-trail-link">{{trans('labels.contract.add_item')}}</a></li>
</ul>
@endsection

@section('tabs')
<div class="row">
    <div class="col-md-12">
        @include('projects.partials.tabs', array('activeTab' => 'contracts'))
    </div>
</div>
@endsection

@section('tabsContent')

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-5">
                        @include('errors.list')
                        {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/contracts/'.$contract->id.'/items', 'role'=> 'form']) !!}
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.masterformat.search')}}:</label>
                            <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                            <input type="hidden" class="master-format-id" name="master_format_id" value="">
                        </div>

                        <div class="form-group">
                            <label class="control-label">{{trans('labels.masterformat.selected')}}:</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control mf-number-auto" name="mf_number" value="{{Input::old('master_format_number')}}">
                                </div>
                                <div class="col-md-8">
                                    <input type="text" class="form-control mf-title-auto" name="mf_title" value="{{Input::old('master_format_title')}}">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name', trans('labels.contract.item_name').':') !!}
                            {!! Form::text('name', Input::old('name'), ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::submit(trans('labels.save'),['class' => 'btn btn-sm btn-success pull-right']) !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
