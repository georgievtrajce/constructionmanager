@extends('layouts.tabs')

@section('title')
{{trans('labels.contract.edit_item')}}
<ul class="cm-trail">
    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/contracts')}}" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/contracts/'.$item->contr_id.'/items/'.$item->id.'/edit')}}" class="cm-trail-link">{{trans('labels.contract.edit_item')}}</a></li>
</ul>
@endsection

@section('tabs')
@include('projects.partials.tabs', array('activeTab' => 'contracts'))
@endsection

@section('tabsContent')
<div class="panel">
    <div class="panel-body">
        {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project['id'].'/contracts/'.$item->contr_id.'/items/'.$item->id, 'role'=> 'form']) !!}
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="control-label">{{trans('labels.masterformat.search')}}</label>

                    <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                    <input type="hidden" class="master-format-id" name="master_format_id" value="">

                </div>
                <div class="form-group">
                    <label class="control-label">{{trans('labels.masterformat.selected')}}:</label>
                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" class="form-control mf-number-auto" name="mf_number" value="{{$item->mf_number}}">
                        </div>
                        <div class="col-md-8">
                         <input type="text" class="form-control mf-title-auto" name="mf_title" value="{{$item->mf_title}}">
                     </div>
                 </div>
             </div>
             <div class="form-group">
                <label class="control-label">{{trans('labels.contract.item_name')}}</label>

                {!! Form::text('name', $item->name, ['class' => 'form-control']) !!}

            </div>
            <div class="form-group ">
                {!! Form::submit(trans('labels.save'),['class' => 'btn btn-success pull-right']) !!}
            </div>

        </div>
    </div>






    




    {!! Form::close() !!}
    @include('errors.list')
</div>
</div>
@endsection