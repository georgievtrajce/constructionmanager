@if(count($contracts))
<div class="table-responsive">
    <table class="table table-hover table-bordered cm-table-compact">
        <thead>
        <tr>
            <th>{{trans('labels.master_format_number_and_title')}}</th>
            <th>{{trans('labels.contract.name')}}</th>
            <th>{{trans('labels.contract.number')}}</th>
            <th>{{trans('labels.contract.company_name')}}</th>
            <th>{{trans('labels.contract.price')}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
            @for($i=0; $i<sizeof($contracts); $i++)
                <tr>
                    <td>
                        @if (isset($contracts[$i]->mf_number) && isset($contracts[$i]->mf_title))
                            {{$contracts[$i]->mf_number.' - '.$contracts[$i]->mf_title}}
                        @endif
                    </td>
                    <td>
                    <a class="" href="{{URL('/projects/'.$contracts[$i]->proj_id.'/shared/contracts/'.$contracts[$i]->id.'/files')}}">{{$contracts[$i]->name}}</a>

                    </td>
                    <td>@if (isset($contracts[$i]->number)) {{$contracts[$i]->number}} @endif</td>
                    <td>@if (isset($contracts[$i]->company_name)) {{$contracts[$i]->company_name}} @endif</td>
                    <td>{{'$'.number_format($contracts[$i]->value, 2)}} </td>
                    <td>

                </tr>
            @endfor
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-sm-12">
        <div class="pull-right">
            <?php echo $contracts->appends([
                    'id'=>Input::get('id'),
                    'company_name'=>Input::get('company_name'),
                    'value'=>Input::get('value'),
                    'number'=>Input::get('number'),
                    'sort'=>Input::get('sort'),
                    'order'=>Input::get('order'),
            ])->render(); ?>
        </div>
    </div>
</div>
@endif