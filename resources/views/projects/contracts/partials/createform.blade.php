<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('contr_ab', trans('labels.contract.company_name').':') !!} {!! Form::text('contr_ab', Input::old('contr_ab'),
                    ['class' => 'form-control']) !!}
                    <input type="hidden" name="contr_ab_id" id="contr_ab_id" value="{{Input::old('contr_ab_id')}}" />
                </div>
                <div class="form-group">
                    {!! Form::label('number', trans('labels.contract.number').':') !!} {!! Form::text('number', Input::old('number'), ['class'
                    => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('contr_price', trans('labels.contract.price').':') !!}
                    <div class="input-group">
                        <span class="input-group-addon">$</span> {!! Form::text('contr_price', Input::old('contr_price'),
                        ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('name', trans('labels.contract.name').':') !!} {!! Form::text('name', Input::old('name'), ['class' => 'form-control'])
                    !!}
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.masterformat.search')}}:</label>
                            <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                            <input type="hidden" class="master-format-id" name="master_format_id" value="">
                            <input type="hidden" id="project_id" value="{{$project->id}}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group cf">
                            <div class="mf-cont">
                                <label class="control-label">&nbsp;</label>
                                <a href="javascript:;" id="custom-mf" class="btn btn-primary mb0 pull-right">
                                    {{trans('labels.submittals.enter_custom_number_and_title')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.spec_number')}}:</label>
                            <input readonly type="text" class="form-control mf-number-auto" name="mf_number" value="{{Input::old('master_format_number')}}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.spec_title')}}:</label>
                            <input readonly type="text" class="form-control mf-title-auto" name="mf_title" value="{{Input::old('master_format_title')}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group cf">
                    {!! Form::submit(trans('labels.save'), ['class' => 'btn btn-success pull-right']) !!}
                </div>
            </div>
        </div>
    </div>
</div>