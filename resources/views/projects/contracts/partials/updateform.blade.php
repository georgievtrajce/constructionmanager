 {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project->id.'/contracts/'.$contract->id, 'role'=> 'form']) !!}
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('contr_ab', trans('labels.contract.company_name').':', ['class' => 'control-label']) !!} {!! Form::text('contr_ab',
                    $contract->company_name, ['class' => 'form-control']) !!}
                    <input type="hidden" name="contr_ab_id" id="contr_ab_id" value="{{$contract->ab_id}}" />
                </div>
                <div class="form-group">
                    {!! Form::label('number', trans('labels.contract.number').':', ['class' => 'control-label']) !!} {!! Form::text('number',
                    $contract->number, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('value', trans('labels.contract.price').':', ['class' => 'control-label']) !!}
                    <div class="input-group">
                        <span class="input-group-addon">$</span> {!! Form::text('value', number_format($contract->value,
                        2), ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    {!! Form::label('name', trans('labels.contract.name').':') !!} {!! Form::text('name', $contract->name, ['class' => 'form-control'])
                    !!}
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.masterformat.search')}}:</label>
                            <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="" autocomplete="off">
                            <input type="hidden" class="master-format-id" name="master_format_id" value="">
                            <input type="hidden" id="project_id" value="{{$project->id}}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group cf">
                            <div class="mf-cont">
                                <label class="control-label">&nbsp;</label>
                                <a href="javascript:;" id="custom-mf" class="btn btn-primary mb0 pull-right">
                                    {{trans('labels.submittals.enter_custom_number_and_title')}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.spec_number')}}:</label>
                            <input readonly type="text" class="form-control mf-number-auto" name="mf_number" value="{{$contract->mf_number}}">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.spec_title')}}:</label>
                            <input readonly type="text" class="form-control mf-title-auto" name="mf_title" value="{{$contract->mf_title}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group ">
                    {!! Form::submit(trans('labels.update'),['class' => 'btn btn-success pull-right']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<div class="row">
    <div class="col-md-12">
        @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
        || Permissions::can('write', 'contracts'))
        <a class="btn btn-info btn-sm pull-left mr5" href="{{URL('/projects/'.$contract->proj_id.'/contracts/'.$contract->id.'/files')}}">{{trans('labels.contract.add_file')}}</a>        @endif @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin
        == Auth::user()->id) || Permissions::can('delete', 'contracts')) {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent',
        'url'=>URL(''), 'id' => 'delete-form']) !!}
        <input type="hidden" value="{{URL('projects/'.$contract->proj_id.'/contracts/'.$contract->id.'/files').'/'}}" id="form-url"
        />
        <button disabled id="delete-button" class='btn btn-xs btn-danger pull-right' type='submit' data-toggle="modal" data-target="#confirmDelete"
            data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                        {{trans('labels.delete')}}
                    </button> {!! Form::close()!!} @endif
    </div>
    <div class="col-md-12">
        @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id)
        || Permissions::can('read', 'contracts')) @if (count($contract->contractFiles))
        <div class="table-responsive">
            <table class="table table-hover table-bordered cm-table-compact">
                <thead>
                    <tr>
                        <th></th>
                        <th>{{trans('labels.file.name')}}</th>
                        <th>{{trans('labels.file.number')}}</th>
                        <th>{{trans('labels.files.upload_date')}}</th>
                        <th>{{trans('labels.pcos.cost')}}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contract->contractFiles as $file)
                    <tr>
                        <td>
                            <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$file->id}}">
                        </td>
                        <td>
                            @if (!is_null($file->files))
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'contracts'))
                                    @if (isset($file->file_id))
                                        <a href="files/{{$file->file_id}}/edit">{{$file->files->name}}</a>
                                    @else
                                        {{$file->files->name}}
                                    @endif
                                @else
                                    {{$file->files->name}}
                                @endif
                            @endif
                        </td>
                        <td>@if (isset($file->files->number)){{$file->files->number}} @endif</td>
                        <td>{{date("m/d/Y", strtotime($file->files->created_at))}}</td>
                        <td>@if (!empty($file->file_cost)) {{'$'.number_format($file->file_cost, 2)}} @endif</td>
                        <td>
                            @if (!is_null($file->files))
                            <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                            <input type="hidden" class="s3FilePath" id="{{$file->files->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/contracts/contract_'.Request::segment(4).'/'.$file->files->file_name}}">                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @else
        <div class="row">
            <div class="col-md-12">
                <hr class="mt0">
                <p class="text-center">No Contacts</p>
            </div>
        </div>
        @endif @endif
    </div>
    @include('popups.delete_record_popup')