<div class="table-responsive">
    <table class="table table-hover table-bordered cm-table-compact">
        <thead>
            <tr>
                <th class="text-center">
                    &nbsp;
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=mf_number&order=desc';
                    } else {
                        $url = Request::url().'?sort=mf_number&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                    $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                    ?>
                    {{trans('labels.master_format_number_and_title')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=name&order=desc';
                    } else {
                        $url = Request::url().'?sort=name&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                    $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                    ?>
                    {{trans('labels.contract.name')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'number' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=number&order=desc';
                    } else {
                        $url = Request::url().'?sort=number&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                    $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                    ?>
                    {{trans('labels.contract.number')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                        if (Input::get('sort') == 'company_name' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=company_name&order=desc';
                        } else {
                            $url = Request::url().'?sort=company_name&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                    ?>
                    {{trans('labels.contract.company_name')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>
                <th>
                    <?php
                    if (Input::get('sort') == 'value' && Input::get('order') == 'asc') {
                        $url = Request::url().'?sort=value&order=desc';
                    } else {
                        $url = Request::url().'?sort=value&order=asc';
                    }
                    $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                    $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                    $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                    $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                    $url .= !empty(Input::get('company_id'))?'&company_id='.Input::get('company_id'):'';
                    ?>
                    {{trans('labels.contract.price')}}
                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                </th>

            </tr>
        </thead>
        <tbody>
        @if(count($contracts))
            @for($i=0; $i<sizeof($contracts); $i++)
                <tr>
                <td class="text-center">
                    <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$contracts[$i]->id}}">
                </td>
                    <td>
                        @if (isset($contracts[$i]->mf_number) && isset($contracts[$i]->mf_title))
                            {{$contracts[$i]->mf_number.' - '.$contracts[$i]->mf_title}}
                        @endif
                    </td>
                    <td>

                        @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('read', 'contracts'))
                            <a class="" href="{{URL('/projects/'.$contracts[$i]->proj_id.'/contracts/'.$contracts[$i]->id.'/edit')}}"> {{$contracts[$i]->name}}</a>
                        @endif

                    </td>
                    <td>@if (isset($contracts[$i]->number)) {{$contracts[$i]->number}} @endif</td>
                    <td>@if (isset($contracts[$i]->company_name)) {{$contracts[$i]->company_name}} @endif</td>
                    <td>@if (isset($contracts[$i]->value)) {{'$'.number_format($contracts[$i]->value, 2)}} @endif</td>

                </tr>
            @endfor
        @else
            <p class="text-center">{{trans('labels.no_records')}}</p>
        @endif
        </tbody>
    </table>
</div>