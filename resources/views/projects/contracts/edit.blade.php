@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <header class="cm-heading">
                {{trans('labels.contract.edit')}} <small class="cm-heading-suffix">{{trans('labels.Contract')}}: {{$contract->id}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/contracts/{{$contract->id}}/edit" class="cm-trail-link">{{trans('labels.contract.edit')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-5">
            <div class="cm-btn-group cm-pull-right cf">
                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('write', 'tasks')))
                    <a class="btn btn-success cm-btn-fixer" href="{{URL('tasks/create').'?module=1&project='.$project->id.'&module_type='.'5'.'&module_item='.$contract->id.'&title='.$contract->name.'&mf_number='.$contract->mf_number.'&mf_title='.$contract->mf_title.'&mf_number_title='.$contract->mf_number.' - '.$contract->mf_title}}">{{trans('labels.tasks.create')}}</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'contracts'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @include('projects.contracts.partials.updateform')
                    @include('errors.list')
                </div>
            </div>
        </div>
    </div>
    @endsection