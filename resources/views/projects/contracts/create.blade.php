@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
              {{trans('labels.contract.create')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/contracts/create')}}" class="cm-trail-link">{{trans('labels.contract.create')}}</a></li>
                </ul>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
           @include('projects.partials.tabs', array('activeTab' => 'contracts'))
        </div>
    </div>
<div class="panel">
    <div class="panel-body">
        @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
        @endif
        @include('errors.list')
        {!! Form::open(['method'=> 'POST', 'url'=>'projects/'.$project->id.'/contracts', 'role'=> 'form']) !!}
        @include('projects.contracts.partials.createform')
        {!! Form::close() !!}
    </div>
</div>
@endsection
