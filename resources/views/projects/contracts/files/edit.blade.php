@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.contract.files')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts/{{Request::segment(4)}}/edit/" class="cm-trail-link">{{trans('labels.contract.edit')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/contracts/{{Request::segment(4)}}/files/{{$file->file_id}}/edit" class="cm-trail-link">{{trans('labels.contract.edit_file')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'contracts', 'projectId' => $project->id))
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            {!! Form::open(['method' => 'PUT', 'url' => 'projects/'.$project->id.'/contracts/'.$file->contr_id.'/files/'.$file->file_id, 'files' => true, 'class'=> 'form-horizontal', 'role'=> 'form'])!!}
            <div class="row">
                <div class="col-sm-6">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('name', trans('labels.file.name').':') !!}
                            {!! Form::text('name', $file->name, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('number', trans('labels.file.number').':') !!}
                            {!! Form::text('number', $file->number, ['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>{{trans('labels.pcos.cost')}}</label>
                            <div class="input-group col-md-6">
                                <div class="input-group-addon">$</div>
                                <input type="text" class="form-control" name="file_cost" value="{{ number_format($file->file_cost, 2) }}">
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-8">
                                <label>{{trans('labels.File')}}:</label>




                                        <!-- upload select input -->
                                        <div class="input-group">
                                            <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">
                                                    <input type="file" style="display: none;" name="contract_file" id="file" multiple>

                                                    <input type="hidden" name="file_id" id="file_id" value="{{$file->f_id}}">
                                                    <span class="glyphicon glyphicon-upload"></span>
                                                </span>
                                            </label>
                                        </div>



                            </div>

                            <div class="col-md-4">
                                <button type="button" id="file-upload" class="btn btn-info pull-right cm-btn-fixer">
                                <div id="file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                    <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                </div>
                                {{trans('labels.upload')}}
                                </button>
                            </div>
                        </div>
                        <div class="row file_main_message_container">
                            <div id="file_message_container"
                                class="col-md-12 file_message_container message_container"
                            style="color: #49A078;"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <label>{{trans('labels.file.current')}}:</label><br />
                            <a class="download" href="javascript:;">{{$file->file_name}}</a>
                            <input type="hidden" class="s3FilePath" id="{{$file->f_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/contracts/contract_'.Request::segment(4).'/'.$file->file_name}}">
                        </div>
                    </div>
                    <div class="cm-spacer-xs"></div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="hidden" id="type" name="dtype" value="{{Config::get('constants.contracts')}}">
                            <input type="hidden" id="file_type" name="type" value="contract">
                            <input type="hidden" id="project_file_type" name="project_file_type" value="contracts">
                            <input type="hidden" id="contract_id" name="contract_id" value="{{Request::segment(4)}}">
                            {!! Form::submit(trans('labels.update'),['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
        @include('errors.list')
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @endsection