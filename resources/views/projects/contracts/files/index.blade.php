@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <header class="cm-heading">
                {{trans('labels.contract.files')}}
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                    class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts"
                    class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
                    <li class="cm-trail-item"><a href="/projects/{{$project->id}}/contracts/{{Request::segment(4)}}/edit/"
                    class="cm-trail-link">{{trans('labels.contract.edit')}}</a></li>
                    <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/contracts/{{Request::segment(4)}}/files/"
                    class="cm-trail-link">{{trans('labels.contract.add_file')}}</a></li>
                </ul>
            </header>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('projects.partials.tabs', array('activeTab' => 'contracts'))
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel">
                <div class="panel-body">
                    @include('errors.list')
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    {!! Form::open(['method'=> 'POST', 'url'=>Request::path(), 'files' => true, 'role'=> 'form']) !!}
                    <!-- <h4>{{trans('labels.file.add')}}</h4> -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="name">{{trans('labels.file.name')}}:</label>
                                <input class="control-form" id="name" name="name" type="text" value="{{Input::old('name')}}">
                            </div>
                            <div class="form-group">
                                <label for="number">{{trans('labels.file.number')}}:</label>
                                <input id="number" name="number" type="text" value="{{Input::old('number')}}">
                            </div>
                            <div class="form-group">
                                <label>{{trans('labels.pcos.cost')}}</label>
                                <div class="input-group col-md-6">
                                    <div class="input-group-addon">$</div>
                                    <input type="text" class="form-control" name="file_cost" value="{{ old('file_cost') }}">
                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label for="contract_file">{{trans('labels.File')}}:</label>
                                <!--         <input type="file" name="contract_file" id="file">
                                        <input type="hidden" name="file_id" id="file_id" value="{{old('file_id') }}"> -->


                                        <!-- upload select input -->
                                        <div class="input-group">
                                            <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                            <label class="input-group-btn">
                                                <span class="btn btn-primary">
                                                    <input type="file" style="display: none;" name="contract_file" id="file" multiple>

                                                    <input type="hidden" name="file_id" id="file_id" value="{{old('file_id') }}">
                                                    <span class="glyphicon glyphicon-upload"></span>
                                                </span>
                                            </label>
                                        </div>



                                    </div>

                                    <div class="col-md-4">
                                        <button type="button" id="file-upload"
                                        class="btn btn-info pull-right cm-btn-fixer">
                                        <div id="file_wait"  class="pull-right ml5" style="display: none; margin-left: 10px;">
                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt=""
                                            width="17px">
                                        </div>
                                        {{trans('labels.upload')}}
                                        </button>
                                    </div>
                                </div>
                                <div class="row file_main_message_container">
                                    <div id="file_message_container"
                                        class="col-md-12 file_message_container message_container"
                                        style="  font-size: 12px;
                                        color: #D64933;
                                        display: block;
                                    overflow: auto;"></div>
                                </div>
                            </div>
                            <div class="cm-spacer-xs"></div>
                            <div class="form-group">
                                <input type="hidden" id="type" name="dtype" value="{{Config::get('constants.contracts')}}">
                                <input type="hidden" id="file_type" name="type" value="contract">
                                <input type="hidden" id="project_file_type" name="project_file_type" value="contracts">
                                <input class="btn btn-success" type="submit" value="Save">
                            </div>
                        </div>
                        <input type="hidden" id="contract_id" name="contract_id" value="{{Request::segment(4)}}">
                        <input type="hidden" id="contr_id" name="contr_id" value="{{Input::old('contr_id')}}">
                        <input type="hidden" id="proj_id" name="proj_id" value="{{$project->id}}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @endsection