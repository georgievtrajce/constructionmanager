<option value="0" selected>{{trans('labels.tasks.select_item')}}</option>
@foreach($items as $item)
  <option value="{{$item->id}}">{{$item->name}}</option>
@endforeach