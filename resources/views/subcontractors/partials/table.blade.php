<table class="table table-hover table-bordered cm-table-compact">
    <thead>
    <tr>
        <th>Project Name</th>
        <th>Project Number</th>
        <th>Project Code</th>
        <th>Location</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Project Value</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($projects as $project)
        <tr>
            <td>@if (isset($project->name))
                    <a href="{{URL('projects/'.$project->id)}}" >{{$project->name}}</a>
                @endif
            </td>
            <td>@if (isset($project->number)) {{$project->number}} @endif</td>
            <td>@if (isset($project->upc_code)) {{$project->upc_code}} @endif</td>
            <td>
                @if (isset($project->address))
                    @if (!empty($project->address->city))
                        {{$project->address->city}}
                    @endif
                    @if (!empty($project->address->city) && !empty($project->address->state))
                        ,
                    @endif
                    @if (!empty($project->address->state))
                        {{$project->address->state}}
                    @endif
                @endif
            </td>
            <td>@if (isset($project->start_date)  && $project->start_date != 0){{date("m/d/Y", strtotime($project->start_date))}} @endif</td>
            <td>@if (isset($project->end_date)  && $project->end_date != 0){{date("m/d/Y", strtotime($project->end_date))}} @endif</td>
            <td>@if (isset($project->price)){{'$'.number_format($project->price,2)}} @endif</td>
        </tr>
    @endforeach
    </tbody>
</table>