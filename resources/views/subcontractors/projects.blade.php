@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-sm-5">
            <h1 class="cm-heading">
            <a href="./">{{trans('labels.project.shared_with', ['name'=> $addressBook->name])}}</a>
            <small class="cm-heading-sub">{{trans('labels.total_records', ['number'=> $projects->count()])}}</small>
            </h1>
        </div>
        <div class="col-sm-7">
            <a href="{{URL('projects/create')}}" class="btn btn-success cm-pull-right">{{trans('labels.project.create')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
        <div class="cm-filter">
                        <div class="row">
                            <div class="col-sm-7">
                                {!! Form::open(['method'=>'GET','url'=>'address-book/'.$addressBook->id.'/projects/search/'.$sortUrl]) !!}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('search_by', trans('labels.search_by')) !!}
                                            {!! Form::select('search_by',['name'=>trans('labels.name'),'number'=>trans('labels.number'),'upc_code'=>'UPC','location'=>trans('labels.location')],Input::get('search_by')) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            {!! Form::label('search', trans('labels.search')) !!}
                                            {!! Form::text('search',Input::get('search'),['class' => 'form-control']); !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('go', ' &nbsp;') !!}
                                            {!! Form::submit(trans('labels.go'),['class' => 'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                           <!--  <div class="col-sm-5">
                                {!! Form::open(['method'=>'GET','url'=>'address-book/'.$addressBook->id.'/projects/'.$sortUrl]) !!}
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('sort', trans('labels.sort_by')) !!}
                                            {!! Form::select('sort',['name'=>trans('labels.name'),'number'=>trans('labels.number'),'upc_code'=>'UPC','price'=>trans('labels.value'),'start_date'=>trans('labels.project.start_date'),'end_date' => trans('labels.project.end_date')],Input::get('sort')) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('order', trans('labels.order_by')) !!}
                                            {!! Form::select('order', array('asc'=>trans('labels.ascending'),'desc'=>trans('labels.descending')),Input::get('order')) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            {!! Form::label('filter', ' &nbsp;') !!}
                                            {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                    <input type="hidden" name="activeTab" id="activeTab" value=""/>
                                </div>
                                {!! Form::close() !!}
                            </div> -->
                        </div>
                    </div>

            <div class="panel">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif

                    <div class="bs-example bs-example-tabs" role="tabpanel" data-example-id="togglable-tabs">
                        <ul class="nav nav-tabs cm-tab-inner" role="tablist">
                            <li @if (Request::segment(4)!= 'completed' && Request::segment(5)!= 'completed') class="active" @endif role="presentation"><a href="{{URL('address-book/'.$addressBook->id.'/projects')}}">{{ trans('labels.project.created') }}</a></li>
                            <li @if (Request::segment(4)== 'completed' || Request::segment(5)== 'completed') class="active" @endif role="presentation"><a href="{{URL('address-book/'.$addressBook->id.'/projects/completed')}}">{{ trans('labels.project.completed') }}</a></li>
                        </ul>
                        <div class="panel panel-default panel-body cm-panel-tabs-body">
                            <div id="myTabContent" class="tab-content">
                                <div role="tabpanel" class="tab-pane fade active in" id="created">
                                    <div class="table-responsive">  
                                        @if(count($projects))
                                        @include('subcontractors.partials.table', ['projects' => $projects])
                                        @else
                                        <span class="col-sm-12 text-center">{{trans('labels.no_projects')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <?php echo $projects->appends([
                                'id'=>Input::get('id'),
                                'name'=>Input::get('company_name'),
                                'number'=>Input::get('number'),
                                'upc_code'=>Input::get('upc_code'),
                                'price'=>Input::get('price'),
                                'start_date'=>Input::get('start_date'),
                                'end_date'=>Input::get('end_date'),
                                'sort'=>Input::get('sort'),
                                'order'=>Input::get('order'),
                                ])->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection