@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <h1 class="cm-heading">
            {{ $addressBookContact->name }}
            </h1>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="cm-pull-right">
                <div class="form-group">
                    <a href="{{URL('address-book/'.$addressBookContact->ab_id.'/contact/'.$addressBookContact->id.'/view/contact-report')}}" target="_blank" class="btn btn-sm pull-right btn-info">{{ trans('labels.save_pdf') }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">
                            Title:
                        </label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookContact->title }}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">
                            {{ trans('labels.address_book.email_address') }}:
                        </label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookContact->email }}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">
                            {{ trans('labels.address_book.office_phone') }}:
                        </label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookContact->office_phone }}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">
                            {{ trans('labels.address_book.cell_phone') }}:
                        </label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookContact->cell_phone }}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">
                            {{ trans('labels.address_book.fax') }}:
                        </label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookContact->fax }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection