@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{ $basicInfo->name }}
            </h1>
        </div>
    </div>
    <div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
            <option data-href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</option>
            <option selected data-href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</option>
        </select>
    </div>

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
        <li  role="presentation"><a href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</a></li>
        <li  role="presentation"><a href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</a></li>
        <li  role="presentation"><a href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</a></li>
        <li class="active" role="presentation"><a href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</a></li>
        <li role="presentation"><a href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</a></li>
    </ul>
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="contacts-main-cont">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group cf">
                            <label>{{trans('labels.project_rating')}}</label>
                            <select id="cc_project" name="cc_project">
                                <option value="">{{trans('labels.address_book.select_project')}}</option>
                                @if(count($projects) > 0)
                                @foreach($projects as $project)
                                <option {{($projectId == $project->id)?'selected':''}} value="{{ $project->id }}">
                                    {{ $project->name }}
                                </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <hr />
                        @if (!empty($projectId))
                        <form role="form" action="{{URL('/address-book/'.$companyId.'/ratings'.(!empty($projectId)?'/'.$projectId:''))}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group cf">
                                        <span>{{trans('labels.address_book.ratings-info')}}</span>
                                        <br /><br />
                                        <h4>{{trans('labels.rating_category')}}</h4>
                                        <div class="cm-spacer-xs"></div>
                                    </div>
                                </div>

                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.price')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_price" value="{{ (!empty($companyRating))?$companyRating->price:old('cc_price') }}">
                                    </div>
                                </div>

                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.quality_of_work')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_quality_of_work" value="{{ (!empty($companyRating))?$companyRating->quality_of_work:old('cc_quality_of_work') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.quality_of_materials')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_quality_of_materials" value="{{ (!empty($companyRating))?$companyRating->quality_of_materials:old('cc_quality_of_materials') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.expertise')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_expertise" value="{{ (!empty($companyRating))?$companyRating->expertise:old('cc_expertise') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.problems_resolution')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_problems_resolution" value="{{ (!empty($companyRating))?$companyRating->problems_resolution:old('cc_problems_resolution') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.safety')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_safety" value="{{ (!empty($companyRating))?$companyRating->safety:old('cc_safety') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.cleanness')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_cleanness" value="{{ (!empty($companyRating))?$companyRating->cleanness:old('cc_cleanness') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.organized_professional')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_organized_professional" value="{{ (!empty($companyRating))?$companyRating->organized_and_professional:old('cc_organized_professional') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.communication_skills')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_communication_skills" value="{{ (!empty($companyRating))?$companyRating->communication_skills:old('cc_communication_skills') }}">
                                    </div>
                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.address_book.completed_on_time')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="number" min="0" max="100" class="form-control rating-field" name="cc_completed_on_time" value="{{ (!empty($companyRating))?$companyRating->completed_on_time:old('cc_completed_on_time') }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-11">
                                        <hr />
                                        <div class="cm-spacer-xs"></div>
                                    </div>
                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-8">
                                        <label class="control-label">{{trans('labels.project_rating')}}</label>
                                    </div>
                                    <div class="col-sm-4">
                                        <input readonly type="number" min="0" max="100"  class="form-control" name="cc_project_rating"
                                               id="cc_project_rating" value="{{ (!empty($companyRating))?number_format((float)$companyRating->project_rating, 2, '.', ''):'' }}">
                                    </div>

                                </div>
                                <div class="form-group cf">
                                    <div class="col-sm-12">
                                        <div class="form-group cf">
                                            <button type="submit" class="btn btn-success pull-left">
                                            {{trans('labels.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </form>
                             @endif
                        </div>
                        <div class="col-sm-1">
                        </div>
                        <div class="col-sm-5">
                            @if (!empty($projects) && count($projects)>0)
                            <div class="form-group cf">
                                <div class="col-sm-11">
                                    <h4>{{trans('labels.total_rating')}}</h4>
                                    <div class="cm-spacer-xs"></div>
                                </div>
                            </div>
                            <?php $totalSum = 0; $rated = false; $numProjects = 0; ?>
                            @foreach($projects as $project)
                                <?php
                                    if (!empty($project->project_rating)) {
                                        $numProjects++;
                                    }

                                ?>
                            <div class="form-group cf">
                                <div class="col-sm-8">
                                    <label class="control-label">{{$project->name}}</label>
                                </div>
                                <div class="col-sm-4">
                                    <span>
                                        {{ (!empty($project->project_rating))?number_format((float)$project->project_rating, 2, '.', ''):'not rated' }}
                                    </span>
                                </div>
                                <div class="col-sm-4">
                                    &nbsp;
                                </div>
                            </div>
                            <?php
                            if (!empty($project->project_rating)) {
                                $rated = true;
                                $totalSum += $project->project_rating;
                            }
                            ?>
                            @endforeach
                            <div class="form-group cf">
                                <div class="col-sm-11">
                                    <hr />
                                    <div class="cm-spacer-xs"></div>
                                </div>
                            </div>
                            <div class="form-group cf">
                                <div class="col-sm-8">

                                    <label class="control-label">{{trans('labels.total_rating')}}</label>
                                </div>
                                <div class="col-sm-4">
                                    <span>
                                        {{ ($rated)?number_format((float)round($totalSum/$numProjects,2), 2, '.', ''):'not rated' }}
                                    </span>
                                </div>
                                <div class="col-sm-4">
                                    &nbsp;
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="cm-spacer-xs"></div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @include('popups.delete_record_popup')