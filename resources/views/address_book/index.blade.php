@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
  <div class="row">
    <div class="col-md-5">
      <h1 class="cm-heading">
      {{trans('labels.address_book_title')}}
      <small class="cm-heading-sub">
      {{trans('labels.total_records', ['number' => $addressBookEntries->total()])}}
      </small>
      </h1>
    </div>
    <div class="col-md-7">
      <div class="cm-btn-group cm-pull-right cf">
        @if ($showUndo)
          <a href="#" class="btn btn-success cm-heading-btn btn-undo">{{trans('labels.address_book.undo-button')}}</a>
        @endif
        @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('write', 'address-book')))
          <a href="{{URL('address-book/create')}}" class="btn btn-success cm-heading-btn">{{trans('labels.address_book.create')}}</a>
        @endif
        <a target="_blank" href="{{Illuminate\Support\Facades\Request::url().'/report'.'?'.$_SERVER['QUERY_STRING']}}" class="btn btn-success">{{trans('labels.address_book.save_to_pdf')}}</a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <div class="cm-filter cf">
        <div class="row">
          <div class="col-md-5 col-lg-3">
            <div class="dropdown cm-btn-fixer">
              <button class="btn btn-default dropdown-toggle cm-heading-btn" data-toggle="dropdown" aria-expanded="false">
              {{trans('labels.address_book.categories')}}
              <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li>
                  <a href="{{URL('address-book')}}">{{trans('labels.address_book.view_all')}}</a>
                </li>
                <li>
                  <h5>{{trans('labels.address_book.default_categories')}}</h5>
                </li>
                @foreach ($defaultCategories as $defaultCategory)
                <li>
                  <a href="{{ URL('address-book').'/default-category/'.$defaultCategory['id'] }}">{{ $defaultCategory['name'] }}</a>
                </li>
                @endforeach
                <li>
                  <h5>{{trans('labels.address_book.custom_categories')}}</h5>
                </li>
                @foreach ($customCategories as $customCategory)
                <li>
                  <a href="{{ URL('address-book').'/custom-category/'.$customCategory['id'] }}">{{ $customCategory['name'] }}</a>
                </li>
                @endforeach
              </ul>
              @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('read', 'address-book')))
              <a href="{{URL('custom-categories')}}" class="btn btn-primary">{{trans('labels.address_book.custom_categories')}}</a>
              @endif
            </div>
          </div>
          <div class="col-md-7 col-lg-9">
            <div class="row">
              {!! Form::open(['method'=>'GET','url'=>'address-book/master-format']) !!}

              <div class="col-md-6 col-lg-6">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      {!! Form::label('search_by', trans('labels.search_by').':') !!}
                      {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                          'comp_name' => trans('labels.company.name'),
                                                          ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                      {!! Form::label('search', trans('labels.search').':') !!}
                      {!! Form::text('search', Input::get('search'), ['class' => 'form-control mf-number-title-auto search-field-input']) !!}
                      {!! Form::text('number', Input::get('number'), ['class' => 'form-control mf-number-auto hide search-field-input', 'readonly']) !!}
                      {!! Form::text('name', Input::get('name'), ['class' => 'form-control mf-title-auto hide search-field-input', 'readonly']) !!}
                    </div>
                    <div class="form-group search-field {{(Input::get('drop_search_by') == 'comp_name')?'show':'hide'}}" id="comp_name">
                      {!! Form::label('search-name', trans('labels.search').':') !!}
                      {!! Form::text('comp_name',Input::get('comp_name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-6 col-md-3 col-lg-1">
                <div class="form-group">
                  {!! Form::submit(trans('labels.address_book.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                </div>
              </div>
              {!! Form::close() !!}
              <div class="col-xs-6 col-sm-6 col-md-3 col-lg-5">
                <div class="form-group">
                  @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('delete', 'address-book')))
                      {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                        <input type="hidden" value="{{URL('address-book').'/'}}" id="form-url" />
                        <button disabled id="delete-button" class='btn btn-xs btn-danger pull-right cm-btn-fixer' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                          {{trans('labels.delete')}}
                        </button>
                      {!! Form::close()!!}
                  @endif
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="panel">
        <div class="panel-body">
          <div class="row">
            <div class="col-md-12 cm-spacer-xs">
              <h4>Category: {{ $category }}</h4>
            </div>
          </div>
          <div class="row">
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="col-md-12">
              @if(!$addressBookEntries->isEmpty())
              <div class="table-responsive">
                <table class="table table-hover table-bordered cm-table-compact">
                <thead>
                  <tr>
                    <th class="text-center">
                      <input type="checkbox" name="select_all" id="addressbook_select_all">
                    </th>
                    <th>
                      <?php
                        if (Input::get('sort') == 'name-desc') {
                          $url = Request::url().'?sort=name-asc';
                        } else {
                          $url = Request::url().'?sort=name-desc';
                        }
                        $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                        $url .= !empty(Input::get('number'))?'&number='.Input::get('number'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                      ?>
                    {{trans('labels.address_book.company_name')}}
                    <span class="row-title"></span> <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                      <?php
                        if (Input::get('sort') == 'categories.name-asc') {
                          $url = Request::url().'?sort=categories.name-desc';
                        } else {
                          $url = Request::url().'?sort=categories.name-asc';
                        }
                        $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                        $url .= !empty(Input::get('number'))?'&number='.Input::get('number'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                      ?>
                    {{trans('labels.address_book.categories')}}
                     <span class="row-title"></span> <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                      <?php
                      if (Input::get('sort') == 'mf.number-asc') {
                        $url = Request::url().'?sort=mf.number-desc';
                      } else {
                        $url = Request::url().'?sort=mf.number-asc';
                      }
                      $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                      $url .= !empty(Input::get('number'))?'&number='.Input::get('number'):'';
                      $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                      ?>
                    {{trans('labels.master_format_number_and_title')}}
                     <span class="row-title"></span> <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th class="no-wrap">
                      <?php
                        if (Input::get('sort') == 'total_rating-asc') {
                        $url = Request::url().'?sort=total_rating-desc';
                        } else {
                        $url = Request::url().'?sort=total_rating-asc';
                        }
                        $url .= !empty(Input::get('search'))?'&search='.Input::get('search'):'';
                        $url .= !empty(Input::get('number'))?'&number='.Input::get('number'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                      ?>
                      <span class="row-title">{{trans('labels.rating')}}</span> <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($addressBookEntries as $item)
                  <tr>
                    <td class="text-center">
                      <input type="checkbox" name="select_all" class="multiple-items-checkbox address-book-chk" data-id="{{$item->id}}">
                    </td>
                    <td>
                      @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('read', 'address-book')))
                        <a href="{{URL('address-book/'.$item->id.'/view')}}">
                      @endif
                        {{$item->name}}
                      @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('read', 'address-book')))
                        </a>
                      @endif
                    </td>
                    <td>
                      <?php
                      if(count($item->default_categories)) {
                        $i = 1;
                        foreach($item->default_categories as $default_category) {
                          $separator = ((count($item->custom_categories)) ? ', ' : ((count($item->default_categories) == $i) ? '' : ', '));
                          echo $default_category->name.$separator;
                          $i++;
                        }
                      }
                      if(count($item->custom_categories)) {
                        $i = 1;
                        foreach($item->custom_categories as $custom_category) {
                          $separator = (count($item->custom_categories) == $i) ? '' : ', ';
                          echo $custom_category->name.$separator;
                          $i++;
                        }
                      }
                      ?>
                    </td>
                    <td>
                      <?php
                      if (count($item->master_format_items)) {
                      $i = 1;
                      foreach ($item->master_format_items as $masterFormatItems) {
                      $separator = (count($item->master_format_items) == $i) ? '' : ', ';
                      ?>
                        {{ $masterFormatItems->number.' - '.$masterFormatItems->title.$separator }}
                          <br/>
                          <?php
                          $i++;
                          }
                          }
                          ?>
                        </td>
                        <td>
                          {{$item->total_rating or 'not rated'}}
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                 </div>
                @else
                <p class="text-center">{{trans('labels.no_records')}}</p>
                @endif
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6 pull-right">
                  <div class="pull-right">
                  <?php echo $addressBookEntries->render(); ?></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
  @include('popups.approve_popup')
  @include('popups.delete_record_popup')