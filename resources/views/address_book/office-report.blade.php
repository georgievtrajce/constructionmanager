<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>


    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700,400,600" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="cm-page-content">
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <table width="690px">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <table width="690px">
                                    <tr>
                                        <td style="width: 260px; vertical-align: bottom;">
                                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                            @if(count(Auth::user()->company->addresses))
                                                {{Auth::user()->company->addresses[0]->street}}<br>
                                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                            @endif
                                        </td>
                                        <td style="width: 250px; margin-left: 40px; vertical-align: bottom;">
                                            {{Auth::user()->email}}<br>
                                            {{Auth::user()->office_phone}}
                                        </td>
                                        <td style="float: right; width: 180px; vertical-align: bottom;">
                                            <h2 style="float: left; margin: 0px; text-align: right;">{{"Office View"}}</h2>
                                            <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="690px">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <h3>
                                    @if(!is_null($companyOffice->address_book))
                                        {{$companyOffice->address_book->name}}
                                    @endif
                                </h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <style>
                    .ab-padding {
                        padding: 2px 2px !important;
                        text-align: left !important;
                    }
                </style>
                <div class="row">
                    <table width="690px" style="border-bottom: 3px solid black;">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.company_name')}}</td>
                            <td class="ab-padding">{{$companyOffice->address_book->name}}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.default_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyOffice->address_book->default_categories))
                                    @foreach($companyOffice->address_book->default_categories as $defaultCategories)
                                        {{ $defaultCategories->name }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.custom_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyOffice->address_book->custom_categories))
                                    @foreach($companyOffice->address_book->custom_categories as $customCategories)
                                        {{ $customCategories->name }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.master_format_number_and_title')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyOffice->address_book->master_format_items))
                                    @foreach($companyOffice->address_book->master_format_items as $masterFormatItems)
                                        {{ $masterFormatItems->number.' - '.$masterFormatItems->title }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.description_of_services')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">{!! $companyOffice->address_book->note !!}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.synced_with_2')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(!is_null($companyOffice->address_book->company))
                                    <span>{{ $companyOffice->address_book->company->name }}</span>
                                @else
                                    <span>{{ trans('labels.none') }}</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="690px" style="font-size: 18px; font-weight: bold; margin-top: 20px!important;">
                        <tr>
                            <td style="width: 690px;">
                                {{ $companyOffice->office_title }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="690px">
                        <tr>
                            <td style="width: 690px;">
                                {{ $companyOffice->street }}
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 690px;">
                                {{ $companyOffice->city.', '.$companyOffice->state.' '.$companyOffice->zip }}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="690px" style="margin-top: 15px!important;">
                        @if(count($companyOffice->ab_contacts))
                            @foreach($companyOffice->ab_contacts as $officeContact)
                                <tr>
                                    <td style="width: 690px;">
                                        <b>{{ $officeContact->name }}</b><br>
                                        <table>
                                            <tr>
                                                <td><i>{{ trans('labels.title')}}:</i></td><td>{{ $officeContact->title }}</td>
                                            </tr>
                                            <tr>
                                                <td><i>{{ trans('labels.address_book.email_address') }}:</i></td><td>{{ $officeContact->email }}</td>
                                            </tr>
                                            <tr>
                                                <td><i>{{ trans('labels.address_book.office_phone') }}:</i></td><td>{{ $officeContact->office_phone }}</td>
                                            </tr>
                                            <tr>
                                                <td><i>{{ trans('labels.address_book.cell_phone') }}:</i></td><td>{{ $officeContact->cell_phone }}</td>
                                            </tr>
                                            <tr>
                                                <td><i>{{ trans('labels.address_book.fax') }}:</i></td><td>{{ $officeContact->fax }}</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td style="width: 690px;">
                                    {{ trans('labels.no_records') }}
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>