@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <h1 class="cm-heading">
            {{trans('labels.address_book.add_new_book_item')}}
            <small class="cm-heading-sub">{{trans('labels.address_book.basic_information')}}</small>
            </h1>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('write', 'address-book')))
                    <a href="{{URL('address-book/import/google')}}" class="btn btn-success cm-heading-btn">{{trans('labels.address_book.import_google_button')}}</a>
                    <a href="{{URL('address-book/import/outlook')}}" class="btn btn-success cm-heading-btn">{{trans('labels.address_book.import_outlook_button')}}</a>
                @endif
            </div>
        </div>
    </div>

    {{--<div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
        <option>{{trans('labels.basic_info')}}</option>
            <option>{{trans('labels.offices')}}</option>
            <option>{{trans('labels.contacts')}}</option>
            <option>{{trans('labels.ratings')}}</option>
            <option>{{trans('labels.ab_files')}}</option>
        </select>
    </div>--}}

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
        <li role="presentation" class="active">
            <a href="{{URL('/address-book/create')}}">{{trans('labels.basic_info')}}</a>
        </li>
        <li role="presentation">
            <a>{{trans('labels.offices')}}</a>
        </li>
        <li role="presentation">
            <a>{{trans('labels.contacts')}}</a>
        </li>
        <li role="presentation">
            <a>{{trans('labels.ratings')}}</a>
        </li>
        <li role="presentation">
            <a>{{trans('labels.ab_files')}}</a>
        </li>
    </ul>
    <div class="panel">
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p> <strong>{{trans('labels.whoops')}}</strong>
                    {{trans('labels.input_problems')}}
                </p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form role="form" action="{{URL::to('address-book')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.address_book.company_name')}} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}"></div>
                        </div>
                    </div>
                    <div class="cm-spacer-xs"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default master-format-container">
                                <div class="panel-heading">
                                {{trans('labels.address_book.categories')}} <span class="text-danger">*</span>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h5>{{trans('labels.address_book.default_categories')}}</h5>
                                            <?php $old_default_categories = old('default_categories'); ?>
                                            @if(!empty($old_default_categories))
                                            @foreach($defaultCategories as $defaultCategory)
                                            <div class="checkbox">
                                                <label>
                                                <input type="checkbox" value="{{ $defaultCategory['id'] }}" name="default_categories[]" {{{ in_array($defaultCategory['id'], $old_default_categories) ? 'checked' : '' }}}>{{ $defaultCategory['name'] }}</label>
                                            </div>
                                            @endforeach
                                            @else
                                            @foreach($defaultCategories as $defaultCategory)
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" value="{{ $defaultCategory['id'] }}" name="default_categories[]">{{ $defaultCategory['name'] }}</label>
                                                </div>
                                                @endforeach
                                                @endif
                                            </div>
                                            <div class="col-md-6">
                                                <h5>{{trans('labels.address_book.custom_categories')}}</h5>
                                                @if(empty($customCategories[0]))
                                                <p>{{trans('labels.address_book.no_custom_categories_added')}}</p>
                                                @else
                                                <?php $old_custom_categories = old('custom_categories'); ?>
                                                @if(!empty($old_custom_categories))
                                                @foreach($customCategories as $customCategory)
                                                <div class="checkbox">
                                                    <label>
                                                    <input type="checkbox" value="{{ $customCategory['id'] }}" name="custom_categories[]" {{{ in_array($customCategory['id'],$old_custom_categories) ? 'checked' : '' }}}>{{ $customCategory['name'] }}</label>
                                                </div>
                                                @endforeach
                                                @else
                                                @foreach($customCategories as $customCategory)
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="{{ $customCategory['id'] }}" name="custom_categories[]">{{ $customCategory['name'] }}</label>
                                                    </div>
                                                    @endforeach
                                                    @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel panel-default master-format-container">
                                        <div class="panel-heading">{{trans('labels.master_format_number_and_title')}} <span class="text-danger">*</span></div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-6">

                                                <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>
                                                <input type="text" id="master_format_search_add" class="form-control ab-mf-number-title-auto" name="master_format_search">
                                                     </div>
                                            </div>
                                            </div>
                                            <div class="selected-mfs cf">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">{{trans('labels.address_book.description_of_services')}}</div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <!-- <label class="col-md-4 control-label">{{trans('labels.address_book.note')}}</label> -->
                                                <textarea class="form-control span12 textEditor" rows="5" name="note">{!! old('note') !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               {{-- <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">{{trans('labels.address_book.sync_company')}}</div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="control-label">{{trans('labels.address_book.company_uac')}}</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control mb15" id="uac" name="uac" value="{{ old('uac') }}">
                                                        <input type="hidden" id="companyId" name="company_id" value="{{ old('company_id') }}">
                                                    </div>
                                                    <div class="col-md-3">
                                                        <a href="javascript:;" class="btn btn-primary" id="validate-uac">{{trans('labels.address_book.validate_uac')}}</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group registered-company-container"></div>
                                        </div>
                                    </div>
                                </div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success pull-right">{{trans('labels.address_book.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script id="master-format-number-title-template" type="text/template">
            <div class="selected-mf-number-title row">
                <div class="col-md-4">
                    <input type="text" class="form-control mf-number-auto master_format_number" name="master_format_number_#<%= counter %>#" readonly value="<%= mf_number %>">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control mf-title-auto master_format_title" name="master_format_title_#<%= counter %>#" readonly value="<%= mf_title %>">
                </div>
                <div class="remove-entry col-md-2">
                    <a href="javascript:;" class="btn btn-sm btn-danger remove-mf-number-title">X</a>
                    <input type="hidden" name="mf_id_#<%= counter %>#" class="mf-id" value="<%= mf_id %>">
                </div>
            </div>
            </script>
            @endsection
