@extends('layouts.master')
@section('content')
<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-sm-4">Import {{ucwords($type)}} contacts</div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <span><em>Please note that only contacts with a specified Company Name will be uploaded.</em></span>
                    <div class="cm-spacer-normal"></div>
                    <form action="../upload/{{$type}}" method="post" enctype="multipart/form-data">
                        <input name="_token" type="hidden" value="{!! csrf_token() !!}"/>
                        <div class="form-group">
                            <label class="control-label" for="google">Import {{ucwords($type)}} contacts</label>
                            <input type="file" name="{{$type}}" accept=".csv">
                        </div>
                        <input type="submit" value="Submit" class="btn btn-success pull-right">
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
