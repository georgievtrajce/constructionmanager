<h4>{{trans('labels.ab_files')}}</h4>
@if(count($addressBookFiles))
<div class="table-responsive">
    <table class="table table-hover table-bordered cm-table-compact">
        <thead>
            <tr>
                <th></th>
                <th>{{trans('labels.address_book.file_name')}}</th>
                <th>{{trans('labels.address_book.file_number')}}</th>
                <th>{{trans('labels.address_book.upload_date')}}</th>
                <th>{{trans('labels.address_book.file_type')}}</th>
                <th>{{trans('labels.address_book.file_size')}}</th>
                <th>{{trans('labels.address_book.expiration_date')}}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @for($i=0; $i<sizeof($addressBookFiles); $i++)
                <tr>
                    <td class="text-center">
                        <input type="checkbox" name="select_all" class="multiple-items-checkbox-docs" data-id="{{$addressBookFiles[$i]->id}}">
                    </td>
                    <td>
                    <a class="" href="{{URL('/address-book/'.$abId.'/files/'.$addressBookFiles[$i]->id.'/edit')}}">{{$addressBookFiles[$i]->name or ''}}</a>

                    </td>
                    <td>
                        {{$addressBookFiles[$i]->number or ''}}
                    </td>
                    <td>
                        {{Carbon\Carbon::parse($addressBookFiles[$i]->created_at)->format('m/d/Y h:i:s')}}
                    </td>
                    <td>{{pathinfo($addressBookFiles[$i]->file_name, PATHINFO_EXTENSION)}}</td>
                    <td>{{CustomHelper::formatByteSizeUnits($addressBookFiles[$i]->size)}}</td>
                    <td>{{Carbon\Carbon::parse($addressBookFiles[$i]->expiration_date)->format('m/d/Y')}}</td>
                    <td>
                        <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                        <input type="hidden" class="s3FilePath" file-type="address-book" id="{{$addressBookFiles[$i]->id}}" value="{{'company_' . $addressBookFiles[$i]->comp_id . '/contact_'. $addressBookFiles[$i]->ab_id .'/address_book_documents/' . $addressBookFiles[$i]->file_name}}">
                    </td>
                </tr>
            @endfor
        </tbody>
    </table>
</div>
@else
<hr>
<p class="text-center">{{trans('labels.no_records')}}</p>
@endif