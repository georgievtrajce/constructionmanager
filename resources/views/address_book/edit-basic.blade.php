@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cm-heading">
                    {{ $basicInfo->name }}
                </h1>
            </div>
        </div>

        <div class="form-group visible-xs visible-sm">
            <select class="form-control" id="sel1">
                <option selected data-href="{{URL('/address-book/'.$basicInfo->id)}}">{{trans('labels.basic_info')}}</option>
                <option data-href="{{URL('/address-book/'.$basicInfo->id.'/addresses')}}">{{trans('labels.offices')}}</option>
                <option data-href="{{URL('/address-book/'.$basicInfo->id.'/contacts')}}">{{trans('labels.contacts')}}</option>
                <option data-href="{{URL('/address-book/'.$basicInfo->id.'/ratings')}}">{{trans('labels.ratings')}}</option>
                <option data-href="{{URL('/address-book/'.$basicInfo->id.'/files')}}">{{trans('labels.ab_files')}}</option>
            </select>
        </div>

        <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
            <li role="presentation" class="active">
                <a href="{{URL('/address-book/'.$basicInfo->id)}}">{{trans('labels.basic_info')}}</a>
            </li>
            <li role="presentation">
                <a href="{{URL('/address-book/'.$basicInfo->id.'/addresses')}}">{{trans('labels.offices')}}</a>
            </li>
            <li role="presentation">
                <a href="{{URL('/address-book/'.$basicInfo->id.'/contacts')}}">{{trans('labels.contacts')}}</a>
            </li>
            <li role="presentation">
                <a href="{{URL('/address-book/'.$basicInfo->id.'/ratings')}}">{{trans('labels.ratings')}}</a>
            </li>
            <li role="presentation">
                <a href="{{URL('/address-book/'.$basicInfo->id.'/files')}}">{{trans('labels.ab_files')}}</a>
            </li>
        </ul>

        <div class="panel">
            <div class="panel-body">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    </p>
                @endif

                {!! Form::open(['method'=> 'PUT', 'url'=>URL('address-book/'.$basicInfo->id)]) !!}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.address_book.company_name')}} <span class="text-danger">*</span></label>
                            <input type="text" class="form-control" name="name" value="{{ $basicInfo->name }}">
                        </div>
                    </div>
                    @if(is_null($basicInfo->synced_comp_id))
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <a class="btn cm-btn-secondary btn-sm mr5" href="{{URL('/referral/invite/')}}">{{trans('labels.invite')}}</a>
                                <span>{{trans('labels.address_book.invite_company')}}</span>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="cm-spacer-xs"></div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default master-format-container">
                            <div class="panel-heading">
                                {{trans('labels.address_book.categories')}} <span class="text-danger">*</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>{{trans('labels.address_book.default_categories')}}</h5>
                                        @foreach($defaultCategories as $defaultCategory)
                                            @if(in_array($defaultCategory['id'], $defaultCategoriesIds))
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="{{ $defaultCategory['id'] }}"
                                                               name="default_categories[]" checked>
                                                        {{ $defaultCategory['name'] }}
                                                    </label>
                                                </div>


                                            @else
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" value="{{ $defaultCategory['id'] }}"
                                                               name="default_categories[]">
                                                        {{ $defaultCategory['name'] }}
                                                    </label>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <div class="col-md-6">
                                        <h5>{{trans('labels.address_book.custom_categories')}}</h5>
                                        @if(empty($customCategories[0]))
                                            <p>{{trans('labels.address_book.no_custom_categories_added')}}</p>
                                        @else
                                            @foreach($customCategories as $customCategory)
                                                @if(in_array($customCategory['id'], $customCategoriesIds))
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="{{ $customCategory['id'] }}"
                                                                   name="custom_categories[]" checked>
                                                            {{ $customCategory['name'] }}
                                                        </label>
                                                    </div>
                                                @else

                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="{{ $customCategory['id'] }}"
                                                                   name="custom_categories[]">
                                                            {{ $customCategory['name'] }}
                                                        </label>
                                                    </div>
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default master-format-container">
                            <div class="panel-heading">
                                {{trans('labels.master_format_number_and_title')}} <span class="text-danger">*</span>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label">{{trans('labels.address_book.master_format_search')}}</label>

                                    <input type="text" id="master_format_search_add"
                                           class="form-control ab-mf-number-title-auto" name="master_format_search">

                                </div>

                                <div class="selected-mfs cf">
                                    @if(count($basicInfo->master_format_items))
                                        <?php $masterFormatCount = 0; ?>
                                        @foreach($basicInfo->master_format_items as $masterFormatItems)
                                            <div class="selected-mf-number-title row">
                                                <div class="col-md-4">
                                                    <input type="text"
                                                           class="form-control mf-number-auto master_format_number"
                                                           name="master_format_number_#{{ $masterFormatCount }}#"
                                                           readonly value="{{ $masterFormatItems->number }}">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text"
                                                           class="form-control mf-title-auto master_format_title"
                                                           name="master_format_title_#{{ $masterFormatCount }}#"
                                                           readonly value="{{ $masterFormatItems->title }}">
                                                </div>
                                                <div class="remove-entry col-md-2">
                                                    <a href="javascript:;"
                                                       class="btn btn-sm btn-danger remove-mf-number-title">X</a>
                                                    <input type="hidden" name="mf_id_#{{ $masterFormatCount }}#"
                                                           class="mf-id" value="{{ $masterFormatItems->id }}">
                                                </div>
                                            </div>
                                            <?php $masterFormatCount++; ?>
                                        @endforeach
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('labels.address_book.description_of_services')}}
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- <label class="col-md-4 control-label">{{trans('labels.address_book.note')}}</label> -->
                                    <textarea class="form-control span12 textEditor" rows="5"
                                              name="note">{!! $basicInfo->note !!}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                {{trans('labels.address_book.sync_company')}}
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label">{{trans('labels.address_book.company_uac')}}</label>

                                    <div class="row">


                                        <div class="col-md-6">
                                            <input type="text" class="form-control mb15" id="uac" name="uac"
                                                   value="{{ old('uac') }}">
                                            <input type="hidden" id="companyId" name="company_id"
                                                   value="{{ is_null($basicInfo->synced_comp_id) ? '' : $basicInfo->synced_comp_id }}">
                                        </div>
                                        <div class="col-md-3">
                                            <a href="javascript:;" class="btn btn-primary"
                                               id="validate-uac">{{trans('labels.address_book.validate_uac')}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="registered-company-container cm-company-list">
                                    @if(!is_null($basicInfo->synced_comp_id) && !empty($basicInfo->address_book))
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <span>{{trans('labels.address_book.synced_with')}}</span>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Avatar</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                {{ $basicInfo->address_book->name }}
                                                            </td>
                                                            <td>
                                                                @if(is_null($basicInfo->address_book->logo))
                                                                    <img src="{{URL('/img/cm-img-user.png')}}" alt="">
                                                                @else
                                                                    <img src="{{ URL(Config::get('constants.s3_base_url').'/'.Config::get('filesystems.disks.s3.bucket').'/'.$basicInfo->address_book->logo) }}"
                                                                        alt="">
                                                                @endif
                                                            </td>
                                                            <td><a href="javascript:;" class="btn btn-danger"
                                                                id="remove-synced-company">{{trans('labels.remove')}}</a>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    -->
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success pull-right">
                            {{trans('labels.update')}}
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script id="master-format-number-title-template" type="text/template">
        <div class="selected-mf-number-title row">
            <div class="col-md-4">
                <input type="text" class="form-control mf-number-auto master_format_number"
                       name="master_format_number_#<%= counter %>#" readonly value="<%= mf_number %>">
            </div>
            <div class="col-md-6">
                <input type="text" class="form-control mf-title-auto master_format_title"
                       name="master_format_title_#<%= counter %>#" readonly value="<%= mf_title %>">
            </div>
            <div class="remove-entry col-md-2">
                <a href="javascript:;" class="btn btn-sm btn-danger remove-mf-number-title">X</a>
                <input type="hidden" name="mf_id_#<%= counter %>#" class="mf-id" value="<%= mf_id %>">
            </div>
        </div>
    </script>
@endsection
