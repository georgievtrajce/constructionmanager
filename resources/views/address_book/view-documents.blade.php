@extends('layouts.master')

@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <h1 class="cm-heading">
          {{ $basicInfo->name }}
        </h1>
      </div>
      <div class="col-md-7">
        <div class="cm-btn-group cm-pull-right cf">
          <a href="{{URL('address-book/'.$abId.'/files/create')}}" class="btn btn-success cm-heading-btn">{{trans('labels.address_book.create-document')}}</a>

          {!! Form::open(['method'=>'POST', 'class' => 'form-prevent pull-right', 'url'=>URL(''), 'id' => 'delete-form']) !!}
            <input type="hidden" value="{{'files/'}}" id="form-url"  />
            <input name="_method" type="hidden" value="DELETE">
            <button disabled id="delete-button" class='btn btn-danger pull-left' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
              {{trans('labels.delete')}}
            </button>
          {!! Form::close()!!}

        </div>
      </div>
    </div>
    <div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
            <option data-href="{{URL('/address-book/'.$abId)}}">{{trans('labels.basic_info')}}</option>
            <option data-href="{{URL('/address-book/'.$abId.'/addresses')}}">{{trans('labels.offices')}}</option>
            <option selected data-href="{{URL('/address-book/'.$abId.'/contacts')}}">{{trans('labels.contacts')}}</option>
            <option data-href="{{URL('/address-book/'.$abId.'/ratings')}}">{{trans('labels.ratings')}}</option>
            <option selected data-href="{{URL('/address-book/'.$abId.'/files')}}">{{trans('labels.ab_files')}}</option>
        </select>
    </div>

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
      <li  role="presentation"><a href="{{URL('/address-book/'.$abId)}}">{{trans('labels.basic_info')}}</a></li>
      <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/addresses')}}">{{trans('labels.offices')}}</a></li>
      <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/contacts')}}">{{trans('labels.contacts')}}</a></li>
      <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/ratings')}}">{{trans('labels.ratings')}}</a></li>
      <li class="active" role="presentation"><a href="{{URL('/address-book/'.$abId.'/files')}}">{{trans('labels.ab_files')}}</a></li>
    </ul>

    <div class="panel">
      <div class="panel-body">
        @if (count($errors) > 0)
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif
        @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
          </div>
        @endif

        @include('address_book.partials.documents-table', $addressBookFiles)
        @if(count($addressBookFiles) > 0)
            <div class="row">
              <div class="col-sm-12">
                <div class="pull-right">
                  <?php echo $addressBookFiles->render(); ?>
                </div>
              </div>
            </div>
        @endif
      </div>
    </div>
  </div>
@endsection
@include('popups.delete_record_popup')