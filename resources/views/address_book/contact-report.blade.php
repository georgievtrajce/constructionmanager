<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<div class="cm-page-content">
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <table width="100%">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <table width="690px">
                                    <tr>
                                        <td style="width: 260px; vertical-align: bottom;">
                                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                            @if(count(Auth::user()->company->addresses))
                                                {{Auth::user()->company->addresses[0]->street}}<br>
                                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                            @endif
                                        </td>
                                        <td style="width: 250px; margin-left: 40px; vertical-align: bottom;">
                                            {{Auth::user()->email}}<br>
                                            {{Auth::user()->office_phone}}
                                        </td>
                                        <td style="float: right; width: 180px; vertical-align: bottom;">
                                            <h2 style="float: left; margin: 0px; text-align: right;">{{"Contact View"}}</h2>
                                            <p style=" float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="100%" class="table table-hover table-striped cm-table-compact">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <h3>
                                    @if(!is_null($companyContact->office))
                                        @if(!is_null($companyContact->office->address_book))
                                            {{$companyContact->office->address_book->name}}
                                        @endif
                                    @endif
                                </h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <style>
                    .ab-padding {
                        padding: 2px 2px !important;
                        text-align: left !important;
                    }
                </style>
                <div class="row">
                    <table width="100%" style="border-bottom: 3px solid black;">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.company_name')}}</td>
                            <td class="ab-padding">{{$companyContact->office->address_book->name}}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.default_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyContact->office->address_book->default_categories))
                                    @foreach($companyContact->office->address_book->default_categories as $defaultCategories)
                                        {{$defaultCategories->name}}<br>
                                    @endforeach
                                @else
                                    {{trans('labels.no_records')}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.custom_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyContact->office->address_book->custom_categories))
                                    @foreach($companyContact->office->address_book->custom_categories as $customCategories)
                                        {{$customCategories->name}}<br>
                                    @endforeach
                                @else
                                    {{trans('labels.no_records')}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.master_format_number_and_title')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($companyContact->office->address_book->master_format_items))
                                    @foreach($companyContact->office->address_book->master_format_items as $masterFormatItems)
                                        {{$masterFormatItems->number.' - '.$masterFormatItems->title}}<br>
                                    @endforeach
                                @else
                                    {{trans('labels.no_records')}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.description_of_services')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">{!! $companyContact->office->address_book->note !!}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.synced_with_2')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(!is_null($companyContact->office->address_book->company))
                                    <span>{{$companyContact->office->address_book->company->name}}</span>
                                @else
                                    <span>{{trans('labels.none')}}</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="100%" style="font-size: 16px; font-weight: bold; margin: 20px 0px 10px 0px!important;">
                        <tr>
                            <td style="width: 690px;">
                                {{$companyContact->name}}
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table>
                        <tr>
                            <td>
                                <i>{{ trans('labels.title')}}:</i>
                            </td>
                            <td>
                                {{$companyContact->title}}
                            </td>
                        </tr>
                        <tr>
                            <td><i>{{ trans('labels.address_book.email_address') }}:</i></td><td>{{ $companyContact->email }}</td>
                        </tr>
                        <tr>
                            <td><i>{{trans('labels.office').':'}}</i></td>
                            <td>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="vertical-align: top;">
                                            {{$companyContact->office->office_title}}<br>
                                            {{$companyContact->office->street}}<br>
                                            {{$companyContact->office->city.', '.$companyContact->office->state.' '.$companyContact->office->zip}}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td><i>{{ trans('labels.address_book.office_phone') }}:</i></td><td>{{ $companyContact->office_phone }}</td>
                        </tr>
                        <tr>
                            <td><i>{{ trans('labels.address_book.cell_phone') }}:</i></td><td>{{ $companyContact->cell_phone }}</td>
                        </tr>
                        <tr>
                            <td><i>{{ trans('labels.address_book.fax') }}:</i></td><td>{{ $companyContact->fax }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>