@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <h1 class="cm-heading">
            {{ $addressBookCompany->name }}
            </h1>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="cm-pull-right">
                {{--<div class="cm-switch">
                    <span>{{trans('labels.address_book.save_with_docs')}}</span>
                    <input type="checkbox" id="addDocuments" class="cm-switch-input" name="addDocuments" />
                    <label for="addDocuments" class="cm-switch-knob"></label>
                </div>--}}
                <a id="printPDF" href="#" data-href="{{URL('address-book/'.$addressBookCompany->id.'/view/report/')}}" class="btn btn-info">{{ trans('labels.save_pdf') }}</a>
                <a href="{{URL('address-book/'.$addressBookCompany->id.'/projects')}}" class="btn btn-success">{{trans('labels.address_book.list_of_projects')}}</a>
                @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('write', 'address-book')))
                <a href="{{URL('address-book/'.$addressBookCompany->id)}}" class="btn cm-btn-secondary">{{trans('labels.edit')}}</a>
                @endif
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.address_book.company_name').': '}}</label>
                        <div class="cm-form-group__info">
                            <span>{{ $addressBookCompany->name }}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.master_format_number_and_title').': '}}</label>
                        <div class="cm-form-group__info">
                            <ul class="cm-item ">
                                @if(count($addressBookCompany->master_format_items))
                                @foreach($addressBookCompany->master_format_items as $masterFormatItems)
                                <li class="item">{{ $masterFormatItems->number.' - '.$masterFormatItems->title }}</li>
                                @endforeach
                                @else
                                <li class="item">{{ trans('labels.no_records') }}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.address_book.default_categories').': '}}</label>
                        <div class="cm-form-group__info">
                            <ul>
                                @if(count($addressBookCompany->default_categories))
                                @foreach($addressBookCompany->default_categories as $defaultCategories)
                                <li class="item">{{ $defaultCategories->name }}</li>
                                @endforeach
                                @else
                                <li class="item">{{ trans('labels.no_records') }}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.address_book.custom_categories').': '}}</label>
                        <div class="cm-form-group__info">
                            <ul class="cm-item ">
                                @if(count($addressBookCompany->custom_categories))
                                @foreach($addressBookCompany->custom_categories as $customCategories)
                                <li class="item">{{ $customCategories->name }}</li>
                                @endforeach
                                @else
                                <li class="item">{{ trans('labels.no_records') }}</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.address_book.description_of_services').': '}}</label>
                        <div class="cm-form-group__info">
                            <span>{!! $addressBookCompany->note !!}</span>
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.address_book.synced_with').': '}}</label>
                        <div class="cm-form-group__info">
                            @if(!is_null($addressBookCompany->company))
                            <span>{{ $addressBookCompany->company->name }}</span>
                            @else
                            <span>{{ trans('labels.none') }}</span>
                            @endif
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.rating').': '}}</label>
                        <div class="cm-form-group__info">
                            @if(!is_null($addressBookCompany->total_rating))
                                <span>{{ $addressBookCompany->total_rating }}</span>
                            @else
                                <span>{{ trans('labels.none') }}</span>
                            @endif
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.ab_files').': '}}</label>
                        <div class="cm-form-group__info">
                            @for($i=0; $i<sizeof($addressBookFiles); $i++)
                                <a class="download" href="javascript:;">{{$addressBookFiles[$i]->name}}</a>  ({{Carbon\Carbon::parse($addressBookFiles[$i]->expiration_date)->format('m/d/Y')}})<br />
                                <input type="hidden" class="s3FilePath" file-type="address-book" id="{{$addressBookFiles[$i]->id}}" value="{{'company_' . $addressBookFiles[$i]->comp_id . '/contact_'. $addressBookFiles[$i]->ab_id .'/address_book_documents/' . $addressBookFiles[$i]->file_name}}">
                            @endfor
                        </div>
                    </div>
                    <hr class="mt5 mb5">
                    <div class="cm-form-group">
                        <label class="cm-form-group__label">{{trans('labels.offices').': '}}</label>
                        <div class="cm-form-group__info">
                            @if(count($addressBookCompany->addresses))
                            @foreach($addressBookCompany->addresses as $office)
                            <div class="office-main-container">
                                <div class="mb5">
                                    <a class="address-book-company-office" href="javascript:;">
                                    <b>{{ $office->office_title }}</b></a>
                                </div>
                                <div class="office-container cf panel panel-default" style="display: none;">
                                    <div class="panel-body">
                                        <div>
                                            <a href="https://www.google.com/maps/search/{{ $office->street }}/@37.0625,-95.677068,4z/data=!3m1!4b1" target="_blank">
                                                <span>{{ $office->street }}</span>
                                                <p>{{ $office->city.', '.$office->state.' '.$office->zip }}</p>
                                            </a>
                                            <a href="{{URL('address-book/address/'.$office->id.'/view/office-report')}}" target="_blank" class="btn btn-sm btn-info mb0">{{ trans('labels.save_office_to_pdf') }}</a>
                                        </div>
                                        @if(count($office->ab_contacts))
                                        @foreach($office->ab_contacts as $officeContact)
                                        <div class="panel panel-default mt20 cf">
                                            <div class="panel-heading cm-panel-heading__combo cf">
                                                <span class="cm-panel-heading__combo--txt"><b>{{ $officeContact->name }}</b></span>
                                                <a href="{{URL('address-book/'.$addressBookCompany->id.'/contact/'.$officeContact->id.'/view')}}" class="btn btn-primary btn-sm cm-panel-heading__combo--btn">{{ trans('labels.view') }}</a>
                                            </div>
                                            <div class="panel-body cf">
                                                <table class="cm-table-no-top-border">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <span>Title:</span>
                                                            </td>
                                                            <td>
                                                                <span>{{ $officeContact->title }}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>{{ trans('labels.address_book.email_address') }}:</span>
                                                            </td>
                                                            <td>
                                                                <a href="mailto:{{ $officeContact->email }}">{{ $officeContact->email }}</a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>{{ trans('labels.address_book.office_phone')}}:</span>
                                                            </td>
                                                            <td>
                                                                <span>{{$officeContact->office_phone }}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>{{ trans('labels.address_book.cell_phone')}}:</span>
                                                            </td>
                                                            <td>
                                                                <span>{{$officeContact->cell_phone }}</span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span>{{ trans('labels.address_book.fax')}}:</span>
                                                            </td>
                                                            <td>
                                                                <span>{{$officeContact->fax }}</span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @else
                            <p>{{ trans('labels.no_records') }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('popups.alert_popup')
@endsection