@extends('layouts.master')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
                {{ $basicInfo->name }}
            </h1>
        </div>
    </div>

    <div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
            <option data-href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</option>
            <option selected data-href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</option>
        </select>
    </div>

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
        <li role="presentation"><a href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</a></li>
        <li role="presentation"><a href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</a></li>
        <li class="active" role="presentation"><a href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</a></li>
        <li role="presentation"><a href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</a></li>
        <li role="presentation"><a href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</a></li>
    </ul>

    <div class="panel">

        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
              <div class="row">
                <div class="col-md-6">
            {!! Form::open(['files'=>true, 'method'=> 'PUT', 'url'=>URL('/address-book/'.$companyId.'/contacts/'.$contactId)]) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="contacts-main-cont">

                <div class="row">

                    <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.name')}}</label>
                            <input type="text" class="form-control" name="cc_name" value="{{ $contact->name }}">
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.title')}}</label>
                            <input type="text" class="form-control" name="cc_title" value="{{ $contact->title }}">
                        </div>
                        </div>
                     <div class="col-sm-12">
                        <div class="form-group">
                            <label class="control-label">{{trans('labels.address_book.address_name_office')}}</label>
                            <select name="cc_address_office_name">
                                <option value="">{{trans('labels.address_book.select_address')}}</option>
                                @if(count($contactAddresses) > 0)
                                @foreach($contactAddresses as $contactAddress)
                                <option {{ $contact->address_id == $contactAddress->id ? 'selected' : '' }} value="{{ $contactAddress->id }}">{{ isset
                                    ($contactAddress->office_title) ? $contactAddress->office_title : $contactAddress->state }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            </div>
                     <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.address_book.email_address')}}</label>
                                <input type="text" class="form-control"
                                name="cc_email" value="{{ $contact->email }}">
                            </div>
                        </div>
                        </div>
                         <hr>
                         <div class="row">

                     <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.address_book.office_phone')}}</label>
                                <input type="text" class="form-control"
                                name="cc_office_phone" value="{{ $contact->office_phone }}">
                            </div>
                            </div>
                     <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.address_book.cell_phone')}}</label>
                                <input type="text" class="form-control"
                                name="cc_cell_phone" value="{{ $contact->cell_phone }}">
                            </div>
                            </div>
                     <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.address_book.fax')}}</label>
                                <input type="text" class="form-control"
                                name="cc_fax" value="{{ $contact->fax }}">
                            </div>
                        </div>
                           </div>
                    </div>

                    <div class="cm-spacer-xs"></div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                             <a href="{{URL('/address-book/'.$companyId.'/contacts')}}" class="btn cm-btn-secondary">{{trans('labels.address_book.cancel_update')}}</a>
                             <button type="submit" class="btn btn-success pull-right">
                                {{trans('labels.address_book.update_contact')}}
                            </button>

                        </div>
                    </div>
                </div>



            </div>
            {!! Form::close() !!}
            </div>


                    <h4 class="pull-left">
                        {{trans('labels.address_book.contacts_list_title')}}
                    </h4>

                    {!! Form::open(['method'=>'POST', 'class' => 'form-prevent pull-right', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                        <input type="hidden" value="{{'contacts/'}}" id="form-url"  />
                        <input name="_method" type="hidden" value="DELETE">
                        <button disabled id="delete-button" class='btn btn-xs btn-danger pull-left' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                            {{trans('labels.delete')}}
                        </button>
                    {!! Form::close()!!}

                    @if(!$contacts->isEmpty())
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered cm-table-compact">
                            <thead>
                                <tr>
                                <th></th>
                                    <th>{{trans('labels.name')}}</th>
                                    <th>{{trans('labels.title')}}</th>
                                    <th>{{trans('labels.office')}}</th>
                                    <th>{{trans('labels.email')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($contacts as $item)
                                <tr>
                                <td class="text-center"><input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$item->id}}"></td>
                                    <td>
                                    <a href="{{URL('address-book/'.$companyId.'/contacts/'.$item->id.'/edit')}}">{{$item->name}}</a>
                                    </td>
                                    <td>{{$item->title}}</td>
                                    <td>
                                        @if(!is_null($item->office))
                                        {{$item->office->office_title}}
                                        @endif
                                    </td>
                                    <td>{{$item->email}}</td>

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <p class="text-center">{{trans('labels.address_book.no_contacts')}}</p>
                    @endif

            </div>
 </div>
            <div>
        </div>
    </div>

</div>
@endsection
@include('popups.delete_record_popup')