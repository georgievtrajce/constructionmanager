<!DOCTYPE html>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
    <link rel="stylesheet"
          href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css"/>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700,400,600"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head><body>
<div class="cm-page-content">
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <table width="690px">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <table width="690px">
                                    <tr>
                                        <td style="width: 260px; vertical-align: bottom;">
                                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                            @if(count(Auth::user()->company->addresses))
                                                {{Auth::user()->company->addresses[0]->street}}<br>
                                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                            @endif
                                        </td>
                                        <td style="width: 250px; margin-left: 40px; vertical-align: bottom;">
                                            {{Auth::user()->email}}<br>
                                            {{Auth::user()->office_phone}}
                                        </td>
                                        <td style="float: right; width: 180px; vertical-align: bottom;">
                                            <h2 style="float: left; margin: 0px; text-align: right;">{{"Company View"}}</h2>

                                            <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="690px">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <h3>
                                    {{$addressBookCompany->name}}
                                </h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <style>
                    .ab-padding {
                        padding: 2px 2px !important;
                        text-align: left !important;
                    }
                </style>
                <div class="row">
                    <table width="690px" style="border-bottom: 3px solid black;">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.company_name')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">{{$addressBookCompany->name}}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.default_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($addressBookCompany->default_categories))
                                    @foreach($addressBookCompany->default_categories as $defaultCategories)
                                        {{ $defaultCategories->name }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.custom_categories')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($addressBookCompany->custom_categories))
                                    @foreach($addressBookCompany->custom_categories as $customCategories)
                                        {{ $customCategories->name }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.master_format_number_and_title')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(count($addressBookCompany->master_format_items))
                                    @foreach($addressBookCompany->master_format_items as $masterFormatItems)
                                        {{ $masterFormatItems->number.' - '.$masterFormatItems->title }}<br>
                                    @endforeach
                                @else
                                    {{ trans('labels.no_records') }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.description_of_services')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">{!! $addressBookCompany->note !!}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.address_book.synced_with_2')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(!is_null($addressBookCompany->company))
                                    <span>{{ $addressBookCompany->company->name }}</span>
                                @else
                                    <span>{{ trans('labels.none') }}</span>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">
                                {{trans('labels.rating')}}
                            </td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{$addressBookCompany->total_rating}}
                            </td>
                        </tr>
                    </table>
                    <table width="690px" style="border-bottom: 3px solid black;">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">
                                {{trans('labels.ab_files')}}
                            </td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @for($i=0; $i<sizeof($addressBookFiles); $i++)
                                    {{$addressBookFiles[$i]->name}} ({{Carbon\Carbon::parse($addressBookFiles[$i]->expiration_date)->format('m/d/Y')}})<br />
                                @endfor
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="form-group">
                    <div class="row">
                        <h2 style="margin-bottom: 0px;">{{trans('labels.offices').': '}}</h2>
                    </div>
                </div>
                <div class="form-group">
                    @if(count($addressBookCompany->addresses))
                        @foreach($addressBookCompany->addresses as $office)
                            <div class="row">
                                <h3 style="text-decoration: underline;">{{ $office->office_title }}</h3>
                            </div>
                            <div class="office-main-container">
                                <div class="office-container">
                                    {{ $office->street }}<br>
                                    {{ $office->city.', '.$office->state.' '.$office->zip }}<br><br>
                                    @if(count($office->ab_contacts))
                                        @foreach($office->ab_contacts as $officeContact)
                                            <b>{{ $officeContact->name }}</b><br>
                                            <table>
                                                <tr>
                                                    <td><i>{{ trans('labels.title')}}:</i></td><td>{{ $officeContact->title }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i>{{ trans('labels.address_book.email_address') }}:</i></td><td>{{ $officeContact->email }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i>{{ trans('labels.address_book.office_phone') }}:</i></td><td>{{ $officeContact->office_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i>{{ trans('labels.address_book.cell_phone') }}:</i></td><td>{{ $officeContact->cell_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td><i>{{ trans('labels.address_book.fax') }}:</i></td><td>{{ $officeContact->fax }}</td>
                                                </tr>
                                            </table>
                                        @endforeach
                                    @else
                                        {{ trans('labels.no_records') }}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
</body></html>