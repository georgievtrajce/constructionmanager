@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="cm-heading">
                    {{ $basicInfo->name }}
                </h1>
            </div>
        </div>
    <div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
            <option data-href="{{URL('/address-book/'.$abId)}}">{{trans('labels.basic_info')}}</option>
            <option data-href="{{URL('/address-book/'.$abId.'/addresses')}}">{{trans('labels.offices')}}</option>
            <option data-href="{{URL('/address-book/'.$abId.'/contacts')}}">{{trans('labels.contacts')}}</option>
            <option data-href="{{URL('/address-book/'.$abId.'/ratings')}}">{{trans('labels.ratings')}}</option>
            <option selected data-href="{{URL('/address-book/'.$abId.'/files')}}">{{trans('labels.ab_files')}}</option>
        </select>
    </div>

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
            <li  role="presentation"><a href="{{URL('/address-book/'.$abId)}}">{{trans('labels.basic_info')}}</a></li>
            <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/addresses')}}">{{trans('labels.offices')}}</a></li>
            <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/contacts')}}">{{trans('labels.contacts')}}</a></li>
            <li  role="presentation"><a href="{{URL('/address-book/'.$abId.'/ratings')}}">{{trans('labels.ratings')}}</a></li>
            <li class="active" role="presentation"><a href="{{URL('/address-book/'.$abId.'/files')}}">{{trans('labels.ab_files')}}</a></li>
        </ul>

        <div class="panel">
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                    <form role="form" class="create" id="form" action="{{URL('/address-book/'.$abId.'/files/create')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <h4>{{(!empty($addressBookFile))?trans('labels.document.edit'):trans('labels.document.add')}}</h4>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.document.name')}}</label>
                                    <input type="text" id="fileName" class="form-control" name="file_name" value="{{(!empty($addressBookFile))?$addressBookFile->name:old('file_name') }}">
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.document.number')}}</label>
                                    <input type="text" id="fileNumber" class="form-control" name="file_number" value="{{(!empty($addressBookFile))?$addressBookFile->number:old('file_number') }}">
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.address_book.expiration_date')}}</label>
                                    <input type="text" id="expirationDate" class="form-control" name="file_expiration_date" value="{{(!empty($addressBookFile))?Carbon::parse($addressBookFile->expiration_date)->format('m/d/Y'):old('file_expiration_date') }}">
                                </div>
                                <div class="form-group">
                                    <label class="cm-control-required">{{trans('labels.address_book.file_orientation')}}</label>
                                    <select id="file_orientation" name="file_orientation">
                                        <option {{(!empty($addressBookFile) && $addressBookFile->file_orientation == 'P') || old('file_orientation') == 'P'?'selected':''}} value="P">Portrait</option>
                                        <option {{(!empty($addressBookFile) && $addressBookFile->file_orientation == 'L') || old('file_orientation') == 'L'?'selected':''}} value="L">Landscape</option>
                                    </select>
                                </div>
                                <div class="">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-8">
                                                <label for="contract_file">{{trans('labels.File')}}:</label>

                                                <!-- upload select input -->
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                                    <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            <input type="file" style="display: none;" name="address_book_file" id="address_book_file" multiple>

                                                            <input type="hidden" name="address_book_file_id" id="address_book_file_id" value="{{(!empty($addressBookFile))?$addressBookFile->id:old('address_book_file_id') }}">
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                    </label>
                                                </div>


                                            </div>

                                            <div class="col-md-4">
                                                <button type="button" id="address-book-file-upload" class="btn btn-info pull-right cm-btn-fixer">
                                                    <div id="address_book_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                                        <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                    </div>
                                                    {{trans('labels.upload')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row file_main_message_container">
                                        <div id="file_message_container"
                                             class="col-md-12 address_book_file_message_container message_container"
                                             style="color: #49A078;"></div>
                                    </div>
                                    @if (!empty($addressBookFile))
                                    <div class="row" id="currentFile">
                                        <div class="col-sm-12">
                                        <div class="panel panel-default">
                                        <div class="panel-body">
                                            <label>{{trans('labels.file.current')}}:</label>
                                            <a class="download" href="javascript:;">{{$addressBookFile->file_name}}</a>
                                            <input type="hidden" class="s3FilePath" file-type="address-book"  id="{{$addressBookFile->id}}" value="{{'company_' . $addressBookFile->comp_id . '/contact_'. $addressBookFile->ab_id .'/address_book_documents/' . $addressBookFile->file_name}}">
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="cm-spacer-xs"></div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <input type="hidden" id="type" name="dtype" value="{{Config::get('constants.address-book-files')}}">
                                            <input type="hidden" id="file_type" name="type" value="address-book-file">
                                            <input type="hidden" id="ab_contact_id" name="type" value="{{$abId}}">
                                            <input type="hidden" id="project_file_type" name="project_file_type" value="address-book-document">
                                            <button id="submit_file" type="submit" class="btn btn-sm btn-success">
                                                {{trans('labels.save')}}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Send email notification</label>
                                    </div>
                              
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="number" id="daysNotify" name="daysNotify" class="" min="0" value="{{$addressBookFile->notification_days or ''}}"/>
                                                </div>
                                                <div class="col-md-8">
                                                    <span class="number-days-text ml5">days before due date.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                            <div class="cms-list-box">
                                                <ul class="cms-list-group">
                                                    <li class="cms-list-group-item cf">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" checked class="companyCheckbox" name="companyRadioDistribution" data-type="user" id="mycompany">
                                                                <span class="radio-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                {{$myCompany->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li class="cms-list-group-item cf">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="companyCheckboxDistribution" name="companyRadioDistribution" data-type="contact" id="address-book-company">
                                                                <span class="radio-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                {{$basicInfo->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                            <div class="cms-list-box">
                                                <ul class="cms-list-group" id="usersList">
                                                    @foreach($myCompany->users as $user)
                                                        <li class="cms-list-group-item cf">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input {{((!empty($selectedDistributionsUsers) && in_array($user->id, $selectedDistributionsUsers)) || (!empty(old('employees_users')) && in_array($user->id, old('employees_users'))))?'checked':''}} type="checkbox" name="employees_users[]" value="{{$user->id}}">
                                                                    <span class="checkbox-material">
                                                        <span class="check"></span>
                                                    </span>
                                                                    {{$user->name}}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                <ul class="cms-list-group hide" id="abCompanyUsersList">
                                                    @foreach($basicInfo->contacts as $contact)
                                                        <li class="cms-list-group-item cf">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input {{((!empty($selectedDistributionsContacts) && in_array($contact->id, $selectedDistributionsContacts)) || (!empty(old('employees_users_ab')) && in_array($contact->id, old('employees_users_ab'))))?'checked':''}} type="checkbox" name="employees_users_ab[]" value="{{$contact->id}}">
                                                                    <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                    {{$contact->name}}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @if(!empty($addressBookFile))
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h4 class="mb20">{{trans('labels.tasks.assignees')}}</h4>

                                            @if(!empty($addressBookFile->addressBookFileUsers))
                                                @foreach($addressBookFile->addressBookFileUsers as $addressBookFileUser)
                                                    @if(!empty($addressBookFileUser->users))
                                                        @foreach($addressBookFileUser->users as $user)
                                                            {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}}<br />
                                                        @endforeach
                                                    @endif
                                                    @if(!empty($addressBookFileUser->abUsers))
                                                        @foreach($addressBookFileUser->abUsers as $user)
                                                            {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}}<br />
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
@include('address_book.partials.successful-upload')
@include('address_book.partials.error-upload')
@include('address_book.partials.upload-limitation')
<script id="cms-dev-alert" type="text/template">
    <div class="alert cms-dev-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <span class="cms-dev-alert-content"></span>
    </div>
</script>
@include('popups.alert_popup')
@include('popups.delete_record_popup')
@include('popups.approve_popup')
@endsection
