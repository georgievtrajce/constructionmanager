<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<table width="100%">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="width: 400px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 450px; vertical-align: bottom;">
                        {{Auth::user()->email}}<br>
                        {{Auth::user()->office_phone}}
                    </td>
                    <td style="float: right; vertical-align: bottom;">
                        <h2 style="float: left; margin: 0px; text-align: right;">{{'Address book entries'}}</h2>

                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="row">
    <h3>{{trans('labels.address_book.address_book_filtered_by')}}</h3>
    <h4>
        @if(!empty($category))
            {{$category}}<br/><br/>
        @endif
        @if(!empty($searchData))
            {{'Master Format Number: '.$searchData['number']}}<br />
            {{'Master Format Title: '.$searchData['title']}}<br/><br/>
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .bids-padding {
            padding: 5px 5px !important;
            text-align: left !important;
        }

        h4 {
            font-size: 14px;
            padding: 0px 5px 5px 5px;
            margin: 0px;
        }
    </style>
    @if(count($addressBookEntries) != 0)
        <table style="width: 100%; padding: 0px; margin: 0px;">
            <thead style="border-bottom: 1px solid #ddd;">
                <tr>
                    <th align="left">{{trans('labels.address_book.company_name')}}</th>
                    <th align="left">{{trans('labels.address_book.categories')}}</th>
                    <th align="left">{{trans('labels.master_format_number_and_title')}}</th>
                    <th align="left">{{trans('labels.rating')}}</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($addressBookEntries as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>
                        <?php
                        if(count($item->
                        default_categories)) {
                            $i = 1;
                            foreach($item->default_categories as $default_category) {
                                $separator = ((count($item->custom_categories)) ? ', ' : ((count($item->default_categories) == $i) ? '' : ', '));
                                echo $default_category->name.$separator;
                                $i++;
                            }
                        }
                        if(count($item->custom_categories)) {
                            $i = 1;
                            foreach($item->custom_categories as $custom_category) {
                                $separator = (count($item->custom_categories) == $i) ? '' : ', ';
                                echo $custom_category->name.$separator;
                                $i++;
                            }
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if (count($item->
                        master_format_items)) {
                        $i = 1;
                        foreach ($item->master_format_items as $masterFormatItems) {
                        $separator = (count($item->master_format_items) == $i) ? '' : ', ';
                        ?>
                        <p>
                        {{ $masterFormatItems->number.' - '.$masterFormatItems->title.$separator }}
                        <p>
                        <?php
                        $i++;
                        }
                        }
                        ?>
                    </td>
                    <td>
                        {{$item->total_rating or 'not rated'}}
                    </td>
                </tr>
                <tr class="cm-row-separator">
                    <td colspan="11"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
</body>
</html>