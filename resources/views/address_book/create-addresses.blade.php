@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">{{$basicInfo->name}}</h1>
        </div>
    </div>
    <div class="form-group visible-xs visible-sm">
        <select class="form-control" id="sel1">
            <option data-href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</option>
            <option selected data-href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</option>
            <option data-href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</option>
        </select>
    </div>

    <ul class="nav nav-tabs cm-tab-alt hidden-xs hidden-sm" role="tablist">
        <li role="presentation">
            <a href="{{URL('/address-book/'.$companyId)}}">{{trans('labels.basic_info')}}</a>
        </li>
        <li class="active" role="presentation">
            <a href="{{URL('/address-book/'.$companyId.'/addresses')}}">{{trans('labels.offices')}}</a>
        </li>
        <li role="presentation">
            <a href="{{URL('/address-book/'.$companyId.'/contacts')}}">{{trans('labels.contacts')}}</a>
        </li>
        <li role="presentation">
            <a href="{{URL('/address-book/'.$companyId.'/ratings')}}">{{trans('labels.ratings')}}</a>
        </li>
        <li role="presentation">
            <a href="{{URL('/address-book/'.$companyId.'/files')}}">{{trans('labels.ab_files')}}</a>
        </li>
    </ul>
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p> <strong>{{trans('labels.whoops')}}</strong>
                    {{trans('labels.input_problems')}}
                </p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <form role="form" action="{{URL('/address-book/'.$companyId.'/addresses')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="addresses-main-cont">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.address_book.office_name')}}</label>
                                        <input type="text" class="form-control" name="office_title" value="{{ old('office_title') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.Address')}}</label>
                                        <input type="text" class="form-control" name="ca_street" value="{{ old('ca_street') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.city')}}</label>
                                        <input type="text" class="form-control" name="ca_town" value="{{ old('ca_town') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.state')}}</label>
                                        <input type="text" class="form-control" name="ca_state" value="{{ old('ca_state') }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.zip')}}</label>
                                        <input type="text" class="form-control" name="ca_zip" value="{{ old('ca_zip') }}">
                                    </div>
                                </div>
                            </div>
                            <div class="cm-spacer-xs"></div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success pull-right">{{trans('labels.save_address')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                    <div class="cm-spacer-xs"></div>
                    <div class="cf">
                        <h4 class="pull-left">
                        {{trans('labels.address_book.addresses_list_title')}}
                        </h4>

                        {!! Form::open(['method'=>'POST', 'class' => 'form-prevent pull-right', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                        <input type="hidden" value="{{'addresses/'}}" id="form-url"  />
                        <input name="_method" type="hidden" value="DELETE">
                        <button disabled id="delete-button" class='btn btn-xs btn-danger pull-left' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                            {{trans('labels.delete')}}
                        </button>
                        {!! Form::close()!!}

                        @if(!$addresses->isEmpty())
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{{trans('labels.address_book.office_name')}}</th>
                                        <th>{{trans('labels.Address')}}</th>
                                        <th>{{trans('labels.city')}}</th>
                                        <th>{{trans('labels.state')}}</th>
                                        <th>{{trans('labels.zip')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($addresses as $item)
                                    <tr>
                                        <td class="text-center"><input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$item->id}}"></td>
                                        <td>
                                            <a href="{{URL('address-book/'.$companyId.'/addresses/'.$item->
                                                id.'/edit')}}">{{$item->office_title}}
                                            </a>
                                        </td>
                                        <td>{{$item->street}}</td>
                                        <td>{{$item->city}}</td>
                                        <td>{{$item->state}}</td>
                                        <td>{{$item->zip}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                        @else
                        <div class="row">
                            <div class="col-md-12">
                                <hr class="mt0">
                                <p class="text-center">{{trans('labels.address_book.no_addresses')}}</p>
                            </div>
                        </div>
                        @endif
                </div>
            </div>
        </div>
        @endsection
        @include('popups.delete_record_popup')