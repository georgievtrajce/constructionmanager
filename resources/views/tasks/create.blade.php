@extends('layouts.master') @section('content')
<div class="container">
  <div class="row">
    <div class="col-md-7">
      <header class="cm-heading">
        @if(empty($task)) {{trans('labels.tasks.create')}} @endif @if(!empty($task)) {{trans('labels.tasks.edit')}}
        <small class="cm-heading-suffix">{{trans('labels.tasks.task').': '.$task->title}}</small>
        <ul class="cm-trail">
          <li class="cm-trail-item"><a href="{{URL('tasks')}}" class="cm-trail-link">{{trans('labels.tasks.tasks')}}</a></li>
          <li class="cm-trail-item active"><a href="" class="cm-trail-link">{{trans('labels.tasks.edit')}}</a></li>
        </ul>
        @endif
      </header>
    </div>
    <div class="col-md-5">
      <div class="cm-pull-right">
        <div class="cm-btn-group cf">
          @if (!empty($task))
          <a target="_blank" href="{{URL('tasks/report/single/'.$task->id)}}" class="btn btn-success pull-right cm-heading-btn">{{trans('labels.address_book.save_to_pdf')}}</a>          @if (empty($task->parent_id) && empty($_GET['parent']))
          <a class="btn btn-success pull-right cm-heading-btn" href="{{URL('tasks/create')."?parent=".$task->id.'&project='.$task->project_id}}">{{trans('labels.tasks.add-subtasks')}}</a>          @endif @endif
        </div>
      </div>
    </div>
  </div>
  @if(!empty($project))
    <div class="row">
        <div class="col-sm-12">
            @include('projects.partials.tabs', array('activeTab' => 'project-tasks'))
        </div>
    </div>
  @endif
  <div class="row">
    <div class="col-sm-12">
      <div class="panel">
        <div class="panel-body">
          @if (Session::has('flash_notification.message'))
          <div class="alert alert-{{ Session::get('flash_notification.level') }}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
            }}
          </div>
          @endif @if (empty($task)) {!! Form::open(['method'=> 'POST', 'url'=>'tasks']) !!} @else {!! Form::open(['method'=> 'PUT',
          'url'=>'tasks/'.$task->id]) !!} @endif
          <input type="hidden" value="{{(!empty($task->parent_id))?$task->parent_id:(isset($_GET['parent'])?$_GET['parent']:'')}}"
            name="parent_id" />
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-12">
                  <h4 class="mb20">{{trans('labels.tasks.general-info')}}</h4>
                  <div class="form-group">
                    <label class="cm-control-required ">{{trans('labels.tasks.title')}}</label>
                    <input type="text" class="form-control " name="title" id="title" value="{{(!empty($task->title))?$task->title:(!empty(old('title'))?old('title'):Input::get('title'))}}">
                  </div>
                </div>
                @if (!(!empty(Input::get('module')) && Input::get('module') == "1") && (empty($task) || !empty($task) && empty($task->type_id)))
                <div class="col-md-10">
                  <div class="form-group">
                    <label class="cm-control">{{trans('labels.tasks.mf_number')}}</label>
                    <input type="text" class="form-control mf-number-title-auto ui-autocomplete-input" name="master_format_search" value="{{(!empty($task->mf_number) && !empty($task->mf_title))?$task->mf_number."
                      -
                      ".$task->mf_title:(!empty(old('master_format_search'))?old('master_format_search'):Input::get('mf_number_title'))}}" autocomplete="off">
                    <input type="hidden" class="master-format-id" name="master_format_id">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group cf">
                    <div class="mf-cont">
                      <label class="control-label">&nbsp;</label>
                      <a href="javascript:;" id="custom-mf" class="btn btn-primary mb0 pull-right">
                          {{trans('labels.submittals.enter_custom_number_and_title')}}
                        </a>
                    </div>
                  </div>
                </div>
                @endif
                <div class="col-md-12">
                  <label class="control-label">{{trans('labels.masterformat.selected')}}:</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <input readonly type="text" class="form-control mf-number-auto" name="mf_number" value="{{(!empty($task->mf_number))?$task->mf_number:(!empty(old('mf_number'))?old('mf_number'):Input::get('mf_number'))}}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <input readonly type="text" class="form-control mf-title-auto" name="mf_title" value="{{(!empty($task->mf_title))?$task->mf_title:(!empty(old('mf_title'))?old('mf_title'):Input::get('mf_title'))}}">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="cm-control-required ">{{trans('labels.tasks.status')}}</label> {!! Form::select('status',config('constants.tasks.status'),
                    (!empty($task->status))?$task->status:Input::old('status')) !!}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="cm-control-required ">{{trans('labels.tasks.due_date')}}</label>
                    <input type="text" id="expiration-date" class="form-control" name="expiration-date" value="{{ (!empty($task->due_date))?Carbon::createFromFormat('Y-m-d', $task->due_date)->format('m/d/Y'):old('expiration-date') }}">
                  </div>
                </div>
                <div class="form-group cf">
                    <div class="col-md-12">
                      <label class="cm-control">{{trans('labels.tasks.send_notification')}}</label>
                    </div>
                    <div class="col-md-4">

                        <input type="number" id="daysNotify" name="daysNotify" class="number-days-tasks" min="0" value="{{$task->notification_days or ''}}" />

                    </div>
                    <div class="col-md-8">
                        <span class="number-days-text ml5">days before due date.</span>
                    </div>
                </div>
                  <div class="col-md-12">
                      <div class="row">
                          <div class="form-group">
                              <div class="col-md-4">
                                  <label for="contract_file">{{trans('labels.File')}}:</label>
                                  <input type="file" name="project_file" id="file" multiple>
                                  <input type="hidden" name="file_id" id="file_id" value="{{ (!empty($projectFile))?$projectFile->id:old('file_id') }}">
                              </div>
                              <div class="col-md-4">
                                  <div id="file_wait" style="display: none;">
                                      <img class="pull-right" src="{{URL('/img/pleasewait.gif')}}" alt="" width="40px">
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <button type="button" id="file-upload-create-task" class="btn btn-info btn-sm pull-right cm-btn-fixer">
                                      {{trans('labels.upload')}}
                                  </button>
                              </div>
                          </div>
                      </div>
                      <div class="row file_main_message_container" style="padding-bottom:10px;">
                          <div id="file_message_container"
                               class="col-md-12 file_message_container message_container"
                               style="color: #49A078;"></div>
                      </div>
                      @if(!empty($projectFile))
                      <div class="row currentFile">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <p>{{trans('labels.file.current').': '}}</p>
                                  <a class="download" href="javascript:;" style="display: inline-block;float:left;">{{$projectFile->file_name}}</a>
                                  <a target="_blank" class="btn btn-sm btn-danger delete-file pull-left mb0 ml5" href="javascript:;"
                                     data-toggle="modal"
                                     data-target="#confirmDelete"
                                     data-title="Delete Record"
                                     data-message="{{trans('labels.global_delete_modal')}}">
                                      <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                      <div class="pull-left delete-wait" style="display: none;">
                                          <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                      </div>
                                  <!-- {{trans('labels.delete')}} -->
                                  </a>
                                  <input type="hidden" data-type="tasks" file-type="tasks" class="s3FilePath" id="{{$projectFile->id}}" value="{{'company_' . Auth::user()->company->id . '/tasks/' . $projectFile->file_name}}">
                              </div>
                          </div>
                      </div>
                      @endif
                  </div>
                <div class="col-md-12">
                  <label class="cm-control control-label">{{trans('labels.tasks.notes')}}</label>
                  <div class="form-group">
                    <textarea class="form-control span12 textEditorSmall" rows="5" id="note" name="note">{!! (!empty($task->note))?$task->note:old('note') !!}</textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <hr>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-6">
                  <h4 class="mb20">{{trans('labels.tasks.assign-module')}}</h4>
                  <div class="form-group">
                    <label class="cm-control-required">{{trans('labels.tasks.select_project')}}</label>
                    <select {{(!empty($task->parent_id) || !empty($_GET['parent']) || (!empty(Input::get('module')) && Input::get('module') == "1") || (!empty($task) && !empty($task->type_id)))?'readonly':''}}
                           @if(!empty($task->parent_id) || !empty($_GET['parent']) || (!empty(Input::get('module')) && Input::get('module') == "1") || (!empty($task) && !empty($task->type_id)))  style="pointer-events: none;" @endif name="project" id="task_project">
                      <option value="" {{(!empty($task->project_id) || !empty(old('project')))?'':'selected'}}>{{trans('labels.tasks.select_project')}}</option>
                      @foreach($projects as $project)
                        <option {{(!empty($task->project_id) && $task->project_id == $project->id) || (!empty(old('project')) && old('project') == $project->id) || (!empty(Input::get('project')) && Input::get('project') == $project->id)?'selected':''}} value="{{$project->id}}">{{$project->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                @if((!empty(Input::get('module')) && Input::get('module') == "1") || (!empty($task) && !empty($task->type_id)))
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="cm-control">{{trans('labels.tasks.select_type')}}</label>
                    <select name="module_type" {{((!empty(Input::get( 'module')) && Input::get( 'module')=="1" ) || (!empty($task) && !empty($task->type_id)))?'readonly':''}}
                          @if((!empty(Input::get('module')) && Input::get('module') == "1") || (!empty($task) && !empty($task->type_id)))  style="pointer-events: none;" @endif id="module_type">
                        <option value="0" {{(!empty($task->type_id) || !empty(old('module_type')))?'':'selected'}}>{{trans('labels.tasks.select_type')}}</option>
                        @foreach(config('constants.tasks.type') as $key => $value)
                            <option {{(!empty($task->type_id) && $task->type_id == $key) || (!empty(old('module_type')) && old('module_type') == $key) || (!empty(Input::get('module_type')) && Input::get('module_type') == $key)?'selected':''}} value="{{$key}}">{{$value}}</option>
                        @endforeach
                      </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="cm-control">{{trans('labels.tasks.select_item')}}</label>
                    <select name="module_item" id="module_item" {{((!empty(Input::get( 'module')) && Input::get( 'module')=="1" ) || (!empty($task) && !empty($task->type_id)))?'readonly':''}} @if((!empty(Input::get('module')) && Input::get('module') == "1") || (!empty($task) && !empty($task->type_id)))  style="pointer-events: none;" @endif >
                        <option value="0" {{(!empty($selectedItem))?'':'selected'}}>{{trans('labels.tasks.select_item')}}</option>
                        @if (!empty($task) || !empty($selectedItem))
                          @foreach($items as $item)
                            <option {{(!empty($selectedItem) && $selectedItem == $item->id)?'selected':''}} value="{{$item->id}}">{{$item->name}}</option>
                          @endforeach
                        @endif
                      </select>
                  </div>
                </div>
                @else
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="cm-control">{{trans('labels.tasks.type')}}</label>
                    <input type="text" class="form-control " name="custom_type" id="custom_type" value="{{(!empty($task->custom_type))?$task->custom_type:(!empty(old('custom_type'))?old('custom_type'):Input::get('custom_type'))}}">
                  </div>
                </div>
                @endif
                <div class="col-md-12">
                  <h4 class="mb20">{{trans('labels.tasks.assign-users')}}</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="mb5">{{trans('labels.tasks.company')}}</label>
                    <div class="cms-list-box">
                      <ul class="cms-list-group">
                        <li class="cms-list-group-item cf">
                          <div class="radio">
                            <label>                           
                                <input type="radio" checked class="companyCheckbox" name="companyRadio" data-type="user" id="mycompany">
                                <span class="radio-material">
                                    <span class="check"></span>
                                </span> 
                                {{$myCompany->name}}
                            </label>
                          </div>
                        </li>
                        @foreach($companies as $id => $name)
                        <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter {{(!empty($task) && $task->project_id == $projectsCompanies[$id])?'':'hide'}}">
                          <div class="radio">
                            <label>
                                <input type="radio" class="companyCheckbox" name="companyRadio" data-type="contact" data-id="{{$id}}" id="comp_{{$id}}">
                                <span class="radio-material">
                                    <span class="check"></span>
                                </span>
                                {{$name}}
                              </label>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                    <div class="cms-list-box">
                      <ul class="cms-list-group" id="usersList">
                        @foreach($myCompany->users as $user)
                        <li class="cms-list-group-item cf">
                          <div class="checkbox">
                            <label>
                                <input {{!empty($assignedUsers) && in_array($user->id, $assignedUsers)?'checked':''}} type="checkbox" name="employees_users[]" value="{{$user->id}}">
                                <span class="checkbox-material">
                                    <span class="check"></span>
                                </span>
                                {{$user->name}}
                            </label>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                      @foreach($users as $id => $value)
                      <ul class="cms-list-group hide contactsList" id="contactsList-{{$id}}">
                        @foreach($value as $project_contact)
                          @if (!empty($project_contact->contact))
                            <li data-id="{{$projectsCompanies[$id] or ''}}" class="cms-list-group-item cf project-filter">
                              <div class="checkbox">
                                <label>
                                      <input {{!empty($assignedContacts) && in_array($project_contact->contact->id, $assignedContacts)?'checked':''}}
                                             type="checkbox" name="employees_contacts[]" value="{{$projectsCompanies[$id].'-'.$project_contact->contact->id}}">
                                      <span class="checkbox-material">
                                        <span class="check"></span>
                                    </span>
                                      {{$project_contact->contact->name}}
                                    </label>
                              </div>
                            </li>
                          @endif
                        @endforeach
                      </ul>
                      @endforeach
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <button type="submit" id="" class="btn btn-success pull-left cm-heading-btn">
                    {{trans('labels.tasks.save')}}
                  </button>
                  @if(!empty($task))
                  <a class="btn btn-sm pull-left btn-primary sendTaskNotification" title="{{trans('labels.tasks.send_email')}}" href="javascript:;">
                    <span aria-hidden="true">Send Notification</span>
                    <div class="pull-left email_wait" style="display: none;">
                      <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                    </div>
                  </a>
                  @endif
                </div>
              </div>
              @if(!empty($task))
              <div class="row">
                <div class="col-md-12">
                  <h4 class="mb20">{{trans('labels.tasks.assignees')}}</h4>
                </div>
                <div class="col-md-12">
                  @if(!empty($task->taskUsers))
                    @foreach($task->taskUsers as $taskUser)
                      <?php $sendDate = (!empty($taskUser->distribution))?'('.Carbon\Carbon::parse($taskUser->distribution->created_at)->format('m/d/Y').')':''; ?>
                      @if(!empty($taskUser->users))
                        @foreach($taskUser->users as $user)
                          {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}} {{$sendDate}}<br />
                        @endforeach
                      @endif
                        @if(!empty($taskUser->abUsers))
                          @foreach($taskUser->abUsers as $user)
                            {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}} {{$sendDate}}<br />
                          @endforeach
                        @endif
                    @endforeach
                  @endif
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
        {!! Form::close() !!} @include('errors.list')
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  var idModuleItem = <?php echo Input::get('module_item', 0); ?>;
</script>
@include('projects.partials.successful-upload-multi')
@include('projects.partials.error-upload')
@include('projects.partials.upload-limitation')
<script id="cms-dev-alert" type="text/template">
    <div class="alert cms-dev-alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <span class="cms-dev-alert-content"></span>
    </div>
</script>
@include('popups.alert_popup')
@include('popups.delete_record_popup')
@include('popups.approve_popup')
@endsection