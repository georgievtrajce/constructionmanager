<div class="cm-filter">
    <div class="row">
        <div class="col-md-12">
                <div class="row">
                    <div class="col-sm-6">
                        <div id="chart_div_tasks_details" style="width: 100%; min-height: 100%;margin-top: -15px;"></div>
                    </div>
                    {!! Form::open(['method'=>'GET','url'=>'tasks']) !!}
                    <div class="col-sm-3">
                        <div class="form-group">
                            {!! Form::label('search_by', trans('labels.search_by').':') !!}
                            {!! Form::select('search_by', $filter, Input::get('search_by')) !!}
                        </div>
                        <div class="form-group" id="text" style="{{(Input::get('search_by') == "type_id")?"display:none;":""}}">
                            {!! Form::label('search', trans('labels.search').':') !!}
                            {!! Form::text('search_text',Input::get('search_text'),['class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            {!! Form::hidden('sort',Input::get('sort','id')) !!}
                            {!! Form::hidden('order',Input::get('order','desc')) !!}
                            {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                    <div class="col-sm-1">
                        @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || Auth::user()->hasRole('Project Admin') || $userCanDelete))
                            {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                            <input type="hidden" value="{{url('tasks')}}/" id="form-url" />
                            <a href="#" disabled id="delete-button" class='btn btn-xs btn-danger pull-right cm-btn-fixer delete-button-multi' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                {{trans('labels.delete')}}
                            </a>
                            {!! Form::close()!!}
                        @endif
                    </div>
                </div>
        </div>
    </div>
</div>
