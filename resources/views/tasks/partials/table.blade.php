@if(count($tasks) > 0)
<div class="table-responsive">
    <table class="table table-hover table-bordered cm-table-compact">
        <thead>
        <tr>
            @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || Auth::user()->hasRole('Project Admin') || $userCanDelete))
            <th class="text-center"></th>
            @endif
            <th>
                <?php
                if (Input::get('sort') == 'title' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=title&order=desc';
                } else {
                    $url = Request::url().'?sort=title&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.title')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=mf_number&order=desc';
                } else {
                    $url = Request::url().'?sort=mf_number&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.mf_number')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'projectName' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=projectName&order=desc';
                } else {
                    $url = Request::url().'?sort=projectName&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.project')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                {{trans('labels.tasks.assignees')}}
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'combinedType' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=combinedType&order=desc';
                } else {
                    $url = Request::url().'?sort=combinedType&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.type')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'created_at' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=created_at&order=desc';
                } else {
                    $url = Request::url().'?sort=created_at&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.start_date')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'due_date' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=due_date&order=desc';
                } else {
                    $url = Request::url().'?sort=due_date&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.due_date')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
                <?php
                if (Input::get('sort') == 'status' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=status&order=desc';
                } else {
                    $url = Request::url().'?sort=status&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.status')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th class="" width="25%">
                <?php
                if (Input::get('sort') == 'note' && Input::get('order') == 'asc') {
                    $url = Request::url().'?sort=note&order=desc';
                } else {
                    $url = Request::url().'?sort=note&order=asc';
                }
                $url .= !empty(Input::get('search_by'))?'&search_by='.Input::get('search_by'):'';
                $url .= !empty(Input::get('search_text'))?'&search_text='.Input::get('search_text'):'';
                $url .= !empty(Input::get('search_module'))?'&search_module='.Input::get('search_module'):'';
                $url .= !empty(Input::get('activeInnerTab'))?'&activeInnerTab='.Input::get('activeInnerTab'):(!empty($activeInnerTab)?'&activeInnerTab='.$activeInnerTab:'');
                ?>
                {{trans('labels.tasks.notes')}}
                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
            </th>
            <th>
            </th>
        </tr>
        </thead>
        <tbody>
        @foreach($tasks as $task)
            <tr>
                @if (!in_array($task->id, $subcontractorTaskIds))
                    @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin')) || $userCanDelete))
                    <td class="text-center">
                        <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$task->id}}">
                    </td>
                    @endif
                @else
                    <td class="text-center"></td>
                @endif
                <td>
                    @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin')) || in_array($task->project_id, $writeArray)) && !in_array($task->id, $subcontractorTaskIds))
                        <a href="{{URL('tasks/'.$task->id.'/edit')}}">{{$task->title}}</a>
                    @else
                        {{$task->title}}
                    @endif
                    <div class="small">by {{(!empty($task->addedByUser->name))?$task->addedByUser->name:''}} {{(!empty($task->addedByUser->name))?'- '.$task->addedByUser->company->name:''}}</div>
                </td>
                <td>
                    @if(!empty($task->mf_number) && !empty($task->mf_title))
                        {{$task->mf_number." - ".$task->mf_title}}
                    @endif
                </td>
                <td>
                    {{$task->projectName}}
                </td>
                <td>
                    @if(!empty($task->taskUsers))
                        @foreach($task->taskUsers as $taskUser)
                            @if(!empty($taskUser->users))
                                @foreach($taskUser->users as $user)
                                    {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}}<br />
                                @endforeach
                            @endif
                            @if(!empty($taskUser->abUsers))
                                @foreach($taskUser->abUsers as $user)
                                    {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}}<br />
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                </td>
                <td>
                    {{!empty($task->custom_type)?$task->custom_type:((isset(config('constants.tasks.type')[$task->type_id]))?config('constants.tasks.type')[$task->type_id]:'')}}
                </td>
                <td>
                    {{date("m/d/Y", strtotime($task->created_at))}}
                </td>
                <td>
                    {{date("m/d/Y", strtotime($task->due_date))}}
                </td>
                <td>
                    {{config('constants.tasks')['status'][$task->status]}}
                </td>
                <td>
                    {!! $task->note !!}
                </td>
                <td>
                    @if(count($task->subtasks) > 0)
                        <a class="btn btn-primary show-subtasks btn-xs glyphicon glyphicon-arrow-down" href="#dev-expand-tasks"></a>
                    @endif
                </td>
            </tr>
            @if(count($task->subtasks) > 0)

                <tr style="display: none;">
                    <td colspan="12" data-title="">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">Sub-tasks</h4>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered cm-table-compact">
                                        <thead>
                                        <tr>
                                            @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin')) || $userCanDelete))
                                                <th class="text-center"></th>
                                            @endif
                                            <th>
                                                {{trans('labels.tasks.title')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.mf_number')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.project')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.assignees')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.type')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.start_date')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.due_date')}}
                                            </th>
                                            <th>
                                                {{trans('labels.tasks.status')}}
                                            </th>
                                            <th class="" width="25%">
                                                {{trans('labels.tasks.notes')}}
                                            </th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        @foreach($task->subtasks as $subtask)
                                            <tr>
                                                @if (!in_array($task->id, $subcontractorTaskIds))
                                                    @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin')) || $userCanDelete))
                                                        <td class="text-center">
                                                            <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$subtask->id}}">
                                                        </td>
                                                    @endif
                                                @else
                                                    <td class="text-center"></td>
                                                @endif
                                                <td>
                                                    @if($page != 'my-tasks' && ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin')) || in_array($subtask->project_id, $writeArray)) && !in_array($task->id, $subcontractorTaskIds))
                                                        <a class="" href="{{URL('tasks/'.$subtask->id.'/edit')}}">{{$subtask->title}}</a>
                                                    @else
                                                        {{$subtask->title}}
                                                    @endif
                                                        <div class="small">by {{(!empty($task->addedByUser->name))?$task->addedByUser->name:''}} {{(!empty($task->addedByUser->name))?'- '.$task->addedByUser->company->name:''}}</div>
                                                </td>
                                                <td>
                                                    @if(!empty($subtask->mf_number) && !empty($subtask->mf_title))
                                                        {{$subtask->mf_number." - ".$subtask->mf_title}}
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$task->projectName}}
                                                </td>
                                                <td>
                                                    @if(!empty($subtask->taskUsers))
                                                        @foreach($subtask->taskUsers as $taskUser)
                                                            @if(!empty($taskUser->users))
                                                                @foreach($taskUser->users as $user)
                                                                    {{$user->name}}<br />
                                                                @endforeach
                                                            @endif
                                                            @if(!empty($taskUser->abUsers))
                                                                @foreach($taskUser->abUsers as $user)
                                                                    {{$user->name}}<br />
                                                                @endforeach
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </td>
                                                <td>
                                                    {{!empty($subtask->custom_type)?$subtask->custom_type:((isset(config('constants.tasks.type')[$subtask->type_id]))?config('constants.tasks.type')[$subtask->type_id]:'')}}
                                                </td>
                                                <td>
                                                    {{date("m/d/Y", strtotime($subtask->created_at))}}
                                                </td>
                                                <td>
                                                    {{date("m/d/Y", strtotime($subtask->due_date))}}
                                                </td>
                                                <td>
                                                    {{config('constants.tasks')['status'][$subtask->status]}}
                                                </td>
                                                <td>
                                                    {!! $subtask->note !!}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    </div>
@else
    <p class="text-center">{{trans('labels.no_records')}}</p>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6 pull-right">
            <div class="pull-right">
                {!! $tasks->render() !!}
            </div>
        </div>
    </div>
</div>