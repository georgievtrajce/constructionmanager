<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<table width="100%">
    <tr>
        <td style="width: 100%; border-bottom: 3px solid black; vertical-align: top;">
            <table width="100%">
                <tr>
                    <td style="width: 400px; vertical-align: bottom;">
                        <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                        @if(count(Auth::user()->company->addresses))
                            {{Auth::user()->company->addresses[0]->street}}<br>
                            {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                        @endif
                    </td>
                    <td style="width: 450px; vertical-align: bottom;">
                        {{Auth::user()->email}}<br>
                        {{Auth::user()->office_phone}}
                    </td>
                    <td style="float: right; vertical-align: bottom;">
                        @if ($page == 'my-tasks')
                            <h2 style="float: left; margin: 0px; text-align: right;">{{'My tasks'}}</h2>
                        @elseif ($page == 'project-tasks')
                            <h2 style="float: left; margin: 0px; text-align: right;">{{'Project tasks'}}</h2>
                        @else
                            <h2 style="float: left; margin: 0px; text-align: right;">{{'All tasks'}}</h2>
                        @endif

                        <p style="float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<div class="row">

    <h3>
        @if(!empty($filterBy))
            {{trans('labels.tasks.tasks_filtered_by')}}
        @else
            &nbsp;
        @endif
    </h3>

    <h4>
        @if(!empty($filterBy))
            <?php $filterArr = ['tasks.title' => trans('labels.tasks.title'), 'projects.name' => trans('labels.tasks.project'), 'type' => trans('labels.tasks.type'), 'assignee' => trans('labels.tasks.assignee')] ?>
            {{$filterArr[$filterBy]}}<br/><br/>
        @endif
        @if(!empty($filterText))    
            {{$filterText}}
        @endif
    </h4>
</div>
<div class="row" style="font-size: 11px;">
    <style>
        .bids-padding {
            padding: 5px 5px !important;
            text-align: left !important;
        }

        h4 {
            font-size: 14px;
            padding: 0px 5px 5px 5px;
            margin: 0px;
        }
        hr{
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid #000;
            margin: 0;
            margin-top: -1px;
            padding: 0; 
        }
    </style>
    @if(!empty($tasks) && count($tasks) > 0)

        <table style="width: 100%; padding: 0px; margin: 0px;border-collapse:collapse; table-layout: fixed; border-bottom: 1px solid #000;">
            <thead style=" text-transform: uppercase; vertical-align: top;  border-bottom: 2px solid #000;">
            <tr>
                <th class="bids-padding">
                    {{trans('labels.tasks.title')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.mf_number')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.project')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.assignees')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.type')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.start_date')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.due_date')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.status')}}
                </th>
                <th class="bids-padding">
                    {{trans('labels.tasks.notes')}}
                </th>
            </tr>
            </thead>
   
            <tbody>
            @foreach ($tasks as $task)
                <tr @if(count($task->subtasks)>0) @endif>
                    <td colspan="12"><hr></td>
                </tr>
                <tr>
                    <td class="bids-padding bb" style="width: 10%">
                        <span style="font-weight: bold;">{{$task->title}}</span>
                        <div style="font-size: 70%;">by {{(!empty($task->addedByUser->name))?$task->addedByUser->name:''}} {{(!empty($task->addedByUser->name))?'- '.$task->addedByUser->company->name:''}}</div>
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        @if(!empty($task->mf_number) && !empty($task->mf_title))
                            {{$task->mf_number." - ".$task->mf_title}}
                        @endif
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        {{$task->projectName}}
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        @if(!empty($task->taskUsers))
                            @foreach($task->taskUsers as $taskUser)
                                @if(!empty($taskUser->users))
                                    @foreach($taskUser->users as $user)
                                        {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}}<br />
                                    @endforeach
                                @endif
                                @if(!empty($taskUser->abUsers))
                                    @foreach($taskUser->abUsers as $user)
                                        {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}}<br />
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        {{!empty($task->custom_type)?$task->custom_type:((isset(config('constants.tasks.type')[$task->type_id]))?config('constants.tasks.type')[$task->type_id]:'')}}
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        {{date("m/d/Y", strtotime($task->created_at))}}
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        {{date("m/d/Y", strtotime($task->due_date))}}
                    </td>
                    <td class="bids-padding bb" style="width: 10%">
                        {{config('constants.tasks')['status'][$task->status]}}
                    </td>
              
                    <td class="bids-padding bb" style="width: 20%">
                        {!! $task->note !!}
                    </td>
                </tr>
                @if(count($task->subtasks) > 0)
                    <tr >
                        <td colspan="12" class="bids-padding">
                        <div style="border: 1px solid grey;">
                            <h4>Sub-tasks</h4>
                            <table style="width: 100%;  table-layout: fixed;">
                                <thead style="vertical-align: top">
                                <tr>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.title')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.mf_number')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.project')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.assignees')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.type')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.start_date')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.due_date')}}
                                    </th>
                                    <th class="bids-padding"  style="width: 10%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.status')}}
                                    </th>
                                    <th class="bids-padding" style="width: 20%; border-bottom: 1px solid grey;">
                                        {{trans('labels.tasks.notes')}}
                                    </th>
                                </tr>
                                </thead>
                               
</div>
                              
                                <tbody style=" height: 30px">
                                @foreach($task->subtasks as $subtask)
                                    <tr>
                                        <td class="bids-padding">
                                            <span>{{$subtask->title}}</span>
                                            <div style="font-size: 70%;">by {{(!empty($task->addedByUser->name))?$task->addedByUser->name:''}} {{(!empty($task->addedByUser->name))?'- '.$task->addedByUser->company->name:''}}</div>
                                        </td>
                                        <td class="bids-padding">
                                            @if(!empty($subtask->mf_number) && !empty($subtask->mf_title))
                                                {{$subtask->mf_number." - ".$subtask->mf_title}}
                                            @endif
                                        </td>
                                        <td class="bids-padding">
                                            {{$task->projectName}}
                                        </td>
                                        <td class="bids-padding">
                                            @if(!empty($subtask->taskUsers))
                                                @foreach($subtask->taskUsers as $taskUser)
                                                    @if(!empty($taskUser->users))
                                                        @foreach($taskUser->users as $user)
                                                            {{$user->name}}<br />
                                                        @endforeach
                                                    @endif
                                                    @if(!empty($taskUser->abUsers))
                                                        @foreach($taskUser->abUsers as $user)
                                                            {{$user->name}}<br />
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        </td>
                                        <td class="bids-padding">
                                            {{!empty($subtask->custom_type)?$subtask->custom_type:((isset(config('constants.tasks.type')[$subtask->type_id]))?config('constants.tasks.type')[$subtask->type_id]:'')}}
                                        </td>
                                        <td class="bids-padding">
                                            {{date("m/d/Y", strtotime($subtask->created_at))}}
                                        </td>
                                        <td class="bids-padding">
                                            {{date("m/d/Y", strtotime($subtask->due_date))}}
                                        </td>
                                        <td class="bids-padding">
                                            {{config('constants.tasks')['status'][$subtask->status]}}
                                        </td>
                                        <td class="bids-padding">
                                            {!! $subtask->note !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        
                    </tr>
                @endif
                <tr class="cm-row-separator">
                    <td colspan="11"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <p class="text-center">{{trans('labels.no_records')}}</p>
    @endif
</div>
</body>
</html>