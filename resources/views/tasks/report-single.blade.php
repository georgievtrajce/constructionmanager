<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Cloud PM</title>
</head>
<body>
<div class="cm-page-content">
    <div class="container-fluid container-inset">
        <div class="panel">
            <div class="panel-body">
                <div class="row">
                    <table width="100%">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <table width="690px">
                                    <tr>
                                        <td style="width: 260px; vertical-align: bottom;">
                                            <h2 style="margin: 0px;">{{Auth::user()->company->name}}</h2>
                                            @if(count(Auth::user()->company->addresses))
                                                {{Auth::user()->company->addresses[0]->street}}<br>
                                                {{Auth::user()->company->addresses[0]->city.', '.Auth::user()->company->addresses[0]->state.' '.Auth::user()->company->addresses[0]->zip}}
                                            @endif
                                        </td>
                                        <td style="width: 250px; margin-left: 40px; vertical-align: bottom;">
                                            {{Auth::user()->email}}<br>
                                            {{Auth::user()->office_phone}}
                                        </td>
                                        <td style="float: right; width: 180px; vertical-align: bottom;">
                                            <h2 style="float: left; margin: 0px; text-align: right;">{{"Task View"}}</h2>
                                            <p style=" float: left; margin: 0px; text-align: right;">{{'Date: '.date("m/d/Y", strtotime(Carbon::now()))}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="row">
                    <table width="100%" class="table table-hover table-striped cm-table-compact">
                        <tr>
                            <td style="width: 690px; border-bottom: 3px solid black;">
                                <h3>
                                    {{$task->title}}
                                </h3>
                            </td>
                        </tr>
                    </table>
                </div>
                <style>
                    .ab-padding {
                        padding: 2px 2px !important;
                        text-align: left !important;
                    }
                </style>
                <div class="row">
                    <table width="100%" style="border-bottom: 3px solid black;">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.assigned_by')}}</td>
                            <td class="ab-padding">
                                {{(!empty($task->addedByUser->name))?$task->addedByUser->name:''}} {{(!empty($task->addedByUser->name))?'- '.$task->addedByUser->company->name:''}}
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.mf_number')}}</td>
                            <td class="ab-padding">
                                @if(!empty($task->mf_number) && !empty($task->mf_title))
                                    {{$task->mf_number." - ".$task->mf_title}}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.project')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{$task->project->name or ''}}
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.assignees')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                @if(!empty($task->taskUsers))
                                    @foreach($task->taskUsers as $taskUser)
                                        @if(!empty($taskUser->users))
                                            @foreach($taskUser->users as $user)
                                                {{$user->name}}{{!empty($user->company)?' - '.$user->company->name:''}}<br />
                                            @endforeach
                                        @endif
                                        @if(!empty($taskUser->abUsers))
                                            @foreach($taskUser->abUsers as $user)
                                                {{$user->name}}{{!empty($user->addressBook)?' - '.$user->addressBook->name:''}}<br />
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.type')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{!empty($task->custom_type)?$task->custom_type:((isset(config('constants.tasks.type')[$task->type_id]))?config('constants.tasks.type')[$task->type_id]:'')}}
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.item')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{!empty($item)?$item->name:''}}
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.start_date')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">{{date("m/d/Y", strtotime($task->created_at))}}</td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.due_date')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{date("m/d/Y", strtotime($task->due_date))}}
                            </td>
                        </tr>
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;">{{trans('labels.tasks.status')}}</td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {{config('constants.tasks')['status'][$task->status]}}
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td class="ab-padding" style="width: 230px; font-weight: bold; vertical-align: top;"><p>{{trans('labels.tasks.notes')}}</p></td>
                            <td class="ab-padding" style="vertical-align: top;">
                                {!! $task->note !!}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>