@extends('layouts.master') @section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-5">
            <h1 class="cm-heading">
                {{trans('labels.tasks.my-tasks')}}
                <small class="cm-heading-sub">{{trans('labels.total_records', ['number' => count($tasks)])}}</small>
            </h1>
        </div>
        <div class="col-md-7">
            <div class="cm-pull-right">
                <div class="cm-btn-group cf">
                    <a target="_blank" href="{{URL('tasks').'/report/mine'.'?'.$_SERVER['QUERY_STRING']}}" class="btn btn-success pull-right">{{trans('labels.address_book.save_to_pdf')}}</a>
                </div>
            </div>
        </div>
    </div>

    @include("tasks.partials.filter")

    <div class="panel">
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            @include("tasks.partials.table")
        </div>
    </div>
</div>
@include('popups.delete_record_popup')
@endsection