@extends('layouts.tabs')
@section('tabsContent')
<div class="panel">
    <div class="panel-body">
        @if (sizeof($logs))
        <div class="row">
            <div class="col-sm-5">
                {!! Form::open(['method'=>'GET','url'=>'logs']) !!}
                <div class="col-sm-12">
                    <h3>{{trans('labels.sort')}}:</h3>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <div class="form-group">
                            {!! Form::label('sort', trans('labels.sort_by').':') !!} <br />
                            {!! Form::select('sort', array('id' => 'ID', 'user_name' => trans('labels.manage_users.user'), 'company_name' => trans('labels.company.name'), 'file_type' => trans('labels.files.file_type'), 'file_name' => trans('labels.file.name'), 'updated_at' => trans('labels.files.download_date')), Input::get('sort')) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::label('order', trans('labels.order_by').':') !!} <br />
                        {!! Form::select('order', array('asc' => trans('labels.ascending'), 'desc' => trans('labels.descending')), Input::get('order')) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="pull-right">
            <?php echo $logs->appends([
                'id'=>Input::get('id'),
                'user_name'=>Input::get('user_name'),
                'company_name'=>Input::get('company_name'),
                'file_type'=>Input::get('file_type'),
                'file_name'=>Input::get('file_name'),
                'updated_at'=>Input::get('updated_at'),
                'sort'=>Input::get('sort'),
                'order'=>Input::get('order'),
                ])->render(); ?>
            </div>
            @include('list_companies_users.table', $logs)
        </div>
        @endif
    </div>
    @endsection
