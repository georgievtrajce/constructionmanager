@extends('layouts.master')

@section('content')
    <div class="cms-page">
        <div class="container container-inset">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="cm-heading">
                      adsad  {{trans('labels.super_admin_companies.create')}}
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" role="form" method="POST" action="{{URL('/list-companies/create-free-account')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <fieldset>
                            <legend>Sign In Info</legend>

                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.login.email')}}</label>
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Company Info</legend>
                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.company.name')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('labels.company_profile.company_logo')}}</label>
                                <div class="col-md-6">
                                    <input type="file" name="company_logo">
                                </div>
                            </div>

                        </fieldset>
                        <div class="addresses-main-cont">
                            <fieldset class="address-container">
                                <legend>{{trans('labels.register.company_address')}}</legend>
                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_name')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="company_addr_office_title_req" value="{{ old('company_addr_office_title_req') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.Address')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="company_addr_street_req" value="{{ old('company_addr_street_req') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.city')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="company_addr_town_req" value="{{ old('company_addr_town_req') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.state')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="company_addr_state_req" value="{{ old('company_addr_state_req') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.zip')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="company_addr_zip_code_req" value="{{ old('company_addr_zip_code_req') }}">
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        <fieldset>
                            <legend>{{trans('labels.register.contact_person')}}</legend>
                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.name')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="admin_name" value="{{ old('admin_name') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.title')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="admin_title" value="{{ old('admin_title') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_phone')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="admin_office_phone" value="{{ old('admin_office_phone') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('labels.address_book.cell_phone')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="admin_cell_phone" value="{{ old('admin_cell_phone') }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">{{trans('labels.address_book.fax')}}</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="admin_fax" value="{{ old('admin_fax') }}">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>
                                {{trans('labels.company_profile.subscription_type')}}
                            </legend>
                            <style>
                                .radio label span.check {
                                    left: 10px!important;
                                }
                            </style>
                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-4" id="inline_content">
                                    <h4>Choose Subscription Level:</h4>
                                    <div class="cm-spacer-xs"></div>
                                    <div class="radio cm-radio-inline">
                                        <label>
                                            <input type="radio" id="subs1" name="subscription_type" value="1" {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-one') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 1 ? 'checked' : '' }} />
                                            Level 1
                                        </label>
                                    </div>
                                    <div class="radio cm-radio-inline">
                                        <label>
                                            <input type="radio" id="subs2" name="subscription_type" value="2" {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-two') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 2 ? 'checked' : '' }} />
                                            Level 2
                                        </label>
                                    </div>
                                    <div class="radio cm-radio-inline">
                                        <label>
                                            <input type="radio" id="subs3" name="subscription_type" value="3" {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.level-three') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 3 ? 'checked' : '' }} />
                                            Level 3
                                        </label>
                                    </div>
                                    <div class="radio cm-radio-inline">
                                        <label>
                                            <input type="radio" id="salesAgent" name="subscription_type" value="4" {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.sales-agent') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 4 ? 'checked' : '' }} />
                                            Sales Agent
                                        </label>
                                    </div>
                                    <div class="radio cm-radio-inline">
                                        <label>
                                            <input type="radio" id="testAccount" name="subscription_type" value="6" {{ isset($subscriptionLevel) && $subscriptionLevel == Config::get('subscription_levels.test-account') ? 'checked' : '' }} {{ old('subscription_type') && old('subscription_type') == 6 ? 'checked' : '' }} />
                                            Test Account
                                        </label>
                                    </div>
                                    <div class="cm-spacer-xs"></div>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend>
                                {{trans('labels.super_admin_companies.expiration_date')}}
                            </legend>
                            <div class="form-group">
                                <label class="cm-control-required col-md-4 control-label">{{trans('labels.super_admin_companies.select_date')}}</label>
                                <div class="col-md-6">
                                    <input type="text" id="expiration-date" name="expiration_date" value="{{old('expiration_date')}}">
                                </div>
                            </div>
                        </fieldset>

                        <div class="cm-spacer-xs"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-success pull-right">
                                    {{trans('labels.create')}}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>

        </div>
    </div>
@endsection
