@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-6">
            <h3 class="cm-heading">{{ $company->name }}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel cf">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="list-group">
                                <div class="cm-form-group-alt">
                                    <span><b>Logo: </b></span>
                                    <span>
                                        @if($company->logo)
                                        <img width="200" class="img-thumbnail" src="{{URL('images/uploads/logos/'.$company->logo)}}" alt="">
                                        @else
                                        {{trans('labels.none')}}
                                        @endif
                                    </span>
                                </div>
                                <div class="cm-form-group-alt">
                                    <span><b>{{trans('labels.list_users.referral_points').': '}}</b></span>
                                    <span>@if ($company->ref_points) {{ $company->ref_points }} @endif</span>
                                </div>
                                <div class="cm-form-group-alt">
                                    <span><b>{{trans('labels.company_profile.subscription_type').': '}}</b></span>
                                    <span>@if ($company->subscription_type->name){{ $company->subscription_type->name }} @endif</span>
                                </div>
                                <div class="cm-form-group-alt">
                                    <span><b>{{trans('labels.list_users.company_registration_date').': '}}</b></span>
                                    <span>@if ($created_at){{ $created_at }} @endif</span>
                                </div>
                                <div class="cm-form-group-alt">
                                    <span><b>{{trans('labels.company_profile.expiry_date').': '}}</b></span>
                                    <span>@if ($company->subscription_expire_date){{ $company->subscription_expire_date }} @endif</span>
                                </div>
                                <div class="cm-form-group-alt">
                                    <span><b>{{trans('labels.company_profile.payment_date').': '}}</b></span>
                                    <span>@if ($company->subscription_payment_date) {{ $company->subscription_payment_date }} @endif</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-default cf">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{trans('labels.company_profile.company_addresses')}}</h3>
                                </div>
                                <div class="panel-body">
                                    @if(count($company->addresses) != 0)
                                    <div class="table-responsive">
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>{{trans('labels.address.office_title')}}</th>
                                                    <th>{{trans('labels.address.number')}}</th>
                                                    <th>{{trans('labels.address.street')}}</th>
                                                    <th>{{trans('labels.address.city')}}</th>
                                                    <th>{{trans('labels.address.state')}}</th>
                                                    <th>{{trans('labels.address.zip')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($company->addresses as $address)
                                                <tr>
                                                    <td>@if (isset($address->office_title)) {{$address->office_title}} @endif</td>
                                                    <td>@if (isset($address->number)){{$address->number}} @endif</td>
                                                    <td>@if (isset($address->street)){{$address->street}} @endif</td>
                                                    <td>@if (isset($address->city)){{$address->city}} @endif</td>
                                                    <td>@if (isset($address->state)){{$address->state}} @endif</td>
                                                    <td>@if (isset($address->zip)){{$address->zip}} @endif</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                    <p>{{trans('labels.list_users.no_address')}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
