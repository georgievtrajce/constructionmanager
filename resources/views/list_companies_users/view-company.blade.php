@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-6">
            <h3 class="cm-heading">{{ $company->name }}</h3>
        </div>
        <div class="col-md-6">
            @if (count($company->addresses()))
            <a href="{{URL('list-companies/'.$company->id.'/view-addresses')}}" class="btn btn-info pull-right">{{trans('labels.address.plural')}}</a>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel cf">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="cm-form-group-alt">
                                <span><b>Logo: </b></span>
                                <span>
                                    @if($company->logo)
                                    <img width="200" class="img-thumbnail" src="{{URL('images/uploads/logos/'.$company->logo)}}" alt="">
                                    @else
                                    {{trans('labels.none')}}
                                    @endif
                                </span>
                            </div>
                            <div class="cm-form-group-alt">
                                <span><b>{{trans('labels.list_users.referral_points').': '}}</b></span>
                                <span>@if ($company->ref_points) {{ $company->ref_points }} @endif</span>
                            </div>
                            <div class="cm-form-group-alt">
                                <span><b>{{trans('labels.company_profile.subscription_type').': '}}</b></span>
                                <span>@if ($company->subscription_type->name){{ $company->subscription_type->name }} @endif</span>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="cm-form-group-alt">
                                <span><b>{{trans('labels.list_users.company_registration_date').': '}}</b></span>
                                <span>@if ($created_at){{ $created_at }} @endif</span>
                            </div>
                            <div class="cm-form-group-alt">
                                <span><b>{{trans('labels.company_profile.expiry_date').': '}}</b></span>
                                <span>@if ($company->subscription_expire_date){{ $company->subscription_expire_date }} @endif</span>
                            </div>
                            <div class="cm-form-group-alt">
                                <span><b>{{trans('labels.company_profile.payment_date').': '}}</b></span>
                                <span>@if ($company->subscription_payment_date) {{ $company->subscription_payment_date }} @endif</span>
                            </div>
                        </div>
                    </div>

                    <div class="cm-spacer-normal"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default cf">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        {{trans('labels.manage_users.company_users')}}
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            @if(count($company->users) != 0)
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th>{{trans('labels.name')}}</th>
                                                            <th>{{trans('labels.title')}}</th>
                                                            <th>{{trans('labels.role')}}</th>
                                                            <th>{{trans('labels.email')}}</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($company->users as $item)
                                                        <tr>
                                                            <td>@if (isset($item->id)){{$item->id}} @endif</td>
                                                            <td>@if (isset($item->name)){{$item->name}} @endif</td>
                                                            <td>@if (isset($item->title)){{$item->title}} @endif</td>
                                                            <td>
                                                                @foreach($item->roles as $role)
                                                                {{$role->name}}
                                                                @endforeach
                                                            </td>
                                                            <td>@if (isset($item->email)){{$item->email}} @endif</td>
                                                            <td><a href="{{URL('/list-companies/'.$company->id.'/'.$item->id)}}" class="btn btn-sm btn-info">{{trans('labels.view')}}</a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            @else
                                            <p>{{trans('labels.list_users.no_users')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
