@extends('layouts.master')

@section('content')
<div class="container-fluid container-inset">

    <div class="row">
        <div class="col-md-12">
            <h3 class="cm-heading"> {{ $user->name }}</h3>
        </div>
    </div>


    <div class="panel cf">
        <div class="panel-body">

            <div class="list-group">
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.Company').': '}}</b></span>
                    <span> @if($user->company->name) {{ $user->company->name }} @endif</span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.email').': '}}</b></span>
                    <span> @if($user->email) {{ $user->email }} @endif</span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.title').': '}}</b></span>
                    <span> @if($user->title) {{ $user->title }}  @endif</span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.role').': '}}</b></span>
                    <span>
                        @foreach($user->roles as $role)
                        {{ $role->name }}
                        @endforeach
                    </span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.Address').': '}}</b></span>
                    <span>
                        @if(count($user->address) > 0)
                        @if($user->address->office_title)
                        {{ $user->address->office_title }}
                        @else
                        {{ $user->address->city.', '.$user->address->state }}
                        @endif
                        @else
                        {{trans('labels.list_users.no_address')}}
                        @endif
                    </span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.address_book.office_phone').': '}}</b></span>
                    <span>@if($user->office_phone) {{ $user->office_phone }} @endif</span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.address_book.cell_phone').': '}}</b></span>
                    <span>
                        @if($user->cell_phone)
                        {{ $user->cell_phone }}
                        @else
                        {{trans('labels.list_users.no_cell_phone')}}
                        @endif
                    </span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.address_book.fax').': '}}</b></span>
                    <span>
                        @if($user->fax)
                        {{ $user->fax }}
                        @else
                        {{trans('labels.list_users.no_fax')}}
                        @endif
                    </span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.list_users.confirmed_user').': '}}</b></span>
                    <span>
                        @if($user->confirmed)
                        {{trans('labels.yes')}}
                        @else
                        {{trans('labels.no')}}
                        @endif
                    </span>
                </div>
                <div class="cm-form-group-alt">
                    <span><b>{{trans('labels.created_at').': '}}</b></span>
                    <span>{{ $created_at }}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
