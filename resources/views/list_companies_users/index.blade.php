@extends('layouts.master')

@section('content')
<div class="container-fluid container-inset">

    <div class="row">
        <div class="col-sm-12">
            <h3 class="cm-heading">
                {{trans('labels.super_admin_companies.registered_companies')}}
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 cf">
            <a href="{{URL('list-companies/create-free-account')}}" class="btn btn-success pull-left">{{trans('labels.super_admin_companies.create')}}</a>
        </div>
        <div class="col-md-4 cf">
        <p class="cm-pull-right">{{trans('labels.total_records', ['number' => $registeredCompanies->total()])}}</p>
        </div>

    </div>
    <div class="row">
     <div class="col-md-12">
        <div class="cm-filter cf">
            <div class="row">
                <div class="col-md-4">
                    {!! Form::open(['method'=>'GET','url'=>'list-companies']) !!}
                     <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                {!! Form::label('sort', trans('labels.sort_by')) !!}
                                {!! Form::select('sort',['id'=>'','subscription_expire_date'=>trans('labels.company_profile.expiry_date')],Input::get('sort')) !!}
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                {!! Form::label('order', trans('labels.order_by')) !!}
                                {!! Form::select('order', array('asc'=>trans('labels.ascending'),'desc'=>trans('labels.descending')),Input::get('order')) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                {!! Form::submit(trans('labels.sort'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                            </div>
                        </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-7">
                    {!! Form::open(['method'=>'GET','url'=>'list-companies/filter']) !!}
                     <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.status')}}</label>
                                {!! Form::select('status',[''=>'','active'=>trans('labels.super_admin_companies.active'),'expired'=>trans('labels.super_admin_companies.expired')],Input::get('status')) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.super_admin_companies.account_type')}}</label>
                                {!! Form::select('account_type',[''=>'','free'=>trans('labels.super_admin_companies.free'),'payed'=>trans('labels.super_admin_companies.payed')],Input::get('account_type')) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">{{trans('labels.subscription_level')}}</label>
                                {!! Form::select('subscription_level',$subscriptionsArr,Input::get('subscription_level')) !!}
                                {!! Form::hidden('sort',Input::get('sort','id')) !!}
                                {!! Form::hidden('order',Input::get('order','desc')) !!}
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                            </div>
                        </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-md-1">
                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                    <div class="form-group">
                        <input type="hidden" value="{{URL('list-companies/delete').'/'}}" id="form-url" />
                        <button disabled id="delete-button" class='btn btn-danger mr5 cm-btn-fixer' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                            {{trans('labels.delete')}}
                        </button>
                    </div>
                    {!! Form::close()!!}
                </div>
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
                @if($registeredCompanies->total() != 0)
          
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="select_all" id="companies_select_all"></th>
                                        <th>{{trans('labels.name')}}</th>
                                        <th>{{trans('labels.status')}}</th>
                                        <th>{{trans('labels.super_admin_companies.account_type')}}</th>
                                        <th>{{trans('labels.subscription_level')}}</th>
                                        <th>{{trans('labels.super_admin_companies.registration_date')}}</th>
                                        <th>{{trans('labels.super_admin_companies.expiry_date')}}</th>
                                        <th></th>
                                        <th>{{trans('labels.referral_points')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($registeredCompanies as $item)
                                    <tr>
                                        <td>
                                            <input type="checkbox" name="comp_id[]" class="company multiple-items-checkbox" value="{{$item->id}}" data-id="{{$item->id}}">
                                        </td>
                                        <td><a href="{{URL('list-companies/'.$item->id)}}">{{$item->name}}</a></td>
                                        <td>
                                            @if($item->subscription_expire_date > Carbon::now()->toDateString())
                                                {{trans('labels.super_admin_companies.active')}}
                                            @else
                                                {{trans('labels.super_admin_companies.expired')}}
                                            @endif
                                        </td>
                                        <td>
                                            @if(empty($item->subscription_payment_date) || empty($item->subscription_expire_date))
                                                {{trans('labels.super_admin_companies.free')}}
                                            @else
                                                {{trans('labels.super_admin_companies.payed')}}
                                            @endif
                                        </td>
                                        <td>
                                            <select id="subscriptionType" name="subscriptionType" data-company-id="{{$item->id}}" data-oldvalue="{{$item->subscription_type->id}}" class="subscriptionTypeSelect">
                                                @foreach($subscriptionsArr as $key => $subscriptionLevelName)
                                                    <option @if($item->subscription_type->id == $key) selected @endif value="{{$key}}">{{$subscriptionLevelName}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            @if($item->subscription_payment_date != 0)
                                                {{date("m/d/Y", strtotime($item->subscription_payment_date))}}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="expire_date_data">
                                                @if($item->subscription_expire_date != 0)
                                                    {{date("m/d/Y", strtotime($item->subscription_expire_date))}}
                                                @endif
                                            </div>
                                            <div class="expire_date_input" style="display: none;">
                                                <input style="float: left; width: 80%" type="text" class="expiry_date" name="expiration_date" @if($item->subscription_expire_date != 0){{'value='.date("m/d/Y", strtotime($item->subscription_expire_date))}}@else{{'value=""'}}@endif>
                                                <a style="float: left; margin-left: 5px; padding: 0px 5px; color: #FFFFFF; background-color: #b23724; border-radius: 2px;" class="cancel_expire_change" href="javascript:;">
                                                    {{'X'}}
                                                </a>
                                            </div>
                                        </td>
                                        <td>
                                            <a href="javascript:;" style="padding: 5px 10px!important;" data-company-id="{{$item->id}}" class="btn btn-sm cm-btn-secondary edit_expire_date"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a class="btn btn-sm btn-success save_expire_date" style="padding: 5px 10px!important; display: none;" href="javascript:;"><span class="glyphicon glyphicon-saved"></span></a>
                                        </td>
                                        <td>
                                            <span style="float: left;margin-right: 10px; line-height: 28px; vertical-align: middle;">{{$item->ref_points}}</span>
                                            @if($item->subs_id == Config::get('subscription_levels.sales-agent'))
                                                {!! Form::open(['method'=>'POST', 'style'=>'float:left', 'url'=>URL('list-companies/'.$item->id.'/reset-points')]) !!}
                                                <button class="btn btn-sm btn-danger" style="padding: 5px 10px;" type="submit" onclick="return confirm('{{trans('labels.super_admin_companies.reset_points_message')}}')"><span class="glyphicon glyphicon-refresh"></span></button>
                                                {!! Form::close()!!}
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <?php echo $registeredCompanies->appends([
                                    'status'=>Input::get('status'),
                                    'account_type'=>Input::get('account_type'),
                                    'subscription_level'=>Input::get('subscription_level'),
                                    'sort'=>Input::get('sort'),
                                    'order'=>Input::get('order')
                                ])->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <p>{{trans('labels.no_records')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
</div>
@include('popups.delete_record_popup')
@include('popups.alert_popup')
@endsection
