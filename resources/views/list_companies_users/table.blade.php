<table class="table table-hover">
    @if (sizeof($logs))
    <thead>
        <tr>
            <th>{{trans('labels.manage_users.user')}}</th>
            <th>{{trans('labels.file.name')}}</th>
            <th>{{trans('labels.files.file_type')}}</th>
            <th>{{trans('labels.company.name')}}</th>
            <th>{{trans('labels.files.download_date')}}</th>
        </tr>
    </thead>
    <tbody>
        @for ($i=0; $i<sizeof($logs); $i++)
        <tr>
            <td>@if (isset($logs[$i]->user_name)){{$logs[$i]->user_name}} @endif</td>
            <td>@if (isset($logs[$i]->file_name)){{$logs[$i]->file_name}} @endif</td>
            <td>@if (isset($logs[$i]->file_type)){{$logs[$i]->file_type}} @endif</td>
            <td>@if (isset($logs[$i]->company_name)){{$logs[$i]->company_name}} @endif</td>
            <td>@if (isset($logs[$i]->updated_at)){{$logs[$i]->updated_at}} @endif</td>
        </tr>
        @endfor
    </tbody>
    @endif
</table>
