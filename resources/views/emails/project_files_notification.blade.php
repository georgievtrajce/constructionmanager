@extends('layouts.email_master')

@section('content')
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #F1F4F5">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" class="block-grid " style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: #FFFFFF">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #FFFFFF;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="500" style="width:500px;"><![endif]-->
                                        <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 5px;padding-right: 0px;padding-bottom: 5px;padding-left: 0px;border-top: 0px solid transparent;border-right: 3px solid #444444;border-bottom: 0px solid transparent;border-left: 3px solid #444444">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="line-height:14px;font-size:12px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">From:</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><strong><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->name}}</span></strong><br></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->title}}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{ Auth::user()->company->name }}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->email}}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->office_phone}}</span></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">Dear <strong>{{ $email['name'] }}</strong>,</p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">&nbsp;<br></p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">We would like to notify you that a new <strong>Project File</strong> was uploaded for the project <strong>{{ $projectName }}</strong><br></p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">&nbsp;<br></p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">You can download the file by clicking the following link:<br />
                                                                                <strong>
                                                                                    @if (!empty($email['comp_child_id']))
                                                                                        <a style="color:#336699;text-decoration: underline;" href="{{url('projects/'.$projectId.'/get-project-file?fileId='.$fileId.'&filePath='.$filePath.'&companyId='.$email['comp_child_id'])}}" target="_blank" rel="noopener noreferrer">
                                                                                            {{$fileName}}
                                                                                        </a>
                                                                                    @else
                                                                                        <a style="color:#336699;text-decoration: underline;" href="{{url('projects/'.$projectId.'/get-project-file-public?fileId='.$fileId.'&filePath='.urlencode($filePath))}}" target="_blank" rel="noopener noreferrer">
                                                                                            {{$fileName}}
                                                                                        </a>
                                                                                    @endif
                                                                                </strong>
                                                                            </p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">&nbsp;<br></p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">
                                                                                Sent to:<br /><br />
                                                                                @foreach($sentTo as $sentItem)
                                                                                    {{$sentItem}} <br/>
                                                                                @endforeach
                                                                            </p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">&nbsp;<br></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table align="center" width="100%" border="0" cellpadding="0" cellspacing="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td align="center" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 30px;padding-right: 30px;padding-bottom: 30px;padding-left: 30px">
                                                                    <div style="height: 0px;">
                                                                        <table align="center" border="0" cellspacing="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;border-top: 0px solid transparent;width: 100%">
                                                                            <tbody>
                                                                            <tr style="vertical-align: top">
                                                                                <td align="center" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top"></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
@endsection
@section('footer-extra')
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #F1F4F5">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#f1f4f5" class="block-grid " style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: #f1f4f5">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #f1f4f5;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#f1f4f5" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="500" style="width:500px;"><![endif]-->
                                        <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 20px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;">
                                                                    <div style="color: #a2a2a2;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color: #848484;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">This Email was sent by <strong>{{Auth::user()->name}}</strong> from <strong>{{Auth::user()->company->name}}</strong>.
                                                                                If you don't want to receive emails from this Company please contact
                                                                                <strong>{{Auth::user()->name}}</strong> at <strong>{{Auth::user()->email}}</strong></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
@endsection