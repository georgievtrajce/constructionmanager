@extends('layouts.email_master')

@section('content')
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #f1f4f5;">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" class="block-grid " style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: #FFFFFF">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #FFFFFF;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="500" style="width:500px;"><![endif]-->
                                        <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 5px;padding-right: 0px;padding-bottom: 5px;padding-left: 0px;border-top: 0px solid transparent;border-right: 3px solid #444444;border-bottom: 0px solid transparent;border-left: 3px solid #444444">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:16px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 14px;line-height: 17px"><span style="font-size: 12px; line-height: 16px;">From:</span><br>
                                                                                <span
                                                                                        style="font-size: 12px; line-height: 16px;">Cloud PM</span><br><span style="font-size: 12px; line-height: 16px;"><a style="color:#336699;text-decoration: underline;" title="support@cloud-pm.com" href="mailto:support@cloud-pm.com">support@cloud-pm.com</a></span><br></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:16px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 16px"><span style="font-size: 12px; line-height: 16px;">You have successfully registered to the Cloud PM web application.</span></p>
                                                                            <p
                                                                                    style="margin: 0;font-size: 12px;line-height: 16px"><span style="font-size: 12px; line-height: 16px;">Please confirm your account by clicking on the button bellow.</span>&nbsp;<br></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td class="button-container" align="center" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                                        <tbody>
                                                                        <tr style="vertical-align: top">
                                                                            <td width="100%" class="button" align="center" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                                <!--[if mso]>
                                                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:34px;   v-text-anchor:middle; width:122px;" arcsize="12%"   strokecolor="#336699"   fillcolor="#336699" >
                                                                                    <w:anchorlock/>
                                                                                    <center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:14px;">
                                                                                <![endif]-->
                                                                                <!--[if !mso]><!-- -->
                                                                                <div align="center" style="display: inline-block; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 100%; width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;height: 34">
                                                                                        <tbody>
                                                                                        <tr style="vertical-align: top">
                                                                                            <td valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; color: #ffffff; background-color: #336699; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: center">
                                                                                                <!--<![endif]-->
                                                                                                <a href="{{ url('activate/'.$code.'?refToken='.$token.'&subToken='.$subToken.'&projectCompanyId='.$projectCompanyId.'&bidderId='.$bidderId) }}" target="_blank" style="display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;background-color: #336699;color: #ffffff">
                                                                                                    <span style="font-size:12px;line-height:24px;">CONFIRM</span>
                                                                                                </a>
                                                                                                <!--[if !mso]><!-- -->
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <!--<![endif]-->
                                                                                <!--[if mso]>
                                                                                </center>
                                                                                </v:roundrect>
                                                                                <![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
@endsection
