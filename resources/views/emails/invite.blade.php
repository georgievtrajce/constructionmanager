@extends('layouts.email_master')

@section('content')
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #F1F4F5">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" class="block-grid " style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: #FFFFFF">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #FFFFFF;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="500" style="width:500px;"><![endif]-->
                                        <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 5px;padding-right: 0px;padding-bottom: 5px;padding-left: 0px;border-top: 0px solid transparent;border-right: 3px solid #444444;border-bottom: 0px solid transparent;border-left: 3px solid #444444">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="line-height:14px;font-size:12px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">From:</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><strong><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->name}}</span></strong><br></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->title}}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{$company}}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->email}}</span></p>
                                                                            <p style="margin: 0;line-height: 14px;font-size: 12px"><span style="font-size: 12px; line-height: 14px;">{{Auth::user()->office_phone}}</span></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">Dear <strong>Sir/Madam,</strong>,</p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">&nbsp;<br></p>
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">Your were invited to join the Cloud PM web application
                                                                                by company &nbsp;<strong>{{$company}}.</strong></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #F1F4F5">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#FFFFFF" class="block-grid two-up" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #333;background-color: #FFFFFF">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #FFFFFF;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#FFFFFF" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="250" style="width:250px;"><![endif]-->
                                        <div class="col num6" style="display: inline-block;vertical-align: top;text-align: center;width: 250px">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 5px;padding-right: 0px;padding-bottom: 5px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid #444444;border-bottom: 0px solid transparent;border-left: 3px solid #444444">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">Click here to accept the&nbsp;invitation</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td class="button-container" align="center" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                                        <tbody>
                                                                        <tr style="vertical-align: top">
                                                                            <td width="100%" class="button" align="center" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                                <!--[if mso]>
                                                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:34px;   v-text-anchor:middle; width:115px;" arcsize="12%"   strokecolor="#43a047"   fillcolor="#43a047" >
                                                                                    <w:anchorlock/>
                                                                                    <center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:14px;">
                                                                                <![endif]-->
                                                                                <!--[if !mso]><!-- -->
                                                                                <div align="center" style="display: inline-block; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 100%; width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;height: 34">
                                                                                        <tbody>
                                                                                        <tr style="vertical-align: top">
                                                                                            <td valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; color: #ffffff; background-color: #43a047; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: center">
                                                                                                <!--<![endif]-->
                                                                                                <a href="{{URL::to('/auth/register/'.$token)}}" target="_blank" style="display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;background-color: #43a047;color: #ffffff">
                                                                                                    <span style="font-size:12px;line-height:24px;">ACCEPT</span>
                                                                                                </a>
                                                                                                <!--[if !mso]><!-- -->
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <!--<![endif]-->
                                                                                <!--[if mso]>
                                                                                </center>
                                                                                </v:roundrect>
                                                                                <![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="250" style="width:250px;"><![endif]-->
                                        <div class="col num6" style="display: inline-block;vertical-align: top;text-align: center;width: 250px">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 5px;padding-right: 0px;padding-bottom: 5px;padding-left: 0px;border-top: 0px solid transparent;border-right: 3px solid #444444;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <div style="color:#555555;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color:#555555;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">Click here to reject the invitation</p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td class="button-container" align="center" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;padding-top: 10px;padding-right: 10px;padding-bottom: 10px;padding-left: 10px">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                                        <tbody>
                                                                        <tr style="vertical-align: top">
                                                                            <td width="100%" class="button" align="center" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                                                <!--[if mso]>
                                                                                <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="" style="height:34px;   v-text-anchor:middle; width:113px;" arcsize="12%"   strokecolor="#e53935"   fillcolor="#e53935" >
                                                                                    <w:anchorlock/>
                                                                                    <center style="color:#ffffff; font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif; font-size:14px;">
                                                                                <![endif]-->
                                                                                <!--[if !mso]><!-- -->
                                                                                <div align="center" style="display: inline-block; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; max-width: 100%; width: auto; border-top: 0px solid transparent; border-right: 0px solid transparent; border-bottom: 0px solid transparent; border-left: 0px solid transparent;">
                                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;height: 34">
                                                                                        <tbody>
                                                                                        <tr style="vertical-align: top">
                                                                                            <td valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; color: #ffffff; background-color: #e53935; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align: center">
                                                                                                <!--<![endif]-->
                                                                                                <a href="" target="_blank" style="display: inline-block;text-decoration: none;-webkit-text-size-adjust: none;text-align: center;background-color: #e53935;color: #ffffff">
                                                                                                    <span style="font-size:12px;line-height:24px;">REJECT</span>
                                                                                                </a>
                                                                                                <!--[if !mso]><!-- -->
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </div>
                                                                                <!--<![endif]-->
                                                                                <!--[if mso]>
                                                                                </center>
                                                                                </v:roundrect>
                                                                                <![endif]-->
                                                                            </td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
@endsection
@section('footer-extra')
    <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
        <tbody>
        <tr style="vertical-align: top">
            <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #F1F4F5">
                <!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center"><tr><td>
                <![endif]-->
                <!--[if (IE)]>
                <table width="500" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td>
                <![endif]-->
                <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" class="container" style="border-spacing: 0;border-collapse: collapse;vertical-align: top;max-width: 500px;margin: 0 auto;text-align: inherit">
                    <tbody>
                    <tr style="vertical-align: top">
                        <td width="100%" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                            <table cellpadding="0" cellspacing="0" width="100%" bgcolor="#f1f4f5" class="block-grid " style="border-spacing: 0;border-collapse: collapse;vertical-align: top;width: 100%;max-width: 500px;color: #000000;background-color: #f1f4f5">
                                <tbody>
                                <tr style="vertical-align: top">
                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: #f1f4f5;text-align: center;font-size: 0">
                                        <!--[if (gte mso 9)|(IE)]><table width="100%" align="center" bgcolor="#f1f4f5" cellpadding="0" cellspacing="0" border="0"><tr><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]><td valign="top" width="500" style="width:500px;"><![endif]-->
                                        <div class="col num12" style="display: inline-block;vertical-align: top;width: 100%">
                                            <table cellpadding="0" cellspacing="0" align="center" width="100%" border="0" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;background-color: transparent;padding-top: 20px;padding-right: 0px;padding-bottom: 0px;padding-left: 0px;border-top: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;border-left: 0px solid transparent">
                                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-spacing: 0;border-collapse: collapse;vertical-align: top">
                                                            <tbody>
                                                            <tr style="vertical-align: top">
                                                                <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top;">
                                                                    <div style="color: #a2a2a2;line-height:120%;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;">
                                                                        <div style="font-size:12px;line-height:14px;color: #848484;font-family:Arial, 'Helvetica Neue', Helvetica, sans-serif;text-align:left;">
                                                                            <p style="margin: 0;font-size: 12px;line-height: 14px">This Email was sent by <strong>{{Auth::user()->name}}</strong> from <strong>{{$company}}</strong>.
                                                                                If you don't want to receive emails from this Company please contact
                                                                                <strong>{{Auth::user()->name}}</strong> at <strong>{{Auth::user()->email}}</strong></p>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--[if (gte mso 9)|(IE)]></td><![endif]-->
                                        <!--[if (gte mso 9)|(IE)]></td></tr></table><![endif]-->
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
                <![endif]-->
                <!--[if (IE)]>
                </td></tr></table>
                <![endif]-->
            </td>
        </tr>
        </tbody>
    </table>
@endsection