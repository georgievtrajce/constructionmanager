@extends('layouts.master') @section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
                {{trans('labels.submittals.view_versions')}}<small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
                <ul class="cm-trail">
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                    <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                    <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/pcos'.$pco->id)}}" class="cm-trail-link">{{trans('labels.pcos.view_details')}}</a></li>
                </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_pcos'))
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group cf">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.pcos.name')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">{{$pco->name}}</span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.pcos.number')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">{{$pco->number}}</span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.submittals.subject')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">{{$pco->subject}}</span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.submittals.sent_via')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">{{$pco->sent_via}}</span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5"> @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.pcos.cost')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">{{'$'.number_format($pco->cost, 2)}}</span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5"> @endif
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.submittals.general_contractor')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    @if($pco->gc_id == 0 && $pco->make_me_gc != 0)
                                                    @if(!is_null($project->company))
                                                    {{$project->company->name}}
                                                    @endif
                                                    @elseif($pco->gc_id != 0 && $pco->make_me_gc == 0)
                                                    @if(!is_null($pco->general_contractor))
                                                    {{$pco->general_contractor->name}}
                                                    @endif
                                                    @endif
                                                </span>
                                            </div>
                                        </div>
                                        <hr class="mt5 mb5">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label class="control-label dib">{{trans('labels.pcos.reason_for_change')}}: </label>
                                            </div>
                                            <div class="col-md-8">
                                                <span class="text-larger">
                                                    {{$pco->reason_for_change}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h4>{{trans('labels.pcos.subcontractors')}}</h4>
                            @if($pco->subcontractors->count() != 0)
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <tbody>
                                        <tr>
                                            <th></th>
                                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                                @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                                    <th>{{trans('labels.pcos.cost')}}</th>
                                                    <th></th>
                                                @endif
                                            @else
                                                <th></th>
                                            @endif

                                        </tr>
                                        @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                            @foreach($pco->subcontractors as $subcontractor)
                                                <tr>
                                                    <td>
                                                        <a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id.'/subcontractors/'.$subcontractor->id)}}" class=""> {{$subcontractor->self_performed ? (!is_null($pco->company) ? $pco->company->name : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name}}</a>
                                                    </td>
                                                    @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                                    <td>@if (isset($subcontractor->sub_cost)) {{'$'.number_format($subcontractor->sub_cost, 2)}} @endif</td>
                                                    <td>
                                                        @if(isset($subcontractor->subcontractor_versions) && count($subcontractor->subcontractor_versions) > 0)
                                                            <?php (count($subcontractor->subcontractor_versions) > 0) ? $lastRevision = count($subcontractor->subcontractor_versions) - 1 : $lastRevision = 0 ?>
                                                            @if($subcontractor->subcontractor_versions[$lastRevision]->transmittal_file == true)
                                                                @if(isset($subcontractor->subcontractor_versions[$lastRevision]->transmittalSubmSentFile) && count($subcontractor->subcontractor_versions[$lastRevision]->transmittalSubmSentFile) > 0)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal"
                                                                           href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$subcontractor->subcontractor_versions[$lastRevision]->transmittalSubmSentFile[0]->id}}"
                                                                               value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$subcontractor->subcontractor_versions[$lastRevision]->transmittalSubmSentFile[0]->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endif
                                                            @if($subcontractor->subcontractor_versions[$lastRevision]->download_file == true)
                                                                @if(!is_null($subcontractor->subcontractor_versions[$lastRevision]->file) && count($subcontractor->subcontractor_versions[$lastRevision]->file))
                                                                    @foreach($subcontractor->subcontractor_versions[$lastRevision]->file as $file)
                                                                        @if($file->version_date_connection == $subcontractor->subcontractor_versions[$lastRevision]->file_status)
                                                                            <p class="transmittal-paragraph">
                                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                            </p>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                        @else
                                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.his_pcos'))
                                                @foreach($pco->subcontractors as $subcontractor)
                                                    @if(!is_null($subcontractor->ab_subcontractor) && $subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->comp_id)
                                                        <tr>
                                                            <td>
                                                                <p>{{$subcontractor->self_performed ? (!is_null($pco->company) ? $pco->company->name
                                                                    : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name}}</p>
                                                            </td>
                                                            <td>
                                                                <a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id.'/subcontractors/'.$subcontractor->id)}}" class="btn btn-sm cm-btn-secondary">{{trans('labels.submittals.view_versions')}}</a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>{{trans('labels.no_records')}}</p>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                            <h4>{{trans('labels.pcos.recipient')}}</h4>
                            @if(!is_null($pco->recipient))
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <tbody>
                                        <tr>
                                            <th></th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id.'/recipient/'.$pco->recipient->id)}}" class="">{{$pco->recipient->ab_recipient->name}}</a>
                                            </td>
                                        </tr>
                                        <tbody>
                                </table>
                            </div>
                            @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>{{trans('labels.no_records')}}</p>
                                </div>
                            </div>
                            @endif @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection