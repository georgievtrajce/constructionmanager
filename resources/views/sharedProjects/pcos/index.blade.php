@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.master_pcos')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/pcos')}}" class="cm-trail-link">{{trans('labels.shared.master_pcos')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_pcos'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @if(count($pcos) > 0)
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="cm-filter cm-filter__alt cf">
                                <div class="row">
                                    <div class="col-sm-10">
                                        {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/pcos/filter']) !!}
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                                    {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                         'recipient' => trans('labels.pcos.recipient'),
                                                                                         'name' => trans('labels.pcos.name'),
                                                                                         ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                                                    {!! Form::label('search', trans('labels.search').':') !!}
                                                    {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                                    {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                                    <input type="hidden" id="project_id" value="{{$project->id}}">
                                                </div>
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'recipient')?'show':'hide'}}" id="recipient">
                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                    {!! Form::text('recipient',Input::get('recipient'),['class' => 'cm-control-required form-control address-book-auto-shared search-field-input']) !!}
                                                    {!! Form::hidden('sub_id', Input::get('sub_id'), ['class' => 'address-book-id']) !!}
                                                </div>
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                    {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    {!! Form::label('status', trans('labels.status')) !!}
                                                    {!! Form::select('status',$statuses,Input::get('status')) !!}
                                                    {!! Form::hidden('sort',Input::get('sort','number')) !!}
                                                    {!! Form::hidden('order',Input::get('order','asc')) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                    <div class="col-sm-2">
                                        {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/pcos/report','target'=>'_blank', 'class'=>'pull-right']) !!}
                                        {!! Form::hidden('sort',Input::get('sort','number')) !!}
                                        {!! Form::hidden('order',Input::get('order','asc')) !!}
                                        {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                                        {!! Form::hidden('report_sub_id',Input::get('sub_id')) !!}
                                        {!! Form::hidden('report_recipient_name',Input::get('recipient')) !!}
                                        {!! Form::hidden('report_status',$status) !!}
                                        {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-info pull-right']) !!}
                                        <br>
                                        <label class="pull-right">
                                            <input type="checkbox" name="print_with_notes"> Print with Notes
                                        </label>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                    <thead>
                                        <tr>
                                            <th class="text-center">
                                                <input type="checkbox" name="select_all" id="pcos_select_all">
                                            </th>
                                            <th>
                                                <?php
                                                if (Input::get('sort') == 'number' && Input::get('order') == 'desc') {
                                                    $url = Request::url().'?sort=number&order=asc';
                                                } else {
                                                    $url = Request::url().'?sort=number&order=desc';
                                                }
                                                $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                                $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                                $url .= !empty(Input::get('recipient'))?'&recipient='.Input::get('recipient'):'';
                                                $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                                $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                                $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                                ?>
                                                {{trans('labels.pcos.version_no')}}
                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                </a>
                                            </th>
                                            <th>
                                                {{trans('labels.submittals.cycle_no')}}
                                            </th>
                                            <th>
                                                <?php
                                                if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                                                    $url = Request::url().'?sort=name&order=desc';
                                                } else {
                                                    $url = Request::url().'?sort=name&order=asc';
                                                }
                                                $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                                $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                                $url .= !empty(Input::get('recipient'))?'&recipient='.Input::get('recipient'):'';
                                                $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                                $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                                $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                                ?>
                                                {{trans('labels.pcos.name')}}
                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                </a>
                                            </th>
                                            <th>
                                                <?php
                                                if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                                                    $url = Request::url().'?sort=mf_number&order=desc';
                                                } else {
                                                    $url = Request::url().'?sort=mf_number&order=asc';
                                                }
                                                $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                                                $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                                                $url .= !empty(Input::get('recipient'))?'&recipient='.Input::get('recipient'):'';
                                                $url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';
                                                $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                                                $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                                                ?>
                                                {{trans('labels.mf_number_and_title').' - '.trans('labels.pcos.subcontractors')}}
                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                </a>
                                            </th>
                                            <th>{{trans('labels.pcos.recipient')}}
                                            </th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}
                                            </th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}
                                            </th>
                                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                            <th>
                                                {{trans('labels.pcos.cost')}}
                                            </th>
                                            @endif
                                            <th>{{trans('labels.status')}}
                                            </th>
                                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                            <th>{{trans('labels.files.download')}}
                                            </th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pcos as $item)
                                        <tr>
                                            <td class="text-center">
                                                <input type="checkbox" name="pco_id[]" value="{{$item->id}}" class="pco">
                                            </td>
                                            <td>{{$item->number}}</td>
                                            <td>{{$item->version_cycle_no}}</td>
                                            <td> <a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$item->id)}}" class="">{{$item->name}}</a>
                                        </td>
                                        <td>
                                            @if(count($item->subcontractors))
                                            @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                            @foreach($item->subcontractors as $subcontractor)
                                            <p>{{$subcontractor->mf_number.' '.$subcontractor->mf_title.' - '.($subcontractor->self_performed ? (!is_null($item->company) ? $item->company->name : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name)}}</p>
                                            @endforeach
                                            @else
                                            @foreach($item->subcontractors as $subcontractor)
                                            @if(!is_null($subcontractor->ab_subcontractor) && $subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->comp_id)
                                            <p>{{$subcontractor->mf_number.' '.$subcontractor->mf_title.' - '.($subcontractor->self_performed ? (!is_null($item->company) ? $item->company->name : trans('labels.unknown')) : $subcontractor->ab_subcontractor->name)}}</p>
                                            @endif
                                            @endforeach
                                            @endif
                                            @endif
                                        </td>
                                        <td>{{(!is_null($item->recipient)) ? $item->recipient->ab_recipient->name : ''}}</td>
                                        <td>
                                            @if($item->version_sent_appr != 0)
                                            {{date("m/d/Y", strtotime($item->version_sent_appr))}}
                                            @if($item->version_rec_appr != 0)
                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_rec_appr))) - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                            @else
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_sent_appr))); ?>
                                            @endif
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($item->version_rec_appr))
                                            @if($item->version_rec_appr != 0)
                                            {{date("m/d/Y", strtotime($item->version_rec_appr))}}
                                            @endif
                                            @if($item->version_rec_appr != 0 && !in_array($item->version_status_short_name, ['APP', 'AAN']))
                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_appr))); ?>
                                            ({{floor($dateDiff / (60 * 60 * 24))}})
                                            @endif
                                            @endif
                                        </td>
                                        @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                        <td>@if (isset($item->cost)) {{'$'.number_format($item->cost, 2)}} @endif</td>
                                        @endif
                                        <td>{{$item->version_status_short_name}}</td>
                                        @if($pcoPermissionsType == Config::get('constants.pco_permission_type.all_pcos'))
                                        <td>
                                            <?php $isSubcontractor = false ?>
                                            @foreach($item->subcontractors as $subcontractor)
                                                @if(isset($subcontractor->subcontractor_versions) && count($subcontractor->subcontractor_versions) > 0)
                                                    <?php (count($subcontractor->subcontractor_versions)) > 0 ? $lastSubcontractorVersion = count($subcontractor->subcontractor_versions) - 1 : $lastSubcontractorVersion = 0 ?>
                                                    @if($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittal_file == true)
                                                        @if(isset($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile) && count($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile) > 0)
                                                            <?php $isSubcontractor = true ?>
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile[0]->id}}"
                                                                       value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$subcontractor->subcontractor_versions[$lastSubcontractorVersion]->transmittalSubmSentFile[0]->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endif
                                                    @if($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->download_file == true)
                                                        @if(isset($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file) && count($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file) > 0)
                                                            @foreach($subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file as $file)
                                                                @if($file->version_date_connection == $subcontractor->subcontractor_versions[$lastSubcontractorVersion]->file_status)
                                                                    <?php $isSubcontractor = true ?>
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}"
                                                                               value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                            @if($isSubcontractor == true)
                                                @if(isset($item->recipient) && isset($item->recipient->recipient_versions) && count($item->recipient->recipient_versions) > 0)
                                                    <?php (count($item->recipient->recipient_versions)) > 0 ? $lastRecipientVersion = count($item->recipient->recipient_versions) - 1 : $lastRecipientVersion = 0 ?>
                                                    @if($item->recipient->recipient_versions[$lastRecipientVersion]->transmittal_file == true)
                                                        @if(isset($item->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile) && count($item->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile) > 0)
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$item->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile[0]->id}}"
                                                                       value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$item->recipient->recipient_versions[$lastRecipientVersion]->transmittalSentFile[0]->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endif
                                                    @if($item->recipient->recipient_versions[$lastRecipientVersion]->download_file == true)
                                                        @if(isset($item->recipient->recipient_versions[$lastRecipientVersion]->file) && count($item->recipient->recipient_versions[$lastRecipientVersion]->file) > 0)
                                                            @foreach($item->recipient->recipient_versions[$lastRecipientVersion]->file as $file)
                                                                @if($file->version_date_connection == $item->recipient->recipient_versions[$lastRecipientVersion]->file_status)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}"
                                                                               value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <?php echo $pcos->appends([
                            'sort'=>Input::get('sort'),
                            'order'=>Input::get('order'),
                            'master_format_search'=>Input::get('master_format_search'),
                            'master_format_id'=>Input::get('master_format_id'),
                            'recipient'=>Input::get('recipient'),
                            'sub_id'=>Input::get('sub_id'),
                            'status'=>Input::get('status'),
                            ])->render(); ?>
                        </div>
                    </div>
                </div>
                @else
                <p class="text-center">{{trans('labels.no_records')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
@include('popups.alert_popup')
@endsection