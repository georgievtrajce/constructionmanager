@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.submittals.view_versions')}}<small class="cm-heading-suffix">{{trans('labels.pco').': '.$recipient->pco->name}}</small>
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$recipient->pco->id)}}" class="cm-trail-link">{{trans('labels.pcos.view_details')}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$recipient->pco->id.'/recipient/'.$recipient->id)}}" class="cm-trail-link">{{trans('labels.submittals.view_versions')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_pcos'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="form-group cf">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.pco')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$recipient->pco->name}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.master_format_number_and_title')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$recipient->mf_number}} - {{$recipient->mf_title}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.pcos.recipient')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$recipient->ab_recipient->name}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($recipient->recipient_versions->count() != 0)
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered cm-table-compact">
                            <thead>
                                <tr>
                                    <th>{{trans('labels.pcos.version_no')}}</th>
                                    <th>{{trans('labels.file.name')}}</th>
                                    <th>{{trans('labels.submittals.cycle_no')}}</th>
                                    <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                    <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                    <th>{{trans('labels.status')}}</th>
                                    <th>{{trans('labels.files.download')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($recipient->recipient_versions as $item)
                                <tr>
                                    <td>
                                        @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                                        <a class="" href="{{URL('projects/'.$project->id.'/shared/pcos/'.$recipient->pco->id.'/recipient/'.$recipient->id.'/version/'.$item->id)}}"> {{$recipient->pco->number}}</a>
                                        @endif
                                    </td>
                                    <td>
                                        @if(!is_null($item->latest_file))
                                        {{$item->latest_file->file->file_name}}
                                        @endif
                                    </td>
                                    <td>{{$item->cycle_no}}</td>
                                    <td>
                                        @if($item->sent_appr != 0){{date("m/d/Y", strtotime($item->sent_appr))}}@endif
                                    </td>
                                    <td>
                                        @if($item->rec_appr != 0){{date("m/d/Y", strtotime($item->rec_appr))}}@endif
                                    </td>
                                    <td>{{$item->status->short_name}}</td>
                                    <td>
                                        @if(isset($recipient->recipient_versions) && count($recipient->recipient_versions) > 0)
                                            <?php (count($recipient->recipient_versions) > 0) ? $lastRevision = count($recipient->recipient_versions) - 1 : $lastRevision = 0 ?>
                                            @if($recipient->recipient_versions[$lastRevision]->transmittal_file == true)
                                                @if(isset($recipient->recipient_versions[$lastRevision]->transmittalSentFile) && count($recipient->recipient_versions[$lastRevision]->transmittalSentFile) > 0)
                                                    <p class="transmittal-paragraph">
                                                        <a class="download transmittal"
                                                           href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                        <input type="hidden" class="s3FilePath" id="{{$recipient->recipient_versions[$lastRevision]->transmittalSentFile[0]->id}}"
                                                               value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$recipient->recipient_versions[$lastRevision]->transmittalSentFile[0]->file_name}}">
                                                    </p>
                                                @endif
                                            @endif
                                            @if($recipient->recipient_versions[$lastRevision]->download_file == true)
                                                @if(!is_null($recipient->recipient_versions[$lastRevision]->file) && count($recipient->recipient_versions[$lastRevision]->file))
                                                    @foreach($recipient->recipient_versions[$lastRevision]->file as $file)
                                                        @if($file->version_date_connection == $recipient->recipient_versions[$lastRevision]->file_status)
                                                            <p class="transmittal-paragraph">
                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                            </p>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @else
                    <p class="text-center">{{trans('labels.no_records')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection