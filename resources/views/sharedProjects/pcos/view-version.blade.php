@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.submittals.view_version')}}<small class="cm-heading-suffix">{{trans('labels.pco').': '.$pco->name}}</small>
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos')}}" class="cm-trail-link">{{trans('labels.pcos.project_pcos')}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id)}}" class="cm-trail-link">{{trans('labels.pcos.view_details')}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId)}}" class="cm-trail-link">{{trans('labels.submittals.view_versions')}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/pcos/'.$pco->id.'/'.$versionType.'/'.$pcoSubcontractorRecipientId.'/version/'.$pcoVersion->id)}}" class="cm-trail-link">{{trans('labels.submittals.view_version')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_pcos'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="form-group cf">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.pcos.name')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$pco->name}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.pcos.version_number')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$pco->number}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.submittals.cycle_no')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$pcoVersion->cycle_no}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.status')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">@if(!is_null($pcoVersion->status)){{$pcoVersion->status->name}}@endif</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">
                                            @if($versionType != 'subcontractors')
                                                {{trans('labels.pcos.description_of_change')}}:
                                            @else
                                                {{trans('labels.pcos.transmittal_notes')}}:
                                            @endif
                                        </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{!! $pcoVersion->notes !!}</span>
                                        @if($versionType != 'subcontractors')
                                            @if(isset($pcoVersion->transmittalSentFile[0]))
                                                <a class="download download-pl0" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                <input type="hidden" class="s3FilePath" id="{{$pcoVersion->transmittalSentFile[0]->id}}" value="{{'company_'.$pcoVersion->transmittalSentFile[0]->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$pcoVersion->transmittalSentFile[0]->file_name}}">
                                            @endif
                                        @else
                                            @if(isset($pcoVersion->transmittalSubmSentFile[0]))
                                                <a class="download download-pl0" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                <input type="hidden" class="s3FilePath" id="{{$pcoVersion->transmittalSubmSentFile[0]->id}}" value="{{'company_'.$pcoVersion->transmittalSubmSentFile[0]->comp_id.'/project_'.$project->id.'/pcos/transmittals/'.$pcoVersion->transmittalSubmSentFile[0]->file_name}}">
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                    <tr>
                                        <th>
                                            {{trans('labels.submittals.received_from_subcontractor')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.sent_for_approval')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.received_from_approval')}}
                                        </th>
                                        <th>
                                            {{trans('labels.submittals.sent_to_subcontractor')}}
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td style="width:30%">
                                            @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                <div class="col-md-5 mt10">
                                                    @if($pcoVersion->rec_sub != 0)
                                                        {{date("m/d/Y", strtotime($pcoVersion->rec_sub))}}
                                                        @if($pcoVersion->sent_appr != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->sent_appr))) - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_sub))); ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_sub))); ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </div>
                                                <?php
                                                    $downloadRecSub = false;
                                                    $ownFileSubcontractor = false;
                                                ?>
                                                @if(count($pcoVersion->file))
                                                    @foreach($pcoVersion->file as $recSubFile)
                                                        @if($recSubFile->version_date_connection == Config::get('constants.rfi_version_files.rec_sub_file'))
                                                            <div class="col-md-7">
                                                                <a class="ml20 download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$recSubFile->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$recSubFile->file_name}}">
                                                            </div>
                                                            <?php
                                                            $proposalFileIdSubcontractors = $recSubFile->id;
                                                            $proposalFilePathSubcontractors = 'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$recSubFile->file_name;
                                                            $downloadRecSub = true;
                                                            $ownFileSubcontractor = (Auth::user()->id == $recSubFile->user_id);
                                                            ?>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if (!$downloadRecSub)
                                                    @if(isset($pcoVersion->subcontractor->ab_subcontractor) &&
                                                        $pcoVersion->subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->company->id &&
                                                        CompanyPermissions::can('write', 'pcos'))
                                                        <div class="col-md-4">
                                                            <div class="input-group mb5">
                                                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                                                <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            <input type="file" style="display: none;" name="rec_sub_file" id="rec_sub_file" multiple>
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                                </label>
                                                            </div>
                                                            @if(count($pcoVersion->file))
                                                                @foreach($pcoVersion->file as $recSubFile)
                                                                    <?php $i = 0; ?>
                                                                    @if($recSubFile->version_date_connection == Config::get('constants.rfi_version_files.rec_sub_file'))
                                                                        <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id" value="{{$recSubFile->id}}">
                                                                        <?php $i = 1; ?>
                                                                    @endif
                                                                @endforeach
                                                                @if($i == 0)
                                                                    <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                                                @endif
                                                            @else
                                                                <input type="hidden" name="rec_sub_file_id" id="rec_sub_file_id">
                                                            @endif
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="button" id="rec-sub-upload" class="btn btn-sm cm-btn-secondary pull-left">
                                                                <div id="rec_sub_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                                {{trans('labels.upload')}}
                                                            </button>
                                                        </div>
                                                    @else
                                                        <div class="col-md-7"></div>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td style="width:20%">
                                            @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                @if($pcoVersion->sent_appr != 0)
                                                    {{date("m/d/Y", strtotime($pcoVersion->sent_appr))}}
                                                    @if($pcoVersion->rec_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))) - strtotime(date("m/d/Y", strtotime($pcoVersion->sent_appr))); ?>
                                                    @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->sent_appr))); ?>
                                                    @endif
                                                    ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                                @if(count($pcoVersion->file))
                                                    @foreach($pcoVersion->file as $file)
                                                        @if($file->version_date_connection == Config::get('constants.rfi_version_files.sent_appr_file'))
                                                            <a class="ml20 download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                            <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </td>
                                        <td style="width:30%">
                                            @if($versionType == Config::get('constants.pco_version_type.recipient'))
                                                <div class="col-md-5 {{(isset($pcoVersion->recipient->ab_recipient) && $pcoVersion->recipient->ab_recipient->synced_comp_id == Auth::user()->company->id)?'mt10':''}}">
                                                    @if($pcoVersion->rec_appr != 0)
                                                        {{date("m/d/Y", strtotime($pcoVersion->rec_appr))}}
                                                        @if($pcoVersion->subm_sent_sub != 0)
                                                            <?php $dateDiff = strtotime(date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))); ?>
                                                        @else
                                                            <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->rec_appr))); ?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                </div>
                                                <?php
                                                    $downloadRecAppr = false;
                                                    $ownFileRecipient = false;
                                                ?>
                                                @if(count($pcoVersion->file))
                                                    @foreach($pcoVersion->file as $file)
                                                        @if($file->version_date_connection == Config::get('constants.rfi_version_files.rec_appr_file'))
                                                            <div class="col-md-7">
                                                                <a class="ml20 download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                            </div>
                                                            <?php
                                                            $proposalFileIdRecipients = $file->id;
                                                            $proposalFilePathRecipients = 'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name;
                                                            $downloadRecAppr = true;
                                                            $ownFileRecipient = (Auth::user()->id == $file->user_id);
                                                            ?>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                @if(!$downloadRecAppr)
                                                    @if(isset($pcoVersion->recipient->ab_recipient) &&
                                                        $pcoVersion->recipient->ab_recipient->synced_comp_id == Auth::user()->company->id &&
                                                        CompanyPermissions::can('write', 'pcos'))
                                                        <div class="col-md-4">
                                                            <div class="input-group mb5">
                                                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                                                <label class="input-group-btn">
                                                        <span class="btn btn-primary">
                                                            <input type="file" style="display: none;" name="rec_appr_file" id="rec_appr_file" multiple>
                                                            <span class="glyphicon glyphicon-upload"></span>
                                                        </span>
                                                                </label>
                                                            </div>
                                                            @if(count($pcoVersion->file))
                                                                <?php $i = 0; ?>
                                                                @foreach($pcoVersion->file as $file)
                                                                    @if($file->version_date_connection == Config::get('constants.rfi_version_files.rec_appr_file'))
                                                                        <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id" value="{{$file->id}}">
                                                                        <?php $i = 1; ?>
                                                                    @endif
                                                                @endforeach
                                                                @if($i == 0)
                                                                    <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id">
                                                                @endif
                                                            @else
                                                                <input type="hidden" name="rec_appr_file_id" id="rec_appr_file_id">
                                                            @endif
                                                        </div>
                                                        <div class="col-md-3">
                                                            <button type="button" id="rec-appr-upload" class="btn btn-sm cm-btn-secondary pull-left">
                                                                <div id="rec_appr_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                                </div>
                                                                {{trans('labels.upload')}}
                                                            </button>
                                                        </div>
                                                    @else
                                                        <div class="col-md-7">
                                                        </div>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if($versionType == Config::get('constants.pco_version_type.subcontractors'))
                                                @if($pcoVersion->subm_sent_sub != 0)
                                                    {{date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))}}
                                                    @if(!in_array($pcoVersion->status->short_name, ['APP','AAN']))
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($pcoVersion->subm_sent_sub))); ?>
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                    @endif
                                                @endif
                                                @if(count($pcoVersion->file))
                                                    @foreach($pcoVersion->file as $file)
                                                        @if($file->version_date_connection == Config::get('constants.rfi_version_files.subm_sent_sub_file'))
                                                            <a class="ml20 download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                            <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/pcos/'.$file->file_name}}">
                                                        @endif
                                                    @endforeach
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <div class="row mt10">
                        @if(isset($pcoVersion->subcontractor->ab_subcontractor) &&
                                    $pcoVersion->subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->company->id &&
                                    $ownFileSubcontractor)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group">
                                            <li class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" checked class="companyCheckboxIn" name="companyRadioSubcontractors" data-id="{{$myCompany->id}}" id="mycompany">
                                                        <span class="radio-material">
                                                <span class="check"></span>
                                            </span>
                                                        {{$myCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                            <li data-id="{{$project->id}}" class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="companyCheckboxIn" name="companyRadioSubcontractors" data-id="{{$parentCompany->id}}">
                                                        <span class="radio-material">
                                                        <span class="check"></span>
                                                    </span>
                                                        {{$parentCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group contactsListSubcontractors" id="contactsListSubcontractors-{{$myCompany->id}}">
                                            @foreach($myCompany->users as $user)
                                                <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_subcontractor_users')) && in_array($user->id, old('proposal_subcontractor_users')))?'checked':''}} type="checkbox"
                                                                   name="proposal_subcontractor_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                <span class="check"></span>
                                            </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <ul class="cms-list-group contactsListSubcontractors hide" id="contactsListSubcontractors-{{$parentCompany->id}}">
                                            @foreach($parentCompany->users as $user)
                                                @if(in_array($user->id, $projectUserIds) || $user->hasRole(Config::get('constants.roles.company_admin')))
                                                <li data-id="{{$project->id}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_subcontractor_users')) && in_array($user->id, old('proposal_subcontractor_users')))?'checked':''}}
                                                                   type="checkbox" name="proposal_subcontractor_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                        <span class="check"></span>
                                                    </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                        @if(isset($pcoVersion->rfi->recipient) &&
                                  $pcoVersion->rfi->recipient->synced_comp_id == Auth::user()->company->id &&
                                  $ownFileRecipient)
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group">
                                            <li class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" checked class="companyCheckboxIn" name="companyRadioRecipients" data-id="{{$myCompany->id}}" id="mycompany">
                                                        <span class="radio-material">
                                            <span class="check"></span>
                                        </span>
                                                        {{$myCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                            <li data-id="{{$project->id}}" class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="companyCheckboxIn" name="companyRadioRecipients" data-id="{{$parentCompany->id}}">
                                                        <span class="radio-material">
                                                    <span class="check"></span>
                                                </span>
                                                        {{$parentCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group contactsListRecipients" id="contactsListRecipients-{{$myCompany->id}}">
                                            @foreach($myCompany->users as $user)
                                                <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_recipient_users')) && in_array($user->id, old('proposal_recipient_users')))?'checked':''}} type="checkbox"
                                                                   name="proposal_recipient_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                <span class="check"></span>
                                            </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <ul class="cms-list-group contactsListRecipients hide" id="contactsListRecipients-{{$parentCompany->id}}">
                                            @foreach($parentCompany->users as $user)
                                                @if(in_array($user->id, $projectUserIds) || $user->hasRole(Config::get('constants.roles.company_admin')))
                                                <li data-id="{{$project->id}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_recipient_users')) && in_array($user->id, old('proposal_recipient_users')))?'checked':''}}
                                                                   type="checkbox" name="proposal_recipient_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                        <span class="check"></span>
                                                    </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        @if(isset($pcoVersion->subcontractor->ab_subcontractor) &&
                                    $pcoVersion->subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->company->id &&
                                    $ownFileSubcontractor)
                            <div class="col-md-6">
                                <a class="btn btn-sm pull-left btn-primary sendProposalNotification" data-type="subcontractor"
                                   title="{{trans('labels.tasks.send_email')}}" href="javascript:;" data-fileid="{{$proposalFileIdSubcontractors}}"
                                   data-filepath="{{$proposalFilePathSubcontractors}}">
                                    <span aria-hidden="true">Send Notification</span>
                                    <div class="pull-left email_wait" style="display: none;">
                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                </a>
                            </div>
                        @endif
                        @if(isset($pcoVersion->rfi->recipient) &&
                                  $pcoVersion->rfi->recipient->synced_comp_id == Auth::user()->company->id &&
                                  $ownFileRecipient)
                            <div class="col-md-6">
                                <a class="btn btn-sm pull-left btn-primary sendProposalNotification" data-type="recipient"
                                   title="{{trans('labels.tasks.send_email')}}" href="javascript:;" data-fileid="{{$proposalFileIdRecipients}}"
                                   data-filepath="{{$proposalFilePathRecipients}}">
                                    <span aria-hidden="true">Send Notification</span>
                                    <div class="pull-left email_wait" style="display: none;">
                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                </a>
                            </div>
                        @endif
                    </div>
                    <div class="row">
                        @if(isset($pcoVersion->subcontractor->ab_subcontractor) &&
                                    $pcoVersion->subcontractor->ab_subcontractor->synced_comp_id == Auth::user()->company->id &&
                                    $ownFileSubcontractor)
                            <div class="col-md-6">
                                <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                @if(!empty($distributionListSubcontractor))
                                    @foreach($distributionListSubcontractor as $distributionItem)
                                        <?php $sendDate = Carbon\Carbon::parse($distributionItem->created_at); ?>
                                        @if(!empty($distributionItem->user))
                                            {{$distributionItem->user->name}}{{!empty($distributionItem->user->company)?' - '.$distributionItem->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        @endif
                        @if(isset($pcoVersion->rfi->recipient) &&
                                  $pcoVersion->rfi->recipient->synced_comp_id == Auth::user()->company->id &&
                                  $ownFileRecipient)
                            <div class="col-md-6">
                                <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                @if(!empty($distributionListRecipient))
                                    @foreach($distributionListRecipient as $distributionItem)
                                        <?php $sendDate = Carbon\Carbon::parse($distributionItem->created_at); ?>
                                        @if(!empty($distributionItem->user))
                                            {{$distributionItem->user->name}}{{!empty($distributionItem->user->company)?' - '.$distributionItem->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection