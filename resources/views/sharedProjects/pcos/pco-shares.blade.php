@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.master_pcos')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/shared/pcos/')}}" class="cm-trail-link">{{trans('labels.shared.master_pcos')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_pcos'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="nav nav-pills">
                                <li class="active dropdown">
                                    <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                        {{trans('labels.select')}}
                                        <span class="caret"></span>
                                    </a>
                                    <ul role="group" class="dropdown-menu">
                                        <li>
                                            <a href="{{URL('projects/'.$project->id.'/shared/pcos/')}}">{{trans('labels.submittals.created')}}
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{URL('projects/'.$project->id.'/shared/pcos/shares')}}">{{trans('labels.submittals.shares')}}
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <div class="pull-right">
                                <?php echo $sharedPcos->render();?>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <h4>{{trans('labels.pcos.pcos_shares')}}</h4>
                            @if(count($sharedPcos) > 0)
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                    <thead>
                                        <tr>
                                            <th>{{trans('labels.pcos.version_no')}}</th>
                                            <th>{{trans('labels.submittals.cycle_no')}}</th>
                                            <th>{{trans('labels.master_format_number_and_title')}}</th>
                                            <th>{{trans('labels.pcos.name')}}</th>
                                            <th>{{trans('labels.pcos.recipient')}}</th>
                                            <th>{{trans('labels.pcos.suppliers_subcontractors')}}</th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                            <th>{{trans('labels.status')}}</th>
                                            <th>{{trans('labels.submittals.shared_with')}}</th>
                                            <th>{{trans('labels.File')}}</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sharedPcos as $item)
                                        <tr>
                                            <td>{{$item->version_pco_no}}</td>
                                            <td>{{$item->version_cycle_no}}</td>
                                            <td>
                                                @if (isset($item->mf_number) && isset($item->mf_title))
                                                {{$item->mf_number.' - '.$item->mf_title}}
                                                @endif
                                            </td>
                                            <td>{{$item->name}}</td>
                                            <td>{{(!is_null($item->recipient)) ? $item->recipient->ab_recipient->name : ''}}</td>
                                            <td>
                                                @if(count($item->subcontractors))
                                                @foreach($item->subcontractors as $subcontractor)
                                                <p>{{$subcontractor->ab_subcontractor->name}}</p>
                                                @endforeach
                                                @endif
                                            </td>
                                            <td>@if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif</td>
                                            <td>@if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif</td>
                                            <td>{{$item->version_status_short_name}}</td>
                                            <td>{{$item->company_name}}</td>
                                            <td>
                                                @if(!is_null($item->file_name))
                                                <a class="btn btn-sm btn-primary download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                <input type="hidden" class="s3FilePath" id="{{$item->file_id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/pcos/'.$item->file_name}}">
                                                @endif
                                            </td>
                                            <td>
                                                {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>URL('projects/'.$project->id.'/pcos/'.$item->id.'/share-company/'.$item->comp_parent_id.'/receive-company/'.$item->comp_child_id.'/unshare')]) !!}
                                                {{--{!! Form::submit(trans('labels.unshare'),['class'=>'btn cm-btn-secondary btn-sm', 'onclick' => 'return confirm("'.trans('labels.pcos.unshare_popup').'")']) !!}--}}
                                                <button class='btn cm-btn-secondary btn-sm' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.files.unshare_popup')}}'>
                                                {{trans('labels.unshare')}}
                                                </button>
                                                {!! Form::close()!!}
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            @else
                            <hr>
                            <p class="text-center">{{trans('labels.files.no_shared_files')}}</p>
                            @endif
                        </div>
                    </div>
                    @include('popups.unshare_project_popup')
                    @endsection
                </div>
            </div>
        </div>
    </div>
</div>