@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.materials_and_services')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/expediting-report')}}" class="cm-trail-link">{{trans('labels.materials_and_services')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs')
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_materials_and_services'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @if (($materials) && sizeof($materials) > 0)
                    <div class="cm-filter cm-filter__alt cf">
                        <div class="row">
                            <div class="col-sm-12">
                                {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/expediting-report/filter']) !!}
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                            {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                 'supplier_subcontractor' => trans('labels.Subcontractor'),
                                                                                 'name' => trans('labels.materials_services.name'),
                                                                                 'location' => trans('labels.materials_services.position'),
                                                                                 ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                                            {!! Form::label('search', trans('labels.search').':') !!}
                                            {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                            {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                            <input type="hidden" id="project_id" value="{{$project->id}}">
                                        </div>
                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'supplier_subcontractor')?'show':'hide'}}" id="supplier_subcontractor">
                                            {!! Form::label('search-name', trans('labels.search').':') !!}
                                            {!! Form::text('supplier_subcontractor',Input::get('supplier_subcontractor'),['class' => 'cm-control-required form-control address-book-auto-shared search-field-input']) !!}
                                            {!! Form::hidden('sub_id', Input::get('sub_id'), ['class' => 'address-book-id']) !!}
                                        </div>
                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                            {!! Form::label('search-name', trans('labels.search').':') !!}
                                            {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                        </div>
                                        <div class="form-group search-field {{(Input::get('drop_search_by') == 'location')?'show':'hide'}}" id="location">
                                            {!! Form::label('search-name', trans('labels.search').':') !!}
                                            {!! Form::text('location',Input::get('location'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('status', trans('labels.submittal.status')) !!}
                                            <select name="status" class="form-control">
                                                <option value="">{{trans('labels.select_status')}}</option>
                                                @foreach ($statuses as $status)
                                                <option value="{{$status->id}}" @if(Input::get('status') == $status->id) selected @endif>{{$status->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('status', trans('labels.status')) !!}
                                            <select name="er_status" class="form-control">
                                                <option value="">{{trans('labels.select_status')}}</option>
                                                @foreach ($materialStatuses as $status)
                                                <option value="{{$status->id}}" @if(Input::get('er_status') == $status->id) selected @endif>{{$status->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            {!! Form::label('filter', ' &nbsp;') !!}
                                            {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="pull-right">
                                <?php echo $materials->appends([
                                'id' => Input::get('id'),
                                'ab_name' => Input::get('ab_name'),
                                'name' => Input::get('name'),
                                'mf_number' => Input::get('mf_number'),
                                'mf_title' => Input::get('mf_title'),
                                'status' => Input::get('status'),
                                'sort' => Input::get('sort'),
                                'order' => Input::get('order'),
                                ])->render(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            @if(count($materials) > 0)
                            {!! Form::open(['method'=>'GET','url'=>'/projects/'.$project->id.'/shared/expediting-report/report', 'target'=>'_blank']) !!}
                            <div class="form-group">
                                {!! Form::hidden('master_format_search',Input::get('master_format_search')); !!}
                                {!! Form::hidden('sub_id',Input::get('sub_id')); !!}
                                {!! Form::hidden('supplier_subcontractor',Input::get('supplier_subcontractor')); !!}
                                {!! Form::hidden('status', Input::get('status')); !!}
                                {!! Form::hidden('erStatus', Input::get('er_status')); !!}
                                <input class="btn btn-info pull-right" type="submit" value="{{trans('labels.save_pdf')}}">
                            </div>
                            {!! Form::close() !!}
                            @endif
                        </div>
                    </div>
                    @include('projects.materials.partials.table', $materials)
                    @else
                    <div>
                        <p class="text-center">{{trans('labels.no_records')}}</p>
                    </div>
                    @endif
                </div>
            </div>
            @endsection