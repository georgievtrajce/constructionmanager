@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <header class="cm-heading">
                    {{'Bidder: '.$supplier->address_book->name}}
                    <ul class="cm-trail">
                        <li class="cm-trail-item"><a href="{{URL('projects/'.$project['id'])}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                        <li class="cm-trail-item"><a href="/projects/{{$project['id']}}/shared/bids" class="cm-trail-link">{{trans('labels.shared.master_bids')}}</a></li>
                        <li class="cm-trail-item active"><a href="/projects/{{$project['id']}}/bids/{{$supplier->prop_id}}/bidders/{{$supplier->id}}" class="cm-trail-link">{{trans('labels.bid.edit_bidder')}}</a></li>
                    </ul>
                </header>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('projects.partials.tabs', array('activeTab' => 'shared_bids'))
            </div>
        </div>
        <div class="panel">
            <div class="panel-body">
                {!! Form::open(['method'=> 'PUT', 'url'=>'projects/'.$project['id'].'/shared/bids/'.$supplier->prop_id.'/bidders/'.$supplier->id]) !!}
                <input type="hidden" name="prop_ab_id" id="prop_ab_id" value="{{$supplier->ab_id}}"/>
                <input type="hidden" name="ab_cont_id" id="ab_cont_id" value="{{$supplier->address_book_contact->id}}"/>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('prop_ab', trans('labels.bid.bidder').':') !!}
                                    {{$supplier->address_book->name}}
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('prop_addr', trans('labels.location').':') !!}
                                     <?php $officeId = 0; ?>
                                        @for($i=0; $i<sizeof($address_book->addresses); $i++)
                                            @if (($supplier->address) && ($supplier->address->id == $address_book->addresses[$i]->id))
                                                <?php $officeId = $supplier->address->id; ?>
                                                {{$address_book->addresses[$i]->office_title}}
                                            @else
                                                {{$address_book->addresses[$i]->office_title}}
                                            @endif
                                        @endfor
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    {!! Form::label('prop_ab_cont', trans('labels.proposal.contact').':') !!}
                                        @for($i=0; $i<sizeof($address_book->contacts); $i++)
                                            @if($officeId === $address_book->contacts[$i]->address_id)
                                                @if(($supplier->address_book_contact) && ($supplier->address_book_contact->id == $address_book->contacts[$i]->id))
                                                <input type="hidden" name="addr_id" id="addr_id" value="{{$supplier->address->id}}"/>
                                                {{$address_book->contacts[$i]->name}}
                                                @endif
                                            @endif
                                        @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($companyReadPermissions)
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            {!! Form::label('proposal_1', trans('labels.proposal.proposal_1').':', ['class' => 'control-label']) !!}
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        {!! Form::text('proposal_1', number_format($supplier->proposal_1, 2), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                    <div class="col-sm-4">

                                        <!-- upload select input -->
                                        <div class="input-group">
                                            <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                            <label class="input-group-btn">
                                        <span class="btn btn-primary">
                                            <input type="file" style="display: none;" name="proposal_one_file" id="proposal_one_file" multiple>
                                            <span class="glyphicon glyphicon-upload"></span>
                                        </span>
                                            </label>
                                        </div>
                                        <?php
                                        $existingFileId = null;
                                        if (count($supplier->files)) {
                                            foreach($supplier->files as $proposalFile) {
                                                if(!is_null($proposalFile)) {
                                                    if($proposalFile->item_no == Config::get('constants.proposal_files.first_proposal')) {
                                                        $existingFileId = $proposalFile->file_id;
                                                    }
                                                }
                                            }
                                        }
                                        if (!is_null($existingFileId)) {
                                        ?>
                                        <input type="hidden" name="proposal_one_file_id" id="proposal_one_file_id" value="{{$existingFileId}}">
                                        <?php
                                        } else {
                                        ?>
                                        <input type="hidden" name="proposal_one_file_id" id="proposal_one_file_id">
                                        <?php
                                        }
                                        ?>
                                    </div>
                                @endif
                                <div class="col-sm-4">
                                    @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                        <button type="button" id="proposal_one_upload" class="btn btn-sm cm-btn-secondary pull-left mb0">
                                            <div id="proposal_one_file_wait" class="pull-right ml5"
                                                 style="display: none; margin-left: 10px;">
                                                <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                            </div>
                                            {{trans('labels.upload')}}
                                        </button>
                                    @endif
                                    <?php $firstFile = false; ?>
                                    @if(count($supplier->files))
                                        @foreach($supplier->files as $proposalFile)
                                            @if(!is_null($proposalFile))
                                                @if($proposalFile->item_no == Config::get('constants.proposal_files.first_proposal'))
                                                    <?php $firstFile = true; ?>
                                                    <a class="download pull-left mb0" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                                    <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                                @endif
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="row proposal_one_file_main_message_container">
                                <div id="proposal_one_file_message_container" class="col-md-8 col-md-offset-4 proposal_one_file_message_container message_container" style="color: #49A078;"></div>
                            </div>
                        </div>
                        @if($firstFile)
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group">
                                            <li class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" checked class="companyCheckboxIn" name="companyRadioBidProposal1" data-id="{{$myCompany->id}}" id="mycompany">
                                                        <span class="radio-material">
                                                    <span class="check"></span>
                                                </span>
                                                        {{$myCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                            <li data-id="{{$project->id}}" class="cms-list-group-item cf">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" class="companyCheckboxIn" name="companyRadioBidProposal1" data-id="{{$parentCompany->id}}">
                                                        <span class="radio-material">
                                                            <span class="check"></span>
                                                        </span>
                                                        {{$parentCompany->name}}
                                                    </label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                    <div class="cms-list-box">
                                        <ul class="cms-list-group contactsListBidProposal1" id="contactsListBidProposal1-{{$myCompany->id}}">
                                            @foreach($myCompany->users as $user)
                                                <li class="cms-list-group-item cf">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_1_users')) && in_array($user->id, old('proposal_1_users')))?'checked':''}} type="checkbox"
                                                                   name="proposal_1_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                        <span class="check"></span>
                                                    </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <ul class="cms-list-group contactsListBidProposal1 hide" id="contactsListBidProposal1-{{$parentCompany->id}}">
                                            @foreach($parentCompany->users as $user)
                                                <li data-id="{{$project->id}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input {{(!empty(old('proposal_1_users')) && in_array($user->id, old('proposal_1_users')))?'checked':''}}
                                                                   type="checkbox" name="proposal_1_users[]" value="{{$user->id}}">
                                                            <span class="checkbox-material">
                                                                <span class="check"></span>
                                                            </span>
                                                            {{$user->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-sm pull-left btn-primary sendBidNotification" data-proposal="1" title="{{trans('labels.tasks.send_email')}}" href="javascript:;">
                                    <span aria-hidden="true">Send Notification</span>
                                    <div class="pull-left email_wait" style="display: none;">
                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                @if(!empty($distributionList1))
                                    @foreach($distributionList1 as $distributionItem)
                                        <?php $sendDate = Carbon\Carbon::parse($distributionItem->created_at); ?>
                                        @if(!empty($distributionItem->user))
                                            {{$distributionItem->user->name}}{{!empty($distributionItem->user->company)?' - '.$distributionItem->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        @endif
                        @if($firstFile && !empty($distributionList1))
                            <div class="form-group mt25">
                                {!! Form::label('proposal_2', trans('labels.proposal.proposal_2').':', ['class' =>
                                'control-label']) !!}
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            {!! Form::text('proposal_2', number_format($supplier->proposal_2, 2), ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                        <div class="col-sm-4">
                                            <!-- upload select input -->
                                            <div class="input-group">
                                                <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                                <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                                <input type="file" style="display: none;" name="proposal_two_file" id="proposal_two_file" multiple>
                                                <span class="glyphicon glyphicon-upload"></span>
                                            </span>
                                                </label>
                                            </div>



                                            <?php
                                            $existingFileId = null;
                                            if(count($supplier->files)) {
                                                foreach($supplier->files as $proposalFile) {
                                                    if(!is_null($proposalFile)) {
                                                        if($proposalFile->item_no == Config::get('constants.proposal_files.second_proposal')) {
                                                            $existingFileId = $proposalFile->file_id;
                                                        }
                                                    }
                                                }
                                            }
                                            if(!is_null($existingFileId)) {
                                            ?>
                                            <input type="hidden" name="proposal_two_file_id" id="proposal_two_file_id" value="{{$existingFileId}}">
                                            <?php
                                            } else {
                                            ?>
                                            <input type="hidden" name="proposal_two_file_id" id="proposal_two_file_id">
                                            <?php
                                            }
                                            ?>
                                        </div>
                                    @endif
                                    <div class="col-sm-4">
                                        @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                            <button type="button" id="proposal_two_upload" class="btn btn-sm cm-btn-secondary pull-left mb0">
                                                <div id="proposal_two_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                                    <img src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                </div>
                                                {{trans('labels.upload')}}
                                            </button>

                                        @endif
                                        <?php $secondFile = false; ?>
                                        @if(count($supplier->files))
                                            @foreach($supplier->files as $proposalFile)
                                                @if(!is_null($proposalFile))
                                                    @if($proposalFile->item_no == Config::get('constants.proposal_files.second_proposal'))
                                                        <?php $secondFile = true; ?>
                                                        <a class="download pull-left" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                                        <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                                    @endif
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="row proposal_two_file_main_message_container">
                                    <div id="proposal_two_file_message_container" class="col-md-8 col-md-offset-4 proposal_two_file_message_container message_container" style="color: #49A078;"></div>
                                </div>
                            </div>
                            @if($secondFile)
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                        <div class="cms-list-box">
                                            <ul class="cms-list-group">
                                                <li class="cms-list-group-item cf">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" checked class="companyCheckboxIn" name="companyRadioBidProposal2" data-id="{{$myCompany->id}}" id="mycompany">
                                                            <span class="radio-material">
                                                        <span class="check"></span>
                                                    </span>
                                                            {{$myCompany->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                                <li data-id="{{$project->id}}" class="cms-list-group-item cf">
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" class="companyCheckboxIn" name="companyRadioBidProposal2" data-id="{{$parentCompany->id}}">
                                                            <span class="radio-material">
                                                                <span class="check"></span>
                                                            </span>
                                                            {{$parentCompany->name}}
                                                        </label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                        <div class="cms-list-box">
                                            <ul class="cms-list-group contactsListBidProposal2" id="contactsListBidProposal2-{{$myCompany->id}}">
                                                @foreach($myCompany->users as $user)
                                                    <li class="cms-list-group-item cf">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input {{(!empty(old('proposal_2_users')) && in_array($user->id, old('proposal_2_users')))?'checked':''}} type="checkbox"
                                                                       name="proposal_2_users[]" value="{{$user->id}}">
                                                                <span class="checkbox-material">
                                                            <span class="check"></span>
                                                        </span>
                                                                {{$user->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <ul class="cms-list-group contactsListBidProposal2 hide" id="contactsListBidProposal2-{{$parentCompany->id}}">
                                                @foreach($parentCompany->users as $user)
                                                    <li data-id="{{$project->id}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input {{(!empty(old('proposal_2_users')) && in_array($user->id, old('proposal_2_users')))?'checked':''}}
                                                                       type="checkbox" name="proposal_2_users[]" value="{{$user->id}}">
                                                                <span class="checkbox-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                {{$user->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a class="btn btn-sm pull-left btn-primary sendBidNotification" data-proposal="2" title="{{trans('labels.tasks.send_email')}}" href="javascript:;">
                                        <span aria-hidden="true">Send Notification</span>
                                        <div class="pull-left email_wait" style="display: none;">
                                            <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                    @if(!empty($distributionList2))
                                        @foreach($distributionList2 as $distributionItem)
                                            <?php $sendDate = Carbon\Carbon::parse($distributionItem->created_at); ?>
                                            @if(!empty($distributionItem->user))
                                                {{$distributionItem->user->name}}{{!empty($distributionItem->user->company)?' - '.$distributionItem->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            @endif
                            @if($secondFile && !empty($distributionList2))
                                <div class="form-group mt25">
                                    {!! Form::label('proposal_final', trans('labels.proposal.proposal_final').':', ['class' => 'control-label']) !!}
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                {!! Form::text('proposal_final', number_format($supplier->proposal_final, 2), ['class' => 'form-control']) !!}
                                            </div>
                                        </div>
                                        @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                            <div class="col-sm-4">

                                                <!-- upload select input -->
                                                <div class="input-group">
                                                    <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                                                    <label class="input-group-btn">
                                            <span class="btn btn-primary">
                                                <input type="file" style="display: none;" name="proposal_final_file" id="proposal_final_file" multiple>
                                                <span class="glyphicon glyphicon-upload"></span>
                                            </span>
                                                    </label>
                                                </div>

                                                <?php
                                                $existingFileId = null;
                                                if(count($supplier->files)) {
                                                    foreach($supplier->files as $proposalFile) {
                                                        if(!is_null($proposalFile)) {
                                                            if($proposalFile->item_no == Config::get('constants.proposal_files.final_proposal')) {
                                                                $existingFileId = $proposalFile->file_id;
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <?php
                                                if(!is_null($existingFileId)) {
                                                ?>
                                                <input type="hidden" name="proposal_final_file_id" id="proposal_final_file_id" value="{{$existingFileId}}">
                                                <?php
                                                } else {
                                                ?>
                                                <input type="hidden" name="proposal_final_file_id" id="proposal_final_file_id">
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        @endif
                                        <div class="col-sm-4">
                                            @if (Auth::user()->hasRole('Company Admin') || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || Permissions::can('write', 'bids'))
                                                <button type="button" id="proposal_final_upload" class="btn btn-sm cm-btn-secondary pull-left">
                                                    <div id="proposal_final_file_wait" class="pull-right ml5" style="display: none; margin-left: 10px;">
                                                        <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                                    </div>
                                                    {{trans('labels.upload')}}
                                                </button>

                                            @endif
                                            <?php $thirdFile = false; ?>
                                            @if(count($supplier->files))
                                                @foreach($supplier->files as $proposalFile)
                                                    @if(!is_null($proposalFile))
                                                        @if($proposalFile->item_no == Config::get('constants.proposal_files.final_proposal'))
                                                            <?php $thirdFile = true; ?>
                                                            <a class="download pull-left" href="javascript:;" style="margin-left: 15px;">{{trans('labels.files.download')}}</a>
                                                            <input type="hidden" class="s3FilePath" id="{{$proposalFile->file->id}}" value="{{'company_'.Auth::user()->company->id.'/project_'.$project->id.'/proposals/'.$proposalFile->file->file_name}}">
                                                        @endif
                                                    @endif
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row proposal_final_file_main_message_container">
                                        <div id="proposal_final_file_message_container" class="col-md-8 col-md-offset-4 proposal_final_file_message_container message_container" style="color: #49A078;"></div>
                                    </div>
                                </div>
                                @if($thirdFile)
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb5">{{trans('labels.tasks.company')}}</label>
                                            <div class="cms-list-box">
                                                <ul class="cms-list-group">
                                                    <li class="cms-list-group-item cf">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" checked class="companyCheckboxIn" name="companyRadioBidProposal3" data-id="{{$myCompany->id}}" id="mycompany">
                                                                <span class="radio-material">
                                                            <span class="check"></span>
                                                        </span>
                                                                {{$myCompany->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                    <li data-id="{{$project->id}}" class="cms-list-group-item cf">
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" class="companyCheckboxIn" name="companyRadioBidProposal3" data-id="{{$parentCompany->id}}">
                                                                <span class="radio-material">
                                                                    <span class="check"></span>
                                                                </span>
                                                                {{$parentCompany->name}}
                                                            </label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="mb5">{{trans('labels.tasks.employees')}}</label>
                                            <div class="cms-list-box">
                                                <ul class="cms-list-group contactsListBidProposal3" id="contactsListBidProposal3-{{$myCompany->id}}">
                                                    @foreach($myCompany->users as $user)
                                                        <li class="cms-list-group-item cf">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input {{(!empty(old('proposal_3_users')) && in_array($user->id, old('proposal_3_users')))?'checked':''}} type="checkbox"
                                                                           name="proposal_3_users[]" value="{{$user->id}}">
                                                                    <span class="checkbox-material">
                                                                <span class="check"></span>
                                                            </span>
                                                                    {{$user->name}}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                <ul class="cms-list-group contactsListBidProposal3 hide" id="contactsListBidProposal3-{{$parentCompany->id}}">
                                                    @foreach($parentCompany->users as $user)
                                                        <li data-id="{{$project->id}}" class="cms-list-group-item cf project-filter-distribution-in">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input {{(!empty(old('proposal_3_users')) && in_array($user->id, old('proposal_3_users')))?'checked':''}}
                                                                           type="checkbox" name="proposal_3_users[]" value="{{$user->id}}">
                                                                    <span class="checkbox-material">
                                                                        <span class="check"></span>
                                                                    </span>
                                                                    {{$user->name}}
                                                                </label>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <a class="btn btn-sm pull-left btn-primary sendBidNotification" data-proposal="3" title="{{trans('labels.tasks.send_email')}}" href="javascript:;">
                                            <span aria-hidden="true">Send Notification</span>
                                            <div class="pull-left email_wait" style="display: none;">
                                                <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="17px">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4 class="mb20">{{trans('labels.transmittals.sent_to')}}</h4>
                                        @if(!empty($distributionList3))
                                            @foreach($distributionList3 as $distributionItem)
                                                <?php $sendDate = Carbon\Carbon::parse($distributionItem->created_at); ?>
                                                @if(!empty($distributionItem->user))
                                                    {{$distributionItem->user->name}}{{!empty($distributionItem->user->company)?' - '.$distributionItem->user->company->name:''}} ({{$sendDate->format('m/d/Y')}})<br />
                                                @endif
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
                <div class="row mt25">
                    <div class="col-sm-12">
                        <div class="form-group pull-right">
                            <input type="hidden" id="type" name="type" value="{{Config::get('constants.bids.plural')}}">
                            {!! Form::submit('Save',['class' => 'btn btn-sm btn-success']) !!}
                        </div>
                    </div>
                </div>
                @endif
                {!! Form::close() !!}
                @include('errors.list')
                @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                @endif
            </div>
        </div>
    @include('projects.partials.successful-upload')
    @include('projects.partials.error-upload')
    @include('projects.partials.upload-limitation')
    @include('popups.alert_popup')
    @include('popups.delete_record_popup')
    @include('popups.approve_popup')
@endsection