<style>
    #ui-datepicker-div {
        z-index: 10000!important;
    }
</style>
    <div class="col-md-6">
        <div class="form-group">
            <label class=" control-label">{{trans('labels.mf_number_or_title')}}</label>
            {{$proposal->mf_number. " - ". $proposal->mf_title}}
        </div>
        <div class="form-group">
            {!! Form::label('name', trans('labels.proposal.name').':', ['class' => 'control-label']) !!}
            {{$proposal->name}}
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('status', trans('labels.status').':') !!}
                <div class="form-group">
                    <div class="status-cont">
                        @if($proposal->status == Config::get('constants.proposal_statuses.open'))
                            {{trans('labels.proposal.open')}}
                        @endif
                        @if($proposal->status == Config::get('constants.proposal_statuses.closed'))
                                {{trans('labels.proposal.closed')}}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                {!! Form::label('status', trans('labels.bid.proposals_needed_by').':') !!}
                <div class="form-group">
                    <div class="form-group">
                        {{date("m/d/Y ", strtotime($proposal->proposals_needed_by))}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('scopeOfWorkContent', trans('labels.proposal.scope_of_work').':') !!}
            {!! $proposal->scope_of_work !!}
        </div>
    </div>