<div class="table-responsive">
        <table class="table table-hover table-bordered cm-table-compact">
            <thead>
                <tr>
                    <th>
                        <?php
                        if (Input::get('sort') == 'mf_number' && Input::get('order') == 'desc') {
                            $url = Request::url().'?sort=mf_number&order=asc';
                        } else {
                            $url = Request::url().'?sort=mf_number&order=desc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.master_format_number_and_title')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        <?php
                        if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
                            $url = Request::url().'?sort=name&order=desc';
                        } else {
                            $url = Request::url().'?sort=name&order=asc';
                        }
                        $url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';
                        $url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';
                        $url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';
                        $url .= !empty(Input::get('company'))?'&company='.Input::get('company'):'';
                        $url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';
                        ?>
                        {{trans('labels.proposal.name')}}
                        <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                    </th>
                    <th>
                        {{trans('labels.bid.due_date')}}
                    </th>
                    <th>
                        {{trans('labels.status')}}
                    </th>
                </tr>
            </thead>
            <tbody>
            @for($i=0; $i<sizeof($proposals); $i++)
                @if (sizeof($proposals[$i]->suppliers))
                <tr>
                    <td>
                        @if (isset($proposals[$i]->mf_number) && isset($proposals[$i]->mf_title))
                            {{$proposals[$i]->mf_number.' - '.$proposals[$i]->mf_title}}
                        @endif
                    </td>
                    <td>

                       @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id) || (Permissions::can('read', 'bids')))
                            <a class="" href="/projects/{{$proposals[$i]->proj_id}}/shared/bids/{{$proposals[$i]->id}}">@if (isset($proposals[$i]->name)) {{$proposals[$i]->name}} @endif</a>
                        @endif

                    </td>
                    <td>@if($proposals[$i]->proposals_needed_by != 0){{date("m/d/Y", strtotime($proposals[$i]->proposals_needed_by))}}@endif</td>
                    <td>@if (isset($proposals[$i]->status)) {{ucfirst($proposals[$i]->status)}} @endif</td>
                </tr>
                @endif
            @endfor
            </tbody>
        </table>
</div>