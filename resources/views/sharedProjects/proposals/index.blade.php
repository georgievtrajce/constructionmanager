@extends('layouts.master')
@section('content')
    <div class="container-fluid container-inset">
        <div class="row">
            <div class="col-md-5">
                <header class="cm-heading">
                    {{trans('labels.Project')}} {{trans('labels.bid.plural')}}
                    <ul class="cm-trail">
                        <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}"
                                                     class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a>
                        </li>
                        <li class="cm-trail-item active"><a href="{{URL('/projects/'.$project->id.'/shared/bids')}}"
                                                            class="cm-trail-link">{{trans('labels.shared.master_bids')}}</a></li>
                    </ul>
                </header>
            </div>
            <div class="col-md-7">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @include('projects.partials.tabs', array('activeTab' => 'shared_bids'))
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-body">
                        @include('errors.list')
                        @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                        @endif
                        @if (sizeof($proposals))
                            <div class="cm-filter cm-filter__alt cf">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-10">
                                                {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/bids/filter']) !!}
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <div class="form-group">
                                                                    {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                                                    {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                                    'name' => trans('labels.proposal.name')
                                                                                                    ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title" >
                                                                    {!! Form::label('search', trans('labels.search').':') !!}
                                                                    {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                                                    {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                                                    <input type="hidden" id="project_id" value="{{$project->id}}">
                                                                </div>
                                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                                    {!! Form::text('bid_name',Input::get('bid_name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                                </div>
                                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'company')?'show':'hide'}}" id="company">
                                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                                    {!! Form::text('bid_company',Input::get('bid_company'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            {!! Form::label('status', trans('labels.status')) !!}
                                                            {!! Form::select('status',['All' => 'All', 'open' => trans('labels.proposal.open'), 'closed' => trans('labels.proposal.closed')],Input::get('status')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('sharedProjects.proposals.partials.table', $proposals)
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="pull-right">
                                        <?php echo $proposals->appends([
                                            'sort' => Input::get('sort'),
                                            'order' => Input::get('order'),
                                            'master_format_search' => Input::get('master_format_search'),
                                            'master_format_id' => Input::get('master_format_id'),
                                            'status' => Input::get('status'),
                                            'mf_number' => Input::get('mf_number'),
                                            'mf_title' => Input::get('mf_title'),
                                            'name' => Input::get('name'),
                                        ])->render(); ?>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div>
                                <p class="text-center">{{trans('labels.no_records')}}</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
@endsection