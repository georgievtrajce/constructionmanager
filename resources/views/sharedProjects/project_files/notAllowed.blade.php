@extends('layouts.vertical-tabs')

@section('title')
    {{trans('labels.shared.project_files')}}
    <ul class="cm-trail">
        <li class="cm-trail-item">
            <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}
            </a>
        </li>
        <li class="cm-trail-item active">
            <a href="{{URL('/projects/'.$project->id.'/project-files/'.$type->display_name.'/file')}}" class="cm-trail-link">{{trans('labels.Project').' '.$type->name}}
            </a>
        </li>
    </ul>
@endsection

@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    @include('projects.partials.sharedTabs', array('activeTab' => 'shared_project_files'))
@endsection

@section('vertical-tabs')
    @include('projects.partials.shared-vertical-tabs', array('activeTab' => $type->display_name))
@endsection

@section('tabsContent')
    <div class="row">
        <div class="col-sm-9">
            {{trans('messages.sharedProject.files.no_permission', ['type' => $type->name])}}
        </div>
    </div>
@endsection