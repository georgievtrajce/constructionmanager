@extends('layouts.vertical-tabs')

@section('title')
    {{trans('labels.received.project_files')}}
    <ul class="cm-trail">
        <li class="cm-trail-item">
            <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}
            </a>
        </li>
        <li class="cm-trail-item active">
            <a href="{{URL('/projects/'.$project->id.'/received/project-files/'.$type->display_name.'/file')}}" class="cm-trail-link">{{trans('labels.shared.project_files').' '.$type->name}}
            </a>
        </li>
    </ul>
@endsection

@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
@endsection

@section('vertical-tabs')
    @include('projects.partials.received-vertical-tabs', array('activeTab' => $type->display_name))
@endsection


@section('tabsContent')
    <div class="row">
        <div class="col-sm-12">
            @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
            @endif
            @if(count($projectFiles) > 0)
                <div class="row">
                    <div class="col-md-12">
                        {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/received/project-files/'.$type->display_name.'/file']) !!}
                        <div class="cm-filter cf">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        {!! Form::label('sort', trans('labels.sort_by')) !!}
                                        {!! Form::select('sort',CustomHelper::getReceivedFilesSortingDetails($type->display_name),Input::get('sort')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        {!! Form::label('order', trans('labels.order_by')) !!}
                                        {!! Form::select('order', array('asc'=>trans('labels.ascending'),'desc'=>trans('labels.descending')),Input::get('order')) !!}
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        {!! Form::label('sort', ' &nbsp;') !!}
                                        {!! Form::submit(trans('labels.sort'),['class' => 'btn btn-primary']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            @endif
        </div>
        <div class="col-sm-12">
            @if(count($projectFiles) > 0)
                <div class="table-responsive">
                    <table class="table table-hover cm-table-compact">
                        <thead>
                        <tr>
                            <th>{{trans('labels.file.name')}}</th>
                            <th>{{trans('labels.files.file_number_code')}}</th>
                            <th>{{trans('labels.master_format_number_and_title')}}</th>
                            <th>{{trans('labels.files.upload_date')}}</th>
                            <th>{{trans('labels.files.file_type')}}</th>
                            <th>{{trans('labels.files.file_size')}}</th>
                            <th>{{trans('labels.received.shared_by')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($projectFiles as $item)
                            <tr>
                                <td>{{$item->name}}</td>
                                <td>{{$item->number}}</td>
                                <td>
                                    @if(!empty($item->mf_number) || !empty($item->mf_title))
                                        {{$item->mf_number.' - '.$item->mf_title}}
                                    @endif
                                </td>
                                <td>{{Carbon::parse($item->created_at)->format('m/d/Y h:i:s')}}</td>
                                <td>
                                    <?php
                                    if ($item->file_name != '') {
                                        $fileExtension = explode('.',$item->file_name);
                                        echo $fileExtension[1];
                                    }
                                    ?>
                                </td>
                                <td>{{CustomHelper::formatByteSizeUnits($item->size)}}</td>
                                @if(array_key_exists($type->display_name, Config::get('constants.received_files_modules')))
                                    <td>{{$item->company_name}}</td>
                                    <td>
                                        @if(!empty($item->file_name))
                                            <a class="btn btn-sm btn-primary download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                            <input type="hidden" class="s3FilePath" id="{{$item->file_id}}" value="{{'company_'.$item->comp_parent_id.'/project_'.$project->id.'/'.$type->display_name.'/'.$item->file_name}}">
                                        @endif
                                    </td>
                                @else
                                    <td>
                                        @if(!is_null($item->company))
                                            {{$item->company->name}}
                                        @endif
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                        <input type="hidden" class="s3FilePath" id="{{$item->f_id}}" value="{{'company_'.$item->comp_id.'/project_'.$project->id.'/'.$type->display_name.'/'.$item->file_name}}">
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <p class="text-center">{{trans('labels.no_records')}}</p>
            @endif
        </div>
    </div>
@endsection