@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.master_project_files')}}
            <ul class="cm-trail">
                <li class="cm-trail-item">
                    <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}
                    </a>
                </li>
                <li class="cm-trail-item active">
                    <a href="{{URL('/projects/'.$project->id.'/shared/project-files/'.$type->display_name.'/file')}}" class="cm-trail-link">{{trans('labels.shared.project_files').' '.$type->name}}
                    </a>
                </li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_project_files'))
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="cm-tab-vertical-alt-container">
                                @include('projects.partials.shared-vertical-tabs', array('activeTab' => $type->display_name))
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-sm-12">
                                    @if (Session::has('flash_notification.message'))
                                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {{ Session::get('flash_notification.message') }}
                                    </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if(count($projectFiles) > 0)
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center">
                                                                <input type="checkbox" name="select_all" id="project_files_select_all">
                                                            </th>
                                                            <th>
                                                                <?php
                                                                if (Input::get('sort') == 'files.name' && Input::get('order') == 'asc') {
                                                                    $url = Request::url().'?sort=files.name&order=desc';
                                                                } else {
                                                                    $url = Request::url().'?sort=files.name&order=asc';
                                                                }
                                                                ?>
                                                                {{trans('labels.file.name')}}
                                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            <th>
                                                                <?php
                                                                if (Input::get('sort') == 'files.number' && Input::get('order') == 'desc') {
                                                                    $url = Request::url().'?sort=files.number&order=asc';
                                                                } else {
                                                                    $url = Request::url().'?sort=files.number&order=desc';
                                                                }
                                                                ?>
                                                                {{trans('labels.files.file_number_code')}}
                                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            @if($type == 'project-photos')
                                                            <th>
                                                                {{trans('labels.file.image_preview')}}
                                                                <a href=""><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            @endif
                                                            <th>
                                                                <?php
                                                                if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
                                                                    $url = Request::url().'?sort=mf_number&order=desc';
                                                                } else {
                                                                    $url = Request::url().'?sort=mf_number&order=asc';
                                                                }
                                                                ?>
                                                                {{trans('labels.master_format_number_and_title')}}
                                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            <th>
                                                                <?php
                                                                if (Input::get('sort') == 'files.created_at' && Input::get('order') == 'asc') {
                                                                    $url = Request::url().'?sort=files.created_at&order=desc';
                                                                } else {
                                                                    $url = Request::url().'?sort=files.created_at&order=asc';
                                                                }
                                                                ?>
                                                                {{trans('labels.files.upload_date')}}
                                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            <th>
                                                                {{trans('labels.files.file_type')}}
                                                            </th>
                                                            <th>
                                                                <?php
                                                                if (Input::get('sort') == 'files.size' && Input::get('order') == 'asc') {
                                                                    $url = Request::url().'?sort=files.size&order=desc';
                                                                } else {
                                                                    $url = Request::url().'?sort=files.size&order=asc';
                                                                }
                                                                ?>
                                                                {{trans('labels.files.file_size')}}
                                                                <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i></a>
                                                            </th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($projectFiles as $item)
                                                        <tr>
                                                            <td class="text-center">
                                                                <input type="checkbox" name="file_id[]" value="{{$item->id}}" class="project_file">
                                                            </td>
                                                            <td>{{$item->name}}</td>
                                                            <td>{{$item->number}}</td>
                                                            @if($type == 'project-photos')
                                                            <td>
                                                                <img width="200px" src="{{env('AWS_CLOUD_FRONT').'/company_'.$project->comp_id.'/project_'.$project->id.'/'.$typeDisplay.'/'.$item->file_name}}" />
                                                            </td>
                                                            @endif
                                                            <td>
                                                                @if(!empty($item->mf_number) || !empty($item->mf_title))
                                                                {{$item->mf_number.' - '.$item->mf_title}}
                                                                @endif
                                                            </td>
                                                            <td>{{Carbon::parse($item->created_at)->format('m/d/Y h:i:s')}}</td>
                                                            <td>
                                                                <?php
                                                                if ($item->file_name != '') {
                                                                $fileExtension = explode('.',$item->file_name);
                                                                echo $fileExtension[1];
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>{{CustomHelper::formatByteSizeUnits($item->size)}}</td>
                                                            <td>
                                                                <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                                <input type="hidden" class="s3FilePath" id="{{$item->f_id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/'.$type->display_name.'/'.$item->file_name}}">
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            @else
                                            <p class="text-center">{{trans('labels.no_records')}}</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pull-right">
                                    <?php echo $projectFiles->
                                    appends([
                                    'sort' => Input::get('sort'),
                                    'order' => Input::get('order'),
                                    ])->render();
                                    ?>
                                </div>
                            </div>
                        </div>
                        @include('popups.alert_popup')
                        @endsection