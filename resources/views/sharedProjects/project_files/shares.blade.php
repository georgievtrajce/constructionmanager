@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.files.files_shared_with_you')}}
            <ul class="cm-trail">
                <li class="cm-trail-item">
                    <a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}
                    </a>
                </li>
                <li class="cm-trail-item active">
                    <a href="{{URL('/projects/'.$project->id.'/shared/project-files/'.$type->display_name.'/file/shares')}}" class="cm-trail-link">{{trans('labels.files.files_shared_with_you').' '.$type->name}}
                    </a>
                </li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_project_files'))
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="cm-tab-vertical-alt-container">
                                @include('projects.partials.shared-vertical-tabs', array('activeTab' => $type->display_name))
                            </div>
                        </div>
                        <div class="col-sm-10">
                            @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">

                                    <h4 class="pull-left">{{trans('labels.files.sent_master_files')}}</h4>
                                        <div class="pull-right">





                                        <ul class="nav nav-pills cm-pills-dropdown cm-heading-btn pull-right">
                                            <li class="active dropdown">
                                                <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                                    {{trans('labels.view')}}
                                                    <span class="caret"></span>
                                                </a>
                                                <ul role="group" class="dropdown-menu">
                                                    <li>
                                                        <a href="{{URL('projects/'.$project->id.'/shared/project-files/'.$type->display_name.'/file')}}">{{trans('labels.all_files')}}
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="{{URL('projects/'.$project->id.'/shared/project-files/'.$type->display_name.'/file/shares')}}">{{trans('labels.sent_files')}}
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>

                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <?php echo $projectFiles->appends([
                                        'sort'=>Input::get('sort'),
                                        'order'=>Input::get('order'),
                                        ])->render();
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="container-fluid">
                                <div class="row">

                                    @if(count($projectFiles) > 0)
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th>{{trans('labels.file.name')}}</th>
                                                    <th>{{trans('labels.files.file_number_code')}}</th>
                                                    <th>{{trans('labels.master_format_number_and_title')}}</th>
                                                    <th>{{trans('labels.files.upload_date')}}</th>
                                                    <th>{{trans('labels.files.file_type')}}</th>
                                                    <th>{{trans('labels.files.file_size')}}</th>
                                                    <th>{{trans('labels.files.shared_with')}}</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($projectFiles as $item)
                                                <tr>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->number}}</td>
                                                    <td>
                                                        @if(!empty($item->mf_number) || !empty($item->mf_title))
                                                        {{$item->mf_number.' - '.$item->mf_title}}
                                                        @endif
                                                    </td>
                                                    <td>{{$item->created_at}}</td>
                                                    <td>
                                                        <?php
                                                        if(isset($item->file_name)) {
                                                        $fileExtension = explode('.',$item->file_name);
                                                        echo $fileExtension[1];
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>{{CustomHelper::formatByteSizeUnits($item->size)}}</td>
                                                    <td>{{$item->company_name}}</td>
                                                    <td>
                                                        <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                                        <input type="hidden" class="s3FilePath" id="{{$item->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/'.$type->display_name.'/'.$item->file_name}}">
                                                    </td>
                                                    <td>
                                                        {!! Form::open(['method'=>'POST', 'class'=>'unshare-form-prevent', 'url'=>URL('projects/'.$project->id.'/file/'.$item->id.'/share-company/'.$item->share_comp_id.'/receive-company/'.$item->receive_comp_id.'/unshare')]) !!}
                                                        {{--{!! Form::submit(trans('labels.unshare'),['class'=>'btn cm-btn-secondary btn-sm','onclick' => 'return confirm("'.trans('labels.files.unshare_popup').'")']) !!}--}}
                                                        <button class='btn cm-btn-secondary btn-sm' type='submit' data-toggle="modal" data-target="#confirmUnshare" data-title="Unshare Project" data-message='{{trans('labels.files.unshare_popup')}}'>
                                                        {{trans('labels.unshare')}}
                                                        </button>
                                                        {!! Form::close()!!}
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                    <p class="text-center">{{trans('labels.files.no_shared_files')}}</p>
                                    @endif
                                </div>
                            </div>
                            @include('popups.unshare_project_popup')
                            @endsection