
@extends('layouts.tabs')
@section('title')
    {{trans('labels.shared.project_info')}}
    <ul class="cm-trail">
        <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
    </ul>
@endsection

@section('tabs')
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    @include('projects.partials.sharedTabs', array('activeTab' => 'shared_'.Request::segment(4)))
@endsection
@section('tabsContent')
    <div class="panel">
        <div class="panel-body">
            {{$dataMessage}}
        </div>
    </div>
@endsection