@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.submittals.view_versions')}}<small class="cm-heading-suffix">{{trans('labels.rfi').': '.$rfi->name}}</small>
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/rfis')}}" class="cm-trail-link">{{trans('labels.rfis.project_rfis')}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/rfis/'.$rfi->id)}}" class="cm-trail-link">{{trans('labels.submittals.view_versions')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_rfis'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="form-group cf">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.master_format_number_and_title')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">{{$rfi->mf_number}} - {{$rfi->mf_title}}</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.rfis.name')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">{{$rfi->name}}</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.submittals.subject')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">{{$rfi->subject}}</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.submittals.sent_via')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">{{$rfi->sent_via}}</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.rfis.is_change')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">{{$rfi->is_change}}</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.submittals.import_supplier')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">   @if(!is_null($rfi->subcontractor))
                                        {{$rfi->subcontractor->name}}
                                    @endif</span>
                                </div>
                            </div>
                            <hr class="mt5 mb5">
                            <div class="row">
                                <div class="col-md-4">
                                    <label class="control-label dib">{{trans('labels.submittals.import_recipient')}}: </label>
                                </div>
                                <div class="col-md-8">
                                    <span class="text-larger">
                                        @if(!is_null($rfi->recipient))
                                        {{$rfi->recipient->name}}
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($rfi->rfis_versions->count() != 0)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                        <tr>
                                            <th>{{trans('labels.rfis.version_no')}}</th>
                                            <th>{{trans('labels.submittals.name')}}</th>
                                            <th>{{trans('labels.submittals.cycle_no')}}</th>
                                            <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                            <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                            <th>{{trans('labels.status')}}</th>
                                            <th>{{trans('labels.files.download')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rfi->rfis_versions as $item)
                                        <tr>
                                            <td>
                                                {{$rfi->number}}
                                            </td>
                                            <td>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                                                    <a class="" href="{{URL('projects/'.$project->id.'/shared/rfis/'.$rfi->id.'/version/'.$item->id)}}"> {{$rfi->name}}</a>
                                                @else
                                                    {{$rfi->name}}
                                                @endif
                                            </td>
                                            <td>{{$item->cycle_no}}</td>
                                            <td>
                                                @if($item->rec_sub != 0)
                                                {{date("m/d/Y", strtotime($item->rec_sub))}}
                                                @if($item->sent_appr != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->sent_appr))) - strtotime(date("m/d/Y", strtotime($item->rec_sub))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->rec_sub))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->sent_appr != 0)
                                                {{date("m/d/Y", strtotime($item->sent_appr))}}
                                                @if($item->rec_appr != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->rec_appr))) - strtotime(date("m/d/Y", strtotime($item->sent_appr))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->sent_appr))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->rec_appr != 0)
                                                {{date("m/d/Y", strtotime($item->rec_appr))}}
                                                @if($item->subm_sent_sub != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($item->rec_appr))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->rec_appr))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->subm_sent_sub != 0)
                                                {{date("m/d/Y", strtotime($item->subm_sent_sub))}}
                                                @if(!in_array($item->status->short_name, ['Closed']))
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->subm_sent_sub))); ?>
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                                @endif
                                            </td>
                                            <td>@if(!is_null($item->status)){{$item->status->short_name}}@endif</td>
                                            <td>
                                                @if(isset($rfi->rfis_versions) && count($rfi->rfis_versions) > 0)
                                                    <?php (count($rfi->rfis_versions) > 0) ? $lastRevision = count($rfi->rfis_versions) - 1 : $lastRevision = 0 ?>
                                                        @if(count($rfi->rfis_versions[$lastRevision]->transmittalSubmSentFile) > 0 || count($rfi->rfis_versions[$lastRevision]->transmittalSentFile) > 0)
                                                            @if(count($rfi->rfis_versions[$lastRevision]->transmittalSubmSentFile) > 0 &&
                                                               !empty($rfi->rfis_versions[$lastRevision]->subm_sent_sub) &&
                                                               $rfi->rfis_versions[$lastRevision]->subm_sent_sub != '0000-00-00')
                                                                <p class="transmittal-paragraph">
                                                                    <a class="download transmittal"
                                                                       href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                    <input type="hidden" class="s3FilePath" id="{{$rfi->rfis_versions[$lastRevision]->transmittalSubmSentFile[0]->id}}"
                                                                           value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/rfis/transmittals/'.$rfi->rfis_versions[$lastRevision]->transmittalSubmSentFile[0]->file_name}}">
                                                                </p>
                                                            @elseif(count($rfi->rfis_versions[$lastRevision]->transmittalSentFile) > 0 &&
                                                                !empty($rfi->rfis_versions[$lastRevision]->sent_appr) &&
                                                                $rfi->rfis_versions[$lastRevision]->sent_appr != '0000-00-00' &&
                                                               (empty($rfi->rfis_versions[$lastRevision]->subm_sent_sub) ||
                                                               $rfi->rfis_versions[$lastRevision]->subm_sent_sub == '0000-00-00') &&
                                                               (empty($rfi->rfis_versions[$lastRevision]->rec_appr) ||
                                                               $rfi->rfis_versions[$lastRevision]->rec_appr == '0000-00-00'))
                                                                <p class="transmittal-paragraph">
                                                                    <a class="download transmittal"
                                                                       href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                    <input type="hidden" class="s3FilePath" id="{{$rfi->rfis_versions[$lastRevision]->transmittalSentFile[0]->id}}"
                                                                           value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/rfis/transmittals/'.$rfi->rfis_versions[$lastRevision]->transmittalSentFile[0]->file_name}}">
                                                                </p>
                                                            @endif
                                                        @endif
                                                    @if($rfi->rfis_versions[$lastRevision]->download_file == true)
                                                        @if(!is_null($rfi->rfis_versions[$lastRevision]->file) && count($rfi->rfis_versions[$lastRevision]->file))
                                                            @foreach($rfi->rfis_versions[$lastRevision]->file as $file)
                                                                @if($file->version_date_connection == $rfi->rfis_versions[$lastRevision]->file_status)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/rfis/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @else
                    <p class="text-center">{{trans('labels.no_records')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection