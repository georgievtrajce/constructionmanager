<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CMS Construction Master</title>


    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700,400,600" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="cm-page-content">
        <div class="container">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-8">
                            <h2>{{trans('labels.Project').': '.$project->name}}</h2>
                            <h3>{{trans('labels.rfis.rfis_filtered_by')}}</h3>
                            <h4>{{$title}}</h4>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row" style="font-size: 10px;">
                        <style>
                            .rfis-padding {
                                padding: 5px 5px!important;
                            }
                        </style>
                        @if(count($rfis) != 0)
                        <div class="table-responsive">
                            <table class="table table-hover table-striped cm-table-compact">
                                <thead>
                                <tr>
                                    <th>{{trans('labels.rfis.version_no')}}</th>
                                    <th>{{trans('labels.submittals.cycle_no')}}</th>
                                    <th>{{trans('labels.master_format_number_and_title')}}</th>
                                    <th>{{trans('labels.rfis.name')}}</th>
                                    <th>{{trans('labels.supplier_subcontractor')}}</th>
                                    <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                    <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                    <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                    <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                    <th>{{trans('labels.status')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($rfis as $item)
                                        <tr>
                                            <td class="rfis-padding">{{$item->version_rfi_no}}</td>
                                            <td class="rfis-padding">{{$item->version_cycle_no}}</td>
                                            <td class="rfis-padding">
                                                @if (isset($item->mf_number) && isset($item->mf_title))
                                                    {{$item->mf_number.' - '.$item->mf_title}}
                                                @endif
                                            </td>
                                            <td class="rfis-padding">{{$item->name}}</td>
                                            <td class="rfis-padding">{{$item->subcontractor_name}}</td>
                                            <td>@if($item->version_rec_sub != 0){{date("m/d/Y", strtotime($item->version_rec_sub))}}@endif</td>
                                            <td>@if($item->version_sent_appr != 0){{date("m/d/Y", strtotime($item->version_sent_appr))}}@endif</td>
                                            <td>@if($item->version_rec_appr != 0){{date("m/d/Y", strtotime($item->version_rec_appr))}}@endif</td>
                                            <td>@if($item->version_subm_sent_sub != 0){{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}@endif</td>
                                            <td class="submittals-padding">{{$item->version_status_short_name}}</td>
                                        </tr>
                                        <tr class="cm-row-separator">
                                            <td colspan="17"></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @else
                            <p class="text-center">{{trans('labels.no_records')}}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>