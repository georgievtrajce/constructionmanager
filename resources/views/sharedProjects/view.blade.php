@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.project_info')}}
            <ul class="cm-trail">
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'info'))
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="">
                        <div class="row box-cont-1">
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4>{{trans('labels.general')}}</h4>
                                            <div class="cm-spacer-xs"></div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.name')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if(!empty($project->name))
                                                                {{$project->name}}
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.Address')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if(!is_null($project->address) && !empty($project->address->street))
                                                                <a target="_blank" href="http://maps.google.com/?q={{(!empty($project->address->street)?$project->address->street:'').
                                                                    (!empty($project->address->city)?', ':'').
                                                                    (!empty($project->address->city)?$project->address->city:'').
                                                                    (!empty($project->address->state)?', ':'').
                                                                    (!empty($project->address->state)?$project->address->state:'')}}">
                                                                    {{(!empty($project->address->street)?$project->address->street:'').
                                                                    (!empty($project->address->city)?', ':'').
                                                                    (!empty($project->address->city)?$project->address->city:'').
                                                                    (!empty($project->address->state)?', ':'').
                                                                    (!empty($project->address->state)?$project->address->state:'')}}
                                                                </a>
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.number')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if(!empty($project->number))
                                                                {{$project->number}}
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.upc_code')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                {{$project->upc_code}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.status')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                <?php echo ($project->active == Config::get('constants.status.active')) ? trans('labels.active') : trans('labels.completed'); ?>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.value')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                {{'$'.number_format($project->price,2)}}
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.owner')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if (isset($project->owner))
                                                                <a href="{{URL('address-book/'.$project->owner->id.'/view')}}">{{$project->owner->name}}</a>
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.architect')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if (isset($project->architect))
                                                                <a href="{{URL('address-book/'.$project->architect->id.'/view')}}">{{$project->architect->name}}</a>
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.start_date')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if(!empty($project->start_date) && $project->start_date != 0)
                                                                {{date("m/d/Y", strtotime($project->start_date))}}
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <hr class="mt5 mb5">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <label class="control-label dib">{{trans('labels.project.end_date')}}: </label>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <span class="text-larger">
                                                                @if(!empty($project->end_date) && $project->end_date != 0)
                                                                {{date("m/d/Y", strtotime($project->end_date))}}
                                                                @else
                                                                <i>{{trans('labels.unknown')}}</i>
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(!empty($city))
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4>{{trans('labels.project.weather')}}</h4>
                                            <div class="cm-spacer-xs"></div>
                                            <iframe id="forecast_embed" type="text/html" frameborder="0" height="245" width="100%" src="https://forecast.io/embed/#lat={{$city->lat}}&lon={{$city->lng}}&name={{$city->city.", ".$city->state}}&color=#00aaff&font=Georgia&units=uk"> </iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            @if (sizeof($blogPosts))
                            <div class="col-md-6">
                                <div class="box">
                                    <div class="card">
                                        <div class="card-body">
                                            <div data-equal-height class="cm-box cm-box-info">
                                                <div class="cm-box-heading">
                                                    <span class="cm-box-heading-icon"><i class="glyphicon glyphicon-comment"></i></span>
                                                    <h3 class="cm-box-heading-title">{{trans('labels.latest_blog_posts')}}</h3>
                                                </div>
                                                <ul class="cm-box-list">
                                                    @foreach ($blogPosts as $blogPost)
                                                    <li class="cm-box-list-item">
                                                        <h6 class="cm-box-list-item-date">{{Carbon::parse($blogPost->created_at)->format('m/d/Y')}}</h6>
                                                        <a class="cm-box-list-item-title" href="{{URL('/projects/'.$blogPost->proj_id.'/blog/'.$blogPost->id)}}"><strong>{{ $blogPost->title }}</strong></a>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                <a class="cm-box-more" href="{{URL('projects/'.$project->id.'/all-blog-posts')}}">{{trans('labels.view_more')}}</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-6">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB0uwmWovEYrgzB97mhaMjbQTgQIYvJ6KQ"></script>
    <style>
    #map_canvas {
    height: 300px;
    width: 100%;
    }
    </style>
    <script type="text/javascript" src="{{URL::asset('js/loader.js')}}"></script>
    <script type="text/javascript">
        var locationAddress = '{{(!empty($address))?implode(',', $address):''}}';

        var dataDbSubmittals = [['Type', 'Submittals']];
        @foreach($submittalsStatistics as $key=>$item)
        dataDbSubmittals.push(['{{$key}}', {{$item}}]);
                @endforeach

        var dataDbRFIs = [['Type', 'RFI\'s']];
        @foreach($rfisStatistics as $key=>$item)
        dataDbRFIs.push(['{{$key}}', {{$item}}]);
                @endforeach

        var dataDbPCOs = [['Type', 'PCO\'s']];
        @foreach($pcoStatistics as $key=>$item)
        dataDbPCOs.push(['{{$key}}', {{$item}}]);
        @endforeach
    </script>
    <script src="{{URL::asset('js/google-charts.js')}}"></script>
    @endsection