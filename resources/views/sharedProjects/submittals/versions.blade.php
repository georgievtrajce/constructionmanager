@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.submittals.view_versions')}}<small class="cm-heading-suffix">{{trans('labels.submittal_global').': '.$submittal->name}}</small>
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item"><a href="/projects/{{$project->id}}/shared/submittals" class="cm-trail-link">{{trans('labels.submittals.project_submittals')}}</a></li>
                <li class="cm-trail-item active"><a href="/projects/{{$project->id}}/shared/submittals/{{$submittal->id}}" class="cm-trail-link">{{trans('labels.submittals.view_versions')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_submittals'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="form-group cf">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.master_format_number_and_title')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$submittal->mf_number}} - {{$submittal->mf_title}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.submittals.name')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger">{{$submittal->name}}</span>
                                    </div>
                                </div>
                                <hr class="mt5 mb5">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="control-label dib">{{trans('labels.submittals.import_supplier')}}: </label>
                                    </div>
                                    <div class="col-md-8">
                                        <span class="text-larger"> @if(!is_null($submittal->subcontractor))
                                            {{$submittal->subcontractor->name}}
                                        @endif</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($submittal->submittals_versions->count() != 0)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover table-bordered cm-table-compact">
                                    <thead>
                                        <tr>
                                            <th>{{trans('labels.submittals.version_no')}}</th>
                                            <th>{{trans('labels.submittals.name')}}</th>
                                            <th>{{trans('labels.submittals.cycle_no')}}</th>
                                            <th>{{trans('labels.submittals.received_from_subcontractor')}}</th>
                                            <th>{{trans('labels.submittals.sent_for_approval')}}</th>
                                            <th>{{trans('labels.submittals.received_from_approval')}}</th>
                                            <th>{{trans('labels.submittals.sent_to_subcontractor')}}</th>
                                            <th>{{trans('labels.status')}}</th>
                                            <th>{{trans('labels.files.download')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($submittal->submittals_versions as $item)
                                        <tr>
                                            <td>
                                                {{$submittal->number}}
                                            </td>
                                            <td>
                                                @if ((Auth::user()->hasRole('Company Admin')) || (Auth::user()->hasRole('Project Admin') && $project->proj_admin == Auth::user()->id))
                                                    <a class="" href="{{URL('projects/'.$project->id.'/shared/submittals/'.$submittal->id.'/version/'.$item->id)}}">{{$submittal->name}}</a>
                                                @else
                                                    {{$submittal->name}}
                                                @endif
                                            </td>
                                            <td>{{$item->cycle_no}}</td>
                                            <td>
                                                @if($item->rec_sub != 0)
                                                {{date("m/d/Y", strtotime($item->rec_sub))}}
                                                @if($item->sent_appr != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->sent_appr))) - strtotime(date("m/d/Y", strtotime($item->rec_sub))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->rec_sub))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->sent_appr != 0)
                                                {{date("m/d/Y", strtotime($item->sent_appr))}}
                                                @if($item->rec_appr != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->rec_appr))) - strtotime(date("m/d/Y", strtotime($item->sent_appr))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->sent_appr))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->rec_appr != 0)
                                                {{date("m/d/Y", strtotime($item->rec_appr))}}
                                                @if($item->subm_sent_sub != 0)
                                                <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($item->rec_appr))); ?>
                                                @else
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->rec_appr))); ?>
                                                @endif
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                            </td>
                                            <td>
                                                @if(!is_null($item->subm_sent_sub) && $item->subm_sent_sub != 0)
                                                {{date("m/d/Y", strtotime($item->subm_sent_sub))}}
                                                @if(!in_array($item->status->short_name, ['APP','AAN']))
                                                <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->subm_sent_sub))); ?>
                                                ({{floor($dateDiff / (60 * 60 * 24))}})
                                                @endif
                                                @endif
                                            </td>
                                            <td>@if(!is_null($item->status)){{$item->status->short_name}}@endif</td>
                                            <td>
                                                @if(isset($submittal->submittals_versions) && count($submittal->submittals_versions) > 0)
                                                    <?php (count($submittal->submittals_versions) > 0) ? $lastRevision = count($submittal->submittals_versions) - 1 : $lastRevision = 0 ?>
                                                    @if(count($submittal->submittals_versions[$lastRevision]->transmittalSubmSentFile) > 0 || count($submittal->submittals_versions[$lastRevision]->transmittalSentFile) > 0)
                                                        @if(count($submittal->submittals_versions[$lastRevision]->transmittalSubmSentFile) > 0 &&
                                                           !empty($submittal->submittals_versions[$lastRevision]->subm_sent_sub) &&
                                                           $submittal->submittals_versions[$lastRevision]->subm_sent_sub != '0000-00-00')
                                                                <p class="transmittal-paragraph">
                                                                    <a class="download transmittal"
                                                                       href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                    <input type="hidden" class="s3FilePath" id="{{$submittal->submittals_versions[$lastRevision]->transmittalSubmSentFile[0]->id}}"
                                                                           value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/transmittals/'.$submittal->submittals_versions[$lastRevision]->transmittalSubmSentFile[0]->file_name}}">
                                                                </p>
                                                        @elseif(count($submittal->submittals_versions[$lastRevision]->transmittalSentFile) > 0 &&
                                                            !empty($submittal->submittals_versions[$lastRevision]->sent_appr) &&
                                                            $submittal->submittals_versions[$lastRevision]->sent_appr != '0000-00-00' &&
                                                           (empty($submittal->submittals_versions[$lastRevision]->subm_sent_sub) ||
                                                           $submittal->submittals_versions[$lastRevision]->subm_sent_sub == '0000-00-00') &&
                                                           (empty($submittal->submittals_versions[$lastRevision]->rec_appr) ||
                                                           $submittal->submittals_versions[$lastRevision]->rec_appr == '0000-00-00'))
                                                                <p class="transmittal-paragraph">
                                                                    <a class="download transmittal"
                                                                       href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                    <input type="hidden" class="s3FilePath" id="{{$submittal->submittals_versions[$lastRevision]->transmittalSentFile[0]->id}}"
                                                                           value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/transmittals/'.$submittal->submittals_versions[$lastRevision]->transmittalSentFile[0]->file_name}}">
                                                                </p>
                                                        @endif
                                                    @endif
                                                    @if($submittal->submittals_versions[$lastRevision]->download_file == true)
                                                        @if(!is_null($submittal->submittals_versions[$lastRevision]->file) && count($submittal->submittals_versions[$lastRevision]->file))
                                                            @foreach($submittal->submittals_versions[$lastRevision]->file as $file)
                                                                @if($file->version_date_connection == $submittal->submittals_versions[$lastRevision]->file_status)
                                                                    <p class="transmittal-paragraph">
                                                                        <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                        <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/'.$file->file_name}}">
                                                                    </p>
                                                                @endif
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    @else
                    <p class="text-center">{{trans('labels.no_records')}}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection