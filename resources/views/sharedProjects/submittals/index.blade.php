@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.master_submittals')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id)}}" class="cm-trail-link">{{trans('labels.Project').': '.$project->name}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/submittals')}}" class="cm-trail-link">{{trans('labels.shared.master_submittals')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_submittals'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @if(count($submittals) > 0)
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-10">
                            <div class="cm-filter cm-filter__alt cf">
                                <div class="row">
                                    <div class="col-sm-10">
                                        {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/submittals/filter']) !!}
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    {!! Form::label('search_by', trans('labels.search_by').':') !!}
                                                    {!! Form::select('drop_search_by', [ 'mf_number_title' => trans('labels.mf_number_and_title'),
                                                                                         'supplier_subcontractor' => trans('labels.submittals.supplier_subcontractor'),
                                                                                         'name' => trans('labels.submittals.name'),
                                                                                         ], Input::get('drop_search_by'), ['id' => 'drop_search_by']) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'mf_number_title' || Input::get('drop_search_by') == null)?'show':'hide'}}" id="mf_number_title">
                                                    {!! Form::label('search', trans('labels.search').':') !!}
                                                    {!! Form::text('master_format_search',Input::get('master_format_search'),['class' => 'cm-control-required form-control mf-number-title-auto search-field-input']) !!}
                                                    {!! Form::hidden('master_format_id',Input::get('master_format_id'),['class' => 'master-format-id search-field-input']) !!}
                                                    <input type="hidden" id="project_id" value="{{$project->id}}">
                                                </div>
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'supplier_subcontractor')?'show':'hide'}}" id="supplier_subcontractor">
                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                    {!! Form::text('supplier_subcontractor',Input::get('supplier_subcontractor'),['class' => 'cm-control-required form-control address-book-auto-shared search-field-input']) !!}
                                                    {!! Form::hidden('sub_id', Input::get('sub_id'), ['class' => 'address-book-id']) !!}
                                                </div>
                                                <div class="form-group search-field {{(Input::get('drop_search_by') == 'name')?'show':'hide'}}" id="name">
                                                    {!! Form::label('search-name', trans('labels.search').':') !!}
                                                    {!! Form::text('name',Input::get('name'),['class' => 'cm-control-required form-control search-field-input']) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    {!! Form::label('status', trans('labels.status')) !!}
                                                    {!! Form::select('status', $statuses, Input::get('status')) !!}
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    {!! Form::submit(trans('labels.filter'),['class' => 'btn btn-primary cm-btn-fixer']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-2">
                            <!-- <div class="panel cm-panel-beta">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label class="control-label">{{trans('labels.files.share_with')}}</label>
                                        <select name="subm_share_with" id="subm_share_with">
                                            <option value="all">{{trans('labels.files.all_subcontractors')}}</option>
                                            <option value="selected">{{trans('labels.files.selected_companies')}}</option>
                                        </select>
                                    </div>
                                    <div id="selected_input_cont">
                                        <div id="search_project_subcontractor">
                                            <div class="form-group">
                                                <label>{{trans('labels.files.select_company')}}</label>
                                                <input type="text" class="form-control project-subcontractor-auto" name="project_subcontractor">
                                                <input type="hidden" class="project-subcontractor-id" name="sub_id">
                                                <input type="hidden" id="project-id" name="proj_id" value="{{$project->id}}"></div>
                                            </div>
                                        </div>
                                        <div  id="selected_subcontractors_cont">
                                            <label>{{trans('labels.files.selected_companies').':'}}</label>
                                            <div id="selected_subcontractors"></div>
                                        </div>
                                        <div >
                                            <a href="javascript:;" id="share_subm_file" class="btn btn-primary cm-btn-fixer pull-right">{{trans('labels.share')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="col-md-10 mt10">
                                    {!! Form::open(['method'=>'GET','url'=>'projects/'.$project->id.'/shared/submittals/savePdf','target'=>'_blank', 'class'=>'pull-right']) !!}
                                    {!! Form::hidden('sort',Input::get('sort','number')) !!}
                                    {!! Form::hidden('order',Input::get('order','asc')) !!}
                                    {!! Form::hidden('report_mf',Input::get('master_format_search')) !!}
                                    {!! Form::hidden('report_sub_id',Input::get('sub_id')) !!}
                                    {!! Form::hidden('report_sub_name',Input::get('supplier_subcontractor')) !!}
                                    {!! Form::hidden('report_status',$status) !!}
                                    {!! Form::submit(trans('labels.submittals.save_to_pdf'),['class' => 'btn btn-info pull-right']) !!}
                                    <br>
                                    <label class="pull-right">
                                        <input type="checkbox" name="print_with_notes"> Print with Notes
                                    </label>
                                    {!! Form::close() !!}
                                    <!-- <ul class="nav nav-pills cm-pills-dropdown cm-heading-btn pull-left">
                                        <li class="active dropdown">
                                            <a data-toggle="dropdown" href="#" class="btn btn-default dropdown-toggle">
                                                {{trans('labels.select')}}
                                                <span class="caret"></span>
                                            </a>
                                            <ul role="group" class="dropdown-menu">
                                                <li>
                                                    <a href="{{URL('projects/'.$project->id.'/shared/submittals/')}}">{{trans('labels.submittals.created')}}
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{URL('projects/'.$project->id.'/shared/submittals/shares')}}">{{trans('labels.submittals.shares')}}
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul> -->
                                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered cm-table-compact" id="unshared-files">
                                        <thead>
                                            <tr>
                                                <th class="text-center">
                                                    <input type="checkbox" name="select_all" id="submittals_select_all">
                                                </th>
                                                <th>
                                                    <?php
if (Input::get('sort') == 'number' && Input::get('order') == 'desc') {
	
	$url = Request::url().'?sort=number&order=asc';
	
}
else {
	
	$url = Request::url().'?sort=number&order=desc';
	
}

$url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';

$url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';

$url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';

$url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';

$url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';

$url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';

?>
                                                    {{trans('labels.submittals.version_no')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                    </a>
                                                </th>
                                                <th>
                                                    {{trans('labels.submittals.cycle_no')}}
                                                </th>
                                                <th>
                                                    <?php
if (Input::get('sort') == 'mf_number' && Input::get('order') == 'asc') {
	
	$url = Request::url().'?sort=mf_number&order=desc';
	
}
else {
	
	$url = Request::url().'?sort=mf_number&order=asc';
	
}

$url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';

$url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';

$url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';

$url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';

$url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';

$url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';

?>
                                                    {{trans('labels.master_format_number_and_title')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                    </a>
                                                </th>
                                                <th>
                                                    <?php
if (Input::get('sort') == 'name' && Input::get('order') == 'asc') {
	
	$url = Request::url().'?sort=name&order=desc';
	
}
else {
	
	$url = Request::url().'?sort=name&order=asc';
	
}

$url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';

$url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';

$url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';

$url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';

$url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';

$url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';

?>
                                                    {{trans('labels.submittals.name')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                    </a>
                                                </th>
                                                <th>
                                                    <?php
if (Input::get('sort') == 'subcontractor_name' && Input::get('order') == 'asc') {
	
	$url = Request::url().'?sort=subcontractor_name&order=desc';
	
}
else {
	
	$url = Request::url().'?sort=subcontractor_name&order=asc';
	
}

$url .= !empty(Input::get('master_format_search'))?'&master_format_search='.Input::get('master_format_search'):'';

$url .= !empty(Input::get('master_format_id'))?'&master_format_id='.Input::get('master_format_id'):'';

$url .= !empty(Input::get('supplier_subcontractor'))?'&supplier_subcontractor='.Input::get('supplier_subcontractor'):'';

$url .= !empty(Input::get('sub_id'))?'&sub_id='.Input::get('sub_id'):'';

$url .= !empty(Input::get('name'))?'&name='.Input::get('name'):'';

$url .= !empty(Input::get('status'))?'&status='.Input::get('status'):'';

?>
                                                    {{trans('labels.supplier_subcontractor')}}
                                                    <a href="{{$url}}"><i class="glyphicon glyphicon-sort" style="display:inline-block"></i>
                                                    </a>
                                                </th>
                                                <th>
                                                    {{trans('labels.submittals.received_from_subcontractor')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.submittals.sent_for_approval')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.submittals.received_from_approval')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.submittals.sent_to_subcontractor')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.status')}}
                                                </th>
                                                <th>
                                                    {{trans('labels.files.download')}}
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($submittals as $item)
                                            <tr>
                                                <td class="text-center">
                                                    <input type="checkbox" name="subm_id[]" value="{{$item->id}}" class="submittal">
                                                </td>
                                                <td>{{$item->number}}</td>
                                                <td>{{$item->version_cycle_no}}</td>
                                                <td>{{$item->mf_number." - ".$item->mf_title}}</td>
                                                <td>
                                                    <a href="{{URL('projects/'.$project->id.'/shared/submittals/'.$item->id)}}" class="">{{$item->name}}</a></td>
                                                    <td>{{$item->subcontractor_name}}</td>
                                                    <td>
                                                        @if($item->version_rec_sub != 0)
                                                        {{date("m/d/Y", strtotime($item->version_rec_sub))}}
                                                        @if($item->version_sent_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_sent_appr))) - strtotime(date("m/d/Y", strtotime($item->version_rec_sub)));
?>
                                                        @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_sub)));
?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($item->version_sent_appr != 0)
                                                        {{date("m/d/Y", strtotime($item->version_sent_appr))}}
                                                        @if($item->version_rec_appr != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_rec_appr))) - strtotime(date("m/d/Y", strtotime($item->version_sent_appr)));
?>
                                                        @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_sent_appr)));
?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($item->version_rec_appr != 0)
                                                        {{date("m/d/Y", strtotime($item->version_rec_appr))}}
                                                        @if($item->version_subm_sent_sub != 0)
                                                        <?php $dateDiff = strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub))) - strtotime(date("m/d/Y", strtotime($item->version_rec_appr)));
?>
                                                        @else
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_rec_appr)));
?>
                                                        @endif
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if(!is_null($item->version_subm_sent_sub) && $item->version_subm_sent_sub != 0)
                                                        {{date("m/d/Y", strtotime($item->version_subm_sent_sub))}}
                                                        @if(!in_array($item->version_status_short_name, ['APP','AAN']))
                                                        <?php $dateDiff = time() - strtotime(date("m/d/Y", strtotime($item->version_subm_sent_sub)));
?>
                                                        ({{floor($dateDiff / (60 * 60 * 24))}})
                                                        @endif
                                                        @endif
                                                    </td>
                                                    <td>{{$item->version_status_short_name}}</td>
                                                    <td>
                                                        @if(isset($item->submittals_versions) && count($item->submittals_versions) > 0)
                                                            <?php (count($item->submittals_versions) > 0) ? $lastRevision = count($item->submittals_versions) - 1 : $lastRevision = 0 ?>
                                                            @if(count($item->submittals_versions[$lastRevision]->transmittalSubmSentFile) > 0 || count($item->submittals_versions[$lastRevision]->transmittalSentFile) > 0)
                                                                @if(count($item->submittals_versions[$lastRevision]->transmittalSubmSentFile) > 0 &&
                                                                   !empty($item->submittals_versions[$lastRevision]->subm_sent_sub) &&
                                                                   $item->submittals_versions[$lastRevision]->subm_sent_sub != '0000-00-00')
                                                                        <p class="transmittal-paragraph">
                                                                            <a class="download transmittal"
                                                                               href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                            <input type="hidden" class="s3FilePath" id="{{$item->submittals_versions[$lastRevision]->transmittalSubmSentFile[0]->id}}"
                                                                                   value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/transmittals/'.$item->submittals_versions[$lastRevision]->transmittalSubmSentFile[0]->file_name}}">
                                                                        </p>
                                                                @elseif(count($item->submittals_versions[$lastRevision]->transmittalSentFile) > 0 &&
                                                                    !empty($item->submittals_versions[$lastRevision]->sent_appr) &&
                                                                    $item->submittals_versions[$lastRevision]->sent_appr != '0000-00-00' &&
                                                                   (empty($item->submittals_versions[$lastRevision]->subm_sent_sub) ||
                                                                   $item->submittals_versions[$lastRevision]->subm_sent_sub == '0000-00-00') &&
                                                                   (empty($item->submittals_versions[$lastRevision]->rec_appr) ||
                                                                   $item->submittals_versions[$lastRevision]->rec_appr == '0000-00-00'))
                                                                        <p class="transmittal-paragraph">
                                                                            <a class="download transmittal"
                                                                               href="javascript:;">{{trans('labels.files.transmittal')}}</a>
                                                                            <input type="hidden" class="s3FilePath" id="{{$item->submittals_versions[$lastRevision]->transmittalSentFile[0]->id}}"
                                                                                   value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/transmittals/'.$item->submittals_versions[$lastRevision]->transmittalSentFile[0]->file_name}}">
                                                                        </p>
                                                                @endif
                                                            @endif
                                                            @if($item->submittals_versions[$lastRevision]->download_file == true)
                                                                @if(!is_null($item->submittals_versions[$lastRevision]->file) && count($item->submittals_versions[$lastRevision]->file))
                                                                    @foreach($item->submittals_versions[$lastRevision]->file as $file)
                                                                        @if($file->version_date_connection == $item->submittals_versions[$lastRevision]->file_status)
                                                                            <p class="transmittal-paragraph">
                                                                                <a class="download transmittal" href="javascript:;">{{trans('labels.files.file')}}</a>
                                                                                <input type="hidden" class="s3FilePath" id="{{$file->id}}" value="{{'company_'.$project->comp_id.'/project_'.$project->id.'/submittals/'.$file->file_name}}">
                                                                            </p>
                                                                        @endif
                                                                    @endforeach
                                                                @endif
                                                            @endif
                                                        @endif
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="pull-right">
                                        <?php echo $submittals->appends([
'sort'=>Input::get('sort'),
'order'=>Input::get('order'),
'master_format_search'=>Input::get('master_format_search'),
'master_format_id'=>Input::get('master_format_id'),
'supplier_subcontractor'=>Input::get('supplier_subcontractor'),
'sub_id'=>Input::get('sub_id'),
'status'=>Input::get('status'),
])->render();
?>
                                    </div>
                                </div>
                            </div>
                            @else
                            <p class="text-center">{{trans('labels.no_records')}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('popups.alert_popup')
        @endsection