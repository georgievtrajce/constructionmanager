@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.contracts')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/contracts')}}" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
                <li class="cm-trail-item active"><a href="{{Request::url()}}" class="cm-trail-link">{{trans('labels.contract.files')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_contracts'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @include('errors.list')
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @if ($files && sizeof($files))
                    <h4>Files</h4>
                    <!-- <hr> -->
                    <div class="cf">
                        <div class="table-responsive">
                            <table class="table table-hover table-bordered cm-table-compact">
                                <thead>
                                    <tr>
                                        <th>{{trans('labels.file.name')}}</th>
                                        <th>{{trans('labels.file.number')}}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($i=0; $i<sizeof($files); $i++)
                                    <tr>
                                        <td>{{$files[$i]->name}}</td>
                                        <td>@if (isset($files[$i]->number)){{$files[$i]->number}} @endif</td>
                                        <td>
                                            @if (isset($files[$i]->file_name))
                                            <a class="download" href="javascript:;">{{trans('labels.files.download')}}</a>
                                            <input type="hidden" class="s3FilePath" id="{{$files[$i]->f_id}}" value="{{'company_'.$files[$i]->comp_id.'/project_'.$project->id.'/contracts/contract_'.$files[$i]->contr_id.'/'.$files[$i]->file_name}}">
                                            @endif
                                        </td>
                                    </tr>
                                    @endfor
                                </tbody>
                            </table>
                        </div>
                    </div>
                    @else
                    <div>
                        <p class="text-center">{{trans('labels.no_files')}}</p>
                    </div>
                    @endif
                </div>
            </div>
            @endsection