@extends('layouts.master')
@section('content')
<div class="container-fluid container-inset">
    <div class="row">
        <div class="col-md-12">
            <h1 class="cm-heading">
            {{trans('labels.shared.contracts')}}
            <ul class="cm-trail">
                <li class="cm-trail-item"><a href="{{URL('projects/'.$project->id.'/shared/info')}}" class="cm-trail-link">{{trans('labels.Project')}}: {{$project->name}}</a></li>
                <li class="cm-trail-item active"><a href="{{URL('projects/'.$project->id.'/shared/contracts')}}" class="cm-trail-link">{{trans('labels.contract.plural')}}</a></li>
            </ul>
            </h1>
        </div>
    </div>
    @include('projects.partials.tabs', array('activeTab' => 'shared'))
    <div class="panel">
        <div class="panel-body panel-inner-tabs">
            @include('projects.partials.sharedTabs', array('activeTab' => 'shared_contracts'))
            <div class="panel panel-default">
                <div class="panel-body">
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    @if ($contracts && sizeof($contracts))
                    {!! Form::open(['method'=>'GET','url'=>'/projects/'.$project->id.'/shared/contracts']) !!}
             <!--        <div class="cm-filter cf">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {!! Form::label('sort', trans('labels.sort_by').':') !!}
                                    {!! Form::select('sort',['id'=>'ID', 'company_name' => trans('labels.contract.company_name'),'number' => trans('labels.contract.number'),' value'=>trans('labels.contract.value')],Input::get('sort')) !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {!! Form::label('sort', trans('labels.order_by').':') !!}
                                    {!! Form::select('order', array('asc' => trans('labels.ascending'), 'desc' => trans('labels.descending')), Input::get('order')) !!}
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    {!! Form::label('filter', ' &nbsp;') !!}
                                    {!! Form::submit(trans('labels.sort'), ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        </div>
                    </div> -->
                    {!! Form::close() !!}
                    @else
                    <div>
                        <p class="text-center">{{trans('labels.no_records')}}</p>
                    </div>
                    @endif
                    @if(Request::segment(3) == 'shared')
                    @include('projects.contracts.partials.shared-table', $contracts)
                    @else
                    @include('projects.contracts.partials.table', $contracts)
                    @endif
                </div>
            </div>
            @endsection