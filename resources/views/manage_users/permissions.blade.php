@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.manage_users.user_permissions')}}
                <small class="cm-heading-sub">{{trans('labels.name').': '.$user->name}}</small>
                <small class="cm-heading-sub">{{trans('labels.manage_users.user').': '.$user->email}}</small>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        @if (Session::has('flash_notification.message'))
                        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{ Session::get('flash_notification.message') }}
                        </div>
                        @endif
                        <div class="col-md-12">
                            <div class="container-fluid">
                                <div class="row">
                                    <form role="form" action="{{URL::to('manage-users/'.$user->id.'/permissions')}}" accept-charset="UTF-8" method="POST">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" name="select_all" id="select_all_permissions"> {{trans('labels.select_all')}}
                                            </label>
                                        </div>
                                        <!--  <h3>{{trans('labels.manage_users.modules')}}</h3> -->
                                        @if(count($user->permissions))
                                            <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th style="width: 300px;">{{trans('labels.manage_users.module')}}</th>
                                                    <th>{{trans('labels.manage_users.read')}}</th>
                                                    <th>{{trans('labels.manage_users.write')}}</th>
                                                    <th>{{trans('labels.manage_users.delete')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=0; ?>
                                                @foreach($user->permissions as $permission)
                                                    @if($permission->entity_type == 1)
                                                    <?php
                                                    if($permission->module->name == 'Address Book' || $permission->module->name == 'Custom Categories')
                                                    {
                                                        $mainModuleReadClass = 'main-module-read ';
                                                    } else {
                                                        $mainModuleReadClass = '';
                                                    }
                                                    $projectEventClass = $permission->module->name == 'Projects' ? 'project-event ' : '';
                                                    if($permission->module->name == 'Projects' || $permission->module->name == 'Address Book' || $permission->module->name == 'Custom Categories')
                                                    {
                                                        $readPermissionsClass = '';
                                                        $eventPermissionsClass = '';
                                                        $mainModuleEventClass = 'main-module-event ';
                                                    } else {
                                                        $readPermissionsClass = 'read-permission ';
                                                        $eventPermissionsClass = 'event-permissions ';
                                                        $mainModuleEventClass = '';
                                                    }
                                                    if($permission->module->display_name == 'all-projects-modules')
                                                    {
                                                        $allProjectsModulesClass = 'all-projects-modules ';
                                                    } else {
                                                        $allProjectsModulesClass = '';
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td>{{$permission->module->name}}</td>
                                                        <td><input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control permission mandatory-permissions <?php echo $allProjectsModulesClass; echo ($permission->module->name == 'Address Book') ? 'address-book-read ' : ''; echo ($permission->module->name == 'Projects') ? 'projects-module ' : ''; echo $readPermissionsClass; echo $mainModuleReadClass; ?>" name="module_read[{{$i}}]"></td>
                                                        <td><input <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control permission <?php echo $allProjectsModulesClass; echo $projectEventClass; echo $eventPermissionsClass; echo ($permission->module->name == 'Address Book') ? 'address-book-event ' : ''; echo $mainModuleEventClass; ?>" name="module_write[{{$i}}]"></td>
                                                        <td><input <?php echo ($permission->delete == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control permission <?php echo $allProjectsModulesClass; echo $projectEventClass; echo $eventPermissionsClass; echo ($permission->module->name == 'Address Book') ? 'address-book-event ' : ''; echo $mainModuleEventClass; ?>" name="module_delete[{{$i}}]"></td>
                                                        <input type="hidden" name="module_id[{{$i}}]" value="{{$permission->module->id}}">
                                                    </tr>
                                                    <?php $i++; ?>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                        @else
                                            <p>{{trans('labels.manage_users.permissions_warning')}}</p>
                                        @endif
                                    </div>
                                    <div class="row">
                                        {{-- <h3>{{trans('labels.project_files')}}</h3> --}}
                                        @if(count($user->permissions))
                                            <div class="table-responsive">
                                            <table class="table table-hover table-striped">
                                                <thead>
                                                    <tr> {{--
                                                        <th style="width: 300px;">{{trans('labels.project_files')}}</th>
                                                        <th>{{trans('labels.manage_users.read')}}</th>
                                                        <th>{{trans('labels.manage_users.write')}}</th>
                                                        <th>{{trans('labels.manage_users.delete')}}</th>
                                                        --}}
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=0; ?>
                                                    @foreach($user->permissions as $permission)
                                                        @if ($permission->entity_type == 2)
                                                        <tr>
                                                            <td>{{$permission->project_file->name}}</td>{{--
                                                            <td><input <?php echo ($permission->read == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control mandatory-permissions permission read-permission" name="project_file_read[{{$i}}]"></td>
                                                            <td><input <?php echo ($permission->write == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control event-permissions permission" name="project_file_write[{{$i}}]"></td>
                                                            <td><input <?php echo ($permission->delete == 1) ? 'checked' : ''; ?> type="checkbox" class="form-control event-permissions permission" name="project_file_delete[{{$i}}]"></td>
                                                            <input type="hidden" name="project_file_id[{{$i}}]" value="{{$permission->project_file->id}}">
                                                            --}}
                                                        </tr>
                                                        <?php $i++; ?>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class=" col-md-2 control-label" for="company_admin" style="padding-right: 0;">
                                                {{trans('labels.manage_users.make_this_user')}}
                                            </label>
                                            <div class="col-md-6" style="padding: 0">
                                                <input id="company_admin" type="checkbox" name="company_admin" @if($user->hasRole(Config::get('constants.roles.company_admin'))){{'checked'}}@endif>
                                                {{trans('labels.manage_users.company_admin')}}
                                                <br>
                                                <input id="project_admin" type="checkbox" name="project_admin" @if($user->hasRole(Config::get('constants.roles.project_admin'))){{'checked'}}@endif>
                                                {{trans('labels.manage_users.project_admin')}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-success pull-right">
                                                {{trans('labels.save')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('popups.alert_popup') @include('popups.delete_record_popup') @include('popups.approve_popup') @endsection