@extends('layouts.master') @section('content')
<div class="container">
    <!--<div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                User Details
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>-->
    <div class="panel">
        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> {{ Session::get('flash_notification.message')
                }}
            </div>
            @endif
            <form class="form form-horizontal" role="form" action="{{URL::to('manage-users')}}" accept-charset="UTF-8" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3>{{trans('labels.manage_users.user_details')}}</h3>
                <div class="form-group">
                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.name')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.email')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.title')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="title" value="{{ old('title') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.address_book.office_phone')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="office_phone" value="{{ old('office_phone') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">{{trans('labels.address_book.cell_phone')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="cell_phone" value="{{ old('cell_phone') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label">{{trans('labels.address_book.fax')}}</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="fax" value="{{ old('fax') }}">
                    </div>
                </div>
                @if(count($companyAddresses) > 0)
                <div class="form-group">
                    <label class="cm-control-required col-md-4 control-label">{{trans('labels.manage_users.user_address_office')}}</label>
                    <div class="col-md-6">
                        <select name="user_address_office">
                                <option value="">{{trans('labels.address_book.select_address')}}</option>
                                @foreach($companyAddresses as $companyAddress)
                                <option {{old('user_address_office') == $companyAddress->id ? 'selected' : ''}} value="{{ $companyAddress->id }}">{{ isset($companyAddress->office_title) ? $companyAddress->office_title : $companyAddress->city.', '.$companyAddress->state }}</option>
                                @endforeach
                            </select>
                    </div>
                </div>
                @else
                <div class="row">
                    <div class="form-group">
                        <label class="col-md-4 control-label">{{trans('labels.manage_users.user_address_office')}}</label>
                        <p class="small">{{trans('labels.manage_users.company_address_warning')}}</p>
                    </div>
                </div>
                @endif
                <style>
                    .radio label span.check {
                        left: 10px!important;
                    }
                </style>
                <div class="row">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-success pull-right">
                                    {{trans('labels.save')}}
                                    </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection