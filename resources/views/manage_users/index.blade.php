@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.manage_users.company_users')}}
                <small class="cm-heading-sub">{{trans('labels.total_records', ['number' => $companyUsers->total()])}}</small>
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
                <a href="{{URL('manage-users/create')}}" class="btn btn-success pull-right">{{trans('labels.manage_users.add_new_user')}}</a>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="row">
                <div class="pull-right">
                    <?php echo $companyUsers->render(); ?>
                </div>
            </div>
            @if($companyUsers->total() != 0)
            <div class="row">
                <div class="col-md-12">

                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                    <input type="hidden" value="{{URL('manage-users').'/'}}" id="form-url" />
                    <button disabled id="delete-button" class='btn btn-xs btn-danger pull-right' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                        {{trans('labels.delete')}}
                    </button>
                    {!! Form::close()!!}

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-hover table-bordered cm-table-compact">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('labels.name')}}</th>
                            <th>{{trans('labels.email')}}</th>
                            <th>{{trans('labels.title')}}</th>
                            <th>{{trans('labels.office')}}</th>
                            <th>{{trans('labels.manage_users.role')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($companyUsers as $item)
                        <tr>
                            <td width="20" class="text-center">
                                <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$item->id}}">
                            </td>
                            <td>
                                <a href="{{URL('manage-users/'.$item->id.'/edit')}}" class=""> {{$item->name}}</a>
                            </td>
                            <td>{{$item->email}}</td>
                            <td>{{$item->title}}</td>
                            <td>{{(isset($item->address))?$item->address->office_title:''}}</td>
                            <td>{{$item->roles()->first()->name}}</td>
                            <td>
                                @if(Auth::user()->hasRole('Company Admin'))
                                <a href="{{URL('manage-users/'.$item->id.'/permissions')}}" class="btn btn-info btn-sm pull-right mr5">{{trans('labels.permissions')}}</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>     
            </div>
            @else
            <p class="text-center">{{trans('labels.no_records')}}</p>
            @endif
        </div>
    </div>
</div>
@endsection
@include('popups.delete_record_popup')