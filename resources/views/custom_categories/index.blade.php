@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <h1 class="cm-heading">
            {{trans('labels.address_book.custom_categories')}}
            <small class="cm-heading-sub">
            {{trans('labels.total_records', ['number' => $customCategories->total()])}}
            </small>
            </h1>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">

                @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('delete', 'address-book')))
                    {!! Form::open(['method'=>'POST', 'class' => 'form-prevent pull-right', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                        <input type="hidden" value="{{'custom-categories/'}}" id="form-url"  />
                        <input name="_method" type="hidden" value="DELETE">
                        <button disabled id="delete-button" class='btn btn-danger pull-left' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                            {{trans('labels.delete')}}
                        </button>
                    {!! Form::close()!!}
                 @endif




                @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('write', 'address-book')))
                <a href="{{URL('custom-categories/create')}}" class="btn btn-success pull-right mr5">{{trans('labels.address_book.add_new_category_btn')}}</a>
                @endif

            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                </div>
            </div>
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            <div class="row">
                <div class="pull-right">
                    <?php echo $customCategories->render(); ?>
                </div>
            </div>
            @if($customCategories->total() != 0)
            <div class="table-responsive">
                <table class="table table-hover table-bordered cm-table-compact">
                    <thead>
                        <tr>
                            <th></th>
                            <th>{{trans('labels.name')}}</th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($customCategories as $item)
                        <tr>
                        <td width="20" class="text-center"><input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$item->id}}"></td>

                        <td>
                            @if (Auth::user()->hasRole('Company Admin') || Auth::user()->hasRole('Project Admin') || (Permissions::can('write', 'address-book')))
                            <a href="{{URL('custom-categories/'.$item->id.'/edit')}}">{{$item->name}}</a></td>
                            @endif

                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
                @else
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center mb0">No records</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
    @endsection
@include('popups.delete_record_popup')