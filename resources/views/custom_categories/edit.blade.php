@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-5">
            <h1 class="cm-heading">
            {{trans('labels.address_book.edit_custom_category')}}
            </h1>
        </div>
        <div class="col-sm-12 col-md-7">
            <div class="cm-pull-right">
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('flash_notification.message'))
            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
            </div>
            @endif
            {!! Form::open(['method'=> 'PUT', 'url'=>URL('custom-categories/'.$category->id)]) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-6">
                    <label>{{trans('labels.address_book.category_name')}} *</label>
                    <input type="text" class="form-control" name="name" value="{{ $category->name }}">
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success cm-btn-fixer">
                    {{trans('labels.update')}}
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection