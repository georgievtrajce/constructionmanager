<!-- Dialog show event handler -->
<script type="text/javascript">

</script>
<!-- Modal Dialog -->
<div class="modal fade" id="uploadS3PopUp" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">{{trans('labels.tiny_mce.upload_image_title')}}</h4>
            </div>
            <div class="modal-body">
                <form class="tiny-image-form" role="form" method="POST" action="#" enctype="multipart/form-data">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-8">
                            <label>
                                {{trans('labels.tiny_mce.image_field_text')}}:
                            </label>

                            <!-- upload select input -->
                            <div class="input-group">
                              <input type="text" class="form-control" readonly title="File name" placeholder="File name">
                              <label class="input-group-btn">
                                <span class="btn btn-primary">
                                  <input type="file" style="display: none;" name="tiny_file" id="tiny_file" accept="image/*">
                                  <span class="glyphicon glyphicon-upload"></span>
                                </span>
                              </label>
                            </div>

                            </div>
                            <div class="col-md-4">
                            <label> &nbsp;</label>
                                <div id="tiny_file_wait" style="display: none;">
                                    <img class="pull-left" src="{{URL('/img/pleasewait.gif')}}" alt="" width="34px">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">{{trans('labels.tiny_mce.cancel')}}</button>
                <button type="button" class="btn btn-success" id="confirm_tiny_upload">{{trans('labels.tiny_mce.yes')}}</button>
            </div>
        </div>
    </div>
</div>