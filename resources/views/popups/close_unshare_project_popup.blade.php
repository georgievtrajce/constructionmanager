<!-- Modal Dialog -->
<div class="modal fade" id="confirmCloseUnshare" role="dialog" aria-labelledby="confirmCloseUnshareLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <p>The Project will be unshared with all the Companies, please confirm your actions! Add the winning bidder at the Companies module to continue sharing the Project.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="confirm_close_unshare">Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal">Yes</button>
            </div>
        </div>
    </div>
</div>