<!-- Modal Dialog -->
<div class="modal fade" id="approveAction" role="dialog" aria-labelledby="approveActionLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Alert</h4>
            </div>
            <div class="modal-body">
                <p></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="cancel_action" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success" id="approve_action">Yes</button>
            </div>
        </div>
    </div>
</div>