<!-- Modal Dialog -->
<div class="modal fade" id="confirmUnshare" role="dialog" aria-labelledby="confirmUnshareLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Unshare Project</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to unshare this project?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success" id="confirm_unshare">Yes</button>
            </div>
        </div>
    </div>
</div>