@extends('layouts.master')
@section('content')
<div class="container">
 <div class="row">
        <div class="col-md-6 col-md-offset-3">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.send_invite')}}
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">


                            @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::open(['method'=> 'POST', 'url'=>'referral/send', 'class'=> 'form', 'role'=> 'form']) !!}


                                            <div class="form-group">
                                                {!! Form::label('email', trans('labels.email').':', ['class' => '']) !!}

                                                    {!! Form::text('email', Input::old('email'), ['class' => 'form-control']) !!}

                                            </div>
                                            <input type="hidden" name="company" value="{{Auth::user()->comp_id}}"/>



                                            <div class="form-group">

                                                    {!! Form::submit(trans('labels.send'),['class' => 'btn btn-success pull-right']) !!}

                                            </div>

                                        {!! Form::close() !!}
                                    </div>
                                    @include('errors.list')
                                </div>

                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection