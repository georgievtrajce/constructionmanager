@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">{{trans('labels.company_profile.edit_company_profile')}}</div>
                <div class="panel-body">


                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @if (Session::has('flash_notification.message'))
                            <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                {{ Session::get('flash_notification.message') }}
                            </div>
                            @endif
                            <form role="form" class="form-horizontal" action="{{URL('/company-profile/'.$company->id)}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.company_uac')}}</label>
                                    <div class="col-md-8">
                                        <span class="cm-form-ctrl">{{ $company->uac }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.expiry_date')}}</label>
                                    <div class="col-md-8">
                                        <span class="cm-form-ctrl">{{ Carbon::parse($company->subscription_expire_date)->format('m/d/Y') }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.subscription_type')}}</label>
                                    <div class="col-md-4">
                                        <span class="cm-form-ctrl">
                                            <?php
                                            if(isset($company->subscription_type)) {
                                                echo $company->subscription_type->name.' -  Storage: '.CustomHelper::formatByteSizeUnits($company->subscription_type->storage_limit);
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    @if((($company->subs_id != Config::get('subscription_levels.level-three')) && ($company->subs_id != Config::get('subscription_levels.sales-agent'))) && Auth::user()->hasRole('Company Admin'))
                                    <div class="col-md-3">
                                        <a href="{{URL('/company-profile/'.$company->id.'/change-subscription-level')}}" class="btn cm-btn-secondary btn-sm btn-beta">{{trans('labels.change')}}</a>
                                    </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.data_used')}}</label>
                                    <div class="col-md-8">
                                        <span class="cm-form-ctrl">{{ 'Storage used: '.CustomHelper::formatByteSizeUnits($usedStorage) }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.referral_points_title')}}</label>
                                    <div class="col-md-6">
                                        <div class="cm-form-ctrl">{{ $company->ref_points.' '.trans('labels.company_profile.points') }}</div>
                                        @if($company->subs_id != Config::get('subscription_levels.sales-agent'))
                                        <span class="cm-form-ctrl">{{ $referralPointsLabel }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.company_name')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="name" value="{{$company->name }}">
                                    </div>
                                </div>
                          <!--       <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.company_profile.company_logo')}}</label>
                                    <div class="col-md-6">
                                        <input type="file" name="logo">
                                    </div>
                                    @if($company->logo)
                                    <div class="col-md-6 col-md-offset-4">
                                        <img width="300" class="img-thumbnail" src="{{URL(Config::get('constants.s3_base_url').'/'.Config::get('filesystems.disks.s3.bucket').'/'.Auth::user()->company->logo)}}" alt="">
                                    </div>
                                    @endif
                                </div> -->
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-success pull-right">
                                        {{trans('labels.update')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <div class="cm-spacer-normal"></div>
                            <div class="container-fluid">
                                <div class="row cf">
                                    <h4 class="mb20">{{trans('labels.company_profile.company_addresses')}}</h4>
                                    <a href="{{URL('company-profile/'.$company->id.'/add-new-address')}}" class="btn btn-primary btn-sm pull-left">{{trans('labels.company_profile.add_address')}}</a>

                                    {!! Form::open(['method'=>'DELETE', 'class' => 'form-prevent', 'url'=>URL(''), 'id' => 'delete-form']) !!}
                                    <input type="hidden" value="{{URL('company-profile/'.$company->id.'/addresses').'/'}}" id="form-url" />
                                    <button disabled id="delete-button" class='btn btn-xs btn-danger pull-right ' type='submit' data-toggle="modal" data-target="#confirmDelete" data-title="Delete Record" data-message='{{trans('labels.global_delete_modal')}}'>
                                        {{trans('labels.delete')}}
                                    </button>
                                    {!! Form::close()!!}
                                </div>
                                @if(count($company->addresses) > 0)
                                <div class="row">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-bordered cm-table-compact">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>{{trans('labels.address_book.office_name')}}</th>
                                                    <th>{{trans('labels.Address')}}</th>
                                                    <th>{{trans('labels.city')}}</th>
                                                    <th>{{trans('labels.state')}}</th>
                                                    <th>{{trans('labels.zip')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($company->addresses as $item)
                                                <tr>
                                                <td  width="20" class="text-center">
                                                    <input type="checkbox" name="select_all" class="multiple-items-checkbox" data-id="{{$item->id}}">
                                                </td>
                                                    <td>
                                                        <a href="{{URL('company-profile/'.$company->id.'/addresses/'.$item->id.'/edit')}}" class=""> {{$item->office_title}}</a>
                                                    </td>
                                                    <td>{{$item->street}}</td>
                                                    <td>{{$item->city}}</td>
                                                    <td>{{$item->state}}</td>
                                                    <td>{{$item->zip}}</td>

                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @else
                                <p class="text-center">{{trans('labels.address_book.no_addresses')}}</p>
                                @endif
                            </div>


                </div>
            </div>
        </div>
        @endsection
        @include('popups.delete_record_popup')