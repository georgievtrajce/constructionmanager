@extends('layouts.master')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-5">
            <header class="cm-heading">
                {{trans('labels.company_profile.edit_company_address')}}
            </header>
        </div>
        <div class="col-md-7">
            <div class="cm-btn-group cm-pull-right cf">
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-body">
            <div class="col-md-6">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (Session::has('flash_notification.message'))
                <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ Session::get('flash_notification.message') }}
                </div>
                @endif
                <!--     <div class="row">
                    <div class="col-md-3">
                        <a href="{{URL('/company-profile/'.$companyId)}}" class="btn btn-default
                        btn-sm">{{'<< '.trans('labels.company_profile.back_to_company_profile')}}</a>
                    </div>
                </div> -->
                <div class="row">
                    {!! Form::open(['files'=>true, 'class'=>'form', 'method'=> 'PUT', 'url'=>URL('/company-profile/'.$companyId.'/addresses/'.$addressId)]) !!}
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>{{trans('labels.address_book.office_name')}}</label>
                        <input type="text" class="form-control" name="office_title" value="{{ $address->office_title }}">
                    </div>
                    <div class="form-group">
                        <label>{{trans('labels.Address')}}</label>
                        <input type="text" class="form-control" name="ca_street" value="{{ $address->street }}">
                    </div>
                    <div class="form-group">
                        <label>{{trans('labels.city')}}</label>
                        <input type="text" class="form-control" name="ca_town" value="{{ $address->city }}">
                    </div>
                    <div class="form-group">
                        <label>{{trans('labels.state')}}</label>
                        <input type="text" class="form-control" name="ca_state" value="{{ $address->state }}">
                    </div>
                    <div class="form-group">
                        <label>{{trans('labels.zip')}}</label>
                        <input type="text" class="form-control" name="ca_zip" value="{{ $address->zip }}">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right">
                        {{trans('labels.update')}}
                        </button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection