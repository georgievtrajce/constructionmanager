@extends('layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">{{trans('labels.company_profile.add_company_address')}}</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if (Session::has('flash_notification.message'))
                    <div class="alert alert-{{ Session::get('flash_notification.level') }}">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{ Session::get('flash_notification.message') }}
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-md-3">
                            <a href="{{URL('/company-profile/'.$companyId)}}" class="btn btn-default
                            btn-sm cm-btn-secondary">{{'<< '.trans('labels.company_profile.back_to_company_profile')}}</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form role="form" action="{{URL('/company-profile/'.$companyId.'/addresses')}}" accept-charset="UTF-8" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.address_book.office_name')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="office_title" value="{{ old('office_title') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.Address')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="ca_street" value="{{ old('ca_street') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.city')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="ca_town" value="{{ old('ca_town') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.state')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="ca_state" value="{{ old('ca_state') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">{{trans('labels.zip')}}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" name="ca_zip" value="{{ old('ca_zip') }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{trans('labels.save_address')}}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection