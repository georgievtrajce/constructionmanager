@extends('layouts.tabs')
@section('title')
<div class="row">
    <div class="col-md-12">
        <div class="cm-error-page">
            @if($code == 404)
                <h2 class="cm-error-page-question">Are you lost?</h2>
                <div class="cm-error-page-img">
                    <img src="/img/maze.svg" class="img-responsive" alt="logo">
                </div>
            @else
                <h2 class="cm-error-page-question">{{trans('labels.whoops')}}</h2>
                <div class="cm-error-page-img-500">
                    <img src="/img/500.svg" class="img-responsive" alt="logo">
                </div>
            @endif
            <h1 class="cm-error-page-title">{{$code}}</h1>
            <h2>{{$errorMessageLarge}}</h2>
            <p class="cm-error-page-subtitle">{{$errorMessageSmall}}</p>
        </div>
    </div>
</div>
@endsection