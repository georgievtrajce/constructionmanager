@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-sm-12">
                        Please confirm your account first. Log out from the application and click the link for confirmation of your account that we sent to you.
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection