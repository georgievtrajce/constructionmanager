@extends('layouts.tabs')
@section('title')
<div class="row">
    <div class="col-md-12">
        <div class="cm-error-page">
            @if($code == 404)
                <h2 class="cm-error-page-question">Are you lost?</h2>
                <div class="cm-error-page-img">
                    <img src="/img/maze.svg" class="img-responsive" alt="logo">
                </div>
            @else
                <h2 class="cm-error-page-question">{{trans('labels.whoops')}}</h2>
                <div class="cm-error-page-img-500">
                    <img src="/img/500.svg" class="img-responsive" alt="logo">
                </div>
            @endif
            <h1 class="cm-error-page-title">{{$code}}</h1>
            <p class="cm-error-page-subtitle">{{$errorMessage}}</p>

            <div class="cf mt20">
                <a href="{{URL('/home')}}" class="btn btn-lg btn-primary">{{trans('labels.back_dashboard')}}</a>
                <a href="#" onclick="history.go(-1);" class="btn btn-lg btn-primary">{{trans('labels.back_to_previous_page')}}</a>
            </div>
        </div>
    </div>
</div>
@endsection