<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="google-site-verification" content="HKS2YAXsZy2TsKyDonJAvCPwbFuz_eD6t4CG8mVHKeY" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Cloud PM</title>

	<link href={{URL::asset('css/app.css')}} rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700,400,600" />
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<nav class="navbar navbar-fixed-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle Navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">
				<span class="navbar-brand-module navbar-brand-1">CMS</span>
				<span class="navbar-brand-module navbar-brand-2">Construction</span>
				<span class="navbar-brand-module navbar-brand-3">Manager</span>
			</a>
		</div>

		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"></div>
	</div>
</nav>
<style>
	.title {
		width: 100%;
		text-align: center;
		font-size: 72px;
		margin-bottom: 40px;
	}
</style>
<div class="cm-page-content">
	<div class="row">
		<div class="col-md-12">
			<div class="title">Coming soon...</div>
		</div>
	</div>
</div>

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/tinymce/4.2.0/tinymce.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.2.10.min.js"></script>
<script src="{{URL::asset('js/all.js')}}"></script>

</body>
</html>