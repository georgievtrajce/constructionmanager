@extends('layouts.tabs')

@section('title')
Subscription Levels
@endsection

@section('tabsContent')
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-body">
                @if (sizeof($subscriptionLevels))
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>{{trans('labels.subscription_level')}}</th>
                                <th>{{trans('labels.referral_points_needed')}}</th>
                                <th>{{trans('labels.referral_points')}}</th>
                                <th>{{trans('labels.upload_limit')}}</th>
                                <th>{{trans('labels.download_limit')}}</th>
                                <th>{{trans('labels.storage_limit')}}</th>
                                <th>{{trans('labels.amount')}}</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($subscriptionLevels as $subscriptionLevel)
                            <tr>
                                <td>{{$subscriptionLevel->name}}</td>
                                <td>{{$subscriptionLevel->ref_points_needed}}</td>
                                <td>{{$subscriptionLevel->ref_points}}</td>
                                <td>{{CustomHelper::formatByteSizeUnits($subscriptionLevel->upload_limit)}}</td>
                                <td>{{CustomHelper::formatByteSizeUnits($subscriptionLevel->download_limit)}}</td>
                                <td>{{CustomHelper::formatByteSizeUnits($subscriptionLevel->storage_limit)}}</td>
                                <td>{{'$'.$subscriptionLevel->amount}}</td>
                                <td>
                                    <a href="{{URL('subscription-levels/'.$subscriptionLevel->id.'/edit')}}" class="btn cm-btn-secondary btn-sm">{{trans('labels.edit')}}</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                <p>{{trans('labels.no_records')}}</p>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
