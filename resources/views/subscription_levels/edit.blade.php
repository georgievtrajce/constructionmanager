@extends('layouts.tabs')
@section('title')
Edit Subscription Level <small class="cm-heading-suffix">{{$subscriptionLevel->name}}</small>
@endsection
@section('tabsContent')
<div class="row">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <p><strong>{{trans('labels.whoops')}}</strong> {{trans('labels.input_problems')}}</p>
          <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if (Session::has('flash_notification.message'))
        <div class="alert alert-{{ Session::get('flash_notification.level') }}">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          {{ Session::get('flash_notification.message') }}
        </div>
        @endif
        {!! Form::open(['files'=>true, 'method'=> 'POST', 'url'=>URL('/subscription-levels/'.$subscriptionLevel->id.'/update'),'id' => 'subscriptionLevels', 'role' => 'form']) !!}
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label>{{trans('labels.referral_points_needed')}}</label>
              <input type="text" class="form-control" name="ref_points_needed" value="{{$subscriptionLevel->ref_points_needed}}">
            </div>
            <div class="form-group">
              <label>{{trans('labels.referral_points')}}</label>
              <input type="text" class="form-control" name="ref_points" value="{{$subscriptionLevel->ref_points}}">
            </div>
          </div>
          <div class="col-md-6">
            <div class="">
              <label>{{trans('labels.upload_limit')}}</label>
              <div class="row">
                <div class="col-md-7">
                  <div class="form-group">
                    <input type="text" class="form-control" name="upload_limit" value="{{CustomHelper::returnByteSizeNumber($subscriptionLevel->upload_limit)}}">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <select name="upload_unit">
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->upload_limit) == 'GB') ? 'selected' : ''; ?> value="GB">GB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->upload_limit) == 'MB') ? 'selected' : ''; ?> value="MB">MB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->upload_limit) == 'KB') ? 'selected' : ''; ?> value="KB">KB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->upload_limit) == 'bytes') ? 'selected' : ''; ?> value="bytes">bytes</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="">
              <label>{{trans('labels.download_limit')}}</label>
              <div class="row">
                <div class="col-md-7">
                  <div class="form-group">
                    <input type="text" class="form-control" name="download_limit" value="{{CustomHelper::returnByteSizeNumber($subscriptionLevel->download_limit)}}">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <select name="download_unit">
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->download_limit) == 'GB') ? 'selected' : ''; ?> value="GB">GB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->download_limit) == 'MB') ? 'selected' : ''; ?> value="MB">MB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->download_limit) == 'KB') ? 'selected' : ''; ?> value="KB">KB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->download_limit) == 'bytes') ? 'selected' : ''; ?> value="bytes">bytes</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="">
              <label>{{trans('labels.storage_limit')}}</label>
              <div class="row">
                <div class="col-md-7">
                  <div class="form-group">
                    <input type="text" class="form-control" name="storage_limit" value="{{CustomHelper::returnByteSizeNumber($subscriptionLevel->storage_limit)}}">
                  </div>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <select name="storage_unit">
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->storage_limit) == 'GB') ? 'selected' : ''; ?> value="GB">GB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->storage_limit) == 'MB') ? 'selected' : ''; ?> value="MB">MB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->storage_limit) == 'KB') ? 'selected' : ''; ?> value="KB">KB</option>
                      <option <?php echo (CustomHelper::returnByteSizeUnit($subscriptionLevel->storage_limit) == 'bytes') ? 'selected' : ''; ?> value="bytes">bytes</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label>{{trans('labels.amount')}}</label>
              <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" name="amount" value="{{ number_format($subscriptionLevel->amount,2) }}">
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-success pull-right">
            {{trans('labels.update')}}
          </button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection
