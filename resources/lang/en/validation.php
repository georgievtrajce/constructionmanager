<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute must be accepted.",
	"active_url"           => "The :attribute is not a valid URL.",
	"after"                => "The :attribute must be a date after :date.",
	"alpha"                => "The :attribute may only contain letters.",
	"alpha_dash"           => "The :attribute may only contain letters, numbers, and dashes.",
	"alpha_num"            => "The :attribute may only contain letters and numbers.",
	"array"                => "The :attribute must be an array.",
	"before"               => "The :attribute must be a date before :date.",
	"between"              => [
		"numeric" => "The :attribute must be between :min and :max.",
		"file"    => "The :attribute must be between :min and :max kilobytes.",
		"string"  => "The :attribute must be between :min and :max characters.",
		"array"   => "The :attribute must have between :min and :max items.",
	],
	"boolean"              => "The :attribute field must be true or false.",
	"confirmed"            => "The :attribute confirmation does not match.",
	"date"                 => "The :attribute is not a valid date.",
	"date_format"          => "The :attribute does not match the format :format.",
	"different"            => "The :attribute and :other must be different.",
	"digits"               => "The :attribute must be :digits digits.",
	"digits_between"       => "The :attribute must be between :min and :max digits.",
	"email"                => "The :attribute must be a valid email address.",
	"filled"               => "The :attribute field is required.",
	"exists"               => "The selected :attribute is invalid.",
	"image"                => "The :attribute must be an image.",
	"in"                   => "The selected :attribute is invalid.",
	"integer"              => "The :attribute must be an integer.",
	"ip"                   => "The :attribute must be a valid IP address.",
	"max"                  => [
		"numeric" => "The :attribute may not be greater than :max.",
		"file"    => "The :attribute may not be greater than :max kilobytes.",
		"string"  => "The :attribute may not be greater than :max characters.",
		"array"   => "The :attribute may not have more than :max items.",
	],
	"mimes"                => "The :attribute must be a file of type: :values.",
	"min"                  => [
		"numeric" => "The :attribute must be at least :min.",
		"file"    => "The :attribute must be at least :min kilobytes.",
		"string"  => "The :attribute must be at least :min characters.",
		"array"   => "The :attribute must have at least :min items.",
	],
	"not_in"               => "The selected :attribute is invalid.",
	"numeric"              => "The :attribute must be a number.",
	"regex"                => "The :attribute format is invalid.",
	"required"             => "The :attribute field is required.",
	"required_if"          => "The :attribute field is required when :other is :value.",
	"required_with"        => "The :attribute field is required when :values is present.",
	"required_with_all"    => "The :attribute field is required when :values is present.",
	"required_without"     => "The :attribute field is required when :values is not present.",
	"required_without_all" => "The :attribute field is required when none of :values are present.",
	"same"                 => "The :attribute and :other must match.",
	"size"                 => [
		"numeric" => "The :attribute must be :size.",
		"file"    => "The :attribute must be :size kilobytes.",
		"string"  => "The :attribute must be :size characters.",
		"array"   => "The :attribute must contain :size items.",
	],
	"unique"               => "The :attribute has already been taken.",
	"url"                  => "The :attribute format is invalid.",
	"timezone"             => "The :attribute must be a valid zone.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' =>  [
        'g-recaptcha-response' => [
            'required' => 'Your response to Google Recaptcha is required.',
            'recaptcha' => 'Your response to Google Recaptcha is invalid.',
        ],
		'terms_agree' => [
			'required' => 'You must first agree with our terms of conditions and privacy policy.',
		],
		'name' => [
			'required' => 'Please insert a name.',
			'alpha_dash' => 'Please insert a name with alphanumeric values.'
		],
		'number' => ['required' => 'Please insert a number.'],
		'title' => ['required' => 'Please insert a title.'],
		'blogContent' => ['required' => 'Please insert content for the blog.'],
		'ab_id' => [
			'required' => 'Please choose an existing address book contact.',
			'exists' => 'Please choose an existing address book contact.',
		],
		'mf_number' => [
			'required' => 'Please insert a spec number.',
		],
		'mf_title' => [
			'required' => 'Please insert a spec title.',
		],
		'master_format_number' => [
			'required' => 'Please insert a spec number.'
		],
		'master_format_title' => ['required' => 'Please insert a spec title.'],
		'contract_file' => ['required' => 'Please insert a contract file.'],
		'contr_ab_id' => ['required' => 'Please insert a company name.'],
		'prop_addr' => ['required' => 'Please select a location.'],
		'addr_id' => ['required' => 'Please select a location.'],
		'cont_id' => ['required' => 'Please select a contact person.'],
        'ab_cont_id' => ['required' => 'Please select a contact person.'],
		'contr_price' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for price.'],
		'unit_price' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for unit price.'],
		'value' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for estimated value.'],
		'proposal_1' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for proposal #1 price.'],
		'proposal_2' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for proposal #2 price.'],
		'proposal_final' => ['regex' => 'Please insert an integer or decimal value with 2 decimal digits for estimated value.'],
		'proposal_file' => ['required' => 'Please insert a proposal file.'],
		'mas_sub_id' => ['exists' => 'Please import a valid subcontractor'],
		'position' => ['required' => 'Please insert a position.'],
		'er_status_id' => ['exists' => 'Please select valid expediting report status.'],
		'cost' => [
			'regex'=> 'Cost has invalid format.'
		],
		'status_id' => [
			'required' => 'Please insert a status.',
			'required_if' => 'Status field is required when submittals needed is present.'
		],
		'subm_needed' => [
			'required' => 'Please insert if submittals are required.',
			'required_with' => 'The submittal needed field is required when status is present.',
		],
		'need_by' => [
			'required' => 'Please insert a date.',
			'date' => 'Need by is not a valid date.'
		],
		'start_date' => ['date' => 'Please insert a date for the start date.'],
		'end_date' => ['date' => 'Please insert a date for the end date.'],
		'zip' => ['numeric' => 'Please insert a numeric value for the zip number.'],
		'custom_categories' => ['required_without' => 'Please check at least one category.'],

		//Addresses validation messages
		'office_title' => ['required' => 'Please insert office name.'],
		'ca_number' => [
			'required' => 'Please insert address number.',
			'max' => 'The number may not be greater than 45 characters.',
		],
		'ca_street' => [
			'required' => 'Please insert address.',
			'max' => 'The address may not be greater than 255 characters.',
		],
		'ca_town' => [
			'required' => 'Please insert city.',
			'max' => 'The city may not be greater than 45 characters.',
		],
		'ca_state' => [
			'required' => 'Please insert state.',
			'max' => 'The state may not be greater than 45 characters.',
		],
		'ca_zip' => [
			'required' => 'Please insert zip code.',
			'max' => 'The zip code may not be greater than 10 characters.',
			'numeric' => 'The zip code must be a number.',
			'digits_between' => 'The zip code must be between 1 and 10 digits.',
		],

		//Contacts validation messages
		'cc_name' => [
			'required' => 'Please insert a contact name.',
			'max' => 'The name may not be greater than 255 characters.',
		],
		'cc_title' => [
			'required' => 'Please insert a contact title.',
			'max' => 'The title may not be greater than 255 characters.',
		],
		'cc_address_office_name' => ['required' => 'Please select an address/office.'],
		'cc_office_phone' => [
			'required' => 'Please insert an office phone.',
			'regex' => 'The office phone format is invalid.',
		],
		'cc_cell_phone' => [
			'required' => 'Please insert a cell phone.',
			'regex' => 'The cell phone format is invalid.',
		],
		'cc_fax' => [
			'required' => 'Please insert a fax.',
			'regex' => 'The fax format is invalid.',
		],
		'cc_email' => [
			'required' => 'Please insert an email address.',
			'email' => 'Please insert a valid email address.'
		],

		//submittals
		'file_name' => ['required' => 'Please select file.'],
		'file_id' => [
			'required' => 'Please upload file.',
			'numeric' => 'Please upload a proper file.',
			'exists' => 'Your file was not properly uploaded. Please uploaded again.',
		],
		'cycle_no' => ['required' => 'Please insert cycle number.'],
		'sub_id' => [
			'required' => 'Please import supplier/subcontractor from address book.',
			'required_without' => 'Please import supplier/subcontractor from address book.',
			'exists' => 'Please import valid supplier/subcontractor.',
		],
		'sub_office' => [
			'exists' => 'Please select valid subcontractor office.'
		],
		'sub_contact' => [
			'exists' => 'Please select valid subcontractor contact.'
		],
		'recipient_id' => [
			'required' => 'Please import recipient from address book.',
			'exists' => 'Please import valid recipient.',
		],
		'recipient_office' => [
			'exists' => 'Please select valid recipient office.'
		],
		'recipient_contact' => [
			'exists' => 'Please select valid recipient contact.'
		],
		'make_me_gc_id' => [
			'required_with' => "Your company can't processed properly. Please try again.",
			'exists' => 'This is not your account id.',
		],
		'general_contractor_id' => [
			'required' => 'Please import general contractor from address book.',
			'exists' => 'Please import valid general contractor.',
		],
		'rec_sub' => [
			'required' => 'Please insert received from subcontractor date.',
			'date' => 'Received from subcontractor is not a valid date.'
		],
		'sent_appr' => [
			'required' => 'Please insert sent for approval date.',
			'date' => 'Sent for approval is not a valid date.'
		],
		'rec_appr' => [
			'required' => 'Please insert received from approval date.',
			'date' => 'Received from approval is not a valid date.'
		],
		'subm_sent_sub' => [
			'required' => 'Please insert sent to subcontractor date.',
			'date' => 'Sent to subcontractor is not a valid date.'
		],
		'submittal_file' => ['required' => 'Please select submittal file.'],

		//project files
		'file_number' => ['required' => 'Please insert file number.'],

		'email' => [
			'required' => 'Please insert an email address.',
			'email' => 'Please insert a valid email address.'
		],
		'office_phone' => ['required' => 'Please insert an office phone.'],
		'user_address_office' => ['required' => 'Please select office address.'],

		//user password
		'password' => [
			'required' => 'Please insert password.',
			'min:6' => 'The new password must be at least 6 characters.',

		],

		'password_confirmation' => [
			'required' => 'Please confirm your password.',

		],

		'company_name' => [
			'required' => 'Please company name.',
			'max:255' => 'The company name must be maximum 255 characters.',
		],

		'company_logo' => [
			'mimes:jpeg,jpg,gif,png' => 'The company logo must be jpeg, jpg, gif or png picture.',
		],

		'office_title_req' => [
			'required' => 'Please insert office name.',
			'max:255' => 'Office name must be maximum 45 characters.',
		],

		'company_addr_number_req' => [
			'required' => 'Please insert address number.',
			'max:45' => 'Address number must be maximum 45 characters.',
		],

		'company_addr_street_req' => [
			'required' => 'Please insert address street.',
			'max:255' => 'Address street must be maximum 255 characters.',
		],

		'company_addr_town_req' => [
			'required' => 'Please insert address city.',
			'max:45' => 'Address city must be maximum 45 characters.',
		],

		'company_addr_state_req' => [
			'required' => 'Please insert address state.',
			'max:45' => 'Address state must be maximum 45 characters.',
		],

		'company_addr_zip_code_req' => [
			'required' => 'Please insert address zip code.',
			'max:45' => 'Address zip code must be maximum 45 characters.',
		],

		'admin_name' => [
			'required' => 'Please insert contact person / company administrator name.',
			'max:255' => 'Contact person / company administrator name must be maximum 255 characters.',
		],

		'admin_title' => [
			'required' => 'Please insert contact person / company administrator title.',
			'max:255' => 'Contact person / company administrator title must be maximum 255 characters.',
		],

		'admin_office_phone' => [
			'required' => 'Please insert contact person / company administrator office phone.',
			'max:45' => 'Contact person / company administrator office phone must be maximum 45 characters.',
		],

		'subscription_type' => [
			'required' => 'Please check one subscription type.',
		],

		'mf_id_#0#' => [
			'required' => 'Please select at least one spec item.',
			'exists' => 'Spec item does not exists.',
		],

		'ref_points_needed' => [
			'numeric' => 'Referral points needed must be a numeric value.',
			'max' => 'Referral points needed should not be longer than 11 digits.',
		],

		'bid_id' => [
			'required' => 'Please use a proper bid item.',
			'exists' => 'Please use a proper bid item.'
		],

		'bidder_id' => [
			'required' => 'Please use a proper bidder.',
			'exists' => 'Please use a proper bidder.'
		],

	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'contr_ab' => "company name",
		'prop_ab_id' => "company name",
		'contr_ab_id' => "company name",
		'mf_title'	=> 'spec title',
		'mf_number'	=> 'spec number',
		'architect_id' => 'architect',
		'owner_id'	   => 'owner',
        'version_question' => 'question',
        'version_answer' => 'answer',
        'version_appr_note' => 'Transmittal Notes (Approval-out)',
        'version_subc_note' => 'Transmittal Notes (Subcontractor-out)',
        'version_note' => 'Transmittal Notes',
	],

];
