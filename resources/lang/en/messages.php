<?php

return [
    'global' => [
        'store' => [
            'success' => 'New record was successfully inserted.',
            'error' => 'Record was not inserted. Please try again',
        ],

        'update' => [
            'success' => 'This record was successfully updated.',
            'error' => 'Record was not updated. Please try again.',
        ],

        'delete' => [
            'success' => 'This record was successfully deleted.',
            'error' => 'Record was not deleted. Please try again.',
        ]
    ],

    'projectItems' => [
        'insert' => [
            'success' => 'New :item was successfully created.',
            'error'    => 'New :item was not inserted. Please try again.',
        ],
        'update' => [
            'success' => 'The :item was successfully updated.',
            'error'    => 'The :item was not updated. Please try again.'
        ],
        'delete' => [
            'success' => 'The :item was successfully deleted.',
            'error'    => 'The :item was not deleted. Please try again.'
        ],
        'limitedNumber' => 'Your subscription allows you to create only :number of projects.',
        'notFound' => 'The :item was not found.'
    ],

    'projectSubcontractor' => [
        'storeContacts' => [
            'success' => 'Contacts were successfully assigned.',
            'error'    => 'Contacts were not assigned. Please try again.',
        ],
        'share' => [
            'success' => 'New :item was successfully added and notified.',
            'error'    => 'New :item was not assigned. Please try again.',
        ],
        'unShare' => [
            'success' => 'The project was successfully unshared with the subcontractor and all his subcontractors.',
            'error'    => 'The project was not unshared with the subcontractor. Please try again.',
        ],
        'invite' => [
            'success' => 'Company was successfully invited to this project.',
            'error'    => 'Company was not invited. You have already invited this company or you are not able to invite this address book company.',
            'select_contact' => 'Please go to manage contacts and select a proper contacts for invitation.'
        ]
    ],

    'bidder' => [
        'invite' => [
            'success' => 'The Bidder was successfully invited to bid on this project.',
            'error'    => 'The Bidder was not invited to bid. Please try again.',
            'select_contact' => 'Please go to manage contacts and select a proper contact for invitation.'
        ]
    ],

    'bidsProposals' => [
        'notifications' => [
            'success' => 'Notification was successfully sent to all recipients.',
        ]
    ],

    'projectFiles' => [
        'upload' => [
            'success' => 'The :item was successfully uploaded.',
            'error'    => 'The :item was not uploaded. Please try again.',
        ],
    ],

    'masterFormat' => [
        'import' => [
            'success' => 'Master Format records were successfully imported.',
            'error'    => 'Master Format records were not imported. Please try again.',
        ],
        'invalidType' => 'Invalid file type. Please import CSV file.'
    ],

    'addressBook' => [
        'import' => [
            'success' => ':type records were successfully imported.',
            'error'    => ':type records were not imported. Please try again.',
            'fileRequired' => 'Please insert file.',
            'fileTypeError' => 'Please insert a :type file.',
            'undo' => 'Import was successfully reversed.',
        ],

        'sync_company' => [
            'your_uac' => 'This is your company UAC',
            'invalid' => 'Invalid UAC',
            'enter_valid' => 'Enter valid UAC',
        ],

        'store' => [
            'success' => 'New :item was successfully created.',
            'error' => ':item was not stored. Please try again.',
        ],

        'update' => [
            'success' => ':item updated successfully.',
            'error'    => ':item was not updated. Please try again.',
        ],

        'delete' => [
            'success' => ':item deleted successfully.',
            'info' => 'This :item can not be deleted at this moment because it is connected with some other entities. Please disconnect this :item from all of your project modules and entities before you delete it!',
            'error' => ':item was not deleted. Please try again.',
        ],

        'rating' => [
            'success' => 'This company was rated successfully for the selected project.'
        ],
    ],
    'referral' => [
        'insert' => [
            'success' => 'A referral was sent on the email :email.',
            'error'    => 'A referral was not sent. Please try again.',
            'exists'    => 'The email address :email is already used by an existing user. Please try another email address. ',
        ],
    ],

    'customCategories' => [
        'delete' => [
            'info' => 'There are entries in your address book that belongs to this category. Please change this before you delete this category.'
        ]
    ],

    'submittals' => [
        'store' => [
            'success' => 'New :item was successfully created.',
            'error' => ':item was not inserted. Please try again',
        ],

        'update' => [
            'success' => ':item updated successfully',
            'error' => ':item was not updated. Please try again.',
        ],

        'delete' => [
            'success' => ':item deleted successfully.',
            'error' => ':item was not deleted. Please try again.',
        ],

        'unshare' => [
            'success' => 'This record was successfully unshared with the specified company.',
            'error' => 'Something went wrong while unsharing the submittal with all subcontractors. Please try again.'
        ]
    ],

    'project_files' => [
        'delete' => [
            'info' => 'This file is shared with some subcontractors. Please unshare the file with all your subcontractors before deleting it.',
        ],

        'unshare' => [
            'success' => 'This record was successfully unshared with the specified company.',
            'error' => 'Something went wrong while unsharing the file with all subcontractors. Please try again.'
        ]
    ],

    'data_transfer_limitation' => [
        'something_wrong' => 'Something went wrong. Please try again.',
        'upload_error' => ':item file could not be uploaded because you reached your subscription level upload limitation.',
        'download_error' => ':item file could not be uploaded because you reached your subscription level download limitation.',
    ],

    'users' => [
        'permissions' => [
            'success' => 'Permissions set successfully.',
            'admin'   => 'Can not set permissions for company admin.'
        ],

        'store' => [
            'success' => 'Company user added successfully.',
            'error' => 'Something went wrong. Please try again.',
            'change' => 'Your account has been assigned with company user role.',
            'sub2allowance' => 'You can not register company users anymore because your subscription level have allowance for only 3 company users.',
        ],

        'update' => [
            'success' => 'Company user updated successfully.',
            'error' => 'Something went wrong. Please try again.',
            'change' => 'Your account has been assigned with company user role.',
            'sub2allowance' => 'You can not register company users anymore because your subscription level have allowance for only 3 company users.',
        ],

        'delete' => [
            'success' => 'Company user deleted successfully.',
            'error' => 'Something went wrong. Please try again.',
        ]
    ],

    'company_profile' => [
        'update' => [
            'success' => 'Company profile updated successfully.',
        ],
        'update_subscription'=> [
            'success' => 'Your subscription has been updated successfully.',
            'not_enough_points' => "You don't have enough referral points for this subscription level.",
            'error' => 'Your subscription has not been updated. Please try again.',
        ],
        'renew_subscription'=> [
            'success' => 'Your subscription has been re-newed successfully.',
        ],
    ],

    'user_profile' => [
        'update' => [
            'success' => 'Your profile has been updated successfully.',
            'error' => 'Your profile has not been updated. Please try again.',
        ],

        'password' => [
            'success' => 'Your password was changed successfully.',
            'error' => 'The old password field doesn\'t match with your password.',
        ]
    ],
    'blog'  => [
        'post_not_found' => 'Post Not Found',
    ],
    'sharedProject' => [
        'files' => [
            'no_permission' => "You don't have permission to view :type files."
        ]
    ],
    'email' => [
        'sendMailSubcontractor' => [
            'subject' => 'A new project was shared with you',
            'name'    => 'Sir or Madam',
        ],
        'sendReferral' => [
            'subject' => 'Cloud PM - Invitation to join',
            'name'    => 'Sir or Madam',
        ],
    ],

    'office' => [
        'success' => 'Office changed successfully',
    ],

    'projects' => [
        'already_unshared' => 'This project can not be accepted because it was un-shared by the company.',
        'successfully_accepted' => 'Project was accepted successfully.',
        'reject' => [
            'success' => 'Project was successfully rejected',
            'error' => 'Project was not rejected! Please try again.',
        ],
        'request-for-batch-sent' => 'Your request was sent. We will notify you on email when the file is ready for download.',
        'error-request-for-batch-sent' => 'Your request was not sent because you are close to your download limit and you don\'t have enough data transfer left to proceed.',
        'download-limit-exceeded' => 'File can not be downloaded because your download limit is exceeded or you don\'t have enough data transfer left to proceed with the download.'
    ],

    'free_account' => [
        'success' => 'Account successfully created. An email for confirmation is sent to the company admin.',
        'points_reset_success' => 'Referral points of this company was successfully reset.',
        'error' => 'Something went wrong. Please try again to register this account.',
        'not_a_free_account' => 'You can not reset this company referral points!',
    ],

    'transmittals' => [
        'notification_success' => 'Transmittal notification sent successfully.',
        'not_exist' => 'Transmittal file does not exist.'
    ],

    'tasks' => [
        'store' => [
            'success' => 'New :item was successfully created.',
            'error' => ':item was not stored. Please try again.',
        ],

        'update' => [
            'success' => ':item updated successfully.',
            'error'    => ':item was not updated. Please try again.',
        ],

        'delete' => [
            'success' => ':item deleted successfully.',
            'info' => 'The Task can\'t be deleted if the status is set to Open.',
            'error' => ':item was not deleted. Please try again.',
        ],

        'notifications' => [
            'success' => 'Notification was successfully sent to all assignees.',
        ]
    ],

    'daily_reports' => [
        'store' => [
            'success' => 'New :item was successfully created.',
            'error' => ':item was not stored. Please try again.',
        ],

        'update' => [
            'success' => ':item updated successfully.',
            'error'    => ':item was not updated. Please try again.',
        ],

        'delete' => [
            'success' => ':item deleted successfully.',
            'info' => 'The Task can\'t be deleted if the status is set to Open.',
            'error' => ':item was not deleted. Please try again.',
        ],

        'notifications' => [
            'success' => 'Daily Report notification sent successfully.',
            'not_exist' => 'Daily report file does not exist.'
        ],

        'copy_report' => [
            'success' => 'Daily Report is copied successfully.',
            'error' => 'Previous Daily report does not exist.'
        ],

    ],

];