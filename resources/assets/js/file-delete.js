$('.delete-file').click(function() {
    var fileId = $(this).next('.s3FilePath').attr("id");
    var fileType = $(this).next('.s3FilePath').attr("data-type");
    var s3FilePath = $(this).next('.s3FilePath').val();
    var s3 = new AWS.S3();

    var params = {
        Bucket: 'cmsconstructionmanager', /* required */
        Key: s3FilePath /* required */
    };


    var loader = $(this).find('.delete-wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');

    $('#confirmDelete').on('show.bs.modal', function(e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_delete"]').on('click', function() {
        $(this).next().next().children().css('display', 'block');
        $(this).attr("disabled", "disabled");

        s3.deleteObject(params, function(err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                //alert('An error occurred. Please try again!');
            } else {
                console.log(data); // successful response
                $.ajax({
                    type: "POST",
                    url: site_url + "/delete-file",
                    dataType: "json",
                    data: {
                        fileId: fileId,
                        fileType: fileType
                    },
                    //error: function(data) {
                    //    alert('An error occurred. Please try again!');
                    //},
                    success: function(data) {
                        location.reload();
                    }
                });
            }
        });
    });
});

//fast inline file delete
$(document).on('click', '.inst_delete_file', function() {
    var fileId = $(this).attr("data-file-id");
    var filePath = $(this).attr("data-file-path");
    var fileIdHolder = $(this).attr("data-file-id-holder");
    var fileType = $('#type').val();
    var deleteButton = $(this);
    var successMessageContainer = $(this).parent().parent();

    //var s3FilePath = $(this).next('.s3FilePath').val();
    var s3 = new AWS.S3();

    var params = {
        Bucket: 'cmsconstructionmanager', /* required */
        Key: filePath /* required */
    };

    //parse url in order to get the project id and other segments
    var url = $(location).attr('href').split("/");

    $('#confirmDelete').on('show.bs.modal', function(e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_delete"]').on('click', function() {
        // deleteButton.parent().children('.delete-wait-cont').children('.delete-wait').css('display', 'block');
        //loader.css('display', 'none');
        //glyphicon.css('display', 'inline-block');
        deleteButton.attr("disabled", "disabled");

        s3.deleteObject(params, function(err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                var errTemplateId = '.error-upload';
                var errContainer = '.' + successMessageContainer.attr("id");
                successMessageContainer.empty();
                cm.toggleTemplate(errTemplateId, errContainer).add();
            } else {
                console.log(data); // successful response
                $.ajax({
                    type: "POST",
                    url: site_url + "/delete-file",
                    dataType: "json",
                    data: {
                        fileId: fileId,
                        fileType: fileType
                    },
                    error: function(data) {
                        $('#confirmDelete').modal('toggle');

                        var errTemplateId = '.error-upload';
                        var errContainer = '.' + successMessageContainer.attr("id");
                        successMessageContainer.empty();
                        cm.toggleTemplate(errTemplateId, errContainer).add();
                    },
                    success: function(data) {
                        $('#confirmDelete').modal().hide();

                        if (fileType == 'contracts') {
                            if (url[url.length - 1] == 'edit') {
                                window.location.replace(site_url + '/projects/' + url[4] + '/contracts/' + url[6] + '/edit');
                            }
                        }

                        if (fileType == 'project-files') {
                            if (url[url.length - 1] == 'edit') {
                                window.location.replace(site_url + '/projects/' + url[4] + '/project-files/' + url[6] + '/file');
                            }
                        }

                        //window.location.replace(site_url+);
                        $(fileIdHolder).val('');
                        successMessageContainer.empty();
                        successMessageContainer.html('File successfully deleted!')
                    }
                });
            }
        });
    });
});

//fast inline file delete
$(document).on('click', '.inst_delete_file_multi', function() {
    var fileId = $(this).attr("data-file-id");
    var filePath = $(this).attr("data-file-path");
    var fileIdHolder = $(this).attr("data-file-id-holder");
    var fileType = $('#type').val();
    var deleteButton = $(this);
    var successMessageContainer = $(this).parent().parent();

    //var s3FilePath = $(this).next('.s3FilePath').val();
    var s3 = new AWS.S3();

    var params = {
        Bucket: 'cmsconstructionmanager', /* required */
        Key: filePath /* required */
    };

    //parse url in order to get the project id and other segments
    var url = $(location).attr('href').split("/");

    $('#confirmDelete').on('show.bs.modal', function(e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_delete"]').on('click', function() {
        // deleteButton.parent().children('.delete-wait-cont').children('.delete-wait').css('display', 'block');
        //loader.css('display', 'none');
        //glyphicon.css('display', 'inline-block');
        deleteButton.attr("disabled", "disabled");

        s3.deleteObject(params, function(err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                var errTemplateId = '.error-upload';
                var errContainer = '.' + successMessageContainer.attr("id");
                successMessageContainer.empty();
                cm.toggleTemplate(errTemplateId, errContainer).add();
            } else {
                console.log(data); // successful response
                $.ajax({
                    type: "POST",
                    url: site_url + "/delete-file",
                    dataType: "json",
                    data: {
                        fileId: fileId,
                        fileType: fileType
                    },
                    error: function(data) {
                        $('#confirmDelete').modal('toggle');

                        var errTemplateId = '.error-upload';
                        var errContainer = '.' + successMessageContainer.attr("id");
                        successMessageContainer.empty();
                        cm.toggleTemplate(errTemplateId, errContainer).add();
                    },
                    success: function(data) {
                        $('#confirmDelete').modal().hide();

                        if (fileType == 'contracts') {
                            if (url[url.length - 1] == 'edit') {
                                window.location.replace(site_url + '/projects/' + url[4] + '/contracts/' + url[6] + '/edit');
                            }
                        }

                        if (fileType == 'project-files') {
                            if (url[url.length - 1] == 'edit') {
                                window.location.replace(site_url + '/projects/' + url[4] + '/project-files/' + url[6] + '/file');
                            }
                        }

                        var ids = $("#file_id").val();
                        ids = ids = ids.replace(fileId + '-', '');
                        ids = ids = ids.replace('-' + fileId, '');
                        ids = ids.replace(fileId, '');
                        $("#file_id").val(ids);
                        $(".container" + fileId).remove();
                        //successMessageContainer.html('File successfully deleted!')
                    }
                });
            }
        });
    });
});