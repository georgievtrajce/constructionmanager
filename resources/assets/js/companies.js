//invite company button disable
$('.invite-company').click(function() {
    $(this).attr('disabled', 'disabled');
    this.value = 'Sending...';
    this.form.submit();
});

$('#pco_permission').click(function() {
    if($('#pco_permission').is(':checked')) {
        $('#pco_permission').attr('checked', 'checked');
        $('.pco_permission_type').removeAttr('disabled');
        $("#own_pcos").prop("checked", true);
    } else {
        $('.pco_permission_type').attr('disabled', 'disabled');
        $('.pco_permission_type').removeAttr('checked');
        $('#pco_permission').removeAttr('checked');
    }
});