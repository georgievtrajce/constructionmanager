$(function() {

    //check project module read permissions if project write or delete permission is checked
    $('.project-event').click(function() {
        $('.projects-module').prop('checked', true);
    });

    //check project module read permissions if any dependent module is checked
    $('.read-permission').click(function() {
        $('.projects-module').prop('checked', true);
    });

    //if read permission is checked uncheck the same entity write and delete permissions
    //the read permissions must exist if a write or delete permission is set
    $('.mandatory-permissions').click(function() {
        if($(this).prop('checked') == false) {
            $(this).parent().parent().find('.event-permissions').removeAttr('checked');
        }
    });

    //if project module read permission is unchecked uncheck all other dependent modules and project files permissions
    $('.projects-module').click(function() {
        if($('.projects-module').prop('checked') == false) {
            $('.read-permission').removeAttr('checked');
            $('.event-permissions').removeAttr('checked');
            $('.project-event').removeAttr('checked');
        }
    });

    //if write or delete permissions are checked, check the read permission for that entity to
    $('.event-permissions').click(function() {
            $(this).parent().parent().find('.mandatory-permissions').prop('checked', true);
            $('.projects-module').prop('checked', true);
    });

    //address book checking if custom categories are checked
    $('.custom-categories-module').click(function() {
        if($(this).prop('checked') == true) {
            $('.address-book-read').prop('checked', true);
        }
    });

    //uncheck custom categories module permissions if address book read permission is unchecked
    $('.address-book-read').click(function() {
        if($('.address-book-read').prop('checked') == false) {
            $('.custom-categories-module').prop('checked', false);
        }
    });

    //address book and custom categories read permissions checking if write and delete permissions are checked
    $('.main-module-event').click(function() {
        if($(this).prop('checked') == true) {
            $('.main-module-read').prop('checked', true);
        }
    });

    //unchecking address book and custom categories write and delete permissions
    // when read and delete method are unchecked
    $('.main-module-read').click(function() {
        if($(this).prop('checked') == false) {
            $('.main-module-event').prop('checked', false);
        }
    });

    //select all permissions event
    $('#select_all_permissions').click(function() {
        if ($('#select_all_permissions').is(':checked')) {
            $('.permission').prop('checked', true);
            $('.pco_permission_type').removeAttr('disabled');
            $("#own_pcos").prop("checked", true);
        }
        else {
            $('.permission').removeAttr('checked');
            $('.pco_permission_type').attr('disabled', 'disabled');
            $('.pco_permission_type').removeAttr('checked');
        }
    });

    //select all read permissions
    $('#select_read_permissions').click(function() {
        if ($('#select_read_permissions').is(':checked')) {
            $('.read').prop('checked', true);
        }
        else {
            $('.read').removeAttr('checked');
        }
    });

    //select all read permissions
    $('#select_write_permissions').click(function() {
        if ($('#select_write_permissions').is(':checked')) {
            $('.write').prop('checked', true);
        }
        else {
            $('.write').removeAttr('checked');
        }
    });

    //select all read permissions
    $('#select_delete_permissions').click(function() {
        if ($('#select_delete_permissions').is(':checked')) {
            $('.delete').prop('checked', true);
        }
        else {
            $('.delete').removeAttr('checked');
        }
    });

    //select company admin checkbox
    $(document).on("change", "input[name='company_admin']", function () {
    //$('#company_admin').click(function() {
        $('#approveAction').modal().hide();
        var checkbox = $('#company_admin');
        if(document.getElementById('company_admin').checked) {
            checkbox.prop("checked", true);

            //var companyAdmin = confirm('Please confirm your actions. You will no longer be a Company Administrator and you won\'t be able to Manage Users or the Company Profile.');
            $('#approveAction').find('.modal-title').text('Notification');
            $('#approveAction').find('.modal-body p').text('Please confirm your actions. You will no longer be a Company Administrator and you won\'t be able to Manage Users or the Company Profile.');
            $('#approveAction').modal();
            //if(companyAdmin != true) {
            //    return false;
            //} else {
            //    $(this).attr('checked','checked');
            //    $('#project_admin').removeAttr('checked');
            //}

            //if confirmation is canceled
            $('[id="cancel_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                checkbox.removeAttr('checked');
                //return false;
            });

            //if confirmation is approved
            $('[id="approve_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                checkbox.attr('checked','checked');
                $('#project_admin').removeAttr('checked');
            });
        } else {
            checkbox.removeAttr('checked');
        }
    });

    //select all project and modules permissions
    $(document).on("click", ".all-projects-modules", function () {
        var checkboxJQuery = $(this);
        if (this.checked === true) {
            checkboxJQuery.prop("checked", true);
            $('#approveAction').find('.modal-title').text('Notification');
            $('#approveAction').find('.modal-body p').text('Please confirm your actions. The User will have Read, Write or Delete permissions for all Projects and Files. You can set the User Permissions for each Project individually.');
            var modal = $('#approveAction').modal();

            //if confirmation is canceled
            $('[id="cancel_action"]').on('click', function(){
                modal.hide();
                checkboxJQuery.removeAttr('checked');
                checkboxJQuery.parent().parent().find('.read-permissions').removeAttr('checked');
            });

            //if confirmation is approved
            $('[id="approve_action"]').on('click', function(){
                modal.hide();
                checkboxJQuery.attr('checked','checked');
            });
        } else {
            checkboxJQuery.removeAttr('checked');
        }
    });

    //select project admin checkbox
    $(document).on("change", "input[name='project_admin']", function (e) {
    //$('#project_admin').click(function() {
        $('#approveAction').modal().hide();
        var checkbox2 = $('#project_admin');
        if(document.getElementById('project_admin') != undefined && document.getElementById('project_admin').checked) {
            checkbox2.prop("checked", true);
            //var projectAdmin = confirm('Please confirm your actions. This user will be a Project Administrator and will be able to create Projects and manage modules, files and user permissions for Projects it created and will be able to manage Address Book and Categories.');
            $('#approveAction').find('.modal-title').text('Notification');
            $('#approveAction').find('.modal-body p').text('Please confirm your actions. This User will become a Project Administrator and will be able to create Projects, manage Modules, Files and User Permissions for the Projects he will create. This User will be able to manage the Address Book and the Address Book Categories.');
            $('#approveAction').modal();

            //if(projectAdmin != true) {
            //    return false;
            //} else {
            //    $(this).attr('checked','checked');
            //    $('#company_admin').removeAttr('checked');
            //}

            //if confirmation is canceled
            $('[id="cancel_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                checkbox2.removeAttr('checked');
                //return false;
            });

            //if confirmation is approved
            $('[id="approve_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                checkbox2.attr('checked','checked');
                $('#company_admin').removeAttr('checked');
            });
        } else {
            checkbox2.removeAttr('checked');

            $('#approveAction').find('.modal-title').text('Notification');
            $('#approveAction').find('.modal-body p').text("Please confirm your actions. This User won't be able to create Projects, manage Modules, Files and User Permissions for the Projects he will create. This User won't be able to manage the Address Book and the Address Book Categories.");
            $('#approveAction').modal();

            //if confirmation is canceled
            $('[id="cancel_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                $("#project_admin").prop( "checked", true );
                //checkbox2.attr('checked','checked');
                //return false;
            });

            //if confirmation is approved
            $('[id="approve_action"]').on('click', function(){
                $('#approveAction').modal().hide();
                checkbox2.removeAttr('checked');
            });
        }
    });

    $('.project_admin').click(function(event) {
        event.preventDefault();
        var userId = $(this).val();
        var projectId = $(this).attr("data-project-id");
        var projectAdmin = $(this).attr("data-project-admin");
        var checked = $(this).attr("data-checked");

        if(checked == 'true') {
            if(projectAdmin == 1) {
                $('#approveAction').find('.modal-body p').text('Are you sure you want to remove project administrator role from this user? If you click OK you will no longer be a project admin for this project.');
                $('#approveAction').modal();
            } else {
                $('#approveAction').find('.modal-body p').text('Are you sure you want to remove project administrator role from this user?');
                $('#approveAction').modal();
            }
        } else {
            //check if the project admin is changing his role for this project
            if(projectAdmin == 1) {
                $('#approveAction').find('.modal-body p').text('Are you sure you want this user to be project administrator? If you click OK you will no longer be a project admin for this project.');
                $('#approveAction').modal();
            } else {
                $('#approveAction').find('.modal-body p').text('Are you sure you want this user to be project administrator?');
                $('#approveAction').modal();
            }
        }


        //if confirmation is canceled
        $('[id="cancel_action"]').on('click', function(){
            $('#approveAction').modal().hide();
        });


        $('[id="approve_action"]').on('click', function(){
            $(this).parent().find('.project_admin_wait').css('display','block');
            $('.project_admin').attr('disabled','disabled');
            $('#approveAction').modal().hide();

            $.ajax({
                url: site_url+"/change-project-admin",
                dataType: "json",
                data: {
                    userId: userId,
                    projectId: projectId,
                    removePermission: checked
                },
                error: function(data) {
                    console.log('test');

                    $('#alertPopup').find('.modal-body p').text('An error occurred. Please try again!');
                    $('#alertPopup').modal();
                },
                success: function(data) {

                    $(this).parent().find('.project_admin_wait').css('display','none');
                    $('.project_admin').removeAttr('disabled');

                    if (projectAdmin == 1) {
                        window.location.replace(site_url+"/projects");
                    } else {
                        document.location.reload(true);
                    }
                }
            });
        });


    });

});