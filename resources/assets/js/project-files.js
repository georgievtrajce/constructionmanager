/**
 * Created by dragan.atanasov on 6/5/2015.
 */

$(function() {

    //adding active class for project files tab link
    var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
    var pathArray = window.location.pathname.split( '/' );
    var segment_3 = pathArray[3];

    if(segment_3 == 'project-files') {
        $('#project-files').addClass('active');
        $('#project-files-container').css('display','block');
    }

    //project files tab drop down slide up or slide down
    $('#project-files').click(function(){

        if($('#project-files').hasClass('active')) {
            $('#project-files').removeClass('active');
            $('#project-files-container').slideUp();
        }
        else {
            $('#project-files').addClass('active');
            $('#project-files-container').slideDown();
        }
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });

    //select all files
    $('#project_files_select_all').click(function(){
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ($('#project_files_select_all').is(':checked')) {
            $('.project_file').prop('checked', true);
        }
        else {
            $('.project_file').removeAttr('checked');
        }
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });

    //Project - UserPermissions - select all office checkboxes
    $('.offices_select_all').click(function(){
        var tableId = $(this).closest('table').attr('id');
        var officeIds = tableId.split("-");

        if ($('#offices_select_all_'+officeIds[1]).is(':checked')) {
            $('.office_file_'+officeIds[1]).prop('checked', true);
        }
        else {
            $('.office_file_'+officeIds[1]).removeAttr('checked');
        }
    });

    //select all users
    $(document).on("click", ".select_all_users", function() {
        var id = $(this).attr('id');
        var companyIds = id.split("_");

        if ($('#select_all_users_'+companyIds[3]).is(':checked')) {
            $('.users_'+companyIds[3]).prop('checked', true);
        }
        else {
            $('.users_'+companyIds[3]).removeAttr('checked');
        }
    });

    //select all users from each company
    $(document).on("click", ".select_all_companies", function() {
        var allCompanyIds = $("#allCompanyIds").val();

        if(allCompanyIds) {
            var companyIds = allCompanyIds.split(",");

            $.each(companyIds, function (key, companyId) {
                if ($('#select_all_companies').is(':checked')) {
                    $('#select_all_users_' + companyId).prop('checked', true);
                    $('.users_' + companyId).prop('checked', true);
                }
                else {
                    $('#select_all_users_' + companyId).prop('checked', false);
                    $('.users_' + companyId).prop('checked', false);
                }
            });
        }
    });

    //select all subcontractor users
    $(document).on("click", ".select_all_subcontractor_users", function() {
        var id = $(this).attr('id');
        var companyIds = id.split("_");
        console.log(id);
        console.log(companyIds[4]);

        if ($('#select_all_subcontractor_users_'+companyIds[4]).is(':checked')) {
            $('.subcontractor_users_'+companyIds[4]).prop('checked', true);
        }
        else {
            $('.subcontractor_users_'+companyIds[4]).removeAttr('checked');
        }
    });

    //select all subcontractor users from each company
    $(document).on("click", ".select_all_subcontractor_companies", function() {
        var allCompanyIds = $("#allSubcontractorCompanyIds").val();
        console.log(allCompanyIds);

        if(allCompanyIds) {
            var companyIds = allCompanyIds.split(",");

            $.each(companyIds, function (key, companyId) {
                if ($('#select_all_subcontractor_companies').is(':checked')) {
                    $('#select_all_subcontractor_users_' + companyId).prop('checked', true);
                    $('.subcontractor_users_' + companyId).prop('checked', true);
                }
                else {
                    $('#select_all_subcontractor_users_' + companyId).prop('checked', false);
                    $('.subcontractor_users_' + companyId).prop('checked', false);
                }
            });
        }
    });

    //share selected files with selected companies - display input event
    $('#share_with').change(function(){
        var value = $(this).val();
        if(value == 'all') {
            $('#selected_input_cont').css('display','none');
            $('.project-subcontractor-auto').val('');
            $('.project-subcontractor-id').val('');

            $('#selected_subcontractors_cont').css('display','none');
            $('#selected_subcontractors').empty();
        }
        if(value == 'selected') {
            $('#selected_input_cont').css('display','block');
            $('#selected_subcontractors_cont').css('display','block');
        }
    });

    //share files
    $('#share_project_file').click(function(){
        //get selected files ids
        var selected_files = $("#unshared-files input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();

        //share files with all subcontractors
        if($('#share_with').val() == 'all') {
            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing.');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/all",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        project_id: $('#project-id').val()
                    },
                    error: function(data) {
                        //alert('Please share this project with your companies subcontractors first before trying to share project files.');
                        $('#alertPopup').find('.modal-body p').text('Please share this project with your companies subcontractors first before trying to share project files.');
                        $('#alertPopup').modal();
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared.');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
        //share files with selected companies
        else {
            //get selected subcontractors ids
            var selected_subcontractors = $(".subcontractor-id").map(function(){
                return $(this).val();
            }).get();

            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing.');
                $('#alertPopup').modal();
            } else if(selected_subcontractors.length === 0) {
                //alert('Please select companies for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select companies for sharing.');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/selected",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        selected_subcontractors: selected_subcontractors
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
    });

    //send transmittal email notification
    $('.sendNotificationEmail').click(function(){
        var sendButton = $(this);

        //disable send button and show loading image
        sendButton.next().children().css('display', 'block');
        sendButton.attr('disabled', 'disabled');

        //get attributes with values
        var filePath = sendButton.attr("data-file-path");
        var fileId = sendButton.attr("data-file-id");
        var emailed = sendButton.attr("data-emailed");
        var type = $('#type').val();

        var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
        var pathArray = window.location.pathname.split( '/' );
        var projectFileType = pathArray[4];
        var projectId = pathArray[2];

        var users = [];
        $("input[name='employees_users_in[]']").each( function () {
            if ($(this).is(":checked")) {
                users.push($(this).val());
            }
        });

        var contacts = [];
        $("input[name='employees_contacts_in[]']").each( function () {
            if ($(this).is(":checked")) {
                contacts.push($(this).val());
            }
        });


        function sendTransmittalEmail(fileId, filePath) {
            $.ajax({
                type: "POST",
                url: site_url+"/send-project-file-email-notification",
                dataType: "json",
                data: {
                    type: type,
                    projectFileType: projectFileType,
                    fileId: fileId,
                    projectId: projectId,
                    filePath: filePath,
                    users: users.join("|"),
                    contacts: contacts.join("|")
                },
                success: function(data) {
                    if(data.status === 'success') {
                        location.reload();
                    } else {
                        //disable send button and show loading image
                        sendButton.next().children().css('display', 'none');
                        sendButton.removeAttr('disabled');
                        //alert(data.message);
                        $('#alertPopup').find('.modal-body p').text(data.message);
                        $('#alertPopup').modal();
                    }

                }
            });
        }

        sendTransmittalEmail(fileId, filePath);


    });


});

$("#select_all").change(function(){  //"select all" change
    var status = this.checked; // "select all" checked status
    $('.contact_item').each(function(){ //iterate all listed checkbox items
        this.checked = status; //change ".checkbox" checked status
    });
});
