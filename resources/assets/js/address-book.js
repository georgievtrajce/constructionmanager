/**
 * Created by dragan.atanasov on 3/11/2015.
 */
$(function() {

    $(".add-new-custom-category").click(function() {

        $('.custom_categories-main-cont').append('<div class="panel panel-default custom-category-container">'+
                                            '<div class="panel-heading">'+
                                            '<h4 class="panel-title">Custom Category</h4>'+
                                            '</div>'+
                                            '<div class="panel-body">'+
                                            '<div class="form-group">'+
                                            '<label class="col-md-4 control-label">Name of the category</label>'+
                                            '<div class="col-md-6">'+
                                            '<input type="text" class="form-control" name="company_custom_categories[]">'+
                                            '</div>'+
                                            '</div>'+
                                            '<div class="form-group">'+
                                            '<div class="col-md-6 col-md-offset-4">'+
                                            '<button type="button" class="btn btn-danger remove-custom-category">Remove Custom Category</button>'+
                                            '</div>'+
                                            '</div>'+
                                            '</div>'+
                                            '</div>');
    });

    $('.panel-default').on('click', '.remove-custom-category', function(e) {
        e.preventDefault();

        $(this).parent().parent().parent().parent().remove();
    });

    $('#validate-uac').click(function() {
        $.ajax({
            url: site_url+"/validate-uac",
            dataType: "json",
            data: {
                uac: $('#uac').val()
            },
            success: function(data) {
                $('.registered-company-container').empty();
                $('#companyId').val('');
                if (data == 'This is your company UAC') {
                    $('.registered-company-container').append('<p>'+data+'</p>');
                    $('#companyId').val('');
                }
                else if(data == 'Invalid UAC') {
                    $('.registered-company-container').append('<p>'+data+'</p>');
                    $('#companyId').val('');
                }
                else if(data == 'Enter valid UAC') {
                    $('.registered-company-container').append('<p>'+data+'</p>');
                    $('#companyId').val('');
                }
                else {
                    if(data.logo != null) {
                        $('.registered-company-container').append('<table class="table"><thead><tr><th>Name</th><th>Avatar</th><th></th></tr></thead><tbody><tr><td>'+data.name+'</td><td><img src="'+aws_url+data.logo+'" alt=""></td><td><a href="javascript:;" class="btn btn-danger btn-sm" id="remove-synced-company">Remove</a></td></tr></tbody></table>');
                    }
                    else {
                        $('.registered-company-container').append('<table class="table"><thead><tr><th>Name</th><th>Avatar</th><th></th></tr></thead><tbody><tr><td>'+data.name+'</td><td><img src="'+site_url+'/img/cm-img-user.png" alt=""></td><td><a href="javascript:;" class="btn btn-danger btn-sm" id="remove-synced-company">Remove</a></td></tr></tbody></table>');
                    }
                    $('#companyId').val(data.id);
                }
            }
        });
    });

    $('.registered-company-container').on('click', '#remove-synced-company', function(e) {
        e.preventDefault();

        $('.registered-company-container').empty();
        $('#companyId').val('');
        $('#uac').val('');
    });

    $( "#master_format_search_add" ).keyup(function() {
        $('.registered-company-container').empty();
    });

    $('.address-book-company-office').on('click', function(e) {
        e.preventDefault();

        if($(this).parent().parent().find('.office-container').hasClass('active')) {
            $(this).parent().parent().find('.office-container').removeClass('active');
            $(this).parent().parent().find('.office-container').slideUp();
        } else {
            $(this).parent().parent().find('.office-container').addClass('active');
            $(this).parent().parent().find('.office-container').slideDown();
        }
    });


    $("#cc_project").change(function() {
        pathArray = location.href.split( '/' );
        protocol = pathArray[0];
        host = pathArray[2];
        addressSegment = pathArray[3];
        companyId = pathArray[4];
        ratingsSegment = pathArray[5];
        urlBase = protocol + '//' + host + '/' + addressSegment + '/' + companyId + '/' + ratingsSegment + '/' + this.value;
        window.location.href = urlBase;
    });

    $('.rating-field').keyup(function(event){
        var sum = 0;
        var numChanged = 0;
        $('.rating-field').each(function(i, obj) {
            if (isNumber($(this).val())) {
                sum += parseInt($(this).val());
                numChanged++;
            }
        });
        $('#cc_project_rating').val((sum/numChanged).toFixed(2));
    });
});

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

$('#printPDF').click(function(e) {
    if ($('#addDocuments:checkbox:checked').length > 0) {
        $('#alertPopup').find('.alert-modal-title').text('Notification');
        $('#alertPopup').find('.modal-body p').text('Please be advised that files which have pages with mixed orientation might be displayed wrong in the resulting PDF file. Please download them and print them manually from the documents page.');
        $('#alertPopup').modal();

        //<!-- Form confirm (yes/ok) handler, submits form -->
        $('.modal-footer .btn-success').on('click', function(){
            window.open($('#printPDF').attr("data-href")+'?documents=true' , '_blank');
        });
    } else {
        window.open($('#printPDF').attr("data-href") , '_blank');
    }
});

$('#addressbook_select_all').click(function(){
    // For some browsers, `attr` is undefined; for others,
    // `attr` is false.  Check for both.
    if ($('#addressbook_select_all').is(':checked')) {
        $('.address-book-chk').prop('checked', true);
    }
    else {
        $('.address-book-chk').removeAttr('checked');
    }
    generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
});

//select company admin checkbox
$(document).on("click", ".btn-undo", function () {
    //$('#company_admin').click(function() {
    $('#approveAction').modal().hide();

    $('#approveAction').find('.modal-title').text('Notification');
    $('#approveAction').find('.modal-body p').text('Please confirm your actions. All the Companies and Contacts from the last CSV Import will be deleted.');
    $('#approveAction').modal();

    //if confirmation is canceled
    $('[id="cancel_action"]').on('click', function(){
        $('#approveAction').modal().hide();
    });

    //if confirmation is approved
    $('[id="approve_action"]').on('click', function(){
        $('#approveAction').modal().hide();
        window.location = site_url+"/address-book/import/undo-last-action";
    });
});

$("#sel1").change(function() {
    var url = $(this).find(":selected").attr('data-href');
    window.location.href = url;
});