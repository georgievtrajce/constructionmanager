$('.download').click(function(){
    var fileId = $(this).parent().find('.s3FilePath').attr("id");
    var fileType = ($(this).parent().find('.s3FilePath').attr("file-type") != undefined)?$(this).parent().find('.s3FilePath').attr("file-type"):'projects';
    var s3FilePath = $(this).parent().find('.s3FilePath').val();
    $.ajax({
        url: site_url+"/check-download-allowance",
        type: 'POST',
        dataType: 'json',
        data: {
            fileId: fileId,
            fileType: fileType
        },
        success: function(data) {
            if(data.allowed == 1) {
                var filesize = data.filesize;
                var s3 = new AWS.S3();
                var params = {Bucket: 'cmsconstructionmanager', Key: s3FilePath};
                s3.getSignedUrl('getObject', params, function (err, url) {
                    console.log("The URL is", url);
                    if(window.open(url, "newPage")){
                        $.post( site_url+"/increase-download-transfer", { filesize: filesize, fileId: fileId, fileType: fileType}, function( data ) {
                            if(data == 'true') {
                                console.log('Download Transfer Limit Increased.');
                            } else {
                                console.log('Download Transfer Limit was not increased.');
                            }
                        });
                    }
                });
            } else {
                alert('File could not be downloaded because you reached your subscription level data transfer limitation.');
            }
        }
    });
});