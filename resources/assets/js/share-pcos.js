$(function() {

    $('#pcos_select_all').click(function(){
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ($('#pcos_select_all').is(':checked')) {
            $('.pco').prop('checked', true);
        }
        else {
            $('.pco').removeAttr('checked');
        }
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });
    //share selected files with selected companies - display input event
    $('#pco_share_with').change(function(){
        var value = $(this).val();
        if(value == 'all') {
            $('#selected_input_cont').css('display','none');
            $('.project-subcontractor-auto').val('');
            $('.project-subcontractor-id').val('');

            $('#selected_subcontractors_cont').css('display','none');
            $('#selected_subcontractors').empty();
        }
        if(value == 'selected') {
            $('#selected_input_cont').css('display','block');
            $('#selected_subcontractors_cont').css('display','block');
        }
    });

    //share files
    $('#share_pco_file').click(function(){
        //get selected files ids
        var selected_files = $("#unshared-files input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();

        //share files with all subcontractors
        if($('#pco_share_with').val() == 'all') {
            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/all/pcos",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        project_id: $('#project-id').val()
                    },
                    error: function(data) {
                        //alert('Please share this project with your companies subcontractors first before trying to share pcos.');
                        $('#alertPopup').find('.modal-body p').text('Please share this project with your companies subcontractors first before trying to share project files.');
                        $('#alertPopup').modal();
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared.');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
        //share files with selected companies
        else {
            //get selected subcontractors ids
            var selected_subcontractors = $(".subcontractor-id").map(function(){
                return $(this).val();
            }).get();

            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing.');
                $('#alertPopup').modal();
            } else if(selected_subcontractors.length === 0) {
                //alert('Please select companies for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select companies for sharing.');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/selected/pcos",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        selected_subcontractors: selected_subcontractors
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared.');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
    });


});
