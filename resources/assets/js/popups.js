//un-share project (Companies)
$(document).on('submit', '.unshare-form-prevent', function(e) {
    var form = this;
    e.preventDefault();
    console.log('prevented');

    $('#confirmUnshare').on('show.bs.modal', function (e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);

        // Pass form reference to modal for submission on yes/ok
        $(this).find('.modal-footer #confirm_unshare').data('form', form);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_unshare"]').on('click', function(){

        form.submit();
        //return true;
    });
});

//un-share project (Companies)
$(document).on('submit', '.share-form-prevent', function(e) {
    var form = this;
    e.preventDefault();
    console.log('prevented');

    $('#confirmShare').on('show.bs.modal', function (e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);

        // Pass form reference to modal for submission on yes/ok
        $(this).find('.modal-footer #confirm_share').data('form', form);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_share"]').on('click', function(){

        form.submit();
        //return true;
    });
});