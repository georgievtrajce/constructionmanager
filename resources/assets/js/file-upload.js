AWS.config.region = 'us-east-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: identityPool,
});

//get aws account bucket
var bucket = new AWS.S3({params: {Bucket: 'cmsconstructionmanager'}});

if (document.getElementById('rec_sub_file') != null) {
    //* //
    // Received from subcontractor - Submittal file upload
    // *//
    var fileChooserS1 = document.getElementById('rec_sub_file');
    var pathS1 = [];

    //upload button click event
    fileChooserS1.addEventListener('change', function() {
        var existingFileId = $('#rec_sub_file_id').val();

        //get file name
        var file = fileChooserS1.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#rec_sub_file_wait').css('display', 'block');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //store the file in database and return the file path with custom file name
            $.ajax({
                url: site_url + "/store-module-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    module1: url[6],
                    version: url[url.length-1],
                    exsistingFileId: existingFileId,
                    versionDate: 'rec_sub_file' //file and date connection string
                },
                success: function(data) {
                    //get the file path
                    pathS1 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#rec_sub_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#rec_sub_file_id').val(file_id);
                        $('#rec_sub_file_wait').css('display', 'none');
                        $('#rec_sub_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.rec_sub_file_message_container';
                        $('.rec_sub_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS1,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#rec_sub_file_id').val('');
                                $('#rec_sub_file_wait').css('display', 'none');

                                //populate  hidden file id value
                                $('#rec_sub_file_id').val(file_id);
                                $('#rec_sub_file_wait').css('display', 'none');
                                $('#rec_sub_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.rec_sub_file_message_container';
                                $('.rec_sub_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });

                                if (url[5] == 'shared') {
                                    window.removeEventListener('beforeunload', uploadHandler);
                                    window.location.reload();
                                }

                                //populate  hidden file id value
                                $('#rec_sub_file_id').val(file_id);
                                $('#rec_sub_file_wait').css('display', 'none');
                                $('#rec_sub_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.rec_sub_file_message_container';
                                $('.rec_sub_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS1,
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);

                                document.versionForm.submit();
                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}


if (document.getElementById('sent_appr_file') != null) {
    //* //
    // Sent for approval - Submittal file upload
    // *//
    var fileChooserS2 = document.getElementById('sent_appr_file');
    var pathS2 = [];

    //upload button click event
    fileChooserS2.addEventListener('change', function() {
        var existingFileId = $('#sent_appr_file_id').val();

        //get file name
        var file = fileChooserS2.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#sent_appr_file_wait').css('display', 'block');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //store the file in database and return the file path with custom file name
            $.ajax({
                url: site_url + "/store-module-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    exsistingFileId: existingFileId,
                    versionDate: 'sent_appr_file' //file and date connection string
                },
                success: function(data) {
                    //get the file path
                    pathS2 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#sent_appr_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#sent_appr_file_id').val(file_id);
                        $('#sent_appr_file_wait').css('display', 'none');
                        $('#sent_appr_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.sent_appr_file_message_container';
                        $('.sent_appr_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS2,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#sent_appr_file_id').val('');
                                $('#sent_appr_file_wait').css('display', 'none');

                                //populate  hidden file id value
                                $('#sent_appr_file_id').val(file_id);
                                $('#sent_appr_file_wait').css('display', 'none');
                                $('#sent_appr_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.sent_appr_file_message_container';
                                $('.rec_sub_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#sent_appr_file_id').val(file_id);
                                $('#sent_appr_file_wait').css('display', 'none');
                                $('#sent_appr_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.sent_appr_file_message_container';
                                $('.sent_appr_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS2,
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);

                                document.versionForm.submit();

                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}

if (document.getElementById('rec_appr_file') != null) {
    //* //
    // Received from approval - Submittal file upload
    // *//
    var fileChooserS3 = document.getElementById('rec_appr_file');
    var pathS3 = [];

    //upload button click event
    fileChooserS3.addEventListener('change', function() {
        var existingFileId = $('#rec_appr_file_id').val();

        //get file name
        var file = fileChooserS3.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#rec_appr_file_wait').css('display', 'block');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //store the file in database and return the file path with custom file name
            $.ajax({
                url: site_url + "/store-module-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    module1: url[6],
                    version: url[url.length-1],
                    exsistingFileId: existingFileId,
                    versionDate: 'rec_appr_file' //file and date connection string
                },
                success: function(data) {
                    //get the file path
                    pathS3 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#rec_appr_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#rec_appr_file_id').val(file_id);
                        $('#rec_appr_file_wait').css('display', 'none');
                        $('#rec_appr_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.rec_appr_file_message_container';
                        $('.rec_appr_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS3,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#rec_appr_file_id').val('');
                                $('#rec_appr_file_wait').css('display', 'none');

                                //populate  hidden file id value
                                $('#rec_appr_file_id').val(file_id);
                                $('#rec_appr_file_wait').css('display', 'none');
                                $('#rec_appr_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.rec_appr_file_message_container';
                                $('.rec_appr_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });

                                if (url[5] == 'shared') {
                                    window.removeEventListener('beforeunload', uploadHandler);
                                    window.location.reload();
                                }

                                //populate  hidden file id value
                                $('#rec_appr_file_id').val(file_id);
                                $('#rec_appr_file_wait').css('display', 'none');
                                $('#rec_appr_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.rec_appr_file_message_container';
                                $('.rec_appr_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS3,
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);

                                document.versionForm.submit();

                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}

if (document.getElementById('subm_sent_sub_file') != null) {
    //* //
    // Sent to subcontractor - Submittal file upload
    // *//
    var fileChooserS4 = document.getElementById('subm_sent_sub_file');
    var pathS4 = [];

    //upload button click event
    fileChooserS4.addEventListener('change', function() {
        var existingFileId = $('#subm_sent_sub_file_id').val();

        //get file name
        var file = fileChooserS4.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#subm_sent_sub_file_wait').css('display', 'block');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            //store the file in database and return the file path with custom file name
            $.ajax({
                url: site_url + "/store-module-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    exsistingFileId: existingFileId,
                    versionDate: 'subm_sent_sub_file' //file and date connection string
                },
                success: function(data) {
                    //get the file path
                    pathS4 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#subm_sent_sub_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#subm_sent_sub_file_id').val(file_id);
                        $('#subm_sent_sub_file_wait').css('display', 'none');
                        $('#subm_sent_sub_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.subm_sent_sub_file_message_container';
                        $('.subm_sent_sub_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS4,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#subm_sent_sub_file_id').val('');
                                $('#subm_sent_sub_file_wait').css('display', 'none');

                                //populate  hidden file id value
                                $('#subm_sent_sub_file_id').val(file_id);
                                $('#subm_sent_sub_file_wait').css('display', 'none');
                                $('#subm-sent_sub-upload').removeAttr('disabled');
                                $('#subm_sent_sub_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.subm_sent_sub_file_message_container';
                                $('.subm_sent_sub_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#subm_sent_sub_file_id').val(file_id);
                                $('#subm_sent_sub_file_wait').css('display', 'none');
                                $('#subm_sent_sub_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.subm_sent_sub_file_message_container';
                                $('.subm_sent_sub_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS4,
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);

                                document.versionForm.submit();

                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}

//upload first proposal file
if (document.getElementById('proposal_one_file') != null) {

    //* //
    // Project files, proposals and contracts upload
    // *//
    var fileChooserS5_p1 = document.getElementById('proposal_one_file');
    var buttonS5_p1 = document.getElementById('proposal_one_upload');
    var pathS5_p1 = [];

    //upload button click event
    buttonS5_p1.addEventListener('click', function() {
        var existingFileId = $('#proposal_one_file_id').val();

        //get file name
        var file = fileChooserS5_p1.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#proposal_one_file_wait').css('display', 'block');
            $('#proposal_one_file_upload').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            $.ajax({
                url: site_url + "/store-proposal-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    exsistingFileId: existingFileId,
                    proposal_item_no: 1, //this value point first, second or final proposal (1, 2 or 3)
                    fileType: 'bid', //file type string
                    projectFileType: 'bids' //project file type (ex, drawings, specifications etc. - it is empty for contracts and proposals)
                },
                success: function(data) {
                    //get the file path
                    pathS5_p1 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#proposal_one_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#proposal_one_file_id').val(file_id);
                        $('#proposal_one_file_wait').css('display', 'none');
                        $('#proposal_one_file_upload').removeAttr('disabled');
                        $('#proposal_one_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.proposal_one_file_message_container';
                        $('.proposal_one_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS5_p1,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#proposal_one_file_id').val('');
                                $('#proposal_one_file_wait').css('display', 'none');
                                $('#proposal_one_file_upload').removeAttr('disabled');

                                //populate  hidden file id value
                                $('#proposal_one_file_id').val(file_id);
                                $('#proposal_one_file_wait').css('display', 'none');
                                $('#proposal_one_file_upload').removeAttr('disabled');
                                $('#proposal_one_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.proposal_one_file_message_container';
                                $('.proposal_one_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#proposal_one_file_id').val(file_id);
                                $('#proposal_one_file_wait').css('display', 'none');
                                $('#proposal_one_file_upload').removeAttr('disabled');
                                $('#proposal_one_file').val('');
                                //alert('File was uploaded');

                                var templateId = '.successful-upload';
                                var container = '.proposal_one_file_message_container';
                                $('.proposal_one_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS5_p1,
                                    type: 'bids',
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);
                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);

}

//upload second proposal file
if (document.getElementById('proposal_two_file') != null) {

    //* //
    // Project files, proposals and contracts upload
    // *//
    var fileChooserS5_p2 = document.getElementById('proposal_two_file');
    var buttonS5_p2 = document.getElementById('proposal_two_upload');
    var pathS5_p2 = [];

    //upload button click event
    buttonS5_p2.addEventListener('click', function() {
        var existingFileId = $('#proposal_two_file_id').val();

        //get file name
        var file = fileChooserS5_p2.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#proposal_two_file_wait').css('display', 'block');
            $('#proposal_two_file_upload').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            $.ajax({
                url: site_url + "/store-proposal-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    exsistingFileId: existingFileId,
                    proposal_item_no: 2, //this value point first, second or final proposal (1, 2 or 3)
                    fileType: 'bid', //file type string
                    projectFileType: 'bids' //project file type (ex, drawings, specifications etc. - it is empty for contracts and proposals)
                },
                success: function(data) {
                    //get the file path
                    pathS5_p2 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#proposal_two_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#proposal_two_file_id').val(file_id);
                        $('#proposal_two_file_wait').css('display', 'none');
                        $('#proposal_two_file_upload').removeAttr('disabled');
                        $('#proposal_two_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.proposal_two_file_message_container';
                        $('.proposal_two_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS5_p2,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#proposal_two_file_id').val('');
                                $('#proposal_two_file_wait').css('display', 'none');
                                $('#proposal_two_file_upload').removeAttr('disabled');
                                //alert('There was some problem with the file upload. Please try again.');

                                //populate  hidden file id value
                                $('#proposal_two_file_id').val(file_id);
                                $('#proposal_two_file_wait').css('display', 'none');
                                $('#proposal_two_file_upload').removeAttr('disabled');
                                $('#proposal_two_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.proposal_two_file_message_container';
                                $('.proposal_two_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#proposal_two_file_id').val(file_id);
                                $('#proposal_two_file_wait').css('display', 'none');
                                $('#proposal_two_file_upload').removeAttr('disabled');
                                $('#proposal_two_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.proposal_two_file_message_container';
                                $('.proposal_two_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS5_p2,
                                    type: 'bids',
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);
                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);

}

//upload final proposal file
if (document.getElementById('proposal_final_file') != null) {

    //* //
    // Project files, proposals and contracts upload
    // *//
    var fileChooserS5_p3 = document.getElementById('proposal_final_file');
    var buttonS5_p3 = document.getElementById('proposal_final_upload');
    var pathS5_p3 = [];

    //upload button click event
    buttonS5_p3.addEventListener('click', function() {
        var existingFileId = $('#proposal_final_file_id').val();

        //get file name
        var file = fileChooserS5_p3.files[0];

        if (file) {
            //show uploading gif and disable upload button
            $('#proposal_final_file_wait').css('display', 'block');
            $('#proposal_final_file_upload').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            $.ajax({
                url: site_url + "/store-proposal-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    projectId: url[4],
                    module: url[5],
                    exsistingFileId: existingFileId,
                    proposal_item_no: 3, //this value point first, second or final proposal (1, 2 or 3)
                    fileType: 'bid', //file type string
                    projectFileType: 'bids' //project file type (ex, drawings, specifications etc. - it is empty for contracts and proposals)
                },
                success: function(data) {
                    //get the file path
                    pathS5_p3 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;
                    var file_id_holder = '#proposal_final_file_id';

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#proposal_final_file_id').val(file_id);
                        $('#proposal_final_file_wait').css('display', 'none');
                        $('#proposal_final_file_upload').removeAttr('disabled');
                        $('#proposal_final_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.proposal_final_file_message_container';
                        $('.proposal_final_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS5_p3,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#proposal_final_file_id').val('');
                                $('#proposal_final_file_wait').css('display', 'none');
                                $('#proposal_final_file_upload').removeAttr('disabled');

                                //populate  hidden file id value
                                $('#proposal_final_file_id').val(file_id);
                                $('#proposal_final_file_wait').css('display', 'none');
                                $('#proposal_final_file_upload').removeAttr('disabled');
                                $('#proposal_final_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.proposal_final_file_message_container';
                                $('.proposal_final_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#proposal_final_file_id').val(file_id);
                                $('#proposal_final_file_wait').css('display', 'none');
                                $('#proposal_final_file_upload').removeAttr('disabled');
                                $('#proposal_final_file').val('');

                                var templateId = '.successful-upload';
                                var container = '.proposal_final_file_message_container';
                                $('.proposal_final_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS5_p3,
                                    type: 'bids',
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);
                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);

}

//upload other module files
if (document.getElementById('file') != null && document.getElementById('file-upload-create') != undefined) {

    //* //
    // Project files and contracts upload
    // *//
    var fileChooserS5 = document.getElementById('file');
    var buttonS5 = document.getElementById('file-upload-create');
    var pathS5 = [];

    //upload button click event
    buttonS5.addEventListener('click', function() {
        var existingFileId = $('#file_id').val();

        //get file name
        var files = fileChooserS5.files;

        if (files.length > 0) {

            //show uploading gif and disable upload button
            $('#file_wait').css('display', 'block');
            $('#file-upload-create').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //iterate through all files
            for (var i = 0; i < files.length; i++) {
                file = files[i];

                //get the file extension
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

                //store the file in database and return the file path with custom file name
                if (document.getElementById('supplier_id') != null) {
                    var supplierId = $('#supplier_id').val();
                } else {
                    var supplierId = '';
                }

                if (document.getElementById('contract_id') != null) {
                    var contractId = $('#contract_id').val();
                } else {
                    var contractId = '';
                }

                var fileType = $('#file_type').val();

                $.ajax({
                    url: site_url + "/store-file",
                    dataType: "json",
                    data: {
                        filename: file.name,
                        filesize: file.size,
                        i: i,
                        type: extension,
                        projectId: url[4],
                        exsistingFileId: existingFileId,
                        supplierId: supplierId, //if the file is a proposal get the supplier id
                        contractId: contractId, //if the file is a contract get the contract id
                        fileType: fileType, //file type string
                        projectFileType: $('#project_file_type').val() //project file type (ex, drawings, specifications etc. - it is empty for contracts and proposals)
                    },
                    success: function(data) {
                        var fileSored = files[data.i];
                        //get the file path
                        pathS5 = data.full_file_path;

                        //get the file id
                        var file_id = data.file_id;

                        var file_name = data.file_name;
                        var file_id_holder = '#file_id';

                        if (data.not_allowed == 1) {
                            //populate  hidden file id value
                            $('#file_id').val(file_id);
                            $('#file_wait').css('display', 'none');
                            $('#file_upload').removeAttr('disabled');
                            $('#file').val('');

                            var limitTemplateId = '.upload-limitation';
                            var limitContainer = '.file_message_container';
                            //$('.file_message_container').empty();
                            cm.toggleTemplate(limitTemplateId, limitContainer, {
                                type: data.not_allowed_type
                            }).add();
                        } else {
                            var params = {
                                Key: pathS5,
                                ContentType: fileSored.type,
                                Body: fileSored
                            };

                            function uploadHandler(e) {
                                var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                            }

                            window.addEventListener("beforeunload", uploadHandler);

                            bucket.upload(params, function(err, data) {
                                if (err) {
                                    //empty hidden file id value
                                    $('#file_id').val('');
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload-create').removeAttr('disabled');

                                    //populate  hidden file id value
                                    if ($('#file_id').val() == "") {
                                        $('#file_id').val(file_id);
                                    } else {
                                        $('#file_id').val($('#file_id').val() + "-" + file_id);
                                    }

                                    $('#file_wait').css('display', 'none');
                                    $('#file_upload').removeAttr('disabled');
                                    $('#file').val('');

                                    var errTemplateId = '.error-upload';
                                    var errContainer = '.file_message_container';
                                    //$('.file_message_container').empty();
                                    cm.toggleTemplate(errTemplateId, errContainer).add();
                                } else {
                                    $.post(site_url + "/increase-upload-transfer", {
                                        filesize: fileSored.size
                                    }, function(data) {
                                        if (data == 'true') {
                                            console.log('Upload Transfer Limit Increased.');
                                        } else {
                                            console.log('Upload Transfer Limit was not increased.');
                                        }
                                    });
                                    //populate  hidden file id value
                                    //populate  hidden file id value
                                    if ($('#file_id').val() == "") {
                                        $('#file_id').val(file_id);
                                    } else {
                                        $('#file_id').val($('#file_id').val() + "-" + file_id);
                                    }
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload-create').removeAttr('disabled');
                                    $('#file').val('');
                                    //alert('File was uploaded');

                                    var templateId = '.successful-upload-multi';
                                    var container = '.file_message_container';
                                    //$('.file_message_container').empty();

                                    cm.toggleTemplate(templateId, container, {
                                        file_name: file_name,
                                        file_id: file_id,
                                        file_path: pathS5,
                                        type: fileType,
                                        file_id_holder: file_id_holder
                                    }).add();

                                    //remove beforeunload prevent event
                                    window.removeEventListener('beforeunload', uploadHandler);
                                }
                            });
                        }
                    }
                });

            }


        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}

//upload tasks files
if (document.getElementById('file') != null && document.getElementById('file-upload-create-task') != undefined) {

    //* //
    // Project files and contracts upload
    // *//
    var fileChooserS5 = document.getElementById('file');
    var buttonS5 = document.getElementById('file-upload-create-task');
    var pathS5 = [];

    //upload button click event
    buttonS5.addEventListener('click', function() {
        var existingFileId = $('#file_id').val();

        //get file name
        var files = fileChooserS5.files;

        if (files.length > 0) {

            //show uploading gif and disable upload button
            $('#file_wait').css('display', 'block');
            $('#file-upload-create-task').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //iterate through all files
            for (var i = 0; i < files.length; i++) {
                file = files[i];

                //get the file extension
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

                $.ajax({
                    url: site_url + "/store-file-task",
                    dataType: "json",
                    data: {
                        filename: file.name,
                        filesize: file.size,
                        i: i,
                        type: extension,
                        exsistingFileId: existingFileId
                    },
                    success: function(data) {
                        var fileSored = files[data.i];
                        //get the file path
                        pathS5 = data.full_file_path;

                        //get the file id
                        var file_id = data.file_id;

                        var file_name = data.file_name;
                        var file_id_holder = '#file_id';

                        if (data.not_allowed == 1) {
                            //populate  hidden file id value
                            $('#file_id').val(file_id);
                            $('#file_wait').css('display', 'none');
                            $('#file_upload').removeAttr('disabled');
                            $('#file').val('');

                            var limitTemplateId = '.upload-limitation';
                            var limitContainer = '.file_message_container';
                            //$('.file_message_container').empty();
                            cm.toggleTemplate(limitTemplateId, limitContainer, {
                                type: data.not_allowed_type
                            }).add();
                        } else {
                            var params = {
                                Key: pathS5,
                                ContentType: fileSored.type,
                                Body: fileSored
                            };

                            function uploadHandler(e) {
                                var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                            }

                            window.addEventListener("beforeunload", uploadHandler);

                            bucket.upload(params, function(err, data) {
                                if (err) {
                                    //empty hidden file id value
                                    $('#file_id').val('');
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload-create-task').removeAttr('disabled');

                                    //populate  hidden file id value
                                    if ($('#file_id').val() == "") {
                                        $('#file_id').val(file_id);
                                    } else {
                                        $('#file_id').val($('#file_id').val() + "-" + file_id);
                                    }

                                    $('#file_wait').css('display', 'none');
                                    $('#file_upload').removeAttr('disabled');
                                    $('#file').val('');

                                    var errTemplateId = '.error-upload';
                                    var errContainer = '.file_message_container';
                                    //$('.file_message_container').empty();
                                    cm.toggleTemplate(errTemplateId, errContainer).add();
                                } else {
                                    $.post(site_url + "/increase-upload-transfer", {
                                        filesize: fileSored.size
                                    }, function(data) {
                                        if (data == 'true') {
                                            console.log('Upload Transfer Limit Increased.');
                                        } else {
                                            console.log('Upload Transfer Limit was not increased.');
                                        }
                                    });
                                    //populate  hidden file id value
                                    //populate  hidden file id value
                                    if ($('#file_id').val() == "") {
                                        $('#file_id').val(file_id);
                                    } else {
                                        $('#file_id').val($('#file_id').val() + "-" + file_id);
                                    }
                                    $('.currentFile').css('display', 'none');
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload-create-task').removeAttr('disabled');
                                    $('#file').val('');
                                    //alert('File was uploaded');

                                    var templateId = '.successful-upload-multi';
                                    var container = '.file_message_container';
                                    //$('.file_message_container').empty();

                                    cm.toggleTemplate(templateId, container, {
                                        file_name: file_name,
                                        file_id: file_id,
                                        file_path: pathS5,
                                        type: 'tasks',
                                        file_id_holder: file_id_holder
                                    }).add();

                                    //remove beforeunload prevent event
                                    window.removeEventListener('beforeunload', uploadHandler);
                                }
                            });
                        }
                    }
                });

            }


        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);
}

//edit project files
if (document.getElementById('file') != null) {

    //* //
    // Project files and contracts upload
    // *//
    var fileChooserS5 = document.getElementById('file');
    var buttonS5 = document.getElementById('file-upload');
    var pathS5 = [];

    //upload button click event
    if (buttonS5 !== null) {
        buttonS5.addEventListener('click', function() {
            var existingFileId = $('#file_id').val();

            //get file name
            var file = fileChooserS5.files[0];

            if (file) {
                //show uploading gif and disable upload button
                $('#file_wait').css('display', 'block');
                $('#file-upload').attr('disabled', 'disabled');

                //parse url in order to get the project id
                var url = $(location).attr('href').split("/");

                //get the file extension
                var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

                //store the file in database and return the file path with custom file name
                if (document.getElementById('supplier_id') != null) {
                    var supplierId = $('#supplier_id').val();
                } else {
                    var supplierId = '';
                }

                if (document.getElementById('contract_id') != null) {
                    var contractId = $('#contract_id').val();
                } else {
                    var contractId = '';
                }

                var fileType = $('#file_type').val();

                $.ajax({
                    url: site_url + "/store-file",
                    dataType: "json",
                    data: {
                        filename: file.name,
                        filesize: file.size,
                        type: extension,
                        projectId: url[4],
                        exsistingFileId: existingFileId,
                        supplierId: supplierId, //if the file is a proposal get the supplier id
                        contractId: contractId, //if the file is a contract get the contract id
                        fileType: fileType, //file type string
                        projectFileType: $('#project_file_type').val() //project file type (ex, drawings, specifications etc. - it is empty for contracts and proposals)
                    },
                    success: function(data) {
                        //get the file path
                        pathS5 = data.full_file_path;

                        //get the file id
                        var file_id = data.file_id;

                        var file_name = data.file_name;
                        var file_id_holder = '#file_id';

                        if (data.not_allowed == 1) {
                            //populate  hidden file id value
                            $('#file_id').val(file_id);
                            $('#file_wait').css('display', 'none');
                            $('#file_upload').removeAttr('disabled');
                            $('#file').val('');

                            var limitTemplateId = '.upload-limitation';
                            var limitContainer = '.file_message_container';
                            $('.file_message_container').empty();
                            cm.toggleTemplate(limitTemplateId, limitContainer, {
                                type: data.not_allowed_type
                            }).add();
                        } else {
                            var params = {
                                Key: pathS5,
                                ContentType: file.type,
                                Body: file
                            };

                            function uploadHandler(e) {
                                var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                                (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                                return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                            }

                            window.addEventListener("beforeunload", uploadHandler);

                            bucket.upload(params, function(err, data) {
                                if (err) {
                                    //empty hidden file id value
                                    $('#file_id').val('');
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload').removeAttr('disabled');

                                    //populate  hidden file id value
                                    $('#file_id').val(file_id);
                                    $('#file_wait').css('display', 'none');
                                    $('#file_upload').removeAttr('disabled');
                                    $('#file').val('');

                                    var errTemplateId = '.error-upload';
                                    var errContainer = '.file_message_container';
                                    $('.file_message_container').empty();
                                    cm.toggleTemplate(errTemplateId, errContainer).add();
                                } else {
                                    $.post(site_url + "/increase-upload-transfer", {
                                        filesize: file.size
                                    }, function(data) {
                                        if (data == 'true') {
                                            console.log('Upload Transfer Limit Increased.');
                                        } else {
                                            console.log('Upload Transfer Limit was not increased.');
                                        }
                                    });
                                    //populate  hidden file id value
                                    $('#file_id').val(file_id);
                                    $('#file_wait').css('display', 'none');
                                    $('#file-upload').removeAttr('disabled');
                                    $('#file').val('');
                                    //alert('File was uploaded');

                                    var templateId = '.successful-upload';
                                    var container = '.file_message_container';
                                    $('.file_message_container').empty();

                                    cm.toggleTemplate(templateId, container, {
                                        file_name: file_name,
                                        file_id: file_id,
                                        file_path: pathS5,
                                        type: fileType,
                                        file_id_holder: file_id_holder
                                    }).add();

                                    //remove beforeunload prevent event
                                    window.removeEventListener('beforeunload', uploadHandler);
                                }
                            });
                        }
                    }
                });
            } else {
                //alert('Nothing to upload.');
                $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
                $('#alertPopup').modal();
            }
        }, false);
    }
}

//upload final proposal file
if (document.getElementById('address_book_file') != null) {

    //* //
    // Project files, proposals and contracts upload
    // *//
    var fileChooserS5_p3 = document.getElementById('address_book_file');
    var buttonS5_p3 = document.getElementById('address-book-file-upload');
    var pathS5_p3 = [];

    //upload button click event
    buttonS5_p3.addEventListener('click', function() {
        var existingFileId = $('#address_book_file_id').val();

        //get file name
        var file = fileChooserS5_p3.files[0];
        var file_id_holder = '#address_book_file_id';

        if (file) {
            //show uploading gif and disable upload button
            $('#address_book_file_wait').css('display', 'block');
            $('#address-book-file-upload').attr('disabled', 'disabled');

            //parse url in order to get the project id
            var url = $(location).attr('href').split("/");

            //get the file extension
            var extension = file.name.substr((file.name.lastIndexOf('.') + 1));

            $.ajax({
                url: site_url + "/store-address-book-file",
                dataType: "json",
                data: {
                    filename: file.name,
                    filesize: file.size,
                    type: extension,
                    contactId: url[4],
                    exsistingFileId: existingFileId,
                    fileType: 'address_book_document', //file type string
                },
                success: function(data) {
                    //get the file path
                    pathS5_p3 = data.full_file_path;

                    //get the file id
                    var file_id = data.file_id;

                    var file_name = data.file_name;

                    if (data.not_allowed == 1) {
                        //populate  hidden file id value
                        $('#address_book_file_id').val(file_id);
                        $('#address_book_file_wait').css('display', 'none');
                        $('#address-book-file-upload').removeAttr('disabled');
                        $('#address_book_file').val('');

                        var limitTemplateId = '.upload-limitation';
                        var limitContainer = '.address_book_file_message_container';
                        $('.address_book_file_message_container').empty();
                        cm.toggleTemplate(limitTemplateId, limitContainer, {
                            type: data.not_allowed_type
                        }).add();
                    } else {
                        var params = {
                            Key: pathS5_p3,
                            ContentType: file.type,
                            Body: file
                        };

                        function uploadHandler(e) {
                            var confirmationMessage = 'It looks like you have been editing something. ' + 'If you leave before saving, your changes will be lost.';

                            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
                            return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
                        }

                        window.addEventListener("beforeunload", uploadHandler);

                        bucket.upload(params, function(err, data) {
                            if (err) {
                                //empty hidden file id value
                                $('#address_book_file_id').val('');
                                $('#address_book_file_wait').css('display', 'none');
                                $('#address-book-file-upload').removeAttr('disabled');

                                //populate  hidden file id value
                                $('#address_book_file_id').val(file_id);
                                $('#address_book_file_wait').css('display', 'none');
                                $('#address-book-file-upload').removeAttr('disabled');
                                $('#address_book_file').val('');

                                var errTemplateId = '.error-upload';
                                var errContainer = '.address_book_file_message_container';
                                $('.address_book_file_message_container').empty();
                                cm.toggleTemplate(errTemplateId, errContainer).add();
                            } else {
                                $.post(site_url + "/increase-upload-transfer", {
                                    filesize: file.size
                                }, function(data) {
                                    if (data == 'true') {
                                        console.log('Upload Transfer Limit Increased.');
                                    } else {
                                        console.log('Upload Transfer Limit was not increased.');
                                    }
                                });
                                //populate  hidden file id value
                                $('#address_book_file_id').val(file_id);
                                $('#address_book_file_wait').css('display', 'none');
                                $('#address-book-file-upload').removeAttr('disabled');
                                $('#address_book_file').val('');
                                $('#currentFile').css('display', 'none');

                                var templateId = '.successful-upload';
                                var container = '.address_book_file_message_container';
                                $('.address_book_file_message_container').empty();

                                cm.toggleTemplate(templateId, container, {
                                    file_name: file_name,
                                    file_id: file_id,
                                    file_path: pathS5_p3,
                                    type: 'address_book',
                                    file_id_holder: file_id_holder
                                }).add();

                                //remove beforeunload prevent event
                                window.removeEventListener('beforeunload', uploadHandler);
                            }
                        });
                    }
                }
            });
        } else {
            //alert('Nothing to upload.');
            $('#alertPopup').find('.modal-body p').text('Nothing to upload.');
            $('#alertPopup').modal();
        }
    }, false);

}

var globalEditor;

function uploadTinyMceImage(editor) {
    $('#uploadS3PopUp').modal('toggle');
    globalEditor = editor;
}


$('#confirm_tiny_upload').on('click', function() {
    $('.tiny-image-form').submit();
});

var request;

// Bind to the submit event of our form
$(".tiny-image-form").submit(function(event) {
    // Abort any pending request
    if (request) {
        request.abort();
    }

    //show waiting
    $('#tiny_file_wait').css('display', 'block');
    $('#confirm_tiny_upload').attr('disabled', 'disabled');

    var formData = new FormData();
    formData.append('file', $('#tiny_file')[0].files[0]);


    // Fire off the request to /form.php
    request = $.ajax({
        url: tinyImageUrl,
        type: "post",
        data: formData,
        processData: false, // tell jQuery not to process the data
        contentType: false, // tell jQuery not to set contentType
    });

    // Callback handler that will be called on success
    request.done(function(response, textStatus, jqXHR) {
        // Log a message to the console
        if (response.status == "Success") {

            //hide waiting
            $('#tiny_file_wait').css('display', 'none');
            $('#confirm_tiny_upload').removeAttr('disabled');
            $('#tiny_file').val('');

            //insert image
            globalEditor.insertContent('<img src="' + cloudFrontUrl + '/' + response.path + '" />');

            $('#uploadS3PopUp').modal('hide');

        } else if (response.status == "Error") {

            $('#tiny_file_wait').css('display', 'none');
            $('#confirm_tiny_upload').removeAttr('disabled');
            $('#tiny_file').val('');

            console.error(
                response.message +
                textStatus, errorThrown
            );
        }
    });

    // Callback handler that will be called on failure
    request.fail(function(jqXHR, textStatus, errorThrown) {
        $('#tiny_file_wait').css('display', 'none');
        $('#confirm_tiny_upload').removeAttr('disabled');
        $('#tiny_file').val('');
        alert('Please select a valid file.');
        // Log the error to the console
        console.error(
            "The following error occurred: " +
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function() {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });

    // Prevent default posting of form
    event.preventDefault();
});