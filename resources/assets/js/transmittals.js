//generate sent for approval transmittal
$('#generateRecTransmittal').click(function () {
    // $('#rec_transmittal_wait').css('display', 'block');
    $('#generateRecTransmittal').attr('disabled', 'disabled');

    var loader = $(this).find('#rec_transmittal_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');

    var type = $('#type').val();
    var databaseType = $('#databaseType').val();
    var versionId = $('#versionId').val();
    var projectId = $('#projectId').val();
    var recTransmittalDate = $('#recTransmittalDate').val();

    //generate transmittal function
    var generateTransmittal = function (type, databaseType, versionId, projectId, recTransmittalDate) {
        $.ajax({
            url: site_url + "/generate-transmittal/recipient",
            type: 'POST',
            dataType: 'json',
            data: {
                type: type,
                databaseType: databaseType,
                versionId: versionId,
                projectId: projectId,
                recTransmittalDate: recTransmittalDate
            },
            success: function (data) {
                if (data.generate == 1) {
                    //alert('Transmittal was successfully generated. Please check your module transmittals list.');

                    var fileId = data.fileId;
                    var s3FilePath = data.url;
                    window.open("about:blank", "newPage");
                    $.ajax({
                        url: site_url + "/check-download-allowance",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            fileId: fileId
                        },
                        success: function (data) {
                            if (data.allowed == 1) {
                                var filesize = data.filesize;
                                var s3 = new AWS.S3();
                                var params = {Bucket: 'cmsconstructionmanager', Key: s3FilePath};
                                s3.getSignedUrl('getObject', params, function (err, url) {
                                    if (window.open(url, "newPage")) {
                                        $.post(site_url + "/increase-download-transfer", {
                                            filesize: filesize,
                                            fileId: fileId
                                        }, function (data) {
                                            if (data == 'true') {
                                                console.log('Upload Transfer Limit Increased.');
                                            } else {
                                                console.log('Upload Transfer Limit was not increased.');
                                            }
                                        });
                                        var url = [location.protocol, '//', location.host, location.pathname].join('');
                                        window.location = url + '?tab=' + recTransmittalDate
                                    }
                                });
                            } else {
                                //alert('File could not be downloaded because you reached your subscription level data transfer limitation.');
                                $('#alertPopup').find('.modal-body p').text('File could not be downloaded because you reached your subscription level data transfer limitation.');
                                $('#alertPopup').modal();
                            }
                        }
                    });
                } else {
                    //alert(data.message);
                    $('#alertPopup').find('.modal-body p').text(data.message);
                    $('#alertPopup').modal();
                }
                // $('#rec_transmittal_wait').css('display', 'none');
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                $('#generateRecTransmittal').removeAttr('disabled');
            }
        });
    };

    //check if transmittal file is already generated
    $.ajax({
        url: site_url + "/check-existing-transmittal",
        type: 'POST',
        dataType: 'json',
        data: {
            type: type,
            versionId: versionId,
            projectId: projectId,
            transmittalDate: recTransmittalDate,
            databaseType: databaseType
        },
        success: function (data) {
            if (data.response) {
                var result = 0;
                $('#approveAction').find('.modal-body p').text('Sent for Approval Transmittal is already generated for this entry. Do you want to re-create it again?');
                $('#approveAction').modal();

                //<!-- Form confirm (yes/ok) handler, submits form -->
                $('[id="approve_action"]').on('click', function () {
                    result = 1;
                    $('#approveAction').modal('toggle');

                    updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, recTransmittalDate);
                });

                if (result === 0) {
                    // $('#rec_transmittal_wait').css('display', 'none');
                    loader.css('display', 'none');
                    glyphicon.css('display', 'inline-block');
                    $('#generateRecTransmittal').removeAttr('disabled');
                }
            } else {
                updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, recTransmittalDate);
            }
        }
    });

    var updateNotesAndGenerateTransmittal = function (type, databaseType, versionId, projectId, recTransmittalDate) {
        tinymce.triggerSave();
        $.ajax({
            type: "POST",
            url: $("#notesForm").attr('action'),
            data: $("#notesForm").serialize(), // serializes the form's elements.
            success: function (data) {
                generateTransmittal(type, databaseType, versionId, projectId, recTransmittalDate);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                var obj = $.parseJSON(xhr.responseText);
                alert(obj.version_question);
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                $('#generateRecTransmittal').removeAttr('disabled');
            }
        });
    }
});



$('#generateSubTransmittal').click(function () {
    // $('#sub_transmittal_wait').css('display', 'block');
    $('#generateSubTransmittal').attr('disabled', 'disabled');

    var loader = $(this).find('#sub_transmittal_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');

    var type = $('#type').val();
    var databaseType = $('#databaseType').val();
    var versionId = $('#versionId').val();
    var projectId = $('#projectId').val();
    var subTransmittalDate = $('#subTransmittalDate').val();

    var generateTransmittal = function (type, databaseType, versionId, projectId, subTransmittalDate) {
        $.ajax({
            url: site_url + "/generate-transmittal/subcontractor",
            type: 'POST',
            dataType: 'json',
            data: {
                type: type,
                databaseType: databaseType,
                versionId: versionId,
                projectId: projectId,
                subTransmittalDate: subTransmittalDate
            },
            success: function (data) {
                if (data.generate == 1) {
                    //alert('Transmittal was successfully generated. Please check your module transmittals list.');

                    var fileId = data.fileId;
                    var s3FilePath = data.url;
                    window.open("about:blank", "newPage");
                    $.ajax({
                        url: site_url + "/check-download-allowance",
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            fileId: fileId
                        },
                        success: function (data) {
                            if (data.allowed == 1) {
                                var filesize = data.filesize;
                                var s3 = new AWS.S3();
                                var params = {Bucket: 'cmsconstructionmanager', Key: s3FilePath};
                                s3.getSignedUrl('getObject', params, function (err, url) {
                                    console.log("The URL is", url);
                                    if (window.open(url, "newPage")) {
                                        $.post(site_url + "/increase-download-transfer", {
                                            filesize: filesize,
                                            fileId: fileId
                                        }, function (data) {
                                            if (data == 'true') {
                                                console.log('Upload Transfer Limit Increased.');
                                            } else {
                                                console.log('Upload Transfer Limit was not increased.');
                                            }
                                        });
                                        var url = [location.protocol, '//', location.host, location.pathname].join('');
                                        window.location = url + '?tab=' + subTransmittalDate
                                    }
                                });
                            } else {
                                //alert('File could not be downloaded because you reached your subscription level data transfer limitation.');
                                $('#alertPopup').find('.modal-body p').text('File could not be downloaded because you reached your subscription level data transfer limitation.');
                                $('#alertPopup').modal();
                            }
                        }
                    });
                } else {
                    //alert(data.message);
                    $('#alertPopup').find('.modal-body p').text(data.message);
                    $('#alertPopup').modal();
                }
                // $('#sub_transmittal_wait').css('display', 'none');
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                $('#generateSubTransmittal').removeAttr('disabled');
            }
        });
    };

    //check if transmittal file is already generated
    $.ajax({
        url: site_url + "/check-existing-transmittal",
        type: 'POST',
        dataType: 'json',
        data: {
            type: type,
            versionId: versionId,
            projectId: projectId,
            transmittalDate: subTransmittalDate,
            databaseType: databaseType
        },
        success: function (data) {
            if (data.response) {
                var result = 0;
                $('#approveAction').find('.modal-body p').text('Sent to Subcontractor Transmittal is already generated for this entry. Do you want to re-create it again?');
                $('#approveAction').modal();

                //<!-- Form confirm (yes/ok) handler, submits form -->
                $('[id="approve_action"]').on('click', function () {
                    result = 1;
                    $('#approveAction').modal('toggle');
                    updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
                });

                if (result === 0) {
                    // $('#sub_transmittal_wait').css('display', 'none');
                    loader.css('display', 'none');
                    glyphicon.css('display', 'inline-block');
                    $('#generateSubTransmittal').removeAttr('disabled');
                }
            } else {
                updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
            }
        }
    });

    var updateNotesAndGenerateTransmittal = function (type, databaseType, versionId, projectId, subTransmittalDate) {
        tinymce.triggerSave();
        $.ajax({
            type: "POST",
            url: $("#notesForm").attr('action'),
            data: $("#notesForm").serialize(), // serializes the form's elements.
            success: function (data) {
                generateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
            }
        });
    }
});

//send transmittal email notification
$('.sendTransmittalEmail').click(function () {
    var sendButton = $(this);

    var loader = $(this).find('.email_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');
    sendButton.attr('disabled', 'disabled');

    //get attributes with values
    var transmittalPath = sendButton.attr("data-transmittal-path");
    var transmittalId = sendButton.attr("data-transmittal-id");
    var subRec = sendButton.attr("data-sub-rec");
    var filePath = '';
    var fileUploadedId = '';
    if (sendButton.attr("data-file-uploaded-selector")) {
        var fileSelector = sendButton.attr("data-file-uploaded-selector");
        filePath = $('.' + fileSelector).val();
        fileUploadedId = $('.' + fileSelector).attr('id');
    }

    var subRecId = 0;
    if (sendButton.attr('data-sub-rec-id')) {
        subRecId = sendButton.attr('data-sub-rec-id')
    }
    var fileId = sendButton.attr("data-file-id");
    var emailed = sendButton.attr("data-emailed");
    var type = $('#databaseType').val();
    var module = $('#module').val();

    var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
    var pathArray = window.location.pathname.split('/');
    if (pathArray[5] == 'version') {
        var versionId = pathArray[6];
    } else {
        var versionId = pathArray[8];
    }

    var entryId = pathArray[4];
    var projectId = pathArray[2];

    var users = [];
    var contacts = [];
    if (subRec === 'recipient' || type === 'pco') {
        $("input[name='employees_users_in[]']").each(function () {
            if ($(this).is(":checked")) {
                users.push($(this).val());
            }
        });

        $("input[name='employees_contacts_in[]']").each(function () {
            if ($(this).is(":checked")) {
                contacts.push($(this).val());
            }
        });
    } else {
        $("input[name='employees_users_out[]']").each(function () {
            if ($(this).is(":checked")) {
                users.push($(this).val());
            }
        });

        $("input[name='employees_contacts_out[]']").each(function () {
            if ($(this).is(":checked")) {
                contacts.push($(this).val());
            }
        });
    }

    function sendTransmittalEmail(transmittalId, fileId, transmittalPath) {
        $.ajax({
            type: "POST",
            url: site_url + "/send-transmittal-email-notification",
            dataType: "json",
            data: {
                type: type,
                module: module,
                entryId: entryId,
                subRec: subRec,
                subRecId: subRecId,
                transmittalId: transmittalId,
                fileId: fileId,
                projectId: projectId,
                versionId: versionId,
                transmittalPath: transmittalPath,
                filePath: filePath,
                fileUploadedId: fileUploadedId,
                users: users.join("|"),
                contacts: contacts.join("|")
            },
            success: function (data) {
                if (data.status === 'success') {
                    location.reload();
                } else {
                    //disable send button and show loading image
                    loader.css('display', 'none');
                    glyphicon.css('display', 'inline-block');
                    sendButton.removeAttr('disabled');
                    //alert(data.message);
                    $('#alertPopup').find('.modal-body p').text(data.message);
                    $('#alertPopup').modal();
                }

            }
        });
    }

    if (emailed == 1) {
        var result = 0;
        $('#approveAction').find('.modal-body p').text('The Transmittal has been already sent. Do you want to email the Transmittal again?');
        $('#approveAction').modal();

        //<!-- Form confirm (yes/ok) handler, submits form -->
        $('[id="approve_action"]').on('click', function () {
            result = 1;
            $('#approveAction').modal('toggle');
            sendTransmittalEmail(transmittalId, fileId, transmittalPath);

            if (result === 0) {
                //disable send button and show loading image
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                sendButton.removeAttr('disabled');
            }
        });

        $('[id="cancel_action"]').on('click', function () {
            //disable send button and show loading image
            sendButton.next().children().css('display', 'none');
            sendButton.removeAttr('disabled');
            loader.css('display', 'none');
            glyphicon.css('display', 'inline-block');
        });
    } else {
        sendTransmittalEmail(transmittalId, fileId, transmittalPath);
    }


});

// $(document).ready(function () {
//     $('#default_recipient').change(function () {
//         debugger;
//         if ($(this).is(":checked")) {
//             $('#select_contact').slideUp();
//             // $('.address-book-gc-auto').val('');
//             // $('.address-book-gc-id').val('');
//             $(this).attr("checked");
//         } else {
//             $('#select_contact').slideDown();
//             $(this).removeAttr("checked");
//         }
//     });
// });