tinymce.init({
    selector: ".textEditor",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    height : "300",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image uploadImage",
    setup: function (editor) {
        editor.addButton('uploadImage', {
            text: 'Upload image',
            icon: false,
            onclick: function () {
                uploadTinyMceImage(editor);
            }
        });
        editor.on('keyup', function (e) {
            var count = CountCharacters(editor.id);
            if (document.getElementById(editor.id+"_character_count") != undefined) {
                document.getElementById(editor.id+"_character_count").innerHTML = "Characters count: " + count;
            }
        });
    },
});

tinymce.init({
    selector: ".textEditorSmall",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    height : "100",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image uploadImage",
    setup: function (editor) {
        editor.addButton('uploadImage', {
            text: 'Upload image',
            icon: false,
            onclick: function () {
                uploadTinyMceImage(editor);
            }
        });
        editor.on('keyup', function (e) {
            var count = CountCharacters(editor.id);
            if (document.getElementById(editor.id+"_character_count") != undefined) {
                document.getElementById(editor.id+"_character_count").innerHTML = "Characters count: " + count;
            }
        });
    },
});

tinymce.init({
    selector: ".textEditorSmallVersion",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    height : "130",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image uploadImage",
    setup: function (editor) {
        editor.addButton('uploadImage', {
            text: 'Upload image',
            icon: false,
            onclick: function () {
                uploadTinyMceImage(editor);
            }
        });
        editor.on('keyup', function (e) {
            var count = CountCharacters(editor.id);
            if (document.getElementById(editor.id+"_character_count") != undefined) {
                document.getElementById(editor.id+"_character_count").innerHTML = "Characters count: " + count;
            }
        });
    },
});

tinymce.init({
    selector: ".textEditorSmallSubcontractors",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    height : "145",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image uploadImage",
    setup: function (editor) {
        editor.addButton('uploadImage', {
            text: 'Upload image',
            icon: false,
            onclick: function () {
                uploadTinyMceImage(editor);
            }
        });
        editor.on('keyup', function (e) {
            var count = CountCharacters(editor.id);
            if (document.getElementById(editor.id+"_character_count") != undefined) {
                document.getElementById(editor.id+"_character_count").innerHTML = "Characters count: " + count;
            }
        });
    },
});

function CountCharacters(id) {
    var body = tinymce.get(id).getBody();
    var content = tinymce.trim(body.innerText || body.textContent);
    return content.length;
};