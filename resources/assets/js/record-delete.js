$(document).on('submit', '.form-prevent', function(e) {
    var form = this;
    e.preventDefault();
    console.log('prevented');

    $('#confirmDelete').on('show.bs.modal', function (e) {
        $message = $(e.relatedTarget).attr('data-message');
        $(this).find('.modal-body p').text($message);
        $title = $(e.relatedTarget).attr('data-title');
        $(this).find('.modal-title').text($title);

        // Pass form reference to modal for submission on yes/ok
        $(this).find('.modal-footer #confirm_delete').data('form', form);
    });

    //<!-- Form confirm (yes/ok) handler, submits form -->
    $('[id="confirm_delete"]').on('click', function(){

        form.submit();
        //return true;
    });
});

$(document).on('click', '.delete-button-multi', function(e) {
    $('#delete-form').submit();
});