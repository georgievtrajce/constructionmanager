/**
 * Created by dragan.atanasov on 4/29/2015.
 */
$(function() {
    $( "#rec-sub" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( "#sent-appr" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( "#rec-appr" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( "#subm-sent-sub" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $("#start_date").datepicker();
    $("#end_date").datepicker();

    $( "#need_by" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( "#expiration-date" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( ".expiry_date" ).datepicker({
        changeMonth: true,
        changeYear: true
    });

    $( "#expirationDate" ).datepicker({
        changeMonth: true,
        changeYear: true
    });
});
