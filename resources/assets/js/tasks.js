$(".show-subtasks").click(function() {
    $(this).parent().parent().next().slideToggle( "slow", function() {
        // Animation complete.
    });
});

$('#task_project').change(function() {
    getOptionsForModuleItems('');
});

$('#module_type').change(function() {
    getOptionsForModuleItems('');
});

function getOptionsForModuleItems(id)
{
    var projectId = $('#task_project').val();
    var module = $('#module_type').val();

    loadAllUsers(projectId)

    if (projectId != "" && module != "0") {
        $("#module_item").load(site_url+"/ajax/module-items/"+projectId+"/"+module, function(){
            $("#module_item").val(id);
        });
    }
}

function loadAllUsers(projectId)
{
    if (projectId != "") {
        jQuery('.project-filter').each(function() {
            var projId = $(this).data('id');
            if (projId != projectId) {
                $(this).addClass('hide');
                $(this).removeClass('show');
            } else {
                $(this).addClass('show');
                $(this).removeClass('hide');
            }
        });
    }
}

$(function(){
    $('input[name="companyRadio"]').click(function(){
        if ($(this).attr("data-type") == "user") {
            $('#usersList').addClass('show').removeClass('hide');
            $('.contactsList').addClass('hide').removeClass('show');
        } else {
            $('#usersList').addClass('hide').removeClass('show');
            $('.contactsList').addClass('hide').removeClass('show');
            var showId = $(this).attr("data-id");
            $('#contactsList-'+showId).addClass('show').removeClass('hide');
        }
    });
    if (typeof idModuleItem !== "undefined" && idModuleItem != 0) {
        getOptionsForModuleItems(idModuleItem);
    }

    var projectId = $('#task_project').val();
    loadAllUsers(projectId)
});

//send transmittal email notification
$('.sendTaskNotification').click(function() {
    var sendButton = $(this);

    var loader = $(this).find('.email_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');
    sendButton.attr('disabled', 'disabled');

    var pathArray = window.location.pathname.split('/');
    var taskId = pathArray[2];

    sendTaskEmail(taskId);
});

function sendTaskEmail(taskId) {
    $.ajax({
        type: "GET",
        url: site_url + "/tasks/"+taskId+"/notifications",
        success: function(data) {
            if (data.status === 'success') {
                location.reload();
            } else {
                //disable send button and show loading image
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                sendButton.removeAttr('disabled');
                //alert(data.message);
                $('#alertPopup').find('.modal-body p').text(data.message);
                $('#alertPopup').modal();
            }
        }
    });
}