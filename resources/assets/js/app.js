$.material.init();
$.material.ripples(".cm-box-more");


$(function () {

    $('table').each(function () {
        var headers = $(this).find('th');
        $(headers).each(function (i) {
            var el = $(this);
            var title = el.html();
            var parent = el.parents('table:first');
            parent.find('td').filter(':nth-child(' + (i + 1) + ')').attr('data-title', title);
        });
    });

});

equalheight = function (container) {

    var currentTallest = 0,
        currentRowStart = 0,
        rowDivs = new Array(),
        $el,
        topPosition = 0;
    $(container).each(function () {

        $el = $(this);
        $($el).height('auto')
        topPostion = $el.position().top;

        if (currentRowStart != topPostion) {
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
            rowDivs.length = 0; // empty the array
            currentRowStart = topPostion;
            currentTallest = $el.height();
            rowDivs.push($el);
        } else {
            rowDivs.push($el);
            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
        }
    });
}





$(window).load(function () {
    equalheight('[data-equal-height]');
    setTimeout("equalheight('.box-cont-1 .card')", 100);
    // setTimeout("equalheight('.box-cont-2 .card')", 100);
});


$(window).resize(function () {
    equalheight('[data-equal-height]');
    equalheight('.box-cont-1 .card');
    // equalheight('.box-cont-2 .card');
});

$('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
    equalheight('[data-equal-height]');
    equalheight('.box-cont-1 .card');
});

$(document).ready(function () {
    $('#daily-reports-select').on('change', function (e) {
        var $optionSelected = $("option:selected", this);
        $optionSelected.tab('show')
    });
});

$(document).ready(function () {
    $('.multiple-items-checkbox').change(function () {
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });
});


$(document).ready(function () {
    //address-book documents
    $('.multiple-items-checkbox-docs').change(function () {
        generateSelectedItemIdsDelete('.multiple-items-checkbox-docs', '/delete');
    });
});

$(document).ready(function () {
    //address-book documents
    $('.multiple-items-checkbox-projects').change(function () {
        generateSelectedItemProjectIds('.multiple-items-checkbox-projects');
    });
});

$(document).ready(function () {
    //address-book documents
    $('.multiple-items-checkbox-companies').change(function () {
        generateSelectedItemCompanyIds('.multiple-items-checkbox-companies');
    });
});

$(document).ready(function () {
    //address-book documents
    $('.multiple-items-checkbox-bidder').change(function () {
        var bidId = $(this).data("bid_id");
        generateSelectedItemBidder('.multiple-items-checkbox-bidder', bidId);
    });
});

$(document).ready(function () {
    //unshare
    $('.multiple-items-checkbox-unshare').change(function () {
        generateSelectedItemIdsUnshare('.multiple-items-checkbox-unshare', '/unshare');
    });
});

$(document).ready(function () {
    $('#companies_select_all').click(function(){
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ($('#companies_select_all').is(':checked')) {
            $('.company').prop('checked', true);
        }
        else {
            $('.company').removeAttr('checked');
        }
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });
});

function generateSelectedItemIdsDelete(className, extra) {
    var ids = '';
    var total = 0;
    var selected = 0;
    $(className).each(function (i, obj) {
        total++;
        if ($(this).is(':checked')) {
            selected++;
            if (ids == '') {
                ids += $(this).data("id");
            } else {
                ids += "-" + $(this).data("id");
            }
        }
    });

    var url = $('#form-url').val();
    var limit = 0;
    if ($('#delete-button').attr("data-limit") != 'undefined') {
        limit = $('#delete-button').attr("data-limit");
    }

    if (ids == '' || limit > (total - selected)) {
        $('#delete-button').attr('disabled', 'disabled');
    } else {
        $('#delete-form').attr('action', url + ids + extra);
        $('#delete-button').removeAttr('disabled');
    }
}

function generateSelectedItemProjectIds(className) {
    var ids = '';
    $(className).each(function (i, obj) {
        if ($(this).is(':checked')) {
            var subId = "";
            if ($(this).attr("data-subid") != "" && $(this).attr("data-subid") != undefined) {
                subId = "|" + $(this).attr("data-subid");
            }

            var bidId = "";
            if ($(this).attr("data-bidid") != "" && $(this).attr("data-bidid") != undefined) {
                bidId = "|" + $(this).attr("data-bidid");
            }

            if (ids == '') {
                ids += $(this).data("id") + subId + bidId;
            } else {
                ids += "-" + $(this).data("id") + subId + bidId;
            }
        }
    });
    var url = $('#form-url').val();

    if (ids == '') {
        $('#delete-button').attr('disabled', 'disabled');
    } else {
        $('#delete-form').attr('action', url + ids);
        $('#delete-button').removeAttr('disabled');
    }
}

function generateSelectedItemCompanyIds(className) {
    var ids = '';
    $(className).each(function (i, obj) {
        if ($(this).is(':checked')) {
            var extId = "";
            if ($(this).attr("data-extid") != "") {
                extId = "|" + $(this).attr("data-extid");
            }

            if (ids == '') {
                ids += $(this).data("id") + extId;
            } else {
                ids += "-" + $(this).data("id") + extId;
            }
        }
    });
    var url = $('#form-url').val();

    if (ids == '') {
        $('#delete-button').attr('disabled', 'disabled');
    } else {
        $('#delete-form').attr('action', url + ids);
        $('#delete-button').removeAttr('disabled');
    }
}

function generateSelectedItemBidder(className, bidId) {
    var ids = '';
    $(className).each(function (i, obj) {
        if ($(this).is(':checked') && bidId == $(this).data("bid_id")) {
            var extId = "";
            if ($(this).attr("data-ab_id") != "") {
                extId = "|" + $(this).attr("data-ab_id");
            }

            if (ids == '') {
                ids += $(this).data("id") + extId;
            } else {
                ids += "-" + $(this).data("id") + extId;
            }
        }
    });
    var url = $('#form-url-' + bidId).val();

    if (ids == '') {
        $('#delete-button-' + bidId).attr('disabled', 'disabled');
    } else {
        $('#delete-form-' + bidId).attr('action', url + ids);
        $('#delete-button-' + bidId).removeAttr('disabled');
    }
}

function generateSelectedItemIdsUnshare(className, extra) {
    var ids = '';
    $(className).each(function (i, obj) {
        if ($(this).is(':checked')) {
            var parId = "|";
            if ($(this).attr("data-comp-parent-id") != "") {
                parId = "|" + $(this).attr("data-comp-parent-id");
            }

            var childId = "|";
            if ($(this).attr("data-comp-child-id") != "") {
                childId = "|" + $(this).attr("data-comp-child-id");
            }

            if (ids == '') {
                ids += $(this).data("id") + parId + childId;
            } else {
                ids += "-" + $(this).data("id") + parId + childId;
            }
        }
    });
    var url = $('#form-url').val();

    if (ids == '') {
        $('#unshare-button').attr('disabled', 'disabled');
    } else {
        $('#unshare-form').attr('action', url + ids + extra);
        $('#unshare-button').removeAttr('disabled');
    }
}

//blog click on img popup
$(document).ready(function () {
    $('.cm-blog img').on("click", function (e) {
        var productImage = $('img');
        var productOverlay = $('.product-image-overlay');
        var productOverlayImage = $('.product-image-overlay img');
        var productImageSource = $(this).attr('src');

        e.preventDefault();
        productOverlayImage.attr('src', productImageSource);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });
});

//blog click on img popup
$(document).ready(function () {
    $('.cm-popover img').on("click", function (e) {
        var productImage = $('img');
        var productOverlay = $('.product-image-overlay');
        var productImageDescription = $('.product-image-description');
        var description = $(this).parent().find("p")["0"].textContent;

        var productOverlayImage = $('.product-image-overlay img');
        var productImageSource = $(this).attr('src');

        e.preventDefault();
        productOverlayImage.attr('src', productImageSource);
        // productImageDescription.html(description);
        productOverlay.fadeIn(100);
        $('body').css('overflow', 'hidden');

        $('.product-image-overlay-close').click(function () {
            productOverlay.fadeOut(100);
            $('body').css('overflow', 'auto');
        });
    });
});

$(document).ready(function () {
    $('#default_recipient_architect').on("click", function () {
        if ($(this).is(":checked")) {
            $('#select_contact_architect').slideDown();
            $('#select_contact_contractor').slideUp();
            $('#select_contact_prime_subcontractor').slideUp();
            $('#select_contact_owner').slideUp();
            $(this).attr("checked");
            $('#transmittal_flag').val("1");
        }
    });

    if($('#default_recipient_architect').is(":checked")) {
        $('#select_contact_architect').slideDown();
        $('#select_contact_contractor').slideUp();
        $('#select_contact_prime_subcontractor').slideUp();
        $('#select_contact_owner').slideUp();
        $('#transmittal_flag').val("1");
    }

    $('#default_recipient_contractor').on("click", function () {
        if ($(this).is(":checked")) {
            $('#select_contact_contractor').slideDown();
            $('#select_contact_architect').slideUp();
            $('#select_contact_prime_subcontractor').slideUp();
            $('#select_contact_owner').slideUp();
            $(this).attr("checked");
            $('#transmittal_flag').val("0");
        }
    });

    if($('#default_recipient_contractor').is(":checked")) {
        $('#select_contact_contractor').slideDown();
        $('#select_contact_architect').slideUp();
        $('#select_contact_prime_subcontractor').slideUp();
        $('#select_contact_owner').slideUp();
        $('#transmittal_flag').val("0");
    }

    $('#default_recipient_prime_subcontractor').on("click", function () {
        if ($(this).is(":checked")) {
            $('#select_contact_prime_subcontractor').slideDown();
            $('#select_contact_contractor').slideUp();
            $('#select_contact_architect').slideUp();
            $('#select_contact_owner').slideUp();
            $(this).attr("checked");
            $('#transmittal_flag').val("2");
        }
    });

    if($('#default_recipient_prime_subcontractor').is(":checked")) {
        $('#select_contact_prime_subcontractor').slideDown();
        $('#select_contact_contractor').slideUp();
        $('#select_contact_architect').slideUp();
        $('#select_contact_owner').slideUp();
        $('#transmittal_flag').val("2");
    }

    $('#default_recipient_owner').on("click", function () {
        if ($(this).is(":checked")) {
            $('#select_contact_owner').slideDown();
            $('#select_contact_contractor').slideUp();
            $('#select_contact_architect').slideUp();
            $('#select_contact_prime_subcontractor').slideUp();
            $(this).attr("checked");
            $('#transmittal_flag').val("3");
        }
    });

    if($('#default_recipient_owner').is(":checked")) {
        $('#select_contact_owner').slideDown();
        $('#select_contact_contractor').slideUp();
        $('#select_contact_architect').slideUp();
        $('#select_contact_prime_subcontractor').slideUp();
        $('#transmittal_flag').val("3");
    }

    $(document).on('change', '#sub_contact', function () {
        if($(this).val()) {
            $('#notif_subcontractor_id').val($(this).val());
            $('#ab_sub_name').val($(this).text());
        } else {
            $('#notif_subcontractor_id').val(0);
            $('#ab_sub_name').val('');
        }
    });

    $(document).on('change', '#recipient_contact', function () {
        if($(this).val()) {
            $('#notif_recipient_id').val($(this).val());
            $('#ab_rec_name').val($(this).text());
        } else {
            $('#notif_recipient_id').val(0);
            $('#ab_rec_name').val('');
        }
    });

});

//input upload btn
$(function () {

    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function () {
        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    // We can watch for our custom `fileselect` event like this
    $(document).ready(function () {

        $(':file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) console.log(log);
            }
        });
    });

});

$(document).ready(function () {
    $("#custom-number").on("click", function () {
        if ($('#item-number').is('[readonly]')) {
            $('#item-number').prop('readonly', false);
            $('#custom-number').text('Auto');
        } else {
            $('#item-number').prop('readonly', true);
            $('#custom-number').text('Custom');
        }
        return false;
    });

    $("#item-name").keyup(function () {
        $("#transmittal-subject").val($("#item-name").val());
    });
});

//search
$(document).ready(function () {
    $("#drop_search_by").change(function () {
        var value = this.value;
        $(".search-field").addClass('hide');
        $(".search-field").removeClass('show');
        $(".search-field-input").val("");
        $("#" + value).addClass('show');
        $("#" + value).removeClass('hide');
    });
});

$(function () {
    $('input[name="companyRadioDistributionIn"]').click(function () {
        if ($(this).attr("data-type") == "user") {
            $('#usersListIn').addClass('show').removeClass('hide');
            $('.contactsListDistributionIn').addClass('hide').removeClass('show');
        } else {
            $('#usersListIn').addClass('hide').removeClass('show');
            $('.contactsListDistributionIn').addClass('hide').removeClass('show');
            var showId = $(this).attr("data-id");
            $('#contactsListDistributionIn-' + showId).addClass('show').removeClass('hide');
        }
    });

    $('input[name="companyRadioDistributionOut"]').click(function () {
        if ($(this).attr("data-type") == "user") {
            $('#usersListOut').addClass('show').removeClass('hide');
            $('.contactsListDistributionOut').addClass('hide').removeClass('show');
        } else {
            $('#usersListOut').addClass('hide').removeClass('show');
            $('.contactsListDistributionOut').addClass('hide').removeClass('show');
            var showId = $(this).attr("data-id");
            $('#contactsListDistributionOut-' + showId).addClass('show').removeClass('hide');
        }
    });
});

$(function () {
    $('input[name="companyRadioDistribution"]').click(function () {
        if ($(this).attr("data-type") == "user") {
            $('#usersList').addClass('show').removeClass('hide');
            $('#abCompanyUsersList').addClass('hide').removeClass('show');
        } else {
            $('#abCompanyUsersList').addClass('show').removeClass('hide');
            $('#usersList').addClass('hide').removeClass('show');
        }
    });
});

$(function () {
    $('input[name="companyRadioBidProposal1"]').click(function () {
        $('.contactsListBidProposal1').addClass('hide').removeClass('show');
        var showId = $(this).attr("data-id");
        $('#contactsListBidProposal1-' + showId).addClass('show').removeClass('hide');
    });
    $('input[name="companyRadioBidProposal2"]').click(function () {
        $('.contactsListBidProposal2').addClass('hide').removeClass('show');
        var showId = $(this).attr("data-id");
        $('#contactsListBidProposal2-' + showId).addClass('show').removeClass('hide');
    });
    $('input[name="companyRadioBidProposal3"]').click(function () {
        $('.contactsListBidProposal3').addClass('hide').removeClass('show');
        var showId = $(this).attr("data-id");
        $('#contactsListBidProposal3-' + showId).addClass('show').removeClass('hide');
    });
    $('input[name="companyRadioSubcontractors"]').click(function () {
        $('.contactsListSubcontractors').addClass('hide').removeClass('show');
        var showId = $(this).attr("data-id");
        $('#contactsListSubcontractors-' + showId).addClass('show').removeClass('hide');
    });
    $('input[name="companyRadioRecipients"]').click(function () {
        $('.contactsListRecipients').addClass('hide').removeClass('show');
        var showId = $(this).attr("data-id");
        $('#contactsListRecipients-' + showId).addClass('show').removeClass('hide');
    });
});



$(document).ready(function () {
    $("#permissions_user_address_office").change(function () {
        var value = this.value;
        $(".permissions-table").addClass('hide');
        $("#container-" + value).removeClass('hide');
    });
});

//toggle sidenav
$(document).ready(function () {
    $(".sidenav-toggle").on("click", function () {
        $("#dev-sidenav").toggleClass("open");
        $("#menu").toggleClass("active");
    });

    // $(".navToggle").on("click", function(){
    //     $(this).toggleClass("open");
    //     $("#menu").toggleClass("active");
    // })
});

$('.custom-options').on('change', function (e) {
    var id = $(this).data("id");
    if (this.value == 0) {
        $(".custom-text-" + id).removeClass("hide");
    } else {
        $(".custom-text-" + id).addClass("hide");
    }
});

$("#customNumber").on("click", function () {
    if ($('#cycle_no').is('[readonly]')) {
        $('#cycle_no').prop('readonly', false);
        $('#useCustom').val(true);
        $('#customNumber').text('Auto');
    } else {
        $('#cycle_no').prop('readonly', true);
        $('#useCustom').val(false);
        $('#cycle_no').val($('#currentCycleNumber').val());
        $('#customNumber').text('Custom');
    }
    return false;
});


//send transmittal email notification
$('.sendBidNotification').click(function () {
    var sendButton = $(this);

    var loader = $(this).find('.email_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');
    sendButton.attr('disabled', 'disabled');

    var proposalNumber = sendButton.data("proposal");
    var fileId = sendButton.data("fileid");
    var filePath = sendButton.data("filepath");

    sendBidEmail(proposalNumber, fileId, filePath);
});

function sendBidEmail(proposalNumber, fileId, filePath) {

    var values = $("input[name='proposal_" + proposalNumber + "_users[]']:checked")
        .map(function () {
            return $(this).val();
        }).get();

    var url = window.location.href;
    url = url.replace("?tab=bidder", "");

    $.ajax({
        type: "POST",
        url: url + "/bidders/" + $('#supplierId').val() + "/notifications/" + proposalNumber,
        data: {
            values: values,
            fileId: fileId,
            filePath: filePath
        },
        success: function (data) {
            if (data.status === 'success') {
                window.location = window.location.href + "?tab=bidder";
            } else {
                //disable send button and show loading image
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                sendButton.removeAttr('disabled');
                //alert(data.message);
                $('#alertPopup').find('.modal-body p').text(data.message);
                $('#alertPopup').modal();
            }
        }
    });
}


//send transmittal email notification
$('.sendProposalNotification').click(function () {
    var sendButton = $(this);

    var loader = $(this).find('.email_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');
    sendButton.attr('disabled', 'disabled');

    var type = sendButton.data("type");
    var fileId = sendButton.data("fileid");
    var filePath = sendButton.data("filepath");

    sendProposalNotificationEmail(type, fileId, filePath);
});

function sendProposalNotificationEmail(type, fileId, filePath) {

    var values = $("input[name='proposal_" + type + "_users[]']:checked")
        .map(function () {
            return $(this).val();
        }).get();

    var url = window.location.href;

    $.ajax({
        type: "POST",
        url: url + "/notifications/" + type,
        data: {
            values: values,
            fileId: fileId,
            filePath: filePath
        },
        success: function (data) {
            if (data.status === 'success') {
                window.location = window.location.href;
            } else {
                //disable send button and show loading image
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                sendButton.removeAttr('disabled');
                //alert(data.message);
                $('#alertPopup').find('.modal-body p').text(data.message);
                $('#alertPopup').modal();
            }
        }
    });
}

$('.parentCheckbox').click(function () {
    var id = $(this).data("id");
    if ($('#parentCheckbox-' + id + ':checkbox:checked').length === 0) {
        $('#childCheckbox-' + id).attr('checked', false); // UnChecks it
    }
});

$('.childCheckbox').click(function () {
    var id = $(this).data("id");
    if ($('#childCheckbox-' + id + ':checkbox:checked').length > 0) {
        if ($('#parentCheckbox-' + id + ':checkbox:checked').length === 0) {
            $('#parentCheckbox-' + id).trigger('click'); // Checks it
        }
    }
});

$('.childRadio').click(function () {
    var id = $(this).data("id");
    if ($('#parentCheckbox-' + id + ':checkbox:checked').length === 0) {
        $('#parentCheckbox-' + id).trigger('click'); // Checks it
    }
});

$('.childRadioPco').click(function () {
    if (!$("input[name='pco_permission_type']:checked").val()) {
        $("#own_pcos").prop("checked", true);
    }
});