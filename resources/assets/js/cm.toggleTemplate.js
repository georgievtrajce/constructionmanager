var cm = cm || {};
/*
*   @param templateId (string), ID of the underscore template
*   @param target (string), CSS selector for the parent where the formatted template will be appended
*   @param opts (object), underscore template settings
*/

cm.toggleTemplate = function (templateId, target, opts) {

    var opts = opts || {};

    var html = $(templateId).html();

    var template = _.template(html)(opts);

    var templateEl = $(template).appendTo(target);
    $(target).trigger('added');

    return {
        element: templateEl,
        add: function () {
            templateEl.slideDown();
            return this;
        },
        remove: function() {
            templateEl.slideUp(function () {
                $.when($(this).remove()).then($(target).trigger('removed'));
            });
            return this;
        }
    }
};

cm.regenIndexes = function (el) {

    var elements = $(el);
    var last;
    elements.each(function (i) {
        var input = $(this).find('input[name]');
        input.each(function () {
            var name = $(this).attr('name');
            var newName = name.replace(/\#(\d+)\#/, '#' + i + '#');
            $(this).attr('name', newName);
        });
        last = i + 1;
    });
    return last;

};

cm.regenRegisterIndexes = function (el) {

    var elements = $(el);
    var last;
    elements.each(function (i) {
        var input = $(this).find('input[name]');
        input.each(function () {
            var name = $(this).attr('name');
            var newName = name.replace(/\[(\d+)\]/, '[' + i + ']');
            $(this).attr('name', newName);
        });
        last = i + 1;
    });
    return last;

};