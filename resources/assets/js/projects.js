$(function() {

    var active = $("ul#myTab li.active a").attr('aria-controls');
    $("#activeTab" ).val(active);
    $('.nav-tabs').on('change', function (e) {
        var active = $("ul#myTab li.active a").attr('aria-controls');
        $("#activeTab" ).val(active);
    });

    $('[data-toggle="popover"]').popover({html:true});

    //change project company office and get proper contacts for the specific office
    $(".sub_addr").change(function (){
        var sub_addr_id = $(this).attr("id");
        var contactsHref = window.location.href;
        var manageContactsLink = $(this).closest('tr').find('.manage_contacts_cont').find('.manage_contacts');
        sub_addr_id = sub_addr_id.replace("sub_addr_", "");
        $.ajax({
            type: "GET",
            dataType: "json",
            url: site_url+"/subcontractors/address",
            data: {
                proj_id:  $(this).attr("data-project-id"),
                address_id:  $(this).val(),
                sub_id: sub_addr_id
            },
            error: function(data) {
                alert(data.error);
            },
            success: function (data) {
                var urlArr = contactsHref.split('/');

                var finalLink;
                if($.isNumeric(urlArr[urlArr.length-1])) {
                    if(data.success == "") {
                        finalLink = contactsHref.replace('/'+urlArr[urlArr.length-1], '');
                    } else {
                        finalLink = contactsHref.replace(urlArr[urlArr.length-1], data.success);
                    }
                } else {
                    if(data.success == "") {
                        finalLink = contactsHref;
                    } else {
                        finalLink = contactsHref+'/'+data.success;
                    }
                }
                window.location = finalLink;
            }
        }).done(function( msg ) {
        });
    });

    //add project number field for subcontractor
    $(".manage_project_number").click(function() {
        if ($('.sub_proj_number').length == 0) {
            $.ajax({
                url: site_url + "/get-subcontractor-project-number",
                dataType: "json",
                data: {
                    sub_proj_number_id: $('.sub_proj_number_id').val()
                },
                success: function (data) {
                    if (data.error == 1) {
                        alert('Something went wrong. Please try again.');
                    } else {
                        var html = $('#sub_proj_number_template').html();

                        var template = _.template(html)();
                        $(template).appendTo('.sub_proj_number_container');
                        $('.sub_proj_number').val(data.number);
                    }
                }
            });
        }
    });

    //cancel project number changing process for subcontractor
    $(document).on("click", "a.sub_project_number_cancel", function(){
        $('.sub_proj_number_container').empty();
    });

    //save project number change for subcontractor
    $(document).on("click", ".sub_project_number_save", function(){
        $.ajax({
            url: site_url + "/change-subcontractor-project-number",
            dataType: "json",
            data: {
                sub_proj_number_id: $('.sub_proj_number_id').val(),
                sub_proj_number: $('.sub_proj_number').val()
            },
            success: function (data) {
                if (data.error == 1) {
                    alert('Something went wrong. Please try again.');
                } else {
                    $('.current_sub_proj_number').empty();
                    $('.current_sub_proj_number').html(data.number);
                    $('.sub_proj_number_container').empty();
                }
            }
        });
    });

    //show and hide proposal suppliers on proposals index page
    $(".show_suppliers").click(function() {
        $(this).parent().parent().next().slideToggle( "slow", function() {
            // Animation complete.
        });
    });

    $('#bid_item_status').change(function() {
        var selected = $(this).val();

        if (selected == 'closed') {
            $('#confirmCloseUnshare').modal('show');

            //<!-- Form confirm (yes/ok) handler, submits form -->
            $('[id="confirm_close_unshare"]').on('click', function(){
                $('#bid_item_status').val('open'); // change back to open
                $('#confirmCloseUnshare').modal('hide');
            });
        }

        $.data(this, 'current', $(this).val());
    });

});


//select company admin checkbox
$(document).on("click", ".btn-batch-download", function () {
    //$('#company_admin').click(function() {
    $('#approveAction').modal().hide();

    $('#approveAction').find('.modal-title').text('Notification');
    $('#approveAction').find('.modal-body p').text('All project files are going to be packed in a single zip file. ' +
        'Notificaton will be sent to your email when the zip file is ready for download. The download link will be active for 24 hours before it is deleted.');
    $('#approveAction').modal();

    var id = $(this).data("id");
    //if confirmation is canceled
    $('[id="cancel_action"]').on('click', function(){
        $('#approveAction').modal().hide();
    });

    //if confirmation is approved
    $('[id="approve_action"]').on('click', function(){
        $('#approveAction').modal().hide();
        window.location = site_url+"/projects/"+id+"/batch/download";
    });
});
