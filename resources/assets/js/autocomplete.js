$(function() {

    $("#proj_ab").autocomplete({
        source: function(request, response) {
            $('#comp_addr').empty();

            var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
            var pathArray = window.location.pathname.split('/');
            var projectId = pathArray[2];

            $.get(site_url + "/autocomplete/addressbook", {
                query: request.term,
                type: 'address-book',
                projectId: projectId
            }, function(data) {
                if ($.isEmptyObject(data)) {
                    response($.map(data, function(item) {
                        return {
                            label: '',
                            value: '',
                            description: '',
                            addresses: ''
                        }
                    }));
                } else {
                    response($.map(data, function(item) {
                        return {
                            label: item.name,
                            value: item.id,
                            description: item.categories,
                            addresses: item.addresses
                        }
                    }));
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#proj_ab").val(ui.item.label);
            $("#ab_id").val(ui.item.value);
            $('#proj_ab_cat').val(ui.item.description);
            $.each(ui.item.addresses, function(index, value) {
                $('#comp_addr').append("<option value='" + value.id + "'>" + value.office_title + "</option>");
            });
            return false;
        }
    });

    $("#proj_sub").autocomplete({
        source: function(request, response) {
            $('#addresses').empty();
            $.ajax({
                url: site_url + "/autocomplete/companyname",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'company'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                description: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                                description: item.addresses
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#proj_sub").val(ui.item.label);
            $("#company_id").val(ui.item.value);
            $.each(ui.item.description, function(index, value) {
                $('#addresses').append("<option value='" + value.id + "'>" + value.office_title + "</option>");
            });

            return false;
        }
    });


    $("#owner_ab").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            if($("#owner_id").val() !== ui.item.value) {
                var projectId = $("#project_id").val();
                if(projectId <= 0) {
                    projectId = 0;
                }
                var addressBookId = ui.item.value;
                var isGeneralContractor = 0;
                getProjectTransmittals('owner', addressBookId, projectId, isGeneralContractor);
            }
            $("#owner_id").val(ui.item.value);
            $("#owner_ab").val(ui.item.label);

            return false;
        }
    });

    $("#architect_ab").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            if($("#architect_id").val() !== ui.item.value) {
                var projectId = $("#project_id").val();
                if(projectId <= 0) {
                    projectId = 0;
                }
                var addressBookId = ui.item.value;
                var isGeneralContractor = 0;
                getProjectTransmittals('architect', addressBookId, projectId, isGeneralContractor);
            }
            $("#architect_id").val(ui.item.value);
            $("#architect_ab").val(ui.item.label);

            return false;
        }
    });

    $("#prime_subcontractor_ab").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            if($("#prime_subcontractor_id").val() !== ui.item.value) {
                var projectId = $("#project_id").val();
                if(projectId <= 0) {
                    projectId = 0;
                }
                var addressBookId = ui.item.value;
                var isGeneralContractor = 0;
                getProjectTransmittals('prime_subcontractor', addressBookId, projectId, isGeneralContractor);
            }
            $("#prime_subcontractor_id").val(ui.item.value);
            $("#prime_subcontractor_ab").val(ui.item.label);

            return false;
        }
    });



    //submittal supplier/subcontractor autocomplete
    $("input.address-book-auto").autocomplete({
        source: function(request, response) {
            var subcontractorContainer = $('.subcontractor_contacts_container');
            subcontractorContainer.empty();

            $('#notif_subcontractor').hide();

            var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
            var pathArray = window.location.pathname.split('/');
            var projectId = pathArray[2];
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'module-companies',
                    projectId: projectId
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                option: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                                option: item
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("input.address-book-id").val(ui.item.value);
            $("input.address-book-auto").val(ui.item.label);
            var subcontractorContainer = $('.subcontractor_contacts_container');
            subcontractorContainer.empty();

            subcontractorContainer.append('<div class="row">' +
                '<div class="col-sm-12">' +
                '<label>Select contact:</label>' +
                '<select name="sub_contact" id="sub_contact">' +
                '<option value=""></option>' +
                '</select>' +
                '</div>' +
                '</div>');

            var contactsSelect = $('#sub_contact');

            if (ui.item.option.contacts.length > 0) {
                $.each(ui.item.option.contacts, function(index, value) {
                    contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            } else {
                contactsSelect.append('<option value="">No available contacts for this company</option>');
            }

            return false;
        }
    });

    //submittal supplier/subcontractor autocomplete
    $("input.address-book-auto-shared").autocomplete({
        source: function(request, response) {
            var subcontractorContainer = $('.subcontractor_contacts_container');
            subcontractorContainer.empty();

            $('#notif_subcontractor').hide();

            var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
            var pathArray = window.location.pathname.split('/');
            var projectId = pathArray[2];
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'module-companies-share',
                    projectId: projectId
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                option: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                                option: item
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("input.address-book-id").val(ui.item.value);
            $("input.address-book-auto-shared").val(ui.item.label);
            var subcontractorContainer = $('.subcontractor_contacts_container');
            subcontractorContainer.empty();

            subcontractorContainer.append('<div class="row">' +
                '<div class="col-sm-12">' +
                '<label>Select contact:</label>' +
                '<select name="sub_contact" id="sub_contact">' +
                '<option value=""></option>' +
                '</select>' +
                '</div>' +
                '</div>');

            var contactsSelect = $('#sub_contact');

            if (ui.item.option.contacts.length > 0) {
                $.each(ui.item.option.contacts, function(index, value) {
                    contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            } else {
                contactsSelect.append('<option value="">No available contacts for this company</option>');
            }

            return false;
        }
    });

    $(document).on('change', '#sub_office', function() {
        //$('#sub_office').on('click', function() {
        $.ajax({
            url: site_url + "/get-office-contacts",
            dataType: "json",
            data: {
                officeId: $('#sub_office').val()
            },
            success: function(data) {
                var contactsSelect = $('#sub_contact');
                contactsSelect.empty();

                if (data.success == 1) {
                    contactsSelect.append('<option value=""></option>');
                    $.each(data.officeContacts, function(index, value) {
                        contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    contactsSelect.append('<option value="">' + data.message + '</option>');
                }
            }
        });
    });

    $(document).on('change', '#prop_addr', function() {
        $.ajax({
            url: site_url + "/get-office-contacts",
            dataType: "json",
            data: {
                officeId: $('#prop_addr').val()
            },
            success: function(data) {
                var contactsSelect = $('#cont_id');
                contactsSelect.empty();

                if (data.success == 1) {
                    contactsSelect.append('<option value="">Select Contact Person</option>');
                    $.each(data.officeContacts, function(index, value) {
                        contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    contactsSelect.append('<option value="">' + data.message + '</option>');
                }
            }
        });
    });

    $(document).on('change', '#addr_id', function() {
        $.ajax({
            url: site_url + "/get-office-contacts",
            dataType: "json",
            data: {
                officeId: $('#addr_id').val()
            },
            success: function(data) {
                var contactsSelect = $('#ab_cont_id');
                contactsSelect.empty();

                if (data.success == 1) {
                    contactsSelect.append('<option value="">Select Contact Person</option>');
                    $.each(data.officeContacts, function(index, value) {
                        contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    contactsSelect.append('<option value="">' + data.message + '</option>');
                }
            }
        });
    });

    //submittal recipient autocomplete
    $("input.address-book-recipient-auto").autocomplete({
        source: function(request, response) {
            var recipientContainer = $('.recipient_contacts_container');
            recipientContainer.empty();

            $('#notif_recipient').hide();

            var newURL = window.location.protocol + "://" + window.location.host + "/" + window.location.pathname;
            var pathArray = window.location.pathname.split('/');
            var projectId = pathArray[2];
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'module-companies',
                    projectId: projectId
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                option: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                                option: item
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("input.address-book-recipient-id").val(ui.item.value);
            $("input.address-book-recipient-auto").val(ui.item.label);
            var recipientContainer = $('.recipient_contacts_container');
            recipientContainer.empty();

            recipientContainer.append('<div class="row">' +
                '<div class="col-sm-12">' +
                '<label>Select contact:</label>' +
                '<select name="recipient_contact" id="recipient_contact">' +
                '<option value=""></option>' +
                '</select>' +
                '</div>' +
                '</div>');

            var contactsSelect = $('#recipient_contact');

            if (ui.item.option.contacts.length > 0) {
                $.each(ui.item.option.contacts, function(index, value) {
                    contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                });
            } else {
                contactsSelect.append('<option value="">No available contacts for this company</option>');
            }

            return false;
        }
    });

    $(document).on('change', '#recipient_office', function() {
        $.ajax({
            url: site_url + "/get-office-contacts",
            dataType: "json",
            data: {
                officeId: $('#recipient_office').val()
            },
            success: function(data) {
                var contactsSelect = $('#recipient_contact');
                contactsSelect.empty();

                if (data.success == 1) {
                    contactsSelect.append('<option value=""></option>');
                    $.each(data.officeContacts, function(index, value) {
                        contactsSelect.append('<option value="' + value.id + '">' + value.name + '</option>');
                    });
                } else {
                    contactsSelect.append('<option value="">' + data.message + '</option>');
                }
            }
        });
    });

    //submittal general contractor autocomplete
    $("input.address-book-gc-auto").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            if($("input.address-book-gc-id").val() !== ui.item.value) {
                var projectId = $("#project_id").val();
                if(projectId <= 0) {
                    projectId = 0;
                }
                var addressBookId = ui.item.value;
                var isGeneralContractor = 0;
                getProjectTransmittals('contractor', addressBookId, projectId, isGeneralContractor);
            }
            $("input.address-book-gc-id").val(ui.item.value);
            $("input.address-book-gc-auto").val(ui.item.label);

            return false;
        }
    });

    //project subcontractors
    var selected_subcontractors = [];
    $("input.project-subcontractor-auto").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/" + $('#project-id').val(),
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'project-subcontractors'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            //get all already selected subcontractors ids
            $('.subcontractor-id').each(function() {
                selected_subcontractors.push($(this).val());
            });

            //check if the subcontractor is already selected
            if (jQuery.inArray(ui.item.value, selected_subcontractors) == -1) {
                $("input.project-subcontractor-id").val('');
                $("input.project-subcontractor-auto").val('');
                $('#selected_subcontractors').append('<div class="list-group list-group-alt cf"><div class="list-group-item-alt cf"><span>' + ui.item.label + '&nbsp;&nbsp;</span>' +
                    '<a href="javascript:;" class="btn-sm btn-danger remove-subcontractor pull-right">X</a>' +
                    '<input type="hidden" name="subcontractor_id" class="subcontractor-id" value="' + ui.item.value + '">' +
                    '</div></div>');
            } else {
                alert('You have already selected this company');
            }

            return false;
        }
    });

    //remove subcontractor from sharing list
    $('#selected_subcontractors').on('click', '.remove-subcontractor', function(e) {
        e.preventDefault();
        var removeItem = $(this).parent().find('.subcontractor-id').val();
        selected_subcontractors = jQuery.grep(selected_subcontractors, function(value) {
            return value != removeItem;
        });
        $(this).parent().remove();
    });


    $("input.address-book-auto").focusout(function() {
        if ($("input.address-book-auto").val() == '') {
            $("input.address-book-id").val('');
            $(".subcontractor_contacts_container").empty();
            $('#notif_subcontractor').hide();
        }
    });

    $("input.address-book-auto-shared").focusout(function() {
        if ($("input.address-book-auto-shared").val() == '') {
            $("input.address-book-id").val('');
            $(".subcontractor_contacts_container").empty();
            $('#notif_subcontractor').hide();
        }
    });

    $("input.address-book-recipient-auto").focusout(function() {
        if ($("input.address-book-recipient-auto").val() == '') {
            $("input.address-book-recipient-id").val('');
            $(".recipient_contacts_container").empty();
            $('#notif_recipient').hide();
        }
    });

    $("input.address-book-gc-auto").focusout(function() {
        if ($("input.address-book-gc-auto").val() == '') {
            $("input.address-book-gc-id").val('');
        }
    });

    $("input.mf-number-title-auto").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/masterformat",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'master-format',
                    projectId: ($('#project_id').val() === undefined)?$('#task_project').val():$('#project_id').val(),
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                description: '',
                                icon: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.number + ' - ' + item.title,
                                value: item.id,
                                description: item.number,
                                icon: item.title
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("input.master-format-id").val(ui.item.value);
            $("input.mf-number-title-auto").val(ui.item.label);
            $("input.mf-number-auto").val(ui.item.description);
            $("input.mf-title-auto").val(ui.item.icon);

            return false;
        }
    });

    //project subcontractors
    var selected_ab_entries = [];
    $("input.ab-mf-number-title-auto").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/masterformat",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'master-format'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: '',
                                description: '',
                                icon: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.number + ' - ' + item.title,
                                value: item.id,
                                description: item.number,
                                icon: item.title
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#master_format_search_add").val('');
            //get all already selected entries ids
            $('.mf-id').each(function() {
                selected_ab_entries.push($(this).val());
            });

            //check if the entry is already selected
            if (jQuery.inArray(ui.item.value, selected_ab_entries) == -1) {
                var mfCounter = $('.selected-mf-number-title').length;
                var opts = {
                    mf_number: ui.item.description,
                    mf_title: ui.item.icon,
                    mf_id: ui.item.value,
                    counter: mfCounter
                };
                var html = $('#master-format-number-title-template').html();
                var template = _.template(html)(opts);
                var target = '.selected-mfs';
                $(template).appendTo(target);
                var mf_ids = $('#mf_ids').val();
            } else {
                alert('You have already selected this master format entry');
            }

            return false;
        }
    });

    //remove master format address book entry from list
    $('.selected-mfs').on('click', '.remove-mf-number-title', function(e) {
        e.preventDefault();
        var removeItem = $(this).parent().find('.mf-id').val();
        selected_ab_entries = jQuery.grep(selected_ab_entries, function(value) {
            return value != removeItem;
        });
        $(this).parent().parent().remove();
        cm.regenIndexes('.selected-mf-number-title');
    });


    $("#prop_ab").autocomplete({
        source: function(request, response) {
            $('#prop_addr').empty();
            $('#prop_addr').append("<option value=''>Select Location</option>");
            $('#prop_ab_cont').empty();
            $('#prop_ab_cont').append("<option value=''>Select Contact Person</option>");
            $('#addr_id').empty();
            $('#addr_id').append("<option value=''>Select Location</option>");
            $('#ab_cont_id').empty();
            $('#ab_cont_id').append("<option value=''>Select Contact Person</option>");
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if (!data.length) {
                        var result = [{
                            label: 'No matches found',
                            value: response.term
                        }];
                        response(result);
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id,
                                addresses: item.addresses,
                                contacts: item.contacts
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#prop_ab").val(ui.item.label);
            $("#prop_ab_id").val(ui.item.value);
            $.each(ui.item.addresses, function(index, value) {
                $('#prop_addr').append("<option value='" + value.id + "'>" + value.office_title + "</option>");
                $('#addr_id').append("<option value='" + value.id + "'>" + value.office_title + "</option>");
            });
            //$.each( ui.item.contacts, function( index, value ) {
            //    $('#prop_ab_cont').append("<option value='"+value.id+"'>"+value.name+"</option>");
            //    $('#ab_cont_id').append("<option value='"+value.id+"'>"+value.name+"</option>");
            //});

            return false;
        }
    });
    $("#prop_ab").bind("keyup", function() {
        if ($(this).val() == "")
            $('#prop_addr').empty();
        $('#prop_addr').append("<option value=''>Select Location</option>");
        $('#prop_ab_cont').empty('');
        $('#prop_ab_cont').append("<option value=''>Select Contact Person</option>");
        $('#addr_id').empty('');
        $('#addr_id').append("<option value=''>Select Location</option>");
        $('#ab_cont_id').empty('');
        $('#ab_cont_id').append("<option value=''>Select Contact Person</option>");
        $('#prop_ab_id').val('');
    });


    $("#contr_ab").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#contr_ab").val(ui.item.label);
            $("#contr_ab_id").val(ui.item.value);
            return false;
        }
    });


    $("#mas_sub").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: site_url + "/autocomplete/addressbook",
                dataType: "json",
                data: {
                    query: request.term,
                    type: 'address-book'
                },
                success: function(data) {
                    if ($.isEmptyObject(data)) {
                        response($.map(data, function(item) {
                            return {
                                label: '',
                                value: ''
                            }
                        }));
                    } else {
                        response($.map(data, function(item) {
                            return {
                                label: item.name,
                                value: item.id
                            }
                        }));
                    }
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $("#mas_sub").val(ui.item.label);
            $("#mas_sub_id").val(ui.item.value);
            return false;
        }
    });

    $("#mas_sub").focusout(function() {
        if ($("#mas_sub").val() == '') {
            $("#mas_sub_id").val('');
        }
    });

    function getProjectTransmittals(name, addressBookId, projectId, isGeneralContractor) {

        //wait start
        $(".please-wait").css({
            "width": $(document).width(),
            "height": $(document).height()
        }).appendTo('body').fadeIn(100);

        $.ajax({
            url: site_url + "/address-book/transmittals",
            dataType: "json",
            data: {
                projectId: projectId,
                addressBookId: addressBookId,
                isGeneralContractor: isGeneralContractor
            },
            success: function (data) {

                //remove all options
                var transmittal = $("#" + name +"_transmittal_id");
                transmittal.find("option").remove().end();

                var count = Object.keys(data.response).length;

                if(count > 0) {
                    transmittal.append('<option value="0"></option>');
                    $.each(data.response, function (index, row) {
                        //add options
                        transmittal.append('<option value="' + row.id + '">' + row.name + '</option>');
                    });
                }

                //wait end
                $(".please-wait").appendTo('body').fadeOut(100);
            }
        });
    }

    $('#make_me_gc').change(function() {

        if ($(this).is(":checked")) {
            // if($('#default_recipient_architect').length > 0)         // use this if you are using id to check
            // {
            //     $('#default_recipient_architect').prop("checked",true);
            //     $('#default_recipient_contractor').prop("checked",false);
            // }
            
            $('.general_contractor_container').slideUp();
            $('.address-book-gc-auto').val('');
            $('.address-book-gc-id').val('');
            $(this).attr("checked");

            var projectId = $('#project_id').val();
            var addressBookId = 0;
            var isGeneralContractor = 1;
            getProjectTransmittals('contractor', addressBookId, projectId, isGeneralContractor);
        } else {
            $('.general_contractor_container').slideDown();
            $(this).removeAttr("checked");
            var contractorTransmittalId = $('#contractor_transmittal_id');
            contractorTransmittalId.find("option").remove().end();
            contractorTransmittalId.append('<option value="0"></option>');
            contractorTransmittalId.append('<option value="">No available contacts for this company</option>');
            contractorTransmittalId.val(0);
        }
    });
});