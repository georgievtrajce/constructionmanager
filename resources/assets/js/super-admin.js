//companies and users - edit expire date event
$(".edit_expire_date").click(function() {
    var expireDateDataCont = $(this).parent().prev().children('.expire_date_data');
    var expireDateInputCont = $(this).parent().prev().children('.expire_date_input');
    var expireDateSaveButton = $(this).next('.save_expire_date');
    var expireDateEditButton = $(this);

    //remove elements
    expireDateDataCont.css('display','none');
    expireDateInputCont.css('display','inline-block');

    expireDateEditButton.css('display','none');
    expireDateSaveButton.css('display','inline-block');
});

//companies and users - cancel expire date change
$('.cancel_expire_change').click(function() {
    var cancelButton = $(this);
    cancelButton.parent().css('display','none');
    cancelButton.parent().parent().next().children('.save_expire_date').css('display','none');
    cancelButton.parent().parent().next().children('.edit_expire_date').css('display','inline-block');
    cancelButton.parent().prev().css('display','inline-block');
});

//companies and users - save expire date
$('.save_expire_date').click(function() {
    var saveButton = $(this);
    var expireDateInput = $(this).parent().prev().children('.expire_date_input').children('.expiry_date');
    saveButton.attr('disabled', 'disabled');
    $.ajax({
        url: site_url + "/list-companies/save-company-expire-date",
        dataType: "json",
        data: {
            companyId: saveButton.prev().attr("data-company-id"),
            expireDate: expireDateInput.val()
        },
        error: function(data) {
            alert(data.error);
        },
        success: function (data) {
            location.reload(true);
        }
    });
});

$('.subscriptionTypeSelect').change(function(){
    if ($(this).attr("data-oldvalue") != $(this).val()) {
        $.ajax({
            url: site_url + "/list-companies/save-company-subscription-type",
            dataType: "json",
            data: {
                companyId: $(this).attr("data-company-id"),
                subscriptionType: $(this).val()
            },
            error: function(data) {
                alert(data.error);
            },
            success: function (data) {

            }
        });
    }
});

