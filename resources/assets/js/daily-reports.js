/****************************************************
 * **************************************************
 * ******** Send Email Daily Report *****************
 * **************************************************
 */

$('.sendDailyReportEmail').click(function() {
    var sendButton = $(this);

    var loader = $(this).find('.email_wait');
    var glyphicon = $(this).find('.glyphicon');

    //disable send button and show loading image
    loader.css('display', 'block');
    glyphicon.css('display', 'none');
    sendButton.attr('disabled', 'disabled');

    //get attributes with values
    var projectId = $("#projectId").val();
    var dailyReportId = $("#dailyReportId").val();
    var filePath = sendButton.attr("data-path");
    var fileId = sendButton.attr("data-file-id");
    var emailed = sendButton.attr("data-emailed");

    var users = [];
    $("input[name='employees_users_daily_report[]']").each( function () {
        if ($(this).is(":checked")) {
            users.push($(this).val());
        }
    });

    var contacts = [];
    $("input[name='employees_contacts_daily_report[]']").each( function () {
        if ($(this).is(":checked")) {
            contacts.push($(this).val());
        }
    });

    function sendReportEmail(projectId, dailyReportId, users, contacts, fileId, filePath) {
        $.ajax({
            type: "POST",
            url: site_url + "/send-daily-report-email-notification",
            dataType: "json",
            data: {
                projectId: projectId,
                dailyReportId: dailyReportId,
                fileId: fileId,
                filePath: filePath,
                users: users.join("|"),
                contacts: contacts.join("|")
            },
            success: function(data) {
                if (data.status === 'success') {

                    // location.reload();
                    window.location = [location.protocol, '//', location.host, location.pathname].join('');
                } else {
                    //disable send button and show loading image
                    loader.css('display', 'none');
                    glyphicon.css('display', 'inline-block');
                    sendButton.removeAttr('disabled');
                    //alert(data.message);
                    $('#alertPopup').find('.modal-body p').text(data.message);
                    $('#alertPopup').modal();
                }

            }
        });
    }

    if (emailed == '1') {
        var result = 0;
        $('#approveAction').find('.modal-body p').text('The Daily Report has been already sent. Do you want to email the Daily Report again?');
        $('#approveAction').modal();

        //<!-- Form confirm (yes/ok) handler, submits form -->
        $('[id="approve_action"]').on('click', function() {
            result = 1;
            $('#approveAction').modal('toggle');
            sendReportEmail(projectId, dailyReportId, users, contacts, fileId, filePath);

            if (result === 0) {
                //disable send button and show loading image
                loader.css('display', 'none');
                glyphicon.css('display', 'inline-block');
                sendButton.removeAttr('disabled');
            }
        });

        $('[id="cancel_action"]').on('click', function() {
            //disable send button and show loading image
            sendButton.next().children().css('display', 'none');
            sendButton.removeAttr('disabled');
        });
    } else {
        sendReportEmail(projectId, dailyReportId, users, contacts, fileId, filePath);
    }


});


/****************************************************
 * **************************************************
 * ******** Generate Daily Report *******************
 * **************************************************
 */

$('#generateDailyReport').click(function() {
    $('#generate_daily_report_wait').css('display', 'block');
    $('#generateDailyReport').attr('disabled', 'disabled');

    var loader = $(this).find('#generate_daily_report_wait');

    //disable send button and show loading image
    loader.css('display', 'block');

    var dailyReportId = $('#dailyReportId').val();
    var projectId = $('#projectId').val();
    var addDocuments = $('#addDocuments').is(":checked");

    $.ajax({
        url: site_url + "/generate-daily-report",
        type: 'POST',
        dataType: 'json',
        data: {
            dailyReportId: dailyReportId,
            projectId: projectId,
            addDocuments: addDocuments
        },
        success: function(data) {
            if (data.generate == 1) {
                //alert('Transmittal was successfully generated. Please check your module transmittals list.');

                var fileId = data.fileId;
                var s3FilePath = data.url;
                window.open("about:blank", "newPage");
                $.ajax({
                    url: site_url + "/check-download-allowance",
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        fileId: fileId
                    },
                    success: function(data) {
                        if (data.allowed == 1) {
                            var filesize = data.filesize;
                            var s3 = new AWS.S3();
                            var params = {
                                Bucket: 'cmsconstructionmanager',
                                Key: s3FilePath
                            };
                            s3.getSignedUrl('getObject', params, function(err, url) {
                                if (window.open(url, "newPage")) {
                                    // $.post(site_url + "/increase-download-transfer", {
                                    //     filesize: filesize,
                                    //     fileId: fileId
                                    // }, function(data) {
                                    //     if (data == 'true') {
                                    //         console.log('Upload Transfer Limit Increased.');
                                    //     } else {
                                    //         console.log('Upload Transfer Limit was not increased.');
                                    //     }
                                    // });
                                    var url = [location.protocol, '//', location.host, location.pathname].join('');
                                    window.location = url;
                                }
                            });
                        } else {
                            //alert('File could not be downloaded because you reached your subscription level data transfer limitation.');
                            $('#alertPopup').find('.modal-body p').text('File could not be downloaded because you reached your subscription level data transfer limitation.');
                            $('#alertPopup').modal();
                        }
                    }
                });
            } else {
                //alert(data.message);
                $('#alertPopup').find('.modal-body p').text(data.message);
                $('#alertPopup').modal();
            }
            // $('#sub_transmittal_wait').css('display', 'none');
            loader.css('display', 'none');
            $('#generateDailyReport').removeAttr('disabled');
        }
    });

    // //check if transmittal file is already generated
    // $.ajax({
    //     url: site_url + "/check-existing-transmittal",
    //     type: 'POST',
    //     dataType: 'json',
    //     data: {
    //         type: type,
    //         versionId: versionId,
    //         projectId: projectId,
    //         transmittalDate: subTransmittalDate,
    //         databaseType: databaseType
    //     },
    //     success: function(data) {
    //         if (data.response) {
    //             var result = 0;
    //             $('#approveAction').find('.modal-body p').text('Sent to Subcontractor Transmittal is already generated for this entry. Do you want to re-create it again?');
    //             $('#approveAction').modal();
    //
    //             //<!-- Form confirm (yes/ok) handler, submits form -->
    //             $('[id="approve_action"]').on('click', function() {
    //                 result = 1;
    //                 $('#approveAction').modal('toggle');
    //                 updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
    //             });
    //
    //             if (result === 0) {
    //                 // $('#sub_transmittal_wait').css('display', 'none');
    //                 loader.css('display', 'none');
    //                 glyphicon.css('display', 'inline-block');
    //                 $('#generateSubTransmittal').removeAttr('disabled');
    //             }
    //         } else {
    //             updateNotesAndGenerateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
    //         }
    //     }
    // });
    //
    // var updateNotesAndGenerateTransmittal = function(type, databaseType, versionId, projectId, subTransmittalDate)
    // {
    //     tinymce.triggerSave();
    //     $.ajax({
    //         type: "POST",
    //         url: $("#notesForm").attr('action'),
    //         data: $("#notesForm").serialize(), // serializes the form's elements.
    //         success: function(data)
    //         {
    //             generateTransmittal(type, databaseType, versionId, projectId, subTransmittalDate);
    //         }
    //     });
    // }
});


/****************************************************
 * **************************************************
 * ******** Edit Daily Report ***********************
 * **************************************************
 */

$('.daily_table_edit').click(function() {

    //wait start
    $(".please-wait").css({
        "width": $(document).width(),
        "height": $(document).height()
    }).appendTo('body').fadeIn(100);

    var editButton = $(this);
    var projectId = $('#projectId').val();
    var dailyReportId = editButton.attr("data-daily-report-id");
    var rowId = editButton.attr("data-row-id");
    var step = editButton.attr("data-step");
    var tab = editButton.attr("data-tab");
    var type = 'update';

    var url = site_url + "/edit-daily-report-row";

    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: {
            type: type,
            projectId: projectId,
            dailyReportId: dailyReportId,
            rowId: rowId,
            step: step,
            tab: tab
        },
        success: function(data) {
            if(data.error) {
                //error
                $('#approveAction').find('.modal-body p').text(data.message);
                $('#approveAction').modal();
            } else {
                //success
                switch(step){
                    case '3': //subcontractor-contractor
                        if(tab == 1) {

                            //same officeId
                            if($( "#contractor1_worker_office" ).val() == data.response.worker_office) {
                                $( "#contractor1_worker_name" ).val(data.response.worker_name);
                            }
                            //different officeId
                            else {
                                changeWorkerName(data.response.worker_office, data.response.worker_name);
                            }

                            $( "#contractor1_title" ).val(data.response.title);
                            $( "#contractor1_regular_hours" ).val(data.response.regular_hours);
                            $( "#contractor1_overtime_hours" ).val(data.response.overtime_hours);
                            if(data.response.description) {
                                tinymce.get('contractor1_description').setContent(data.response.description);
                            }

                            //buttons
                            $( "#contractor1_save" ).hide();
                            $( "#contractor1_update" ).show();

                            //set update hidden button to true
                            $( "#contractor1_update_hidden_btn" ).val(rowId);
                        } else {
                            $( "#contractor2_trade" ).val(data.response.trade);
                            $( "#contractor2_workers" ).val(data.response.workers);
                            $( "#contractor2_regular_hours" ).val(data.response.regular_hours);
                            $( "#contractor2_overtime_hours" ).val(data.response.overtime_hours);
                            if(data.response.description) {
                                tinymce.get('contractor2_description').setContent(data.response.description);
                            }

                            //buttons
                            $( "#contractor2_save" ).hide();
                            $( "#contractor2_update" ).show();

                            //set update hidden button to true
                            $( "#contractor2_update_hidden_btn" ).val(rowId);
                        }
                        break;
                    case '4': //subcontractor-contractor
                        $( "#subcontractor_selected_comp_id" ).val(data.response.selected_comp_id);
                        $( "#subcontractor_trade" ).val(data.response.trade);
                        $( "#subcontractor_workers" ).val(data.response.workers);
                        $( "#subcontractor_regular_hours" ).val(data.response.regular_hours);
                        $( "#subcontractor_overtime_hours" ).val(data.response.overtime_hours);
                        $( "#subcontractor_description" ).val(data.response.description);
                        if(data.response.description) {
                            tinymce.get('subcontractor_description').setContent(data.response.description);
                        }

                        //buttons
                        $( "#subcontractor_save" ).hide();
                        $( "#subcontractor_update" ).show();

                        //set update hidden button to true
                        $( "#subcontractor_update_hidden_btn" ).val(rowId);
                        break;
                    case '5': //equipment
                        $( "#equipment_equipment" ).val(data.response.equipment);
                        $( "#equipment_quantity" ).val(data.response.quality);
                        // $( "#equipment_owner" ).val(data.response.owner);
                        $( "#equipment_selected_comp_id" ).val(data.response.selected_comp_id);
                        $( "#equipment_hours" ).val(data.response.hours);
                        if(data.response.description) {
                            tinymce.get('equipment_description').setContent(data.response.description);
                        }

                        //buttons
                        $( "#equipment_save" ).hide();
                        $( "#equipment_update" ).show();

                        //set update hidden button to true
                        $( "#equipment_update_hidden_btn" ).val(rowId);
                        break;
                    case '6': //materials
                        $( "#materials_delivered" ).val(data.response.material_delivered);
                        $( "#materials_quantity" ).val(data.response.quantity);
                        $( "#materials_selected_comp_id" ).val(data.response.selected_comp_id);
                        if(data.response.notes) {
                            tinymce.get('materials_notes').setContent(data.response.notes);
                        }

                        //buttons
                        $( "#materials_save" ).hide();
                        $( "#materials_update" ).show();

                        //set update hidden button to true
                        $( "#materials_update_hidden_btn" ).val(rowId);
                        break;
                    case '7': //inspections
                        $( "#inspections_performed" ).val(data.response.testing_performed);
                        $( "#inspections_mf_number" ).val(data.response.mf_number);
                        $( "#inspections_mf_title" ).val(data.response.mf_title);
                        // $( "#inspections_inspector" ).val(data.response.inspector);

                        //same companyId
                        if($( "#inspections_selected_comp_id" ).val() == data.response.selected_comp_id) {
                            $( "#inspections_selected_employee_id" ).val(data.response.selected_employee_id);
                        }
                        //different companyId
                        else {
                            $( "#inspections_selected_comp_id" ).val(data.response.selected_comp_id);
                            changeEmployeeName('inspections', data.response.selected_comp_id, data.response.selected_employee_id);
                        }

                        if(data.response.other_company) {
                            $( "#inspections_other_company" ).val(data.response.other_company);
                        }

                        if(data.response.other_employee) {
                            $( "#inspections_selected_employee_id" ).val(0);
                            $('.other_employee').show();
                            $( "#inspections_other_employee" ).val(data.response.other_employee);
                        }

                        if(data.response.notes) {
                            tinymce.get('inspections_notes').setContent(data.response.notes);
                        }

                        //buttons
                        $( "#inspections_save" ).hide();
                        $( "#inspections_update" ).show();

                        //set update hidden button to true
                        $( "#inspections_update_hidden_btn" ).val(rowId);
                        break;
                    case '8': //observations
                        $( "#observations_safety" ).val(data.response.safety_deficiencies_observed);
                        $( "#observations_selected_comp_id" ).val(data.response.selected_comp_id);
                        if(data.response.notes) {
                            tinymce.get('observations_notes').setContent(data.response.notes);
                        }

                        //buttons
                        $( "#observations_save" ).hide();
                        $( "#observations_update" ).show();

                        //set update hidden button to true
                        $( "#observations_update_hidden_btn" ).val(rowId);
                        break;
                    case '9': //instructions
                        // $( "#instructions_name" ).val(data.response.name);

                        //same companyId
                        if($("#instructions_selected_comp_id" ).val() == data.response.selected_comp_id) {
                            $("#instructions_selected_employee_id" ).val(data.response.selected_employee_id);
                        }
                        //different companyId
                        else {
                            $("#instructions_selected_comp_id" ).val(data.response.selected_comp_id);
                            changeEmployeeName('instructions', data.response.selected_comp_id, data.response.selected_employee_id);
                        }

                        if(data.response.description) {
                            tinymce.get('instructions_description').setContent(data.response.description);
                        }

                        //buttons
                        $( "#instructions_save" ).hide();
                        $( "#instructions_update" ).show();

                        //set update hidden button to true
                        $( "#instructions_update_hidden_btn" ).val(rowId);
                        break;
                    case '12': //instructions
                        $( "#attachments_name" ).val(data.response.name);
                        $( "#attachments_description" ).val(data.response.description);
                        $( "#attachments_pdf" ).val(data.response.file_name);

                        //buttons
                        $( "#iattachments_save" ).hide();
                        $( "#iattachments_update" ).show();

                        //set update hidden button to true
                        $( "#iattachments_update_hidden_btn" ).val(rowId);
                        break;
                }

                //wait end
                $(".please-wait").appendTo('body').fadeOut(100);
            }
        }
    });

});

function cancelRow(clickedButton) {
    var buttonId = clickedButton.id;
    var findTab = buttonId.split('_');
    var tab = findTab[0];

    //switch buttons
    $("#"+tab+"_save").show();
    $("#"+tab+"_update").hide();

    //set update hidden button to false
    $( "#"+tab+"_update_hidden_btn" ).val(0);

    //clear values
    switch(tab){
        case 'contractor1':
            $( "#contractor1_title" ).val('');
            $( "#contractor1_regular_hours" ).val('');
            $( "#contractor1_overtime_hours" ).val('');
            tinymce.get('contractor1_description').setContent('');
            break;
        case 'contractor2':
            $( "#contractor2_trade" ).val('');
            $( "#contractor2_workers" ).val('');
            $( "#contractor2_regular_hours" ).val('');
            $( "#contractor2_overtime_hours" ).val('');
            tinymce.get('contractor2_description').setContent('');
            break;
        case 'subcontractor':
            $( "#subcontractor_trade" ).val('');
            $( "#subcontractor_workers" ).val('');
            $( "#subcontractor_regular_hours" ).val('');
            $( "#subcontractor_overtime_hours" ).val('');
            $( "#subcontractor_description" ).val('');
            tinymce.get('subcontractor_description').setContent('');
            break;
        case 'equipment':
            $( "#equipment_equipment" ).val('');
            $( "#equipment_quantity" ).val('');
            $( "#equipment_owner" ).val('');
            $( "#equipment_hours" ).val('');
            tinymce.get('equipment_description').setContent('');
            break;
        case 'materials':
            $( "#materials_delivered" ).val('');
            $( "#materials_quantity" ).val('');
            tinymce.get('materials_notes').setContent('');
            break;
        case 'inspections':
            $( "#inspections_performed" ).val('');
            $( "#inspections_spec_or_dwg" ).val('');
            $( "#inspections_inspector" ).val('');
            $( "#inspections_mf_number" ).val('');
            $( "#inspections_mf_title" ).val('');

            $( "#inspections_selected_comp_id" ).val($( "#myCompanyId" ).val());
            changeEmployeeName('inspections', $( "#myCompanyId" ).val(), $( "#myFirstCompanyUserId" ).val());

            $( "#inspections_other_company" ).val('');
            $( "#inspections_other_employee" ).val('');
            tinymce.get('inspections_notes').setContent('');
            break;
        case 'observations':
            $( "#observations_safety" ).val('');
            tinymce.get('observations_notes').setContent('');
            break;
        case 'instructions':
            $( "#instructions_name" ).val('');
            tinymce.get('instructions_description').setContent('');
            break;
        case 'attachments':
            $( "#attachments_name" ).val('');
            $( "#attachments_description" ).val('');
            $( "#attachments_pdf" ).val('');
            break;
    }
}

/****************************************************
 * **************************************************
 * ******** Delete Daily Report *********************
 * **************************************************
 */

$('.daily_table_delete').click(function() {

    var projectId = $('#projectId').val();
    var deleteButton = $(this);
    var dailyReportId = deleteButton.attr("data-daily-report-id");
    var rowId = deleteButton.attr("data-row-id");
    var step = deleteButton.attr("data-step");
    var tab = deleteButton.attr("data-tab");
    var type = 'single';

    $('#approveAction').find('.modal-body p').text('Are you sure you want to delete this record?');
    $('#approveAction').modal();

    $('[id="approve_action"]').on('click', function() {
        $('#approveAction').modal('toggle');
        deleteRows(type, projectId, dailyReportId, rowId, step, tab);
    });

});

function deleteRows(type, projectId, dailyReportId, rowId, step, tab) {

    //wait start
    $(".please-wait").css({
        "width": $(document).width(),
        "height": $(document).height()
    }).appendTo('body').fadeIn(100);

    $.ajax({
        url: site_url + "/delete-daily-report-row",
        type: 'POST',
        dataType: 'json',
        data: {
            type: type,
            projectId: projectId,
            dailyReportId: dailyReportId,
            rowId: rowId,
            step: step,
            tab: tab
        },
        success: function(data) {
            //Error
            if (data.error) {
                var alert = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
                alert += '<p><strong>Whoops!</strong> There were some problems, the row is not deleted.</p></div>'
                $("#alert-danger-ajax").html(alert);
            }
            //Success
            else {
                var success = ' <div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>';
                    success += 'The row is succesfully deleted</div>';
                $("#alert-success-ajax").html(success);

                setTimeout(function(){
                    var url = [location.protocol, '//', location.host, location.pathname].join('') + '?step=' + data.step;
                    if(data.tab !== undefined) {
                        url = url  + '&tab=' + data.tab;
                    }
                    window.location = url;
                }, 1000);
            }

            //wait end
            $(".please-wait").appendTo('body').fadeOut(100);
        }

    });
}

//Multiple deletes
function enableDeleteButton(checkbox) {
    var checkboxName = checkbox.name;
    var findTab = checkboxName.split('_');
    var tab = findTab[0];
    generateDeleteIds(checkboxName, tab);
}

function generateDeleteIds(className, tabName) {
    var ids = '';
    var total = 0;
    var selected = 0;
    $('.'+className).each(function(i, obj) {
        total++;
        if ($(this).is(':checked')) {
            selected++;
            if (ids == '') {
                ids += $(this).data("id");
            } else {
                ids += "-" + $(this).data("id");
            }
        }
    });

    var limit = 0;
    if (ids == '' || limit > (total - selected)) {
        $('#'+tabName+'_delete_all').attr('disabled', 'disabled');
        $('#'+tabName+'_ids').val(0);
    } else {
        $('#'+tabName+'_delete_all').removeAttr('disabled');
        $('#'+tabName+'_ids').val(ids);
    }
}

function deleteMultipleRows(deleteButton) {
    var deleteButtonId = deleteButton.id;
    var findTab = deleteButtonId.split('_');
    var tabName = findTab[0];

    var thisButton = $('#'+deleteButtonId);

    var ids = $("#"+tabName+"_ids").val();
    var projectId = $('#projectId').val();
    var dailyReportId = thisButton.data("daily-report-id");
    var step = thisButton.data("step");
    var tab = thisButton.data("tab");
    var type = 'multi';

    $('#approveAction').find('.modal-body p').text('Are you sure you want to delete this record?');
    $('#approveAction').modal();

    $('[id="approve_action"]').on('click', function() {
        $('#approveAction').modal('toggle');
        deleteRows(type, projectId, dailyReportId, ids, step, tab);
    });
}

/****************************************************
 * **************************************************
 * ******** Worker Office on change *****************
 * **************************************************
 */

function changeWorkerName(paramOfficeId, paramNameId) {
    var workerName = $('#contractor1_worker_name');
    var workerTitle =  $("#contractor1_title");

    if (allWorkerUsers) {
        //remove all user options
        workerName.find("option").remove().end();
        var officeId;

        // allWorkerUsersCleanArray = JSON.parse(allWorkerUsers.replace(/&quot;/g, '"'));
        var allWorkerUsersCleanArray = JSON.parse(allWorkerUsers);
        var count = Object.keys(allWorkerUsersCleanArray).length;

        if (count > 0) {

            //officeId from edit
            if(paramOfficeId) {
                officeId = paramOfficeId;
            } else {
                officeId = $('#contractor1_worker_office').val();
            }

            var firstOfficeUserId = null;

            $.each(allWorkerUsersCleanArray, function (indexOfficeId, users) {
                if (officeId == indexOfficeId) {
                    var countUsers = Object.keys(users).length;
                    if (countUsers > 0) {
                        $.each(users, function (id, name) {
                            workerName.append('<option data-office-id="' + indexOfficeId + '" value="' + id + '">' + name + '</option>');

                            if(!firstOfficeUserId) {
                                firstOfficeUserId = id;
                            }
                        });
                    }
                    // else {
                    //     workerName.append('<option data-office-id="' + indexOfficeId + '" value="0">No users</option>');
                    // }
                }

                //office from edit
                if(paramNameId) {
                    workerName.val(paramNameId);
                }
            });

            if(allWorkerTitles) {
                var allWorkerTitlesCleanArray = JSON.parse(allWorkerTitles);
                var countTitles = Object.keys(allWorkerTitlesCleanArray).length;
                $("#contractor1_title").val('');

                if(countTitles > 0) {
                    $.each(allWorkerTitlesCleanArray, function (officeId, titles) {
                        $.each(titles, function (userId, title) {
                            if (userId == firstOfficeUserId) {
                                $("#contractor1_title").val(title);
                            }
                        });
                    });
                }
            }
        }
    }
}

function changeWorkerTitle(paramWorkerNameId) {

    var paramUserId;
    if(paramWorkerNameId) {
        paramUserId = paramWorkerNameId;
    } else {
        paramUserId = $('#contractor1_worker_name').val();
    }


    if(allWorkerTitles) {
        var allWorkerTitlesCleanArray = JSON.parse(allWorkerTitles);
        var countTitles = Object.keys(allWorkerTitlesCleanArray).length;
        $("#contractor1_title").val('');

        if(countTitles > 0) {
            $.each(allWorkerTitlesCleanArray, function (officeId, titles) {
                $.each(titles, function (userId, title) {
                    if (userId == paramUserId) {
                        $("#contractor1_title").val(title);
                    }
                });
            });
        }
    }
}

function changeEmployeeName(tab, paramCompanyID, paramEmployeeID)
{
    var paramUserId;
    if(paramCompanyID) {
        paramCompanyId = paramCompanyID;
    } else {
        paramCompanyId = $('#'+tab+'_selected_comp_id').val();
    }

    var employee = $('#'+tab+'_selected_employee_id');
    employee.find("option").remove().end();

    var countUsers = 0;

    if(myCompanyUsers) {
        var myCompanyUserArray = JSON.parse(myCompanyUsers);
        var countMyCompanyUsers = Object.keys(myCompanyUserArray).length;
        if(countMyCompanyUsers > 0)
        {
            $.each(myCompanyUserArray, function (userId, value) {
                $.each(value, function (companyId, userName) {
                    if(companyId == paramCompanyId) {
                        employee.append('<option value="' + userId + '">' + userName + '</option>');
                        countUsers++;
                    }
                });
            });
        }
    }

    if(otherUsers) {
        var otherUserArray = JSON.parse(otherUsers);
        var countOtherUsers = Object.keys(otherUserArray).length;
        if(countOtherUsers > 0)
        {
            $.each(otherUserArray, function (userId, value) {
                $.each(value, function (companyId, userName) {
                    if(companyId == paramCompanyId) {
                        employee.append('<option value="' + userId + '">' + userName + '</option>');
                        countUsers++;
                    }
                });
            });
        }
    }

    if(tab == 'inspections') {
        //other user option
        employee.append('<option value="0">Other user</option>');

        //show other company
        if (paramCompanyId == 0) {
            $('.other_company').show();
        } else {
            $('.other_company').hide();
        }

        //show other employee
        if (countUsers == 0) {
            $('.other_employee').show();
        } else {
            $('.other_employee').hide();
        }
    }

    if(paramEmployeeID) {
        employee.val(paramEmployeeID);
    }
}

$("#inspections_selected_employee_id").on("change", function(){
    //show other employee
    if ($(this).val() == 0) {
        $('.other_employee').show();
    } else {
        $('.other_employee').hide();
    }
});


/****************************************************
 * **************************************************
 * ******** Custom Report Number ********************
 * **************************************************
 */
$("#custom_report_num").on("click", function(){
    if ($('#report_num').is('[readonly]')) {
        $('#report_num').prop('readonly',false);
        $('#custom_report_num').text('System generated report number');
    } else {
        $('#report_num').prop('readonly',true);
        $('#custom_report_num').text('Enter custom report number');
    }
});

$("#custom_mf_button").on("click", function(){
    if ($('#inspections_spec_or_dwg').is('[readonly]')) {
        $('#inspections_spec_or_dwg').prop('readonly',false);
        $('#inspections_mf_number').prop('readonly',true);
        $('#inspections_mf_title').prop('readonly',true);
        $('#custom_mf_button').text('Enter custom number and title');
    } else {
        $('#inspections_spec_or_dwg').prop('readonly',true);
        $('#inspections_mf_number').prop('readonly',false);
        $('#inspections_mf_title').prop('readonly',false);
        $('#custom_mf_button').text('Search master format number and title');
    }
});

/****************************************************
 * **************************************************
 * ******** Report Datepicker ***********************
 * **************************************************
 */
$("#report_date").datepicker({
    changeMonth: true,
    changeYear: true,
    dateFormat: "mm/dd/yy",
    onSelect: function(date) {
        var selectedDate = $(this).datepicker('getDate');
        var dayOfWeek = selectedDate.getUTCDay();

        var dayOfWeeks = {
            0 : "Monday",
            1 : "Tuesday",
            2 : "Wednesday",
            3 : "Thursday",
            4 : "Friday",
            5 : "Saturday",
            6 : "Sunday"
        };

        $("#day_of_week").val(dayOfWeeks[dayOfWeek]);
    }
});

//clear day_of_week if report_date is empty
$("#report_date").keyup(function() {
    if (!this.value) {
        $("#day_of_week").val('');
    }
});