/**
 * Created by dragan.atanasov on 4/29/2015.
 */
$(function() {

    $(document).on('click', '#custom-mf', function() {
        var mfCont = $("div.mf-cont");
        var search = $('input.mf-number-title-auto');
        var mfNumber = $('input.mf-number-auto');
        var mfTitle = $('input.mf-title-auto');
        search.prop('readonly', true);
        search.val('');
        mfNumber.prop('readonly', false);
        mfNumber.val('');
        mfTitle.prop('readonly', false);
        mfTitle.val('');
        mfCont.empty();
        mfCont.append('<label class="control-label">&nbsp;</label><a href="javascript:;" id="search-mf" class="btn btn-primary pull-right mb0">Search</a>');
    });

    $(document).on('click', '#custom-mf-sub', function() {
        var mfCont = $("div.mf-cont");
        var search = $('input.mf-number-title-auto');
        var mfNumber = $('input.mf-number-auto');
        var mfTitle = $('input.mf-title-auto');
        search.prop('readonly', true);
        search.val('');
        mfNumber.prop('readonly', false);
        mfNumber.val('');
        mfTitle.prop('readonly', false);
        mfTitle.val('');
        mfCont.empty();
        mfCont.append('<a href="javascript:;" id="search-mf-sub" class="btn btn-primary pull-right mb0">Search</a>');
    });

    $(document).on('click', '#search-mf', function() {
        var mfCont = $("div.mf-cont");
        var search = $('input.mf-number-title-auto');
        var mfNumber = $('input.mf-number-auto');
        var mfTitle = $('input.mf-title-auto');
        search.prop('readonly', false);
        search.val('');
        mfNumber.prop('readonly', true);
        mfNumber.val('');
        mfTitle.prop('readonly', true);
        mfTitle.val('');
        mfCont.empty();
        mfCont.append('<label class="control-label">&nbsp;</label><a href="javascript:;" id="custom-mf" class="btn btn-primary pull-right mb0">Custom</a>');
    });

    $(document).on('click', '#search-mf-sub', function() {
        var mfCont = $("div.mf-cont");
        var search = $('input.mf-number-title-auto');
        var mfNumber = $('input.mf-number-auto');
        var mfTitle = $('input.mf-title-auto');
        search.prop('readonly', false);
        search.val('');
        mfNumber.prop('readonly', true);
        mfNumber.val('');
        mfTitle.prop('readonly', true);
        mfTitle.val('');
        mfCont.empty();
        mfCont.append('<a href="javascript:;" id="custom-mf-sub" class="btn btn-primary pull-right mb0">Custom</a>');
    });

    $("#transmittal_info").click(function() {
        $("#transmittal_info_cont").slideToggle("slow");
    });

    $('#submittals').submit(function(e) {
        $('#submittal-file-submit').attr('disabled', 'disabled');
        $('<p>Please wait...</p>').appendTo('.please_wait');
    });
});