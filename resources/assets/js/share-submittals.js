/**
 * Created by dragan.atanasov on 6/5/2015.
 */

$(function() {

    $('#submittals_select_all').click(function(){
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ($('#submittals_select_all').is(':checked')) {
            $('.submittal').prop('checked', true);
        }
        else {
            $('.submittal').removeAttr('checked');
        }
        generateSelectedItemIdsDelete('.multiple-items-checkbox', '');
    });

    $('#transmittal_select_all').click(function(){
        // For some browsers, `attr` is undefined; for others,
        // `attr` is false.  Check for both.
        if ($('#transmittal_select_all').is(':checked')) {
            $('.transmittal').prop('checked', true);
        }
        else {
            $('.transmittal').removeAttr('checked');
        }
    });

    //share selected files with selected companies - display input event
    $('#subm_share_with').change(function(){
        var value = $(this).val();
        if(value == 'all') {
            $('#selected_input_cont').css('display','none');
            $('.project-subcontractor-auto').val('');
            $('.project-subcontractor-id').val('');

            $('#selected_subcontractors_cont').css('display','none');
            $('#selected_subcontractors').empty();
        }
        if(value == 'selected') {
            $('#selected_input_cont').css('display','block');
            $('#selected_subcontractors_cont').css('display','block');
        }
    });

    //share files
    $('#share_subm_file').click(function(){
        //get selected files ids
        var selected_files = $("#unshared-files input:checkbox:checked").map(function(){
            return $(this).val();
        }).get();

        //share files with all subcontractors
        if($('#subm_share_with').val() == 'all') {
            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing.');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/all/submittals",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        project_id: $('#project-id').val()
                    },
                    error: function(data) {
                        //alert('Please share this project with your companies subcontractors first before trying to share project files.');
                        $('#alertPopup').find('.modal-body p').text('Please share this project with your companies subcontractors first before trying to share project files.');
                        $('#alertPopup').modal();
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared.');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
        //share files with selected companies
        else {
            //get selected subcontractors ids
            var selected_subcontractors = $(".subcontractor-id").map(function(){
                return $(this).val();
            }).get();

            if(selected_files.length === 0) {
                //alert('Please select files for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select files for sharing');
                $('#alertPopup').modal();
            } else if(selected_subcontractors.length === 0) {
                //alert('Please select companies for sharing');
                $('#alertPopup').find('.modal-body p').text('Please select companies for sharing');
                $('#alertPopup').modal();
            } else {
                $.ajax({
                    url: site_url+"/share/selected/submittals",
                    dataType: "json",
                    data: {
                        selected_files: selected_files,
                        selected_subcontractors: selected_subcontractors
                    },
                    success: function(data) {
                        //alert('Files were successfully shared');
                        $('#alertPopup').find('.modal-body p').text('Files were successfully shared');
                        $('#alertPopup').modal();
                        //location.reload();
                    }
                });
            }
        }
    });


});
