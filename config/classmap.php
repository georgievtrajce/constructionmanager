<?php
/**
 * Created by PhpStorm.
 * User: dragan.atanasov
 * Date: 6/4/2015
 * Time: 9:48 AM
 */

return [


    /*
	|--------------------------------------------------------------------------
	| Address Book Module
	|--------------------------------------------------------------------------
	|
	| Returning the address book module (in App -> Modules -> Address_book)
	| classes needed for different types of implementations.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'address-book' => [

        'default-category' => 'App\Modules\Address_book\Implementations\DefaultCategory',
        'custom-category' => 'App\Modules\Address_book\Implementations\CustomCategory',
        'mf-filter-all' => 'App\Modules\Address_book\Implementations\MasterFormatFilterAll',
        'mf-filter-segment' => 'App\Modules\Address_book\Implementations\MasterFormatFilterSegment',
        'mf-filter-concrete' => 'App\Modules\Address_book\Implementations\MasterFormatFilterConcrete',
        'company-name' => 'App\Modules\Address_book\Implementations\CompanyNameFilter',
    ],



    /*
	|--------------------------------------------------------------------------
	| Addresses Module
	|--------------------------------------------------------------------------
	|
	| Returning the addresses module (in App -> Modules -> Addresses)
	| classes needed for different types of implementations.
    | There are get, store, update and delete classes for every different
    | type of address.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'addresses' => [

        'address-book' => [
            'get' => 'App\Modules\Addresses\Implementations\GetAddressBookAddresses',
            'store' => 'App\Modules\Addresses\Implementations\StoreAddressBookAddress',
            'update' => 'App\Modules\Addresses\Implementations\EditAddressBookAddress',
            'delete' => 'App\Modules\Addresses\Implementations\CheckAddressBookAddressDelete',
        ],

        'company-profile' => [
            'get' => 'App\Modules\Addresses\Implementations\GetCompanyAddresses',
            'store' => 'App\Modules\Addresses\Implementations\StoreCompanyAddress',
            'update' => 'App\Modules\Addresses\Implementations\EditCompanyAddress',
            'delete' => 'App\Modules\Addresses\Implementations\CheckCompanyAddressDelete',
        ]

    ],


    /*
	|--------------------------------------------------------------------------
	| Auto Complete Module
	|--------------------------------------------------------------------------
	|
	| Returning the auto complete module (in App -> Modules -> Auto_complete)
	| classes needed for different types of implementations.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'auto-complete' => [
        'master-format' => 'App\Modules\Auto_complete\Implementations\MasterFormatAutocomplete',
        'address-book' => 'App\Modules\Auto_complete\Implementations\AddressBookAutocomplete',
        'module-companies' => 'App\Modules\Auto_complete\Implementations\CompaniesAddressBookAutocomplete',
        'module-companies-share' => 'App\Modules\Auto_complete\Implementations\CompaniesAddressBookShareAutocomplete',
        'project-subcontractors' => 'App\Modules\Auto_complete\Implementations\ProjectSubcontractorsAutocomplete',
        'company' => 'App\Modules\Auto_complete\Implementations\CompanyAutocomplete',
        'address-book-cat' => 'App\Modules\Auto_complete\Implementations\AddressBookCatAutoComplete'

    ],


    /*
	|--------------------------------------------------------------------------
	| Contacts Module
	|--------------------------------------------------------------------------
	|
	| Returning the contacts module (in App -> Modules -> Contacts)
	| classes needed for different types of implementations.
    | There are get, store and edit classes for every different
    | type of contact.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'contacts' => [

        'address-book' => [

            'edit' => 'App\Modules\Contacts\Implementations\EditAddressBookContact',
            'get' => 'App\Modules\Contacts\Implementations\GetAddressBookContacts',
            'store' => 'App\Modules\Contacts\Implementations\StoreAddressBookContact'

        ],

    ],


    /*
	|--------------------------------------------------------------------------
	| Filter Module
	|--------------------------------------------------------------------------
	|
	| Returning the filter module (in App -> Modules -> Filter)
	| classes needed for different types of implementations.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'filter' => [

        'proposals' => 'App\Modules\Filter\Implementations\ProposalsFilter',
        'proposals-report' => 'App\Modules\Filter\Implementations\ProposalsReport',
        'contracts' => 'App\Modules\Filter\Implementations\ContractsFilter',
        'submittals' => 'App\Modules\Filter\Implementations\SubmittalsFilter',
        'submittals-report' => 'App\Modules\Filter\Implementations\SubmittalsReport',
        'submittals-shared' => 'App\Modules\Filter\Implementations\SharedSubmittalsFilter',
        'submittals-shared-report' => 'App\Modules\Filter\Implementations\SharedSubmittalsReport',
        'submittal-transmittals' => 'App\Modules\Filter\Implementations\SubmittalTransmittalsFilter',
        'submittal-transmittals-filter' => 'App\Modules\Filter\Implementations\SubmittalTransmittalsDataFilter',
        'rfis' => 'App\Modules\Filter\Implementations\RfisFilter',
        'rfis-report' => 'App\Modules\Filter\Implementations\RfisReport',
        'rfis-shared' => 'App\Modules\Filter\Implementations\SharedRfisFilter',
        'rfis-shared-report' => 'App\Modules\Filter\Implementations\SharedRfisReport',
        'rfi-transmittals' => 'App\Modules\Filter\Implementations\RfiTransmittalsFilter',
        'rfi-transmittals-filter' => 'App\Modules\Filter\Implementations\RfiTransmittalsDataFilter',
        'pcos' => 'App\Modules\Filter\Implementations\PcosFilter',
        'pcos-report' => 'App\Modules\Filter\Implementations\PcosReport',
        'pcos-shared' => 'App\Modules\Filter\Implementations\SharedPcosFilter',
        'pcos-shared-report' => 'App\Modules\Filter\Implementations\SharedPcosReport',
        'pco-transmittals' => 'App\Modules\Filter\Implementations\PcoTransmittalsFilter',
        'pco-transmittals-filter' => 'App\Modules\Filter\Implementations\PcoTransmittalsDataFilter',
        'materials' => 'App\Modules\Filter\Implementations\MaterialsFilter',
        'materials-shared' => 'App\Modules\Filter\Implementations\SharedMaterialsFilter',
        'materials-shared-report' => 'App\Modules\Filter\Implementations\SharedMaterialsReport',
        'address-book-company' => 'App\Modules\Filter\Implementations\AddressBookCompanyReport',
        'company-office' => 'App\Modules\Filter\Implementations\CompanyOfficeReport',
        'company-contact' => 'App\Modules\Filter\Implementations\AddressBookContactReport',

    ],

    /*
	|--------------------------------------------------------------------------
	| Invitation classes
	|--------------------------------------------------------------------------
	|
	| Returning the invitations module (in App -> Modules -> Invitations)
	| classes needed for different types of invitations like inviting company
    | with sharing a project through Companies Module or invite company to bid
    | on a project through Bids Module.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'invitations' => [
        'accept' => [
            'subcontractor' => 'App\Modules\Invitations\Implementations\AcceptSubcontractor',
            'bidder' => 'App\Modules\Invitations\Implementations\AcceptBidder',
        ],
        'reject' => [
            'subcontractor' => 'App\Modules\Invitations\Implementations\RejectSubcontractor',
            'bidder' => 'App\Modules\Invitations\Implementations\RejectBidder',
        ],
    ],


    /*
	|--------------------------------------------------------------------------
	| Registration Module
	|--------------------------------------------------------------------------
	|
	| Returning the registration module (in App -> Modules -> Registration)
	| classes needed for different types of subscription.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'registration' => [

        '1' => 'App\Modules\Registration\Implementations\FirstSubscription',
        '2' => 'App\Modules\Registration\Implementations\SecondSubscription',
        '3' => 'App\Modules\Registration\Implementations\ThirdSubscription',
        '4' => 'App\Modules\Registration\Implementations\SalesAgentSubscription',
        '5' => 'App\Modules\Registration\Implementations\ZeroSubscription',
        '6' => 'App\Modules\Registration\Implementations\TestAccountSubscription'

    ],

    /*
	|--------------------------------------------------------------------------
	| Payments
	|--------------------------------------------------------------------------
	|
	| Returning the registration module (in App -> Modules -> Registration)
	| classes needed for different types of subscription.
	| This classes are handled and instantiate in Utilities -> ClassMap.php
    | The Class Map returns new instance from the needed class.
	|
	*/
    'payments' => [

        '1' => 'App\Modules\Registration\Implementations\FirstSubscriptionPayment',
        '2' => 'App\Modules\Registration\Implementations\SecondSubscriptionPayment',
        '3' => 'App\Modules\Registration\Implementations\ThirdSubscriptionPayment',
        '5' => 'App\Modules\Registration\Implementations\ZeroSubscriptionPayment'

    ],

    /*
	|--------------------------------------------------------------------------
	| Store File
	|--------------------------------------------------------------------------
	|
	| Returning the different file type storing (proposals, contracts, project files)
	|
	*/
    'store-file' => [
        'project-file' => 'App\Services\Files\StoreProjectFile',
        'bid' => 'App\Services\Files\StoreProposalFile',
        'contract' => 'App\Services\Files\StoreContractFile',
        'tasks' => 'App\Services\Files\StoreTasksFile',
        'address_book_document' => 'App\Services\Files\StoreAddressBookFile'
    ],

    /*
	|--------------------------------------------------------------------------
	| Delete File
	|--------------------------------------------------------------------------
	|
	| Returning the different file type needed for deleting files (proposals, contracts, project files,
    | submittals, RFI's and PCO's)
	|
	*/
    'delete-file' => [
        'project-files' => 'App\Services\Files\DeleteProjectFile',
        'bids' => 'App\Services\Files\DeleteProposalFile',
        'proposals' => 'App\Services\Files\DeleteProposalFile',
        'contracts' => 'App\Services\Files\DeleteContractFile',
        'submittals' => 'App\Services\Files\DeleteSubmittalFile',
        'rfis' => 'App\Services\Files\DeleteRfiFile',
        'pcos' => 'App\Services\Files\DeletePcoFile',
        'address-book-files' => 'App\Services\Files\DeleteAddressBookFile',
        'tasks' => 'App\Services\Files\DeleteTasksFile',
    ],

    /*
	|--------------------------------------------------------------------------
	| Pco version handling
	|--------------------------------------------------------------------------
	|
	| Returning the different version type storing, updating and deleting (subcontractors and recipient)
	|
	*/
    'pcos' => [

        'subcontractors' => 'App\Modules\Pcos\Implementations\SubcontractorVersions',
        'recipient' => 'App\Modules\Pcos\Implementations\RecipientVersions',

    ],

    /*
	|--------------------------------------------------------------------------
	| Different type of transmittals generator
	|--------------------------------------------------------------------------
	|
	| Transmittals could be of type submittal, rfi or pco
	|
	*/
    'transmittals' => [

        'submittal' => 'App\Modules\Transmittals\Implementations\GenerateSubmittalTransmittal',
        'rfi' => 'App\Modules\Transmittals\Implementations\GenerateRfiTransmittal',
        'pco' => 'App\Modules\Transmittals\Implementations\GeneratePcoTransmittal',

    ],

    /*
	|--------------------------------------------------------------------------
	| Different type of transmittals database table connector
	|--------------------------------------------------------------------------
	|
	| Transmittals could be of type submittal, rfi or pco
	|
	*/
    'transmittals-database' => [

        'submittal' => 'App\Modules\Transmittals\Implementations\DatabaseSubmittalTransmittal',
        'rfi' => 'App\Modules\Transmittals\Implementations\DatabaseRfiTransmittal',
        'pco' => 'App\Modules\Transmittals\Implementations\DatabasePcoTransmittal',

    ],

    /*
	|--------------------------------------------------------------------------
	| Different type of transmittal module table connector for management
    | and manipulation with the transmittal notifications
	|--------------------------------------------------------------------------
	|
	| Transmittals could be of type submittal, rfi or pco
	|
	*/
    'transmittals-notifications' => [

        'submittal' => 'App\Modules\Transmittals\Implementations\SubmittalNotificationsTransmittal',
        'rfi' => 'App\Modules\Transmittals\Implementations\RfiNotificationsTransmittal',
        'pco' => 'App\Modules\Transmittals\Implementations\PcoNotificationsTransmittal',

    ],

    /*
	|--------------------------------------------------------------------------
	| Different type of email notification - class implementation via strategy
	|--------------------------------------------------------------------------
	|
	| Email notifications could be of type project files
	|
	*/
    'email-notifications' => [

        'project-files' => 'App\Modules\Project_files\Implementations\ProjectFileNotifications',

]

];