<?php

return [
    'merchant-id' => env('PAY_ID'),

    //live environment test payment page token
    //'token' => '3af502e1hh4768a7cg',

    //local environment test payment page token
    //'token' => '179g2f22a2fi2i1882',
    //'token' => '1igfdd248ai5efh4c',

    //test server environment test payment page token
    //'token' => 'c77bha2i81bi3436f',

    //aws - test environment page token
    //'token' => 'g86h8gd78e01cea17c',

    //AWS - production token
    'token' => env('PAY_TOKEN'),

    'subscription-level-2' => '180.00',
    'subscription-level-3' => '270.00',
    'numberOfDays'         => 365,

];
