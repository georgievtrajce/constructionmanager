<?php

return [

    'level-zero' => 5,

    'level-zero-title' => 'Level 0',

    'level-one' => 1,

    'level-one-title' => 'Level 1',

    'level-two' => 2,

    'level-two-title' => 'Level 2',

    'level-three' => 3,

    'sales-agent' => 4,

    'test-account' => 6,

    'test-account-title' => 'Test Account',

    'level-three-title' => 'Level 3',

    'level-one-referral-points' => 0,

    'level-two-referral-points' => 1,

    'level-three-referral-points' => 2,

    'level-two-referral-points-free' => 5,

    'level-three-referral-points-free' => 10,

    'level-one-projects' => 1,

    'level-two-projects' => 3,

    'level-two-files' => 3221225472,

    'file-create-upload' => 'create',

    'file-update-upload' => 'update',

    'address-book' => 'address-book',

    'project' => 'projects',

    'projects' => [

        'shared' => 'shared',
        'completed' => 'completed',
        'companies' => 'companies',
        'proposals' => 'proposals',
        'contracts' => 'contracts',
        'submittals' => 'submittals',
        'materials_and_services' => 'materials_and_services',
        'blog' => 'blog',
        'project-files' => 'project-files'

    ],

    'manage-users' => 'manage-users',

    'company-profile' => 'company-profile',

    'referral-points' => [
        'level-two-lowest-limit' => 5,
        'level-two-highest-limit' => 10,
        'level-three-lowest-limit' => 10,
    ],

    'level1' => [
        'id'                => 1,
        'ref_points'        => 0,
        'projects'          => 1,
        'ref_points_needed' => 0,
        'files'             => 0,
        'upload_limit'      => 1073741824,
        'download_limit'    => 1073741824,
        'storage_limit'     => 10737418240,
    ],
    'level2' => [
        'id'                => 2,
        'ref_points'        => 1,
        'projects'          => 3,
        'ref_points_needed' => 5,
        'files'             => 3221225472,
        'upload_limit'      => 5368709120,
        'download_limit'    => 5368709120,
        'storage_limit'     => 32212254720,
    ],
    'level3' => [
        'id'                => 3,
        'ref_points'        => 2,
        'projects'          => 100,
        'ref_points_needed' => 10,
        'files'             => 32212254723221225472,
        'upload_limit'      => 10737418240,
        'download_limit'    => 10737418240,
        'storage_limit'     => 53687091200,
    ],
    'sales_agent' => [
        'id'                => 4,
        'ref_points'        => 2,
        'projects'          => 100,
        'ref_points_needed' => 10,
        'files'             => 32212254723221225472,
        'upload_limit'      => 10737418240,
        'download_limit'    => 10737418240,
    ],
    'level0' => [
        'id'                => 5,
        'ref_points'        => 0,
        'projects'          => 0,
        'ref_points_needed' => 0,
        'files'             => 0,
        'upload_limit'      => 0,
        'download_limit'    => 0,
    ],
    //this should be removed when changes-2-2 are pushed
    'testaccount' => [
        'id'                => 6,
        'ref_points'        => 2,
        'projects'          => 100,
        'ref_points_needed' => 10,
        'files'             => 32212254723221225472,
        'upload_limit'      => 10737418240,
        'download_limit'    => 10737418240,
    ],
    'test_account' => [
        'id'                => 6,
        'ref_points'        => 2,
        'projects'          => 100,
        'ref_points_needed' => 10,
        'files'             => 32212254723221225472,
        'upload_limit'      => 10737418240,
        'download_limit'    => 10737418240,
    ],
    'account_type' => [
        'payed' => 'payed',
        'free' => 'free',
    ]

];