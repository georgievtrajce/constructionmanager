<?php

return [
    //submittals and PCO's all open statuses
    'all_open' => [
        'AAN/R', 'R&R', 'REJ/R', 'SFA'
    ],

    'all_closed' => [
        'AAN', 'APP'
    ],

    'submittal_statuses' => [
        'expediting_report_exclude' => [
            'rejected' => 'Rejected'
        ]
    ],

    'module_permission_type' => [
        'companies' => 'companies',
        'bids' => 'bids'
    ],

    'company_type' => [
        'creator' => 0,
        'subcontractor' => 1
    ],

    'shared_files' => [
        'master' => 'master',
        'received' => 'received',
    ],

    'project_files_upload_type' => [
        'create' => 'create',
        'update' => 'update',
    ],
    'address_book_categories' => [
        'all' => 'All',
        'default' => 'default-category',
        'custom' => 'custom-category',
    ],
    'pagination' => [
        'contracts' => 50,
        'proposals' => 50,
        'blog_posts' => 3,
        'materials_and_services' => 50,
        'projects'  => 50,
        'companies' => 50,
        'logs' => 10,
        'all_files' => 20,
        'all_blog_posts' => 20,
        'super_admin_companies' => 50,
        'address_book_files' => 10,
        'project_files' => 50
    ],
    'entity_type' => [
        'module' => 1,
        'file' => 2,
        'other' => 3,
    ],

    'modules_display_names' => [
        'expediting_report' => 'expediting-report'
    ],

    'modules' => [
        'projects' => 'Projects',
        'project_info' => 'Project Info',
        'blog' => 'Blog',
        'bids' => 'Bids',
        'companies' => 'companies',
        'master_files' => 'shared',
        'received_files' => 'received',
        'submittals' => 'Submittals',
        'contracts' => 'Contracts',
        'expediting-reports' => 'Expediting Reports',
        'companies-module' => 'Companies',
        'pcos' => 'PCO\'s',
        'tasks' => 'Tasks',
        'permits' => 'Permits',
        'work-schedules' => 'Work Schedules',
        'punch-lists' => 'Punch Lists',
        'meeting-minutes' => 'Meeting Minutes',
        'closeout-documents' => 'Closeout Documents',
        'billing' => 'Billing',
        'download_batch_files' => 'Download All Project Files',
        'rfis' => 'RFI\'s',
        'address-book' => 'Address Book & Custom categories',
        'all-projects' => 'All projects & modules',
        'daily-reports' => 'Daily Reports',
        'daily-reports-images' => 'Daily Reports Images',
        'daily-reports-files' => 'Daily Reports Files',
        'correspondence-emails' => 'Correspondence/Emails',
    ],

    'recent_files' => [
        'files'=> 5,
        'blog'=> 5
    ],
    'projects'=>[
        'shared_status' =>[
            'pending'=>1,
            'accepted'=>2,
            'rejected'=>3,
            'unshared'=>4
        ],
        'status' => [
            'active' => 1,
            'inactive' => 0,
        ],
        'once_accepted' => [
            'yes' => 1,
            'no' => 0,
        ],
        'submittals_counter' => 'submittals_counter',
        'rfi_counter' => 'rfi_counter',
        'pco_counter' => 'pco_counter',
    ],
    'bids' => [
        'status' => [
            'not_invited' => 0,
            'pending' => 1,
            'accepted' => 2,
            'rejected' => 3,
            'unshared' => 4,
            'invited' => 5,
        ],
        'invitation_type' => [
            'proper' => 1,
            'informational' => 2
        ],
        'bid_item_status' => [
            'open' => 'open',
            'closed' => 'closed'
        ],
        'plural' => 'bids'
    ],
    'materials_and_services'=>[
        'submittals_needed'=> [
            'yes'=>1,
            'no'=>0,
        ]
    ],

    'proposal_files' => [
        'first_proposal' => 1,
        'second_proposal' => 2,
        'final_proposal' => 3,
    ],

    'proposal_statuses' => [
        'open' => 'open',
        'closed' => 'closed'
    ],

    //* Project status *//
    'status' => [
        'queue' => 2,
        'active' => 1,
        'inactive' => 0,
    ],

    'hasPermission' => 1,
    'hasNoPermission'=>0,
    'readMore'      => 300,
    'homepage' => [
        'blog_posts'    => 8,
    ],

    'roles' => [
        'super_admin' => 'Super Admin',
        'company_admin' => 'Company Admin',
        'company_user' => 'Company User',
        'project_admin' => 'Project Admin',
    ],
    's3_base_url' => 'https://s3.amazonaws.com',
    'project-files' => 'project-files',
    'address-book-files' => 'address-book-files',
    'contracts' => 'contracts',
    'proposals' => 'proposals',
    'proposals-report' => 'proposals-report',
    'address-book-report' => 'address-book-report',
    'tasks-report' => 'tasks-report',
    'submittals' => 'submittals',
    'submittals-report' => 'submittals-report',
    'submittals-shared' => 'submittals-shared',
    'submittals-shared-report' => 'submittals-shared-report',
    'submittals_versions' => 'submittals_versions',
    'rfis' => 'rfis',
    'rfis-report' => 'rfis-report',
    'rfis-shared' => 'rfis-shared',
    'rfis-shared-report' => 'rfis-shared-report',
    'rfis_versions' => 'rfis_versions',
    'pcos' => 'pcos',
    'pcos-report' => 'pcos-report',
    'pcos-shared' => 'pcos-shared',
    'pcos-shared-report' => 'pcos-shared-report',
    'materials' => 'materials',
    'materials-shared' => 'materials-shared',
    'materials-shared-report' => 'materials-shared-report',
    'address-book-company' => 'address-book-company',
    'recipient_versions' => 'recipient_versions',
    'subcontractor_versions' => 'subcontractor_versions',

    'submittal_version_files' => [
        'rec_sub_file' => 'rec_sub_file',
        'sent_appr_file' => 'sent_appr_file',
        'rec_appr_file' => 'rec_appr_file',
        'subm_sent_sub_file' => 'subm_sent_sub_file',
    ],

    'rfi_version_files' => [
        'rec_sub_file' => 'rec_sub_file',
        'sent_appr_file' => 'sent_appr_file',
        'rec_appr_file' => 'rec_appr_file',
        'subm_sent_sub_file' => 'subm_sent_sub_file',
    ],

    'submittal_date_type' => [
        'rec_sub' => 'rec_sub',
        'sent_appr' => 'sent_appr',
        'rec_appr' => 'rec_appr',
        'subm_sent_sub' => 'subm_sent_sub',
    ],

    'transmittal_types' => [
        'transmittalSubmSentFile' => 'transmittalSubmSentFile',
        'transmittalSentFile' => 'transmittalSentFile',
    ],

    'transmittal_date_type' => [
        'sent_appr' => 'sent_appr',
        'subm_sent_sub' => 'subm_sent_sub',
    ],

    'pco_version_type' => [
        'subcontractors' => 'subcontractors',
        'recipient' => 'recipient',
    ],

    'pco_recipient' => [
        'empty_recipient' => 0
    ],

    'pco_subcontractor_or_recipient' => [
        'subcontractor' => 0,
        'recipient' => 1
    ],

    'pco_permission_type' => [
        'none' => 0,
        'all_pcos' => 1,
        'his_pcos' => 2
    ],

    'transmittal_flags' => [
        'transmittal' => 1,
        'regular' => 0
    ],

    'transmittal_type' => [
        'submittal' => 'submittal',
        'rfi' => 'rfi',
        'pco' => 'pco',
    ],

    'transmittal_emailed' => [
        'yes' => 1,
        'no' => 0
    ],

    'ab_company_type' => [
        'subcontractor' => 'subcontractor',
        'recipient' => 'recipient'
    ],

    'file_transmittal_type' => [
        'submittals' => 'submittals',
        'rfis' => 'rfis',
        'pcos' => 'pcos',
    ],

    'transmittal_view' => [
        'submittal' => 'projects.submittals.transmittal',
        'rfi' => 'projects.rfis.transmittal',
        'pco_subcontractor' => 'projects.pcos.transmittal',
        'pco_recipient' => 'projects.pcos.recipient-transmittal',
    ],

    'received_files_modules' => [
        'submittals' => 'submittals',
        'rfis' => 'rfis',
        'pcos' => 'pcos'
    ],

    'tasks' => [
        'status' => ['1' => 'Open', '2' => 'Closed'],
        'type' => ['4' => 'Bids', '5' => 'Contracts', '3' => 'Submittals', '9' => 'RFIs', '10' => 'PCOs', '6' => 'Materials']
    ],

    'submittal_transmittals' => 'submittal-transmittals',

    'rfi_transmittals' => 'rfi-transmittals',

    'pco_transmittals' => 'pco-transmittals',

    'max-batch-file-size' => 250000000,

    'empty_date' => "0000-00-00",

    'received_under_review' => 'Received - Under Review',

    //Daily Report Constants
    'daily_report_file_type_id' => 20,
    'daily_report_image_file_type_id' => 21,
    'daily_report_pdf_file_type_id' => 22,
    'daily_report_file_type' => 'daily-reports',
    'daily_report_short_file_type' => 'report',
    'daily_report_uploaded' => 'Uploaded Daily Report',
    'workers-with-name' => 'workers-with-name',
    'generate_daily_report_name' => '%s - Daily Report: %s %s',
    'generate_daily_report_type' => 'daily report type',
    'generate_daily_report_view' => 'projects.daily_reports.pdf-report',
    'carbon_days' => [
        0 => 'Sunday',
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday'
    ],
    'daily_file_types' => [
        19 => 'report',
        20 => 'image',
        21 => 'pdf',
    ],
    'daily_report_steps' => [1,2,3,4,5,6,7,8,9,10,11,12],
    'upload' => [
        'subcontractor' => 'Upload as Subcontractor',
        'recipient' => 'Upload as Recipient'
    ]
];
