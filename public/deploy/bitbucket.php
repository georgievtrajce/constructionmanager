<?php
if ($_GET['key'] != 'cms123!') {
    die('No Access !!!');
}

if ($_GET['destination'] == 'test') {
    $branch = 'master';

    if(isset($_GET['branch'])){
        $branch = $_GET['branch'];
    }


    $deploy_log = '/var/www/testcm/public/storage/logs/deploy.log';
    $repo_dir = '/var/www/testcm/public';
    //$sudo_user='sudo -u constructionmaster ';
    $sudo_user='';

    $web_root_dir = '/var/www/testcm/public';


    file_put_contents($deploy_log, FILE_APPEND);


    $x = shell_exec($sudo_user . '/usr/bin/git -C /var/www/testcm/public/ reset --hard origin/'. $branch);
    print $x;
    file_put_contents($deploy_log, date('m/d/Y h:i:s a')  . "\n" . $x, FILE_APPEND);

    $x = shell_exec($sudo_user . '/usr/bin/git -C /var/www/testcm/public/ pull origin '. $branch);
    print $x;
    $commit_hash = shell_exec($sudo_user . '/usr/bin/git -C /var/www/testcm/public/ rev-parse --short HEAD');

    $x = shell_exec($sudo_user . '/var/www/testcm/public/composer.phar update');
    print $x;
    $x = shell_exec($sudo_user . '/var/www/testcm/public/composer.phar dump-autoload');
    print $x;


    print (date('m/d/Y h:i:s a') . " Deployed branch:" . $branch .   " Commit: " . $commit_hash);
    file_put_contents($deploy_log, date('m/d/Y h:i:s a') . " Deployed branch:" . $branch .   " Commit: " . $commit_hash . "\n", FILE_APPEND);
} else if ($_GET['destination'] == 'production') {
    $branch = 'master';

    if(isset($_GET['branch'])){
        $branch = $_GET['branch'];
    }


    $deploy_log = '/var/www/cm/public/storage/logs/deploy.log';
    $repo_dir = '/var/www/cm/public';
    //$sudo_user='sudo -u constructionmaster ';
    $sudo_user='';

    $web_root_dir = '/var/www/cm/public';


    file_put_contents($deploy_log, FILE_APPEND);


    $x = shell_exec($sudo_user . '/usr/bin/git -C /var/www/cm/public/ reset --hard origin/'. $branch);
    print $x;
    file_put_contents($deploy_log, date('m/d/Y h:i:s a')  . "\n" . $x, FILE_APPEND);

    $x = shell_exec($sudo_user . '/usr/bin/git -C /var/www/cm/public/ pull origin '. $branch);
    print $x;
    $commit_hash = shell_exec($sudo_user . '/usr/bin/git -C /var/www/cm/public/ rev-parse --short HEAD');

    $x = shell_exec($sudo_user . '/var/www/cm/public/composer.phar update');
    print $x;
    $x = shell_exec($sudo_user . '/var/www/cm/public/composer.phar dump-autoload');
    print $x;


    print (date('m/d/Y h:i:s a') . " Deployed branch:" . $branch .   " Commit: " . $commit_hash);
    file_put_contents($deploy_log, date('m/d/Y h:i:s a') . " Deployed branch:" . $branch .   " Commit: " . $commit_hash . "\n", FILE_APPEND);
} else {
    die('No Access !!!');
}