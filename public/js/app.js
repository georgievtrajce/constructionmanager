// Equal Height
$(function () {
	equalheight = function(container){

	var currentTallest = 0,
	     currentRowStart = 0,
	     rowDivs = new Array(),
	     $el,
	     topPosition = 0;
	 $(container).each(function() {

	   $el = $(this);
	   $($el).height('auto')
	   topPostion = $el.position().top;

	   if (currentRowStart != topPostion) {
	     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	       rowDivs[currentDiv].height(currentTallest);
	     }
	     rowDivs.length = 0; // empty the array
	     currentRowStart = topPostion;
	     currentTallest = $el.height();
	     rowDivs.push($el);
	   } else {
	     rowDivs.push($el);
	     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
	  }
	   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
	     rowDivs[currentDiv].height(currentTallest);
	   }
	 });
	}
});

// ScrollTo

$(function () {
	$('.cms-header-navigation a[href^="#"]').on('click',function (e) {
	    e.preventDefault();
	    $('.cms-header-navigation a').removeClass('active');
	    $(this).addClass('active');
	    var target = this.hash,
	    $target = $(target);
	    $('html, body').stop().animate({
	        scrollTop: $target.offset().top
	    }, 700, function () {
	        window.location.hash = target;
	    });
	});
});

// Fixed Header

function transparentHeader () {
	var header = $('.cms-header-wrap');
	var headerHeight = header.outerHeight();

    $(window).scroll(function () {
        if ($(this).scrollTop() > headerHeight) {
            header.removeClass('transparent');
            
        } else if ($(this).scrollTop() <= headerHeight) {
            header.addClass('transparent');
            
        }
    });
};

// Navigation

$(function () {
	$('.cms-header-navigation-trigger').on('click', function(){
		$('.cms-header-navigation').slideToggle();

	});
});

// Flyer
function openFlyer () {
	
	var el = $('.cms-page-flyer');
	var elCld = $('.cms-page-flyer-img');
	var elCldWidth = elCld.width();
	var elLeft = el.css("left");

	el.delay(600).animate({
		left: -elCldWidth
	});

	el.addClass("closed");

	$('.cms-page-flyer-trigger').on('click', function(){
		var elClass = el.hasClass("closed");

		if(elClass == true){
			el.animate({
				left: 0
			});
			el.removeClass('closed');
		} else{
			el.animate({
				left: -elCldWidth
			});
			el.addClass('closed');
		}

	});
};


$(window).load(function() {
  equalheight('[data-equal-height]');
  openFlyer();
  $('.cms-loader').fadeOut('fast');
});


$(window).resize(function(){
  equalheight('[data-equal-height]');
 });
