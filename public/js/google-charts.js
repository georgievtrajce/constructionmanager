google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    if (document.getElementById('chart_div_submittals') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbSubmittals);
        var options = {
            title: 'Submittals',
            colors: ['red', 'green', 'blue'],
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_submittals'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div_rfis') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbRFIs);
        var options = {
            title: 'RFIs',
            colors: ['red', 'green', 'blue'],
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_rfis'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbPCOs);
        var options = {
            title: 'PCOs',
            colors: ['red', 'green', 'blue'],
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div_submittals_details') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbSubmittals);
        var options = {
            colors: ['green', 'lightgreen', 'lightcoral', 'orange', 'red', '#CCCC00', 'blue'],
            backgroundColor: '#f5f5f5',
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_submittals_details'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div_pcos_details') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbPCOs);
        var options = {
            colors: ['green', 'lightgreen', 'lightcoral', 'orange', 'red', '#CCCC00', 'red', 'blue', 'pink'],
            backgroundColor: '#f5f5f5',
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_pcos_details'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div_rfis_details') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbRFIs);
        var options = {
            title: '',
            colors: ['red', 'green', 'blue'],
            backgroundColor: '#f5f5f5',
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_rfis_details'));

        chart.draw(data, options);
    }

    if (document.getElementById('chart_div_tasks_details') != undefined) {
        var data = google.visualization.arrayToDataTable(dataDbTasks);
        var options = {
            title: '',
            colors: ['red', 'green'],
            pieSliceTextStyle: {
                fontSize: 12,
            },
            pieSliceText: 'value',
            is3D: true,
            legend: {
                position: 'labeled'
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_div_tasks_details'));

        chart.draw(data, options);
    }
}
