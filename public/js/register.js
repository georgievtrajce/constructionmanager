/**
 * Created by dragan.atanasov on 3/11/2015.
 */


$(function() {


    $(".js-add-address").click(function() {

    	var addressesCounter = $('.address-container').length;

    	var templateId = '#company-address-template';
    	var container = '.addresses-main-cont';
    	var templateClass = '.address-container';

        var newAddress = cm.toggleTemplate(templateId, container, { counter: addressesCounter }).add();
        
        $(newAddress.element).on('click', '.js-remove-address', function (e) {
            e.preventDefault();
            newAddress.remove();
        });
        
        $(container).on('removed', function () {
        	cm.regenRegisterIndexes(templateClass);
        });

    });

    $('div[data-type="alternative-page"]').parent().css('padding', '0');
    


});