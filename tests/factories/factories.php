<?php
use App\Models\User;
use Laracasts\TestDummy\Factory;
use Illuminate\Support\Facades\Hash;

$company_admin = User::where('title', '=', 'Company Admin')->with('company')->first();

$factory('App\Models\Address_book','Address_book', [
    'name' => $faker->company,
    //'mf_id' => $faker->numberBetween(1,8123),
    'owner_comp_id' => $company_admin->company->id,
]);

$factory('App\Models\Mf_address_book','Mf_address_book', [
    'ab_id' => '',
    'mf_id' => $faker->numberBetween(1,8123),
]);

$factory('App\Models\Ab_contact','Ab_contact', [
    'name' => $faker->name,
    'title' => $faker->title,
    'email' => $faker->email,
    'office_phone' => $faker->phoneNumber,
    'cell_phone' => $faker->phoneNumber,
    'fax' => $faker->phoneNumber,
    'ab_id' => ''
]);
$factory('App\Models\Address','Address', [
    'number' => $faker->numberBetween(1,99),
    'office_title' => $faker->country,
    'street' => $faker->streetAddress,
    'zip' => $faker->postcode,
    'city' => $faker->city,
    'state' => $faker->country,
    'comp_id' => '',
    'ab_id' => '',
    'proj_id'=>'',
]);


$factory('App\Models\Company','Company', [
    'name' => $faker->company,
    'logo' => null,
    'subs_id' => $faker->numberBetween(2,3),
    'ref_points' => $faker->numberBetween(0,10),
    'subscription_payment_date' => date('Y-m-d', strtotime(date("Y-m-d", time()) . " - 300 day")),
    'subscription_expire_date' => date('Y-m-d', strtotime(date("Y-m-d", time()) . " + 65 day")),
    'uploaded' => '0',
    'downloaded' => '0',
]);

//projects
$factory('App\Models\Project','Project', [
    'name' => $faker->sentence(4),
    'price' => $faker->randomFloat($nbMaxDecimals = 4, $min = 0, $max = 4),
    'active' => 1,
    'number' => $faker->randomNumber($nbDigits = NULL),
    'start_date' => date("Y-m-d", time()),
    'end_date' => date("Y-m-d", time()),
    'owner_id' => '',
    'architect_id' => '',
    'comp_id' => '',
]);

//submittals
$factory('App\Models\Submittal','Submittal', [
    'name' => $faker->sentence(4),
    'number' => $faker->randomNumber($nbDigits = NULL),
    'subject' => $faker->sentence(4),
    'sent_via' => $faker->sentence(4),
    'submitted_for' => $faker->sentence(4),
    'submittal_type' => $faker->sentence(4),
    'quantity' => $faker->numberBetween(1,500),
    'mf_number' => '',
    'mf_title' => '',
    'sub_id' => '',
    'recipient_id' => '',
    'gc_id' => '',
    'proj_id' => '',
    'comp_id' => '',
    'user_id' => '',
]);

//submittal versions
$factory('App\Models\Submittal_version','Submittal_version', [
    'submittal_id' => '',
    'cycle_no' => $faker->numberBetween(1,5),
    'status_id' => '',
]);

//files
$factory('App\Models\Project_file','Project_file', [
    'file_name' => 'test_file_name_UPC-000001.pdf',
    'version_date_connection' => '',
    'size' => $faker->numberBetween(5,5000),
]);

//rfis
$factory('App\Models\Rfi','Rfi', [
    'name' => $faker->sentence(4),
    'number' => $faker->randomNumber($nbDigits = NULL),
    'subject' => $faker->sentence(4),
    'sent_via' => $faker->sentence(4),
    'is_change' => $faker->sentence(4),
    'mf_number' => '',
    'mf_title' => '',
    'sub_id' => '',
    'recipient_id' => '',
    'gc_id' => '',
    'proj_id' => '',
    'question' => $faker->sentence(6),
    'answer' => $faker->sentence(6),
    'comp_id' => '',
    'user_id' => '',
]);

//rfi versions
$factory('App\Models\Rfi_version','Rfi_version', [
    'rfi_id' => '',
    'cycle_no' => $faker->numberBetween(1,5),
    'status_id' => '',
]);

//pcos
$factory('App\Models\Pco','Pco', [
    'name' => $faker->sentence(4),
    'number' => $faker->randomNumber($nbDigits = NULL),
    'subject' => $faker->sentence(4),
    'sent_via' => $faker->sentence(4),
    'cost' => $faker->randomFloat($nbMaxDecimals = 4, $min = 0, $max = 4),
    'gc_id' => '',
    'proj_id' => '',
    'reason_for_change' => $faker->sentence(6),
    'description_of_change' => $faker->sentence(6),
]);

//pco subcontractors recipient
$factory('App\Models\Pco_subcontractor_recipient','Pco_subcontractor_recipient', [
    'mf_number' => '',
    'mf_title' => '',
]);

//pco versions
$factory('App\Models\Pco_version','Pco_version', [
    'cycle_no' => $faker->numberBetween(1,5),
    'status_id' => '',
]);

$factory('App\Models\User','factoryUser',[
    'email' => $faker->email,
    'password' => Hash::make('companyadmin123'),
    'name' => $faker->name,
    'title' => $faker->title,
    'office_phone' => $faker->phoneNumber,
    'cell_phone' => $faker->phoneNumber,
    'fax' => $faker->phoneNumber,
    'comp_id' => 'factory:Company'
]);

$factory('App\Models\Address','companyAddress', [
    'number' => $faker->numberBetween(1,99),
    'office_title' => $faker->country,
    'street' => $faker->streetAddress,
    'zip' => $faker->postcode,
    'city' => $faker->city,
    'state' => $faker->country,
    'comp_id' => '',
    'ab_id' => '',
    'proj_id'=>'',
]);

$factory('App\Models\Ab_default_category','Address_book_categories',[
    'ab_id' => $faker->numberBetween(1,50),
    'default_category_id' => $faker->numberBetween(1,6),
]);
