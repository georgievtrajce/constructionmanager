<?php
use Laracasts\TestDummy\DbTestCase;
use Laracasts\TestDummy\Factory;

class ExampleTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public function testBasicExample()
	{
		$response = $this->call('GET', '/');

		$this->assertEquals(200, $response->getStatusCode());
	}

	/** @test **/
	public function generateABEntries(){
		// Before each test, your database will be rolled back
		//Factory::$factoriesPath = 'app/tests/factories';
		Factory::times(50)->create('Address_Book');
		Factory::times(100)->create('Ab_contact');
		Factory::times(100)->create('Address');
	}
}
