var Promise = require('es6-promise').polyfill();
var gulp = require("gulp");
var elixir = require('laravel-elixir');
var exec = require('child_process').exec;
//var sass = require('gulp-ruby-sass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

process.env.DISABLE_NOTIFIER = true;

//elixir.extend("hologram", function() {
//
//	gulp.task("hologram", function (cb) {
//
//		exec('hologram', function (err, stdout, stderr) {
//			console.log(stdout);
//			console.log(stderr);
//			cb(err);
//		});
//
//	});
//
//	this.registerWatcher("hologram", "**/*.scss");
//
//	return this.queueTask("hologram");
//
//});

elixir(function(mix) {
	mix
		.sass('app.scss')
		//.rubySass('doc.scss', 'resources/docs')
		//.rubySass('app.scss', 'public/css')
		.copy('resources/assets/img', 'public/img')
		.scripts([
			"jquery-ui.js",
			"underscore.js",
			"material.js",
			"ripples.js",
			"cm.toggleTemplate.js",
			"projects.js",
			"register.js",
			"address-book.js",
			"autocomplete.js",
			"datepicker.js",
			"submittals.js",
			"share-submittals.js",
			"share-rfis.js",
			"share-pcos.js",
			"project-files.js",
			"app.js",
			"manage-users.js",
			"tinymce.js",
			"super-admin.js",
			"companies.js",
			"file-upload.js",
			"file-download.js",
			"file-delete.js",
			"transmittals.js",
			"pcos.js",
			"record-delete.js",
			"popups.js",
            "tasks.js",
			"daily-reports.js"
		]);

		//.hologram();

});
